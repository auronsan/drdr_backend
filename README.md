# README #

Please read to get your application up and running.

### What is this repository for? ###

### DrDr Backend ###
* **Language**: Python
* **Database**: Postgresql
* **Framework API**: falcon
* **URL**: doctordoctor.help:8000
* **API Specifications**: https://goo.gl/QCn21n
* **Branch DEV**: master
* **Branch Production**: master
### Version: 0.1 ###

### How do I get set up? ###

* OS: MacOs/Ubuntu
* Install Python 2.x
* Dependencies Modules (Daily update)
* * opentok
* * paho-mqtt
* * psycopg2
* * falcon
* * facebook-sdk
* * twilio (sudo apt-get install libssl-dev)
* * python-dateutil
* * apns
* * numpy
* * paypalrestsdk
* * raven
* * stripe
* * configparser
* * ...

* Database configuration at file "environment.conf" in project
* * ERP_DB_HOST = doctordoctor.help
* * ERP_DB_PORT = 5432
* * ERP_DB_USER = ...
* * ERP_DB_PASS = ...
* * ERP_DB_NAME = ..

```
cd /drdr_api
gunicorn 'drdr_api:load_app("environment.conf")'

```
* Setup supervisor to run

```
[program:drdr_api]
command=/usr/local/bin/gunicorn -w 5 -b 0.0.0.0:8000 'drdr_api:load_app("environment.conf")'
user=bao
directory=/home/bao/drdr_backend/
autostart=true
autorestart=true
stderr_logfile=/var/log/drdr_api.log
stdout_logfile=/var/log/drdr_api.log
```

* * Add cron job (if not existed)
```
insert into ir_cron(name,function,interval_type,args,numbercall,nextcall,priority,doall,active,interval_number,model,user_id)
values
('Cancel Appointment live','cancel_appointment_live','minutes','()','-1',now(),'5','False','True','2','cron_appointment',1),
('Update activity status user','update_activity_status_user','minutes','()','-1',now(),'5','False','True','15','cron_user_account',1);
```

```
[program:drdr_cronjob]
command=/usr/bin/python /home/bao/drdr_backend/autoscript/cron_job.py
user=bao
autostart=true
autorestart=true
stderr_logfile=/var/log/drdr_cronjob.log
stdout_logfile=/var/log/drdr_cronjob.log
```

```
sudo service supervisor restart
```



* Deployment instructions