-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2017-11-30 06:11:17.959

-- tables
-- Table: appointment
CREATE TABLE appointment (
    id bigserial  NOT NULL,
    created_date timestamp  NULL,
    schedule_date timestamp  NULL,
    type varchar(50)  NULL,
    state varchar(50)  NULL,
    note text  NULL,
    doctor_id bigint  NULL,
    patient_id bigint  NULL,
    rating_point real  NULL,
    rating_comment text  NULL,
    cost real  NULL,
    unit_cost varchar(50)  NULL,
    review_update timestamp  NULL,
    write_date timestamp  NULL,
    consultation_note text  NULL,
    prom_trans_id bigint  NULL,
    consultation_duration text  NULL,
    parent_id bigint  NULL,
    reschedule_count int  NULL,
    consultation_fee float  NULL,
    suspend_reason text  NULL,
    is_accepted boolean  NULL,
    timeslot_code varchar(100)  NULL,
    booking_status varchar(100) NULL,
    CONSTRAINT appointment_pk PRIMARY KEY (id)
);

CREATE INDEX appointment_idx_type on appointment (type ASC);

CREATE INDEX appointment_idx_state on appointment (state ASC);

CREATE INDEX appointment_idx_schedule_date on appointment (schedule_date ASC);

CREATE INDEX appointment_idx_doctor_id on appointment (doctor_id ASC);

CREATE INDEX appointment_idx_patient_id on appointment (parent_id ASC);

CREATE INDEX appointment_idx_is_accepted on appointment (is_accepted ASC);

-- Table: appointment_call_history
CREATE TABLE appointment_call_history (
    id bigserial  NOT NULL,
    appointment_id bigint  NOT NULL,
    archive_id varchar(100)  NULL,
    start_time timestamp  NULL,
    end_time timestamp  NULL,
    session_id varchar(100)  NULL,
    token_to varchar(100)  NULL,
    token_from varchar(100)  NULL,
    state varchar(50)  NULL,
    doctor_call_type varchar(50)  NULL,
    patient_call_type varchar(50)  NULL,
    create_date timestamp  NULL,
    write_date timestamp  NULL,
    CONSTRAINT appointment_call_history_pk PRIMARY KEY (id)
);

CREATE INDEX appointment_call_history_idx_appointment_id on appointment_call_history (appointment_id ASC);

CREATE INDEX appointment_call_history_idx_state on appointment_call_history (state ASC);

CREATE INDEX appointment_call_history_idx_session_id on appointment_call_history (session_id ASC);

CREATE INDEX appointment_call_history_idx_token_to on appointment_call_history (token_to ASC);

CREATE INDEX appointment_call_history_idx_token_from on appointment_call_history (token_from ASC);

-- Table: appointment_medical_record
CREATE TABLE appointment_medical_record (
    id bigserial  NOT NULL,
    create_date timestamp  NULL,
    write_date timestamp  NULL,
    appointment_id bigint  NULL,
    note text  NULL,
    code varchar(50)  NULL,
    attachment_id bigint  NULL,
    user_mngment_id bigint  NULL,
    create_uid bigint  NULL,
    CONSTRAINT appointment_medical_record_pk PRIMARY KEY (id)
);

CREATE INDEX appointment_medical_record_idx_appointment_id on appointment_medical_record (appointment_id ASC);

CREATE INDEX appointment_medical_record_idx_user_mngment_id on appointment_medical_record (user_mngment_id ASC);

CREATE INDEX appointment_medical_record_idx_attachment_id on appointment_medical_record (attachment_id ASC);

-- Table: appointment_review
CREATE TABLE appointment_review (
    id bigserial  NOT NULL,
    appointment_id bigint  NOT NULL,
    comment text  NULL,
    rating_point real  NULL,
    write_date timestamp  NULL,
    create_date timestamp  NULL,
    CONSTRAINT appointment_review_pk PRIMARY KEY (id)
);

CREATE INDEX appointment_review_idx_appointment_id on appointment_review (appointment_id ASC);

-- Table: appointment_transaction
CREATE TABLE appointment_transaction (
    id bigserial  NOT NULL,
    create_date timestamp  NULL,
    write_date timestamp  NULL,
    appointment_id bigint  NOT NULL,
    payment_trans_id bigint  NULL,
    from_state varchar(50)  NULL,
    to_state varchar(50) NULL,
    note text  NULL,
    user_account_id bigint  NOT NULL,
    CONSTRAINT appointment_transaction_pk PRIMARY KEY (id)
);

CREATE INDEX appointment_transaction_idx_appointment_id on appointment_transaction (appointment_id ASC);

CREATE INDEX appointment_transaction_idx_user_account_id on appointment_transaction (user_account_id ASC);

-- Table: attachment
CREATE TABLE attachment (
    id bigserial  NOT NULL,
    create_date timestamp  NULL,
    write_date timestamp  NULL,
    write_uid bigint  NULL,
    create_uid bigint  NULL,
    res_model varchar(100)  NULL,
    res_id bigint  NULL,
    file_name varchar(250)  NULL,
    file_size int  NULL,
    description text  NULL,
    mimetype varchar(250)  NULL,
    url text  NULL,
    file_extension varchar(100)  NULL,
    CONSTRAINT attachment_pk PRIMARY KEY (id)
);

CREATE INDEX attachment_idx_res_model on attachment (res_model ASC);

CREATE INDEX attachment_idx_res_id on attachment (res_id ASC);

-- Table: business_transfer_transaction
CREATE TABLE business_transfer_transaction (
    id bigserial  NOT NULL,
    type varchar(50)  NULL,
    amount float  NULL,
    trans_date timestamp  NULL,
    state varchar(50)  NULL,
    note text  NULL,
    business_type varchar(50)  NULL,
    payment_transaction_id bigint  NULL,
    withdraw_transaction_id bigint  NULL,
    created_date timestamp  NULL,
    write_date timestamp  NULL,
    wallet_source_id bigint  NULL,
    wallet_dest_id bigint  NULL,
    appointment_id bigint  NULL,
    fee_type varchar(50)  NULL,
    currency varchar(50)  NULL,
    balance_source_before float  NULL,
    balance_source_after float  NULL,
    balance_dest_before float  NULL,
    balance_dest_after float  NULL,
    amount_commission float  NULL,
    CONSTRAINT business_transfer_transaction_pk PRIMARY KEY (id)
);

CREATE INDEX business_transfer_transaction_idx_state on business_transfer_transaction (state ASC);

CREATE INDEX business_transfer_transaction_idx_type on business_transfer_transaction (type ASC);

CREATE INDEX business_transfer_transaction_idx_appointment_id on business_transfer_transaction (appointment_id ASC);

CREATE INDEX business_transfer_transaction_idx_wallet_source_id on business_transfer_transaction (wallet_source_id ASC);

CREATE INDEX business_transfer_transaction_idx_wallet_dest_id on business_transfer_transaction (wallet_dest_id ASC);

-- Table: calendar_event
CREATE TABLE calendar_event (
    id bigserial  NOT NULL,
    name varchar(100)  NULL,
    create_date timestamp  NULL,
    write_date timestamp  NULL,
    user_id bigint  NULL,
    allday boolean  NULL,
    start_date date  NULL,
    stop_date date  NULL,
    duration float  NULL,
    start_datetime timestamp  NULL,
    end_datetime timestamp  NULL,
    recurrency boolean  NULL,
    recurrent_id bigint  NULL,
    end_type varchar(50)  NULL,
    count int  NULL,
    final_date date  NULL,
    interval int  NULL,
    rrule_type text  NULL,
    state varchar(50)  NULL,
    description text  NULL,
    stop timestamp  NULL,
    stop_datetime timestamp  NULL,
    active boolean  NULL,
    mo boolean  NULL,
    tu boolean  NULL,
    we boolean  NULL,
    th boolean  NULL,
    fr boolean  NULL,
    sa boolean  NULL,
    su boolean  NULL,
    month_by varchar(50)  NULL,
    day int  NULL,
    byday varchar(50)  NULL,
    week_list text  NULL,
    CONSTRAINT calendar_event_pk PRIMARY KEY (id)
);

CREATE INDEX calendar_event_idx_user_id on calendar_event (user_id ASC);

CREATE INDEX calendar_event_idx_active on calendar_event (active ASC);

CREATE INDEX calendar_event_idx_state on calendar_event (state ASC);

-- Table: doctor_profile
CREATE TABLE doctor_profile (
    id bigserial  NOT NULL,
    user_mngment_id bigint  NOT NULL,
    consultation_type varchar(50)  NULL,
    consultation_fee real  NULL,
    consultation_duration varchar(50)  NULL,
    years_of_experience int  NULL,
    graduation_date date  NULL,
    medical_school text  NULL,
    post_graduate_training text  NULL,
    industry_relationship text  NULL,
    publications text  NULL,
    teaching text  NULL,
    community_service text  NULL,
    criminal_convictions text  NULL,
    limitations text  NULL,
    hospital_restrictions text  NULL,
    about text  NULL,
    sum_rating_point real  NULL,
    count_rating_point int  NULL,
    consultation_unit varchar(50)  NULL,
    professional_affiliations_and_activities text  NULL,
    hospital_profile_id bigint  NULL,
    CONSTRAINT doctor_profile_pk PRIMARY KEY (id)
);

CREATE INDEX doctor_profile_idx_user_mngment_id on doctor_profile (user_mngment_id ASC);

-- Table: dr_doctorprofile_languages
CREATE TABLE dr_doctorprofile_languages (
    id bigserial  NOT NULL,
    doctor_profile_id bigint  NOT NULL,
    spoken_language varchar(50)  NOT NULL,
    CONSTRAINT dr_doctorprofile_languages_pk PRIMARY KEY (id)
);

CREATE INDEX dr_doctorprofile_languages_idx_doctor_profile_id on dr_doctorprofile_languages (doctor_profile_id ASC);

-- Table: dr_doctorprofile_specialties
CREATE TABLE dr_doctorprofile_specialties (
    id bigserial  NOT NULL,
    doctor_profile_id bigint  NOT NULL,
    specialties_code varchar(50)  NOT NULL,
    years_of_exp int  NULL,
    CONSTRAINT dr_doctorprofile_specialties_pk PRIMARY KEY (id)
);

CREATE INDEX dr_doctorprofile_specialties_idx_doctor_profile_id on dr_doctorprofile_specialties (doctor_profile_id ASC);

-- Table: dr_license
CREATE TABLE dr_license (
    id bigserial  NOT NULL,
    number varchar(50)  NULL,
    license_status varchar(100)  NULL,
    issue_date date  NULL,
    expiry_date date  NULL,
    issue_by varchar(50)  NULL,
    doctor_profile_id bigint  NOT NULL,
    CONSTRAINT dr_license_pk PRIMARY KEY (id)
);

CREATE INDEX dr_license_idx_doctor_profile_id on dr_license (doctor_profile_id ASC);

-- Table: facebook_account
CREATE TABLE facebook_account (
    id bigserial  NOT NULL,
    user_account_id bigint  NOT NULL,
    access_token text  NULL,
    access_token_expiry_time timestamp  NULL,
    facebook_id bigint  NULL,
    CONSTRAINT facebook_account_pk PRIMARY KEY (id)
);

CREATE INDEX facebook_account_idx_user_account_id on facebook_account (user_account_id ASC);

CREATE INDEX facebook_account_idx_facebook_id on facebook_account (facebook_id ASC);

-- Table: general_medical_record
CREATE TABLE general_medical_record (
    id bigserial  NOT NULL,
    create_date timestamp  NULL,
    write_date timestamp  NULL,
    user_mngment_id bigint  NOT NULL,
    height text  NULL,
    weight text  NULL,
    blood_type text  NULL,
    smoking text  NULL,
    alcohol_intake text  NULL,
    sleep_rest text  NULL,
    exercise text  NULL,
    stress_coping text  NULL,
    roles_relationship text  NULL,
    psychococial text  NULL,
    other_substance text  NULL,
    CONSTRAINT general_medical_record_pk PRIMARY KEY (id)
);

CREATE INDEX general_medical_record_idx_user_mngment_id on general_medical_record (user_mngment_id ASC);

-- Table: patient_doctor_favourite
CREATE TABLE patient_doctor_favourite (
    id bigserial  NOT NULL,
    create_date timestamp  NULL,
    patient_id bigint  NOT NULL,
    doctor_id bigint  NOT NULL,
    CONSTRAINT patient_doctor_favourite_pk PRIMARY KEY (id)
);

-- Table: patient_profile
CREATE TABLE patient_profile (
    id bigserial  NOT NULL,
    user_mngment_id bigint  NOT NULL,
    ethnicity varchar(50)  NULL,
    highest_education_level varchar(100)  NULL,
    occupation varchar(50)  NULL,
    material_status varchar(50)  NULL,
    hospital_profile_id bigint  NULL,
    CONSTRAINT patient_profile_pk PRIMARY KEY (id)
);

CREATE INDEX patient_profile_idx_user_mngment_id on patient_profile (user_mngment_id ASC);

-- Table: payment_transaction
CREATE TABLE payment_transaction (
    id bigserial  NOT NULL,
    merref varchar(100)  NULL,
    amount float  NULL,
    phone_card_sn varchar(100)  NULL,
    state varchar(50)  NULL,
    created_date timestamp  NULL,
    write_date timestamp  NULL,
    type varchar(50)  NULL,
    payment_gateway varchar(50)  NULL,
    customer_token varchar(50)  NULL,
    user_wallet_id bigint  NOT NULL,
    user_payment_info_id bigint  NULL,
    trans_date timestamp  NULL,
    note text  NULL,
    gateway_trans_code text  NULL,
    CONSTRAINT payment_transaction_pk PRIMARY KEY (id)
);

CREATE INDEX payment_transaction_idx_user_wallet_id on payment_transaction (user_wallet_id ASC);

CREATE INDEX payment_transaction_idx_type on payment_transaction (type ASC);

CREATE INDEX payment_transaction_idx_state on payment_transaction (state ASC);

-- Table: promotion_code
CREATE TABLE promotion_code (
    id bigserial  NOT NULL,
    code varchar(250)  NULL,
    active boolean  NULL,
    type varchar(50)  NULL,
    discount_type varchar(50)  NULL,
    value real  NULL,
    unit_code varchar(50)  NULL,
    qty int  NULL,
    qty_available int  NULL,
    expiry_time timestamp  NULL,
    CONSTRAINT promotion_code_pk PRIMARY KEY (id)
);

CREATE INDEX promotion_code_idx_code on promotion_code (code ASC);

CREATE INDEX promotion_code_idx_type on promotion_code (type ASC);

CREATE INDEX promotion_code_idx_active on promotion_code (active ASC);

-- Table: promotion_transaction
CREATE TABLE promotion_transaction (
    id bigserial  NOT NULL,
    user_account_id bigint  NOT NULL,
    promotion_id bigint  NULL,
    state varchar(50)  NULL,
    token varchar(256)  NULL,
    expiry_time timestamp  NULL,
    CONSTRAINT promotion_transaction_pk PRIMARY KEY (id)
);

CREATE INDEX promotion_transaction_idx_user_account_id on promotion_transaction (user_account_id ASC);

CREATE INDEX promotion_transaction_idx_token on promotion_transaction (token ASC);

CREATE INDEX promotion_transaction_idx_state on promotion_transaction (state ASC);

-- Table: user_account
CREATE TABLE user_account (
    id bigserial  NOT NULL,
    user_mngment_id bigint  NULL,
    email varchar(100)  NULL,
    phone varchar(50)  NULL,
    phone_confirmed boolean  NULL,
    validation_status varchar(20)  NULL,
    email_confirmed boolean  NULL,
    password varchar(250)  NULL,
    password_salt varchar(10)  NULL,
    password_hash_algorithm varchar(50)  NULL,
    latest_activity_time timestamp  NULL,
    created_date timestamp  NULL,
    email_confirm_token varchar(256)  NULL,
    email_confirm_token_expiry_time timestamp  NULL,
    phone_confirm_token varchar(10)  NULL,
    phone_confirm_token_expiry_time timestamp  NULL,
    reset_password_token varchar(256)  NULL,
    reset_password_token_expiry_time timestamp  NULL,
    activity_status varchar(20)  NULL,
    call_register boolean  NULL,
    ios_device_token varchar(250)  NULL,
    locale varchar(20) NULL,

    CONSTRAINT user_account_pk PRIMARY KEY (id)
);

CREATE INDEX user_account_idx_user_mngment_id on user_account (user_mngment_id ASC);

CREATE INDEX user_account_idx_email on user_account (email ASC);

CREATE INDEX user_account_idx_phone on user_account (phone ASC);

CREATE INDEX user_account_idx_password on user_account (password ASC);

CREATE INDEX user_account_idx_validation_status on user_account (validation_status ASC);

-- Table: user_account_bank
CREATE TABLE user_account_bank (
    id bigserial  NOT NULL,
    user_account_id bigint  NULL,
    holder_name varchar(100)  NULL,
    account_number varchar(100)  NULL,
    bank_branch varchar(100)  NULL,
    meta_bank_id int  NULL,
    is_primary boolean  NULL,
    CONSTRAINT user_account_bank_pk PRIMARY KEY (id)
);

CREATE INDEX user_account_bank_idx_user_account_id on user_account_bank (user_account_id ASC);

CREATE INDEX user_account_bank_idx_is_primary on user_account_bank (is_primary ASC);

-- Table: user_account_pagination
CREATE TABLE user_account_pagination (
    id bigserial  NOT NULL,
    list_id text  NULL,
    table_pagination varchar(100)  NULL,
    user_account_id bigint  NOT NULL,
    CONSTRAINT user_account_pagination_pk PRIMARY KEY (id)
);

CREATE INDEX user_account_pagination_idx_user_account_id on user_account_pagination (user_account_id ASC);

CREATE INDEX user_account_pagination_idx_table_pagination on user_account_pagination (table_pagination ASC);

-- Table: user_account_token
CREATE TABLE user_account_token (
    id bigserial  NOT NULL,
    user_account_id bigint  NOT NULL,
    token varchar(256)  NOT NULL,
    expiry_time timestamp  NULL,
    client_id text  NULL,
    CONSTRAINT user_account_token_pk PRIMARY KEY (id)
);

CREATE INDEX user_account_token_idx_user_account_id on user_account_token (user_account_id ASC);

CREATE INDEX user_account_token_idx_client_id on user_account_token (client_id ASC);

CREATE INDEX user_account_token_idx_token on user_account_token (token ASC);

-- Table: user_account_user_role
CREATE TABLE user_account_user_role (
    id bigserial  NOT NULL,
    user_role_id bigint  NOT NULL,
    user_account_id bigint  NOT NULL,
    CONSTRAINT user_account_user_role_pk PRIMARY KEY (id)
);

CREATE INDEX user_account_user_role_idx_1 on user_account_user_role (user_role_id ASC,user_account_id ASC);

-- Table: user_address
CREATE TABLE user_address (
    id bigserial  NOT NULL,
    number varchar(100)  NULL,
    street varchar(100)  NULL,
    state varchar(100)  NULL,
    city varchar(100)  NULL,
    country varchar(100)  NULL,
    postcode varchar(100)  NULL,
    user_contact_id bigint  NOT NULL,
    is_primary boolean  NULL,
    CONSTRAINT user_address_pk PRIMARY KEY (id)
);

CREATE INDEX user_address_idx_user_contact_id on user_address (user_contact_id ASC);

CREATE INDEX user_address_idx_is_primary on user_address (is_primary ASC);

-- Table: user_contact
CREATE TABLE user_contact (
    id bigserial  NOT NULL,
    user_mngment_id bigint  NOT NULL,
    home_phone varchar(50)  NULL,
    work_phone varchar(50)  NULL,
    mobile_phone varchar(50)  NULL,
    email varchar(50)  NULL,
    CONSTRAINT user_contact_pk PRIMARY KEY (id)
);

CREATE INDEX user_contact_idx_user_mngment_id on user_contact (user_mngment_id ASC);

-- Table: user_login_activity
CREATE TABLE user_login_activity (
    id bigserial  NOT NULL,
    user_account_id bigint  NOT NULL,
    platform varchar(50)  NULL,
    ip_address varchar(50)  NULL,
    time timestamp  NULL,
    location varchar(50)  NULL,
    is_register boolean  NULL,
    CONSTRAINT user_login_activity_pk PRIMARY KEY (id)
);

CREATE INDEX user_login_activity_idx_user_account_id on user_login_activity (user_account_id ASC);

-- Table: user_mngment
CREATE TABLE user_mngment (
    id bigserial  NOT NULL,
    type_profile varchar(10)  NULL,
    last_name varchar(100)  NULL,
    middle_name varchar(100)  NULL,
    first_name varchar(100)  NULL,
    complete_name varchar(300)  NULL,
    birthday date  NULL,
    user_mngment_id bigint  NULL,
    relationship varchar(50)  NULL,
    gender varchar(20)  NULL,
    title varchar(20)  NULL,
    profile_image_url varchar(250)  NULL,
    spoken_language varchar(50)  NULL,
    degree_title varchar(50)  NULL,
    CONSTRAINT user_mngment_pk PRIMARY KEY (id)
);

CREATE INDEX user_mngment_idx_user_mngment_id on user_mngment (user_mngment_id ASC);

-- Table: user_payment_info
CREATE TABLE user_payment_info (
    id bigserial  NOT NULL,
    payment_type_code varchar(50)  NULL,
    short_number bigint  NULL,
    subscription_key varchar(50)  NULL,
    crerated_date timestamp  NULL,
    write_date timestamp  NULL,
    user_wallet_id bigint  NOT NULL,
    CONSTRAINT user_payment_info_pk PRIMARY KEY (id)
);

CREATE INDEX user_payment_info_idx_user_wallet_id on user_payment_info (user_wallet_id ASC);

-- Table: user_role
CREATE TABLE user_role (
    id bigserial  NOT NULL,
    name varchar(255)  NOT NULL,
    code varchar(100)  NOT NULL,
    CONSTRAINT user_role_pk PRIMARY KEY (id)
);

insert into user_role(code,name) values ('patient','Patient'), ('doctor','Doctor'), ('admin','Admin');

-- Table: user_setting
CREATE TABLE user_setting (
    id bigserial  NOT NULL,
    user_account_id bigint  NOT NULL,
    language_code varchar(50)  NULL,
    automatic_video boolean  NULL,
    CONSTRAINT user_setting_pk PRIMARY KEY (id)
);

CREATE INDEX user_setting_idx_user_account_id on user_setting (user_account_id ASC);

-- Table: user_wallet
CREATE TABLE user_wallet (
    id bigserial  NOT NULL,
    total_balance float  NULL,
    available_balance float  NULL,
    state varchar(50)  NULL,
    created_date timestamp  NULL,
    write_date timestamp  NULL,
    user_account_id bigint  NOT NULL,
    unitcode varchar(50) NULL,
    hold_balance float  NULL,
    CONSTRAINT user_wallet_pk PRIMARY KEY (id)
);

CREATE INDEX user_wallet_idx_user_account_id on user_wallet (user_account_id ASC);

CREATE INDEX user_wallet_idx_state on user_wallet (state ASC);

-- Table: withdraw_transaction
CREATE TABLE withdraw_transaction (
    id bigserial  NOT NULL,
    amount float  NULL,
    withdraw_trans_code varchar(50)  NULL,
    trans_date timestamp  NULL,
    bank_account_name varchar(100)  NULL,
    bank_code varchar(50)  NULL,
    state varchar(50)  NULL,
    created_date timestamp  NULL,
    write_date timestamp  NULL,
    user_account_id bigint  NULL,
    note text  NULL,
    currency_unit varchar(50)  NULL,
    account_bank_id int  NULL,
    CONSTRAINT withdraw_transaction_pk PRIMARY KEY (id)
);

CREATE INDEX withdraw_transaction_idx_user_account_id on withdraw_transaction (user_account_id ASC);

CREATE INDEX withdraw_transaction_idx_state on withdraw_transaction (state ASC);

-- Table: hospital_profile

CREATE TABLE hospital_profile (
    id bigserial  NOT NULL,
    hospital_name  varchar(50)  NULL,
    hospital_address  varchar(50)  NULL,
    hospital_desc  varchar(200)  NULL,
    hospital_logo  varchar(50)  NULL,
    CONSTRAINT hospital_pk PRIMARY KEY (id)
);


CREATE TABLE call_record_logs (
    id bigserial  NOT NULL,
    patient_id  varchar(50)  NULL,
    doctor_id  varchar(50)  NULL,
    status_call varchar(200)  NULL,
    last_update  timestamp NULL DEFAULT current_timestamp,
    CONSTRAINT call_record_logs_pk PRIMARY KEY (id)
);



CREATE TABLE user_status_call (
    id bigserial  NOT NULL,
    user_account_id bigint  NOT NULL,
    status_call varchar(200)  NULL,
    last_update  timestamp NULL DEFAULT current_timestamp,
    CONSTRAINT user_status_pk PRIMARY KEY (id)
);


CREATE TABLE dd_card_file_list (
    id bigserial  NOT NULL,
    csv_file  varchar(200)  NULL,
    data_file varchar(200)  NULL,
    pdf_file   varchar(200)  NULL
);

-- foreign keys
-- Reference: Feedback_Appointment (table: appointment_review)
ALTER TABLE appointment_review ADD CONSTRAINT Feedback_Appointment
    FOREIGN KEY (appointment_id)
    REFERENCES appointment (id)
    ON DELETE  SET NULL
    ON UPDATE  CASCADE
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: VideoCall_Appointment (table: appointment_call_history)
ALTER TABLE appointment_call_history ADD CONSTRAINT VideoCall_Appointment
    FOREIGN KEY (appointment_id)
    REFERENCES appointment (id)
    ON DELETE  SET NULL
    ON UPDATE  CASCADE
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: appointment_medical_record_appointment (table: appointment_medical_record)
ALTER TABLE appointment_medical_record ADD CONSTRAINT appointment_medical_record_appointment
    FOREIGN KEY (attachment_id)
    REFERENCES appointment (id)
    ON DELETE  SET NULL
    ON UPDATE  CASCADE
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: appointment_transaction_appointment (table: appointment_transaction)
ALTER TABLE appointment_transaction ADD CONSTRAINT appointment_transaction_appointment
    FOREIGN KEY (user_account_id)
    REFERENCES appointment (id)
    ON DELETE  SET NULL
    ON UPDATE  CASCADE
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: business_transfer_transaction_appointment (table: business_transfer_transaction)
ALTER TABLE business_transfer_transaction ADD CONSTRAINT business_transfer_transaction_appointment
    FOREIGN KEY (appointment_id)
    REFERENCES appointment (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: business_transfer_transaction_payment_transaction (table: business_transfer_transaction)
ALTER TABLE business_transfer_transaction ADD CONSTRAINT business_transfer_transaction_payment_transaction
    FOREIGN KEY (payment_transaction_id)
    REFERENCES payment_transaction (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: business_transfer_transaction_withdraw_transaction (table: business_transfer_transaction)
ALTER TABLE business_transfer_transaction ADD CONSTRAINT business_transfer_transaction_withdraw_transaction
    FOREIGN KEY (withdraw_transaction_id)
    REFERENCES withdraw_transaction (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: doctor_favourite_user_mngment (table: patient_doctor_favourite)
ALTER TABLE patient_doctor_favourite ADD CONSTRAINT doctor_favourite_user_mngment
    FOREIGN KEY (patient_id)
    REFERENCES user_mngment (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: doctor_profile_user_mngment (table: doctor_profile)
ALTER TABLE doctor_profile ADD CONSTRAINT doctor_profile_user_mngment
    FOREIGN KEY (user_mngment_id)
    REFERENCES user_mngment (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: dr_doctorprofile_languages_doctor_profile (table: dr_doctorprofile_languages)
ALTER TABLE dr_doctorprofile_languages ADD CONSTRAINT dr_doctorprofile_languages_doctor_profile
    FOREIGN KEY (doctor_profile_id)
    REFERENCES doctor_profile (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: dr_license_doctor_profile (table: dr_license)
ALTER TABLE dr_license ADD CONSTRAINT dr_license_doctor_profile
    FOREIGN KEY (doctor_profile_id)
    REFERENCES doctor_profile (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: dr_specialties_doctor_profile (table: dr_doctorprofile_specialties)
ALTER TABLE dr_doctorprofile_specialties ADD CONSTRAINT dr_specialties_doctor_profile
    FOREIGN KEY (doctor_profile_id)
    REFERENCES doctor_profile (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: facebook_account_UserAccount (table: facebook_account)
ALTER TABLE facebook_account ADD CONSTRAINT facebook_account_UserAccount
    FOREIGN KEY (user_account_id)
    REFERENCES user_account (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: general_medical_record_user_mngment (table: general_medical_record)
ALTER TABLE general_medical_record ADD CONSTRAINT general_medical_record_user_mngment
    FOREIGN KEY (user_mngment_id)
    REFERENCES user_mngment (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: patient_favourite_user_mngment (table: patient_doctor_favourite)
ALTER TABLE patient_doctor_favourite ADD CONSTRAINT patient_favourite_user_mngment
    FOREIGN KEY (doctor_id)
    REFERENCES user_mngment (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: patient_profile_user_mngment (table: patient_profile)
ALTER TABLE patient_profile ADD CONSTRAINT patient_profile_user_mngment
    FOREIGN KEY (user_mngment_id)
    REFERENCES user_mngment (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: payment_transaction_user_payment_info (table: payment_transaction)
ALTER TABLE payment_transaction ADD CONSTRAINT payment_transaction_user_payment_info
    FOREIGN KEY (user_payment_info_id)
    REFERENCES user_payment_info (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: payment_transaction_user_wallet (table: payment_transaction)
ALTER TABLE payment_transaction ADD CONSTRAINT payment_transaction_user_wallet
    FOREIGN KEY (user_wallet_id)
    REFERENCES user_wallet (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: promotion_transaction_promotion_code (table: promotion_transaction)
ALTER TABLE promotion_transaction ADD CONSTRAINT promotion_transaction_promotion_code
    FOREIGN KEY (promotion_id)
    REFERENCES promotion_code (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: promotion_transaction_user_account (table: promotion_transaction)
ALTER TABLE promotion_transaction ADD CONSTRAINT promotion_transaction_user_account
    FOREIGN KEY (user_account_id)
    REFERENCES user_account (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: user_account_pagination_user_account (table: user_account_pagination)
ALTER TABLE user_account_pagination ADD CONSTRAINT user_account_pagination_user_account
    FOREIGN KEY (user_account_id)
    REFERENCES user_account (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: user_account_token_user_account (table: user_account_token)
ALTER TABLE user_account_token ADD CONSTRAINT user_account_token_user_account
    FOREIGN KEY (user_account_id)
    REFERENCES user_account (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: user_account_user_mngment (table: user_account)
ALTER TABLE user_account ADD CONSTRAINT user_account_user_mngment
    FOREIGN KEY (user_mngment_id)
    REFERENCES user_mngment (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: user_account_user_role_user_account (table: user_account_user_role)
ALTER TABLE user_account_user_role ADD CONSTRAINT user_account_user_role_user_account
    FOREIGN KEY (user_account_id)
    REFERENCES user_account (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: user_account_user_role_user_role (table: user_account_user_role)
ALTER TABLE user_account_user_role ADD CONSTRAINT user_account_user_role_user_role
    FOREIGN KEY (user_role_id)
    REFERENCES user_role (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: user_address_user_contact (table: user_address)
ALTER TABLE user_address ADD CONSTRAINT user_address_user_contact
    FOREIGN KEY (user_contact_id)
    REFERENCES user_contact (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: user_contact_user_profile (table: user_contact)
ALTER TABLE user_contact ADD CONSTRAINT user_contact_user_profile
    FOREIGN KEY (user_mngment_id)
    REFERENCES user_mngment (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: user_login_activity_user_account (table: user_login_activity)
ALTER TABLE user_login_activity ADD CONSTRAINT user_login_activity_user_account
    FOREIGN KEY (user_account_id)
    REFERENCES user_account (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: user_mngment_user_mngment (table: user_mngment)
ALTER TABLE user_mngment ADD CONSTRAINT user_mngment_user_mngment
    FOREIGN KEY (user_mngment_id)
    REFERENCES user_mngment (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: user_payment_info_user_wallet (table: user_payment_info)
ALTER TABLE user_payment_info ADD CONSTRAINT user_payment_info_user_wallet
    FOREIGN KEY (user_wallet_id)
    REFERENCES user_wallet (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: user_setting_user_account (table: user_setting)
ALTER TABLE user_setting ADD CONSTRAINT user_setting_user_account
    FOREIGN KEY (user_account_id)
    REFERENCES user_account (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: user_wallet_user_account (table: user_wallet)
ALTER TABLE user_wallet ADD CONSTRAINT user_wallet_user_account
    FOREIGN KEY (user_account_id)
    REFERENCES user_account (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference : add new column for hospital migrate function
ALTER TABLE doctor_profile ADD COLUMN hospital_profile_id bigint NULL 
;
ALTER TABLE patient_profile ADD COLUMN hospital_profile_id bigint NULL 
;
--end alter constrain :
ALTER TABLE doctor_profile ADD CONSTRAINT hospital_profile
    FOREIGN KEY (hospital_profile_id)
    REFERENCES hospital_profile (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

ALTER TABLE patient_profile ADD CONSTRAINT hospital_profile
    FOREIGN KEY (hospital_profile_id)
    REFERENCES hospital_profile (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;
-- End of file.
