-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2017-11-30 06:06:14.249

-- Table: system_params
CREATE TABLE system_params (
    id bigserial  NOT NULL,
    code char(20)  NOT NULL,
    value varchar(255)  NOT NULL,
    CONSTRAINT system_params_pk PRIMARY KEY (id)
);

insert into system_params(code,value) values ('update_time',to_char(now(), 'YYYY-MM-DD HH24:MI:SS'));

-- End of file.

CREATE OR REPLACE FUNCTION public.func_trigger_update_time_update_meta()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
BEGIN

update system_params set value = to_char(now(), 'YYYY-MM-DD HH24:MI:SS') where code = 'update_time';

RETURN NEW;
END;
$function$;

CREATE TRIGGER trigger_update_time_user_role
AFTER INSERT OR UPDATE OR DELETE ON user_role
FOR EACH ROW
EXECUTE PROCEDURE func_trigger_update_time_update_meta();



-- tables
-- Table: ir_cron
CREATE TABLE ir_cron (
    id serial  NOT NULL,
    create_uid bigint  NULL,
    create_date timestamp  NULL,
    write_date timestamp  NULL,
    write_uid bigint  NULL,
    function varchar(100)  NULL,
    interval_type varchar(50)  NULL,
    user_id bigint  NULL,
    name varchar(250)  NULL,
    args text  NULL,
    numbercall int  NULL,
    nextcall timestamp  NULL,
    priority int  NULL,
    doall boolean  NULL,
    active boolean  NULL,
    interval_number int  NULL,
    model varchar(64)  NULL,
    CONSTRAINT ir_cron_pk PRIMARY KEY (id)
);

insert into ir_cron(name,function,interval_type,args,numbercall,nextcall,priority,doall,active,interval_number,model,user_id)
values
('Cancel Appointment live','cancel_appointment_live','minutes','()','-1',now(),'5','False','True','1','cron_appointment',1),
('Update activity status user','update_activity_status_user','minutes','()','-1',now(),'5','False','True','15','cron_user_account',1);

-- Table: meta_apointment_states
CREATE TABLE meta_apointment_states (
    id serial  NOT NULL,
    name varchar(50)  NULL,
    code varchar(50)  NULL,
    active boolean  NULL DEFAULT True,
    CONSTRAINT meta_apointment_states_pk PRIMARY KEY (id)
);

insert into meta_apointment_states(code,name) values ('confirmed','Confirmed'), ('processing','Processing'), ('suspended','Suspended'), ('free_finished','Successful'), ('canceled_by_doctor','Canceled'), ('canceled_by_patient','Canceled'), ('confirmed_by_patient','Unconfirmed'), ('rescheduled_by_patient','Rescheduled'), ('successful_finished','Successful');
CREATE TRIGGER trigger_update_time_meta_apointment_states
AFTER INSERT OR UPDATE OR DELETE ON meta_apointment_states
FOR EACH ROW
EXECUTE PROCEDURE func_trigger_update_time_update_meta();

insert into meta_apointment_states (name,code) values ('Session Failed','session_failed')

-- Table: meta_apointment_types
CREATE TABLE meta_apointment_types (
    id serial  NOT NULL,
    code varchar(50)  NULL,
    name varchar(50)  NULL,
    active boolean  NULL DEFAULT True,
    CONSTRAINT meta_apointment_types_pk PRIMARY KEY (id)
);

insert into meta_apointment_types(code,name) values ('by_appointment','Appointment'), ('live','Live');
CREATE TRIGGER trigger_update_time_meta_apointment_types
AFTER INSERT OR UPDATE OR DELETE ON meta_apointment_types
FOR EACH ROW
EXECUTE PROCEDURE func_trigger_update_time_update_meta();


-- Table: meta_booking_status
CREATE TABLE meta_booking_status (
    id serial  NOT NULL,
    code varchar(50)  NULL,
    name varchar(50)  NULL,
    active boolean  NULL DEFAULT True,
    CONSTRAINT meta_booking_status_pk PRIMARY KEY (id)
);

insert into meta_booking_status(code,name) values ('open','Open'), ('close','Close');
CREATE TRIGGER trigger_update_time_meta_booking_status
AFTER INSERT OR UPDATE OR DELETE ON meta_booking_status
FOR EACH ROW
EXECUTE PROCEDURE func_trigger_update_time_update_meta();

-- Table: meta_appointment_finishing_options
CREATE TABLE meta_appointment_finishing_options (
    id serial  NOT NULL,
    state varchar(100)  NULL,
    option varchar(100)  NULL,
    explanation text  NULL,
    text text  NULL,
    active boolean  NULL DEFAULT True,
    CONSTRAINT meta_appointment_finishing_options_pk PRIMARY KEY (id)
);

insert into meta_appointment_finishing_options(state, option, explanation, text) values ('successful_finished','successful','','Session completed'), ('successful_finished','patient_did_not_answer','','Patient did not answer'), ('free_finished','free','Your SET fee for the appointment is refunded to the Patient. Patient is required to pay for Doctor Doctor services.','Free consultation'), ('free_finished','poor_connection','Enabled when session cannot be completed due to poor Internet connection. A FULL refund is issued for the appointment.','Poor connection'), ('suspended','','Appointment with Patient is left OPEN and not finalised. Patient is required to reschedule a new TIME and Date for appointment.','Suspend / Leave open');
insert into meta_appointment_finishing_options (state,option,text) values ('session_failed','session_failed', 'Session Failed ')

CREATE TRIGGER trigger_update_time_meta_appointment_finishing_options
AFTER INSERT OR UPDATE OR DELETE ON meta_appointment_finishing_options
FOR EACH ROW
EXECUTE PROCEDURE func_trigger_update_time_update_meta();
1	"sucessful_finished"	"successful"		"Successful"
2	"sucessful_finished"	"patient_did_not_answer"		"Patient did not answer"
3	"free_finished"	"free"	"Your SET fee for the appointment is refunded to the Patient. Patient is required to pay for Doctor Doctor services."	"Mark as Free"
4	"free_finished"	"poor_connection"	"Enabled when session cannot be completed due to poor Internet connection. A FULL refund is issued for the appointment."	"Poor connection"
5	"suspended"		"Appointment with Patient is left OPEN and not finalised. Patient is required to reschedule a new TIME and Date for appointment."	"Suspend"
6	"session_failed"	"session_failed"		"Session Failed"
-- Table: meta_bank
CREATE TABLE meta_bank (
    id serial  NOT NULL,
    name varchar(100)  NULL,
    bank_group_code varchar(100)  NULL,
    active boolean  NULL DEFAULT True,
    CONSTRAINT meta_bank_pk PRIMARY KEY (id)
);

insert into meta_bank(name, bank_group_code) values ('NH Cong thuong VN (Vietinbank)','nnnc'), ('NH Dau tu va Phat trien VN (BIDV)','nnnc'), ('NH Nong nghiep & PT Nong thon VN (Agribank)','nnnc'), ('NH Phat trien Viet Nam (VDB)','nnnc'), ('NHTMCP Viet A','nncp'), ('NH BAO VIET (Bao Viet Bank)','nncp'), ('NH Tien Phong (Tien Phong Bank)','nncp'), ('NH TMCP Ban Viet','nncp'), ('NH TM TNHH MTV Xay Dung VN(CB Bank)','nncp'), ('NH TMCP Buu Dien Lien Viet','nncp'), ('NH TMCP Dai Chung (PVcombank)','nncp'), ('NH TMCP Viet Nam Thuong Tin (Vietbank)','nncp'), ('NHTMCP A Chau (ACB)','nncp'), ('NHTMCP An Binh (ABBank)','nncp'), ('NHTMCP Bac A (Bac A bank)','nncp'), ('NHTMCP Dai Duong (Oceanbank)','nncp'), ('NHTMCP Dau khi Toan cau (GPBank)','nncp'), ('NHTMCP Dong A (Dong A bank)','nncp'), ('NHTMCP Dong Nam A (Seabank)','nncp'), ('NHTMCP Hang Hai (Maritime Bank)','nncp'), ('NHTMCP Kien Long (Kien Long bank)','nncp'), ('NHTMCP Ky thuong VN (Techcombank)','nncp'), ('NHTMCP Nam A (Nam A Bank)','nncp'), ('NHTMCP Ngoại Thương Việt Nam (Vietcombank)','nncp'), ('NHTMCP phat trien Tp HCM (HD Bank)','nncp'), ('NHTMCP Phuong Dong (OCB)','nncp'), ('NHTMCP Quan Doi (MB)','nncp'), ('NHTMCP Quoc Te (VIB)','nncp'), ('NHTMCP Sai Gon (SCB)','nncp'), ('NHTMCP Sai gon - Ha Noi (SHB)','nncp'), ('NHTMCP Sai gon Thuong Tin (Sacombank)','nncp'), ('NHTMCP SG Cong Thuong (SaigonBank)','nncp'), ('NHTMCP VN Thinh Vuong(VP Bank)','nncp'), ('NHTMCP Xang dau Petrolimex (PGBank)','nncp'), ('NHTMCP Xuat Nhap Khau (Eximbank)','nncp'), ('NH lien doanh Viet Nga','nnld'), ('NH lien doanh Viet Thai','nnld'), ('PUBLIC BANK VN','nnld'), ('BANGKOK BANK','cnnn'), ('Bank of Communications','cnnn'), ('Bank of India','cnnn'), ('BANK OF TOKYO-MITSUBISHI UFJ,LTD','cnnn'), ('BANK OF TOKYO-MITSUBISHI UFJ,LTD CN HCM','cnnn'), ('BNP Paribas Chi nhanh HN','cnnn'), ('BNP PARIBAS Chi nhanh HCM','cnnn'), ('BPCE IOM','cnnn'), ('China Construction Bank Corporation','cnnn'), ('CIMB','cnnn'), ('CITI BANK','cnnn'), ('CITI BANK HN','cnnn'), ('Credit Agricole CIB','cnnn'), ('DEUTSCHE BANK','cnnn'), ('FIRST COMMERCIAL BANK HCM','cnnn'), ('HONGKONG AND SHANGHAI BANK (HSBC)','cnnn'), ('HUANAN COMMERECIAL BANK LTD chi nhanh SG','cnnn'), ('Industrial Bank of Korea','cnnn'), ('JP MORGAN CHASE BANK','cnnn'), ('KOREA EXCHANGE BANK','cnnn'), ('MALAYAN BANKING BERHAD HCM','cnnn'), ('MAY BANK (HN)','cnnn'), ('MEGA INTERNATIONAL COMMERCIAL Co.LTD','cnnn'), ('Mizuho Corporate Bank Ltd., HN','cnnn'), ('MIZUHO CORPORATE BANK, LTD','cnnn'), ('Ngan hang Busan','cnnn'), ('NH Hana','cnnn'), ('NH WOORI - Chi nhanh Tp HCM','cnnn'), ('NH CATHAY','cnnn'), ('NH Cong nghiep Han Quoc','cnnn'), ('NH DBS Bank Ltd CN HCM','cnnn'), ('NH DTPT Campuchia Ha Noi','cnnn'), ('NH DTPT Campuchia - HCM','cnnn'), ('NH FAR EAST NATIONAL BANK','cnnn'), ('NH FIRST COMMERCIAL BANK HANOI','cnnn'), ('NH Industrial & Commercial Bank of China','cnnn'), ('NH Kookmin TP Ho Chi Minh','cnnn'), ('NH NongHyup','cnnn'), ('NH Taipei FubonC.B','cnnn'), ('NH TM Taipei Fubon','cnnn'), ('NH TM TNHH E.Sun','cnnn'), ('NH TNHH CTBC ( NHTM Chinatrust)','cnnn'), ('NH TNHH MTV Shinhan VN','cnnn'), ('NHTM Taipei Fubon','cnnn'), ('NHTNHH MTV Hong Leong VN','cnnn'), ('OVERSEA - CHINESE BANKING CORP Ltd','cnnn'), ('Siam Commercial Bank Public Company Ltd','cnnn'), ('STANDARD CHARTERED BANK','cnnn'), ('SUMITOMO MITSUI BANKING CORPORATION','cnnn'), ('The Shanghai Com&Savings Bank','cnnn'), ('UNITED OVERSEAS BANK (UOB)','cnnn'), ('WOORI BANK HA NOI','cnnn');
CREATE TRIGGER trigger_update_time_meta_bank
AFTER INSERT OR UPDATE OR DELETE ON meta_bank
FOR EACH ROW
EXECUTE PROCEDURE func_trigger_update_time_update_meta();

-- Table: meta_bank_group
CREATE TABLE meta_bank_group (
    id serial  NOT NULL,
    code varchar(50)  NULL,
    name varchar(100)  NULL,
    active boolean  NULL DEFAULT True,
    CONSTRAINT meta_bank_group_pk PRIMARY KEY (id)
);
insert into meta_bank_group(code, name) values ('nnnc','Ngan hang thuong mai nha nuoc'),('nncp','Ngan hang thuong mai co phan'),('nnld','Ngan hang lien doanh'),('cnnn','Chi nhanh ngan hang nuoc ngoai o Viet Nam');
CREATE TRIGGER trigger_update_time_meta_bank_group
AFTER INSERT OR UPDATE OR DELETE ON meta_bank_group
FOR EACH ROW
EXECUTE PROCEDURE func_trigger_update_time_update_meta();

-- Table: meta_call_types
CREATE TABLE meta_call_types (
    id serial  NOT NULL,
    code varchar(50)  NULL,
    name varchar(50)  NULL,
    active boolean  NULL DEFAULT True,
    CONSTRAINT meta_call_types_pk PRIMARY KEY (id)
);

insert into meta_call_types(code,name) values ('incoming','Incoming'), ('outgoing','Outgoing'), ('missed','Missed call'), ('rejected','Rejected call');
CREATE TRIGGER trigger_update_time_meta_call_types
AFTER INSERT OR UPDATE OR DELETE ON meta_call_types
FOR EACH ROW
EXECUTE PROCEDURE func_trigger_update_time_update_meta();;

-- Table: meta_cities
CREATE TABLE meta_cities (
    id serial  NOT NULL,
    code varchar(100)  NULL,
    name varchar(250)  NULL,
    active boolean  NULL DEFAULT True,
    CONSTRAINT meta_cities_pk PRIMARY KEY (id)
);

insert into meta_cities(code,name) values ('hcm','Ho Chi Minh'), ('hn','Ha Noi'), ('dn','Da Nang');
CREATE TRIGGER trigger_update_time_meta_cities
AFTER INSERT OR UPDATE OR DELETE ON meta_cities
FOR EACH ROW
EXECUTE PROCEDURE func_trigger_update_time_update_meta();

-- Table: meta_countries
CREATE TABLE meta_countries (
    id serial  NOT NULL,
    code varchar(100)  NULL,
    name varchar(250)  NULL,
    active boolean  NULL DEFAULT True,
    CONSTRAINT meta_countries_pk PRIMARY KEY (id)
);

insert into meta_countries(code,name) values ('vn','Viet Nam'), ('en','England');
CREATE TRIGGER trigger_update_time_meta_countries
AFTER INSERT OR UPDATE OR DELETE ON meta_countries
FOR EACH ROW
EXECUTE PROCEDURE func_trigger_update_time_update_meta();

-- Table: meta_country_phone_code
CREATE TABLE meta_country_phone_code (
    id serial  NOT NULL,
    code varchar(50)  NULL,
    name varchar(100)  NULL,
    active boolean  NULL DEFAULT True,
    CONSTRAINT meta_country_phone_code_pk PRIMARY KEY (id)
);

insert into meta_country_phone_code(code,name) values ('+84','Viet Nam');
CREATE TRIGGER trigger_update_time_meta_country_phone_code
AFTER INSERT OR UPDATE OR DELETE ON meta_country_phone_code
FOR EACH ROW
EXECUTE PROCEDURE func_trigger_update_time_update_meta();

-- Table: meta_currency_units
CREATE TABLE meta_currency_units (
    id serial  NOT NULL,
    code varchar(50)  NULL,
    name varchar(50)  NULL,
    short_name varchar(50)  NULL,
    active boolean  NULL DEFAULT True,
    CONSTRAINT meta_currency_units_pk PRIMARY KEY (id)
);

insert into meta_currency_units(code, name, short_name) values ('usd','USD','$'), ('vnd','VND','đ');
CREATE TRIGGER trigger_update_time_meta_currency_units
AFTER INSERT OR UPDATE OR DELETE ON meta_currency_units
FOR EACH ROW
EXECUTE PROCEDURE func_trigger_update_time_update_meta();

-- Table: meta_dr_consultation_durations
CREATE TABLE meta_dr_consultation_durations (
    id serial  NOT NULL,
    code varchar(50)  NULL,
    value varchar(50)  NULL,
    unit varchar(50)  NULL,
    active boolean  NULL DEFAULT True,
    CONSTRAINT meta_dr_consultation_durations_pk PRIMARY KEY (id)
);

insert into meta_dr_consultation_durations(code,value, unit) values ('10m','10','minute'), ('15m','15','minute'), ('30m','30','minute'), ('60m','60','minute'), ('90m','90','minute'), ('20m','20','minute');
CREATE TRIGGER trigger_update_time_meta_dr_consultation_durations
AFTER INSERT OR UPDATE OR DELETE ON meta_dr_consultation_durations
FOR EACH ROW
EXECUTE PROCEDURE func_trigger_update_time_update_meta();

-- Table: meta_dr_consultation_types
CREATE TABLE meta_dr_consultation_types (
    id serial  NOT NULL,
    code varchar(50)  NULL,
    name varchar(100)  NULL,
    active boolean  NULL DEFAULT True,
    CONSTRAINT meta_dr_consultation_types_pk PRIMARY KEY (id)
);

insert into meta_dr_consultation_types(code,name) values ('live','Live'), ('by_appointment','Appointment');
CREATE TRIGGER trigger_update_time_meta_dr_consultation_types
AFTER INSERT OR UPDATE OR DELETE ON meta_dr_consultation_types
FOR EACH ROW
EXECUTE PROCEDURE func_trigger_update_time_update_meta();;

-- Table: meta_dr_languages
CREATE TABLE meta_dr_languages (
    id serial  NOT NULL,
    code varchar(50)  NULL,
    name varchar(100)  NULL,
    active boolean  NULL DEFAULT True,
    CONSTRAINT meta_dr_languages_pk PRIMARY KEY (id)
);

insert into meta_dr_languages(code,name) values ('en','English'), ('vi','Vietnamese');
CREATE TRIGGER trigger_update_time_meta_dr_languages
AFTER INSERT OR UPDATE OR DELETE ON meta_dr_languages
FOR EACH ROW
EXECUTE PROCEDURE func_trigger_update_time_update_meta();;

-- Table: meta_dr_specialties
CREATE TABLE meta_dr_specialties (
    id serial  NOT NULL,
    code varchar(50)  NULL,
    name varchar(100)  NULL,
    active boolean  NULL DEFAULT True,
    CONSTRAINT meta_dr_specialties_pk PRIMARY KEY (id)
);

insert into meta_dr_specialties(code,name) values ('mental_health','Mental Health'), ('general_practitioner','General Practitioner'), ('medical_specialist','Medical Specialist'), ('paediatrics','Paediatrics');
CREATE TRIGGER trigger_update_time_meta_dr_specialties
AFTER INSERT OR UPDATE OR DELETE ON meta_dr_specialties
FOR EACH ROW
EXECUTE PROCEDURE func_trigger_update_time_update_meta();

-- Table: meta_education_levels
CREATE TABLE meta_education_levels (
    id serial  NOT NULL,
    code varchar(50)  NULL,
    name varchar(100)  NULL,
    active boolean  NULL DEFAULT True,
    CONSTRAINT meta_education_levels_pk PRIMARY KEY (id)
);

insert into meta_education_levels(code,name) values ('undergraduate','Undergraduate'), ('graduate','Graduate'), ('other','Other');
CREATE TRIGGER trigger_update_time_meta_education_levels
AFTER INSERT OR UPDATE OR DELETE ON meta_education_levels
FOR EACH ROW
EXECUTE PROCEDURE func_trigger_update_time_update_meta();

-- Table: meta_genders
CREATE TABLE meta_genders (
    id serial  NOT NULL,
    code varchar(50)  NULL,
    name varchar(100)  NULL,
    active boolean  NULL DEFAULT True,
    CONSTRAINT meta_genders_pk PRIMARY KEY (id)
);

insert into meta_genders(code,name) values ('male','Male'), ('female','Female'), ('other','Other');
CREATE TRIGGER trigger_update_time_meta_genders
AFTER INSERT OR UPDATE OR DELETE ON meta_genders
FOR EACH ROW
EXECUTE PROCEDURE func_trigger_update_time_update_meta();

-- Table: meta_languages
CREATE TABLE meta_languages (
    id serial  NOT NULL,
    code varchar(50)  NULL,
    name varchar(100)  NULL,
    active boolean  NULL DEFAULT True,
    CONSTRAINT meta_languages_pk PRIMARY KEY (id)
);

insert into meta_languages(code,name) values ('en','English'), ('vi','Tiếng Việt');
CREATE TRIGGER trigger_update_time_meta_languages
AFTER INSERT OR UPDATE OR DELETE ON meta_languages
FOR EACH ROW
EXECUTE PROCEDURE func_trigger_update_time_update_meta();

-- Table: meta_medicalrecord_fields
CREATE TABLE meta_medicalrecord_fields (
    id serial  NOT NULL,
    code varchar(100)  NULL,
    name varchar(250)  NULL,
    active boolean  NULL DEFAULT True,
    CONSTRAINT meta_medicalrecord_fields_pk PRIMARY KEY (id)
);

insert into meta_medicalrecord_fields(code,name) values ('allergies','Allergies'), ('autoimmune','Autoimune'), ('cancer','Cancer'), ('cardiovascular','Cardiovascular'), ('endocrine_and_metabolic','Endocrine and Metabolic'), ('family_history','Family history'), ('gastrointestial','Gastrointetial'), ('genital_and_urinarysystem','Genital and Urinary System '), ('gynecologic_women','Gynecologic (Women Only)'), ('hospitalisation_and_surgeries','Hospitalisation and Surgeries'), ('injuries','Injuries'), ('medications','Medications'), ('musculoskeletal','Musculoskeletal'), ('neurological','Neurological'), ('nutritional_supplements','Nutritional Supplements'), ('obstetric','Obstetric'), ('respiratory','Respiratory'), ('skin','Skin');
CREATE TRIGGER trigger_update_time_meta_medicalrecord_fields
AFTER INSERT OR UPDATE OR DELETE ON meta_medicalrecord_fields
FOR EACH ROW
EXECUTE PROCEDURE func_trigger_update_time_update_meta();

-- Table: meta_mobile_operators
CREATE TABLE meta_mobile_operators (
    id serial  NOT NULL,
    code varchar(50)  NULL,
    name varchar(100)  NULL,
    active boolean  NULL DEFAULT True,
    CONSTRAINT meta_mobile_operators_pk PRIMARY KEY (id)
);

insert into meta_mobile_operators(code,name) values ('viettel','Viettel'), ('mobifone','Mobifone'), ('vinaphone','Vinaphone'), ('vnmobile','Vietnammobile');
CREATE TRIGGER trigger_update_time_meta_mobile_operators
AFTER INSERT OR UPDATE OR DELETE ON meta_mobile_operators
FOR EACH ROW
EXECUTE PROCEDURE func_trigger_update_time_update_meta();

-- Table: meta_payment_options
CREATE TABLE meta_payment_options (
    id serial  NOT NULL,
    code varchar(50)  NULL,
    name varchar(100)  NULL,
    active boolean  NULL DEFAULT True,
    CONSTRAINT meta_payment_options_pk PRIMARY KEY (id)
);

insert into meta_payment_options(code,name) values ('credit_card','Credit Card'), ('mobile_card','Mobile Card'), ('dd_card','D+D Card');
CREATE TRIGGER trigger_update_time_meta_payment_options
AFTER INSERT OR UPDATE OR DELETE ON meta_payment_options
FOR EACH ROW
EXECUTE PROCEDURE func_trigger_update_time_update_meta();

-- Table: meta_payout_states
CREATE TABLE meta_payout_states (
    id serial  NOT NULL,
    code varchar(50)  NULL,
    name varchar(100)  NULL,
    active boolean  NULL DEFAULT True,
    CONSTRAINT meta_payout_states_pk PRIMARY KEY (id)
);

insert into meta_payout_states(code,name) values ('pending','Pending'), ('paid','Paid');
CREATE TRIGGER trigger_update_time_meta_payout_states
AFTER INSERT OR UPDATE OR DELETE ON meta_payout_states
FOR EACH ROW
EXECUTE PROCEDURE func_trigger_update_time_update_meta();

-- Table: meta_time_units
CREATE TABLE meta_time_units (
    id serial  NOT NULL,
    code varchar(50)  NULL,
    name varchar(50)  NULL,
    active boolean  NULL DEFAULT True,
    CONSTRAINT meta_time_units_pk PRIMARY KEY (id)
);

insert into meta_time_units(code,name) values ('hour','Hour'), ('minute','Minute'), ('second','Second');
CREATE TRIGGER trigger_update_time_meta_time_units
AFTER INSERT OR UPDATE OR DELETE ON meta_time_units
FOR EACH ROW
EXECUTE PROCEDURE func_trigger_update_time_update_meta();

-- Table: meta_titles
CREATE TABLE meta_titles (
    id serial  NOT NULL,
    code varchar(50)  NULL,
    name varchar(100)  NULL,
    active boolean  NULL DEFAULT True,
    CONSTRAINT meta_titles_pk PRIMARY KEY (id)
);

insert into meta_titles(code,name) values ('dr','Dr.'), ('ms','Ms.'), ('mr','Mr.');
CREATE TRIGGER trigger_update_time_meta_titles
AFTER INSERT OR UPDATE OR DELETE ON meta_titles
FOR EACH ROW
EXECUTE PROCEDURE func_trigger_update_time_update_meta();

-- Table: meta_transaction_categories
CREATE TABLE meta_transaction_categories (
    id serial  NOT NULL,
    code varchar(50)  NULL,
    name varchar(100)  NULL,
    active boolean  NULL DEFAULT True,
    CONSTRAINT meta_transaction_categories_pk PRIMARY KEY (id)
);

insert into meta_transaction_categories(code,name) values ('appointment','Appointment'), ('deposit','Add credit'), ('refund','Refund');
CREATE TRIGGER trigger_update_time_meta_transaction_categories
AFTER INSERT OR UPDATE OR DELETE ON meta_transaction_categories
FOR EACH ROW
EXECUTE PROCEDURE func_trigger_update_time_update_meta();
