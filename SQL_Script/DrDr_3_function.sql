-- [SQL DRDR]
-- FUNCTION: calculate_review_rating_update();
-- DROP FUNCTION calculate_review_rating_update();
CREATE OR REPLACE FUNCTION public.calculate_review_rating_update()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
BEGIN

IF (OLD.rating_point != NEW.rating_point) THEN
	update doctor_profile dp
		set sum_rating_point = dp.sum_rating_point - OLD.rating_point + NEW.rating_point
	from
	(
	select ua.user_mngment_id
	from appointment apm
	left join user_account ua on ua.id = apm.doctor_id
	where apm.id = NEW.appointment_id
	) x
	where dp.user_mngment_id = x.user_mngment_id;

END IF;

RETURN NEW;
END;
$function$;


-- TRIGGER appointment_review_update_trigger();
-- DROP TRIGGER appointment_review_update_trigger ON appointment_review CASCADE
CREATE TRIGGER appointment_review_update_trigger
    AFTER UPDATE OF rating_point
    ON appointment_review
    FOR EACH ROW
    EXECUTE PROCEDURE calculate_review_rating_update();


-- FUNCTION: calculate_review_rating_insert();
-- DROP FUNCTION calculate_review_rating_insert();
CREATE OR REPLACE FUNCTION public.calculate_review_rating_insert()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
BEGIN

update doctor_profile dp
	set sum_rating_point = COALESCE(dp.sum_rating_point,0) + NEW.rating_point,
		count_rating_point = COALESCE(dp.count_rating_point,0) + 1
from
(
select ua.user_mngment_id
from appointment apm
left join user_account ua on ua.id = apm.doctor_id
where apm.id = NEW.appointment_id
) x
where dp.user_mngment_id = x.user_mngment_id;


RETURN NEW;
END;
$function$;


-- TRIGGER appointment_review_insert_trigger();
-- DROP TRIGGER appointment_review_insert_trigger ON appointment_review CASCADE
CREATE TRIGGER appointment_review_insert_trigger
    AFTER INSERT ON public.appointment_review
    FOR EACH ROW
    EXECUTE PROCEDURE calculate_review_rating_insert();

-- FUNCTION: insert_business_transfer_transaction();
-- DROP FUNCTION insert_business_transfer_transaction();
CREATE OR REPLACE FUNCTION public.insert_business_transfer_transaction()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
BEGIN

update business_transfer_transaction
set balance_source_before = w.balance_source_before,
	balance_source_after = w.balance_source_before - NEW.amount,
	balance_dest_before = w.balance_dest_before,
	balance_dest_after = w.balance_dest_before + NEW.amount
from
(select
	(select available_balance from user_wallet where id = NEW.wallet_source_id) balance_source_before,
	(select available_balance from user_wallet where id = NEW.wallet_dest_id) balance_dest_before
) w
where id = NEW.id;
IF NEW.state = 'confirmed' THEN
update user_wallet w
set available_balance = COALESCE(w.available_balance,0) - NEW.amount,
	hold_balance = COALESCE(w.hold_balance,0) + NEW.amount,
	write_date = now()
where id = NEW.wallet_source_id;
END IF;

IF NEW.state = 'paid' THEN
update user_wallet w
set total_balance = COALESCE(w.total_balance,0) + NEW.amount,
	available_balance = COALESCE(w.available_balance,0) + NEW.amount,
	write_date = now()
where id = NEW.wallet_dest_id;
update user_wallet w
set total_balance = COALESCE(w.total_balance,0) - NEW.amount,
	available_balance = COALESCE(w.available_balance,0) - NEW.amount,
	write_date = now()
where id = NEW.wallet_source_id;
END IF;

RETURN NEW;
END;
$function$;


-- TRIGGER business_transfer_transaction_insert_trigger();
-- DROP TRIGGER business_transfer_transaction_insert_trigger ON business_transfer_transaction CASCADE
CREATE TRIGGER business_transfer_transaction_insert_trigger
    AFTER INSERT
    ON business_transfer_transaction
    FOR EACH ROW
    EXECUTE PROCEDURE insert_business_transfer_transaction();


-- FUNCTION: update_business_transfer_transaction();
-- DROP FUNCTION update_business_transfer_transaction();
CREATE OR REPLACE FUNCTION public.update_business_transfer_transaction()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
BEGIN

IF OLD.state = 'confirmed' AND NEW.state = 'paid' THEN
	update user_wallet
	set available_balance = COALESCE(available_balance,0) + NEW.amount,
		total_balance = COALESCE(total_balance,0) + NEW.amount,
		write_date = now()
	where id = OLD.wallet_dest_id;

	update user_wallet
	set total_balance = COALESCE(total_balance,0) - NEW.amount,
		hold_balance = COALESCE(hold_balance,0) - NEW.amount,
		write_date = now()
	where id = OLD.wallet_source_id;
END IF;

IF OLD.state = 'confirmed' AND NEW.state = 'cancel' THEN
	update user_wallet
	set available_balance = COALESCE(available_balance,0) + NEW.amount,
		hold_balance = COALESCE(hold_balance,0) - NEW.amount,
		write_date = now()
	where id = OLD.wallet_source_id;
END IF;


RETURN NEW;
END;
$function$;


-- TRIGGER business_transfer_transaction_update_trigger();
-- DROP TRIGGER business_transfer_transaction_update_trigger ON business_transfer_transaction CASCADE
CREATE TRIGGER business_transfer_transaction_update_trigger
    AFTER UPDATE
    ON business_transfer_transaction
    FOR EACH ROW
    EXECUTE PROCEDURE update_business_transfer_transaction();