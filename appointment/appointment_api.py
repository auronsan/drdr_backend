#!/usr/bin/env python
# -*- coding: utf-8 -*-
import falcon
import json
import sys
import os
import ConfigParser
from appointment_business import appointment_business

project_path =  str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
config = ConfigParser.RawConfigParser(allow_no_value=True)
config.read(project_path + '/' + "environment.conf")
sys.path.insert(0, "..")
try:
    from general import general_business
    from aws_management import aws_management
except ImportError as e:
    print('No Import ', e)

AWS_BUCKET = config.get('environment', 's3_bucket')
BUCKET_URL_DEFAULT = AWS_BUCKET + '.' + config.get('environment', 's3_address')

status_code = {
    200: falcon.HTTP_200,
    401: falcon.HTTP_401,
    502: falcon.HTTP_502,
    203: falcon.HTTP_203
}


class appointment_api(object):
    def __init__(self, signal, erp_connection=False, var_contructor=False):
        self.business = appointment_business(signal, erp_connection, var_contructor)
        self.general_business = general_business(erp_connection, var_contructor)
        self.aws = aws_management()
        self.pool_conn = erp_connection
        self.signal = signal

    def on_get(self, req, resp, apm_id=None,hospital_id=None, reason = ''):

        try:
            data = {}
            header = req.headers  # key cua header luon viet hoa
            all_header = self.general_business.get_header_request(header)
            lang = all_header.get('lang')
            _ = self.general_business.get_lang_instance(lang)
            data.update(all_header)
            if self.signal == 'get_appointment_details':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Get appointment details failed"),
                    "msg": _("An error occurred. Please try to request again."),
                    "code": "appointment_details_failed"
                }
                try:
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'apm_id': apm_id,
                        'lang': lang
                    })

                    record = self.business.get_appointment_details(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']
                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            elif self.signal == 'get_appointment_details_hospital':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Get appointment details failed"),
                    "msg": _("An error occurred. Please try to request again."),
                    "code": "appointment_details_failed"
                }
                try:
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'apm_id': apm_id,
                        'lang': lang
                    })

                    record = self.business.get_appointment_details_hospital(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']
                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'appointment_note_get':
                resp_status = status_code[401]
                resp_body = {
                    "code": "appointment_note_get_failed",
                    "title": _("appointment note get failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'apm_id': apm_id,
                        'lang': lang
                    })
                    record = self.business.appointment_note_get(data)
                    resp_status = status_code[record['status_code']]
                    resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # MP6.6 API for patient to check appointment cancelation conditions
            elif self.signal == 'get_check_appointment_cancelation':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Get check appointment cancelation failed"),
                    "msg": _("An error occurred. Please try to request again."),
                    "code": "check_appointment_cancelation"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'apm_id': apm_id,
                        'lang': lang,
                    })
                    record = self.business.get_check_appointment_cancelation(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']
                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # MP 6.8
            elif self.signal == 'get_check_appointment_reschedule':
                resp_status = status_code[401]
                resp_body = {
                    "code": "appointment_reschedule_check_failed",
                    "title": _("Appointment reschedule check failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = json.loads(req.stream.read(req.content_length or 0) or '{}')
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'apm_id': apm_id,
                        'lang': lang,
                    })
                    data.update(raw_data)

                    record = self.business.get_check_appointment_reschedule(data)
                    resp_status = status_code[record['status_code']]
                    resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # WD6.20 API for doctor to get specific appointment details
            elif self.signal == 'doctor_get_appointment_details':
                resp_status = status_code[401]
                resp_body = {
                    "code": "doctor_get_appointment_details",
                    "title": _("Get_appointment_details failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = json.loads(req.stream.read(req.content_length or 0) or '{}')
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'apm_id': apm_id,
                        'lang': lang,
                    })
                    data.update(raw_data)

                    record = self.business.doctor_get_appointment_details(data)
                    resp_status = status_code[record['status_code']]
                    resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'doctor_get_appointment_details_hospital':
                resp_status = status_code[401]
                resp_body = {
                    "code": "doctor_get_appointment_details",
                    "title": _("Get_appointment_details failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = json.loads(req.stream.read(req.content_length or 0) or '{}')
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'apm_id': apm_id,
                        'lang': lang,
                    })
                    data.update(raw_data)

                    record = self.business.doctor_get_appointment_details_hospital(data)
                    resp_status = status_code[record['status_code']]
                    resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
        except Exception as e:
            print str(e)

    def on_post(self, req, resp, apm_id=None, doc_id=None,hospital_id=None, reason=None):
        try:
            data = {}
            header = req.headers  # key cua header luon viet hoa
            all_header = self.general_business.get_header_request(header)
            lang = all_header.get('lang')
            _ = self.general_business.get_lang_instance(lang)
            data.update(all_header)
            if self.signal == 'appointment_review_post':
                resp_status = status_code[401]
                resp_body = {
                    "code": "update_review_appointment_failed",
                    "title": _("Update review appointment failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = json.loads(req.stream.read(req.content_length or 0) or '{}')
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'apm_id': apm_id,
                        'lang': lang
                    })
                    data.update(raw_data)
                    record = self.business.appointment_review_update(data)
                    resp_status = status_code[record['status_code']]
                    resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'appointment_note_submit':
                resp_status = status_code[401]
                resp_body = {
                    "code": "appointment_note_submit_failed",
                    "title": _("Appointment note submit failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = json.loads(req.stream.read(req.content_length or 0) or '{}')
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'apm_id': apm_id,
                        'lang': lang
                    })
                    data.update(raw_data)
                    record = self.business.appointment_note_submit(data)
                    resp_status = status_code[record['status_code']]
                    resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # WD 6.21 API for doctor to submit document to specific appointment
            elif self.signal == 'appointment_document_submit':
                resp_status = status_code[401]
                resp_body = {
                    "code": "appointment_document_submit_failed",
                    "title": _("appointment document submit failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    document = req.get_param('document', default=False)
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'apm_id': apm_id,
                        'file_info': {},
                        'lang': lang
                    })
                    if apm_id and type(document).__name__ in ('instance', 'bool'):
                        # Write image to local
                        if type(document).__name__ == 'instance':
                            def get_infomation_file(file):
                                f = file.file
                                file_size = os.fstat(f.fileno()).st_size
                                file_name, file_extension = os.path.splitext(file.filename)
                                return {
                                    'file_size': file_size,
                                    'file_name': file_name,
                                    'file_extension': file_extension.replace('.', '').lower()
                                }

                            user_objs = self.general_business.validate_user_token(header.get("X-USER-TOKEN", False), data.get('client_id'))
                            if user_objs:
                                file_info = get_infomation_file(document)
                                data['file_info'] = file_info
                                data['user_id'] = user_objs[1]
                                if user_objs and user_objs[7]:
                                    if file_info['file_extension'] in (
                                    'jpg', 'png', 'gif', 'jpeg', 'pdf', 'doc', 'docx'):
                                        filename = '%s_%s.%s' % (
                                        apm_id, file_info['file_name'], file_info['file_extension'])
                                        self.aws.upload_obj(document.file, AWS_BUCKET, filename)
                                        data['file_info'].update(
                                            {'url': 'https://' + BUCKET_URL_DEFAULT + '/' + filename,
                                             'file_name': filename})
                                    else:
                                        data['file_info'].update({'url': 'wrong_file_type'})
                                        # End Write image to local

                                record = self.business.appointment_document_submit(data)
                                if record:
                                    resp_status = status_code[record['status_code']]
                                    resp_body = record['resq_body']
                            else:
                                resp_status = status_code[203]
                                resp_body = {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }


                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # WD6.8 API for doctor to get all the appoinment list in a date range
            elif self.signal == 'doctor_get_all_the_oppointment_list_in_date_range':
                resp_status = status_code[401]
                resp_body = {
                    "code": "get_all_the_oppointment_list_in_date_range_failed",
                    "title": _("Get all the appoinment list in a date range"),
                    "msg": _("An error occurred. Please try to request again.")
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = json.loads(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang
                    })
                    data.update(raw_data)

                    record = self.business.doctor_get_all_the_oppointment_list_in_date_range(data)
                    resp_status = status_code[record['status_code']]
                    resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # MP6.7 API for patient to cancel the specificic appointment
            elif self.signal == 'patient_cancel_the_specificic_appointment':
                resp_status = status_code[401]
                resp_body = {
                    "code": "patient_cancel_the_specificic_appointment_failed",
                    "title": _("Patient cancel the specificic appointment failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'apm_id': apm_id,
                        'lang': lang
                    })

                    record = self.business.patient_cancel_the_specificic_appointment(data)
                    resp_status = status_code[record['status_code']]
                    resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
                # MP 6.9
            elif self.signal == 'patient_reschedule_the_specificic_appointment':
                resp_status = status_code[401]
                resp_body = {
                    "code": "patient_reschedule_the_specificic_appointment_failed",
                    "title": _("Patient reschedule the specificic appointment failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = json.loads(req.stream.read(req.content_length or 0) or '{}')
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'apm_id': apm_id,
                        'lang': lang
                    })
                    data.update(raw_data)

                    record = self.business.patient_reschedule_the_specificic_appointment(data)
                    resp_status = status_code[record['status_code']]
                    resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # WD6.14 API for doctor to cancel the specificic appointment
            elif self.signal == 'doctor_cancel_the_specificic_appointment':
                resp_status = status_code[401]
                resp_body = {
                    "code": "doctor_cancel_the_specificic_appointment_failed",
                    "title": _("Doctor cancel the specificic appointment failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = json.loads(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'apm_id': apm_id,
                        'lang': lang
                    })
                    data.update(raw_data)

                    record = self.business.doctor_cancel_the_specificic_appointment(data)
                    resp_status = status_code[record['status_code']]
                    resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # WD6.15 API for doctor to suspend the specificic appointment 
            elif self.signal == 'doctor_suspend_the_specificic_appointment':
                resp_status = status_code[401]
                resp_body = {
                    "code": "doctor_suspend_the_specificic_appointment_failed",
                    "title": _("Doctor suspend the specificic appointment failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = json.loads(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'apm_id': apm_id,
                        'lang': lang
                    })
                    data.update(raw_data)

                    record = self.business.doctor_suspend_the_specificic_appointment(data)
                    resp_status = status_code[record['status_code']]
                    resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # WD6.16 API for doctor to complete the specificic appointment 
            elif self.signal == 'doctor_complete_the_specificic_appointment':
                resp_status = status_code[401]
                resp_body = {
                    "code": "doctor_complete_the_specificic_appointment_failed",
                    "title": _("Doctor complete the specificic appointment failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = json.loads(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'apm_id': apm_id,
                        'lang': lang
                    })
                    data.update(raw_data)
                    #record = self.business.doctor_complete_the_specificic_appointment(data)
                    record = self.business.doctor_complete_the_specificic_appointment_hospital(data)
                    resp_status = status_code[record['status_code']]
                    resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # WD6.17 API for doctor to mark the specific appointment as free
            elif self.signal == 'doctor_mark_the_specific_appointment_as_free':
                resp_status = status_code[401]
                resp_body = {
                    "code": "doctor_mark_the_specific_appointment_as_free_failed",
                    "title": _("Doctor mark the specific appointment as free failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = json.loads(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'apm_id': apm_id,
                        'lang': lang
                    })
                    data.update(raw_data)
                    record = self.business.doctor_mark_the_specific_appointment_as_free(data)
                    resp_status = status_code[record['status_code']]
                    resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            
            elif self.signal == 'doctor_mark_the_specific_appointment_as_failed':
                resp_status = status_code[401]
                resp_body = {
                    "code": "doctor_mark_the_specific_appointment_as_session_failed_failed",
                    "title": _("Doctor mark the specific appointment as failed session failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = json.loads(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'apm_id': apm_id,
                        'lang': lang
                    })
                    data.update(raw_data)
                    record = self.business.doctor_mark_the_specific_appointment_as_failed(data)
                    resp_status = status_code[record['status_code']]
                    resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # WD6.22 API for doctor to delete specific document of corresponding appointment
            # MP6.11 API for patient to delete specific document of corresponding appointment
            elif self.signal == 'delete_the_specific_appointment_document':
                resp_status = status_code[401]
                resp_body = {
                    "code": "delete_the_specific_appointment_document_failed",
                    "title": _("Delete the specific appointment document failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'apm_id': apm_id,
                        'doc_id': doc_id,
                        'lang': lang
                    })
                    record = self.business.delete_the_specific_appointment_document(data)
                    resp_status = status_code[record['status_code']]
                    resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # WD6.23 API for doctor to confirm a specific appointment
            elif self.signal == 'doctor_confirm_the_specific_appointment':
                resp_status = status_code[401]
                resp_body = {
                    "code": "doctor_confirm_the_specific_appointment_failed",
                    "title": _("Doctor confirm the specific appointment failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'apm_id': apm_id,
                        'lang': lang
                    })
                    #record = self.business.doctor_confirm_the_specific_appointment(data)
                    record = self.business.doctor_confirm_the_specific_appointment_hospital(data)
                    resp_status = status_code[record['status_code']]
                    resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # MP6.10 API for patient to submit document to specific appointment
            elif self.signal == 'patient_submit_document_to_the_specificic_appointment':
                resp_status = status_code[401]
                resp_body = {
                    "code": "patient_submit_document_to_the_specificic_appointment_failed",
                    "title": _("Patient submit document to the specificic appointment failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    document = req.get_param('document', default=False)
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'apm_id': apm_id,
                        'file_info': {},
                        'lang': lang
                    })
                    if apm_id and type(document).__name__ in ('instance', 'bool'):
                        # Write image to local
                        if type(document).__name__ == 'instance':
                            def get_infomation_file(file):
                                f = file.file
                                file_size = os.fstat(f.fileno()).st_size
                                file_name, file_extension = os.path.splitext(file.filename)
                                return {
                                    'file_size': file_size,
                                    'file_name': file_name,
                                    'file_extension': file_extension.replace('.', '').lower()
                                }

                            user_objs = self.general_business.validate_user_token(header.get("X-USER-TOKEN", False),data.get('client_id'))
                            if user_objs:
                                file_info = get_infomation_file(document)
                                if file_info['file_size'] > 1024*1024*10:
                                    resp_status = status_code[401]
                                    resp_body = {
                                        "code": "invalid_file",
                                        "title": _("Upload failed"),
                                        "msg": _("Maximum file size: 10MB. Supported types: jpg, jpeg, png, gif, pdf.")

                                    }
                                elif file_info['file_extension'] not in (
                                        'jpg', 'png', 'gif', 'jpeg', 'pdf'):
                                    resp_status = status_code[401]
                                    resp_body = {
                                        "code": "invalid_file",
                                        "title": _("Upload failed"),
                                        "msg": _("Maximum file size: 10MB. Supported types: jpg, jpeg, png, gif, pdf.")
                                    }
                                else:
                                    data['file_info'] = file_info
                                    data['user_id'] = user_objs[1]
                                    if user_objs and user_objs[7]:
                                        if file_info['file_extension'] in (
                                        'jpg', 'png', 'gif', 'jpeg', 'pdf', 'doc', 'docx'):
                                            filename = '%s_%s.%s' % (
                                            apm_id, file_info['file_name'], file_info['file_extension'])
                                            self.aws.upload_obj(document.file, AWS_BUCKET, filename)
                                            data['file_info'].update(
                                                {'url': 'https://' + BUCKET_URL_DEFAULT + '/' + filename,
                                                'file_name': filename})
                                        else:
                                            data['file_info'].update({'url': 'wrong_file_type'})
                                            # End Write image to local

                                    record = self.business.patient_submit_document_to_the_specificic_appointment(data)
                                    if record:
                                        resp_status = status_code[record['status_code']]
                                        resp_body = record['resq_body']
                            else:
                                resp_status = status_code[203]
                                resp_body = {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                    pass
                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

        except Exception as e:
            print str(e)
