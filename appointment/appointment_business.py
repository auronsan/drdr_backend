#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
import time
import logging
from raven import Client
import ConfigParser
import sys
import os

project_path =  str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
config = ConfigParser.RawConfigParser(allow_no_value=True)
config.read(project_path + '/' + "environment.conf")
sentry_key = config.get('environment', 'sentry_key')
client = Client(sentry_key)
logger = logging.getLogger('drdr_api')
sys.path.insert(0, "..")
try:
    from general import general_business
    from payment.model.business_transfer_transaction import business_transfer_transaction
except ImportError as e:
    print('No Import ', e)


class appointment_business(object):
    def __init__(self, signal, erp_connection, var_contructor):
        self.pool_conn = erp_connection
        self.general_business = general_business(erp_connection, var_contructor)
        self.signal = signal
        self.business_transfer = business_transfer_transaction(erp_connection, var_contructor)
        self.drdr_wallet_id = int(config.get('environment', 'drdr_wallet_id')) or 1
        self.com_ratio = float(config.get('environment', 'com_ratio')) or 40.0
        self.min_commission = float(config.get('environment', 'min_commission')) or 40000.0
        self.vi_ = self.general_business.vi_
        self.en_ = self.general_business.en_

    # MP6.1 API for getting details of specific appointment
    def get_appointment_details(self, data={}):
        logger.info('get_appointment_details %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get appointment details failed"),
                        "msg": _("An error occurred. Please try again."),
                        "code": "appointment_details_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    sql = '''select ua.id, --0 
                                    umng.title,--1
                                    umng.first_name,--2
                                    umng.middle_name,--3
                                    umng.last_name,--4
                                    umng.profile_image_url,--5
                                    apm.consultation_note, --6
                                    apm.state, --7
                                    apm.schedule_date, --8
                                    apm.type, --9                              
                                    apm.consultation_duration, --10
                                    COALESCE(apm.cost,0), --11
                                    COALESCE(apm.unit_cost,'vnd'), --12
                                    mta.value, --13
                                    mta.unit, --14
                                    umng.id, --15
                                    att.file_extension, --16
                                    att.file_size, --17
                                    att.write_date, --18
                                    att.url, -- 19
                                    att.file_name, --20
                                    att.create_uid, -- 21
                                    att.id, -- 22
                                    apm.write_date, --23                              
                                    case when ua.latest_activity_time > current_timestamp - interval '15 minutes' then True
                                            else False
                                    end onlineStatus, --24
                                    ua.activity_status -- 25
                                from appointment apm
                                    left join user_account ua on apm.doctor_id = ua.id
                                    left join user_mngment umng on  umng.id = ua.user_mngment_id 
                                    left join doctor_profile dp on  umng.id = dp.user_mngment_id
                                    left join meta_dr_consultation_durations mta on mta.code = apm.consultation_duration
                                    left join attachment att on att.res_id = apm.id and att.res_model = 'appointment'
                                where apm.id = %s '''
                    #print sql%(data.get('apm_id'))
                    record = gs.sql_execute(pool, sql, para_values=[data.get('apm_id')])

                    user_account_id = None if record[0][0] in ('False', 'None', '') else record[0][0]
                    title_code = None if record[0][1] in ('False', 'None', '') else record[0][1]
                    first_name = None if record[0][2] in ('False', 'None', '') else record[0][2]
                    middle_name = None if record[0][3] in ('False', 'None', '') else record[0][3]
                    last_name = None if record[0][4] in ('False', 'None', '') else record[0][4]
                    profile_image_url = None if record[0][5] in ('False', 'None', '') else record[0][5]
                    note = None if record[0][6] in ('False', 'None', '') else record[0][6]
                    state_code = None if record[0][7] in ('False', 'None', '') else record[0][7]
                    schedule_date = int(time.mktime(record[0][8].timetuple())) or None
                    type_code = None if record[0][9] in (None, '') else record[0][9]
                    final_cost_value = None if record[0][11] in (None, '') else record[0][11]
                    final_cost_unit = None if record[0][12] in (None, '') else record[0][12]
                    duration = None if record[0][13] in (None, '') else record[0][13]
                    unit = None if record[0][14] in (None, '') else record[0][14]
                    profile_id = None if record[0][15] in (None, '') else record[0][15]
                    appointmentTimeDelta = record[0][8] + timedelta(minutes=15)
                    isAvailableToCall = True if record[0][9] == 'live' and record[0][24] == True and record[0][25] == 'online' and record[0][7] == 'suspended' else False

                    documents = []
                    for line in record:
                        if line[21]:
                            document = {}
                            document.update({
                                "id": line[22],
                                "url": line[19],
                                "fileName": line[20],
                                "fileExtension": line[16],
                                "fileSize": self.general_business.bytesToSize(line[17]),
                                "latestUpdateTime": int(time.mktime(line[18].timetuple()))
                            })
                            if line[0] == line[21]:
                                document.update({
                                    "uploadUser": {
                                        "id": res_token[7],
                                        "titleCode": title_code,
                                        "firstName": first_name,
                                        "middleName": middle_name,
                                        "lastName": last_name
                                    }
                                })
                            else:
                                sql = '''select ua.user_mngment_id,umg.title,umg.first_name,umg.middle_name,umg.last_name
                                        from user_account ua
                                        left join user_mngment umg on umg.id = ua.user_mngment_id
                                        where ua.id = %s ''' % (line[21])

                                uinfo = gs.sql_execute(pool, sql)[0]
                                document.update({
                                    "uploadUser": {
                                        "id": uinfo[0],
                                        "titleCode": uinfo[1],
                                        "firstName": uinfo[2],
                                        "middleName": uinfo[3],
                                        "lastName": uinfo[4]
                                    }
                                })
                            documents.append(document)
                    if state_code in ('successful_finished','free_finished','canceled_by_doctor'):
                        isAvailableToReview = True
                    elif state_code == 'canceled_by_patient' and record[0][23] > appointmentTimeDelta:
                        isAvailableToReview = True
                    else:
                        isAvailableToReview = False

                    resp.update({
                        'status_code': 200,
                        'resq_body': {
                            'doctor': {
                                'id': profile_id,
                                'user_account_id': user_account_id,
                                'titleCode': title_code,
                                'firstName': first_name,
                                'middleName': middle_name,
                                'lastName': last_name,
                                'avatar': profile_image_url,
                                'specialtyCodes': self.get_specialty_codes(profile_id)
                            },
                            'typeCode': type_code,
                            'stateCode': state_code,
                            'scheduleTime': schedule_date,
                            'duration': {
                                'value': duration,
                                'unitCode': unit
                            },
                            'note': note,
                            'finalCost': {
                                'value': final_cost_value,
                                'unitCode': final_cost_unit
                            },
                            'callList': self.get_appoinment_callList(data.get('apm_id')),
                            'review': self.get_appoinment_review(data.get('apm_id')),
                            "documentList": documents,
                            'isAvailableToReview': isAvailableToReview,
                            "isAvailableToCall": isAvailableToCall
                        }})
                    
                        
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_get_appointment_details %s' % str(e))
            client.captureException()
        return resp

    
    def get_appointment_details_hospital(self, data={}):
        logger.info('get_appointment_details %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get appointment details failed"),
                        "msg": _("An error occurred. Please try again."),
                        "code": "appointment_details_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    sql = '''select ua.id, --0 
                                    umng.title,--1
                                    umng.first_name,--2
                                    umng.middle_name,--3
                                    umng.last_name,--4
                                    umng.profile_image_url,--5
                                    apm.consultation_note, --6
                                    apm.state, --7
                                    apm.schedule_date, --8
                                    apm.type, --9                              
                                    apm.consultation_duration, --10
                                    COALESCE(apm.cost,0), --11
                                    COALESCE(apm.unit_cost,'vnd'), --12
                                    mta.value, --13
                                    mta.unit, --14
                                    umng.id, --15
                                    att.file_extension, --16
                                    att.file_size, --17
                                    att.write_date, --18
                                    att.url, -- 19
                                    att.file_name, --20
                                    att.create_uid, -- 21
                                    att.id, -- 22
                                    apm.write_date, --23                              
                                    case when ua.latest_activity_time > current_timestamp - interval '15 minutes' then True
                                            else False
                                    end onlineStatus, --24
                                    ua.activity_status, -- 25
                                    arev.rating_point, --26
                                    arev.comment, --27
                                    apm.cost, --28
                                    apm.consultation_duration, --29
                                    apm.unit_cost, --30
                                    apm.booking_status --31
                                from appointment apm
                                    left join user_account ua on apm.doctor_id = ua.id
                                    left join user_mngment umng on  umng.id = ua.user_mngment_id 
                                    left join doctor_profile dp on  umng.id = dp.user_mngment_id
                                    left join meta_dr_consultation_durations mta on mta.code = apm.consultation_duration
                                    left join attachment att on att.res_id = apm.id and att.res_model = 'appointment'
                                    left join appointment_review arev on arev.appointment_id = apm.id
                                where apm.id = %s '''
                    #print sql%(data.get('apm_id'))
                    record = gs.sql_execute(pool, sql, para_values=[data.get('apm_id')])

                    user_account_id = None if record[0][0] in ('False', 'None', '') else record[0][0]
                    title_code = None if record[0][1] in ('False', 'None', '') else record[0][1]
                    first_name = None if record[0][2] in ('False', 'None', '') else record[0][2]
                    middle_name = None if record[0][3] in ('False', 'None', '') else record[0][3]
                    last_name = None if record[0][4] in ('False', 'None', '') else record[0][4]
                    profile_image_url = None if record[0][5] in ('False', 'None', '') else record[0][5]
                    note = None if record[0][6] in ('False', 'None', '') else record[0][6]
                    state_code = None if record[0][7] in ('False', 'None', '') else record[0][7]
                    schedule_date = int(time.mktime(record[0][8].timetuple())) or None
                    type_code = None if record[0][9] in (None, '') else record[0][9]
                    final_cost_value = None if record[0][11] in (None, '') else record[0][11]
                    final_cost_unit = None if record[0][12] in (None, '') else record[0][12]
                    duration = None if record[0][13] in (None, '') else record[0][13]
                    unit = None if record[0][14] in (None, '') else record[0][14]
                    profile_id = None if record[0][15] in (None, '') else record[0][15]
                    appointmentTimeDelta = record[0][8] + timedelta(minutes=15)
                    isAvailableToCall = True if record[0][9] == 'live' and record[0][24] == True and record[0][25] == 'online' and record[0][7] == 'suspended' else False


                    status_call = 'incomplete'
                    status_call_name = 'Incomplete'
                    if state_code in ('successful_finished','free_finished','canceled_by_doctor'):
                        status_call = 'complete'
                        status_call_name  = 'Complete'

                    documents = []
                    for line in record:
                        if line[21]:
                            document = {}
                            document.update({
                                "id": line[22],
                                "url": line[19],
                                "fileName": line[20],
                                "fileExtension": line[16],
                                "fileSize": self.general_business.bytesToSize(line[17]),
                                "latestUpdateTime": int(time.mktime(line[18].timetuple()))
                            })
                            if line[0] == line[21]:
                                document.update({
                                    "uploadUser": {
                                        "id": res_token[7],
                                        "titleCode": title_code,
                                        "firstName": first_name,
                                        "middleName": middle_name,
                                        "lastName": last_name
                                    }
                                })
                            else:
                                sql = '''select ua.user_mngment_id,umg.title,umg.first_name,umg.middle_name,umg.last_name
                                        from user_account ua
                                        left join user_mngment umg on umg.id = ua.user_mngment_id
                                        where ua.id = %s ''' % (line[21])

                                uinfo = gs.sql_execute(pool, sql)[0]
                                document.update({
                                    "uploadUser": {
                                        "id": uinfo[0],
                                        "titleCode": uinfo[1],
                                        "firstName": uinfo[2],
                                        "middleName": uinfo[3],
                                        "lastName": uinfo[4]
                                    }
                                })
                            documents.append(document)
                    if state_code in ('successful_finished','free_finished','canceled_by_doctor'):
                        isAvailableToReview = True
                    elif state_code == 'canceled_by_patient' and record[0][23] > appointmentTimeDelta:
                        isAvailableToReview = True
                    else:
                        isAvailableToReview = False

                    resp.update({
                        'status_code': 200,
                        'resq_body': {
                            'doctor': {
                                'id': profile_id,
                                'user_account_id': user_account_id,
                                'titleCode': title_code,
                                'firstName': first_name,
                                'middleName': middle_name,
                                'lastName': last_name,
                                'avatar': profile_image_url,
                                'specialtyCodes': self.get_specialty_codes(profile_id)
                            },
                            'typeCode': type_code,
                            'stateCode': _(status_call),
                            'stateCodeName': _(status_call_name),
                            'booking_status': record[0][31],
                            'scheduleTime': schedule_date,
                            'duration': {
                                'value': duration,
                                'unitCode': unit
                            },
                            'note': note,
                            'finalCost': {
                                'value': final_cost_value,
                                'unitCode': final_cost_unit
                            },
                            'callList': self.get_appoinment_callList(data.get('apm_id')),
                            'review': self.get_appoinment_review(data.get('apm_id')),
                            "documentList": documents,
                            'isAvailableToReview': isAvailableToReview,
                            "isAvailableToCall": isAvailableToCall
                        }})
                    
                        
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_get_appointment_details %s' % str(e))
            client.captureException()
        return resp

    def get_specialty_codes(self, doctor_mngment_id):
        try:
            gs = self.general_business
            pool = self.pool_conn
            specialty_codes = []
            sql = '''select drspc.specialties_code,dp.consultation_type
                        from doctor_profile dp
                            left join dr_doctorprofile_specialties drspc on dp.id = drspc.doctor_profile_id
                        where dp.user_mngment_id = %s ''' % (doctor_mngment_id)

            records = gs.sql_execute(pool, sql)

            for line in records:
                specialty_codes.append({'code': line[0]})

        except Exception as e:
            logger.error('error_get_specialty_codes %s' % str(e))
            client.captureException()
        return specialty_codes

    def get_appoinment_review(self, apm_id):
        try:
            gs = self.general_business
            pool = self.pool_conn

            review = {}

            sql = '''select rating_point, comment
                        from appointment_review
                        where appointment_id = %s '''

            records = gs.sql_execute(pool, sql, para_values=[apm_id])
            if records:
                review = {
                    'ratingPoint': records[0][0],
                    'comment': records[0][1]
                }

        except Exception as e:
            logger.error('error_get_appoinment_review %s' % str(e))
            client.captureException()
        return review

    def get_appoinment_callList(self, apm_id):
        try:
            gs = self.general_business
            pool = self.pool_conn

            callList = []

            sql = '''select session_id, COALESCE(start_time,create_date), end_time
                        from appointment_call_history
                        where appointment_id = %s '''

            records = gs.sql_execute(pool, sql, para_values=[apm_id])

            for line in records:
                if line[1]:
                    start_time = int(time.mktime(line[1].timetuple()))
                else:
                    start_time = 0
                if line[2] and line[1]:
                    end_time = int(time.mktime(line[2].timetuple()))
                    diff_time = end_time - start_time
                else:
                    diff_time = 0

                callList.append({
                    'sessionId': line[0],
                    'action_time': start_time,
                    'callTypeCode': 'incoming',
                    'duration': {
                        'value': diff_time,
                        'unitCode': 'second'
                    }
                })

        except Exception as e:
            logger.error('error_get_appoinment_callList %s' % str(e))
            client.captureException()
        return callList

    def appointment_review_update(self, data):
        logger.info('appointment_review_update %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "update_review_appointment_failed",
                        "title": _("Update review appointment failed"),
                        "msg": _("An error occurred. Please try to request again.")
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            dateTimeInput = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            ratingPoint = data.get('ratingPoint')
            apm_id = data.get('apm_id')
            if float(ratingPoint) > 5.0 or float(ratingPoint) < 1.0:
                resp = {'status_code': 401,
                        'resq_body':
                            {
                                "title": _("Invalid rating point"),
                                "msg": _("Rating point must be between 1 and 5"),
                                "code": "invalid_rating_point"
                            }
                        }
            else:
                user_token = data.get('user_token')
                if user_token:
                    res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                    if res_token:
                        sql_select_appointment = ''' SELECT state,apmr.id 
                                                    FROM appointment apm
                                                        left join appointment_review apmr on apmr.appointment_id = apm.id
                                                    WHERE apm.id = %s '''
                        records = gs.sql_execute(pool, sql_select_appointment, para_values=[apm_id])
                        if records[0][0] == 'successful_finished' or records[0][0] == 'free_finished' or records[0][
                            0] == 'canceled_by_doctor':
                            if records[0][1]:
                                sql_update = '''UPDATE appointment_review 
                                                            SET rating_point = %s, 
                                                                comment = %s, 
                                                                write_date   = %s
                                                            WHERE appointment_id = %s'''
                                gs.sql_execute(pool, sql_update, commit=True, para_values=(data.get('ratingPoint'),
                                                                                           data.get('comment'),
                                                                                           dateTimeInput,
                                                                                           apm_id))
                            else:
                                sql_insert = '''INSERT INTO appointment_review (appointment_id,comment,rating_point,create_date,write_date)
                                                VALUES (%s,%s,%s,%s,%s)
                                                            '''
                                gs.sql_execute(pool, sql_insert, commit=True, para_values=(apm_id,
                                                                                           data.get('comment'),
                                                                                           data.get('ratingPoint'),
                                                                                           dateTimeInput,
                                                                                           dateTimeInput
                                                                                           ))

                            resp = {'status_code': 200,
                                    'resq_body':
                                        {
                                            "code": "success",
                                            "title": _("Success"),
                                            "msg": _("Update review successful.")
                                        }
                                    }

                        else:
                            resp = {'status_code': 401,
                                    'resq_body':
                                        {
                                            "code": "invalid_appointment_state",
                                            "title": _("invalid appointment state"),
                                            "msg": _("The appointment is not completed yet")
                                        }
                                    }
                    else:
                        resp = {'status_code': 203,
                                'resq_body':
                                    {
                                        "code": "expired_token",
                                        "title": _("Expired token"),
                                        "msg": _("Your login-token is expired, please try to login again")
                                    }
                                }
        except Exception as e:
            logger.error('error_appointment_review_update %s' % str(e))
            client.captureException()
        return resp

    # WD6.8 API for doctor to get all the appoinment list in a date range
    def doctor_get_all_the_oppointment_list_in_date_range(self, data):
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        logger.info('doctor_get_all_the_oppointment_list_in_date_range %s', data)
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "get_all_the_oppointment_list_in_date_range_failed",
                        "title": _("Get all the appoinment list in a date range"),
                        "msg": _("An error occurred. Please try to request again.")
                    }
                }

        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            startTime = data.get('startDate', None)
            endTime = data.get('endDate', None)

            if startTime == None or endTime == None:
                return resp

            start_datetime = datetime.strptime(startTime + ' 00:00:00', '%d-%m-%Y %H:%M:%S')
            end_datetime = datetime.strptime(endTime + ' 23:59:59', '%d-%m-%Y %H:%M:%S')

            if start_datetime > end_datetime:
                resp.update({'status_code': 401,
                             'resq_body':
                                 {
                                     "code": "wrong_date_range",
                                     "title": _("Wrong date range"),
                                     "msg": _("The start time is bigger than end time, please try to set again")
                                 }
                             })
                return resp

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    sql_select_appointment = ''' SELECT apm.id, --0
                                                        apm.schedule_date, --1
                                                        apm.state, --2
                                                        mta.value, --3
                                                        mta.unit, --4
                                                        umng.id, --5
                                                        umng.first_name,  --6
                                                        umng.middle_name, --7
                                                        umng.last_name, --8
                                                        date(apm.schedule_date) as schedule, --9
                                                        ua.phone, --10
                                                        umng.gender, --11
                                                        umng.birthday,--12
                                                        coalesce(umng.profile_image_url,''),--13
                                                        coalesce(uad.number,''),--14
                                                        coalesce(uad.street,''),--15
                                                        coalesce(uad.city,''),--16
                                                        coalesce(uad.state,''),--17
                                                        coalesce(uad.postcode,''),--18
                                                        coalesce(uad.country,'')--19
                                                    FROM appointment apm
                                                        left join user_account ua on apm.patient_id = ua.id
                                                        left join user_mngment umng on  umng.id = ua.user_mngment_id
                                                        left join doctor_profile dp on dp.user_mngment_id = umng.id 
                                                        left join meta_dr_consultation_durations mta on mta.code = apm.consultation_duration
                                                        left join user_contact uc on uc.user_mngment_id = umng.id
                                                        left join user_address uad on uad.user_contact_id = uc.id and is_primary = True
                                                    WHERE apm.doctor_id = %s  AND schedule_date >= %s::timestamp AND schedule_date <= %s::timestamp 
                                                      and apm.type != 'live'
                                                    ORDER BY apm.schedule_date '''
                    records = gs.sql_execute(pool, sql_select_appointment,
                                             para_values=(user_id, start_datetime, end_datetime))

                    allList = []
                    item = None
                    flag = None
                    for line in records:
                        if line[9] != flag:
                            flag = line[9]
                            if item != None:
                                allList.append(item)

                            item = {
                                'date': line[9].strftime('%d-%m-%Y'),
                                'appointmentList': [{
                                    'id': line[0],
                                    'stateCode': line[2],
                                    'scheduleTime': int(time.mktime(line[1].timetuple())) or None,
                                    'duration': {
                                        'value': line[3],
                                        'unitCode': line[4]
                                    },
                                    'patient': {
                                        'id': line[5],
                                        'firstName': None if line[6] in ('False', 'None', '') else line[6],
                                        'middleName': None if line[7] in ('False', 'None', '') else line[7],
                                        'lastName': None if line[8] in ('False', 'None', '') else line[8],
                                        'phoneNumber': None if line[10] in ('False', 'None', '') else line[10],
                                        'genderCode': None if line[11] in ('False', 'None', '') else line[11],
                                        'dayOfBirth': line[12] and int(time.mktime(line[12].timetuple())) or None,
                                        'avatar': None if line[13] in ('False', 'None', '') else line[13],
                                        'primaryAddress': {
                                            "number": None if line[14] in ('False', 'None', '') else line[14],
                                            "street": None if line[15] in ('False', 'None', '') else line[15],
                                            "cityCode": None if line[16] in ('False', 'None', '') else line[16],
                                            "state": None if line[17] in ('False', 'None', '') else line[17],
                                            "postcode": None if line[18] in ('False', 'None', '') else line[18],
                                            "countryCode": None if line[19] in ('False', 'None', '') else line[19],
                                        }
                                    }
                                }]
                            }

                        elif line[9] == flag:
                            item['appointmentList'].append(
                                {
                                    'id': line[0],
                                    'stateCode': line[2],
                                    'scheduleTime': int(time.mktime(line[1].timetuple())) or None,
                                    'duration': {
                                        'value': line[3],
                                        'unitCode': line[4]
                                    },
                                    'patient': {
                                        'id': line[5],
                                        'firstName': None if line[6] in ('False', 'None', '') else line[6],
                                        'middleName': None if line[7] in ('False', 'None', '') else line[7],
                                        'lastName': None if line[8] in ('False', 'None', '') else line[8],
                                        'phoneNumber': None if line[10] in ('False', 'None', '') else line[10],
                                        'genderCode': None if line[11] in ('False', 'None', '') else line[11],
                                        'dayOfBirth': line[12] and int(time.mktime(line[12].timetuple())) or None,
                                        'avatar': None if line[13] in ('False', 'None', '') else line[13],
                                        'primaryAddress': {
                                            "number": None if line[14] in ('False', 'None', '') else line[14],
                                            "street": None if line[15] in ('False', 'None', '') else line[15],
                                            "cityCode": None if line[16] in ('False', 'None', '') else line[16],
                                            "state": None if line[17] in ('False', 'None', '') else line[17],
                                            "postcode": None if line[18] in ('False', 'None', '') else line[18],
                                            "countryCode": None if line[19] in ('False', 'None', '') else line[19],
                                        }
                                    }
                                }
                            )

                    if item != None:
                        allList.append(item)

                    resp.update({'status_code': 200,
                                 'resq_body':
                                     {
                                         'list': allList
                                     }
                                 })
                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
                return resp
        except Exception as e:
            logger.error('error_doctor_get_all_the_oppointment_list_in_date_range %s' % str(e))
            client.captureException()
        return resp

    def appointment_change_state(self, user_id, from_state, to_state, apm_id, payment_trans_id=0, reason=''):
        try:
            gs = self.general_business
            pool = self.pool_conn
            dateTimeInput = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            if payment_trans_id == '' or not payment_trans_id:
                payment_trans_id = 0
            sql_insert_trans = '''INSERT INTO appointment_transaction(create_date, 
                                                                      write_date, 
                                                                      appointment_id, 
                                                                      payment_trans_id, 
                                                                      from_state, 
                                                                      to_state, 
                                                                      user_account_id,
                                                                      note)
                                        VALUES (%s, %s, %s, %s, %s, %s, %s, %s)
                                '''
            gs.sql_execute(pool, sql_insert_trans, commit=True, para_values=(dateTimeInput,
                                                                             dateTimeInput,
                                                                             apm_id,
                                                                             payment_trans_id,
                                                                             from_state,
                                                                             to_state,
                                                                             user_id,
                                                                             reason))
        except Exception as e:
            logger.error('error_appointment_change_state %s' % str(e))
            client.captureException()

    def appointment_note_submit(self, data):
        logger.info('appointment_note_submit %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "appointment_note_submit_failed",
                        "title": _("appointment note submit failed"),
                        "msg": _("An error occurred. Please try to request again.")
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            apm_id = data.get('apm_id')
            note = data.get('note', '')
            user_token = data.get('user_token')

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    sql_update = '''update appointment set consultation_note = %s where id = %s '''
                    gs.sql_execute(pool, sql_update, commit=True, para_values=(note, apm_id))
                    resp.update({'status_code': 200,
                                 'resq_body':
                                     {
                                         "code": "success",
                                         "title": _("Success"),
                                         "msg": _("Summit appointment note successfully")
                                     }
                                 })
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_appointment_note_submit %s' % str(e))
            client.captureException()
        return resp

    def appointment_note_get(self, data):
        logger.info('appointment_note_get %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "appointment_note_get_failed",
                        "title": _("appointment note get failed"),
                        "msg": _("An error occurred. Please try to request again.")
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            apm_id = data.get('apm_id')
            user_token = data.get('user_token')

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    sql_update = '''select COALESCE(consultation_note,'') from appointment where id = %s ''' % (apm_id)
                    note = gs.sql_execute(pool, sql_update, commit=True)
                    resp.update({'status_code': 200,
                                 'resq_body':
                                     {
                                         "note": None if note[0][0] in ('False', 'None', '') else note[0][0]
                                     }
                                 })
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_appointment_note_get %s' % str(e))
            client.captureException()
        return resp

    def check_action_appointment(self, apm_id, action, lang = 'en'):
        _ = self.general_business.get_lang_instance(lang)
        resp = {}
        state_check = {'cancel': ['confirmed_by_patient', 'confirmed'],
                       'reschedule': ['confirmed_by_patient', 'suspended', 'confirmed']}
        state_cost = {'cancel': ['confirmed'],
                      'reschedule': ['confirmed']}
        state_percen = {'cancel': 0.5,
                        'reschedule': 0.3}
        state_key = {'cancel': 'cancelationCost',
                     'reschedule': 'reschedulingCost'}
        gs = self.general_business
        pool = self.pool_conn
        if action == 'reschedule':
            cost_sql = '''when now() >= schedule_date - interval '1 day' 
                                and now() <= schedule_date + interval '15 minute' 
                        then (coalesce(apm.consultation_fee,0)::float + greatest(%s,coalesce(apm.consultation_fee,0)*%s)) * %s
                        ''' %(self.min_commission,
                              self.com_ratio/100,
                              state_percen[action])
        else:
            cost_sql = '''when now() >= schedule_date - interval '1 day' 
                                and now() <= schedule_date + interval '15 minute' 
                        then (coalesce(apm.cost,0)::float) '''# % state_percen[action]
        sql = '''
                    with discount as
                      (select amount,
                            appointment_id 
                      from business_transfer_transaction 
                      where business_type = 'apm_discount' 
                          and state = 'paid' 
                          and appointment_id = %s)

                    select apm.state,
                          coalesce(consultation_unit,'vnd') consultation_unit,
                          coalesce(available_balance,0) available_balance,
                    case 
                        when now() < schedule_date - interval '1 day' then 0 '''\
                        + cost_sql +\
                        ''' else 0
                    end reschedule_cost,
                    coalesce(dc.amount,0) discount_amount,--4
                    coalesce(apm.consultation_fee,0),--5
                    coalesce(apm.reschedule_count,0)--6
                    from appointment apm
                    left join discount dc on dc.appointment_id = apm.id
                    left join user_account ua on ua.id = apm.doctor_id
                    left join user_wallet uw on uw.user_account_id = apm.patient_id
                    left join doctor_profile dp on dp.user_mngment_id = ua.user_mngment_id
                    where apm.id = %s
                '''
        # print sql

        res = gs.sql_execute(pool, sql, para_values=(apm_id,apm_id))
        if res:
            cost = 0
            if res[0][0] in state_cost[action] and res[0][3] > 0:
                cost = res[0][3]

            if action == 'cancel':
                margin = max(self.min_commission, res[0][5] * (self.com_ratio / 100))
                discount_per = float(res[0][4]*100/(res[0][5] + margin))
                if discount_per == 50.0:
                    cost = res[0][3]*state_percen[action]
                elif discount_per == 100.0:
                    cost = -float((res[0][5] + margin)) / 2  # + ((res[0][5] + margin) * 0.3 * res[0][6])*0.5
                else:
                    cost = float((res[0][5] + margin))/2 #+ ((res[0][5] + margin) * 0.3 * res[0][6])*0.5

            if res[0][0] == 'confirmed':
                if res[0][2] < cost or (action == 'cancel' and cost < 0 and res[0][2] < -cost) :
                    resp = {
                        'status_code': 401,
                        'resq_body': {
                            "code": "not_enough_balance",
                            "title": _("Not enough balance"),
                            "msg": _("Your account balance is not enough for %s the appointment" % action)
                        }
                    }
                    return resp
            elif res[0][0] not in state_check[action]:
                resp = {
                    'status_code': 401,
                    'resq_body': {
                        "code": "invalid_source_state",
                        "title": _("Invalid source state"),
                        "msg": _("You cannot change the state from %s to %s") % (res[0][0], action)
                    }
                }
                return resp
            resp = {
                'status_code': 200,
                'resq_body':
                    {
                        state_key[action]: {
                            "value": cost,
                            "unitCode": res[0][1],
                            "discount": res[0][4]
                        }

                    }
            }
        return resp

    # MP 6.8
    def get_check_appointment_reschedule(self, data):
        logger.info('get_check_appointment_reschedule %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "appointment_reschedule_check_failed",
                        "title": _("Appointment reschedule check failed"),
                        "msg": _("An error occurred. Please try to request again.")
                    }
                }
        try:
            user_token = data.get('user_token')
            apm_id = data.get('apm_id')

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    resp = self.check_action_appointment(apm_id, 'reschedule')
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_get_check_appointment_reschedule %s' % str(e))
            client.captureException()

        return resp

    # MP6.9 API for patient to reschedule the specificic appointment
    def patient_reschedule_the_specificic_appointment(self, data):
        logger.info('patient_reschedule_the_specificic_appointment %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "patient_reschedule_the_specificic_appointment_failed",
                        "title": _("Patient reschedule the specificic appointment failed"),
                        "msg": _("An error occurred. Please try to request again.")
                    }
                }
        try:
            gs = self.general_business
            bt = self.business_transfer
            pool = self.pool_conn
            user_token = data.get('user_token')
            newScheduleDate = data.get('newScheduleDate')
            startTime = data.get('newStartTime')
            apm_id = data.get('apm_id')

            if startTime == None or newScheduleDate == None:
                return resp
            newScheduleDate = datetime.strptime(data.get('newScheduleDate'), '%d-%m-%Y').strftime('%Y-%m-%d')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    sql = '''select doctor_id,--0 
                                    patient_id,--1
                                    schedule_date,--2
                                    state,--3
                                    consultation_duration,--4
                                    coalesce(unit_cost,'vnd'),--5
                                    coalesce(cost,0),--6
                                    coalesce(parent_id,id),--7
                                    COALESCE(reschedule_count,0),--8
                                    COALESCE(consultation_fee,0)--9
                             from appointment
                             where id = %s '''
                    apm_info = gs.sql_execute(pool, sql, para_values=[apm_id])[0]

                    sql = '''select id, 
                                    user_account_id, 
                                    available_balance 
                            from user_wallet 
                            where user_account_id = %s''' % res_token[1]
                    wallets = gs.sql_execute(pool, sql)
                    info_wallets = {}
                    for wallet in wallets:
                        if int(wallet[1]) == int(res_token[1]):
                            info_wallets.update({'patient': {'wallet_id': wallet[0],
                                                             'available_balance': wallet[2]}})
                        else:
                            info_wallets.update({'doctor': {'wallet_id': wallet[0],
                                                            'available_balance': wallet[2]}})

                    res_check = self.check_action_appointment(apm_id, 'reschedule')
                    if res_check and res_check['status_code'] != 200:
                        return res_check
                    elif res_check and res_check['status_code'] == 200:
                        reschedule_cost = res_check['resq_body']['reschedulingCost']['value']
                        timeslot_sql = '''select apm.id
                                          from appointment apm 
                                            inner join user_account ua on apm.doctor_id = ua.id                               
                                          where apm.doctor_id = %s 
                                            and apm.schedule_date = %s and apm.state not in ('successful_finished','rescheduled_by_patient','canceled_by_doctor','canceled_by_patient')'''
                        timeslot_result = gs.sql_execute(pool, timeslot_sql,
                                                         para_values=(apm_info[0], '%s %s' % (newScheduleDate, startTime)))
                        if timeslot_result:
                            resp.update({'status_code': 401,
                                         'resq_body': {
                                             "code": "doctor_unavailable",
                                             "title": _("Doctor unavailable"),
                                             "msg": _("The doctor is not available for the time you choose")
                                         }
                                         })
                            return resp
                        else:
                            currentTime = datetime.now().strftime('%Y-%m-%d %H:%M')

                            sql_update = '''UPDATE appointment SET
                                                                  state='rescheduled_by_patient',
                                                                  write_date=%s
                                            WHERE id = %s '''
                            self.appointment_change_state(res_token[1], apm_info[3], 'rescheduled_by_patient', apm_id)

                            gs.sql_execute(pool, sql_update, commit=True,
                                           para_values=(currentTime,
                                                        apm_id))
                            reschedule_count = 1 if reschedule_cost > 0 else 0
                            sql_insert = '''INSERT INTO appointment (created_date, 
                                                                        schedule_date,
                                                                        state,
                                                                        doctor_id,
                                                                        patient_id,
                                                                        write_date,
                                                                        consultation_duration,
                                                                        unit_cost,
                                                                        cost,
                                                                        type,
                                                                        parent_id,
                                                                        reschedule_count,
                                                                        consultation_fee) 
                                            VALUES (%s,%s,'confirmed_by_patient',%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
                                            RETURNING ID'''

                            new_apm_id = gs.sql_execute(pool, sql_insert, commit=True, para_values=(currentTime,
                                                                                                '%s %s' % (
                                                                                                newScheduleDate,
                                                                                                startTime),
                                                                                                apm_info[0],
                                                                                                res_token[1],
                                                                                                currentTime,
                                                                                                apm_info[4],
                                                                                                apm_info[5],
                                                                                                apm_info[6] + reschedule_cost,
                                                                                                'by_appointment',
                                                                                                apm_info[7],
                                                                                                apm_info[8] + reschedule_count,
                                                                                                apm_info[9]))[0][0]
                            sql_update = '''update business_transfer_transaction 
                                          set appointment_id = %s where appointment_id = %s''' % (new_apm_id, apm_id)
                            gs.sql_execute(pool, sql_update, commit=True)

                            if reschedule_cost > 0:
                                bt.create_reschedule_fee(new_apm_id, reschedule_cost)
                            # Save trans apm
                            sql_insert_trans = '''INSERT INTO appointment_transaction(create_date, 
                                                                                        write_date, 
                                                                                        appointment_id, 
                                                                                        payment_trans_id, 
                                                                                        from_state, 
                                                                                        to_state, 
                                                                                        user_account_id,
                                                                                        note)
                                                VALUES (%s, %s, %s, %s, %s, %s, %s, %s)'''
                            gs.sql_execute(pool, sql_insert_trans, commit=True, para_values=(currentTime,
                                                                                             currentTime,
                                                                                             new_apm_id,
                                                                                             0,
                                                                                             'new',
                                                                                             'confirmed_by_patient',
                                                                                             res_token[1],
                                                                                             'Create APM'))
                            bt.confirm_appointment(new_apm_id)
                            # Push notify to webapp
                            gs.publish_message_mqtt("%s" % apm_info[0], 'patient',
                                                    context={'action': 'patient_reschedule_appointment',
                                                             'appointmentId': int(new_apm_id),
                                                             'lang': data.get('lang', 'en')})
                            resp.update({'status_code': 200,
                                         'resq_body':
                                             {
                                                 "newAppointmentID": new_apm_id
                                             }
                                         })
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('patient_reschedule_the_specificic_appointment %s' % str(e))
            client.captureException()

        return resp

    # WD6.14 API for doctor to cancel the specificic appointment
    def doctor_cancel_the_specificic_appointment(self, data, context={}):
        logger.info('doctor_cancel_the_specificic_appointment %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "doctor_cancel_the_specificic_appointment_failed",
                        "title": _("Doctor cancel the specificic appointment failed"),
                        "msg": _("An error occurred. Please try to request again.")
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            bt = self.business_transfer
            if not context:
                context = {}
            cron_call = context.get('cron_call', False)
            user_token = data.get('user_token')
            apm_id = data.get('apm_id')
            reason = data.get('reason', '')

            # if reason in (None, ''):
            #     resp = {
            #         'status_code': 401,
            #         'resq_body': {
            #             "title": _("No reason"),
            #             "code": "no_reason",
            #             "msg": _("The canceling reason is required, please input it")
            #         }
            #     }
            #     return resp

            if user_token or cron_call:
                if not cron_call:
                    res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                else:
                    res_token = ['',context.get('user_id')]
                if res_token:
                    user_id = res_token[1]
                    sql = ''' select apm.state,--0
                                    doctor_id,--1
                                    patient_id,--2
                                    COALESCE(cost,0),--3
                                    COALESCE(consultation_fee,0),--4
                                    unit_cost,--5
                                    COALESCE(reschedule_count,0)--6
                                    
                              from appointment apm
                              where apm.id = %s '''

                    res = gs.sql_execute(pool, sql, para_values=[apm_id])
                    if res:
                        if res[0][0] not in ('confirmed_by_patient', 'suspended', 'confirmed') and not cron_call:
                            resp = {
                                'status_code': 401,
                                'resq_body': {
                                    "code": "invalid_source_state",
                                    "title": _("Invalid source state"),
                                    "msg": _("You cannot change the state from %s to canceled") % res[0][0]
                                }
                            }
                            return resp

                        currentTime = datetime.now().strftime('%Y-%m-%d %H:%M')
                        sql_update = '''UPDATE appointment SET state= 'canceled_by_doctor',note = %s,write_date=%s
                                        WHERE id = %s '''
                        gs.sql_execute(pool, sql_update, commit=True, para_values=(reason,currentTime,apm_id))
                        # Refund 100% to patient
                        bt.refund_appointment_patient(apm_id, amount_refund=res[0][3])
                        bt.done_appointment(apm_id,amount=0)
                        self.appointment_change_state(user_id, res[0][0], 'canceled_by_doctor', apm_id,
                                                      reason=reason)

                        # Push notify to mobile
                        #gs.publish_message_mqtt("%s" % res[0][2], 'doctor',
                        #                        context={'action': 'doctor_cancel_appointment',
                        #                                 'appointmentId': int(apm_id),
                        #                                     'lang': data.get('lang', 'en')})
                        resp = {
                            'status_code': 200,
                            'resq_body': {
                                "code": "success",
                                "title": _("Success"),
                                "msg": _("You canceled the specificic appointment successfully")
                            }
                        }
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('doctor_cancel_the_specificic_appointment %s' % str(e))
            client.captureException()

        return resp

    # WD6.15 API for doctor to suspend the specificic appointment
    def doctor_suspend_the_specificic_appointment(self, data):
        logger.info('doctor_suspend_the_specificic_appointment %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "doctor_suspend_the_specificic_appointment_failed",
                        "title": _("Doctor suspend the specificic appointment failed"),
                        "msg": _("An error occurred. Please try to request again.")
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            apm_id = data.get('apm_id')
            reason = data.get('reason', '')

            # if reason in (None, ''):
            #     resp = {
            #         'status_code': 401,
            #         'resq_body': {
            #             "title": "No reason",
            #             "code": "no_reason",
            #             "msg": "The suspending reason is required, please input it"
            #         }
            #     }
            #     return resp

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    sql = ''' select apm.state,
                                    doctor_id,
                                    patient_id
                              from appointment apm
                              where apm.id = %s and apm.doctor_id = %s '''

                    res = gs.sql_execute(pool, sql, para_values=(apm_id, res_token[1]))
                    if res:
                        if res[0][0] not in ('processing','confirmed'):
                            resp = {
                                'status_code': 401,
                                'resq_body': {
                                    "code": "invalid_source_state",
                                    "title": _("Invalid source state"),
                                    "msg": _("You cannot change the state from %s to suspended") % res[0][0]
                                }
                            }
                            return resp
                        
                        currentTime = datetime.now().strftime('%Y-%m-%d %H:%M')
                        sql_update = '''UPDATE appointment SET state= 'suspended',suspend_reason = %s,write_date=%s
                                        WHERE id = %s '''
                        gs.sql_execute(pool, sql_update, commit=True, para_values=(reason,currentTime, apm_id))
                        self.appointment_change_state(res_token[1], res[0][0], 'suspended', apm_id, reason=reason)

                        # Push notify to mobile
                        #gs.publish_message_mqtt("%s" % res[0][2], 'doctor',
                        #                        context={'action': 'doctor_suspend_appointment',
                        #                                 'appointmentId': int(apm_id),
                        #                                     'lang': data.get('lang', 'en')})

                        resp = {
                            'status_code': 200,
                            'resq_body': {
                                "code": "success",
                                "title": _("Success"),
                                "msg": _("You suspended the specificic appointment successfully")
                            }
                        }
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_doctor_suspend_the_specificic_appointment %s' % str(e))
            client.captureException()

        return resp

    # WD6.16 API for doctor to complete the specificic appointment
    def doctor_complete_the_specificic_appointment(self, data):
        _ = self.general_business.get_lang_instance(data.get('lang','en'))
        logger.info('doctor_complete_the_specificic_appointment %s', data)
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "doctor_complete_the_specificic_appointment_failed",
                        "title": _("Doctor complete the specificic appointment failed"),
                        "msg": _("An error occurred. Please try to request again.")
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            apm_id = data.get('apm_id')
            optionCode = data.get('optionCode')
            if not optionCode:
                return resp
            reason = optionCode

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    sql = ''' select apm.state,--0
                                    doctor_id,--1
                                    patient_id,--2
                                    COALESCE(cost,0),--3
                                    COALESCE(consultation_fee,0),--4
                                    unit_cost,--5
                                    COALESCE(reschedule_count,0)--6
                                    
                              from appointment apm
                              where apm.id = %s'''

                    res = gs.sql_execute(pool, sql, para_values=[apm_id])
                    if res:
                        apm_info = {
                            'reschedule_count': res[0][6],
                            'consultation_fee': res[0][4],
                            'unit_cost': res[0][5],
                            'cost': res[0][3]
                        }
                        if res[0][0] not in ('processing','confirmed','suspended','session_failed','successful_finished'):
                            resp = {
                                'status_code': 401,
                                'resq_body': {
                                    "code": "invalid_source_state",
                                    "title": _("Invalid source state"),
                                    "msg": _("You cannot change the state from %s to successful") % res[0][0]
                                }
                            }
                            return resp
                        sql_role_hotline = ''' select uaur.id,ur.code from user_account_user_role uaur INNER JOIN user_role ur ON ur.id = uaur.user_role_id where uaur.user_account_id =  %s ''' % (res[0][1])
                                    
                        res_role_hotline = gs.sql_execute(pool, sql_role_hotline)
                        currentTime = datetime.now().strftime('%Y-%m-%d %H:%M')
                        if res_role_hotline and res_role_hotline[0][1] == 'hotline' :
                            sql_update = '''UPDATE appointment SET state= 'free_finished', note= %s,write_date=%s
                                        WHERE id = %s '''
                            gs.sql_execute(pool, sql_update, commit=True, para_values=(reason,currentTime, apm_id))
                            bt = self.business_transfer
                            cal_amount = self.calculate_amount_done_appointment(apm_info, reason)
                            bt.refund_appointment_patient(apm_id, amount_refund=cal_amount['patient_amount'])
                            bt.done_appointment(apm_id, amount=0)
                            self.appointment_change_state(res_token[1], res[0][0], 'free_finished', apm_id, reason=reason)

                        else : 
                            sql_update = '''UPDATE appointment 
                                            SET state= 'successful_finished', 
                                                note= %s,write_date=%s
                                            WHERE id = %s '''

                            gs.sql_execute(pool, sql_update, commit=True, para_values=(optionCode,currentTime, apm_id))

                            cal_amount = self.calculate_amount_done_appointment(apm_info,'done')
                            bt = self.business_transfer
                            bt.done_appointment(apm_id,amount=cal_amount['doctor_amount'])
                            self.appointment_change_state(res_token[1], res[0][0], 'successful_finished', apm_id,
                                                        reason=optionCode)

                        # Push notify to mobile(disabled)
                        #gs.publish_message_mqtt("%s" % res[0][2], 'doctor',
                        #                        context={'action': 'doctor_complete_appointment',
                        #                                 'appointmentId': int(apm_id),
                        #                                 'lang': data.get('lang', 'en')})

                        resp = {
                            'status_code': 200,
                            'resq_body': {
                                "code": "success",
                                "title": _("Success"),
                                "msg": _("You completed the specificic appointment successfully")
                            }
                        }
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_doctor_complete_the_specificic_appointment %s' % str(e))
            client.captureException()

        return resp

    def doctor_complete_the_specificic_appointment_hospital(self, data):
        _ = self.general_business.get_lang_instance(data.get('lang','en'))
        logger.info('doctor_complete_the_specificic_appointment_hospital %s', data)
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "doctor_complete_the_specificic_appointment_failed",
                        "title": _("Doctor complete the specificic appointment failed"),
                        "msg": _("An error occurred. Please try to request again.")
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            apm_id = data.get('apm_id')
            optionCode = data.get('optionCode')
            if not optionCode:
                return resp
            reason = optionCode

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    sql = ''' select apm.state,--0
                                    doctor_id,--1
                                    patient_id,--2
                                    COALESCE(cost,0),--3
                                    COALESCE(consultation_fee,0),--4
                                    unit_cost,--5
                                    COALESCE(reschedule_count,0)--6
                                    
                              from appointment apm
                              where apm.id = %s'''

                    res = gs.sql_execute(pool, sql, para_values=[apm_id])
                    if res:
                        apm_info = {
                            'reschedule_count': res[0][6],
                            'consultation_fee': res[0][4],
                            'unit_cost': res[0][5],
                            'cost': res[0][3]
                        }
                        if res[0][0] not in ('processing','confirmed','suspended','session_failed','successful_finished'):
                            resp = {
                                'status_code': 401,
                                'resq_body': {
                                    "code": "invalid_source_state",
                                    "title": _("Invalid source state"),
                                    "msg": _("You cannot change the state from %s to successful") % res[0][0]
                                }
                            }
                            return resp
                        sql_role_hotline = ''' select uaur.id,ur.code from user_account_user_role uaur INNER JOIN user_role ur ON ur.id = uaur.user_role_id where uaur.user_account_id =  %s ''' % (res[0][1])
                                    
                        res_role_hotline = gs.sql_execute(pool, sql_role_hotline)
                        currentTime = datetime.now().strftime('%Y-%m-%d %H:%M')
                        if res_role_hotline and res_role_hotline[0][1] == 'hotline' :
                            sql_update = '''UPDATE appointment SET state= 'free_finished', note= %s,write_date=%s
                                        WHERE id = %s '''
                            gs.sql_execute(pool, sql_update, commit=True, para_values=(reason,currentTime, apm_id))
                            bt = self.business_transfer
                            cal_amount = self.calculate_amount_done_appointment(apm_info, reason)
                            bt.refund_appointment_patient(apm_id, amount_refund=cal_amount['patient_amount'])
                            bt.done_appointment(apm_id, amount=0)
                            self.appointment_change_state(res_token[1], res[0][0], 'free_finished', apm_id, reason=reason)

                        else : 
                            sql_update = '''UPDATE appointment 
                                            SET state= 'successful_finished', 
                                                note= %s,write_date=%s
                                            WHERE id = %s '''

                            gs.sql_execute(pool, sql_update, commit=True, para_values=(optionCode,currentTime, apm_id))
                            if res[0][0] != 'successful_finished' :
                                cal_amount = self.calculate_amount_done_appointment(apm_info,'done')
                                bt = self.business_transfer
                                bt.done_appointment(apm_id,amount=cal_amount['doctor_amount'])
                                self.appointment_change_state(res_token[1], res[0][0], 'successful_finished', apm_id,
                                                            reason=optionCode)

                        # Push notify to mobile(disabled)
                        #gs.publish_message_mqtt("%s" % res[0][2], 'doctor',
                        #                        context={'action': 'doctor_complete_appointment',
                        #                                 'appointmentId': int(apm_id),
                        #                                 'lang': data.get('lang', 'en')})

                        if apm_id:
                            sql_update = '''update appointment set booking_status = 'close' where id = %s'''
                            gs.sql_execute(pool, sql_update, commit=True, para_values=[apm_id])
                        
                        resp = {
                            'status_code': 200,
                            'resq_body': {
                                "code": "success",
                                "title": _("Success"),
                                "msg": _("You completed the specificic appointment successfully")
                            }
                        }
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_doctor_complete_the_specificic_appointment %s' % str(e))
            client.captureException()

        return resp

    # WD6.17 API for doctor to mark the specific appointment as free
    def doctor_mark_the_specific_appointment_as_free(self, data):
        logger.info('doctor_mark_the_specific_appointment_as_free %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "doctor_mark_the_specific_appointment_as_free_failed",
                        "title": _("Doctor mark the specific appointment as free failed"),
                        "msg": _("An error occurred. Please try to request again.")
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            apm_id = data.get('apm_id')
            reason = data.get('optionCode')

            if reason not in ('free', 'poor_connection'):
                resp = {
                    'status_code': 401,
                    'resq_body': {
                        "title": "No reason",
                        "code": _("no_reason"),
                        "msg": _("The free reason is invalid")
                    }
                }
                return resp

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    sql = ''' select apm.state,--0
                                    doctor_id,--1
                                    patient_id,--2
                                    COALESCE(cost,0),--3
                                    COALESCE(consultation_fee,0),--4
                                    unit_cost,--5
                                    COALESCE(reschedule_count,0)--6

                              from appointment apm
                              where apm.id = %s '''

                    res = gs.sql_execute(pool, sql, para_values=[apm_id])
                    if res:
                        apm_info = {
                            'reschedule_count': res[0][6],
                            'consultation_fee': res[0][4],
                            'unit_cost': res[0][5],
                            'cost': res[0][3]
                        }
                        if res[0][0] not in ('processing','confirmed','suspended','free_finished'):
                            resp = {
                                'status_code': 401,
                                'resq_body': {
                                    "code": "invalid_source_state",
                                    "title": _("Invalid source state"),
                                    "msg": _("You cannot change the state from %s to free") % res[0][0]
                                }
                            }
                            return resp
                        
                        currentTime = datetime.now().strftime('%Y-%m-%d %H:%M')
                        sql_update = '''UPDATE appointment SET state= 'free_finished', note= %s,write_date=%s
                                        WHERE id = %s '''
                        gs.sql_execute(pool, sql_update, commit=True, para_values=(reason,currentTime, apm_id))
                        bt = self.business_transfer
                        cal_amount = self.calculate_amount_done_appointment(apm_info, reason)
                        bt.refund_appointment_patient(apm_id, amount_refund=cal_amount['patient_amount'])
                        bt.done_appointment(apm_id, amount=0)
                        # bt.done_appointment(apm_id,amount=cal_amount['doctor_amount'])
                        self.appointment_change_state(res_token[1], res[0][0], 'free_finished', apm_id, reason=reason)

                        # Push notify to mobile
                        #gs.publish_message_mqtt("%s" % res[0][2], 'doctor',
                        #                        context={'action': 'doctor_markasfree_appointment',
                        #                                 'appointmentId': int(apm_id),
                        #                                 'lang': data.get('lang', 'en')})
                        resp = {
                            'status_code': 200,
                            'resq_body': {
                                "code": "success",
                                "title": _("Success"),
                                "msg": _("You mark the specific appointment as free successfully")
                            }
                        }
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_doctor_mark_the_specificic_appointment_as_free %s' % str(e))
            client.captureException()

        return resp
    
    def doctor_mark_the_specific_appointment_as_failed(self, data):
        logger.info('doctor_mark_the_specific_appointment_as_failed %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "doctor_mark_the_specific_appointment_as_failed_failed",
                        "title": _("Doctor mark the specific appointment as failed session failed"),
                        "msg": _("An error occurred. Please try to request again.")
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            apm_id = data.get('apm_id')
            reason = data.get('optionCode')

            if reason not in ('session_failed','free', 'poor_connection','session_failed'):
                resp = {
                    'status_code': 401,
                    'resq_body': {
                        "title": "No reason",
                        "code": _("no_reason"),
                        "msg": _("The failed reason is invalid")
                    }
                }
                return resp

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    sql = ''' select apm.state,--0
                                    doctor_id,--1
                                    patient_id,--2
                                    COALESCE(cost,0),--3
                                    COALESCE(consultation_fee,0),--4
                                    unit_cost,--5
                                    COALESCE(reschedule_count,0)--6

                              from appointment apm
                              where apm.id = %s '''

                    res = gs.sql_execute(pool, sql, para_values=[apm_id])
                    if res:
                        apm_info = {
                            'reschedule_count': res[0][6],
                            'consultation_fee': res[0][4],
                            'unit_cost': res[0][5],
                            'cost': res[0][3]
                        }
                        if res[0][0] not in ('processing','confirmed','suspended','session_failed'):
                            resp = {
                                'status_code': 401,
                                'resq_body': {
                                    "code": "invalid_source_state",
                                    "title": _("Invalid source state"),
                                    "msg": _("You cannot change the state from %s to failed session") % res[0][0]
                                }
                            }
                            return resp
                        
                        currentTime = datetime.now().strftime('%Y-%m-%d %H:%M')
                        sql_update = '''UPDATE appointment SET state= 'session_failed', note= %s,write_date=%s
                                        WHERE id = %s '''
                        gs.sql_execute(pool, sql_update, commit=True, para_values=(reason,currentTime, apm_id))

                        #sql_update = '''update appointment set booking_status = 'open' where id = %s'''
                        #gs.sql_execute(pool, sql_update, commit=True, para_values=[apm_id])
                        #bt = self.business_transfer
                        #cal_amount = self.calculate_amount_done_appointment(apm_info, reason)
                        #bt.refund_appointment_patient(apm_id, amount_refund=cal_amount['patient_amount'])
                        #bt.done_appointment(apm_id, amount=0)
                        # bt.done_appointment(apm_id,amount=cal_amount['doctor_amount'])
                        self.appointment_change_state(res_token[1], res[0][0], 'session_failed', apm_id)

                        # Push notify to mobile
                        #gs.publish_message_mqtt("%s" % res[0][2], 'doctor',
                        #                        context={'action': 'doctor_markasfree_appointment',
                        #                                 'appointmentId': int(apm_id),
                        #                                 'lang': data.get('lang', 'en')})
                        resp = {
                            'status_code': 200,
                            'resq_body': {
                                "code": "success",
                                "title": _("Success"),
                                "msg": _("You mark the specific appointment as failed successfully")
                            }
                        }
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_doctor_mark_the_specificic_appointment_as_failed %s' % str(e))
            client.captureException()

        return resp

    def calculate_amount_done_appointment(self,apm_info,action):
        consultation_fee = apm_info.get('consultation_fee',False)
        # unit_cost = apm_info.get('unit_cost',False)
        reschedule_count = apm_info.get('reschedule_count',False)
        cost = apm_info.get('cost',False)
        margin = max(self.min_commission, consultation_fee*(self.com_ratio/100))
        fee_with_margin = consultation_fee + margin
        res = {
            'patient_amount': 0,
            'doctor_amount': 0
        }
        if action == 'done':
            dr_amount = consultation_fee + (consultation_fee * 0.3 * reschedule_count)
            res.update({'doctor_amount': dr_amount})
        elif action == 'cancel':
            pa_amount = cost/2
            dr_amount = (consultation_fee + (consultation_fee * 0.3 * reschedule_count))/2
            res = {
                'patient_amount': pa_amount,
                'doctor_amount': dr_amount
            }
        elif action == 'free':
            res = {
                'patient_amount': cost - margin,
                'doctor_amount': 0
            }
        elif action == 'poor_connection':
            res = {
                'patient_amount': cost,
                'doctor_amount': 0
            }

        return res

    # WD6.20 API for doctor to get specific appointment details
    def doctor_get_appointment_details(self, data=None):
        logger.info('doctor_get_appointment_details %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body': {
                    "code": "doctor_get_appointment_details",
                    "title": _("Get_appointment_details failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }}
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            apm_id = data.get('apm_id')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    sql = '''select ua.id, --0
                    umng.title,--1
                    umng.first_name,--2
                    umng.middle_name,--3
                    umng.last_name,--4
                    umng.profile_image_url,--5
                    apm.consultation_note, --6
                    apm.state, --7
                    apm.schedule_date, --8
                    apm.type, --9
                    apm.consultation_duration, --10
                    COALESCE(apm.cost,0), --11
                    COALESCE(apm.unit_cost,'vnd'), --12
                    mta.value, --13
                    mta.unit, --14
                    umng.id, --15
                    att.file_extension, --16
                    att.file_size, --17
                    att.write_date, --18
                    att.url, -- 19
                    att.file_name, --20
                    att.create_uid, -- 21
                    ua.phone, --22
                    att.id, --23
                    umng2.first_name,--24
                    umng2.middle_name,--25
                    umng2.last_name,--26
                    umng2.profile_image_url,--27
                    coalesce(uad.number,''),--28
                    coalesce(uad.street,''),--29
                    coalesce(uad.city,''),--30
                    coalesce(uad.state,''),--31
                    coalesce(uad.postcode,''),--32
                    coalesce(uad.country,''),--33
                    apm.suspend_reason, --34
                    apm.patient_id --35
                    from appointment apm
                    left join user_account ua on apm.doctor_id = ua.id
                    left join user_mngment umng on umng.id = ua.user_mngment_id
                    left join doctor_profile dp on umng.id = dp.user_mngment_id
                    left join meta_dr_consultation_durations mta on mta.code = apm.consultation_duration
                    left join attachment att on att.res_id = apm.id and att.res_model = 'appointment'
                    left join user_account ua2 on apm.patient_id = ua2.id
                    left join user_mngment umng2 on umng2.id = ua2.user_mngment_id
                    left join user_contact uc on uc.user_mngment_id = umng2.id
                    left join user_address uad on uad.user_contact_id = uc.id and is_primary = True
                    where apm.id = %s '''

                    record = gs.sql_execute(pool, sql, para_values=[apm_id])
                    if record:
                        # user_account_id = None if record[0][0] in ('False','None','') else record[0][0]
                        title_code = None if record[0][1] in ('False', 'None', '') else record[0][1]
                        first_name = None if record[0][2] in ('False', 'None', '') else record[0][2]
                        middle_name = None if record[0][3] in ('False', 'None', '') else record[0][3]
                        last_name = None if record[0][4] in ('False', 'None', '') else record[0][4]
                        # profile_image_url = None if record[0][5] in ('False','None','') else record[0][5]
                        note = None if record[0][6] in ('False', 'None', '') else record[0][6]
                        state_code = None if record[0][7] in ('False', 'None', '') else record[0][7]
                        schedule_date = int(time.mktime(record[0][8].timetuple())) or None
                        type_code = None if record[0][9] in (None,'') else record[0][9]
                        # final_cost_value = None if record[0][11] in (None,'') else record[0][11]
                        # final_cost_unit = None if record[0][12] in (None,'') else record[0][12]
                        duration = None if record[0][13] in (None, '') else record[0][13]
                        unit = None if record[0][14] in (None, '') else record[0][14]
                        # profile_id = None if record[0][15] in (None, '') else record[0][15]
                        phone = None if record[0][22] in (None, '') else record[0][22]

                        documents = []
                        for line in record:
                            if line[21]:
                                document = {}
                                document.update({
                                    "id": line[23],
                                    "url": line[19],
                                    "fileName": line[20],
                                    "fileExtension": line[16],
                                    "fileSize": self.general_business.bytesToSize(line[17]),
                                    "latestUpdateTime": int(time.mktime(line[18].timetuple()))
                                })
                                if line[0] == line[21]:
                                    document.update({
                                        "uploadUser": {
                                            "id": res_token[7],
                                            "titleCode": title_code,
                                            "firstName": first_name,
                                            "middleName": middle_name,
                                            "lastName": last_name,
                                            "phoneNumber": phone
                                        }
                                    })
                                else:
                                    sql = '''select ua.user_mngment_id,umg.title,umg.first_name,umg.middle_name,umg.last_name,ua.phone
                                                from user_account ua
                                                left join user_mngment umg on umg.id = ua.user_mngment_id
                                                where ua.id = %s ''' % (line[21])

                                    uinfo = gs.sql_execute(pool, sql)[0]
                                    document.update({
                                        "uploadUser": {
                                            "id": uinfo[0],
                                            "titleCode": uinfo[1],
                                            "firstName": uinfo[2],
                                            "middleName": uinfo[3],
                                            "lastName": uinfo[4],
                                            "phoneNumber": uinfo[5]
                                        }
                                    })
                                documents.append(document)
                        resp.update({
                            'status_code': 200,
                            'resq_body': {
                                'stateCode': state_code,
                                'scheduleTime': schedule_date,
                                'duration': {
                                    'value': duration,
                                    'unitCode': unit
                                },
                                'note': note,
                                "documentList": documents,
                                'review': self.get_appoinment_review(apm_id),
                                'consultationType': type_code,
                                'patient': {
                                        'id': record[0][35],
                                        'primaryAddress': {
                                            "number": None if record[0][28] in ('False', 'None', '') else record[0][28],
                                            "street": None if record[0][29] in ('False', 'None', '') else record[0][29],
                                            "cityCode": None if record[0][30] in ('False', 'None', '') else record[0][30],
                                            "state": None if record[0][31] in ('False', 'None', '') else record[0][31],
                                            "postcode": None if record[0][32] in ('False', 'None', '') else record[0][32],
                                            "countryCode": None if record[0][33] in ('False', 'None', '') else record[0][33]
                                        },
                                        "avatar": None if record[0][27] in ('False', 'None', '') else record[0][27],
		                                "firstName": None if record[0][24] in ('False', 'None', '') else record[0][24],
                                        "middleName": None if record[0][25] in ('False', 'None', '') else record[0][25],
                                        "lastName": None if record[0][26] in ('False', 'None', '') else record[0][26]
                                },
                                "suspendReason": None if record[0][34] in ('False', 'None', '') else record[0][34]
                            }})

                        sql = '''select ach.session_id, COALESCE(ach.start_time,ach.create_date), ach.end_time,umg.id,umg.first_name,umg.middle_name,umg.last_name,ua.phone
                                    from appointment_call_history ach
                                    inner join appointment apm on apm.id = ach.appointment_id
                                    inner join user_account ua on ua.id = apm.patient_id
                                    inner join user_mngment umg on umg.id = ua.user_mngment_id 
                                    where ach.appointment_id = %s '''

                        records = gs.sql_execute(pool, sql, para_values=[apm_id])
                        callList = []
                        for line in records:
                            start_time = line[1] and int(time.mktime(line[1].timetuple())) or None
                            if line[2] and line[1]:
                                end_time = int(time.mktime(line[2].timetuple()))
                                diff_time = end_time - start_time
                            else:
                                diff_time = 0
                            callList.append({
                                'sessionId': line[0],
                                'action_time': start_time,
                                'callTypeCode': 'incoming',
                                'duration': {
                                    'value': diff_time,
                                    'unitCode': 'second'
                                },
                                'caller': {
                                    'id': line[3],
                                    'firstName': line[4],
                                    'middleName': line[5],
                                    'lastName': line[6],
                                    'phone': line[7]
                                }
                            })
                        resp['resq_body'].update({'callList': callList})

                else:
                    resp = {'status_code': 203,
                            'resq_body': {
                                "code": "expired_token",
                                "title": _("Expired token"),
                                "msg": _("Your login-token is expired, please try to login again")
                            }}
        except Exception as e:
            logger.error('error_doctor_get_appointment_details %s' % str(e))
            client.captureException()
        return resp

    def doctor_get_appointment_details_hospital(self, data=None):
        logger.info('doctor_get_appointment_details hospital %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body': {
                    "code": "doctor_get_appointment_details",
                    "title": _("Get_appointment_details failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }}
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            apm_id = data.get('apm_id')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    sql = '''select ua.id, --0
                    umng.title,--1
                    umng.first_name,--2
                    umng.middle_name,--3
                    umng.last_name,--4
                    umng.profile_image_url,--5
                    apm.consultation_note, --6
                    apm.state, --7
                    apm.schedule_date, --8
                    apm.type, --9
                    apm.consultation_duration, --10
                    COALESCE(apm.cost,0), --11
                    COALESCE(apm.unit_cost,'vnd'), --12
                    mta.value, --13
                    mta.unit, --14
                    umng.id, --15
                    att.file_extension, --16
                    att.file_size, --17
                    att.write_date, --18
                    att.url, -- 19
                    att.file_name, --20
                    att.create_uid, -- 21
                    ua.phone, --22
                    att.id, --23
                    umng2.first_name,--24
                    umng2.middle_name,--25
                    umng2.last_name,--26
                    umng2.profile_image_url,--27
                    coalesce(uad.number,''),--28
                    coalesce(uad.street,''),--29
                    coalesce(uad.city,''),--30
                    coalesce(uad.state,''),--31
                    coalesce(uad.postcode,''),--32
                    coalesce(uad.country,''),--33
                    apm.suspend_reason, --34
                    arev.rating_point, --35
                    arev.comment, --36
                    apm.cost, --37
                    apm.consultation_duration, --38
                    apm.unit_cost, --39
                    apm.booking_status --40
                    from appointment apm
                    left join user_account ua on apm.doctor_id = ua.id
                    left join user_mngment umng on umng.id = ua.user_mngment_id
                    left join doctor_profile dp on umng.id = dp.user_mngment_id
                    left join meta_dr_consultation_durations mta on mta.code = apm.consultation_duration
                    left join attachment att on att.res_id = apm.id and att.res_model = 'appointment'
                    left join user_account ua2 on apm.patient_id = ua2.id
                    left join user_mngment umng2 on umng2.id = ua2.user_mngment_id
                    left join user_contact uc on uc.user_mngment_id = umng2.id
                    left join user_address uad on uad.user_contact_id = uc.id and is_primary = True
                    left join appointment_review arev on arev.appointment_id = apm.id
                    where apm.id = %s '''

                    record = gs.sql_execute(pool, sql, para_values=[apm_id])
                    if record:
                        # user_account_id = None if record[0][0] in ('False','None','') else record[0][0]
                        title_code = None if record[0][1] in ('False', 'None', '') else record[0][1]
                        first_name = None if record[0][2] in ('False', 'None', '') else record[0][2]
                        middle_name = None if record[0][3] in ('False', 'None', '') else record[0][3]
                        last_name = None if record[0][4] in ('False', 'None', '') else record[0][4]
                        # profile_image_url = None if record[0][5] in ('False','None','') else record[0][5]
                        note = None if record[0][6] in ('False', 'None', '') else record[0][6]
                        state_code = None if record[0][7] in ('False', 'None', '') else record[0][7]
                        schedule_date = int(time.mktime(record[0][8].timetuple())) or None
                        type_code = None if record[0][9] in (None,'') else record[0][9]
                        # final_cost_value = None if record[0][11] in (None,'') else record[0][11]
                        # final_cost_unit = None if record[0][12] in (None,'') else record[0][12]
                        duration = None if record[0][13] in (None, '') else record[0][13]
                        unit = None if record[0][14] in (None, '') else record[0][14]
                        # profile_id = None if record[0][15] in (None, '') else record[0][15]
                        phone = None if record[0][22] in (None, '') else record[0][22]

                        status_call = 'incomplete'
                        status_call_name = 'Incomplete'
                        if state_code in ('successful_finished','free_finished','canceled_by_doctor'):
                            status_call = 'complete'
                            status_call_name  = 'Complete'

                        documents = []
                        for line in record:
                            if line[21]:
                                document = {}
                                document.update({
                                    "id": line[23],
                                    "url": line[19],
                                    "fileName": line[20],
                                    "fileExtension": line[16],
                                    "fileSize": self.general_business.bytesToSize(line[17]),
                                    "latestUpdateTime": int(time.mktime(line[18].timetuple()))
                                })
                                if line[0] == line[21]:
                                    document.update({
                                        "uploadUser": {
                                            "id": res_token[7],
                                            "titleCode": title_code,
                                            "firstName": first_name,
                                            "middleName": middle_name,
                                            "lastName": last_name,
                                            "phoneNumber": phone
                                        }
                                    })
                                else:
                                    sql = '''select ua.user_mngment_id,umg.title,umg.first_name,umg.middle_name,umg.last_name,ua.phone
                                                from user_account ua
                                                left join user_mngment umg on umg.id = ua.user_mngment_id
                                                where ua.id = %s ''' % (line[21])

                                    uinfo = gs.sql_execute(pool, sql)[0]
                                    document.update({
                                        "uploadUser": {
                                            "id": uinfo[0],
                                            "titleCode": uinfo[1],
                                            "firstName": uinfo[2],
                                            "middleName": uinfo[3],
                                            "lastName": uinfo[4],
                                            "phoneNumber": uinfo[5]
                                        }
                                    })
                                documents.append(document)
                        resp.update({
                            'status_code': 200,
                            'resq_body': {
                                'apm_state_real': state_code,
                                'stateCode': _(status_call),
                                'stateCodeName': _(status_call_name),
                                'booking_status':line[40],
                                'scheduleTime': schedule_date,
                                'duration': {
                                    'value': duration,
                                    'unitCode': unit
                                },
                                'note': note,
                                "documentList": documents,
                                'review': self.get_appoinment_review(apm_id),
                                'consultationType': type_code,
                                'patient': {
                                        'primaryAddress': {
                                            "number": None if record[0][28] in ('False', 'None', '') else record[0][28],
                                            "street": None if record[0][29] in ('False', 'None', '') else record[0][29],
                                            "cityCode": None if record[0][30] in ('False', 'None', '') else record[0][30],
                                            "state": None if record[0][31] in ('False', 'None', '') else record[0][31],
                                            "postcode": None if record[0][32] in ('False', 'None', '') else record[0][32],
                                            "countryCode": None if record[0][33] in ('False', 'None', '') else record[0][33]
                                        },
                                        "avatar": None if record[0][27] in ('False', 'None', '') else record[0][27],
		                                "firstName": None if record[0][24] in ('False', 'None', '') else record[0][24],
                                        "middleName": None if record[0][25] in ('False', 'None', '') else record[0][25],
                                        "lastName": None if record[0][26] in ('False', 'None', '') else record[0][26]
                                },
                                "suspendReason": None if record[0][34] in ('False', 'None', '') else record[0][34],
                                "consultation_cost" : None if record[0][37] in (None, '') else record[0][37],
                                "consultation_duration" : None if record[0][38] in (None, '') else record[0][38],
                                "consultation_unit" :  None if record[0][39] in (None, '') else record[0][39]
                            }})

                        sql = '''select ach.session_id, COALESCE(ach.start_time,ach.create_date), ach.end_time,umg.id,umg.first_name,umg.middle_name,umg.last_name,ua.phone
                                    from appointment_call_history ach
                                    inner join appointment apm on apm.id = ach.appointment_id
                                    inner join user_account ua on ua.id = apm.patient_id
                                    inner join user_mngment umg on umg.id = ua.user_mngment_id 
                                    where ach.appointment_id = %s '''

                        records = gs.sql_execute(pool, sql, para_values=[apm_id])
                        callList = []
                        for line in records:
                            start_time = line[1] and int(time.mktime(line[1].timetuple())) or None
                            if line[2] and line[1]:
                                end_time = int(time.mktime(line[2].timetuple()))
                                diff_time = end_time - start_time
                            else:
                                diff_time = 0
                            callList.append({
                                'sessionId': line[0],
                                'action_time': start_time,
                                'callTypeCode': 'incoming',
                                'duration': {
                                    'value': diff_time,
                                    'unitCode': 'second'
                                },
                                'caller': {
                                    'id': line[3],
                                    'firstName': line[4],
                                    'middleName': line[5],
                                    'lastName': line[6],
                                    'phone': line[7]
                                }
                            })
                        resp['resq_body'].update({'callList': callList})

                else:
                    resp = {'status_code': 203,
                            'resq_body': {
                                "code": "expired_token",
                                "title": _("Expired token"),
                                "msg": _("Your login-token is expired, please try to login again")
                            }}
        except Exception as e:
            logger.error('error_doctor_get_appointment_details %s' % str(e))
            client.captureException()
        return resp

    # MP6.6 API for patient to check appointment cancelation conditions
    def get_check_appointment_cancelation(self, data):
        logger.error('get_check_appointment_cancelation %s' % data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body': {
                    "code": "get_check_appointment_cancelation",
                    "title": _("get_check_appointment_cancelation failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }}
        try:
            user_token = data.get('user_token')
            apm_id = data.get('apm_id')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token and apm_id:
                    resp = self.check_action_appointment(apm_id, 'cancel')
                else:
                    resp = {'status_code': 203,
                            'resq_body': {
                                "code": "expired_token",
                                "title": _("Expired token"),
                                "msg": _("Your login-token is expired, please try to login again")
                            }}
        except Exception as e:
            logger.error('error_get_check_appointment_cancelation %s' % str(e))
            client.captureException()
        return resp

    # MP6.7 API for patient to cancel the specificic appointment
    def patient_cancel_the_specificic_appointment(self, data):
        logger.info('patient_cancel_the_specificic_appointment %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "patient_cancel_the_specificic_appointment_failed",
                        "title": _("Patient cancel the specificic appointment failed"),
                        "msg": _("An error occurred. Please try to request again.")
                    }
                }
        try:
            gs = self.general_business
            bt = self.business_transfer
            pool = self.pool_conn
            user_token = data.get('user_token')
            apm_id = data.get('apm_id')

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    sql = '''select doctor_id,--0
                                    patient_id,--1
                                    schedule_date,--2
                                    apm.state,--3
                                    COALESCE(cost,0),--4
                                    COALESCE(consultation_fee,0),--5
                                    unit_cost,--6
                                    COALESCE(reschedule_count,0)--7
                                    
                              from appointment apm
                              where apm.id = %s'''
                    apm_info = gs.sql_execute(pool, sql, para_values=[apm_id])[0]
                    res_check = self.check_action_appointment(apm_id, 'cancel')
                    if res_check and res_check['status_code'] != 200:
                        return res_check
                    elif res_check and res_check['status_code'] == 200:
                        fee_with_margin = apm_info[5] + max(self.min_commission, apm_info[5] * (self.com_ratio / 100))
                        amount_refund = res_check['resq_body']['cancelationCost']['value']
                        amount_discount = res_check['resq_body']['cancelationCost']['discount']
                        dr_amount = (apm_info[5] + (fee_with_margin * 0.3 * apm_info[7]))*0.5
                        if amount_refund != 0 or (amount_refund == 0 and amount_discount != 0):
                            bt.refund_appointment_patient(apm_id, amount_refund=amount_refund)
                            bt.done_appointment(apm_id, amount=dr_amount)
                        elif res_check['resq_body']['cancelationCost']['value'] == 0:
                            bt.refund_appointment_patient(apm_id, amount_refund=apm_info[4])

                        currentTime = datetime.now().strftime('%Y-%m-%d %H:%M')
                        sql_update = '''UPDATE appointment SET state='canceled_by_patient',
                                                               write_date=%s
                                        WHERE id = %s '''
                        gs.sql_execute(pool, sql_update, commit=True,para_values=(currentTime,apm_id))
                        self.appointment_change_state(res_token[1], apm_info[3], 'canceled_by_patient', apm_id)

                        # Push notify to webapp
                        gs.publish_message_mqtt("%s" % apm_info[0], 'patient',
                                                context={'action': 'patient_cancel_appointment',
                                                         'appointmentId': int(apm_id),
                                                         'lang': data.get('lang', 'en')})
                        resp.update({'status_code': 200,
                                     'resq_body': {
                                         "code": "success",
                                         "title": _("Success"),
                                         "msg": _("Cancel appointment successful.")
                                     }
                                     })
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }

        except Exception as e:
            logger.error('patient_cancel_the_specificic_appointment %s' % str(e))
            client.captureException()

        return resp

    # WD 6.21 API for doctor to submit document to specific appointment
    def appointment_document_submit(self, data):
        logger.info('appointment_document_submit %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "appointment_document_submit_failed",
                        "title": _("appointment document submit failed"),
                        "msg": _("An error occurred. Please try to request again.")
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            if not data['file_info']:
                return resp
            if data['file_info']:
                if data['file_info']['url'] == 'wrong_file_type':
                    return {'status_code': 401,
                            'resq_body':
                                {
                                    "title": _("Wrong type of file"),
                                    "msg": _("An error occurred. Please try to setup profile again."),
                                    "code": "wrong_file_type"
                                }
                            }
            user_id = data['user_id']
            apm_id = data['apm_id']
            create_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            if data['file_info']:
                if data['file_info']['url'] == 'wrong_file_type':
                    return {'status_code': 401,
                            'resq_body':
                                {
                                    "title": _("Wrong type of file"),
                                    "msg": _("An error occurred. Please try to setup profile again."),
                                    "code": "wrong_file_type"
                                }
                            }
                sql_insert = '''insert into attachment(create_date,
                                                      write_date,
                                                      create_uid,
                                                      write_uid,
                                                      res_model,
                                                      res_id,
                                                      file_name,
                                                      file_extension,
                                                      file_size,
                                                      url)
                                values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
                                '''
                gs.sql_execute(pool, sql_insert, commit=True, para_values=(create_date,
                                                                           create_date,
                                                                           user_id,
                                                                           user_id,
                                                                           'appointment',
                                                                           apm_id,
                                                                           data['file_info']['file_name'],
                                                                           data['file_info']['file_extension'],
                                                                           data['file_info']['file_size'],
                                                                           data['file_info']['url'],
                                                                           ))

            resp.update({'status_code': 200,
                         'resq_body': {
                             "code": "success",
                             "title": _("Success"),
                             "msg": _("Submit appointment document successful.")
                         }
                         })

        except Exception as e:
            logger.error('error_appointment_document_submit %s' % str(e))
            client.captureException()
        return resp

    # WD6.22 API for doctor to delete specific document of corresponding appointment
    # MP6.11 API for patient to delete specific document of corresponding appointment
    def delete_the_specific_appointment_document(self, data):
        logger.info('delete_the_specific_appointment_document %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body': {
                    "code": "delete_the_specific_appointment_document_failed",
                    "title": _("Delete the specific appointment document failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }}
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            apm_id = data.get('apm_id')
            doc_id = data.get('doc_id')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    sql = '''select create_uid,file_name
                            from attachment
                            where res_model = 'appointment' and res_id = %s and id = %s '''
                    record = gs.sql_execute(pool, sql, para_values=(apm_id, doc_id))
                    if record:
                        if record[0][0] == res_token[1]:
                            delete = '''delete from attachment where id = %s'''
                            gs.sql_execute(pool, delete, commit=True, para_values=[doc_id])
                            resp = {'status_code': 200,
                                    'resq_body': {
                                        "code": "success",
                                        "title": _("Success"),
                                        "msg": _("Delete the document successfully")
                                    }}
                        else:
                            resp = {'status_code': 401,
                                    'resq_body': {
                                        "code": "user_permission",
                                        "title": _("User permission"),
                                        "msg": _("You lack permission to delete the document")
                                    }}
                else:
                    resp = {'status_code': 203,
                            'resq_body': {
                                "code": "expired_token",
                                "title": _("Expired token"),
                                "msg": _("Your login-token is expired, please try to login again")
                            }}

        except Exception as e:
            logger.error('error_delete_the_specific_appointment_document %s' % str(e))
            client.captureException()
        return resp

    # MP6.10 API for patient to submit document to specific appointment
    def patient_submit_document_to_the_specificic_appointment(self, data):
        logger.info('patient_submit_document_to_the_specificic_appointment %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body': {
                    "code": "patient_submit_document_to_the_specificic_appointment_failed",
                    "title": _("Patient submit document to the specificic appointment failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }}
        try:
            gs = self.general_business
            pool = self.pool_conn
            if not data['file_info']:
                return resp
            if data['file_info']:
                if data['file_info']['url'] == 'wrong_file_type':
                    return {'status_code': 401,
                            'resq_body':
                                {
                                    "title": _("Wrong type of file"),
                                    "msg": _("An error occurred. Please try to setup profile again."),
                                    "code": "wrong_file_type"
                                }
                            }
            user_id = data['user_id']
            apm_id = data['apm_id']
            create_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            if data['file_info']:
                if data['file_info']['url'] == 'wrong_file_type':
                    return {'status_code': 401,
                            'resq_body':
                                {
                                    "title": _("Wrong type of file"),
                                    "msg": _("An error occurred. Please try to setup profile again."),
                                    "code": "wrong_file_type"
                                }
                            }
                sql_insert = '''insert into attachment(create_date,
                                                          write_date,
                                                          create_uid,
                                                          write_uid,
                                                          res_model,
                                                          res_id,
                                                          file_name,
                                                          file_extension,
                                                          file_size,
                                                          url)
                                    values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
                                    '''
                gs.sql_execute(pool, sql_insert, commit=True, para_values=(create_date,
                                                                           create_date,
                                                                           user_id,
                                                                           user_id,
                                                                           'appointment',
                                                                           apm_id,
                                                                           data['file_info']['file_name'],
                                                                           data['file_info']['file_extension'],
                                                                           data['file_info']['file_size'],
                                                                           data['file_info']['url'],
                                                                           ))

            resp.update({'status_code': 200,
                         'resq_body': {
                             "code": "success",
                             "title": _("Success"),
                             "msg": _("Submit appointment document successful.")
                         }
                         })

        except Exception as e:
            logger.error('error_patient_submit_document_to_the_specificic_appointment %s' % str(e))
            client.captureException()
        return resp

    # WD6.23 API for doctor to confirm a specific appointment
    def doctor_confirm_the_specific_appointment(self, data):
        logger.info('doctor_confirm_the_specific_appointment %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body': {
                    "code": "doctor_confirm_the_specific_appointment_failed",
                    "title": _("Doctor confirm the specific appointment failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }}
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            apm_id = data.get('apm_id')
            
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    sql = '''select doctor_id, 
                                    patient_id,
                                    state
                        from appointment
                        where id = %s '''

                    records = gs.sql_execute(pool, sql, para_values=[apm_id])
                    if records:
                        if records[0][2] in ['confirmed_by_patient' , 'session_failed']:
                            currentTime = datetime.now().strftime('%Y-%m-%d %H:%M')
                            sql_update = '''update appointment set state = 'confirmed',write_date=%s where id = %s'''
                            gs.sql_execute(pool, sql_update, commit=True, para_values=[currentTime,apm_id])
                            self.appointment_change_state(res_token[1], records[0][2], 'confirmed', apm_id)

                            # Push notify to mobile
                            #gs.publish_message_mqtt("%s" % records[0][1], 'doctor',
                            #                        context={'action': 'doctor_confirm_appointment',
                            #                                 'appointmentId': int(apm_id),
                            #                                 'lang': data.get('lang', 'en')})
                            resp = {
                                'status_code': 200,
                                'resq_body': {
                                    "code": "success",
                                    "title": _("Success"),
                                    "msg": _("You confirmed the specific appointment successfully")
                                }
                            }
                        else:
                            resp = {
                                'status_code': 401,
                                'resq_body': {
                                    "code": "invalid_source_state",
                                    "title": _("Invalid source state"),
                                    "msg": _("You cannot change the state from %s to confirm") % records[0][2]
                                }
                            }
                            return resp
                else:
                    resp = {'status_code': 203,
                            'resq_body': {
                                "code": "expired_token",
                                "title": _("Expired token"),
                                "msg": _("Your login-token is expired, please try to login again")
                            }}

        except Exception as e:
            logger.error('error_doctor_confirm_the_specific_appointment %s' % str(e))
            client.captureException()
        return resp



    def doctor_confirm_the_specific_appointment_hospital(self, data):
        logger.info('doctor_confirm_the_specific_appointment_hospital %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body': {
                    "code": "doctor_confirm_the_specific_appointment_failed",
                    "title": _("Doctor confirm the specific appointment failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }}
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            apm_id = data.get('apm_id')
            
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    sql = '''select doctor_id, 
                                    patient_id,
                                    state
                        from appointment
                        where id = %s '''

                    records = gs.sql_execute(pool, sql, para_values=[apm_id])

                    if records:
                        if records[0][2] in ['confirmed_by_patient' , 'session_failed']:
                            currentTime = datetime.now().strftime('%Y-%m-%d %H:%M')
                            sql_update = '''update appointment set state = 'confirmed',write_date=%s where id = %s'''
                            gs.sql_execute(pool, sql_update, commit=True, para_values=[currentTime,apm_id])
                            self.appointment_change_state(res_token[1], records[0][2], 'confirmed', apm_id)

                            
                            if apm_id:
                                sql_update = '''update appointment set booking_status = 'close' where id = %s'''
                                gs.sql_execute(pool, sql_update, commit=True, para_values=[apm_id])

                            # Push notify to mobile
                            #gs.publish_message_mqtt("%s" % records[0][1], 'doctor',
                            #                        context={'action': 'doctor_confirm_appointment',
                            #                                 'appointmentId': int(apm_id),
                            #                                 'lang': data.get('lang', 'en')})
                            resp = {
                                'status_code': 200,
                                'resq_body': {
                                    "code": "success",
                                    "title": _("Success"),
                                    "msg": _("You confirmed the specific appointment successfully")
                                }
                            }
                        else:
                            resp = {
                                'status_code': 401,
                                'resq_body': {
                                    "code": "invalid_source_state",
                                    "title": _("Invalid source state"),
                                    "msg": _("You cannot change the state from %s to confirm") % records[0][2]
                                }
                            }
                            return resp
                else:
                    resp = {'status_code': 203,
                            'resq_body': {
                                "code": "expired_token",
                                "title": _("Expired token"),
                                "msg": _("Your login-token is expired, please try to login again")
                            }}

        except Exception as e:
            logger.error('error_doctor_confirm_the_specific_appointment %s' % str(e))
            client.captureException()
        return resp
