#!/usr/bin/env python
# -*- coding: utf-8 -*-
import falcon
import json
import sys
import os
import ConfigParser
from medical_record_business import medical_record_business

project_path =  str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
config = ConfigParser.RawConfigParser(allow_no_value=True)
config.read(project_path + '/' + "environment.conf")
sys.path.insert(0, "..")
try:
    from general import general_business
    from aws_management import aws_management
except ImportError as e:
    print('No Import ', e)
AWS_BUCKET = config.get('environment', 's3_bucket')
BUCKET_URL_DEFAULT = AWS_BUCKET + '.' + config.get('environment', 's3_address')

status_code = {
    200: falcon.HTTP_200,
    401: falcon.HTTP_401,
    502: falcon.HTTP_502,
    203: falcon.HTTP_203
}


class medical_record_api(object):
    def __init__(self, signal, erp_connection=False, var_contructor=False):
        self.business = medical_record_business(signal, erp_connection, var_contructor)
        self.pool_conn = erp_connection
        self.signal = signal
        self.aws = aws_management()
        self.general_business = general_business(erp_connection, var_contructor)

    def on_get(self, req, resp, apm_id=None):
        try:
            data = {}
            header = req.headers  # key cua header luon viet hoa
            all_header = self.general_business.get_header_request(header)
            lang = all_header.get('lang')
            _ = self.general_business.get_lang_instance(lang)
            data.update(all_header)
            if self.signal == 'get_appointment_details':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Get appointment details failed"),
                    "msg": _("An error occurred. Please try to request again."),
                    "code": "appointment_details_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'apm_id': apm_id,
                        'lang': lang,
                    })
                    record = self.business.get_appointment_details(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']
                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # WD6.13 API for doctor to get patient's note list of specific medical record field
            elif self.signal == 'medical_record_get':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Get medical record failed"),
                    "msg": _("An error occurred. Please try to request again."),
                    "code": "get_medical_record_failed"
                }
                try:

                    header = req.headers  # key cua header luon viet hoa
                    medicalRecordFieldCode = req.get_param('medicalRecordFieldCode', default=False)
                    requestCount = req.get_param('requestCount', default=10)
                    nextItemId = req.get_param('nextItemId', default=0)
                    patientId = req.get_param('patientId', default=False)

                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        "recordField": medicalRecordFieldCode,
                        "requestCount": requestCount,
                        "nextItemId": nextItemId,
                        "patientId": patientId,
                        'lang': lang,
                    })
                    record = self.business.medical_record_get(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']
                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

        except Exception as e:
            print str(e)

    def on_post(self, req, resp):
        try:
            data = {}
            header = req.headers  # key cua header luon viet hoa
            all_header = self.general_business.get_header_request(header)
            lang = all_header.get('lang')
            _ = self.general_business.get_lang_instance(lang)
            data.update(all_header)
            # WD 6.11
            if self.signal == 'medical_record_addnote':
                resp_status = status_code[401]
                resp_body = {
                    "code": "medical_record_addnote_failed",
                    "title": _("Medical record add note failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = json.loads(req.stream.read(req.content_length or 0) or '{}')
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    data.update(raw_data)
                    record = self.business.medical_record_addnote(data)
                    resp_status = status_code[record['status_code']]
                    resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # WD 6.12
            elif self.signal == 'medical_record_submit':
                resp_status = status_code[401]
                resp_body = {
                    "code": "medical_record_addfile_failed",
                    "title": _("Medical record add file failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    medicalRecordFieldCode = req.get_param('medicalRecordFieldCode', default=False)
                    appointmentId = req.get_param('appointmentId', default=False)
                    attachment = req.get_param('attachment', default=False)
                    note = req.get_param('note', default='')
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'appointmentId': appointmentId,
                        'medicalRecordFieldCode': medicalRecordFieldCode,
                        'note': note,
                        'file_info': {},
                        'lang': lang,
                    })
                    if medicalRecordFieldCode and appointmentId and type(attachment).__name__ in ('instance', 'bool'):
                        # Write image to local
                        if type(attachment).__name__ == 'instance':
                            def get_infomation_file(file):
                                f = file.file
                                file_size = os.fstat(f.fileno()).st_size
                                file_name, file_extension = os.path.splitext(file.filename)
                                return {
                                    'file_size': file_size,
                                    'file_name': file_name,
                                    'file_extension': file_extension.replace('.', '').lower()
                                }

                            user_objs = self.general_business.validate_user_token(header.get("X-USER-TOKEN", False),data.get('client_id'))
                            file_info = get_infomation_file(attachment)
                            data['file_info'] = file_info
                            if user_objs and user_objs[7]:
                                if file_info['file_extension'] in ('jpg', 'png', 'gif', 'jpeg', 'pdf', 'doc', 'docx'):
                                    filename = '%s_%s.%s' % (
                                    appointmentId, file_info['file_name'], file_info['file_extension'])
                                    self.aws.upload_obj(attachment.file, AWS_BUCKET, filename)
                                    data['file_info'].update({'url': 'https://' + BUCKET_URL_DEFAULT + '/' + filename,
                                                              'file_name': filename})
                                else:
                                    data['file_info'].update({'url': 'wrong_file_type'})
                                    # End Write image to local
                        record = self.business.medical_record_submit(data)
                        if record:
                            resp_status = status_code[record['status_code']]
                            resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

        except Exception as e:
            pass
