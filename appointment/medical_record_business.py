#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime
from collections import OrderedDict
import time
import logging
from raven import Client
import ConfigParser
import sys
import os

project_path =  str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
config = ConfigParser.RawConfigParser(allow_no_value=True)
config.read(project_path + '/' + "environment.conf")
sentry_key = config.get('environment', 'sentry_key')
client = Client(sentry_key)
logger = logging.getLogger('drdr_api')
sys.path.insert(0, "..")
try:
    from general import general_business
    from profile.profile_patient_business import profile_patient_business
except ImportError as e:
    print('No Import ', e)


class medical_record_business(object):
    def __init__(self, signal, erp_connection, var_contructor):
        self.var_contructor = var_contructor
        self.pool_conn = erp_connection
        self.general_business = general_business(erp_connection, var_contructor)
        self.signal = signal
        self.vi_ = self.general_business.vi_
        self.en_ = self.general_business.en_

    def medical_record_addnote(self, data=None):
        logger.info('medical_record_addnote %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "medical_record_addnote_failed",
                        "title": _("Medical record addnote failed"),
                        "msg": _("An error occurred. Please try to request again.")
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    appointmentId = data.get('appointmentId', False)
                    medicalRecordFieldCode = data.get('medicalRecordFieldCode', False)
                    note = data.get('note', False)
                    if not appointmentId or not medicalRecordFieldCode or not note:
                        return resp
                    create_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    sql_insert = '''insert into appointment_medical_record(create_date, write_date, appointment_id, code, note)
                                    values (%s,%s,%s,%s,%s)'''
                    gs.sql_execute(pool, sql_insert, commit=True, para_values=(create_date,
                                                                               create_date,
                                                                               appointmentId,
                                                                               medicalRecordFieldCode,
                                                                               note))
                    resp = {'status_code': 200,
                            'resq_body':
                                {
                                    'code': 'success',
                                    'title': 'Success',
                                    'msg': 'Add medical record note successful.'
                                }
                            }
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_medical_record_addnote %s' % str(e))
            client.captureException()
        return resp

    def medical_record_submit(self, data):
        logger.info('medical_record_submit %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "medical_record_submit_failed",
                        "title": _("Medical record submit failed"),
                        "msg": _("An error occurred. Please try to request again.")
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    if not data['note'] and not data['file_info']:
                        return resp
                    if data['file_info']:
                        if data['file_info']['url'] == 'wrong_file_type':
                            return {'status_code': 401,
                                    'resq_body':
                                        {
                                            "title": _("Wrong type of file"),
                                            "msg": _("An error occurred. Please try to setup profile again."),
                                            "code": "wrong_file_type"
                                        }
                                    }
                    user_id = res_token[1]
                    create_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    sql_insert_mr = '''insert into appointment_medical_record(create_date, write_date, appointment_id, code, note)
                                       values (%s,%s,%s,%s,%s) 
                                       returning ID'''
                    medical_record_id = gs.sql_execute(pool, sql_insert_mr, commit=True, para_values=(create_date,
                                                                                                      create_date,
                                                                                                      data[
                                                                                                          'appointmentId'],
                                                                                                      data[
                                                                                                          'medicalRecordFieldCode'],
                                                                                                      data['note']))[0][
                        0]
                    if data['file_info']:
                        if data['file_info']['url'] == 'wrong_file_type':
                            return {'status_code': 401,
                                    'resq_body':
                                        {
                                            "title": _("Wrong type of file"),
                                            "msg": _("An error occurred. Please try to setup profile again."),
                                            "code": "wrong_file_type"
                                        }
                                    }
                        sql_insert = '''insert into attachment(create_date,
                                                              write_date,
                                                              create_uid,
                                                              write_uid,
                                                              res_model,
                                                              res_id,
                                                              description,
                                                              file_name,
                                                              file_extension,
                                                              file_size,
                                                              url)
                                        values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
                                        returning id
                                        '''
                        attachment_id = gs.sql_execute(pool, sql_insert, commit=True, para_values=(create_date,
                                                                                                   create_date,
                                                                                                   user_id,
                                                                                                   user_id,
                                                                                                   'appointment_medical_record',
                                                                                                   medical_record_id,
                                                                                                   data[
                                                                                                       'medicalRecordFieldCode'],
                                                                                                   data['file_info'][
                                                                                                       'file_name'],
                                                                                                   data['file_info'][
                                                                                                       'file_extension'],
                                                                                                   data['file_info'][
                                                                                                       'file_size'],
                                                                                                   data['file_info'][
                                                                                                       'url'],
                                                                                                   ))[0][0]

                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     'code': 'success',
                                     'title': 'Success',
                                     'msg': 'Submit medical record successful.'
                                 }
                                 })

                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }

        except Exception as e:
            logger.error('error_medical_record_submit %s' % str(e))
            client.captureException()
        return resp

    # WD6.13 API for doctor to get patient's note list of specific medical record field
    def medical_record_get(self, data):
        logger.info('medical_record_get %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body': {
                    "title": _("Get medical record failed"),
                    "msg": _("An error occurred. Please try to request again."),
                    "code": "get_medical_record_failed"
                }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            patientId = data.get('patientId')
            requestCount = int(data.get('requestCount'))
            nextItemId = data.get('nextItemId')
            recordField = data.get('recordField')

            if not patientId:
                return resp
            res = {
                'pagination': {
                    "resultCount": 0,
                    "totalCount": 0,
                    "nextItemId": 0
                },
                'list': []
            }
            res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
            start = 0
            end = 0
            if res_token:
                patient_business = profile_patient_business(self.signal, self.pool_conn, self.var_contructor)
                user_id = res_token[1]
                if not nextItemId or nextItemId == 0:
                    select_list_ids = []
                    full_list_ids = patient_business.generate_pagination_list(user_id, 'appointment_medical_record',
                                                                              context=data)
                    start, end = 0, requestCount
                else:
                    sql = '''select list_id 
                            from user_account_pagination
                            where user_account_id = %s
                                and table_pagination = '%s'
                            ''' % (user_id, 'appointment_medical_record')
                    full_list_ids = gs.sql_execute(pool, sql)
                    if full_list_ids:
                        full_list_ids = eval(full_list_ids[0][0])
                        # patient_business.update_pagination_list(user_id, 'doctor_profile', full_list_ids,{'recordField': recordField})
                        start, end = full_list_ids.index(nextItemId), full_list_ids.index(nextItemId) + requestCount
                    else:
                        full_list_ids = patient_business.generate_pagination_list(user_id, 'appointment_medical_record',
                                                                                  context=data)
                        start, end = 0, requestCount
                if not full_list_ids:
                    resp.update({'status_code': 200,
                                 'resq_body': res
                                 })
                    return resp
                end = end if end <= len(full_list_ids) else len(full_list_ids)
                select_list_ids = full_list_ids[start:end]

                res['pagination']['totalCount'] = len(full_list_ids)
                res['pagination']['nextItemId'] = full_list_ids[end] if end < len(full_list_ids) else full_list_ids[
                    end - 1]
                res['pagination']['resultCount'] = len(select_list_ids)

                # get list_specialties
                if len(select_list_ids) > 0:
                    select_list_ids = select_list_ids + [-1, -1]
                    sql = '''select mr.id, --0
                                    mr.note, --1
                                    mr.write_date rm_write_date,--2
                                    um.id as doctor_profile_id,--3
                                    mr.create_uid as doctor_account_id,--4
                                    um.first_name doctor_first_name,--5
                                    um.middle_name doctor_middle_name,--6
                                    um.last_name doctor_last_name,--7
                                    um.title doctor_title,--8
                                    att.url,--9
                                    att.file_name,--10
                                    att.file_size,--11
                                    att.file_extension,--12
                                    att.write_date,--13
                                    um1.id uploader_profile_id,--14
                                    ua1.id uploader_account_id,--15
                                    um1.first_name uploader_first_name,--16
                                    um1.middle_name uploader_middle_name,--17
                                    um1.last_name uploader_last_name,--18
                                    um1.title uploader_title,--19
                                    mr.code--20
                            from appointment_medical_record mr 
                                left join attachment att on att.res_id = mr.id and att.res_model = 'appointment_medical_record'
                                left join user_account ua1 on ua1.id = att.create_uid
                                left join user_account ua on ua.id = mr.create_uid
                                left join user_mngment um on um.id = ua.user_mngment_id
                                left join user_mngment um1 on um1.id = ua1.user_mngment_id
                            where mr.id in %s
                            order by rm_write_date desc''' % str(tuple(select_list_ids))
                    list_medical_record = gs.sql_execute(pool, sql)
                    if list_medical_record:
                        res_list = OrderedDict()
                        for item in list_medical_record:
                            if item[0] not in res_list:
                                res_list.update({item[0]: {
                                    "latestUpdateTime": item[2] and int(time.mktime(item[2].timetuple())) or None,
                                    "appointmentId": None,
                                    "note": item[1],
                                    "note_id": item[0],
                                    "medicalRecordFieldCode": item[20],
                                    "attachments": [],
                                    "uploader": {
                                        "id": item[3],
                                        "userId": item[4],
                                        "firstName": item[5],
                                        "middleName": item[6],
                                        "lastName": item[7],
                                        "titleCode": item[8],
                                    }
                                }})
                            if item[9]:
                                res_list[item[0]]['attachments'].append({"url": item[9],
                                                                          "fileName": item[10],
                                                                          "fileSize": self.general_business.bytesToSize(
                                                                              item[11]),
                                                                          "fileExtension": item[12],
                                                                          "latestUpdateTime": item[13] and int(
                                                                              time.mktime(
                                                                                  item[13].timetuple())) or None,
                                                                          })
                        res['list'] = res_list.values()

                resp.update({'status_code': 200,
                             'resq_body': res
                             })
            else:
                resp.update({'status_code': 203,
                             'resq_body':
                                 {
                                     "code": "expired_token",
                                     "title": _("Expired token"),
                                     "msg": _("Your login-token is expired, please try to login again")
                                 }
                             })
        except Exception as e:
            logger.error('error_medical_record_get %s' % str(e))
            client.captureException()
        return resp
