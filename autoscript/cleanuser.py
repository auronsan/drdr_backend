import ConfigParser

import sys

sys.path.insert(0, "..")
try:
    from general import general_business
    from connection_pool import connection_pool
except ImportError as e:
    print('No Import ',e)


try:
    config = ConfigParser.RawConfigParser(allow_no_value=True)
    config.read('../environment.conf')

    ERP_DB_HOST = config.get('environment', 'ERP_DB_HOST')
    ERP_DB_PORT = config.get('environment', 'ERP_DB_PORT')
    ERP_DB_USER = config.get('environment', 'ERP_DB_USER')
    ERP_DB_PASS = config.get('environment', 'ERP_DB_PASS')
    ERP_DB_NAME = config.get('environment', 'ERP_DB_NAME')

    var_contructor = {
                            'ERP_DB_HOST' : ERP_DB_HOST,
                            'ERP_DB_PORT' : ERP_DB_PORT,
                            'ERP_DB_USER' : ERP_DB_USER,
                            'ERP_DB_PASS' : ERP_DB_PASS,
                            'ERP_DB_NAME' : ERP_DB_NAME
                        }

    conn_obj = connection_pool(ERP_DB_HOST,ERP_DB_NAME,ERP_DB_USER,ERP_DB_PASS,ERP_DB_PORT)
    pool_conn = conn_obj.get_conn()#.connect()
    general_business = general_business(pool_conn, var_contructor)
    sql = ''' DELETE FROM user_mngment WHERE id = ( SELECT user_mngment_id 
                            FROM user_account 
                            WHERE validation_status = 'inactive' AND created_date::date < (CURRENT_DATE - INTERVAL '7 day')::date ) '''
    records = general_business.sql_execute(pool_conn,sql)

    sql = ''' DELETE FROM user_account WHERE validation_status = 'inactive' AND created_date::date < (CURRENT_DATE - INTERVAL '7 day')::date '''
    general_business.sql_execute(pool_conn,sql)

except Exception as e:
        print str(e)