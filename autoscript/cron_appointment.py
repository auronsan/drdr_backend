import sys
sys.path.insert(0, "..")
try:
    from general import general_business
    from appointment.appointment_business import appointment_business
except ImportError as e:
    print('No Import ',e)



class cron_appointment(object):
    def __init__(self, erp_connection, var_contructor):
        self.pool_conn = erp_connection
        self.var_contructor = var_contructor

    def cancel_appointment_live(self):
        pool_conn = self.pool_conn
        gs = general_business(pool_conn, self.var_contructor)
        apm = appointment_business('', pool_conn,self.var_contructor)



        sql = ''' select id,doctor_id from appointment 
                  where type = 'live' 
                    and state = 'confirmed' 
                    and coalesce(is_accepted,False) != True 
                    and write_date < current_timestamp - interval '1 minutes' '''
        list_apm = gs.sql_execute(pool_conn, sql)
        print 'List appointment to cancel %s - %s'%(len(list_apm), list_apm)
        if list_apm:
            for apm_id in list_apm:
                data = {'apm_id': apm_id[0],
                        'reason': 'cron auto cancel apm'}
                context = {'cron_call': True,
                           'user_id': apm_id[1]}
                apm.doctor_cancel_the_specificic_appointment(data,context)

        return True