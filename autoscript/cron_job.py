#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from raven import Client
import ConfigParser
from exceptions import Exception
import os
from datetime import datetime
from dateutil.relativedelta import relativedelta
import time

project_path =  str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
config = ConfigParser.RawConfigParser(allow_no_value=True)
config.read(project_path + '/' + "environment.conf")
sentry_key = config.get('environment', 'sentry_key')
client = Client(sentry_key)
sys.path.insert(0, project_path)
try:
    from general import general_business
    from connection_pool import connection_pool
except ImportError as e:
    print('No Import ',e)

# insert into ir_cron(name,function,interval_type,args,numbercall,nextcall,priority,doall,active,interval_number,model,user_id)
# values
# ('Cancel Appointment live','cancel_appointment_live','minutes','()','-1',now(),'5','False','True','1','cron_appointment',1),
# ('Update activity status user','update_activity_status_user','minutes','()','-1',now(),'5','False','True','15','cron_user_account',1),
# ('Push Upcomming','push_upcomming_appointment','minutes','()','-1',now(),'5','False','True','5','cron_up_comming',1);

#----------------CONFIG-----------------
DEFAULT_SERVER_DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'
DELAY_DURATION = 150
conf = {
            'ERP_DB_HOST' : config.get('environment', 'ERP_DB_HOST'),
            'ERP_DB_PORT' : config.get('environment', 'ERP_DB_PORT'),
            'ERP_DB_USER' : config.get('environment', 'ERP_DB_USER'),
            'ERP_DB_PASS' : config.get('environment', 'ERP_DB_PASS'),
            'ERP_DB_NAME' : config.get('environment', 'ERP_DB_NAME'),
        }
_intervalTypes = {
    'work_days': lambda interval: relativedelta(days=interval),
    'days': lambda interval: relativedelta(days=interval),
    'hours': lambda interval: relativedelta(hours=interval),
    'weeks': lambda interval: relativedelta(days=7*interval),
    'months': lambda interval: relativedelta(months=interval),
    'minutes': lambda interval: relativedelta(minutes=interval),
}
#----------------CONFIG-----------------


def prepare_conn():
    try:
        conn_obj = connection_pool(conf['ERP_DB_HOST'],conf['ERP_DB_NAME'],conf['ERP_DB_USER'],conf['ERP_DB_PASS'],conf['ERP_DB_PORT'])
        pool_conn = conn_obj.get_conn()
        return pool_conn
    except:
        client.captureException()
        return False

pool = prepare_conn()
gs = general_business(pool, conf)

def get_job():
    try:
        sql = """SELECT id,--0
                        name,--1
                        user_id,--2
                        function,--3
                        interval_type,--4
                        args,--5
                        numbercall,--6
                        nextcall,--7
                        priority,--8
                        doall,--9
                        active,--10
                        interval_number,--111
                        model--12 
                      FROM ir_cron
                      WHERE numbercall != 0
                          AND active AND nextcall <= (now())
                      ORDER BY priority"""
        res = gs.sql_execute(pool, sql)
        jobs = []
        if res:
            for line in res:
                jobs.append({
                        'id': line[0],
                        'name': line[1],
                        'user_id': line[2],
                        'function': line[3],
                        'interval_type': line[4],
                        'args': line[5],
                        'numbercall': line[6],
                        'nextcall': line[7],
                        'priority': line[8],
                        'doall': line[9],
                        'active': line[10],
                        'interval_number': line[11],
                        'model': line[12],
                        })


    except Exception as e:
        print e
        client.captureException()
    return jobs

def _acquire_job():
    jobs = get_job()
    print 'Number of jobs: %s - %s'%(len(jobs),jobs)
    for job in jobs:
        try:
            try:
                _process_job(job)
            except Exception as e:
                print 'ERROR - %s'%str(e)

        except Exception as e:
            client.captureException()
            print str(e)

def str2tuple(s):
    return eval('tuple(%s)' % (s or ''))

def _callback(uid, model_name, method_name, args, job_id):
    args = str2tuple(args)
    module = __import__(model_name)
    class_ = getattr(module, model_name)
    instance = class_(pool,conf)
    print hasattr(instance, method_name)
    if hasattr(instance, method_name):
        method = getattr(instance, method_name)
        try:
            method(*args)
        except Exception, e:
            client.captureException()

def _process_job(job):
    try:
        now = datetime.now()
        nextcall = job['nextcall']
        numbercall = job['numbercall']

        ok = False
        while nextcall < now and numbercall:
            if numbercall > 0:
                numbercall -= 1
            if not ok or job['doall']:
                print 'Start func %s'%job['function']
                _callback(job['user_id'], job['model'], job['function'], job['args'], job['id'])
                print 'End func %s' % job['function']
            if numbercall:
                nextcall += _intervalTypes[job['interval_type']](job['interval_number'])
            ok = True
        addsql = ''
        if not numbercall:
            addsql = ', active=False'
        sql = "UPDATE ir_cron SET nextcall=%s, numbercall=%s"+addsql+" WHERE id=%s"
        print 'Set nextcall for job %s'%job['function']
        gs.sql_execute(pool, sql, commit=True, para_values=(nextcall.strftime(DEFAULT_SERVER_DATETIME_FORMAT), numbercall, job['id']))

    finally:
        return True



def main():
    while True:
        try:
            print '--------Start------'
            _acquire_job()
            print '--------End------'
        except Exception as e:
            print e
            client.captureException()
        if not get_job():
            print 'No jobs: sleep %s second'%DELAY_DURATION
            time.sleep(DELAY_DURATION)


if __name__ == '__main__':
    main()
