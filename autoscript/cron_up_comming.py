import sys
from datetime import datetime
import time
import ConfigParser
import os
import json
import paho.mqtt.client as paho
import ssl

project_path =  str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
config = ConfigParser.RawConfigParser(allow_no_value=True)
config.read(project_path + '/' + "environment.conf")
sys.path.insert(0, "..")

try:
    from general import general_business
    from connection_pool import connection_pool
except ImportError as e:
    print('No Import ',e)



class cron_up_comming(object):
    def __init__(self, erp_connection, var_contructor):
        self.pool_conn = erp_connection
        self.var_contructor = var_contructor

    def push_upcomming_appointment(self):
        pool_conn = self.pool_conn
        gs = general_business(pool_conn, self.var_contructor)
        sql = ''' SELECT apm.id, 
                            apm.schedule_date, 
                            apm.doctor_id, 
                            apm.patient_id,
                            ua1.id, --4
                            um1.id, --5
                            um1.first_name, --6
                            um1.middle_name, --7
                            um1.last_name, --8
                            um1.profile_image_url, --9
                            coalesce(ua1.ios_device_token,''), --10
                            coalesce(ua1.droid_device_token,''), --11
                            ua2.id, --12
                            um2.id, --13
                            um2.first_name, --14
                            um2.middle_name, --15
                            um2.last_name, --16
                            um2.profile_image_url, --17
                            coalesce(ua2.ios_device_token,''), --18
                            coalesce(ua2.droid_device_token,'') --19
                            
                  FROM appointment apm
                  INNER JOIN user_account ua1 on ua1.id = apm.patient_id
                  INNER JOIN user_mngment um1 on um1.id = ua1.user_mngment_id
                  INNER JOIN user_account ua2 on ua2.id = apm.doctor_id
                  INNER JOIN user_mngment um2 on um2.id = ua2.user_mngment_id

                  WHERE apm.schedule_date <= current_timestamp - interval '30 minutes' 
						and apm.schedule_date > current_timestamp - interval '35 minutes'
						and apm.state = 'confirmed' '''
        list_apm = gs.sql_execute(pool_conn, sql)
        if list_apm:
            payload = {
                "action": "alarm_upcoming_appointment"
            }

            mqtt_config = json.loads(config.get('environment', 'mqtt_config'))
            mqtt_host = mqtt_config['host']
            mqtt_port = mqtt_config['port']
            authen_ssl = mqtt_config.get('authen_ssl',False)
            client = paho.Client()
            if authen_ssl == True:
                client = paho.Client(transport='websockets')
                client.tls_set(project_path + '/' + 'tokbox/ca_file.crt', cert_reqs=ssl.CERT_NONE)
                client.tls_insecure_set(True)
            client.connect(mqtt_host, mqtt_port, 60)
            client.loop_start()

            for apm in list_apm:
                action_time = int(time.mktime(datetime.now().timetuple()))
                scheduleTime = apm[1].strftime('%H:%M')
                payload.update({
                    "actionTime": action_time, 
                    "appointmentId": apm[0], 
                    "scheduleTime": int(time.mktime(apm[1].timetuple())),
                    "message": '''Reminder: Your appointment will come at %s, please check it'''%(scheduleTime)
                })

                patient_user_id = apm[4]
                patient_profile_id = apm[5]
                patient_first_name = apm[6]
                patient_middle_name = apm[7]
                patient_last_name = apm[8]
                patient_avatar = apm[9]
                patient_ios_device_token = apm[10]
                patient_droid_device_token = apm[11]

                doctor_user_id = apm[12]
                doctor_profile_id = apm[13]
                doctor_first_name = apm[14]
                doctor_middle_name = apm[15]
                doctor_last_name = apm[16]
                doctor_avatar = apm[17]
                doctor_ios_device_token = apm[18]
                doctor_droid_device_token = apm[19]

                patient_payload = {
                    "doctor": {
                        "id": doctor_user_id,
                        "profile_id": doctor_profile_id,
                        "firstName": doctor_first_name,
                        "middleName": doctor_middle_name,
                        "lastName": doctor_last_name,
                        "avatar": doctor_avatar
                    }
                }
                patient_payload.update(payload)

                if patient_ios_device_token and patient_ios_device_token not in ('','False','NULL'):
                    ios_apns = json.loads(config.get('environment', 'ios_apns'))
                    is_sandbox = ios_apns["sandbox"]
                    from apns import APNs, Payload
                    apns = APNs(use_sandbox=is_sandbox, cert_file="tokbox/VOIP.pem", enhanced=True)
                    ios_payload = Payload(alert=json.dumps(patient_payload))
                    apns.gateway_server.send_notification(patient_ios_device_token, ios_payload)

                if patient_droid_device_token and patient_droid_device_token not in ('','False','NULL'):
                    fcm_key = config.get('environment', 'fcm_key')
                    from pyfcm import FCMNotification
                    push_service = FCMNotification(api_key=fcm_key)
                    result = push_service.single_device_data_message(registration_id=patient_droid_device_token, data_message=patient_payload)
                    
                client.publish(patient_user_id, json.dumps(patient_payload), qos=2)
            
            
                doctor_payload = {
                    "patient": {
                        "id": patient_user_id,
                        "profile_id": patient_profile_id,
                        "firstName": patient_first_name,
                        "middleName": patient_middle_name,
                        "lastName": patient_last_name,
                        "avatar": patient_avatar
                    }
                }

                doctor_payload.update(payload)

                if doctor_ios_device_token and doctor_ios_device_token not in ('','False','NULL'):
                    ios_apns = json.loads(config.get('environment', 'ios_apns'))
                    is_sandbox = ios_apns["sandbox"]
                    from apns import APNs, Payload
                    apns = APNs(use_sandbox=is_sandbox, cert_file="DOCTORAPP.pem", enhanced=True)
                    ios_payload = Payload(alert=json.dumps(doctor_payload))
                    apns.gateway_server.send_notification(doctor_ios_device_token, ios_payload)

                
                if doctor_droid_device_token and doctor_droid_device_token not in ('','False','NULL'):
                    fcm_key = config.get('environment', 'fcm_key')
                    from pyfcm import FCMNotification
                    push_service = FCMNotification(api_key=fcm_key)
                    result = push_service.single_device_data_message(registration_id=doctor_droid_device_token, data_message=doctor_payload)
                    
                client.publish(patient_user_id, json.dumps(doctor_payload), qos=2)
            
            client.loop_stop()
            client.disconnect()

        return True
