
# -*- coding: utf-8 -*-
import sys
from raven import Client
import ConfigParser
from exceptions import Exception
import os
from datetime import datetime
from dateutil.relativedelta import relativedelta
import time
import paho.mqtt.client as paho

import sys
import json
import os

import logging
project_path =  str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
config = ConfigParser.RawConfigParser(allow_no_value=True)
config.read(project_path + '/' + "environment.conf")
sentry_key = config.get('environment', 'sentry_key')
client = Client(sentry_key)
sys.path.insert(0, project_path)
logging.basicConfig(filename='cron_api.log',level=logging.DEBUG)
try:
    from general import general_business
    from connection_pool import connection_pool
except ImportError as e:
    print('No Import ',e)

mqtt_config = json.loads(config.get('environment', 'mqtt_config'))

mqtt_host = mqtt_config['host']
mqtt_port = mqtt_config['port']
authen_ssl = mqtt_config.get('authen_ssl',False)
# insert into ir_cron(name,function,interval_type,args,numbercall,nextcall,priority,doall,active,interval_number,model,user_id)
# values
# ('Cancel Appointment live','cancel_appointment_live','minutes','()','-1',now(),'5','False','True','1','cron_appointment',1),
# ('Update activity status user','update_activity_status_user','minutes','()','-1',now(),'5','False','True','15','cron_user_account',1),
# ('Push Upcomming','push_upcomming_appointment','minutes','()','-1',now(),'5','False','True','5','cron_up_comming',1);

#----------------CONFIG-----------------
DEFAULT_SERVER_DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'
DELAY_DURATION = 150
conf = {
            'ERP_DB_HOST' : config.get('environment', 'ERP_DB_HOST'),
            'ERP_DB_PORT' : config.get('environment', 'ERP_DB_PORT'),
            'ERP_DB_USER' : config.get('environment', 'ERP_DB_USER'),
            'ERP_DB_PASS' : config.get('environment', 'ERP_DB_PASS'),
            'ERP_DB_NAME' : config.get('environment', 'ERP_DB_NAME'),
        }
#----------------CONFIG-----------------


def prepare_conn():
    try:
        conn_obj = connection_pool(conf['ERP_DB_HOST'],conf['ERP_DB_NAME'],conf['ERP_DB_USER'],conf['ERP_DB_PASS'],conf['ERP_DB_PORT'])
        pool_conn = conn_obj.get_conn()
        return pool_conn
    except:
        #client.captureException()
        return False

pool = prepare_conn()
gs = general_business(pool, conf)

sql = ''' update user_account set activity_status= 'online' where activity_status = 'busy' returning id'''

sqlOnline = gs.sql_execute(pool, sql,commit=True)
if sqlOnline:
    logging.debug('updated online %s' %sqlOnline[0])
        
try:
    sql_doctor = ''' select ua.id,ddl.spoken_language,ua.ios_device_token, ua.droid_device_token from user_account as ua INNER JOIN doctor_profile dp ON dp.user_mngment_id = ua.user_mngment_id INNER JOIN dr_doctorprofile_languages ddl ON dp.id = ddl.doctor_profile_id where activity_status = 'online' and CURRENT_TIMESTAMP + interval '1 minute' > checkout_time and checkout_time + interval '1 minute' > CURRENT_TIMESTAMP '''
    sqlNotifD = gs.sql_execute(pool, sql_doctor)
    if True == False:
        for dr in sqlNotifD:
            logging.info('notification checkout time send to doctor %s', dr)
            mqtt_body = {
                "user_id": int(dr[0])
            }
            ios_device_token = dr[2]
            droid_device_token = dr[3]
            mqtt_body = {
                "to": {
                    "user_id": dr[0],
                    "alert": "checkout TIme"
                }
            }
            if ios_device_token and ios_device_token not in ('','False','NULL'):
                ios_apns = json.loads(config.get('environment', 'ios_apns'))
                is_sandbox = ios_apns["sandbox"]
                # if ios_apns == "development":
                #     is_sandbox = True
                from apns import APNs, Payload
                apns = APNs(use_sandbox=is_sandbox, cert_file="tokbox/VOIP.pem", enhanced=True)
                mqtt_body["to"].update({ 
                    "alert": "Checkout Time"
                })
                payload = Payload(alert=json.dumps(mqtt_body['to']))
                
                logging.info('notification checkout time send to doctor IOS %s', payload)
                apns.gateway_server.send_notification(ios_device_token, payload)
            print 'droid_device_token ' ,droid_device_token
            if droid_device_token and droid_device_token not in ('','False','NULL'):
                fcm_key = config.get('environment', 'fcm_key')
                from pyfcm import FCMNotification
                push_service = FCMNotification(api_key=fcm_key)
                result = push_service.single_device_data_message(registration_id=droid_device_token, data_message=mqtt_body['to'])
                logging.info('notification checkout time send to doctor Android %s', result)
                
                print 'result ',result
except Exception as e:
    logging.error('error_send %s' % str(e))
        
