
import sys
from raven import Client
import ConfigParser
from exceptions import Exception
import os
from datetime import datetime
from dateutil.relativedelta import relativedelta
import time
import paho.mqtt.client as paho

import sys
import json
import os

import logging
project_path =  str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
config = ConfigParser.RawConfigParser(allow_no_value=True)
config.read(project_path + '/' + "environment.conf")
sentry_key = config.get('environment', 'sentry_key')
client = Client(sentry_key)
sys.path.insert(0, project_path)
logging.basicConfig(filename='cron_api.log',level=logging.DEBUG)
try:
    from general import general_business
    from connection_pool import connection_pool
except ImportError as e:
    print('No Import ',e)

mqtt_config = json.loads(config.get('environment', 'mqtt_config'))

mqtt_host = mqtt_config['host']
mqtt_port = mqtt_config['port']
authen_ssl = mqtt_config.get('authen_ssl',False)
# insert into ir_cron(name,function,interval_type,args,numbercall,nextcall,priority,doall,active,interval_number,model,user_id)
# values
# ('Cancel Appointment live','cancel_appointment_live','minutes','()','-1',now(),'5','False','True','1','cron_appointment',1),
# ('Update activity status user','update_activity_status_user','minutes','()','-1',now(),'5','False','True','15','cron_user_account',1),
# ('Push Upcomming','push_upcomming_appointment','minutes','()','-1',now(),'5','False','True','5','cron_up_comming',1);

#----------------CONFIG-----------------
DEFAULT_SERVER_DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'
DELAY_DURATION = 150
conf = {
            'ERP_DB_HOST' : config.get('environment', 'ERP_DB_HOST'),
            'ERP_DB_PORT' : config.get('environment', 'ERP_DB_PORT'),
            'ERP_DB_USER' : config.get('environment', 'ERP_DB_USER'),
            'ERP_DB_PASS' : config.get('environment', 'ERP_DB_PASS'),
            'ERP_DB_NAME' : config.get('environment', 'ERP_DB_NAME'),
        }
#----------------CONFIG-----------------


def prepare_conn():
    try:
        conn_obj = connection_pool(conf['ERP_DB_HOST'],conf['ERP_DB_NAME'],conf['ERP_DB_USER'],conf['ERP_DB_PASS'],conf['ERP_DB_PORT'])
        pool_conn = conn_obj.get_conn()
        return pool_conn
    except:
        #client.captureException()
        return False

pool = prepare_conn()
gs = general_business(pool, conf)
sql = ''' UPDATE user_account 
            set activity_status = 'offline' 
            WHERE activity_status = 'online' and checkout_time <= current_timestamp '''
gs.sql_execute(pool, sql, commit=True)