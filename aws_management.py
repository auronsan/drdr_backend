#!/usr/bin/env python
# -*- coding: utf-8 -*-
import boto3


class aws_management(object):
    def __init__(self):
        self.s3 = boto3.resource('s3')
        self.client = boto3.client('s3')

    def upload_obj(self, filebinary, bucket, keyname):
        self.client.upload_fileobj(filebinary, bucket, keyname, ExtraArgs={'ACL': 'public-read'})

    def delete_obj(self, bucket, keyname):
        self.client.delete_object(bucket, keyname)
