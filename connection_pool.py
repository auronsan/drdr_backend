import sqlalchemy.pool as pool
import psycopg2

class Singleton(object):
    _instance = None

    def __new__(class_, *args, **kwargs):
        if not isinstance(class_._instance, class_):
            class_._instance = object.__new__(class_, *args, **kwargs)
        return class_._instance


class connection_pool(Singleton):
    def __init__(self, ERP_DB_HOST, ERP_DB_NAME, ERP_DB_USER, ERP_DB_PASS, ERP_DB_PORT):
        self.host = ERP_DB_HOST
        self.db = ERP_DB_NAME
        self.port = ERP_DB_PORT
        self.user = ERP_DB_USER
        self.dbpass = ERP_DB_PASS

    def create_conn(self):
        return psycopg2.connect("host='%s' dbname='%s' user='%s' password='%s' port='%s'" % (
            self.host, self.db, self.user, self.dbpass, self.port))

    def get_conn(self):
        mypool = pool.QueuePool(self.create_conn, max_overflow=10, pool_size=70)
        return mypool
