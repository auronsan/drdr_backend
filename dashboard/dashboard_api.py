import falcon
import json
import sys
import ConfigParser
from dashboard_business import dashboard_business
import os

project_path =  str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
config = ConfigParser.RawConfigParser(allow_no_value=True)
config.read(project_path + '/' + "environment.conf")
sys.path.insert(0, "..")
try:
    from general import general_business
    from aws_management import aws_management
except ImportError as e:
    print('No Import ', e)

AWS_BUCKET = config.get('environment', 's3_bucket')
BUCKET_URL_DEFAULT = AWS_BUCKET + '.' + config.get('environment', 's3_address')

status_code = {
    200: falcon.HTTP_200,
    401: falcon.HTTP_401,
    502: falcon.HTTP_502,
    203: falcon.HTTP_203
}


class dashboard_api(object):
    def __init__(self, signal, erp_connection=False, var_contructor=False):
        self.business = dashboard_business(signal, erp_connection, var_contructor)
        self.pool_conn = erp_connection

        if var_contructor:
            self.ERP_DB_HOST = var_contructor['ERP_DB_HOST']
            self.ERP_DB_PORT = var_contructor['ERP_DB_PORT']
            self.ERP_DB_USER = var_contructor['ERP_DB_USER']
            self.ERP_DB_PASS = var_contructor['ERP_DB_PASS']
            self.ERP_DB_NAME = var_contructor['ERP_DB_NAME']

        self.signal = signal
        self.general_business = general_business(erp_connection, var_contructor)

    def on_get(self, req, resp):
        try:
            data = {}
            header = req.headers  # key cua header luon viet hoa
            all_header = self.general_business.get_header_request(header)
            lang = all_header.get('lang')
            _ = self.general_business.get_lang_instance(lang)
            data.update(all_header)
            
            # WD7.1 API for getting dashboard summary
            if self.signal == 'dashboard_get_summary':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Dashboard get summary failed"),
                    "msg": _("An error occurred. Please try to get profile again."),
                    "code": "dashboard_get_summary_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    record = self.business.dashboard_get_summary(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            
            # WD7.2 API for getting appointment list of specific type
            elif self.signal == 'dashboard_get_appointment_list':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Dashboard get appointment list failed"),
                    "msg": _("An error occurred. Please try to get profile again."),
                    "code": "dashboard_get_appointment_list_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    requestCount = req.get_param('requestCount', default=10)
                    nextItemId = req.get_param('nextItemId', default=0)
                    appointmentType = req.get_param('type', default=False)
                    
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'requestCount':requestCount,
                        'nextItemId':nextItemId,
                        'appointmentType':appointmentType,
                        'lang': lang,
                    })
                    record = self.business.dashboard_get_appointment_list(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # WD7.3 API for getting top patient list
            elif self.signal == 'dashboard_get_top_patient_list':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Dashboard get top patient list failed"),
                    "msg": _("An error occurred. Please try to get profile again."),
                    "code": "dashboard_get_top_patient_list_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    requestCount = req.get_param('requestCount', default=10)
                    nextItemId = req.get_param('nextItemId', default=0)
                    
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'requestCount':requestCount,
                        'nextItemId':nextItemId,
                        'lang': lang,
                    })
                    record = self.business.dashboard_get_top_patient_list(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            
        except Exception as e:
            print str(e)

    def on_post(self, req, resp, setting_type=None):
        try:
            data = {}
            header = req.headers  # key cua header luon viet hoa
            all_header = self.general_business.get_header_request(header)
            lang = all_header.get('lang')
            _ = self.general_business.get_lang_instance(lang)
            data.update(all_header)
            
            # WD7.4 API for getting availability status of doctor by month
            if self.signal == 'dashboard_get_doctor_availability_status_by_month':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Dashboard get doctor availability status by month failed"),
                    "msg": _("An error occurred. Please try to get profile again."),
                    "code": "dashboard_get_doctor_availability_status_by_month_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    data.update(raw_data)

                    record = self.business.dashboard_get_doctor_availability_status_by_month(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
        except Exception as e:
            print str(e)
