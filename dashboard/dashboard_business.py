# -*- coding: utf-8 -*-
from datetime import datetime
import time
import logging
from raven import Client
import ConfigParser
import sys
import random
import calendar
import numpy as np
import dateutil.relativedelta as relativedelta
import dateutil.rrule as rrule
from datetime import timedelta
import os

project_path =  str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
config = ConfigParser.RawConfigParser(allow_no_value=True)
config.read(project_path + '/' + "environment.conf")
sentry_key = config.get('environment', 'sentry_key')
client = Client(sentry_key)
logger = logging.getLogger('drdr_api')
sys.path.insert(0, "..")
try:
    from general import general_business
except ImportError as e:
    print('No Import ', e)


class dashboard_business(object):
    def __init__(self, signal, erp_connection, var_contructor):
        self.pool_conn = erp_connection
        self.var_contructor = var_contructor
        self.signal = signal
        self.ERP_DB_HOST = var_contructor['ERP_DB_HOST']
        self.ERP_DB_PORT = var_contructor['ERP_DB_PORT']
        self.ERP_DB_USER = var_contructor['ERP_DB_USER']
        self.ERP_DB_PASS = var_contructor['ERP_DB_PASS']
        self.ERP_DB_NAME = var_contructor['ERP_DB_NAME']
        self.general_business = general_business(erp_connection, var_contructor)
        self.vi_ = self.general_business.vi_
        self.en_ = self.general_business.en_

    # WD7.1 API for getting dashboard summary
    def dashboard_get_summary(self, data):
        logger.info('dashboard_get_summary %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Dashboard get summary failed"),
                        "msg": _("An error occurred. Please try to get profile again."),
                        "code": "dashboard_get_summary_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    sql = '''select coalesce(sum_rating_point,0)/coalesce(count_rating_point,1)
                                from doctor_profile
                                where user_mngment_id = %s'''
                    
                    avgRatingPoint = gs.sql_execute(pool, sql, para_values=[res_token[7]])[0][0]

                    sql = '''select state, count(*)
                            from appointment
                            where doctor_id = %s
                              and ((type != 'live' and state='confirmed')
                              or (type = 'live' and state='confirmed' and COALESCE(is_accepted,False)=True)
                              or (state in ('rescheduled_by_patient', 'successful_finished','free_finished','canceled_by_patient','canceled_by_doctor')))
                            group by 1'''
                    
                    records = gs.sql_execute(pool, sql, para_values=[res_token[1]])
                    previousTotalCount,upcomingTotalCount,unconfirmedTotalCount = 0,0,0
            
                    for line in records:
                        if line[0] in ('confirmed_by_patient'):
                            upcomingTotalCount = upcomingTotalCount + line[1]
                            unconfirmedTotalCount = unconfirmedTotalCount + line[1]
                        elif line[0] in ('confirmed','processing','suspended'):
                            upcomingTotalCount = upcomingTotalCount + line[1]
                        else:
                            previousTotalCount = previousTotalCount + line[1]

                    sql = '''select count(distinct patient_id)
                            from appointment
                            where doctor_id = %s'''
                    
                    records = gs.sql_execute(pool, sql, para_values=[res_token[1]])
                    patientTotalCount = 0
                    if records:
                        patientTotalCount = records[0][0]


                    resp.update({'status_code': 200,
                                    'resq_body': {
                                        'doctor': {
                                                'firstName': res_token[8],
                                                'middleName': res_token[9],
                                                'lastName': res_token[10],
                                                'avatarURL': res_token[11],
                                                'avgRatingPoint': avgRatingPoint,
                                                'degreeTitle': res_token[15]
                                        },
                                        'previousTotalCount':previousTotalCount,
                                        'upcomingTotalCount':upcomingTotalCount,
                                        'unconfirmedTotalCount':unconfirmedTotalCount,
                                        'patientTotalCount':patientTotalCount
                                    }
                                    })

                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }

        except Exception as e:
            logger.error('error_dashboard_get_summary %s' % str(e))
            client.captureException()
        return resp
    
    # WD7.2 API for getting appointment list of specific type
    def dashboard_get_appointment_list(self, data):
        logger.info('dashboard_get_appointment_list %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Dashboard get appointment list failed"),
                        "msg": _("An error occurred. Please try to get profile again."),
                        "code": "dashboard_get_appointment_list_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            requestCount = int(data.get('requestCount'))
            nextItemId = int(data.get('nextItemId'))
            appointmentType = data.get('appointmentType')
            
            if not appointmentType:
                return resp

            res = {
                'pagination': {
                    "resultCount": 0,
                    "totalCount": 0,
                    "nextItemId": 0
                },
                'list': [],
            }

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    start, end = 0, 0
                    user_id = res_token[1]
                    if not nextItemId or nextItemId == 0:
                        full_list_ids = self.generate_pagination_list(user_id,'dashboard_appointment',context={'appointmentType': appointmentType})
                        start = 0
                    else:
                        sql = '''select list_id 
                                from user_account_pagination
                                where user_account_id = %s
                                    and table_pagination = '%s'
                                ''' % (user_id, 'dashboard_appointment')
                        full_list_ids = gs.sql_execute(pool, sql)
                        if full_list_ids:
                            full_list_ids = eval(full_list_ids[0][0])
                            start = full_list_ids.index(nextItemId)
                        else:
                            full_list_ids = self.generate_pagination_list(user_id,'dashboard_appointment',context={'appointmentType': appointmentType})
                            start = 0

                    if not full_list_ids:
                        resp.update({'status_code': 200,
                                     'resq_body': res
                                     })
                        return resp

                    if (start + requestCount) <= len(full_list_ids):
                        end = start + requestCount
                    else:
                        end = len(full_list_ids)

                    select_list_ids = full_list_ids[start:end]
                    res['pagination']['totalCount'] = len(full_list_ids)
                    res['pagination']['nextItemId'] = full_list_ids[end] if end < len(full_list_ids) else full_list_ids[end - 1]
                    res['pagination']['resultCount'] = len(select_list_ids)

                    # get list_specialties
                    if len(select_list_ids) > 0:
                        select_list_ids = select_list_ids + [-1, -1]
                        
                        orderBy = "asc"
                        if appointmentType == 'upcoming':
                            orderBy = "asc"
                        elif appointmentType == 'previous':
                            orderBy = "desc"
                        elif appointmentType == 'unconfirmed':
                            orderBy = "desc"
                        elif appointmentType == 'live':
                            orderBy = "desc"
                                          
                        sql = '''select apm.id, --0
                                        apm.schedule_date, --1
                                        apm.state, --2
                                        umng.id, --3
                                        umng.first_name, --4
                                        umng.middle_name, --5
                                        umng.last_name, --6
                                        umng.profile_image_url,--7
                                        apm.consultation_note, --8
                                        mta.value, --9
                                        mta.unit, --10
                                        umng.gender, --11
                                        coalesce(uad.number,''),--12
                                        coalesce(uad.street,''),--13
                                        coalesce(uad.city,''),--14
                                        coalesce(uad.state,''),--15
                                        coalesce(uad.postcode,''),--16
                                        coalesce(uad.country,''), --17
                                        ua.id --18
                                    from appointment apm
                                    left join user_account ua on apm.patient_id = ua.id
                                    left join user_mngment umng on  umng.id = ua.user_mngment_id
                                    left join meta_dr_consultation_durations mta on mta.code = apm.consultation_duration
                                    left join user_contact uc on uc.user_mngment_id = umng.id
                                    left join user_address uad on uad.user_contact_id = uc.id and is_primary = True
                                    where apm.id in %s
                                    order by apm.schedule_date %s ''' % (str(tuple(select_list_ids)), orderBy)
                        if appointmentType == 'live':
                            sql = '''select apm.id, --0
                                        apm.schedule_date, --1
                                        apm.state, --2
                                        umng.id, --3
                                        umng.first_name, --4
                                        umng.middle_name, --5
                                        umng.last_name, --6
                                        umng.profile_image_url,--7
                                        apm.consultation_note, --8
                                        mta.value, --9
                                        mta.unit, --10
                                        umng.gender, --11
                                        coalesce(uad.number,''),--12
                                        coalesce(uad.street,''),--13
                                        coalesce(uad.city,''),--14
                                        coalesce(uad.state,''),--15
                                        coalesce(uad.postcode,''),--16
                                        coalesce(uad.country,''), --17
                                        ua.id, --18
                                        ach.state as call_history_state, --19
                                        ach.create_date as call_history_time, --20
                                        ach.start_time as call_start_time,  --21
                                        ach.end_time as call_end_time,  --22
                                        apm.cost, --23
                                        apm.consultation_duration, --24
                                        apm.unit_cost, --25
                                        apm.booking_status --26
                                    from appointment apm
                                    left join user_account ua on apm.patient_id = ua.id
                                    left join user_mngment umng on  umng.id = ua.user_mngment_id
                                    left join meta_dr_consultation_durations mta on mta.code = apm.consultation_duration
                                    left join user_contact uc on uc.user_mngment_id = umng.id
                                    left join user_address uad on uad.user_contact_id = uc.id and is_primary = True
                                    inner join appointment_call_history ach on ach.appointment_id = apm.id
                                    where ach.id in %s
                                    order by apm.schedule_date %s ''' % (str(tuple(select_list_ids)), orderBy)
                     
                        records = gs.sql_execute(pool, sql)
                        apms = []
                        if records:
                            for line in records:
                                if appointmentType == 'live':

                                    status_call = 'incomplete'
                                    status_call_name  = 'Incomplete'
                                    if line[2] in ('successful_finished','free_finished','canceled_by_doctor'):
                                        status_call = 'complete'
                                        status_call_name  = 'Complete'
                                
                                    
                                    #if line[21] and line[22]:
                                    #    status_call = 'accept_call'
                                    #elif line[22] or line[2] == 'denied':
                                    #    status_call = 'denied_call'
                                    # 
                                    #if status_call == 'missed_call' :
                                    #    continue
                                    if not line[26] in ('False', 'None', '','null',None) : 
                                        apms.append({
                                        'patient': {
                                            'id': line[3],
                                            'firstName': None if line[4] in ('False', 'None', '') else line[4],
                                            'middleName': None if line[5] in ('False', 'None', '') else line[5],
                                            'lastName': None if line[6] in ('False', 'None', '') else line[6],
                                            'avatar': line[7],
                                            'userId': line[18]
                                        },
                                        'id': line[0],
                                        'stateCode': status_call,
                                        'stateCodeName': _(status_call_name),
                                        'booking_status':line[26],
                                        'scheduleTime': int(time.mktime(line[1].timetuple())) or None,
                                        'duration': {
                                            'value': line[9],
                                            'unitCode': line[10]
                                        },
                                        'consultation_fee': line[23],
                                        'consultation_duration': line[24],
                                        'consultation_unit': line[25],
                                        'money': line[23],
                                        'note': line[8],
                                        'genderCode':line[11],
                                        'call_history_state':line[19],
                                        'call_history_time':int(time.mktime(line[20].timetuple())) or None,
                                        'call_start_time': 'null' if line[21] is None else int(time.mktime(line[20].timetuple())),
                                        'call_end_time': 'null' if line[22] is None else int(time.mktime(line[20].timetuple())),
                                        'primaryAddress': {
                                                "number": None if line[12] in ('False', 'None', '') else line[12],
                                                "street": None if line[13] in ('False', 'None', '') else line[13],
                                                "cityCode": None if line[14] in ('False', 'None', '') else line[14],
                                                "state": None if line[15] in ('False', 'None', '') else line[15],
                                                "postcode": None if line[16] in ('False', 'None', '') else line[16],
                                                "countryCode": None if line[17] in ('False', 'None', '') else line[17]
                                            }
                                        })
                                else:
                                    apms.append({
                                        'patient': {
                                            'id': line[3],
                                            'firstName': None if line[4] in ('False', 'None', '') else line[4],
                                            'middleName': None if line[5] in ('False', 'None', '') else line[5],
                                            'lastName': None if line[6] in ('False', 'None', '') else line[6],
                                            'avatar': line[7],
                                            'userId': line[18]
                                        },
                                        'id': line[0],
                                        'stateCode': line[2],
                                        'scheduleTime': int(time.mktime(line[1].timetuple())) or None,
                                        'duration': {
                                            'value': line[9],
                                            'unitCode': line[10]
                                        },
                                        'note': line[8],
                                        'genderCode':line[11],
                                        'primaryAddress': {
                                                "number": None if line[12] in ('False', 'None', '') else line[12],
                                                "street": None if line[13] in ('False', 'None', '') else line[13],
                                                "cityCode": None if line[14] in ('False', 'None', '') else line[14],
                                                "state": None if line[15] in ('False', 'None', '') else line[15],
                                                "postcode": None if line[16] in ('False', 'None', '') else line[16],
                                                "countryCode": None if line[17] in ('False', 'None', '') else line[17]
                                            }
                                    })

                            res['list'] = apms

                    resp.update({'status_code': 200,
                                 'resq_body': res
                                 })

                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }

        except Exception as e:
            logger.error('error_dashboard_get_appointment_list %s' % str(e))
            client.captureException()
        return resp
    
    # WD7.3 API for getting top patient list
    def dashboard_get_top_patient_list(self, data):
        logger.info('dashboard_get_top_patient_list %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Dashboard get top patient list failed"),
                        "msg": _("An error occurred. Please try to get profile again."),
                        "code": "dashboard_get_top_patient_list_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            requestCount = int(data.get('requestCount'))
            nextItemId = int(data.get('nextItemId'))
            
            res = {
                'pagination': {
                    "resultCount": 0,
                    "totalCount": 0,
                    "nextItemId": 0
                },
                'list': [],
            }

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    start, end = 0, 0
                    user_id = res_token[1]
                    if not nextItemId or nextItemId == 0:
                        full_list_ids = self.generate_pagination_list(user_id,'dashboard_patient',context={'dashboard_patient': True})
                        start = 0
                    else:
                        sql = '''select list_id 
                                from user_account_pagination
                                where user_account_id = %s
                                    and table_pagination = '%s'
                                ''' % (user_id, 'dashboard_patient')
                        full_list_ids = gs.sql_execute(pool, sql)
                        if full_list_ids:
                            full_list_ids = eval(full_list_ids[0][0])
                            start = full_list_ids.index(nextItemId)
                        else:
                            full_list_ids = self.generate_pagination_list(user_id,'dashboard_patient',context={'dashboard_patient': True})
                            start = 0

                    if not full_list_ids:
                        resp.update({'status_code': 200,
                                     'resq_body': res
                                     })
                        return resp

                    if (start + requestCount) <= len(full_list_ids):
                        end = start + requestCount
                    else:
                        end = len(full_list_ids)

                    select_list_ids = full_list_ids[start:end]
                    res['pagination']['totalCount'] = len(full_list_ids)
                    res['pagination']['nextItemId'] = full_list_ids[end][0] if end < len(full_list_ids) else full_list_ids[end - 1][0]
                    res['pagination']['resultCount'] = len(select_list_ids)

                    # get list_specialties
                    if len(select_list_ids) > 0:
                        patient_select_list_ids = {}
                        list_ids = [-1,-1]
                        for item in select_list_ids:
                            list_ids.append(int(item[0]))
                            patient_select_list_ids.update({str(item[0]): {
                                'visitedCount': item[1]
                            }})

                        sql = '''select umng.id, --0
                                        umng.first_name, --1
                                        umng.middle_name, --2
                                        umng.last_name, --3
                                        umng.profile_image_url, --4
                                        ua.id, --5
                                        coalesce(uad.number,''),--6
                                        coalesce(uad.street,''),--7
                                        coalesce(uad.city,''),--8
                                        coalesce(uad.state,''),--9
                                        coalesce(uad.postcode,''),--10
                                        coalesce(uad.country,'')--11
                                    from user_mngment umng
                                    left join user_account ua on umng.id = ua.user_mngment_id
                                    left join user_contact uc on uc.user_mngment_id = umng.id
                                    left join user_address uad on uad.user_contact_id = uc.id and is_primary = True
                                    where ua.id in %s ''' % (str(tuple(list_ids)))

                        records = gs.sql_execute(pool, sql)
                        patients = []
                        if records:
                            for line in records:
                                patients.append({
                                    'id': line[0],
                                    'firstName': None if line[1] in ('False', 'None', '') else line[1],
                                    'middleName': None if line[2] in ('False', 'None', '') else line[2],
                                    'lastName': None if line[3] in ('False', 'None', '') else line[3],
                                    'avatar': line[4],
                                    'visitedCount':patient_select_list_ids[str(line[5])]['visitedCount'],
                                    'primaryAddress': {
                                            "number": None if line[6] in ('False', 'None', '') else line[6],
                                            "street": None if line[7] in ('False', 'None', '') else line[7],
                                            "cityCode": None if line[8] in ('False', 'None', '') else line[8],
                                            "state": None if line[9] in ('False', 'None', '') else line[9],
                                            "postcode": None if line[10] in ('False', 'None', '') else line[10],
                                            "countryCode": None if line[11] in ('False', 'None', '') else line[11]
                                        }
                                })

                            res['list'] = patients

                    resp.update({'status_code': 200,
                                 'resq_body': res
                                 })

                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }

        except Exception as e:
            logger.error('error_dashboard_get_top_patient_list %s' % str(e))
            client.captureException()
        return resp
    
    # WD7.4 API for getting availability status of doctor by month
    def dashboard_get_doctor_availability_status_by_month(self, data):
        logger.info('dashboard_get_doctor_availability_status_by_month %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Dashboard get doctor availability status by month failed"),
                        "msg": _("An error occurred. Please try to get profile again."),
                        "code": "dashboard_get_doctor_availability_status_by_month_failed"
                    }
                }
        try:
            month = data.get('month', None)
            year = data.get('year', None)

            if month == None or year == None:
                return resp

            if (year == datetime.now().year and month < datetime.now().month) or (year < datetime.now().year):
                dayStatusList = []
                for i in range(1, calendar.monthrange(year, month)[1] + 1):
                    dayStatusList.append({
                        "dayOfTheMonth": i,
                        "status": "not_working"
                    })
                resp.update({'status_code': 200,
                             'resq_body': {
                                 'dayStatusList': dayStatusList
                             }
                             })
                return resp

            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            
            endayOfMonth = calendar.monthrange(year, month)[1]
            start_datetime = datetime.strptime('''%s-%s-1 00:00:00''' % (year, month), '%Y-%m-%d %H:%M:%S')
            end_datetime = datetime.strptime('''%s-%s-%s 23:59:59''' % (year, month, endayOfMonth), '%Y-%m-%d %H:%M:%S')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                
                if res_token:
                    doctor_id = res_token[7]
                    sql = '''SELECT apm.schedule_date
                            FROM appointment apm
                            INNER JOIN user_account ua on ua.id = apm.doctor_id
                            WHERE ua.user_mngment_id = %s AND
                            apm.schedule_date >= %s::timestamp AND apm.schedule_date <= %s::timestamp  
                            and apm.state in ('confirmed_by_patient','confirmed','processing','rescheduled_by_patient') '''

                    apm_records = gs.sql_execute(pool, sql, para_values=(doctor_id, start_datetime, end_datetime))

                    whitelist = []
                    blacklist = []
                    dayStatusList = []
                    sql = '''SELECT cv.id, --0
                                    cv.start_datetime, --1
                                    cv.end_datetime, --2
                                    cv.duration --3
                            FROM calendar_event cv
                            INNER JOIN user_account ua on ua.id = cv.user_id 
                            WHERE cv.recurrency = False 
                            and cv.active = True 
                            AND ua.user_mngment_id = %s 
                            AND cv.start_datetime >= %s::timestamp 
                            AND cv.start_datetime <= %s::timestamp'''

                    records_checkexist = gs.sql_execute(pool, sql,
                                                        para_values=(doctor_id, start_datetime, end_datetime))

                    for line in records_checkexist:
                        whitelist.append({
                            'date': line[1].strftime('%d'),
                            'startTime': line[1],
                            'endTime': line[2],
                            'duration': line[3]
                        })

                    sql_loop = '''select cv.id, cv.start_datetime, cv.end_datetime, cv.count, cv.interval, cv.recurrency, cv.byday, cv.week_list, cv.rrule_type, cv.month_by, cv.end_type,cv.su,cv.mo,cv.tu,cv.we,cv.th,cv.fr,cv.sa,cv.day,cv.duration,cv.final_date
                                                from calendar_event cv
                                                   INNER JOIN user_account ua on ua.id = cv.user_id
                                                where recurrency = True
                                                  and cv.active = True
                                                  and cv.recurrent_id is Null
                                                  and ua.user_mngment_id = %s
                                                  and cv.final_date >= %s
                                                  and cv.start_datetime <= %s
                                                                        '''
                    loop_checkexist = gs.sql_execute(pool, sql_loop,
                                                     para_values=(doctor_id, start_datetime, end_datetime))

                    for line in loop_checkexist:
                        count = int(line[3])
                        interval = int(line[4])
                        if line[8] == 'daily':
                            for i in range(0, count):
                                sd = line[1] + timedelta(days=interval * i)
                                if sd >= start_datetime and sd <= end_datetime:
                                    whitelist.append({
                                        'date': sd.strftime('%d'),
                                        'startTime': line[1],
                                        'endTime': line[2],
                                        'duration': line[19]
                                    })
                                elif sd > end_datetime:
                                    break

                        elif line[8] == 'weekly':

                            days = []
                            if line[11] == True:  # su
                                days.append('su')
                            if line[12] == True:  # su
                                days.append('mo')
                            if line[13] == True:  # su
                                days.append('tu')
                            if line[14] == True:  # su
                                days.append('we')
                            if line[15] == True:  # su
                                days.append('th')
                            if line[16] == True:  # su
                                days.append('fr')
                            if line[17] == True:  # su
                                days.append('sa')

                            for i in range(0, int(endayOfMonth)):
                                sd = start_datetime + timedelta(days=i)
                                if sd.date() >= line[1].date() and sd < datetime.strptime(
                                                '''%s 00:00:00''' % (line[20]), '%Y-%m-%d %H:%M:%S'):
                                    dayOfweek = sd.date().strftime("%A").lower()[:2]
                                    if dayOfweek in days:
                                        whitelist.append({
                                            'date': sd.strftime('%d'),
                                            'startTime': line[1],
                                            'endTime': line[2],
                                            'duration': line[19]
                                        })
                                elif sd > end_datetime:
                                    break

                        elif line[8] == 'monthly':
                            if line[1] <= end_datetime:
                                if line[9] == 'date':
                                    sd = start_datetime.replace(day=int(line[18]))
                                    whitelist.append({
                                        'date': sd.strftime('%d'),
                                        'startTime': line[1],
                                        'endTime': line[2],
                                        'duration': line[19]
                                    })
                                elif line[9] == 'day':
                                    if line[7] == 'su':
                                        rr = rrule.rrule(rrule.WEEKLY, byweekday=relativedelta.SU,
                                                         dtstart=start_datetime)
                                    elif line[7] == 'mo':
                                        rr = rrule.rrule(rrule.WEEKLY, byweekday=relativedelta.MO,
                                                         dtstart=start_datetime)
                                    elif line[7] == 'tu':
                                        rr = rrule.rrule(rrule.WEEKLY, byweekday=relativedelta.TU,
                                                         dtstart=start_datetime)
                                    elif line[7] == 'we':
                                        rr = rrule.rrule(rrule.WEEKLY, byweekday=relativedelta.WE,
                                                         dtstart=start_datetime)
                                    elif line[7] == 'th':
                                        rr = rrule.rrule(rrule.WEEKLY, byweekday=relativedelta.TH,
                                                         dtstart=start_datetime)
                                    elif line[7] == 'fr':
                                        rr = rrule.rrule(rrule.WEEKLY, byweekday=relativedelta.FR,
                                                         dtstart=start_datetime)
                                    else:
                                        rr = rrule.rrule(rrule.WEEKLY, byweekday=relativedelta.SA,
                                                         dtstart=start_datetime)

                                    result = rr.between(start_datetime, end_datetime, inc=True)[-1]
                                    whitelist.append({
                                        'date': result.strftime('%d'),
                                        'startTime': line[1],
                                        'endTime': line[2],
                                        'duration': line[19]
                                    })

                        sql = ''' SELECT cv.start_datetime, --0
                                        cv.end_datetime, --1
                                        cv.active,  --2
                                        cv.duration, -- 3
                                        cv.recurrent_id
                                        FROM calendar_event cv
                                        WHERE cv.recurrent_id = %s 
                                        AND cv.start_datetime >= %s::timestamp 
                                        AND cv.start_datetime <= %s::timestamp '''

                        checkexist = gs.sql_execute(pool, sql, para_values=(line[0], start_datetime, end_datetime))

                        if checkexist:
                            for ll in checkexist:
                                if ll[2] == True:
                                    whitelist.append({
                                        'date': ll[0].strftime('%d'),
                                        'startTime': ll[0],
                                        'endTime': ll[1],
                                        'duration': ll[3]
                                    })
                                else:
                                    blacklist.append({
                                        'date': ll[0].strftime('%d'),
                                        'startTime': ll[0],
                                        'endTime': ll[1],
                                        'duration': ll[3]
                                    })

                    sql = ''' SELECT consultation_duration
                                        FROM doctor_profile
                                        WHERE user_mngment_id = %s '''
                    duration = gs.sql_execute(pool, sql, para_values=(doctor_id,))[0][0][:-1]

                    whitetimeslot = []
                    for x in whitelist:
                        mul = int(x['duration']) / int(duration)
                        for t in range(1, mul + 1):
                            if t == 1:
                                whitetimeslot.append({
                                    'date': x['date'],
                                    'startTime': x['startTime'].strftime('%H:%M'),
                                    'endTime': (x['startTime'] + timedelta(minutes=int(duration))).strftime('%H:%M')
                                })
                            elif t > 1 and t < mul:
                                whitetimeslot.append({
                                    'date': x['date'],
                                    'startTime': (x['startTime'] + timedelta(minutes=int(duration) * (t - 1))).strftime(
                                        '%H:%M'),
                                    'endTime': (x['startTime'] + timedelta(minutes=int(duration) * t)).strftime('%H:%M')
                                })
                            else:
                                whitetimeslot.append({
                                    'date': x['date'],
                                    'startTime': (x['startTime'] + timedelta(minutes=int(duration) * (t - 1))).strftime(
                                        '%H:%M'),
                                    'endTime': x['endTime'].strftime('%H:%M')
                                })

                    white_unique_dicts = np.unique(np.array(whitetimeslot))

                    blacktimeslot = []
                    for x in blacklist:
                        mul = int(x['duration']) / int(duration)
                        for t in range(1, mul + 1):
                            if t == 1:
                                blacktimeslot.append({
                                    'date': x['date'],
                                    'startTime': x['startTime'].strftime('%H:%M'),
                                    'endTime': (x['startTime'] + timedelta(minutes=int(duration))).strftime('%H:%M')
                                })
                            elif t > 1 and t < mul:
                                blacktimeslot.append({
                                    'date': x['date'],
                                    'startTime': (x['startTime'] + timedelta(minutes=int(duration) * (t - 1))).strftime(
                                        '%H:%M'),
                                    'endTime': (x['startTime'] + timedelta(minutes=int(duration) * t)).strftime('%H:%M')
                                })
                            else:
                                blacktimeslot.append({
                                    'date': x['date'],
                                    'startTime': (x['startTime'] + timedelta(minutes=int(duration) * (t - 1))).strftime(
                                        '%H:%M'),
                                    'endTime': x['endTime'].strftime('%H:%M')
                                })
                    black_of_unique_dicts = np.unique(np.array(blacktimeslot))

                    def checkContains(x, items):
                        for item in items:
                            if item['date'] == x['date']:
                                if (x['startTime'] >= item['startTime'] and x['startTime'] <= item['endTime']) or (
                                        x['endTime'] >= item['startTime'] and x['endTime'] <= item['endTime']):
                                    return True
                        return False

                    flag = -1
                    available = 0
                    notAvailable = 0
                    total = 0
                    for w in white_unique_dicts:
                        if flag != w['date']:
                            if (flag != -1):
                                timeSlot = {
                                    "dayOfTheMonth": int(flag),
                                }
                                if available == 0 and notAvailable == 0:
                                    timeSlot.update({
                                        "status": "not_working"
                                    })
                                else:
                                    if available / ((available + notAvailable) * 1.0) == 0:
                                        timeSlot.update({
                                            "status": "none"
                                        })
                                    elif available / ((available + notAvailable) * 1.0) < 0.5:
                                        timeSlot.update({
                                            "status": "few"
                                        })
                                    else:
                                        timeSlot.update({
                                            "status": "many"
                                        })

                                dayStatusList.append(timeSlot)
                            total = 1
                            flag = w['date']
                            available = 0
                            notAvailable = 0
                        else:
                            total = total + 1

                        if not checkContains(w, black_of_unique_dicts):
                            if datetime.now().date().month == month and datetime.now().date().year == year:
                                if datetime.now().date().day > int(flag):
                                    pass
                                elif datetime.now().date().day == int(flag) and datetime.now().strftime('%H:%M') >= w[
                                    'endTime']:
                                    notAvailable = notAvailable + 1
                                else:
                                    available = available + 1
                                    for apm in apm_records:
                                        if not ((w['startTime'] > apm[0].strftime('%H:%M') and w['endTime'] > apm[
                                            0].strftime('%H:%M')) or
                                                    (w['startTime'] < apm[0].strftime('%H:%M') and w['endTime'] < apm[
                                                        0].strftime('%H:%M'))) and apm[0].strftime('%d') == w['date']:
                                            available = available - 1
                                            notAvailable = notAvailable + 1
                                            break
                            else:
                                available = available + 1
                                for apm in apm_records:
                                    if not ((w['startTime'] > apm[0].strftime('%H:%M') and w['endTime'] > apm[
                                        0].strftime('%H:%M')) or
                                                (w['startTime'] < apm[0].strftime('%H:%M') and w['endTime'] < apm[
                                                    0].strftime('%H:%M'))) and apm[0].strftime('%d') == w['date']:
                                        available = available - 1
                                        notAvailable = notAvailable + 1
                                        break

                    if (flag != -1):
                        timeSlot = {
                            "dayOfTheMonth": int(flag),
                        }
                        if available == 0 and notAvailable == 0:
                            timeSlot.update({
                                "status": "not_working"
                            })
                        else:
                            if available / ((available + notAvailable) * 1.0) == 0:
                                timeSlot.update({
                                    "status": "none"
                                })
                            elif available / ((available + notAvailable) * 1.0) < 0.5:
                                timeSlot.update({
                                    "status": "few"
                                })
                            else:
                                timeSlot.update({
                                    "status": "many"
                                })

                        dayStatusList.append(timeSlot)

                    def checkWorkingDay(day, dayStatusList):
                        for dayStatus in dayStatusList:
                            if int(dayStatus['dayOfTheMonth']) == day:
                                return dayStatus
                        return {
                            "dayOfTheMonth": day,
                            "status": "not_working"
                        }

                    resultList = []
                    for i in range(1, calendar.monthrange(year, month)[1] + 1):
                        resultList.append(checkWorkingDay(i, dayStatusList))

                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     'dayStatusList': resultList
                                 }
                                 })


                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })

        except Exception as e:
            logger.error('error_dashboard_get_doctor_availability_status_by_month %s' % str(e))
            client.captureException()
        return resp
    
    
    
    def dashboard_get_call_history(self, data):
        logger.info('dashboard_get_call %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
      
        patients = ["zzzsss2s"]  

        gs = self.general_business
        pool = self.pool_conn
        user_token = data.get('user_token')
        
        result = gs.sql_execute(pool, '''select * from appointment_call_history as ach INNER JOIN appointment as apm where apm.type='live' ''') 
        if result:
            for line in result:
                patients.append(line[0])
        resp = {'status_code': 200,
                'resq_body':
                    {
                        "call_history" : patients
                    }
        }
        return resp  




    def generate_pagination_list(self, user_id, table_pagination, context={}):
        gs = self.general_business
        pool = self.pool_conn
        full_list_ids = []

        def cache_list(full_list_ids, table_pagination, rand=False):
            if rand == True:
                random.shuffle(full_list_ids)
            if full_list_ids:
                sql_search = '''select id 
                                            from user_account_pagination
                                            where user_account_id = %s
                                                and table_pagination = %s '''
                exist = gs.sql_execute(pool, sql_search, para_values=(user_id,
                                                                      table_pagination))
                if exist:
                    sql_update = '''update user_account_pagination set list_id = %s
                                                where user_account_id = %s
                                                    and table_pagination = %s '''
                    gs.sql_execute(pool, sql_update, commit=True, para_values=(str(full_list_ids),
                                                                               user_id,
                                                                               table_pagination))

                else:
                    sql_insert = '''insert into user_account_pagination(user_account_id, list_id, table_pagination)
                                                values (%s,%s,%s)'''
                    gs.sql_execute(pool, sql_insert, commit=True, para_values=(user_id,
                                                                               str(full_list_ids),
                                                                               table_pagination))

            return full_list_ids

        if 'appointmentType' in context:
            sql_list = '''select id
                            from appointment
                            %s '''

            if context['appointmentType'] == 'upcoming':
                condition = '''  '''
            elif context['appointmentType'] == 'previous':
                condition = '''where doctor_id = %s and  (state in ('rescheduled_by_patient', 'successful_finished','free_finished','canceled_by_patient','canceled_by_doctor')) 
                                order by schedule_date desc '''
            elif context['appointmentType'] == 'unconfirmed':
                condition = '''where doctor_id = %s and  state = 'confirmed_by_patient' 
                                order by schedule_date desc '''
            elif context['appointmentType'] == 'live':
                sql_list = '''select min(ach.id),apm.id as apmId from appointment as apm %s '''
                condition = '''INNER JOIN appointment_call_history as ach ON apm.id = ach.appointment_id where apm.doctor_id = %s and (apm.type = 'live')  and apm.booking_status IS NOT NULL group by apm.id order by apm.schedule_date desc '''
            res_list = gs.sql_execute(pool, sql_list%condition, para_values=[user_id])
            for rc in res_list:
                full_list_ids.append(int(rc[0]))

            full_list_ids = cache_list(full_list_ids, table_pagination, rand=False)

        elif 'dashboard_patient' in context:
            sql_list = '''select patient_id, count(*)
                            from appointment
                            where doctor_id = %s 
                            group by 1'''

            res_list = gs.sql_execute(pool, sql_list, para_values=[user_id])
            for rc in res_list:
                full_list_ids.append([rc[0], rc[1]])

            full_list_ids = cache_list(full_list_ids, table_pagination, rand=False)
        return full_list_ids

