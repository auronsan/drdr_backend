# -*- coding: utf-8 -*-
import ConfigParser
import falcon
from falcon_multipart.middleware import MultipartMiddleware
import logging
import os
from connection_pool import connection_pool

from user_account.user_account_api import user_account
from metadata.metadata_api import metadata_api
from tokbox.tokbox_api import tokbox_api
from profile.profile_patient_api import profile_patient_api
from profile.profile_doctor_api import profile_doctor_api
from payment.payment_account_virtual_api import account_virtual_api
from appointment.appointment_api import appointment_api
from schedule.doctor_schedule_api import doctor_schedule_api
from schedule.patient_schedule_api import patient_schedule_api
from appointment.medical_record_api import medical_record_api
from payment.user_payment_gateway_api import user_payment_gateway_api
from dashboard.dashboard_api import dashboard_api

from hospital.hospital_api import hospital_api


class DBFormatter(logging.Formatter):
    def __init__(self, fmt=None, datefmt=None, config=None):
        super(DBFormatter, self).__init__(fmt, datefmt)
        self.config = config

    def format(self, record):
        record.pid = os.getpid()
        record.dbname = self.config.get('environment', 'ERP_DB_NAME')
        return logging.Formatter.format(self, record)


def init_logger(config):
    logfile = config.get('environment', 'logfile')
    format_template = '%(asctime)s %(pid)s %(levelname)s %(dbname)s : %(message)s'
    formatter = DBFormatter(fmt=format_template, config=config)
    logger = logging.getLogger('drdr_api')
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler(logfile)
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    return logger


def prepare_conn():
    try:
        # conn = psycopg2.connect("host='%s' dbname='%s' user='%s' password='%s' port='%s'" %(ERP_DB_HOST, ERP_DB_NAME, ERP_DB_USER, ERP_DB_PASS, ERP_DB_PORT))
        conn_obj = connection_pool(ERP_DB_HOST, ERP_DB_NAME, ERP_DB_USER, ERP_DB_PASS, ERP_DB_PORT)
        pool_conn = conn_obj.get_conn()  # .connect()
        return pool_conn
    except Exception as e:
        print str(e)


def load_app(config_file=False):
    global ERP_DB_HOST
    global ERP_DB_PORT
    global ERP_DB_USER
    global ERP_DB_PASS
    global ERP_DB_NAME
    # Default route
    # Mobile
    route_mp = '/mp'

    # Website
    route_wd = '/wd'

    # Website Admin
    route_wa = '/wa'


    # load config file
    if config_file:
        config = ConfigParser.RawConfigParser(allow_no_value=True)
        config.read(config_file)

        # Create logger - sudo chmod -R a+rwX /var/log/drdr_api_logging.log
        init_logger(config)

        ERP_DB_HOST = config.get('environment', 'ERP_DB_HOST')
        ERP_DB_PORT = config.get('environment', 'ERP_DB_PORT')
        ERP_DB_USER = config.get('environment', 'ERP_DB_USER')
        ERP_DB_PASS = config.get('environment', 'ERP_DB_PASS')
        ERP_DB_NAME = config.get('environment', 'ERP_DB_NAME')

        var_conf = {
            'ERP_DB_HOST': config.get('environment', 'ERP_DB_HOST'),
            'ERP_DB_PORT': config.get('environment', 'ERP_DB_PORT'),
            'ERP_DB_USER': config.get('environment', 'ERP_DB_USER'),
            'ERP_DB_PASS': config.get('environment', 'ERP_DB_PASS'),
            'ERP_DB_NAME': config.get('environment', 'ERP_DB_NAME'),
        }

    app = falcon.API(middleware=[MultipartMiddleware()])
    pool_conn = prepare_conn()

    # App class
    user_login = user_account('user_login', pool_conn, var_conf)
    user_login_v2 = user_account('user_login_v2', pool_conn, var_conf)
    admin_user_login_v2 = user_account('admin_user_login_v2', pool_conn, var_conf)
    user_facebook_login = user_account('user_facebook_login', pool_conn, var_conf)
    user_logout = user_account('user_logout', pool_conn, var_conf)
    user_register = user_account('user_register', pool_conn, var_conf)
    user_register_v2 = user_account('user_register_v2', pool_conn, var_conf)
    user_register_v2_editprofile = user_account('user_register_v2_editprofile', pool_conn, var_conf)
    user_register_v2_uploaddocs = user_account('user_register_v2_uploaddocs', pool_conn, var_conf)
    user_active_sms = user_account('user_active_sms', pool_conn, var_conf)
    user_request_sms = user_account('user_request_sms', pool_conn, var_conf)
    user_request_sms_v2 = user_account('user_request_sms_v2', pool_conn, var_conf)
    user_request_password = user_account('user_request_new_pasword', pool_conn, var_conf)
    user_change_password = user_account('user_change_pasword', pool_conn, var_conf)
    user_update_phone = user_account('user_update_phone', pool_conn, var_conf)
    user_settings_get = user_account('user_settings_get', pool_conn, var_conf)
    user_settings_set = user_account('user_settings_set', pool_conn, var_conf)
    get_metadata = metadata_api('get_metadata', pool_conn, var_conf)
    check_metadata = metadata_api('check_metadata', pool_conn, var_conf)
    get_current_time = metadata_api('get_current_time', pool_conn, var_conf)
    tokbox_init = tokbox_api('tokbox_init', pool_conn, var_conf)
    tokbox_call = tokbox_api('tokbox_call', pool_conn, var_conf)
    tokbox_call_v2 = tokbox_api('tokbox_call_v2', pool_conn, var_conf)
    tokbox_register = tokbox_api('tokbox_register', pool_conn, var_conf)
    tokbox_deregister = tokbox_api('tokbox_deregister', pool_conn, var_conf)
    tokbox_end = tokbox_api('tokbox_end', pool_conn, var_conf)
    tokbox_deny = tokbox_api('tokbox_deny', pool_conn, var_conf)
    tokbox_accept = tokbox_api('tokbox_accept', pool_conn, var_conf)
    profile_firsttimesetup = profile_patient_api('profile_firsttimesetup', pool_conn, var_conf)
    profile_patient_get = profile_patient_api('profile_patient_get', pool_conn, var_conf)
    profile_patient_setup = profile_patient_api('profile_patient_setup', pool_conn, var_conf)
    profile_patient_get_doctor = profile_patient_api('profile_patient_get_doctor', pool_conn, var_conf)

    
    profile_patient_create_token = profile_patient_api('profile_patient_create_token', pool_conn, var_conf)
    profile_patient_get_dr_shortlist = profile_patient_api('profile_patient_get_dr_shortlist', pool_conn, var_conf)
    profile_patient_dr_list_byspecicalty = profile_patient_api('profile_patient_dr_list_byspecicalty', pool_conn,var_conf)
    profile_patient_mark_doctor = profile_patient_api('profile_patient_mark_doctor', pool_conn, var_conf)
    profile_patient_unmark_doctor = profile_patient_api('profile_patient_unmark_doctor', pool_conn, var_conf)
    profile_patient_get_medicalRecordFieldCode = profile_patient_api('profile_patient_get_medicalRecordFieldCode',pool_conn, var_conf)
    profile_patient_get_general_medicalRecord = profile_patient_api('profile_patient_get_general_medicalRecord',pool_conn, var_conf)
    profile_patient_setup_general_medicalRecord = profile_patient_api('profile_patient_setup_general_medicalRecord',pool_conn, var_conf)
    profile_patient_get_dr_list_by_sort_filter = profile_patient_api('profile_patient_get_dr_list_by_sort_filter',pool_conn, var_conf)
    profile_patient_get_live_dr_list = profile_patient_api('profile_patient_get_live_dr_list', pool_conn, var_conf)
    profile_patient_get_live_dr_list_by_sort_filter = profile_patient_api('profile_patient_get_live_dr_list_by_sort_filter', pool_conn, var_conf)
    profile_patient_get_appointment_upcominglist = profile_patient_api('profile_patient_get_appointment_upcominglist',pool_conn, var_conf)
    profile_patient_get_appointment_previouslist = profile_patient_api('profile_patient_get_appointment_previouslist',pool_conn, var_conf)
    profile_patient_get_details_by_id = profile_doctor_api('get_patient_profile_details_by_id', pool_conn, var_conf)
    profile_doctor_get_patient_history = profile_doctor_api('profile_doctor_get_patient_history', pool_conn, var_conf)
    profile_doctor_add_note_document = profile_doctor_api('profile_doctor_add_note_document', pool_conn, var_conf)
    profile_patient_get_medical_record = profile_patient_api('profile_patient_get_medical_record', pool_conn, var_conf)
    profile_doctor_get = profile_doctor_api('profile_doctor_get', pool_conn, var_conf)
    profile_doctor_setup = profile_doctor_api('profile_doctor_setup', pool_conn, var_conf)
    profile_doctor_avatar_upload = profile_doctor_api('profile_doctor_avatar_upload', pool_conn, var_conf)
    profile_reviews_avgRate_get = profile_doctor_api('profile_reviews_avgRate_get', pool_conn, var_conf)
    profile_reviews_get = profile_doctor_api('profile_reviews_get', pool_conn, var_conf)
    get_list_patient_interacted = profile_doctor_api('get_list_patient_interacted', pool_conn, var_conf)
    get_review_list_pagination = profile_doctor_api('get_review_list_pagination', pool_conn, var_conf)
    profile_doctor_avatar_remove = profile_doctor_api('profile_doctor_avatar_remove', pool_conn, var_conf)
    profile_doctor_set_online = profile_doctor_api('doctor_go_online', pool_conn, var_conf)
    profile_doctor_set_offline = profile_doctor_api('doctor_go_offline', pool_conn, var_conf)
    profile_doctor_get_online_status = profile_doctor_api('doctor_get_online_status', pool_conn, var_conf)
    profile_doctor_get_documents = profile_doctor_api('profile_doctor_get_documents', pool_conn, var_conf)
    profile_doctor_check_email_existence = profile_doctor_api('doctor_check_email_existence', pool_conn, var_conf)
    profile_doctor_check_phone_existence = profile_doctor_api('doctor_check_phone_existence', pool_conn, var_conf)
    user_register_v2_getdocs = user_account('user_register_v2_getdocs', pool_conn, var_conf)
    user_register_v2_deletedoc = user_account('user_register_v2_deletedoc', pool_conn, var_conf)
    user_documents_delete = user_account('user_documents_delete', pool_conn, var_conf)
    user_documents_upload = user_account('user_documents_upload', pool_conn, var_conf)
    user_register_v2_sendtheform = user_account('user_register_v2_sendtheform', pool_conn, var_conf)
    doctor_goOnline_v2 = user_account('doctor_goOnline_v2', pool_conn, var_conf)
    doctor_get_appointment_upcominglist = profile_doctor_api('profile_doc_get_appointment_upcominglist_of_patient', pool_conn, var_conf)
    doctor_get_appointment_previouslist = profile_doctor_api('profile_doc_get_appointment_previouslist_of_patient', pool_conn, var_conf)
    # medical_record_addnote = medical_record_api('medical_record_addnote', pool_conn, var_conf)
    medical_record_submit = medical_record_api('medical_record_submit', pool_conn, var_conf)
    medical_record_get = medical_record_api('medical_record_get', pool_conn, var_conf)
    doctor_cancel_the_specificic_appointment = appointment_api('doctor_cancel_the_specificic_appointment', pool_conn,var_conf)
    doctor_suspend_the_specificic_appointment = appointment_api('doctor_suspend_the_specificic_appointment', pool_conn,var_conf)
    doctor_complete_the_specificic_appointment = appointment_api('doctor_complete_the_specificic_appointment',pool_conn, var_conf)
    doctor_mark_the_specific_appointment_as_free = appointment_api('doctor_mark_the_specific_appointment_as_free',pool_conn, var_conf)
    balance_get = account_virtual_api('account_virtual_balance_get', pool_conn, var_conf)
    payment_summary = account_virtual_api('payment_summary', pool_conn, var_conf)
    payment_statement_daily_detail = account_virtual_api('payment_statement_daily_detail', pool_conn, var_conf)
    payment_appointment_get = account_virtual_api('payment_appointment_get', pool_conn, var_conf)
    profile_doctor_add_bank_account = profile_doctor_api('profile_doctor_add_bank_account', pool_conn, var_conf)
    profile_doctor_set_bank_account_default = profile_doctor_api('profile_doctor_set_bank_account_default', pool_conn,var_conf)
    profile_doctor_get_bank_account = profile_doctor_api('profile_doctor_get_bank_account', pool_conn, var_conf)
    admin_get_bank_account = profile_doctor_api('admin_get_bank_account', pool_conn, var_conf)
    admin_create_doctor_payout = profile_doctor_api('admin_create_doctor_payout', pool_conn, var_conf)
    admin_update_doctor_payout = profile_doctor_api('admin_update_doctor_payout', pool_conn, var_conf)
    admin_active_user_account = profile_doctor_api('admin_active_user_account', pool_conn, var_conf)
    profile_doctor_remove_bank_account = profile_doctor_api('profile_doctor_remove_bank_account', pool_conn, var_conf)
    profile_doctor_get_payout_list = profile_doctor_api('profile_doctor_get_payout_list', pool_conn, var_conf)
    appointment_get_details = appointment_api('get_appointment_details', pool_conn, var_conf)
    appointment_review_post = appointment_api('appointment_review_post', pool_conn, var_conf)
    doctor_schedule_set_singleday_availability = doctor_schedule_api('doctor_schedule_set_singleday_availability',pool_conn, var_conf)
    doctor_schedule_repeat_day_availability = doctor_schedule_api('doctor_schedule_repeat_day_availability', pool_conn,var_conf)
    doctor_schedule_repeat_week_availability = doctor_schedule_api('doctor_schedule_repeat_week_availability',pool_conn, var_conf)
    doctor_schedule_repeat_monthly_availability = doctor_schedule_api('doctor_schedule_repeat_monthly_availability',pool_conn, var_conf)
    doctor_schedule_get_availability_daterange = doctor_schedule_api('doctor_schedule_get_availability_daterange',pool_conn, var_conf)
    doctor_schedule_remove_availability = doctor_schedule_api('doctor_schedule_remove_availability', pool_conn,var_conf)
    doctor_schedule_edit_availability = doctor_schedule_api('doctor_schedule_edit_availability', pool_conn, var_conf)
    doctor_check_appointment_existence = doctor_schedule_api('doctor_check_appointment_existence', pool_conn, var_conf)
    doctor_get_all_the_oppointment_list_in_date_range = appointment_api('doctor_get_all_the_oppointment_list_in_date_range', pool_conn, var_conf)
    appointment_note_submit = appointment_api('appointment_note_submit', pool_conn, var_conf)
    appointment_note_get = appointment_api('appointment_note_get', pool_conn, var_conf)
    doctor_get_appointment_details = appointment_api('doctor_get_appointment_details', pool_conn, var_conf)
    appointment_document_submit = appointment_api('appointment_document_submit', pool_conn, var_conf)
    delete_the_specific_appointment_document = appointment_api('delete_the_specific_appointment_document', pool_conn,var_conf)
    doctor_confirm_the_specific_appointment = appointment_api('doctor_confirm_the_specific_appointment', pool_conn,var_conf)
    dashboard_get_summary = dashboard_api('dashboard_get_summary', pool_conn, var_conf)
    dashboard_get_appointment_list = dashboard_api('dashboard_get_appointment_list', pool_conn, var_conf)
    dashboard_get_top_patient_list = dashboard_api('dashboard_get_top_patient_list', pool_conn, var_conf)
    dashboard_get_doctor_availability_status_by_month = dashboard_api('dashboard_get_doctor_availability_status_by_month', pool_conn, var_conf)
    get_doctor_schedule_timeslot = patient_schedule_api('get_schedule_timeslot_list_of_a_doctor_for_specific_date', pool_conn, var_conf)
    get_schedule_status = patient_schedule_api('get_schedule_status', pool_conn, var_conf)
    get_schedule_information_to_book_the_appointment = patient_schedule_api('get_schedule_information_to_book_the_appointment', pool_conn, var_conf)
    patient_get_promotion_code = patient_schedule_api('patient_get_promotion_code', pool_conn, var_conf)
    patient_book_the_appointment = patient_schedule_api('patient_book_the_appointment', pool_conn, var_conf)
    profile_patient_get_appointment_sumarylist = profile_patient_api('profile_patient_get_appointment_sumarylist',pool_conn, var_conf)
    get_check_appointment_reschedule = appointment_api('get_check_appointment_reschedule', pool_conn, var_conf)
    get_check_appointment_cancelation = appointment_api('get_check_appointment_cancelation', pool_conn, var_conf)
    patient_cancel_the_specificic_appointment = appointment_api('patient_cancel_the_specificic_appointment', pool_conn,var_conf)
    patient_reschedule_the_specificic_appointment = appointment_api('patient_reschedule_the_specificic_appointment',pool_conn, var_conf)
    patient_submit_document_to_appointment = appointment_api('patient_submit_document_to_the_specificic_appointment', pool_conn, var_conf)
    payment_proceed = user_payment_gateway_api('payment_proceed', pool_conn, var_conf)
    dd_card_proceed = user_payment_gateway_api('dd_card_proceed', pool_conn, var_conf)
    payment_paypal_proceed = user_payment_gateway_api('payment_paypal_proceed', pool_conn, var_conf)
    payment_paypal_deposit = user_payment_gateway_api('payment_paypal_deposit', pool_conn, var_conf)
    payment_paypal_cancel = user_payment_gateway_api('payment_paypal_cancel', pool_conn, var_conf)
    admin_virtualaccount_get = account_virtual_api('admin_virtualaccount_get', pool_conn, var_conf)
    transactionhistory_get = account_virtual_api('transactionhistory_get', pool_conn, var_conf)
    specific_transaction_detail = account_virtual_api('specific_transaction_detail', pool_conn, var_conf)
    payment_stripe_create_customer = user_payment_gateway_api('payment_stripe_create_customer', pool_conn,var_conf)
    payment_stripe_get_list_card = user_payment_gateway_api('payment_stripe_get_list_card', pool_conn,var_conf)
    payment_stripe_deposit = user_payment_gateway_api('payment_stripe_deposit', pool_conn,var_conf)
    payment_stripe_add_card = user_payment_gateway_api('payment_stripe_add_card', pool_conn,var_conf)
    payment_stripe_remove_card = user_payment_gateway_api('payment_stripe_remove_card', pool_conn, var_conf)
    profile_patient_search_otherppl = profile_patient_api('profile_patient_search_otherppl', pool_conn, var_conf)

    #hospital

    
    hospital_get_dr_list_by_sort_filter = hospital_api('hospital_get_dr_list_by_sort_filter', pool_conn, var_conf)
    hospital_get_live_dr_list = hospital_api('hospital_get_live_dr_list', pool_conn, var_conf)
    hospital_get_live_dr_list_by_sort_filter = hospital_api('hospital_get_live_dr_list_by_sort_filter', pool_conn, var_conf)
    hospital_get_dr_shortlist = hospital_api('hospital_get_dr_shortlist', pool_conn, var_conf)
    hospital_dr_list_byspecicalty = hospital_api('hospital_dr_list_byspecicalty', pool_conn, var_conf)
    hospital_get_dr_fulllist = hospital_api('hospital_get_dr_fulllist', pool_conn, var_conf)
    user_login_v2_hospital = user_account('user_login_v2_hospital', pool_conn, var_conf)
    user_register_v2_hospital = user_account('user_register_v2_hospital', pool_conn, var_conf)
    user_facebook_login_hospital = user_account('user_facebook_login_hospital', pool_conn, var_conf)
    user_request_new_pasword_hospital = user_account('user_request_new_pasword_hospital', pool_conn, var_conf)


    patient_call_logs = hospital_api('patient_call_logs', pool_conn, var_conf)

    #admin hospital
    
    admin_get_dr_fulllist = hospital_api('admin_get_dr_fulllist', pool_conn, var_conf)
    admin_doctor_update_profile  = hospital_api('admin_doctor_update_profile', pool_conn, var_conf)
    admin_doctor_update_profile_experiences  = hospital_api('admin_doctor_update_profile_experiences', pool_conn, var_conf)

    admin_doctor_deactive  = hospital_api('admin_doctor_deactive', pool_conn, var_conf)
    
    admin_doctor_active  = hospital_api('admin_doctor_active', pool_conn, var_conf)

    admin_call_logs  = hospital_api('admin_call_logs', pool_conn, var_conf) 
    admin_doctor_call_logs  = hospital_api('admin_doctor_call_logs', pool_conn, var_conf) 
    admin_doctor_avatar_upload  = hospital_api('admin_doctor_avatar_upload', pool_conn, var_conf)
    admin_doctor_avatar_remove  = hospital_api('admin_doctor_avatar_remove', pool_conn, var_conf)
    
    admin_add_doctor  = hospital_api('admin_add_doctor', pool_conn, var_conf) 
    
    tokbox_init_hospital = tokbox_api('tokbox_init_hospital', pool_conn, var_conf)
    # -----------------------------Route------------------------------------

    # User mobile
    # app.add_route(route_mp + '/user/login', user_login)
    app.add_route(route_mp + '/user/login/v2', user_login_v2)
    app.add_route(route_mp + '/user/facebook_login', user_facebook_login)
    app.add_route(route_mp + '/user/logout', user_logout)
    app.add_route(route_mp + '/user/register', user_register)
    app.add_route(route_mp + '/user/activate/sms', user_active_sms)
    app.add_route(route_mp + '/user/activate/requestSMS', user_request_sms)
    app.add_route(route_mp + '/user/activate/requestSMS/v2', user_request_sms_v2)
    app.add_route(route_mp + '/user/password/requestNew', user_request_password)
    app.add_route(route_mp + '/user/password/change', user_change_password)
    app.add_route(route_mp + '/user/activate/updatePhone', user_update_phone)
    app.add_route(route_mp + '/settings/get', user_settings_get)
    app.add_route(route_mp + '/settings/set', user_settings_set)

    # Metadata mobile
    app.add_route(route_mp + '/metadata/get', get_metadata)
    app.add_route(route_mp + '/metadata/check', check_metadata)
    app.add_route(route_mp + '/systemTime/getCurrent', get_current_time)

    # Tokbox mobile
    app.add_route(route_mp + '/videocall/register', tokbox_register)
    app.add_route(route_mp + '/videocall/deregister', tokbox_deregister)
    app.add_route(route_mp + '/videocall/init', tokbox_init)
    app.add_route(route_mp + '/videocall/call', tokbox_call)
    app.add_route(route_mp + '/videocall/call/v2', tokbox_call_v2)
    app.add_route(route_mp + '/videocall/end', tokbox_end)
    app.add_route(route_mp + '/videocall/deny', tokbox_deny)
    app.add_route(route_mp + '/videocall/accept', tokbox_accept)

    # Profile mobile
    app.add_route(route_mp + '/profile/firsttimesetup', profile_firsttimesetup)
    app.add_route(route_mp + '/profile/get', profile_patient_get)
    app.add_route(route_mp + '/profile/setup', profile_patient_setup)
    app.add_route(route_mp + '/profile/avatar/upload', profile_doctor_avatar_upload)
    app.add_route(route_mp + '/doctor/{doctor_id}/profile/get', profile_patient_get_doctor)
    app.add_route(route_mp + '/doctor/shortlist/get', profile_patient_get_dr_shortlist)
    app.add_route(route_mp + '/doctor/list/getbyspecicalty', profile_patient_dr_list_byspecicalty)
    app.add_route(route_mp + '/doctor/{doctor_id}/favourite/mark', profile_patient_mark_doctor)
    app.add_route(route_mp + '/doctor/{doctor_id}/favourite/unmark', profile_patient_unmark_doctor)
    app.add_route(route_mp + '/medicalRecord/get', profile_patient_get_medicalRecordFieldCode)
    app.add_route(route_mp + '/doctor/list/advancedget', profile_patient_get_dr_list_by_sort_filter)
    app.add_route(route_mp + '/doctor/livelist/advancedget', profile_patient_get_live_dr_list_by_sort_filter)
    app.add_route(route_mp + '/doctor/livelist/get', profile_patient_get_live_dr_list)
    app.add_route(route_mp + '/doctor/{doctor_id}/reviews/get', get_review_list_pagination)
    app.add_route(route_mp + '/appointment/upcominglist/get', profile_patient_get_appointment_upcominglist)
    app.add_route(route_mp + '/appointment/previouslist/get', profile_patient_get_appointment_previouslist)

    #create token
    
    app.add_route(route_mp + '/doctor/{doctor_id}/token/create', profile_patient_create_token)

    #Get listing Doctor based Hospital  by anshor
    app.add_route(route_mp + '/hospital/{hospital_id}/doctor/shortlist/get', hospital_get_dr_shortlist)
    app.add_route(route_mp + '/hospital/{hospital_id}/doctor/list/getbyspecicalty', hospital_dr_list_byspecicalty)
    app.add_route(route_mp + '/hospital/{hospital_id}/doctor/list/advancedget', hospital_get_dr_list_by_sort_filter)
    app.add_route(route_mp + '/hospital/{hospital_id}/doctor/livelist/advancedget', hospital_get_live_dr_list_by_sort_filter)
    app.add_route(route_mp + '/hospital/{hospital_id}/doctor/livelist/get', hospital_get_live_dr_list)
    app.add_route(route_mp + '/hospital/{hospital_id}/doctor/fulllist/get', hospital_get_dr_fulllist)
    #login facebook hospital
    app.add_route(route_mp + '/hospital/{hospital_id}/user/facebook_login', user_facebook_login_hospital)
    app.add_route(route_mp + '/hospital/{hospital_id}/user/login/v2', user_login_v2_hospital)
    app.add_route(route_mp + '/hospital/{hospital_id}/user/register/', user_register_v2_hospital)
    app.add_route(route_mp + '/hospital/{hospital_id}/user/password/requestNew/', user_request_new_pasword_hospital)
    
    app.add_route(route_mp + '/hospital/{hospital_id}/videocall/init', tokbox_init_hospital)
    

    app.add_route(route_wd + '/hospital/{hospital_id}/videocall/init', tokbox_init_hospital)
    
    #app.add_route(route_mp + '/hospital/{hospital_id}/doctor/status', hospital_get_dr_status)
    
    app.add_route(route_wd + '/hospital/{hospital_id}/user/login/v2', user_login_v2_hospital)
    app.add_route(route_wd + '/hospital/{hospital_id}/user/register/', user_register_v2_hospital)
    app.add_route(route_wd + '/hospital/{hospital_id}/user/password/requestNew/', user_request_new_pasword_hospital)
    # WD API for doctor to mark the specificic appointment as session failed
    doctor_mark_the_specific_appointment_as_failed = appointment_api('doctor_mark_the_specific_appointment_as_failed', pool_conn, var_conf)
    app.add_route(route_wd + '/appointment/{apm_id}/markasfailed', doctor_mark_the_specific_appointment_as_failed)
    

    user_login_v3_hospital = user_account('user_login_v3_hospital', pool_conn, var_conf)
    
    tokbox_init_hospital_v3 = tokbox_api('tokbox_init_hospital_v3', pool_conn, var_conf)
    
    tokbox_end_v3 = tokbox_api('tokbox_end_v3', pool_conn, var_conf)
    user_facebook_login_hospital_v3 = user_account('user_facebook_login_hospital_v3', pool_conn, var_conf)
    app.add_route(route_mp + '/hospital/{hospital_id}/v3/user/facebook_login', user_facebook_login_hospital_v3)
    
    tokbox_status_hospital_v3 = tokbox_api('tokbox_status_hospital_v3', pool_conn, var_conf)
    app.add_route(route_wd + '/hospital/{hospital_id}/v3/user/login', user_login_v3_hospital)
    
    app.add_route(route_mp + '/hospital/{hospital_id}/v3/user/login', user_login_v3_hospital)
    
    app.add_route(route_mp + '/hospital/{hospital_id}/v3/videocall/init', tokbox_init_hospital_v3)
    
    app.add_route(route_wd + '/hospital/{hospital_id}/v3/videocall/init', tokbox_init_hospital_v3)
    app.add_route(route_mp + '/hospital/{hospital_id}/v3/videocall/status', tokbox_status_hospital_v3)
    
    app.add_route(route_wd + '/hospital/{hospital_id}/v3/videocall/status', tokbox_status_hospital_v3)
    
    app.add_route(route_mp + '/hospital/{hospital_id}/v3/videocall/register', tokbox_register)
    app.add_route(route_mp + '/hospital/{hospital_id}/v3/videocall/deregister', tokbox_deregister)
    app.add_route(route_mp + '/hospital/{hospital_id}/v3/videocall/call', tokbox_call)
    app.add_route(route_mp + '/hospital/{hospital_id}/v3/videocall/call/v2', tokbox_call_v2)
    app.add_route(route_mp + '/hospital/{hospital_id}/v3/videocall/end', tokbox_end_v3)
    
    app.add_route(route_wd + '/hospital/{hospital_id}/v3/videocall/end', tokbox_end_v3)

    tokbox_deny_v3 = tokbox_api('tokbox_deny_v3', pool_conn, var_conf)
    
    app.add_route(route_mp + '/hospital/{hospital_id}/v3/videocall/deny', tokbox_deny_v3)
    
    app.add_route(route_wd + '/hospital/{hospital_id}/v3/videocall/deny', tokbox_deny_v3)
   
    app.add_route(route_mp + '/hospital/{hospital_id}/v3/videocall/accept', tokbox_accept)
    
    app.add_route(route_wd + '/hospital/{hospital_id}/v3/videocall/accept', tokbox_accept)


    
    check_version =  metadata_api('check_version_app', pool_conn, var_conf)
    
    app.add_route(route_wd + '/hospital/{hospital_id}/v3/version', check_version)
   
    app.add_route(route_mp + '/hospital/{hospital_id}/v3/version', check_version)
    
    #call logs
    
    app.add_route(route_wd + '/hospital/{hospital_id}/v3/patient/callLogs', patient_call_logs)
   
    app.add_route(route_mp + '/hospital/{hospital_id}/v3/patient/callLogs', patient_call_logs)

    
    #call missed
    
    tokbox_missed_v3 =  tokbox_api('tokbox_missed_v3', pool_conn, var_conf)
    app.add_route(route_wd + '/hospital/{hospital_id}/v3/videocall/missed', tokbox_missed_v3)
   
    app.add_route(route_mp + '/hospital/{hospital_id}/v3/videocall/missed', tokbox_missed_v3)




    app.add_route(route_mp + '/hospital/{hospital_id}/videocall/init', tokbox_init_hospital)
    # MP4.8 API for patient to get general medical record information
    app.add_route(route_mp + '/medicalRecord/general/get', profile_patient_get_general_medicalRecord)
    # MP4.9 API for patient to setup general medical record information
    app.add_route(route_mp + '/medicalRecord/general/set', profile_patient_setup_general_medicalRecord)
    # MP4.10 API for patient to search other patients by email or phone
    app.add_route(route_mp + '/patientlist/searchByKeyword', profile_patient_search_otherppl)

    # MP 8.1
    app.add_route(route_mp + '/schedule/{doctor_id}/timeslotlist/get', get_doctor_schedule_timeslot)
    # MP 8.2
    # MP 8.6
    app.add_route(route_mp + '/schedule/{doctor_id}/dailyStatusList/get', get_schedule_status)
    # MP 8.3
    app.add_route(route_mp + '/schedule/info/get', get_schedule_information_to_book_the_appointment)
    # MP 8.4
    app.add_route(route_mp + '/promotionCode/getToken', patient_get_promotion_code)
    # MP 8.5
    app.add_route(route_mp + '/schedule/bookNewAppointment', patient_book_the_appointment)

    # MP 6.5
    app.add_route(route_mp + '/myhealth/summary/get', profile_patient_get_appointment_sumarylist)
    # MP 6.8
    app.add_route(route_mp + '/appointment/{apm_id}/reschedule/check', get_check_appointment_reschedule)
    # MP6.7 API for patient to cancel the specificic appointment
    app.add_route(route_mp + '/appointment/{apm_id}/cancel', patient_cancel_the_specificic_appointment)
    # MP6.9 API for patient to reschedule the specificic appointment
    app.add_route(route_mp + '/appointment/{apm_id}/reschedule', patient_reschedule_the_specificic_appointment)
    # MP6.10 API for patient to submit document to specific appointment
    app.add_route(route_mp + '/appointment/{apm_id}/document/submit',patient_submit_document_to_appointment)
    # MP6.11 API for patient to delete specific document of corresponding appointment
    app.add_route(route_mp + '/appointment/{apm_id}/document/{doc_id}/delete', delete_the_specific_appointment_document)

    # payment-stripe
    # app.add_route(route_mp + '/stripe/setup', payment_setup)
    app.add_route(route_mp + '/stripe/createcard', payment_stripe_create_customer)
    app.add_route(route_mp + '/stripe/creditCard/getlist', payment_stripe_get_list_card)
    app.add_route(route_mp + '/stripe/creditCard/deposit', payment_stripe_deposit)
    app.add_route(route_mp + '/stripe/creditCard/add', payment_stripe_add_card)
    app.add_route(route_mp + '/stripe/creditCard/remove', payment_stripe_remove_card)

    # payment/virtualaccount
    app.add_route(route_mp + '/virtualaccount/balance/get', balance_get)
    # appointment
    app.add_route(route_mp + '/appointment/{apm_id}/get', appointment_get_details)


    
    app.add_route(route_mp + '/appointment/{apm_id}/review', appointment_review_post)
    app.add_route(route_mp + '/appointment/{apm_id}/cancelation/check', get_check_appointment_cancelation)
    app.add_route(route_mp + '/mobileCard/deposit', payment_proceed)
    app.add_route(route_mp + '/drdrCard/deposit', dd_card_proceed)
    
    app.add_route(route_mp + '/virtualaccount/payment/paypal/deposit', payment_paypal_deposit)
    app.add_route(route_mp + '/paypal/{user_id}/process', payment_paypal_proceed)
    app.add_route(route_mp + '/paypal/{user_id}/cancel', payment_paypal_cancel)
    app.add_route(route_mp + '/virtualaccount/transactionhistory/get', transactionhistory_get)
    app.add_route(route_mp + '/virtualaccount/transaction/{id}/get', specific_transaction_detail)


    #hospital payment
    app.add_route(route_mp + '/hospital/{hospital_id}/stripe/createcard', payment_stripe_create_customer)
    app.add_route(route_mp + '/hospital/{hospital_id}/stripe/creditCard/getlist', payment_stripe_get_list_card)
    app.add_route(route_mp + '/hospital/{hospital_id}/stripe/creditCard/deposit', payment_stripe_deposit)
    app.add_route(route_mp + '/hospital/{hospital_id}/stripe/creditCard/add', payment_stripe_add_card)
    app.add_route(route_mp + '/hospital/{hospital_id}/stripe/creditCard/remove', payment_stripe_remove_card)

    # payment/virtualaccount
    app.add_route(route_mp + '/hospital/{hospital_id}/virtualaccount/balance/get', balance_get)

    # appointment
    
    appointment_get_details_hospital = appointment_api('get_appointment_details_hospital', pool_conn, var_conf)
    app.add_route(route_mp + '/hospital/{hospital_id}/appointment/{apm_id}/get', appointment_get_details_hospital)
    app.add_route(route_mp + '/hospital/{hospital_id}/appointment/{apm_id}/review', appointment_review_post)
    app.add_route(route_mp + '/hospital/{hospital_id}/appointment/{apm_id}/cancelation/check', get_check_appointment_cancelation)
    app.add_route(route_mp + '/hospital/{hospital_id}/mobileCard/deposit', payment_proceed)
    app.add_route(route_mp + '/hospital/{hospital_id}/drdrCard/deposit', dd_card_proceed)
    app.add_route(route_mp + '/hospital/{hospital_id}/virtualaccount/payment/paypal/deposit', payment_paypal_deposit)
    app.add_route(route_mp + '/hospital/{hospital_id}/paypal/{user_id}/process', payment_paypal_proceed)
    app.add_route(route_mp + '/hospital/{hospital_id}/paypal/{user_id}/cancel', payment_paypal_cancel)
    app.add_route(route_mp + '/hospital/{hospital_id}/virtualaccount/transactionhistory/get', transactionhistory_get)
    app.add_route(route_mp + '/hospital/{hospital_id}/virtualaccount/transaction/{id}/get', specific_transaction_detail)

    doctor_hospital_payment_summary = account_virtual_api('doctor_hospital_payment_summary', pool_conn, var_conf)

    app.add_route(route_mp + '/hospital/{hospital_id}/v3/payment/summary/get', doctor_hospital_payment_summary)
    app.add_route(route_wd + '/hospital/{hospital_id}/v3/payment/summary/get', doctor_hospital_payment_summary)
    
    doctor_hospital_payment_statement_daily_detail = account_virtual_api('doctor_hospital_payment_statement_daily_detail', pool_conn, var_conf)


    app.add_route(route_mp + '/hospital/{hospital_id}/v3/payment/full/get', doctor_hospital_payment_statement_daily_detail)
    app.add_route(route_wd + '/hospital/{hospital_id}/v3/payment/full/get', doctor_hospital_payment_statement_daily_detail)
    



    # User website
    app.add_route(route_wd + '/user/login', user_login)
    app.add_route(route_wd + '/user/facebook_login', user_facebook_login)
    app.add_route(route_wd + '/user/logout', user_logout)
    app.add_route(route_wd + '/user/register', user_register)
    app.add_route(route_wd + '/user/register/v2', user_register_v2)
    app.add_route(route_wd + '/user/register/v2/editprofile', user_register_v2_editprofile)
    app.add_route(route_wd + '/user/register/v2/uploaddocs', user_register_v2_uploaddocs)
    app.add_route(route_wd + '/user/activate/sms', user_active_sms)
    app.add_route(route_wd + '/user/activate/requestSMS', user_request_sms)
    app.add_route(route_wd + '/user/password/requestNew', user_request_password)
    app.add_route(route_wd + '/user/password/change', user_change_password)
    app.add_route(route_wd + '/user/activate/updatePhone', user_update_phone)
    app.add_route(route_wd + '/user/goOnline', profile_doctor_set_online)
    app.add_route(route_wd + '/user/goOffline', profile_doctor_set_offline)
    app.add_route(route_wd + '/user/checkOnline', profile_doctor_get_online_status)
    app.add_route(route_wd + '/user/profile/documents/get', profile_doctor_get_documents)
    # WD2.14 API for checking email existence
    app.add_route(route_wd + '/user/checkemail', profile_doctor_check_email_existence)
    # WD2.15 API for checking phone existence
    app.add_route(route_wd + '/user/checkphone', profile_doctor_check_phone_existence)
    # WD2.17 API for listing the documents of an inactivated doctor account
    app.add_route(route_wd + '/user/register/v2/getdocs', user_register_v2_getdocs)
    # WD2.18 API for deleting a specific document of an inactivated doctor account
    app.add_route(route_wd + '/user/register/v2/deletedoc', user_register_v2_deletedoc)
    app.add_route(route_wd + '/user/profile/documents/delete', user_documents_delete)
    app.add_route(route_wd + '/user/profile/documents/upload', user_documents_upload)
    # WD2.19 API for sending application form
    app.add_route(route_wd + '/user/register/v2/sendtheform', user_register_v2_sendtheform)
    # WD2.20 API for doctor to go online with a specific duration
    app.add_route(route_wd + '/user/goOnline/v2', doctor_goOnline_v2)


    # Metadata website
    app.add_route(route_wd + '/metadata/get', get_metadata)
    app.add_route(route_wd + '/metadata/check', check_metadata)
    app.add_route(route_wd + '/systemTime/getCurrent', get_current_time)



    # Tokbox website
    app.add_route(route_wd + '/videocall/register', tokbox_register)
    app.add_route(route_wd + '/videocall/deregister', tokbox_deregister)
    app.add_route(route_wd + '/videocall/init', tokbox_init)
    app.add_route(route_wd + '/videocall/call', tokbox_call)
    app.add_route(route_wd + '/videocall/call/v2', tokbox_call_v2)
    app.add_route(route_wd + '/videocall/end', tokbox_end)
    app.add_route(route_wd + '/videocall/deny', tokbox_deny)
    app.add_route(route_wd + '/videocall/accept', tokbox_accept)



    # Profile website
    app.add_route(route_wd + '/profile/get', profile_doctor_get)
    app.add_route(route_wd + '/profile/{setting_type}/setup', profile_doctor_setup)
    app.add_route(route_wd + '/profile/avatar/upload', profile_doctor_avatar_upload)
    app.add_route(route_wd + '/profile/reviews/avgRate/get', profile_reviews_avgRate_get)
    app.add_route(route_wd + '/profile/reviews/get', profile_reviews_get)
    app.add_route(route_wd + '/patient/interactedList/get', get_list_patient_interacted)
    app.add_route(route_wd + '/patient/profile/{patient_id}/get', profile_patient_get_details_by_id)
    app.add_route(route_wd + '/patient/profile/{patient_id}/gethistory', profile_doctor_get_patient_history)
    # WD5.4 API for doctor to add note and document of medical record field to specific patient
    app.add_route(route_wd + '/patient/profile/medicalrecord/submit', profile_doctor_add_note_document)
    # WD5.5 API for doctor to get patient's note and attachment list of specific medical record field 
    app.add_route(route_wd + '/patient/profile/medicalrecord/get', profile_patient_get_medical_record)

    # WD3.9 API for doctor to remove his avatar    
    app.add_route(route_wd + '/profile/avatar/remove', profile_doctor_avatar_remove)

    # Schedule
    # WD 6.1
    app.add_route(route_wd + '/availability/singleDay/set', doctor_schedule_set_singleday_availability)
    # WD 6.2
    app.add_route(route_wd + '/availability/dailyRepeat/set', doctor_schedule_repeat_day_availability)
    # WD 6.3
    app.add_route(route_wd + '/availability/weeklyRepeat/set', doctor_schedule_repeat_week_availability)
    # WD 6.4
    app.add_route(route_wd + '/availability/monthlyRepeat/set', doctor_schedule_repeat_monthly_availability)
    # WD 6.5 API for doctor to get the previous appointment list
    app.add_route(route_wd + '/availabilityList/dateRange/get', doctor_schedule_get_availability_daterange)
    # WD 6.6 API for doctor to remove specific availability of corresponding date
    app.add_route(route_wd + '/availability/ofDate/remove', doctor_schedule_remove_availability)
    # WD 6.7 API for doctor to edit specific availability of corresponding date
    app.add_route(route_wd + '/availability/ofDate/edit', doctor_schedule_edit_availability)
    # WD 6.8
    app.add_route(route_wd + '/appointmentList/dateRange/get', doctor_get_all_the_oppointment_list_in_date_range)
    # WD 6.9 API for doctor to get the upcoming appointment list
    app.add_route(route_wd + '/apmt/upcominglist/patient/{patient_id}/get',doctor_get_appointment_upcominglist)
    # WD 6.10 API for doctor to get the previous appointment list
    app.add_route(route_wd + '/apmt/previouslist/patient/{patient_id}/get',doctor_get_appointment_previouslist)
    # WD 6.12 API for doctor to add document of medical record field for specific appointment
    app.add_route(route_wd + '/appointment/medicalRecord/submit', medical_record_submit)
    # WD 6.13 API for doctor to get patient's note list of specific medical record field
    app.add_route(route_wd + '/appointment/medicalRecord/get', medical_record_get)
    # WD 6.14 API for doctor to cancel the specificic appointment
    app.add_route(route_wd + '/appointment/{apm_id}/cancel', doctor_cancel_the_specificic_appointment)
    # WD 6.15 API for doctor to suspend the specificic appointment
    app.add_route(route_wd + '/appointment/{apm_id}/suspend', doctor_suspend_the_specificic_appointment)
    # WD 6.16 API for doctor to complete the specificic appointment
    app.add_route(route_wd + '/appointment/{apm_id}/complete', doctor_complete_the_specificic_appointment)
    # WD 6.17 API for doctor to mark the specificic appointment as free
    app.add_route(route_wd + '/appointment/{apm_id}/markasfree', doctor_mark_the_specific_appointment_as_free)
    # WD 6.18 API for doctor to submit consultation note of specific appointment
    app.add_route(route_wd + '/appointment/{apm_id}/note/submit', appointment_note_submit)
    # WD 6.19 API for doctor to get consultation note of specific appointment
    app.add_route(route_wd + '/appointment/{apm_id}/note/get', appointment_note_get)
    # WD 6.20 API for doctor to get specific appointment details
    app.add_route(route_wd + '/appointment/{apm_id}/get', doctor_get_appointment_details)
    # WD 6.21 API for doctor to submit document to specific appointment
    app.add_route(route_wd + '/appointment/{apm_id}/document/submit', appointment_document_submit)
    # WD6.22 API for doctor to delete specific document of corresponding appointment
    app.add_route(route_wd + '/appointment/{apm_id}/document/{doc_id}/delete', delete_the_specific_appointment_document)
    # WD6.23 API for doctor to confirm a specific appointment
    app.add_route(route_wd + '/appointment/{apm_id}/confirm', doctor_confirm_the_specific_appointment)
    # WD6.25 API for doctor to check appointment existence in specific availability
    app.add_route(route_wd + '/availability/checkApmtExistence', doctor_check_appointment_existence)


    doctor_get_appointment_details_hospital = appointment_api('doctor_get_appointment_details_hospital', pool_conn, var_conf)
    app.add_route(route_wd + '/hospital/{hospital_id}/appointment/{apm_id}/get', doctor_get_appointment_details_hospital)

    # Dashboard
    # WD7.1 API for getting dashboard summaryWD
    app.add_route(route_wd + '/dashboard/summary/get', dashboard_get_summary)
    # WD7.2 API for getting appointment list of specific type
    app.add_route(route_wd + '/dashboard/appointmentList/get', dashboard_get_appointment_list)
    # WD7.3 API for getting top patient list
    app.add_route(route_wd + '/dashboard/topPatientList/get', dashboard_get_top_patient_list)
    # WD7.4 API for getting availability status of doctor by month
    app.add_route(route_wd + '/dashboard/dailyStatusList/get', dashboard_get_doctor_availability_status_by_month)



    app.add_route(route_wd + '/stripe/creditCard/add', payment_stripe_add_card)
    app.add_route(route_wd + '/stripe/creditCard/remove', payment_stripe_remove_card)
    app.add_route(route_wd + '/stripe/creditCard/getlist', payment_stripe_get_list_card)


    # WD8.1 API for doctor to get his/her virtual account balance
    app.add_route(route_wd + '/virtualaccount/balance/get', balance_get)
    # WD8.2 API for doctor to get payment statement summary
    app.add_route(route_wd + '/payment/summary/get', payment_summary)
    # WD8.3 API for doctor to get payment statement daily detail
    app.add_route(route_wd + '/payment/daily/get', payment_statement_daily_detail)
    # WD8.4 API for doctor to get payment detail of specific appointment
    app.add_route(route_wd + '/payment/appointment/{apm_id}/get', payment_appointment_get)
    # WD8.8 API for doctor to add bank account
    app.add_route(route_wd + '/bankaccount/add', profile_doctor_add_bank_account)
    # WD8.9 API for doctor to add bank account
    app.add_route(route_wd + '/bankaccount/{bank_id}/setasdefault', profile_doctor_set_bank_account_default)
    # WD8.10 API for doctor to get bank account list
    app.add_route(route_wd + '/bankaccount/getList', profile_doctor_get_bank_account)
    # WD8.11 API for doctor to remove bank account
    app.add_route(route_wd + '/bankaccount/{bank_id}/remove', profile_doctor_remove_bank_account)
    # WD8.12 API for doctor to get payout list
    app.add_route(route_wd + '/payout/list/get', profile_doctor_get_payout_list)

    # Website admin
    app.add_route(route_wa + '/virtualaccount/get', admin_virtualaccount_get)
    app.add_route(route_wa + '/metadata/get', get_metadata)
    app.add_route(route_wa + '/metadata/check', check_metadata)
    app.add_route(route_wa + '/systemTime/getCurrent', get_current_time)
    app.add_route(route_wa + '/admin/login/v2', admin_user_login_v2)
    app.add_route(route_wa + '/payment/{profile_id}/getBankAccountList', admin_get_bank_account)
    app.add_route(route_wa + '/payment/createPayout', admin_create_doctor_payout)
    app.add_route(route_wa + '/payment/updatePayout', admin_update_doctor_payout)
    app.add_route(route_wa + '/doctor/{user_id}/activate', admin_active_user_account)

    #website admin with wd
    app.add_route(route_wd + '/hospital/virtualaccount/get', admin_virtualaccount_get)
    app.add_route(route_wd + '/hospital/metadata/get', get_metadata)
    app.add_route(route_wd + '/hospital/metadata/check', check_metadata)
    app.add_route(route_wd + '/hospital/systemTime/getCurrent', get_current_time)
    app.add_route(route_wd + '/hospital/admin/login/v2', admin_user_login_v2)
    app.add_route(route_wd + '/hospital/payment/{profile_id}/getBankAccountList', admin_get_bank_account)
    app.add_route(route_wd + '/hospital/payment/createPayout', admin_create_doctor_payout)
    app.add_route(route_wd + '/hospital/payment/updatePayout', admin_update_doctor_payout)
    app.add_route(route_wd + '/hospital/doctor/{user_id}/activate', admin_active_user_account)

    app.add_route(route_wd + '/hospital/admin/doctor/fulllist/get', admin_get_dr_fulllist)
    app.add_route(route_wd + '/hospital/admin/doctor/update', admin_doctor_update_profile)
    app.add_route(route_wd + '/hospital/admin/doctor/update/experiences', admin_doctor_update_profile_experiences)

    app.add_route(route_wd + '/hospital/admin/doctor/deactive', admin_doctor_deactive)
    app.add_route(route_wd + '/hospital/admin/doctor/active', admin_doctor_active)

    
    app.add_route(route_wd + '/hospital/admin/doctor/calllogs', admin_doctor_call_logs)
    
    
    app.add_route(route_wd + '/hospital/admin/calllogs', admin_call_logs)
    
    app.add_route(route_wd + '/hospital/admin/doctor/avatar/upload', admin_doctor_avatar_upload)
    
    app.add_route(route_wd + '/hospital/admin/doctor/avatar/remove', admin_doctor_avatar_remove)

    
    app.add_route(route_wd + '/hospital/admin/doctor/add', admin_add_doctor)

    admin_dd_card_generate =  hospital_api('admin_dd_card_generate', pool_conn, var_conf)
    app.add_route(route_wd + '/hospital/admin/ddcard/generate', admin_dd_card_generate)

    
    dd_card_check_pin = account_virtual_api('get_dd_pin_role', pool_conn, var_conf)
    app.add_route(route_mp + '/drdrCard/check/pin', dd_card_check_pin)


    update_locale_user_account = hospital_api('update_locale_user_account', pool_conn, var_conf)
    get_locale_user_account = hospital_api('get_locale_user_account', pool_conn, var_conf)
    
    
    app.add_route(route_wd + '/hospital/{hospital_id}/locale/get', get_locale_user_account)
    
    app.add_route(route_wd + '/hospital/{hospital_id}/locale/update', update_locale_user_account)
    
    app.add_route(route_mp + '/hospital/{hospital_id}/locale/get', get_locale_user_account)
    
    app.add_route(route_mp + '/hospital/{hospital_id}/locale/update', update_locale_user_account)
    


    return app