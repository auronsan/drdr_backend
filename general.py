#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import string
import random
import time
from datetime import datetime
import math
import paho.mqtt.client as paho
import ssl
import json
import re
import gettext
import os
from raven import Client
import ConfigParser

logger = logging.getLogger('drdr_api')
localedir = os.path.join(os.path.abspath(os.path.dirname(__file__)))
config = ConfigParser.RawConfigParser(allow_no_value=True)
config.read(localedir + '/' + "environment.conf")
sentry_key = config.get('environment', 'sentry_key')
client_sentry = Client(sentry_key)


class general_business(object):
    en = gettext.translation('test', localedir=localedir+'/languages', languages=['en'], fallback=True)
    vi = gettext.translation('vi', localedir=localedir+'/languages', languages=['vi'], fallback=True)
    en_ = en.ugettext
    vi_ = vi.ugettext

    def __init__(self, erp_connection, var_contructor):
        self.ERP_DB_HOST = var_contructor['ERP_DB_HOST']
        self.ERP_DB_PORT = var_contructor['ERP_DB_PORT']
        self.ERP_DB_USER = var_contructor['ERP_DB_USER']
        self.ERP_DB_PASS = var_contructor['ERP_DB_PASS']
        self.ERP_DB_NAME = var_contructor['ERP_DB_NAME']
        self.pool_conn = erp_connection

    def sql_execute(self, pool_conn, sql, commit=False, para_values=False):
        try:
            conn = self.pool_conn.connect()
            cur = conn.cursor()
            try:
                if para_values:
                    cur.execute(sql, para_values)
                else:
                    cur.execute(sql)
            except Exception as e:
                logger.error('error_execute_sql %s - %s' % (str(e), sql))
                client_sentry.captureException()
                try:
                    conn.close()
                except Exception as e:
                    logger.error('error_close_conn_execute_sql %s' % str(e))
                    conn = self.pool_conn.connect()
                    cur = conn.cursor()
                    cur.execute(sql, para_values)
            if 'delete ' in sql or 'DELETE ' in sql or 'update ' in sql or 'UPDATE ' in sql or 'insert ' in sql or 'INSERT ' in sql:
                res = []
                if 'RETURNING ' in sql or 'returning ' in sql:
                    res = cur.fetchall()
                if commit:
                    conn.commit()
                return res
            else:
                res = cur.fetchall()
                return res
        except Exception as e:
            logger.error('error_execute_sql %s - %s' % (str(e), sql))
            client_sentry.captureException()
            raise
        try:
            conn.close()
        except Exception as e:
            logger.error('error_close_conn_general_execute %s' % str(e))

    def get_system_param(self, key):
        conn = self.pool_conn.connect()
        cur = conn.cursor()
        sql = ("""select id,
                    code,
                    value
                from system_params
                where code = '%s' """ % (key))
        cur.execute(sql)
        res = cur.fetchall()
        try:
            conn.close()
        except Exception as e:
            logger.error('error_close_conn_get_system_param %s' % str(e))

        if res:
            try:
                res_ = eval(res[0][2])
            except:
                res_ = res[0][2]
            return res_
        else:
            return False

    def validate_user_token(self, token, client_id, update_status = True):
        conn = self.pool_conn.connect()
        cur = conn.cursor()
        if not client_id or not token:
            return []
        sql = """select uat.id token_id, --0
                        ua.id user_id, --1
                        token, --2
                        validation_status, --3
                        activity_status, --4
                        call_register, --5
                        type_profile, --6
                        ua.user_mngment_id, --7
                        pr.first_name, --8
                        pr.middle_name, --9
                        pr.last_name, --10
                        pr.profile_image_url, --11
                        pr.gender, --12
                        pr.birthday, --13
                        pr.title, -- 14
                        pr.degree_title --15
                from user_account ua
                    left join user_account_token uat on uat.user_account_id = ua.id
                    left join user_mngment pr on pr.id = ua.user_mngment_id
                where token = %s and client_id = %s and client_id is not null"""
        cur.execute(sql,[token, client_id])
        res = cur.fetchall()

        if res:
            if update_status:
                sql = '''update user_account set latest_activity_time = now() where id = %s ''' %res[0][1]
                cur.execute(sql)
                conn.commit()
                try:
                    conn.close()
                except Exception as e:
                    logger.error('error_close_conn_latest_activity_time %s' % str(e))

            return res[0]
        else:
            try:
                conn.close()
            except Exception as e:
                logger.error('error_close_conn_validate_user_token %s' % str(e))
            return []

    def randompassword(self):
        charsUpper = ''.join(random.choice(string.ascii_uppercase) for x in range(1))
        charsLower = ''.join(random.choice(string.ascii_lowercase) for x in range(4))
        charsDigits = ''.join(random.choice(string.digits) for x in range(1))
        stringPass = charsLower + charsUpper + charsDigits
        return (stringPass)

    def bytesToSize(self, fileSize=0):
        sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB']
        try:
            fileSize = int(fileSize)
            return "%.2f %s" % (fileSize / math.pow(1024, 2), sizes[2])
        except:
            return '0.0 MB'

    def publish_message_mqtt(self, topic, current_type_user, payload={}, qos=2, context={}):
        try:
            doctor_id = 0
            _ = self.get_lang_instance(context.get('lang','en'))
            if not payload and context.get('action', False) and context.get('appointmentId', False):
                action_mess = {
                    'patient_create_appointment': 'A patient just booked an appointment with you',
                    'patient_cancel_appointment': 'A patient just canceled an appointment with you',
                    'patient_reschedule_appointment': 'A patient just rescheduled an appointment with you',
                    'doctor_confirm_appointment': 'A doctor just confirmed an appointment with you',
                    'doctor_cancel_appointment': 'A doctor just canceled an appointment with you',
                    'doctor_suspend_appointment': 'A doctor just suspended an appointment with you',
                    'doctor_complete_appointment': 'A doctor just completed an appointment with you',
                    'doctor_markasfree_appointment': 'A doctor just completed an appointment with you (free)',
                }
                action_time = int(time.mktime(datetime.now().timetuple()))
                payload = {
                    "action": context['action'],
                    "message": _(action_mess[context['action']]),
                    "actionTime": action_time,
                    "appointmentId": context['appointmentId'],
                }
                sql = '''select title,--0
                                first_name,--1
                                middle_name,--2
                                first_name,--3
                                profile_image_url,--4
                                birthday,--5
                                gender,--6
                                city,--7
                                schedule_date, --8
                                apm.doctor_id, --9
                                apm.patient_id --10
                        from appointment apm
                        left join user_account uc on uc.id = apm.%s_id
                        left join user_mngment um on um.id = uc.user_mngment_id
                        left join user_contact uct on uct.user_mngment_id = um.id
                        left join user_address ua on ua.user_contact_id = uct.id
                        where apm.id = %s
                        and ua.is_primary = True''' % (current_type_user,
                                                       context['appointmentId'])
                conn = self.pool_conn.connect()
                cur = conn.cursor()
                cur.execute(sql)
                res = cur.fetchall()
                if res:
                    scheduleTime = int(time.mktime(res[0][8].timetuple()))
                    payload.update({'scheduleTime': scheduleTime})
                    dayOfBirth = res[0][5] and int(time.mktime(res[0][5].timetuple())) or None
                    user_info = {
                        "title": None if res[0][0] in ('False', '', 'None') else res[0][0],
                        "firstName": None if res[0][1] in ('False', '', 'None') else res[0][1],
                        "middleName": None if res[0][2] in ('False', '', 'None') else res[0][2],
                        "lastName": None if res[0][3] in ('False', '', 'None') else res[0][3],
                        "avatarUrl": None if res[0][4] in ('False', '', 'None') else res[0][4],
                        "dayOfBirth": dayOfBirth,
                        "genderCode": None if res[0][6] in ('False', '', 'None') else res[0][6],
                        "cityCode": None if res[0][7] in ('False', '', 'None') else res[0][7],
                    }
                    if current_type_user == 'patient':
                        payload.update({'patient': user_info})
                    else:
                        payload.update({'doctor': user_info})
                    doctor_id = res[0][9]
                    patient_id = res[0][10]

            mqtt_config = json.loads(config.get('environment', 'mqtt_config'))
            mqtt_host = mqtt_config['host']
            mqtt_port = mqtt_config['port']
            authen_ssl = mqtt_config.get('authen_ssl',False)
            client = paho.Client()
            if authen_ssl == True:
                client = paho.Client(transport='websockets')
                client.tls_set(localedir + '/' + 'tokbox/ca_file.crt', cert_reqs=ssl.CERT_NONE)
                client.tls_insecure_set(True)
            client.connect(mqtt_host, mqtt_port, 60)
            client.loop_start()

            if current_type_user == 'patient':
                sql_select = '''select ios_device_token, droid_device_token from user_account where id = %s'''%(doctor_id)
                cur.execute(sql_select)
                token = cur.fetchall()
                print token
                ios_device_token = token[0][0]
                droid_device_token = token[0][1]
                if ios_device_token and ios_device_token not in ('','False','NULL'):
                    ios_apns = json.loads(config.get('environment', 'ios_apns'))
                    is_sandbox = ios_apns["sandbox"]
                    from apns import APNs, Payload
                    apns = APNs(use_sandbox=is_sandbox, cert_file="DOCTORAPP.pem", enhanced=False)
                    ios_payload = Payload(alert=json.dumps(payload),sound="default", badge=1,content_available=True)
                    apns.gateway_server.send_notification(ios_device_token, ios_payload)
                
                if droid_device_token and droid_device_token not in ('','False','NULL'):
                    fcm_key = config.get('environment', 'fcm_key')
                    from pyfcm import FCMNotification
                    push_service = FCMNotification(api_key=fcm_key)
                    result = push_service.single_device_data_message(registration_id=droid_device_token, data_message=payload)

            else:
                sql_select = '''select ios_device_token, droid_device_token from user_account where id = %s'''%(patient_id)
                cur.execute(sql_select)
                token = cur.fetchall()
                print token
                ios_device_token = token[0][0]
                droid_device_token = token[0][1]
                if ios_device_token and ios_device_token not in ('','False','NULL'):
                    ios_apns = json.loads(config.get('environment', 'ios_apns'))
                    is_sandbox = ios_apns["sandbox"]
                    from apns import APNs, Payload
                    apns = APNs(use_sandbox=is_sandbox, cert_file="tokbox/VOIP.pem", enhanced=False)
                    ios_payload = Payload(alert=json.dumps(payload))
                    apns.gateway_server.send_notification(ios_device_token, ios_payload)
                
                if droid_device_token and droid_device_token not in ('','False','NULL'):
                    fcm_key = config.get('environment', 'fcm_key')
                    from pyfcm import FCMNotification
                    push_service = FCMNotification(api_key=fcm_key)
                    result = push_service.single_device_data_message(registration_id=droid_device_token, data_message=payload)

            client.publish(topic, json.dumps(payload), qos=qos)
            client.loop_stop()
            client.disconnect()
            return True
        except Exception as e:
            logger.error('error_publish_message_mqtt %s' % str(e))
            client_sentry.captureException()
        return False

    def isPwdValid(self, pwd=''):
        if len(pwd) < 6:
            return False
        # matchObj = re.match(r"^.*(?:([0-9]+)|([A-Z]+)).*(?:([0-9]+)|([A-Z]+)).*$", pwd)
        hasNumber = False
        hasUpper = False
        for x in pwd:
            if x.isupper()==True:
                hasUpper = True
            elif x.isalpha()==False:
                hasNumber = True
        
        if hasNumber and hasUpper:
            return True
        else:
            return False

    def get_lang_instance(self, lang):
        if lang == 'vi':
            _ = self.vi_
        else:
            _ = self.en_
        return _

    def get_header_request(self, header):
        token = header.get('X-User-Token'.upper(), False)
        client_version = header.get('X-App-Client-Version'.upper(), False)
        client_type = header.get('X-App-Client-Type'.upper(), False)
        content_type = header.get('Content-Type'.upper(), False)
        lang = header.get('Accept-Language'.upper(), False)
        client_id = header.get('X-Client-ID'.upper(), '')
        return {
            'user_token': token,
            'client_version': client_version,
            'client_type': client_type,
            'content_type': content_type,
            'lang': lang,
            'client_id': client_id
        }


