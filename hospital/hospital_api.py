#!/usr/bin/env python
# -*- coding: utf-8 -*-
import falcon
import json
import sys
from datetime import datetime
from hospital_business import hospital_business
import ConfigParser
import os
import math
project_path =  str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
config = ConfigParser.RawConfigParser(allow_no_value=True)
config.read(project_path + '/' + "environment.conf")
ddcard_config = json.loads(config.get('environment', 'ddcard_config'))
sys.path.insert(0, "..")
try:
    from general import general_business
    from aws_management import aws_management
except ImportError as e:
    print('No Import ', e)

AWS_BUCKET = config.get('environment', 's3_bucket')
BUCKET_URL_DEFAULT = AWS_BUCKET + '.' + config.get('environment', 's3_address')

status_code = {
    200: falcon.HTTP_200,
    401: falcon.HTTP_401,
    502: falcon.HTTP_502,
    203: falcon.HTTP_203
}


class hospital_api(object):
    def __init__(self, signal, erp_connection=False, var_contructor=False):
        self.business = hospital_business(signal, erp_connection, var_contructor)
        # self.aws = aws_management()
        self.pool_conn = erp_connection

        if var_contructor:
            self.ERP_DB_HOST = var_contructor['ERP_DB_HOST']
            self.ERP_DB_PORT = var_contructor['ERP_DB_PORT']
            self.ERP_DB_USER = var_contructor['ERP_DB_USER']
            self.ERP_DB_PASS = var_contructor['ERP_DB_PASS']
            self.ERP_DB_NAME = var_contructor['ERP_DB_NAME']

        self.signal = signal
        self.general_business = general_business(erp_connection, var_contructor)

    def on_get(self, req, resp, hospital_id=None):
        try:
            data = {}
            header = req.headers  # key cua header luon viet hoa
            all_header = self.general_business.get_header_request(header)
            lang = all_header.get('lang')
            _ = self.general_business.get_lang_instance(lang)
            data.update(all_header)

            if self.signal == 'hospital_get_dr_shortlist':
                resp_status = status_code[401]
                resp_body = {
                    "title": "Get doctor shortlist failed",
                    "msg": "An error occurred. Please try to get doctor shortlist again.",
                    "code": "get_doctor_shortlist_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        "hospital_id": hospital_id
                    })
                    record = self.business.hospital_get_dr_shortlist(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'hospital_dr_list_byspecicalty':
                resp_status = status_code[401]
                resp_body = {
                    "title": "Get doctor list failed",
                    "msg": "An error occurred. Please try to get doctor list again.",
                    "code": "get_doctor_list_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    specialtyCode = req.get_param('specialtyCode', default=False)
                    requestCount = req.get_param('requestCount', default=10)
                    nextItemId = req.get_param('nextItemId', default=0)

                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        "specialtyCode": specialtyCode,
                        "hospital_id": hospital_id,
                        "requestCount": requestCount,
                        "nextItemId": nextItemId,
                    })
                    record = self.business.hospital_dr_list_byspecicalty(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            elif self.signal == 'hospital_get_dr_fulllist':
                resp_status = status_code[401]
                resp_body = {
                    "title": "Get doctor full list failed",
                    "msg": "An error occurred. Please try to get doctor list again.",
                    "code": "get_doctor_list_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    requestCount = req.get_param('requestCount', default=10)
                    nextItemId = req.get_param('nextItemId', default=0)
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        "hospital_id": hospital_id,
                        "requestCount": requestCount,
                        "nextItemId": nextItemId,
                        "lang": lang
                    })
                    record = self.business.hospital_get_dr_fulllist(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            elif self.signal == 'admin_get_dr_fulllist':
                resp_status = status_code[401]
                resp_body = {
                    "title": "Get doctor full list failed",
                    "msg": "An error occurred. Please try to get doctor list again.",
                    "code": "get_doctor_list_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    requestCount = req.get_param('requestCount', default=10)
                    nextItemId = req.get_param('nextItemId', default=0)
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        "requestCount": requestCount,
                        "nextItemId": nextItemId,
                    })
                    record = self.business.admin_get_dr_fulllist(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
    

            # MP5.8 API for getting live doctor list (support pagination)
            elif self.signal == 'hospital_get_live_dr_list':
                resp_status = status_code[401]
                resp_body = {
                    "title": "Get live doctor list failed",
                    "msg": "An error occurred. Please try to get doctor list again.",
                    "code": "profile_patient_get_live_dr_list_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    requestCount = req.get_param('requestCount', default=10)
                    nextItemId = req.get_param('nextItemId', default=0)

                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        "requestCount": requestCount,
                        "hospital_id": hospital_id,
                        "nextItemId": nextItemId,
                    })
                    record = self.business.hospital_get_live_dr_list(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            elif self.signal == 'admin_call_logs':
                resp_status = status_code[401]
                resp_body = {
                    "title": "Get live callLogs list failed",
                    "msg": "An error occurred. Please try to get callLogs list again.",
                    "code": "callLogs"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    requestCount = req.get_param('requestCount', default=10)
                    nextItemId = req.get_param('nextItemId', default=0)

                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        "requestCount": requestCount,
                        "nextItemId": nextItemId,
                    })
                    record = self.business.admin_call_logs(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            
            elif self.signal == 'patient_call_logs':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Patient call logs"),
                    "msg": _("An error occurred. Please try to get Patient call logs again."),
                    "code": "patient_call_logs"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    requestCount = req.get_param('requestCount', default=10)
                    nextItemId = req.get_param('nextItemId', default=0)
                    
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'requestCount':requestCount,
                        'nextItemId':nextItemId,
                        'lang': lang,
                        'hospital_id': hospital_id
                    })
                    record = self.business.patient_call_logs(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'admin_dd_card_generate':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("admin_dd_card_generate logs"),
                    "msg": _("An error occurred. Please try to getadmin_dd_card_generate logs again."),
                    "code": "admin_dd_card_generate_logs"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    voucher = req.get_param('voucher', default=0)
                    number = req.get_param('number', default=0)
                    url =  ddcard_config['dd_admin_url']
                    api_key = ddcard_config['dd_admin_api_key']
                    param='?api_key=%s&auto=1&number=%s&voucher=%s'%(api_key, number, voucher)
                    gen_url= url + param
                    print gen_url
                    response = requests.get(gen_url)
                    if response:
                        print 'response ',response
                        res=response.json()
                        print 'res ',res
                        if res.get('code')== 200:
                            resp_body = {"code": "success"}
                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)    
        
            elif self.signal == 'get_locale_user_account':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("locale"),
                    "msg": _("An error occurred. Please try to get locale again."),
                    "code": "locale"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'hospital_id': hospital_id
                    })
                    record = self.business.get_locale_user_account(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                    resp_body = {
                    "title": _("locale"),
                    "msg": _("An error occurred. Please try to get locale again."),
                    "code": "locale",
                    "error": str(e),
                    "data" : data
                     }

                resp.status = resp_status
                resp.body = json.dumps(resp_body)



               
        except Exception as e:
            print str(e)

    def on_post(self, req, resp, hospital_id=None):
        try:
            data = {}
            header = req.headers  # key cua header luon viet hoa
            all_header = self.general_business.get_header_request(header)
            lang = all_header.get('lang')
            _ = self.general_business.get_lang_instance(lang)
            data.update(all_header)

            if self.signal == 'hospital_get_dr_list_by_sort_filter':
                resp_status = status_code[401]
                resp_body = {
                    "title": "Get doctor list by sort and filter criterias failed",
                    "msg": "An error occurred. Please try to get doctor list again.",
                    "code": "get_dr_list_by_sort_and_filter_criterias"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        "hospital_id": hospital_id
                    })
                    data.update(raw_data)
                    record = self.business.hospital_get_dr_list_by_sort_filter(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            
            # MP5.9 API for getting live doctor list with sort and filter criterias (support pagination)
            elif self.signal == 'hospital_get_live_dr_list_by_sort_filter':
                resp_status = status_code[401]
                resp_body = {
                    "title": "Get live doctor list by sort and filter criterias failed",
                    "msg": "An error occurred. Please try to get doctor list again.",
                    "code": "get_live_dr_list_by_sort_and_filter_criterias"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        "hospital_id": hospital_id
                    })
                    data.update(raw_data)
                    record = self.business.hospital_get_live_dr_list_by_sort_filter(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            elif self.signal == 'admin_doctor_update_profile':
                resp_status = status_code[401]
                resp_body = {
                    "title": "Get update doctor failed",
                    "msg": "An error occurred. Please try to update doctor again.",
                    "code": "post_doctor"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    data.update(raw_data)
                    record = self.business.admin_doctor_update_profile(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            elif self.signal == 'admin_doctor_update_profile_experiences':
                resp_status = status_code[401]
                resp_body = {
                    "title": "Get update admin_doctor_update_profile_experiences failed",
                    "msg": "An error occurred. Please try to update doctor again.",
                    "code": "post_doctor"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    data.update(raw_data)
                    record = self.business.admin_doctor_update_profile_experiences(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'admin_doctor_deactive':
                resp_status = status_code[401]
                resp_body = {
                    "title": "Deactive doctor failed",
                    "msg": "An error occurred. Please try to deactive doctor again.",
                    "code": "post_doctor"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    data.update(raw_data)
                    record = self.business.admin_doctor_deactive(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            elif self.signal == 'admin_doctor_active':
                resp_status = status_code[401]
                resp_body = {
                    "title": "active doctor failed",
                    "msg": "An error occurred. Please try to active doctor again.",
                    "code": "post_doctor"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    data.update(raw_data)
                    record = self.business.admin_doctor_active(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)    
            elif self.signal == 'admin_doctor_call_logs':
                resp_status = status_code[401]
                resp_body = {
                    "title": "call logs doctor failed",
                    "msg": "An error occurred. Please try to call logs doctor again.",
                    "code": "post_doctor"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    data.update(raw_data)
                    record = self.business.admin_doctor_call_logs(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)   

            elif self.signal == 'admin_doctor_avatar_upload':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Upload avatar failed"),
                    "msg": _("An error occurred. Please try to setup again."),
                    "code": "upload_avatar_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    avatar = req.get_param('avatar', default=False)
                    doctor_mngment_id = req.get_param('doctor_mngment_id', default=False)
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'avatar_url': '',
                        'lang': lang,
                        'doctor_mngment_id': doctor_mngment_id
                    })
                    record = []
                    if type(avatar).__name__ == 'instance':
                        # Write image to local
                        user_objs = self.general_business.validate_user_token(header.get("X-USER-TOKEN", False),data.get('client_id'))
                       
                        if user_objs and doctor_mngment_id:
                            aws = aws_management()
                            name, ext = os.path.splitext(avatar.filename)
                            if ext.lower() in ('.jpg', '.png', '.gif', '.jpeg'):
                                create_date = datetime.now().strftime('%Y%m%d%H%M%S')
                                filename = 'avatar_%s_%s_%s%s' % (user_objs[1], doctor_mngment_id, create_date, ext)
                                aws.upload_obj(avatar.file, AWS_BUCKET, filename)
                                data.update({'avatar_url': 'https://' + BUCKET_URL_DEFAULT + '/' + filename})
                            else:
                                data.update({'avatar_url': 'wrong_file_type'})
                        # End Write image to local

                        record = self.business.admin_doctor_avatar_upload(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'admin_doctor_avatar_remove':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Remove avatar failed"),
                    "msg": _("An error occurred. Please try to setup again."),
                    "code": "remove_avatar_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    doctor_mngment_id = req.get_param('doctor_mngment_id', default=False)
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                         'doctor_mngment_id': doctor_mngment_id
                    })
                    record = self.business.admin_doctor_avatar_remove(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            elif self.signal == 'admin_add_doctor':
                resp_status = status_code[200]
                resp_body = {
                    "code": "add_doctor_failed",
                    "title": _("add_doctor failed"),
                    "msg": _("An error occurred. Please try to add_doctor again.")
                }
                try:
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    data.update(raw_data)
                    record = self.business.admin_add_doctor(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']
                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            
            elif self.signal == 'update_locale_user_account':
                resp_status = status_code[401]
                resp_body = {
                    "title": "locale failed",
                    "msg": "An error occurred. Please try to locale again.",
                    "code": "post_locale"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    data.update(raw_data)
                    record = self.business.update_locale_user_account(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

        except Exception as e:
            print str(e)

