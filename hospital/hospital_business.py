# -*- coding: utf-8 -*-
from datetime import datetime
import time
import logging
import random
from raven import Client
import ConfigParser
from collections import OrderedDict
import sys
import os
import json
from dateutil.relativedelta import relativedelta
import sys
import smtplib
import threading

project_path =  str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
config = ConfigParser.RawConfigParser(allow_no_value=True)
config.read(project_path + '/' + "environment.conf")
sentry_key = config.get('environment', 'sentry_key')
client = Client(sentry_key)
logger = logging.getLogger('drdr_api')
sys.path.insert(0, "..")
try:
    from general import general_business
    from profile.profile_doctor_business import profile_doctor_business
except ImportError as e:
    print('No Import ', e)


class hospital_business(object):
    def __init__(self, signal, erp_connection, var_contructor):
        self.pool_conn = erp_connection

        self.ERP_DB_HOST = var_contructor['ERP_DB_HOST']
        self.ERP_DB_PORT = var_contructor['ERP_DB_PORT']
        self.ERP_DB_USER = var_contructor['ERP_DB_USER']
        self.ERP_DB_PASS = var_contructor['ERP_DB_PASS']
        self.ERP_DB_NAME = var_contructor['ERP_DB_NAME']
        self.general_business = general_business(erp_connection, var_contructor)
        
        self.doctor_profile = profile_doctor_business('',erp_connection, var_contructor)
        self.com_ratio = float(config.get('environment', 'com_ratio')) or 40.0
        self.min_commission = float(config.get('environment', 'min_commission')) or 40000.0
        self.vi_ = self.general_business.vi_
        self.en_ = self.general_business.en_

    def generate_pagination_list(self, user_id, table_pagination, context={}):
        gs = self.general_business
        pool = self.pool_conn
        full_list_ids = []

        def cache_list(full_list_ids, table_pagination, rand=False):
            if rand == True:
                random.shuffle(full_list_ids)
            if full_list_ids:
                sql_search = '''select id 
                                            from user_account_pagination
                                            where user_account_id = %s
                                                and table_pagination = %s '''
                exist = gs.sql_execute(pool, sql_search, para_values=(user_id,
                                                                      table_pagination))
                if exist:
                    sql_update = '''update user_account_pagination set list_id = %s
                                                where user_account_id = %s
                                                    and table_pagination = %s '''
                    gs.sql_execute(pool, sql_update, commit=True, para_values=(str(full_list_ids),
                                                                               user_id,
                                                                               table_pagination))

                else:
                    sql_insert = '''insert into user_account_pagination(user_account_id, list_id, table_pagination)
                                                values (%s,%s,%s)'''
                    gs.sql_execute(pool, sql_insert, commit=True, para_values=(user_id,
                                                                               str(full_list_ids),
                                                                               table_pagination))

            return full_list_ids

        if 'specialtyCode' in context:
            sql_list = '''select distinct dp.user_mngment_id 
                            from dr_doctorprofile_specialties dr
                            inner join doctor_profile dp on dp.id = dr.doctor_profile_id
                            inner join user_account ua on ua.user_mngment_id = dp.user_mngment_id
                            inner join calendar_event ce on ua.id = ce.user_id
                            where dr.specialties_code = %s and final_date > current_timestamp and dp.hospital_profile_id = %s '''
            res_list = gs.sql_execute(pool, sql_list, para_values=(context['specialtyCode'],context['hospital_id']))
            for rc in res_list:
                full_list_ids.append(int(rc[0]))
            full_list_ids = cache_list(full_list_ids, table_pagination, rand=True)
        elif 'api' in context and context['api'] == 'patient_call_logs' :
            sql_list = '''select min(ach.id),apm.id as apmId from appointment as apm INNER JOIN appointment_call_history as ach ON apm.id = ach.appointment_id INNER JOIN user_account ua on ua.id = apm.patient_id INNER JOIN patient_profile as pp ON ua.user_mngment_id = pp.user_mngment_id where apm.patient_id = %s and (apm.type = 'live') and pp.hospital_profile_id = %s and apm.booking_status IS NOT NULL group by apm.id order by apm.schedule_date desc ''' %( user_id,context['hospital_id'])
            res_list = gs.sql_execute(pool, sql_list)
            for rc in res_list:
                full_list_ids.append(int(rc[0]))
        elif 'full' in context:
            sql_list = '''select distinct um.id,  (COALESCE(dp.sum_rating_point,0) / COALESCE(dp.count_rating_point,1)) as rating_point, um.first_name, um.last_name,dp.consultation_fee
                            from user_mngment um
                            inner join doctor_profile dp on dp.user_mngment_id = um.id
                            inner join user_account ua on ua.user_mngment_id = um.id
                            where um.type_profile = 'doctor' and dp.hospital_profile_id = %s and ua.validation_status = 'active'  ''' %( context['hospital_id'])
            res_list = gs.sql_execute(pool, sql_list)
            for rc in res_list:
                full_list_ids.append(int(rc[0]))
            full_list_ids = cache_list(full_list_ids, table_pagination, rand=True)
        elif 'api' in context and context['api'] == 'MP5_7':
            sql_list = '''select distinct um.id,  (COALESCE(dp.sum_rating_point,0) / COALESCE(dp.count_rating_point,1)) as rating_point, um.first_name, um.last_name,dp.consultation_fee
                            from user_mngment um
                            inner join doctor_profile dp on dp.user_mngment_id = um.id
                            inner join user_account ua on ua.user_mngment_id = um.id
                            inner join calendar_event ce on ce.user_id = ua.id
                            where um.type_profile = 'doctor' and dp.hospital_profile_id = %s and ((dp.consultation_type = 'by_appointment' and ce.end_datetime > current_timestamp) or dp.consultation_type = 'live') ''' %( context['hospital_id'])

            if context['filterBy']:
                if context['filterBy'].get('specialtyCodes') or context['filterBy'].get('experienceYearRange'):
                    sql_list = '''select distinct um.id,  (COALESCE(dp.sum_rating_point,0) / COALESCE(dp.count_rating_point,1)) as rating_point, um.first_name, um.last_name,dp.consultation_fee
                                from user_mngment um
                                inner join doctor_profile dp on dp.user_mngment_id = um.id
                                left join dr_doctorprofile_specialties dr on dr.doctor_profile_id = dp.id
                                inner join user_account ua on ua.user_mngment_id = um.id
                                inner join calendar_event ce on ce.user_id = ua.id
                                where um.type_profile = 'doctor' and dp.hospital_profile_id = %s and ((dp.consultation_type = 'by_appointment' and ce.end_datetime > current_timestamp) or dp.consultation_type = 'live') ''' %( context['hospital_id'])

            if context['keyword']:
                whereLike = '''and (first_name ILIKE '%%%s%%' or last_name ILIKE '%%%s%%' )''' % (
                context['keyword'], context['keyword'])
                sql_list += whereLike

            sql_list += self.filter_doctors_by(context['filterBy'])
            sql_list += context['sortBy']
            res_list = gs.sql_execute(pool, sql_list)
            for rc in res_list:
                full_list_ids.append(int(rc[0]))
            full_list_ids = cache_list(full_list_ids, table_pagination)
        
        elif 'api' in context and context['api'] == 'MP5_9':
            sql_list = '''select distinct um.id,  (COALESCE(dp.sum_rating_point,0) / COALESCE(dp.count_rating_point,1)) as rating_point, um.first_name, um.last_name,dp.consultation_fee
                            from user_mngment um
                            inner join doctor_profile dp on dp.user_mngment_id = um.id
                            inner join user_account ua on ua.user_mngment_id = um.id
                            inner join calendar_event ce on ce.user_id = ua.id
                            where um.type_profile = 'doctor' and dp.consultation_type='live' and ce.final_date > current_timestamp and current_timestamp <= ua.checkout_time and ua.activity_status = 'online' and dp.hospital_profile_id = %s'''%( context['hospital_id'])

            if context['filterBy']:
                if context['filterBy'].get('specialtyCodes') or context['filterBy'].get('experienceYearRange'):
                    sql_list = '''select distinct um.id,  (COALESCE(dp.sum_rating_point,0) / COALESCE(dp.count_rating_point,1)) as rating_point, um.first_name, um.last_name,dp.consultation_fee
                                from user_mngment um
                                inner join doctor_profile dp on dp.user_mngment_id = um.id
                                left join dr_doctorprofile_specialties dr on dr.doctor_profile_id = dp.id
                                inner join user_account ua on ua.user_mngment_id = um.id
                                inner join calendar_event ce on ce.user_id = ua.id
                                where um.type_profile = 'doctor' and dp.consultation_type='live' and current_timestamp <= ua.checkout_time and ua.activity_status = 'online' and ua.activity_status = 'online' and dp.hospital_profile_id = %s'''%( context['hospital_id'])

            if context['keyword']:
                whereLike = '''and (first_name ILIKE '%%%s%%' or last_name ILIKE '%%%s%%' )''' % (
                context['keyword'], context['keyword'])
                sql_list += whereLike

            sql_list += self.filter_doctors_by(context['filterBy'])
            sql_list += context['sortBy']

            res_list = gs.sql_execute(pool, sql_list)
            for rc in res_list:
                full_list_ids.append(int(rc[0]))
            full_list_ids = cache_list(full_list_ids, table_pagination)
      
        elif 'api' in context and context['api'] == 'MP5_8':
            sql_list = '''select distinct um.id, um.first_name,
                            case when current_timestamp <= ua.checkout_time and ua.activity_status = 'online' then True
                            else False end onlineStatus --8
                            from user_mngment um
                            inner join user_account ua on ua.user_mngment_id = um.id
                            inner join doctor_profile dp on dp.user_mngment_id = um.id
                            where um.type_profile = 'doctor' and dp.consultation_type='live' and dp.hospital_profile_id = %s and ua.validation_status = 'active' 
                            order by onlineStatus desc, um.first_name asc''' %( context['hospital_id'])
            #sql_list = '''select distinct um.id, um.first_name
            #                from user_mngment um
            #                inner join user_account ua on ua.user_mngment_id = um.id
            #                inner join doctor_profile dp on dp.user_mngment_id = um.id
            #                inner join calendar_event ce on ce.user_id = ua.id
            #                where um.type_profile = 'doctor' and dp.consultation_type='live' and ce.final_date > current_timestamp and ua.latest_activity_time > current_timestamp - interval '15 minutes' and ua.activity_status = 'online' and dp.hospital_profile_id = %s
            #                order by um.first_name asc''' % (hospital_id)
            res_list = gs.sql_execute(pool, sql_list)
            for rc in res_list:
                full_list_ids.append(int(rc[0]))
            full_list_ids = cache_list(full_list_ids, table_pagination)
        elif 'api' in context and context['api'] == 'MP5_8A':
            sql_list = '''select distinct um.id, um.first_name
                            from user_mngment um
                            inner join user_account ua on ua.user_mngment_id = um.id
                            inner join doctor_profile dp on dp.user_mngment_id = um.id
                            where um.type_profile = 'doctor' and dp.consultation_type='live' and dp.hospital_profile_id = %s
                            order by um.first_name asc''' %( context['hospital_id'])
            #sql_list = '''select distinct um.id, um.first_name
            #                from user_mngment um
            #                inner join user_account ua on ua.user_mngment_id = um.id
            #                inner join doctor_profile dp on dp.user_mngment_id = um.id
            #                inner join calendar_event ce on ce.user_id = ua.id
            #                where um.type_profile = 'doctor' and dp.consultation_type='live' and ce.final_date > current_timestamp and ua.latest_activity_time > current_timestamp - interval '15 minutes' and ua.activity_status = 'online' and dp.hospital_profile_id = %s
            #                order by um.first_name asc''' % (hospital_id)
            res_list = gs.sql_execute(pool, sql_list)
            for rc in res_list:
                full_list_ids.append(int(rc[0]))
            full_list_ids = cache_list(full_list_ids, table_pagination)    
       
        return full_list_ids

    def filter_doctors_by(self, filterBy):
        sql_where = ''
        if filterBy:
            if filterBy.get('feeRange'):
                sql_where += '''and (consultation_fee >= %s and  consultation_fee <= %s ) ''' % (
                filterBy['feeRange']['min'].get('value'), filterBy['feeRange']['max'].get('value'))

            if filterBy.get('experienceYearRange'):
                sql_where += '''and (years_of_exp >= %s and  years_of_exp <= %s ) ''' % (
                filterBy['experienceYearRange'].get('min'), filterBy['experienceYearRange'].get('max'))

            if filterBy.get('rating'):
                rating = int(filterBy.get('rating'))
                if rating == 1:
                    sql_where += '''and (COALESCE(sum_rating_point,0) / COALESCE(count_rating_point,1)) <= %s ''' % (rating)
                elif rating == 2:
                    sql_where += '''and ( (COALESCE(sum_rating_point,0) / COALESCE(count_rating_point,1)) > 1 and (COALESCE(sum_rating_point,0) / COALESCE(count_rating_point,1)) <= %s )''' % (rating)
                elif rating == 3:
                    sql_where += '''and ( (COALESCE(sum_rating_point,0) / COALESCE(count_rating_point,1)) > 2 and (COALESCE(sum_rating_point,0) / COALESCE(count_rating_point,1)) <= %s )''' % (rating)
                elif rating == 4:
                    sql_where += '''and ( (COALESCE(sum_rating_point,0) / COALESCE(count_rating_point,1)) > 3 and (COALESCE(sum_rating_point,0) / COALESCE(count_rating_point,1)) <= %s )''' % (rating)
                elif rating == 5:
                    sql_where += '''and (COALESCE(sum_rating_point,0) / COALESCE(count_rating_point,1)) > 4 '''

            if filterBy.get('specialtyCodes'):
                sql_where += '''and specialties_code in (%s) ''' % (str(filterBy.get('specialtyCodes'))[1:-1])

            if filterBy.get('doctorSpokenLanguageCodes'):
                sql_where += '''and spoken_language in (%s) ''' % (str(filterBy.get('doctorSpokenLanguageCodes'))[1:-1])

            if filterBy.get('consultationType'):
                sql_where += '''and consultation_type = '%s' ''' % (filterBy.get('consultationType'))

        return sql_where

    def update_pagination_list(self, user_id, table_pagination, current_list, context={}):
        gs = self.general_business
        pool = self.pool_conn
        full_list_ids = current_list
        list_ids = []
        if 'specialtyCode' in context:
            sql_list = '''select distinct user_mngment_id 
                            from dr_doctorprofile_specialties dr
                            inner join doctor_profile dp on dp.id = doctor_profile_id
                            where specialties_code = %s 
                                and user_mngment_id not in %s and dp.hospital_profile_id = %s'''
            res_list = gs.sql_execute(pool, sql_list, para_values=(context['specialtyCode'], current_list, context['hospital_id']))
            for rc in res_list:
                list_ids.append(int(rc[0]))

            random.shuffle(list_ids)
            if list_ids:
                sql_update = '''update user_account_pagination set list_id = %s
                                where user_account_id = %s
                                    and table_pagination = %s '''
                gs.sql_execute(pool, sql_update, commit=True, para_values=(str(current_list + list_ids),
                                                                           user_id,
                                                                           table_pagination))
                full_list_ids.extend(list_ids)

        elif 'recordField' in context:
            patientId = context['patientId']
            recordField = context['recordField']
            if recordField:
                where = "and mr.code = '%s'" % recordField
            else:
                where = "and mr.code is not null"
            sql_list = '''select mr.id
                        from appointment_medical_record mr 
                          left join appointment apm on apm.id = mr.appointment_id
                          left join user_account ua on ua.id = apm.patient_id
                        where 1=1
                        %s
                          and ua.user_mngment_id = %s
                          and rm.id not in %s
                        order by mr.create_date desc ''' % (where,
                                                            patientId,
                                                            str(tuple(current_list)))
            res_list = gs.sql_execute(pool, sql_list)
            for rc in res_list:
                list_ids.append(int(rc[0]))

            if list_ids:
                sql_update = '''update user_account_pagination set list_id = %s
                                where user_account_id = %s
                                    and table_pagination = %s '''
                gs.sql_execute(pool, sql_update, commit=True, para_values=(str(current_list + list_ids),
                                                                           user_id,
                                                                           table_pagination))
                full_list_ids.extend(list_ids)

        elif 'doctor_id' in context:
            sql_list = '''SELECT ar.id FROM appointment ap 
                        INNER join user_account ua on ua.id = ap.doctor_id
                        INNER JOIN appointment_review ar ON ar.appointment_id = ap.id 
                        WHERE ua.user_mngment_id = %s and ar.id not in %s
                            ORDER BY ar.write_date DESC '''
            res_list = gs.sql_execute(pool, sql_list, para_values=(context['doctor_id'], tuple(current_list)))
            for rc in res_list:
                list_ids.append(int(rc[0]))

            if list_ids:
                sql_update = '''update user_account_pagination set list_id = %s
                                where user_account_id = %s
                                    and table_pagination = %s '''
                gs.sql_execute(pool, sql_update, commit=True, para_values=(str(current_list + list_ids),
                                                                           user_id,
                                                                           table_pagination))
                full_list_ids.extend(list_ids)
        return full_list_ids

    def hospital_dr_list_byspecicalty(self, data):
        logger.info('hospital_dr_list_byspecicalty %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get doctor list byspecicalty failed"),
                        "msg": _("An error occurred. Please try to get doctor list again."),
                        "code": "hospital_list_byspecicalty_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            specialtyCode = data.get('specialtyCode')
            requestCount = int(data.get('requestCount'))
            nextItemId = int(data.get('nextItemId'))
            hospital_id = int(data.get('hospital_id', 0))
            res = {
                'pagination': {
                    "resultCount": 0,
                    "totalCount": 0,
                    "nextItemId": 0
                },
                'doctorList': [],
            }
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                start = 0
                end = 0
                if res_token:
                    user_id = res_token[1]
                    if not nextItemId or nextItemId == 0:
                        select_list_ids = []
                        full_list_ids = self.generate_pagination_list(user_id, 'doctor_profile',
                                                                      context={'specialtyCode': specialtyCode})
                        start, end = 0, requestCount
                    else:
                        sql = '''select list_id 
                                from user_account_pagination
                                where user_account_id = %s
                                    and table_pagination = %s
                                '''
                        full_list_ids = gs.sql_execute(pool, sql, para_values=(user_id, 'doctor_profile'))
                        if full_list_ids:
                            full_list_ids = eval(full_list_ids[0][0])
                            start, end = full_list_ids.index(nextItemId), full_list_ids.index(nextItemId) + requestCount
                        else:
                            full_list_ids = self.generate_pagination_list(user_id, 'doctor_profile',
                                                                          context={'specialtyCode': specialtyCode})
                            start, end = 0, requestCount

                    if not full_list_ids:
                        resp.update({'status_code': 200,
                                     'resq_body': res
                                     })
                        return resp

                    end = end if end <= len(full_list_ids) else len(full_list_ids)
                    select_list_ids = full_list_ids[start:end]

                    res['pagination']['totalCount'] = len(full_list_ids)
                    res['pagination']['nextItemId'] = full_list_ids[end] if end < len(full_list_ids) else full_list_ids[
                        end - 1]
                    res['pagination']['resultCount'] = len(select_list_ids)

                    # get list_specialties
                    if len(select_list_ids) > 0:
                        select_list_ids = select_list_ids + [-1, -1]
                        sql = '''with list_doctor as
                                (
                                
                                select um.id profile_id,
                                        dp.id doctor_id, 
                                        COALESCE(um.profile_image_url,'') profile_image_url,
                                        COALESCE(um.title,'') title,
                                        COALESCE(um.first_name,'') first_name,
                                        COALESCE(um.middle_name,'') middle_name,
                                        COALESCE(um.last_name,'') last_name,
                                        consultation_type,
                                        drsp.specialties_code,
                                        mtsp.name specialties_name,
                                        (COALESCE(sum_rating_point,0)/COALESCE(count_rating_point,1))::float rating_point,
                                        case when current_timestamp <= ua.checkout_time and ua.activity_status = 'online' then True
                                            else False
                                        end onlineStatus,
                                        ua.activity_status
                                from doctor_profile dp
                                left join user_mngment um on um.id = dp.user_mngment_id
                                left join dr_doctorprofile_specialties drsp on drsp.doctor_profile_id = dp.id
                                left join meta_dr_specialties mtsp on code = specialties_code
                                left join user_account ua on ua.user_mngment_id = um.id
                                where um.id in %s and dp.hospital_profile_id = %s
                                )
                                select d.profile_id,--0
                                        d.profile_image_url,--1
                                        d.title,--2
                                        d.first_name,--3
                                        d.middle_name,--4
                                        d.last_name,--5
                                        d.consultation_type,--6
                                        d.rating_point, --7
                                        d.onlineStatus, --8
                                        d.activity_status, --9
                                        array_to_string(array_agg(distinct s.specialties_code), ', ') as list_specialties --10
                                from list_doctor d
                                left join dr_doctorprofile_specialties s on s.doctor_profile_id = d.doctor_id
                                group by 1,2,3,4,5,6,7,8,9,10''' % (str(tuple(select_list_ids)), hospital_id)

                        list_doctor = gs.sql_execute(pool, sql)
                        if list_doctor:
                            tmp_res = []
                            for dr in list_doctor:
                                list_sp = []
                                for sp in dr[10].split(', '):
                                    list_sp.append({'code': sp})
                                tmp_res.append({
                                    "id": dr[0],
                                    "avatar": dr[1],
                                    "titleCode": dr[2],
                                    "firstName": None if dr[3] in ('False', 'None', '') else dr[3],
                                    "middleName": None if dr[4] in ('False', 'None', '') else dr[4],
                                    "lastName": None if dr[5] in ('False', 'None', '') else dr[5],
                                    "specialtyCodes": list_sp,
                                    "isAvailableToCall": True if dr[6] == 'live' and dr[8] == True and dr[9] == 'online' else False,
                                    "ratingPoint": dr[7]
                                })
                            res['doctorList'] = tmp_res

                    resp.update({'status_code': 200,
                                 'resq_body': res
                                 })
                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
        except Exception as e:
            logger.error('profile_patient_dr_list_byspecicalty %s' % str(e))
            client.captureException()

        return resp

    
    def hospital_get_dr_fulllist(self, data):
        logger.info('hospital_dr_list_fulllist %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get doctor list fulllist failed"),
                        "msg": _("An error occurred. Please try to get doctor list again."),
                        "code": "hospital_fulllist_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            nextItemId = int(data.get('nextItemId', 0))
            requestCount = int(data.get('requestCount'))
            hospital_id = int(data.get('hospital_id', 0))
            if not requestCount:
                return resp

            res = {
                'pagination': {
                    "resultCount": 0,
                    "totalCount": 0,
                    "nextItemId": 0
                },
                'doctorList': [],
            }
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                start = 0
                end = 0
                if res_token:
                    user_id = res_token[1]
                    if not nextItemId or nextItemId == 0:
                        full_list_ids = self.generate_pagination_list(user_id, 'doctor_full_live',
                                                                      context={'api': 'MP5_8', 'hospital_id' : hospital_id})
                        start = 0
                    else:
                        sql = '''select list_id 
                                from user_account_pagination
                                where user_account_id = %s
                                    and table_pagination = '%s'
                                ''' % (user_id, 'doctor_full_live')
                        full_list_ids = gs.sql_execute(pool, sql)
                        if full_list_ids:
                            full_list_ids = eval(full_list_ids[0][0])
                            start = full_list_ids.index(nextItemId)
                        else:
                            full_list_ids = self.generate_pagination_list(user_id, 'doctor_full_live',
                                                                          context={'api': 'MP5_8', 'hospital_id' : hospital_id})
                            start = 0

                    if not full_list_ids:
                        resp.update({'status_code': 200,
                                     'resq_body': res
                                     })
                        return resp

                    if (start + requestCount) <= len(full_list_ids):
                        end = start + requestCount
                    else:
                        end = len(full_list_ids)

                    select_list_ids = full_list_ids[start:end]
                    res['pagination']['totalCount'] = len(full_list_ids)
                    res['pagination']['nextItemId'] = full_list_ids[end] if end < len(full_list_ids) else full_list_ids[
                        end - 1]
                    res['pagination']['resultCount'] = len(select_list_ids)

                    #print select_list_ids
                    # get list_specialties
                    if len(select_list_ids) > 0:
                        select_list_ids = select_list_ids + [-1, -1]
                        sql = '''with list_doctor as
                                (
                                select um.id profile_id,
                                        dp.id doctor_id, 
                                        COALESCE(um.profile_image_url,'') profile_image_url,
                                        COALESCE(um.title,'') title,
                                        COALESCE(um.first_name,'') first_name,
                                        COALESCE(um.middle_name,'') middle_name,
                                        COALESCE(um.last_name,'') last_name,
                                        dp.consultation_type,
                                        drsp.specialties_code,
                                        dp.consultation_fee,
                                        ua.activity_status,
                                        mtsp.name specialties_name,
                                        (COALESCE(dp.sum_rating_point,0) / COALESCE(dp.count_rating_point,1)) as rating_point, --7
                                        case when current_timestamp <= ua.checkout_time and ua.activity_status != 'offline' then True
                                            else False
                                        end onlineStatus --8
                                from doctor_profile dp
                                left join user_mngment um on um.id = dp.user_mngment_id
                                left join dr_doctorprofile_specialties drsp on drsp.doctor_profile_id = dp.id
                                left join meta_dr_specialties mtsp on mtsp.code = drsp.specialties_code
                                left join user_account ua on ua.user_mngment_id = um.id
                                where um.id in %s and dp.hospital_profile_id = %s and ua.validation_status = 'active' 
                                )
                                select d.profile_id,--0
                                        d.profile_image_url,--1
                                        d.title,--2
                                        d.first_name,--3
                                        d.middle_name,--4
                                        d.last_name,--5
                                        d.onlineStatus,--6
                                        d.rating_point, --7
                                        d.consultation_fee, -- 8
                                        array_to_string(array_agg(distinct s.specialties_code), ', ') as list_specialties, --9
                                        d.activity_status --10
                                from list_doctor d
                                left join dr_doctorprofile_specialties s on s.doctor_profile_id = d.doctor_id
                                group by 1,2,3,4,5,6,7,8,9,11
                                order by onlineStatus desc, first_name asc''' % (str(tuple(select_list_ids)), hospital_id)

                        list_doctor = gs.sql_execute(pool, sql)
                        if list_doctor:
                            tmp_res = []
                            for dr in list_doctor:
                                list_sp = []
                                for sp in dr[9].split(', '):
                                    list_sp.append({'code': sp})
                                tmp_res.append({
                                    "id": dr[0],
                                    "avatar": dr[1],
                                    "titleCode": dr[2],
                                    "firstName": None if dr[3] in ('False', 'None', '') else dr[3],
                                    "middleName": None if dr[4] in ('False', 'None', '') else dr[4],
                                    "lastName": None if dr[5] in ('False', 'None', '') else dr[5],
                                    "specialtyCodes": list_sp,
                                    "isAvailableToCall": dr[6],
                                    "status": dr[10],
                                    "ratingPoint": dr[7]
                                })
                            res['doctorList'] = tmp_res
                            res['pagination']['resultCount'] = len(tmp_res)

                    resp.update({'status_code': 200,
                                 'resq_body': res
                                 })
                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
        except Exception as e:
            logger.error('hospital_get_live_dr_fulllist %s' % str(e))

        return resp
    
    def admin_get_dr_fulllist(self, data):
        logger.info('hospital_dr_list_fulllist %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get doctor list fulllist failed"),
                        "msg": _("An error occurred. Please try to get doctor list again."),
                        "code": "hospital_fulllist_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            nextItemId = int(data.get('nextItemId', 0))
            requestCount = int(data.get('requestCount'))
            #sql = ''''''
            #full_list_ids = gs.sql_execute(pool, sql)
            
            hospital_id = 0
            if not requestCount:
                return resp

            res = {
                'pagination': {
                    "resultCount": 0,
                    "totalCount": 0,
                    "nextItemId": 0
                },
                'doctorList': [],
            }
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                start = 0
                end = 0
                if res_token:
                    user_id = res_token[1]
                    #waiting db updated
                    sql_hospital = ''' select hospital_profile_id from admin_profile where user_account_id = %s ''' % (user_id)
                    hospital_id  = gs.sql_execute(pool, sql_hospital)[0][0]
                    #hospital_id = 1
                    if not nextItemId or nextItemId == 0:
                        full_list_ids = self.generate_pagination_list(user_id, 'doctor_full_live',
                                                                      context={'api': 'MP5_8A', 'hospital_id' : hospital_id})
                        start = 0
                    else:
                        sql = '''select list_id 
                                from user_account_pagination
                                where user_account_id = %s
                                    and table_pagination = '%s'
                                ''' % (user_id, 'doctor_full_live')
                        full_list_ids = gs.sql_execute(pool, sql)
                        if full_list_ids:
                            full_list_ids = eval(full_list_ids[0][0])
                            start = full_list_ids.index(nextItemId)
                        else:
                            full_list_ids = self.generate_pagination_list(user_id, 'doctor_full_live',
                                                                          context={'api': 'MP5_8A', 'hospital_id' : hospital_id})
                            start = 0

                    if not full_list_ids:
                        resp.update({'status_code': 200,
                                     'resq_body': res
                                     })
                        return resp

                    if (start + requestCount) <= len(full_list_ids):
                        end = start + requestCount
                    else:
                        end = len(full_list_ids)

                    select_list_ids = full_list_ids[start:end]
                    res['pagination']['totalCount'] = len(full_list_ids)
                    res['pagination']['nextItemId'] = full_list_ids[end] if end < len(full_list_ids) else full_list_ids[
                        end - 1]
                    res['pagination']['resultCount'] = len(select_list_ids)

                    #print select_list_ids
                    # get list_specialties
                    if len(select_list_ids) > 0:
                        select_list_ids = select_list_ids + [-1, -1]
                        sql = '''with list_doctor as
                                (
                                select um.id profile_id,
                                        dp.id doctor_id, 
                                        COALESCE(um.profile_image_url,'') profile_image_url,
                                        COALESCE(um.title,'') title,
                                        COALESCE(um.first_name,'') first_name,
                                        COALESCE(um.middle_name,'') middle_name,
                                        COALESCE(um.last_name,'') last_name,
                                        dp.consultation_type,
                                        drsp.specialties_code,
                                        dp.consultation_fee,
                                        mtsp.name specialties_name,
                                        (COALESCE(dp.sum_rating_point,0) / COALESCE(dp.count_rating_point,1)) as rating_point, --7
                                        case when current_timestamp <= ua.checkout_time and ua.activity_status = 'online' then True
                                            else False
                                        end onlineStatus, --8
                                        ua.activity_status,
                                        ua.email,
                                        ua.phone, 
                                        ua.password, 
                                        dp.about,
                                        ua.validation_status,
                                        ua.latest_activity_time as last_active,
                                        dp.years_of_experience,
                                        dp.graduation_date,
                                        dp.medical_school,
                                        dp.post_graduate_training,
                                        dp.industry_relationship,
                                        dp.publications,
                                        dp.teaching,
                                        dp.community_service
                                from doctor_profile dp
                                left join user_mngment um on um.id = dp.user_mngment_id
                                left join dr_doctorprofile_specialties drsp on drsp.doctor_profile_id = dp.id
                                left join meta_dr_specialties mtsp on mtsp.code = drsp.specialties_code
                                left join user_account ua on ua.user_mngment_id = um.id
                                where um.id in %s and dp.hospital_profile_id = %s
                                )
                                select d.profile_id,--0
                                        d.profile_image_url,--1
                                        d.title,--2
                                        d.first_name,--3
                                        d.middle_name,--4
                                        d.last_name,--5
                                        d.onlineStatus,--6
                                        d.rating_point, --7
                                        d.consultation_fee, -- 8
                                        array_to_string(array_agg(distinct s.specialties_code), ', ') as list_specialties ,--9
                                        d.activity_status, --10
                                        d.email, --11
                                        d.phone, --12
                                        d.password, --13
                                        d.about, --14
                                        d.validation_status, --15
                                        d.last_active, --16
                                        d.years_of_experience, --17
                                        d.graduation_date, --18
                                        d.medical_school, --19
                                        d.post_graduate_training, --20
                                        d.industry_relationship,--21
                                        d.publications,--22
                                        d.teaching,--23
                                        d.community_service --24
                                from list_doctor d
                                left join dr_doctorprofile_specialties s on s.doctor_profile_id = d.doctor_id
                                group by 1,2,3,4,5,6,7,8,9,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25
                                order by first_name asc''' % (str(tuple(select_list_ids)), hospital_id)

                        list_doctor = gs.sql_execute(pool, sql)

                        if list_doctor:
                            tmp_res = []
                            for dr in list_doctor:
                                list_sp = []
                                for sp in dr[9].split(', '):
                                    list_sp.append({'code': sp})
                                tmp_res.append({
                                    "id": dr[0],
                                    "avatar": dr[1],
                                    "titleCode": dr[2],
                                    "firstName": None if dr[3] in ('False', 'None', '') else dr[3],
                                    "middleName": None if dr[4] in ('False', 'None', '') else dr[4],
                                    "lastName": None if dr[5] in ('False', 'None', '') else dr[5],
                                    "specialtyCodes": list_sp,
                                    "isAvailableToCall": dr[6],
                                    "ratingPoint": dr[7],
                                    "status": dr[10],
                                    "email": dr[11],
                                    "phone": dr[12],
                                    "password": dr[13],
                                    "about": dr[14],
                                    "validation": dr[15],
                                    "last_active": dr[16] and int(time.mktime(dr[16].timetuple())) or None,
                                    "yearsOfExperience": dr[17],
                                    "graduationDate": dr[18] and int(time.mktime(dr[18].timetuple())) or None,
                                    "medicalSchool": dr[19],
                                    "post_graduate_training" : dr[20],
                                    "industry_relationship" : dr[21],
                                    "publications" : dr[22],
                                    "teaching" : dr[23],
                                    "community_service" : dr[24]
                                })
                            res['doctorList'] = tmp_res
                            res['pagination']['resultCount'] = len(tmp_res)

                    resp.update({'status_code': 200,
                                 'resq_body': res
                                 })
                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
        except Exception as e:
            logger.error('hospital_get_live_dr_fulllist %s' % str(e))

        return resp
    

    #MP5.2

    #MP5.2
    def hospital_get_dr_shortlist(self, data):
        logger.info('hospitalt_get_dr_shortlist %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get doctor shortlist failed"),
                        "msg": _("An error occurred. Please try to get doctor shortlist again."),
                        "code": "hospital_shortlist_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            hospital_id = int(data.get('hospital_id', 0))
            res = {
                'favouriteList': [],
                'liveList': [],
                'summary': {
                    'favouriteListCount': 0,
                    'liveListCount': 0
                }
            }
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    # get sumary
                    sql = '''select     drsp.specialties_code,
                                        mtsp.name specialties_name,
                                        count(distinct ua.id)
                                from doctor_profile dp
                                left join user_account ua on ua.user_mngment_id = dp.user_mngment_id
                                left join dr_doctorprofile_specialties drsp on drsp.doctor_profile_id = dp.id
                                left join meta_dr_specialties mtsp on code = specialties_code
                                left join calendar_event ce on ce.user_id = ua.id
                                where ((dp.consultation_type = 'by_appointment' and ce.end_datetime > current_timestamp) or dp.consultation_type = 'live') and dp.hospital_profile_id = %s
                                group by 1,2
                    ''' % (hospital_id)
                    list_sumary = gs.sql_execute(pool, sql)
                    for line in list_sumary:
                        if line[1]:
                            spec_name = line[1].split(' ')
                            spec_name[0] = spec_name[0].lower()
                            key = ''.join(spec_name) + 'ListCount'
                            res['summary'].update({key: line[2]})

                    # get list_specialties
                    sql = '''select code from meta_dr_specialties'''
                    list_specialties = gs.sql_execute(pool, sql)
                    if list_specialties:
                        sql = '''with list_doctor as
                                (
                                
                                select distinct um.id profile_id,
                                        dp.id doctor_id, 
                                        COALESCE(um.profile_image_url,'') profile_image_url,
                                        COALESCE(um.title,'') title,
                                        COALESCE(um.first_name,'') first_name,
                                        COALESCE(um.middle_name,'') middle_name,
                                        COALESCE(um.last_name,'') last_name,
                                        consultation_type,
                                        drsp.specialties_code,
                                        mtsp.name specialties_name,
                                        (COALESCE(sum_rating_point,0)/COALESCE(count_rating_point,1))::float rating_point,
                                        case when current_timestamp <= ua.checkout_time and ua.activity_status = 'online' then True
                                            else False
                                        end onlineStatus,
                                        ua.activity_status
                                from doctor_profile dp
                                left join user_mngment um on um.id = dp.user_mngment_id
                                left join user_account ua on ua.user_mngment_id = um.id
                                left join dr_doctorprofile_specialties drsp on drsp.doctor_profile_id = dp.id
                                left join meta_dr_specialties mtsp on code = specialties_code
                                left join calendar_event ce on ce.user_id = ua.id
                                %s and dp.hospital_profile_id = %s and ua.validation_status = 'active' 
                                limit 10
                                )
                                select d.profile_id,--0
                                        d.profile_image_url,--1
                                        d.title,--2
                                        d.first_name,--3
                                        d.middle_name,--4
                                        d.last_name,--5
                                        d.consultation_type,--6
                                        d.specialties_code,--7
                                        d.specialties_name,--8
                                        d.rating_point,--9
                                        d.onlineStatus, --10
                                        d.activity_status, --11
                                        array_to_string(array_agg(distinct s.specialties_code), ', ') as list_specialties --12
                                from list_doctor d
                                left join dr_doctorprofile_specialties s on s.doctor_profile_id = d.doctor_id
                                group by 1,2,3,4,5,6,7,8,9,10,11,12'''
                        for spec in list_specialties:
                            where = '''where drsp.specialties_code = '%s' and ((dp.consultation_type = 'by_appointment' and ce.end_datetime > current_timestamp) or dp.consultation_type = 'live') ''' % spec                     
                            list_doctor = gs.sql_execute(pool, sql % (where, hospital_id))
                            if list_doctor:
                                spec_name = list_doctor[0][8].split(' ')
                                spec_name[0] = spec_name[0].lower()
                                key = ''.join(spec_name) + 'List'
                                tmp_res = []
                                for dr in list_doctor:
                                    list_sp = []
                                    for sp in dr[12].split(', '):
                                        list_sp.append({'code': sp})
                                    tmp_res.append({
                                        "id": dr[0],
                                        "avatar": dr[1],
                                        "titleCode": dr[2],
                                        "firstName": None if dr[3] in ('False', 'None', '') else dr[3],
                                        "middleName": None if dr[4] in ('False', 'None', '') else dr[4],
                                        "lastName": None if dr[5] in ('False', 'None', '') else dr[5],
                                        "specialtyCodes": list_sp,
                                        "isAvailableToCall": True if dr[6] == 'live' and dr[10] == True and dr[11] == 'online' else False,
                                        "ratingPoint": dr[9]
                                    })
                                res.update({key: tmp_res})

                    # update live doctor
                    sql = '''select consultation_type,count(distinct ua.id)
                            from doctor_profile dp
                                inner join user_mngment um on um.id = dp.user_mngment_id
                                inner join user_account ua on ua.user_mngment_id = um.id
                                inner join calendar_event ce on ce.user_id = ua.id
                            where um.type_profile = 'doctor' and consultation_type = 'live' and current_timestamp <= ua.checkout_time and ua.activity_status = 'online' and dp.hospital_profile_id = %s
                            group by 1'''
                    live_count = gs.sql_execute(pool, sql % hospital_id)
                    if live_count and live_count[0][1]:
                        res['summary'].update({'liveListCount': live_count[0][1]})
                    else:
                        res['summary'].update({'liveListCount': 0})
                    sql_live_dr = '''with list_doctor as
                                    (
                                    
                                    select distinct um.id profile_id,
                                            dp.id doctor_id, 
                                            COALESCE(um.profile_image_url,'') profile_image_url,
                                            COALESCE(um.title,'') title,
                                            COALESCE(um.first_name,'') first_name,
                                            COALESCE(um.middle_name,'') middle_name,
                                            COALESCE(um.last_name,'') last_name,
                                            consultation_type,
                                            (COALESCE(sum_rating_point,0)/COALESCE(count_rating_point,1))::float rating_point
                                    from doctor_profile dp
                                        left join user_mngment um on um.id = dp.user_mngment_id
                                        left join user_account ua on ua.user_mngment_id = um.id
                                        left join calendar_event ce on ce.user_id = ua.id
                                    where um.type_profile = 'doctor' and ua.validation_status = 'active' and consultation_type = 'live' and current_timestamp <= ua.checkout_time and ua.activity_status = 'online' and dp.hospital_profile_id = %s
                                    order by first_name asc
                                    limit 10
                                    )
                                    select d.profile_id,--0
                                            d.profile_image_url,--1
                                            d.title,--2
                                            d.first_name,--3
                                            d.middle_name,--4
                                            d.last_name,--5
                                            d.consultation_type,--6
                                            d.rating_point,--7
                                            array_to_string(array_agg(distinct s.specialties_code), ', ') as list_specialties --8
                                    from list_doctor d
                                        left join dr_doctorprofile_specialties s on s.doctor_profile_id = d.doctor_id
                                    group by 1,2,3,4,5,6,7,8
                                    order by first_name asc '''
                    list_live_doctor = gs.sql_execute(pool, sql_live_dr % hospital_id)
                    if list_live_doctor:
                        key = 'liveList'
                        tmp_res = []
                        for dr in list_live_doctor:
                            list_sp = []
                            for sp in dr[8].split(', '):
                                list_sp.append({'code': sp})
                            tmp_res.append({
                                "id": dr[0],
                                "avatar": dr[1],
                                "titleCode": dr[2],
                                "firstName": None if dr[3] in ('False', 'None', '') else dr[3],
                                "middleName": None if dr[4] in ('False', 'None', '') else dr[4],
                                "lastName": None if dr[5] in ('False', 'None', '') else dr[5],
                                "specialtyCodes": list_sp,
                                "isAvailableToCall": True,
                                "ratingPoint": dr[7]
                            })
                        res.update({key: tmp_res})
                    # done update live doctor
                    # get favourite list
                    patient_mngment_id = res_token[7]
                    sql = '''SELECT doctor_id as id FROM patient_doctor_favourite WHERE patient_id  = %s '''
                    favourite_count = gs.sql_execute(pool, sql, para_values=[patient_mngment_id])
                    ids = [0]
                    if favourite_count:
                        for line in favourite_count:
                            ids.append(line[0])
                    favourite_count = len(ids)
                    ids = random.sample(ids, 10 if favourite_count >= 10 else favourite_count)
                    idss = ','.join(str(x) for x in ids)

                    sql_list_info_dr = '''with list_doctor as
                                (
                                
                                select distinct um.id profile_id,
                                        dp.id doctor_id, 
                                        COALESCE(um.profile_image_url,'') profile_image_url,
                                        COALESCE(um.title,'') title,
                                        COALESCE(um.first_name,'') first_name,
                                        COALESCE(um.middle_name,'') middle_name,
                                        COALESCE(um.last_name,'') last_name,
                                        consultation_type,
                                        case when current_timestamp <= ua.checkout_time and ua.activity_status = 'online' then True
                                                else False
                                        end onlineStatus,
                                        ua.activity_status
                                from doctor_profile dp
                                    left join user_mngment um on um.id = dp.user_mngment_id
                                    left join user_account ua on ua.user_mngment_id = um.id
                                where um.id in (%s) and dp.hospital_profile_id = %s and ua.validation_status = 'active' 
                                )
                                select d.profile_id,--0
                                        d.profile_image_url,--1
                                        d.title,--2
                                        d.first_name,--3
                                        d.middle_name,--4
                                        d.last_name,--5
                                        d.consultation_type,--6
                                        d.onlineStatus, --7
                                        d.activity_status, --8
                                        array_to_string(array_agg(distinct s.specialties_code), ', ') as list_specialties --9
                                from list_doctor d
                                    left join dr_doctorprofile_specialties s on s.doctor_profile_id = d.doctor_id
                                group by 1,2,3,4,5,6,7,8,9 ''' % (idss, hospital_id)
                    list_info_dr = gs.sql_execute(pool, sql_list_info_dr)
                    if list_info_dr:
                        tmp_res = []
                        i = 0
                        for dr in list_info_dr:
                            list_sp = []
                            i = i + 1
                            for sp in dr[9].split(', '):
                                list_sp.append({'code': sp})
                            tmp_res.append({
                                "id": dr[0],
                                "avatar": dr[1],
                                "titleCode": dr[2],
                                "firstName": None if dr[3] in ('False', 'None', '') else dr[3],
                                "middleName": None if dr[4] in ('False', 'None', '') else dr[4],
                                "lastName": None if dr[5] in ('False', 'None', '') else dr[5],
                                "specialtyCodes": list_sp,
                                "isAvailableToCall": True if dr[6] == 'live' and dr[7] == True and dr[8] == 'online' else False,
                            })
                        key = 'favouriteList'
                        res.update({key: tmp_res})
                        res['summary'].update({'favouriteListCount': favourite_count})

                    resp.update({'status_code': 200,
                                 'resq_body': res
                                 })
                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
        except Exception as e:
            logger.error('error_profile_patient_get_dr_shortlist %s' % str(e))
            client.captureException()

        return resp

        # MP5.7 API for getting doctor list by with sort and filter criterias (support pagination)
    def hospital_get_dr_list_by_sort_filter(self, data):
        logger.info('hospital_get_dr_list_by_sort_filter %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get doctor list by sort and filter criterias failed"),
                        "msg": _("An error occurred. Please try to get doctor list again."),
                        "code": "get_dr_list_by_sort_and_filter_criterias"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            pagination = data.get('pagination')
            sortBy = data.get('sortBy', 'firstname_asc')
            keyword = data.get('keyword')
            filterBy = data.get('filterBy')
            hospital_id = int(data.get('hospital_id', 0))
            res = {
                'pagination': {
                    "resultCount": 0,
                    "totalCount": 0,
                    "nextItemId": 0
                },
                'doctorList': [],
            }
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                start = 0
                end = 0
                if res_token:
                    user_id = res_token[1]
                    nextItemId = int(pagination.get('nextItemId', 0))
                    requestCount = int(pagination.get('requestCount'))
                    dicOrderBy = {'lastname_asc': '\norder by last_name asc',
                                  'lastname_desc': '\norder by last_name desc',
                                  'price_asc': '\norder by consultation_fee asc',
                                  'price_desc': '\norder by consultation_fee desc',
                                  'rate_asc': '\norder by rating_point asc',
                                  'rate_desc': '\norder by rating_point desc',
                                  'firstname_asc': '\norder by first_name asc'}
                    orderBy = dicOrderBy[sortBy]

                    if not pagination.get('nextItemId') or pagination.get('nextItemId') == 0:
                        full_list_ids = self.generate_pagination_list(user_id, 'doctor_profile',
                                                                      context={'filterBy': filterBy, 'sortBy': orderBy,
                                                                               'keyword': keyword, 'api': 'MP5_7', 'hospital_id' : hospital_id})
                        start = 0
                    else:
                        sql = '''select list_id 
                                from user_account_pagination
                                where user_account_id = %s
                                    and table_pagination = '%s'
                                ''' % (user_id, 'doctor_profile')
                        full_list_ids = gs.sql_execute(pool, sql)
                        if full_list_ids:
                            full_list_ids = eval(full_list_ids[0][0])
                            start = full_list_ids.index(nextItemId)
                        else:
                            full_list_ids = self.generate_pagination_list(user_id, 'doctor_profile',
                                                                          context={'filterBy': filterBy,
                                                                                   'sortBy': sortBy, 'keyword': keyword,
                                                                                   'api': 'MP5_7', 'hospital_id' : hospital_id})
                            start = 0

                    if not full_list_ids:
                        resp.update({'status_code': 200,
                                     'resq_body': res
                                     })
                        return resp

                    if (start + requestCount) <= len(full_list_ids):
                        end = start + requestCount
                    else:
                        end = len(full_list_ids)

                    select_list_ids = full_list_ids[start:end]
                    res['pagination']['totalCount'] = len(full_list_ids)
                    res['pagination']['nextItemId'] = full_list_ids[end] if end < len(full_list_ids) else full_list_ids[
                        end - 1]
                    res['pagination']['resultCount'] = len(select_list_ids)

                    # get list_specialties
                    if len(select_list_ids) > 0:
                        select_list_ids = select_list_ids + [-1, -1]
                        sql = '''with list_doctor as
                                (
                                select um.id profile_id,
                                        dp.id doctor_id, 
                                        COALESCE(um.profile_image_url,'') profile_image_url,
                                        COALESCE(um.title,'') title,
                                        COALESCE(um.first_name,'') first_name,
                                        COALESCE(um.middle_name,'') middle_name,
                                        COALESCE(um.last_name,'') last_name,
                                        dp.consultation_type,
                                        drsp.specialties_code,
                                        dp.consultation_fee,
                                        mtsp.name specialties_name,
                                        (COALESCE(dp.sum_rating_point,0) / COALESCE(dp.count_rating_point,1)) as rating_point, --7
                                        case when current_timestamp <= ua.checkout_time and ua.activity_status = 'online' then True
                                            else False
                                        end onlineStatus, --8
                                        ua.activity_status
                                from doctor_profile dp
                                left join user_mngment um on um.id = dp.user_mngment_id
                                left join dr_doctorprofile_specialties drsp on drsp.doctor_profile_id = dp.id
                                left join meta_dr_specialties mtsp on mtsp.code = drsp.specialties_code
                                left join user_account ua on ua.user_mngment_id = um.id
                                where um.id in %s and dp.hospital_profile_id = %s and ua.validation_status = 'active' 
                                )
                                select d.profile_id,--0
                                        d.profile_image_url,--1
                                        d.title,--2
                                        d.first_name,--3
                                        d.middle_name,--4
                                        d.last_name,--5
                                        d.consultation_type,--6
                                        d.rating_point, --7
                                        d.consultation_fee, -- 8
                                        d.onlineStatus, --9
                                        d.activity_status, --10
                                        array_to_string(array_agg(distinct s.specialties_code), ', ') as list_specialties --11
                                from list_doctor d
                                left join dr_doctorprofile_specialties s on s.doctor_profile_id = d.doctor_id
                                group by 1,2,3,4,5,6,7,8,9,10,11 %s''' % (str(tuple(select_list_ids)), hospital_id, orderBy)

                        list_doctor = gs.sql_execute(pool, sql)
                        if list_doctor:
                            tmp_res = []
                            for dr in list_doctor:
                                list_sp = []
                                for sp in dr[11].split(', '):
                                    list_sp.append({'code': sp})
                                tmp_res.append({
                                    "id": dr[0],
                                    "avatar": dr[1],
                                    "titleCode": dr[2],
                                    "firstName": None if dr[3] in ('False', 'None', '') else dr[3],
                                    "middleName": None if dr[4] in ('False', 'None', '') else dr[4],
                                    "lastName": None if dr[5] in ('False', 'None', '') else dr[5],
                                    "specialtyCodes": list_sp,
                                    "isAvailableToCall": True if dr[6] == 'live' and dr[9] == True and dr[10] == 'online' else False,
                                    "ratingPoint": dr[7]
                                })
                            res['doctorList'] = tmp_res

                    resp.update({'status_code': 200,
                                 'resq_body': res
                                 })
                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
        except Exception as e:
            logger.error('hospital_get_dr_list_by_sort_filter %s' % str(e))

        return resp

    # MP5.8 API for getting live doctor list (support pagination)
    def hospital_get_live_dr_list(self, data):
        logger.info('profile_patient_get_live_dr_list %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get live doctor list failed"),
                        "msg": _("An error occurred. Please try to get doctor list again."),
                        "code": "hospital_get_live_dr_list_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            nextItemId = int(data.get('nextItemId', 0))
            requestCount = int(data.get('requestCount'))
            hospital_id = int(data.get('hospital_id', 0))
            if not requestCount:
                return resp

            res = {
                'pagination': {
                    "resultCount": 0,
                    "totalCount": 0,
                    "nextItemId": 0
                },
                'doctorList': [],
            }
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                start = 0
                end = 0
                if res_token:
                    user_id = res_token[1]
                    if not nextItemId or nextItemId == 0:
                        full_list_ids = self.generate_pagination_list(user_id, 'doctor_profile_live',
                                                                      context={'api': 'MP5_8', 'hospital_id' : hospital_id})
                        start = 0
                    else:
                        sql = '''select list_id 
                                from user_account_pagination
                                where user_account_id = %s
                                    and table_pagination = '%s'
                                ''' % (user_id, 'doctor_profile_live')
                        full_list_ids = gs.sql_execute(pool, sql)
                        if full_list_ids:
                            full_list_ids = eval(full_list_ids[0][0])
                            start = full_list_ids.index(nextItemId)
                        else:
                            full_list_ids = self.generate_pagination_list(user_id, 'doctor_profile_live',
                                                                          context={'api': 'MP5_8', 'hospital_id' : hospital_id})
                            start = 0

                    if not full_list_ids:
                        resp.update({'status_code': 200,
                                     'resq_body': res
                                     })
                        return resp

                    if (start + requestCount) <= len(full_list_ids):
                        end = start + requestCount
                    else:
                        end = len(full_list_ids)

                    select_list_ids = full_list_ids[start:end]
                    res['pagination']['totalCount'] = len(full_list_ids)
                    res['pagination']['nextItemId'] = full_list_ids[end] if end < len(full_list_ids) else full_list_ids[
                        end - 1]
                    res['pagination']['resultCount'] = len(select_list_ids)

                    #print select_list_ids
                    # get list_specialties
                    if len(select_list_ids) > 0:
                        select_list_ids = select_list_ids + [-1, -1]
                        sql = '''with list_doctor as
                                (
                                select um.id profile_id,
                                        dp.id doctor_id, 
                                        COALESCE(um.profile_image_url,'') profile_image_url,
                                        COALESCE(um.title,'') title,
                                        COALESCE(um.first_name,'') first_name,
                                        COALESCE(um.middle_name,'') middle_name,
                                        COALESCE(um.last_name,'') last_name,
                                        dp.consultation_type,
                                        drsp.specialties_code,
                                        dp.consultation_fee,
                                        mtsp.name specialties_name,
                                        (COALESCE(dp.sum_rating_point,0) / COALESCE(dp.count_rating_point,1)) as rating_point, --7
                                        case when current_timestamp <= ua.checkout_time and ua.activity_status = 'online' then True
                                                else False
                                        end onlineStatus --8
                                from doctor_profile dp
                                left join user_mngment um on um.id = dp.user_mngment_id
                                left join dr_doctorprofile_specialties drsp on drsp.doctor_profile_id = dp.id
                                left join meta_dr_specialties mtsp on mtsp.code = drsp.specialties_code
                                left join user_account ua on ua.user_mngment_id = um.id
                                where um.id in %s and dp.hospital_profile_id = %s and ua.validation_status = 'active' 
                                )
                                select d.profile_id,--0
                                        d.profile_image_url,--1
                                        d.title,--2
                                        d.first_name,--3
                                        d.middle_name,--4
                                        d.last_name,--5
                                        d.onlineStatus,--6
                                        d.rating_point, --7
                                        d.consultation_fee, -- 8
                                        array_to_string(array_agg(distinct s.specialties_code), ', ') as list_specialties --9
                                from list_doctor d
                                left join dr_doctorprofile_specialties s on s.doctor_profile_id = d.doctor_id
                                group by 1,2,3,4,5,6,7,8,9
                                order by first_name asc''' % (str(tuple(select_list_ids)), hospital_id)

                        list_doctor = gs.sql_execute(pool, sql)
                        if list_doctor:
                            tmp_res = []
                            for dr in list_doctor:
                                list_sp = []
                                for sp in dr[9].split(', '):
                                    list_sp.append({'code': sp})
                                tmp_res.append({
                                    "id": dr[0],
                                    "avatar": dr[1],
                                    "titleCode": dr[2],
                                    "firstName": None if dr[3] in ('False', 'None', '') else dr[3],
                                    "middleName": None if dr[4] in ('False', 'None', '') else dr[4],
                                    "lastName": None if dr[5] in ('False', 'None', '') else dr[5],
                                    "specialtyCodes": list_sp,
                                    "isAvailableToCall": dr[6],
                                    "ratingPoint": dr[7]
                                })
                            res['doctorList'] = tmp_res

                    resp.update({'status_code': 200,
                                 'resq_body': res
                                 })
                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
        except Exception as e:
            logger.error('hospital_get_live_dr_list %s' % str(e))

        return resp

    # MP5.9 API for getting live doctor list with sort and filter criterias (support pagination)
    def hospital_get_live_dr_list_by_sort_filter(self, data):
        logger.info('hospital_get_live_dr_list_by_sort_filter %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get live doctor list by sort and filter criterias failed"),
                        "msg": _("An error occurred. Please try to get doctor list again."),
                        "code": "hospital_get_live_dr_list_by_sort_and_filter_criterias"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            pagination = data.get('pagination')
            sortBy = data.get('sortBy', 'firstname_asc')
            keyword = data.get('keyword')
            filterBy = data.get('filterBy')
            hospital_id = int(data.get('hospital_id', 0))
            res = {
                'pagination': {
                    "resultCount": 0,
                    "totalCount": 0,
                    "nextItemId": 0
                },
                'doctorList': [],
            }
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                start = 0
                end = 0
                if res_token:
                    user_id = res_token[1]
                    nextItemId = int(pagination.get('nextItemId', 0))
                    requestCount = int(pagination.get('requestCount'))
                    dicOrderBy = {'lastname_asc': '\norder by last_name asc',
                                  'lastname_desc': '\norder by last_name desc',
                                  'price_asc': '\norder by consultation_fee asc',
                                  'price_desc': '\norder by consultation_fee desc',
                                  'rate_asc': '\norder by rating_point asc',
                                  'rate_desc': '\norder by rating_point desc',
                                  'firstname_asc': '\norder by first_name asc'}
                    orderBy = dicOrderBy[sortBy]

                    if not pagination.get('nextItemId') or pagination.get('nextItemId') == 0:
                        full_list_ids = self.generate_pagination_list(user_id, 'doctor_profile_live',
                                                                      context={'filterBy': filterBy, 'sortBy': orderBy,
                                                                               'keyword': keyword, 'api': 'MP5_9', 'hospital_id' : hospital_id})
                        start = 0
                    else:
                        sql = '''select list_id 
                                from user_account_pagination
                                where user_account_id = %s
                                    and table_pagination = '%s'
                                ''' % (user_id, 'doctor_profile_live')
                        full_list_ids = gs.sql_execute(pool, sql)
                        if full_list_ids:
                            full_list_ids = eval(full_list_ids[0][0])
                            start = full_list_ids.index(nextItemId)
                        else:
                            full_list_ids = self.generate_pagination_list(user_id, 'doctor_profile_live',
                                                                          context={'filterBy': filterBy,
                                                                                   'sortBy': sortBy, 'keyword': keyword,
                                                                                   'api': 'MP5_9', 'hospital_id' : hospital_id})
                            start = 0

                    if not full_list_ids:
                        resp.update({'status_code': 200,
                                     'resq_body': res
                                     })
                        return resp

                    if (start + requestCount) <= len(full_list_ids):
                        end = start + requestCount
                    else:
                        end = len(full_list_ids)

                    select_list_ids = full_list_ids[start:end]
                    res['pagination']['totalCount'] = len(full_list_ids)
                    res['pagination']['nextItemId'] = full_list_ids[end] if end < len(full_list_ids) else full_list_ids[
                        end - 1]
                    res['pagination']['resultCount'] = len(select_list_ids)

                    # get list_specialties
                    if len(select_list_ids) > 0:
                        select_list_ids = select_list_ids + [-1, -1]
                        sql = '''with list_doctor as
                                (
                                select um.id profile_id,
                                        dp.id doctor_id, 
                                        COALESCE(um.profile_image_url,'') profile_image_url,
                                        COALESCE(um.title,'') title,
                                        COALESCE(um.first_name,'') first_name,
                                        COALESCE(um.middle_name,'') middle_name,
                                        COALESCE(um.last_name,'') last_name,
                                        dp.consultation_type,
                                        drsp.specialties_code,
                                        dp.consultation_fee,
                                        mtsp.name specialties_name,
                                        (COALESCE(dp.sum_rating_point,0) / COALESCE(dp.count_rating_point,1)) as rating_point --7
                                from doctor_profile dp
                                left join user_mngment um on um.id = dp.user_mngment_id
                                left join dr_doctorprofile_specialties drsp on drsp.doctor_profile_id = dp.id
                                left join meta_dr_specialties mtsp on mtsp.code = drsp.specialties_code
                                left join user_account ua on ua.user_mngment_id = um.id
                                where um.id in %s and dp.hospital_profile_id = %s and ua.validation_status = 'active' 
                                )
                                select d.profile_id,--0
                                        d.profile_image_url,--1
                                        d.title,--2
                                        d.first_name,--3
                                        d.middle_name,--4
                                        d.last_name,--5
                                        d.consultation_type,--6
                                        d.rating_point, --7
                                        d.consultation_fee, -- 8
                                        array_to_string(array_agg(distinct s.specialties_code), ', ') as list_specialties --9
                                from list_doctor d
                                left join dr_doctorprofile_specialties s on s.doctor_profile_id = d.doctor_id
                                group by 1,2,3,4,5,6,7,8,9 %s''' % (str(tuple(select_list_ids)), hospital_id, orderBy )
                        
                        list_doctor = gs.sql_execute(pool, sql)
                        if list_doctor:
                            tmp_res = []
                            for dr in list_doctor:
                                list_sp = []
                                for sp in dr[9].split(', '):
                                    list_sp.append({'code': sp})
                                tmp_res.append({
                                    "id": dr[0],
                                    "avatar": dr[1],
                                    "titleCode": dr[2],
                                    "firstName": None if dr[3] in ('False', 'None', '') else dr[3],
                                    "middleName": None if dr[4] in ('False', 'None', '') else dr[4],
                                    "lastName": None if dr[5] in ('False', 'None', '') else dr[5],
                                    "specialtyCodes": list_sp,
                                    "isAvailableToCall": True,
                                    "ratingPoint": dr[7]
                                })
                            res['doctorList'] = tmp_res

                    resp.update({'status_code': 200,
                                 'resq_body': res
                                 })
                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
        except Exception as e:
            logger.error('profile_patient_get_live_dr_list_by_sort_filter %s' % str(e))

        return resp
    def admin_doctor_update_profile(self, data):
        logger.info('profile_doctor_setup %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Setup profile failed"),
                        "msg": _("An error occurred. Please try to setup profile again."),
                        "code": "setup_profile_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            doctor_mngment_id = data.get('doctor_mngment_id')

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    #sql_hospital = ''' select hospital_profile_id from admin_profile where user_account_id = %s ''' % (user_id)
                    #hospital_id = gs.sql_execute(pool,sql_hospital)[0][0]
                  
                    # update table user_mngment
                    sql_update = '''UPDATE user_account SET phone = %s,password = %s WHERE user_mngment_id =%s'''
                    gs.sql_execute(pool, sql_update, commit=True, para_values=(data.get('phone', ''),data.get('password', ''),doctor_mngment_id))
                    sql_update_mng = '''UPDATE user_mngment SET first_name = %s, middle_name = %s, last_name = %s  where id =%s '''
                    data_graduate = long(data.get('graduationDate', False)) 
                    graduationDate =  datetime.fromtimestamp(data_graduate).strftime('%Y-%m-%d')
                    gs.sql_execute(pool, sql_update_mng, commit=True, para_values=(data.get('firstName', ''),data.get('middleName', ''),data.get('lastName', ''),doctor_mngment_id))
                    sql_update_about = '''UPDATE doctor_profile SET about = %s,years_of_experience = %s, graduation_date = %s , medical_school = %s WHERE user_mngment_id =%s '''
                    gs.sql_execute(pool, sql_update_about, commit=True, para_values=(data.get('about', ''),data.get('yearsOfExperience', ''),graduationDate,data.get('medicalSchool', ''),doctor_mngment_id))

                    sql_select_doctor = ''' select id from doctor_profile where user_mngment_id = %s ''' % doctor_mngment_id
                    res_select_doctor = gs.sql_execute(pool, sql_select_doctor)
                    if res_select_doctor :
                        sql_select_profile = ''' select id from dr_doctorprofile_specialties where doctor_profile_id = %s''' % res_select_doctor[0][0]
                        res_select_profile = gs.sql_execute(pool, sql_select_profile)
                        if not res_select_profile :
                            #logger.info(res_select_doctor)
                            sql_insert_profile_sp = ''' insert into dr_doctorprofile_specialties (doctor_profile_id,specialties_code,years_of_exp) values (%s,'paediatrics','1') ;'''
                            gs.sql_execute(pool, sql_insert_profile_sp, commit=True, para_values=[res_select_doctor[0][0]])
                   
                    resp = {'status_code': 200,
                            'resq_body':
                                {
                                    "code": "success_update",
                                    "title": _("Success Update"),
                                    "msg": _("Success")
                                }
                            }
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_profile_doctor_update %s' % str(e))
            client.captureException()
        return resp

    def admin_doctor_update_profile_experiences(self, data):
        logger.info('profile_doctor_setup exp %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Setup profile failed exp"),
                        "msg": _("An error occurred. Please try to setup profile again."),
                        "code": "setup_profile_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            doctor_mngment_id = data.get('doctor_mngment_id')

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    #sql_hospital = ''' select hospital_profile_id from admin_profile where user_account_id = %s ''' % (user_id)
                    #hospital_id = gs.sql_execute(pool,sql_hospital)[0][0]
                  
                    # update table user_mngment
                    
                    sql_update_about = '''UPDATE doctor_profile SET 
                                        post_graduate_training = %s, --20
                                        industry_relationship = %s,--21
                                        publications = %s,--22
                                        teaching = %s,--23
                                        community_service  = %s WHERE user_mngment_id =%s '''
                    gs.sql_execute(pool, sql_update_about, commit=True, para_values=(data.get('post_graduate_training', ''),data.get('industry_relationship', ''),data.get('publications', ''),data.get('teaching', ''),data.get('community_service', ''),doctor_mngment_id))

                    resp = {'status_code': 200,
                            'resq_body':
                                {
                                    "code": "success_update_exp",
                                    "title": _("Success Update"),
                                    "msg": _("Success")
                                }
                            }
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_profile_doctor_update %s' % str(e))
            client.captureException()
        return resp
    
    def admin_doctor_deactive(self, data):
        logger.info('doctor_deactive %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Deactive failed"),
                        "msg": _("An error occurred. Please try to deactive again."),
                        "code": "deactive_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            doctor_mngment_id = data.get('doctor_mngment_id')

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    # update table user_mngment
                    sql_update = '''update user_account set validation_status = %s where user_mngment_id = %s '''
                    gs.sql_execute(pool, sql_update, commit=True,para_values=('inactive',doctor_mngment_id))
                    resp = {'status_code': 200,
                            'resq_body':
                                {
                                    "code": "success_deactive",
                                    "title": _("Success deactive"),
                                    "msg": _("Success")
                                }
                            }
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_doctor_deactive %s' % str(e))
            client.captureException()
        return resp
    def admin_doctor_active(self, data):
        logger.info('doctor_active %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("active failed"),
                        "msg": _("An error occurred. Please try to active again."),
                        "code": "deactive_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            doctor_mngment_id = data.get('doctor_mngment_id')

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    # update table user_mngment
                    sql_update = '''update user_account set validation_status = %s where user_mngment_id = %s '''
                    gs.sql_execute(pool, sql_update, commit=True,para_values=('active',doctor_mngment_id))
                    resp = {'status_code': 200,
                            'resq_body':
                                {
                                    "code": "success_active",
                                    "title": _("Success active"),
                                    "msg": _("Success")
                                }
                            }
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_doctor_active %s' % str(e))
            client.captureException()
        return resp


        
    def admin_get_call_logs_today(self, data):
        logger.info('admin_get_call_logs_today %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("admin_get_call_logs_today failed"),
                        "msg": _("An error occurred. Please try to admin_get_call_logs_todayagain."),
                        "code": "admin_get_call_logs_today"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                start = 0
                end = 0
                if res_token:
                    user_id = res_token[1]
                    sql_hospital = ''' select hospital_profile_id from admin_profile where user_account_id = %s ''' % (user_id)
                    hospital_id  = gs.sql_execute(pool, sql_hospital)[0][0]
                    if hospital_id :
                        sql =  ''' select count(crl.id),date_part('day',last_update) as timeday,max(last_update)  from call_record_logs crl inner join appointment apm on apm.id = crl.apm_id inner join user_account ua on ua.id = apm.doctor_id inner join doctor_profile dp on dp.user_mngment_id = ua.user_mngment_id where dp.hospital_profile_id = %s and date_part('month',last_update) = date_part('month',current_timestamp) group by timeday order by timeday asc ''' % (hospital_id)
                        call_logs = gs.sql_execute(pool, sql)
                        tmp_res = []
                        if call_logs:
                            for line in call_logs:
                                tmp_res.append({
                                        'total': line[0],
                                        'day': line[1],
                                        'date': line[2]
                                        })

                            resp.update({'status_code': 200,
                                        'resq_body':tmp_res
                                        })

        
        except Exception as e:
            logger.error('admin_get_call_logs_today %s' % str(e))
        return resp


    def admin_doctor_call_logs(self, data):
        logger.info('call_logs %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("call logs failed"),
                        "msg": _("An error occurred. Please call logs again."),
                        "code": "call logs_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            doctor_mngment_id = data.get('doctor_mngment_id')

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    sql_doctor = ''' select id from user_account where user_mngment_id = %s ''' % (doctor_mngment_id)
                    user_doctor_id = gs.sql_execute(pool,sql_doctor)[0][0]
                    sql = ''' select apm.id, --0
                                        apm.schedule_date, --1
                                        apm.state, --2
                                        umng.id, --3
                                        umng.first_name, --4
                                        umng.middle_name, --5
                                        umng.last_name, --6
                                        umng.profile_image_url,--7
                                        apm.consultation_note, --8
                                        mta.value, --9
                                        mta.unit, --10
                                        umng.gender, --11
                                        coalesce(uad.number,''),--12
                                        coalesce(uad.street,''),--13
                                        coalesce(uad.city,''),--14
                                        coalesce(uad.state,''),--15
                                        coalesce(uad.postcode,''),--16
                                        coalesce(uad.country,''), --17
                                        ua.id, --18
                                        ach.state as call_history_state, --19
                                        ach.create_date as call_history_time, --20
                                        ach.start_time as call_start_time,  --21
                                        ach.end_time as call_end_time  --22
                                    from appointment apm
                                    left join user_account ua on apm.patient_id = ua.id
                                    left join user_mngment umng on  umng.id = ua.user_mngment_id
                                    left join meta_dr_consultation_durations mta on mta.code = apm.consultation_duration
                                    left join user_contact uc on uc.user_mngment_id = umng.id
                                    left join user_address uad on uad.user_contact_id = uc.id and is_primary = True
                                    inner join appointment_call_history ach on ach.appointment_id = apm.id
                                    where apm.doctor_id = %s and apm.type = 'live' 
                                    order by apm.schedule_date ''' % (user_doctor_id)
                    records = gs.sql_execute(pool,sql)
                    tmp_res = []
                    if records:
                        for line in records:
                            tmp_res.append({
                                    'patient': {
                                        'id': line[3],
                                        'firstName': None if line[4] in ('False', 'None', '') else line[4],
                                        'middleName': None if line[5] in ('False', 'None', '') else line[5],
                                        'lastName': None if line[6] in ('False', 'None', '') else line[6],
                                        'avatar': line[7],
                                        'userId': line[18]
                                    },
                                    'id': line[0],
                                    'stateCode': line[2],
                                    'scheduleTime': int(time.mktime(line[1].timetuple())) or None,
                                    'duration': {
                                        'value': line[9],
                                        'unitCode': line[10]
                                    },
                                    'note': line[8],
                                    'genderCode':line[11],
                                    'call_history_state':line[19],
                                    'call_history_time':int(time.mktime(line[20].timetuple())) or None,
                                    'call_start_time': 'null' if line[21] is None else int(time.mktime(line[20].timetuple())),
                                    'call_end_time': 'null' if line[22] is None else int(time.mktime(line[20].timetuple())),
                                    'primaryAddress': {
                                            "number": None if line[12] in ('False', 'None', '') else line[12],
                                            "street": None if line[13] in ('False', 'None', '') else line[13],
                                            "cityCode": None if line[14] in ('False', 'None', '') else line[14],
                                            "state": None if line[15] in ('False', 'None', '') else line[15],
                                            "postcode": None if line[16] in ('False', 'None', '') else line[16],
                                            "countryCode": None if line[17] in ('False', 'None', '') else line[17]
                                        }})
                    resp.update({'status_code': 200,
                                 'resq_body':tmp_res
                                 })
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_doctor_active %s' % str(e))
            client.captureException()
        return resp
    
    def admin_doctor_avatar_upload(self, data):
        logger.info('profile_doctor_avatar_upload %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Upload avatar failed"),
                        "msg": _("An error occurred. Please try to setup again."),
                        "code": "upload_avatar_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            doctor_mngment_id = data.get('doctor_mngment_id')

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    avatar_url = data.get('avatar_url')
                    if avatar_url == 'wrong_file_type':
                        return {'status_code': 401,
                                'resq_body':
                                    {
                                        "title": _("Wrong type of file"),
                                        "msg": _("An error occurred. Please try to setup profile again."),
                                        "code": "wrong_file_type"
                                    }
                                }
                    sql_update = '''update user_mngment set profile_image_url = %s where id = %s '''
                    gs.sql_execute(pool, sql_update, commit=True, para_values=(avatar_url, doctor_mngment_id))

                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     'avatarUrl': avatar_url
                                 }
                                 })

                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }

        except Exception as e:
            logger.error('error_profile_doctor_avatar_upload %s' % str(e))
            client.captureException()
        return resp

    # WD3.9 API for doctor to remove his avatar
    def admin_doctor_avatar_remove(self, data):
        logger.info('profile_doctor_avatar_remove %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Remove avatar failed"),
                        "msg": _("An error occurred. Please try to setup again."),
                        "code": "remove_avatar_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            doctor_mngment_id = data.get('doctor_mngment_id')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]

                    sql_update = '''update user_mngment set profile_image_url = NULL where id = %s ''' % (doctor_mngment_id)
                    gs.sql_execute(pool, sql_update, commit=True)

                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     "code": "success",
                                     "title": _("Success"),
                                     "msg": _("Removed doctor avatar successfully.")
                                 }
                                 })

                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }

        except Exception as e:
            logger.error('error_profile_doctor_avatar_upload %s' % str(e))
            client.captureException()

        return resp
    def admin_add_doctor(self, data):
        logger.info('admin_add_doctor %s' % data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "admin_add_doctor",
                        "title": _("admin_add_doctor"),
                        "msg": _("An error occurred. Please try to admin_add_doctor again.")
                    }
                }
        pwd = data.get('password','')
        if self.general_business.isPwdValid(pwd) == False:
            resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "bad_password",
	                    "title": _("Register failed"),
	                    "msg": _("The password requires 6 charaters with at least 1 capital and 1 number")
                    }
                }
            return resp
        try:
            gs = self.general_business
            pool = self.pool_conn
            doctor_profile = self.doctor_profile
            user_token = data.get('user_token')
            hospital_id = 0 
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    sql_hospital = ''' select hospital_profile_id from admin_profile where user_account_id = %s ''' % (user_id)
                    hospital_id  = gs.sql_execute(pool, sql_hospital)[0][0]
            
                  
            # Check trung email/phone
            sql = """select id,email,phone from user_account where email=%s or phone =%s """
            check_exist = gs.sql_execute(pool, sql, para_values=(data['email'], data['phoneNumber']))
            if check_exist:
                if check_exist[0][1] == data['email'].strip():
                    resp.update({
                        'status_code': 401,
                        'resq_body': {
                            "code": "exist_email",
                            "title": _("Register failed"),
                            "msg": _("The email already exists in our system, please try to register with another one")
                        }
                    })
                elif check_exist[0][2] == data['phoneNumber'].strip():
                    resp.update({
                        'status_code': 401,
                        'resq_body': {
                            "code": "exist_phone",
                            "title": _("Register failed"),
                            "msg": _("The phone already exists in our system, please try to register with another one")
                        }
                    })
            else:
                # check role
                sql = '''select id from user_role where code = 'doctor' '''
                role_id = gs.sql_execute(pool, sql)
                if role_id:
                    create_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    # insert profile to db
                    insert_profile = '''insert into user_mngment (type_profile, relationship,first_name,last_name)
                                        values ('doctor','me',%s,%s)
                                        RETURNING id'''
                    new_profile_id = \
                        gs.sql_execute(pool, insert_profile, commit=True,para_values=(data['firstName'],
                                                                                 data['lastName']))[0][0]

                    # insert user account to db
                    sql = """
                              insert into user_account (email,
                                                        phone,
                                                        password,
                                                        validation_status,
                                                        activity_status,
                                                        created_date,
                                                        phone_confirmed,
                                                        email_confirmed,
                                                        user_mngment_id
                                                        )
                              VALUES (%s, %s, %s, 'inactive','offline',%s, 'False', 'False', %s)
                              RETURNING id
                            """
                    new_id = gs.sql_execute(pool, sql, commit=True, para_values=(data['email'],
                                                                                 data['phoneNumber'],
                                                                                 data['password'],
                                                                                 create_date,
                                                                                 new_profile_id))

                    # insert role-user to db
                    sql_insert_role = ''' insert into user_account_user_role(user_role_id,user_account_id) 
                                    Values  (%s,%s)
                                    returning id'''
                    gs.sql_execute(pool, sql_insert_role, commit=True, para_values=(role_id[0][0], new_id[0][0]))

                    # insert wallet to db
                    sql_insert_wallet = ''' insert into user_wallet(total_balance,available_balance,state,created_date,write_date,user_account_id,unitcode) 
                                            Values  (0,0,'active',%s,%s,%s,'vnd')
                                            returning id'''
                    gs.sql_execute(pool, sql_insert_wallet, commit=True,
                                   para_values=(create_date, create_date, new_id[0][0]))

                    # insert profile
                    sql_insert_profile = '''INSERT INTO doctor_profile (user_mngment_id,consultation_type,consultation_fee,consultation_duration,about,hospital_profile_id,consultation_unit) VALUES (%s,'live','1000','15m','New',%s,'vnd') RETURNING id'''
                    new_doctor_id = gs.sql_execute(pool, sql_insert_profile, commit=True, para_values=(new_profile_id,hospital_id))

                    sql_insert_profile_sp = ''' insert into dr_doctorprofile_specialties (doctor_profile_id,specialties_code,years_of_exp) values (%s,'paediatrics',1) ;'''
                    gs.sql_execute(pool, sql_insert_profile_sp, commit=True, para_values=(new_doctor_id[0][0]))

                    # insert user_contact
                    sql_insert_contact = ''' INSERT INTO user_contact(user_mngment_id) VALUES (%s) returning id'''
                    new_id_contact = gs.sql_execute(pool, sql_insert_contact, commit=True,
                                                    para_values=(new_profile_id,))
                    # insert user_address with primary = true
                    sql_insert_address = ''' INSERT INTO user_address(user_contact_id,is_primary) 
                                    VALUES  (%s,'true')
                                    returning id'''
                    gs.sql_execute(pool, sql_insert_address, commit=True, para_values=(new_id_contact[0][0],))
                    # insert user_address with primary = fals
                    all_ok = True
                    
                    if all_ok == True:
                        resp.update({'status_code': 200,
                                     'resq_body': {
                                         "userId": new_id[0][0],
                                     }
                                     })
                else:
                    resp.update({
                        'status_code': 401,
                        'resq_body': {
                            "code": "no_exist_role",
                            "title": _("No exist role"),
                            "msg": _("The role does not exists in our system, please try to register with another one")
                        }
                    })

        except Exception as e:
            logger.error('admin_add_doctor %s' % str(e))
            client_sentry.captureException()

        return resp        


    
    def admin_call_logs(self, data):
        logger.info('call logs %s' % data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "call logs",
                        "title": _("call logs"),
                        "msg": _("An error occurred. Please try to call logs again.")
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            hospital_id = 0 
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    sql_hospital = ''' select hospital_profile_id from admin_profile where user_account_id = %s ''' % (user_id)
                    hospital_id  = gs.sql_execute(pool, sql_hospital)[0][0]
                    sql_call_record = '''select id,patient_id,doctor_id,status_call,last_update from call_record_logs where id >= 0'''
                    logger.info('call logs %s' % sql_call_record)
                    res_call_record = gs.sql_execute(pool, sql_call_record)
                    tmp_res = []
                    #logger.info('call logs %s' % res_call_record)
                    if res_call_record:
                        for line in res_call_record:
                            tmp_res.append({
                                    'patient_id': line[1],
                                    'doctor_id': line[2],
                                    'call_status': line[3],
                                    'last_update':  int(time.mktime(line[4].timetuple())) or None
                                    })
                        resp.update({'status_code': 200,
                                     'resq_body': tmp_res
                                     })
                    else:
                        resp.update({'status_code': 200,
                                     'resq_body': []
                                     })
                    logger.info('call logs %s' % resp)
                    
            else: 
                resp.update({
                    'status_code': 401,
                    'resq_body': {
                        "code": "Error call logs",
                        "title": _("error"),
                        "msg": _("errorrr")
                    }
                })
        except Exception as e:
            logger.error('admin call logs %s' % str(e))
            client_sentry.captureException()

        return resp            

    def patient_call_logs(self, data):
        
        logger.info('patient call logs %s' % data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Patient call logs failed"),
                        "msg": _("An error occurred. Please try to get Patient call logs again."),
                        "code": "patient_call_logs_failed",
                        "test" : data
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            requestCount = int(data.get('requestCount'))
            nextItemId = int(data.get('nextItemId'))
            
            hospital_id = int(data.get('hospital_id', 0))
            
            res = {
                'pagination': {
                    "resultCount": 0,
                    "totalCount": 0,
                    "nextItemId": 0
                },
                'list': [],
            }
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    start, end = 0, 0
                    user_id = res_token[1]
                    if not nextItemId or nextItemId == 0:
                        full_list_ids = self.generate_pagination_list(user_id,'patient_call_logs',context={'api': 'patient_call_logs', 'hospital_id' : hospital_id})
                        start = 0
                    else:
                        sql = '''select list_id 
                                from user_account_pagination
                                where user_account_id = %s
                                    and table_pagination = '%s'
                                ''' % (user_id, 'patient_call_logs')
                        full_list_ids = gs.sql_execute(pool, sql)
                        if full_list_ids:
                            full_list_ids = eval(full_list_ids[0][0])
                            start = full_list_ids.index(nextItemId)
                        else:
                            full_list_ids = self.generate_pagination_list(user_id,'patient_call_logs',context={'api': 'patient_call_logs', 'hospital_id' : hospital_id})
                            start = 0

                    if not full_list_ids:
                        resp.update({'status_code': 200,
                                     'resq_body': res
                                     })
                        return resp

                    if (start + requestCount) <= len(full_list_ids):
                        end = start + requestCount
                    else:
                        end = len(full_list_ids)

                    select_list_ids = full_list_ids[start:end]
                    res['pagination']['totalCount'] = len(full_list_ids)
                    res['pagination']['nextItemId'] = full_list_ids[end] if end < len(full_list_ids) else full_list_ids[end - 1]
                    res['pagination']['resultCount'] = len(select_list_ids)

                    # get list_specialties
                    if len(select_list_ids) > 0:
                        select_list_ids = select_list_ids + [-1, -1]
                        
                        sql_call = ''' select apm.id, --0
                                    apm.schedule_date, --1
                                    apm.state, --2
                                    umng.id, --3
                                    umng.first_name, --4
                                    umng.middle_name, --5
                                    umng.last_name, --6
                                    umng.profile_image_url,--7
                                    apm.consultation_note, --8
                                    mta.value, --9
                                    mta.unit, --10
                                    umng.gender, --11, 
                                    arev.rating_point, --12
                                    apm.cost, --13
                                    apm.consultation_duration, --14
                                    drsp.specialties_code, --15
                                    ua.id, --16
                                    ach.state as call_history_state, --17
                                    ach.create_date as call_history_time, --18
                                    ach.start_time as call_start_time,  --19
                                    ach.end_time as call_end_time, --20
                                    arev.comment, --21
                                    apm.unit_cost, --22
                                    dp.years_of_experience, --23
                                    apm.type, --24
                                    apm.booking_status --25
                                from appointment apm
                                left join user_account ua on apm.doctor_id = ua.id
                                left join user_mngment umng on  umng.id = ua.user_mngment_id
                                left join meta_dr_consultation_durations mta on mta.code = apm.consultation_duration
                                left join doctor_profile dp on dp.user_mngment_id = umng.id 
                                left join dr_doctorprofile_specialties drsp on drsp.doctor_profile_id = dp.id
                                left join appointment_review arev on arev.appointment_id = apm.id
                                inner join appointment_call_history ach on ach.appointment_id = apm.id
                                where ach.id in %s
                                order by apm.schedule_date desc '''  % (str(tuple(select_list_ids)))
                        records = gs.sql_execute(pool, sql_call)
                        apms = []
                        if records:
                            for line in records:
                                status_call = 'incomplete'
                                status_call_name = 'Incomplete'
                                if line[2] in ('successful_finished','free_finished','canceled_by_doctor'):
                                    status_call = 'complete'
                                    status_call_name  = 'Complete'
                                #if line[19] and line[20]:
                                #    status_call = 'accept_call'
                                #    status_call_name = 'Accepted Call'
                                #elif line[20] or line[2] == 'denied':
                                #    status_call = 'denied_call'
                                #    status_call_name = 'Denied Call'
                                if line[2] in ('successful_finished','free_finished','canceled_by_doctor') and line[12] in ('False', 'None', '', None, 'null'):
                                    isAvailableToReview = True
                                else:
                                    isAvailableToReview = False
                                if status_call != 'missed_call' or True == True:
                                    if not line[25] in ('False', 'None', '','null',None) : 
                                        apms.append({
                                        'doctor': {
                                            'id': line[3],
                                            'firstName': None if line[4] in ('False', 'None', '') else line[4],
                                            'middleName': None if line[5] in ('False', 'None', '') else line[5],
                                            'lastName': None if line[6] in ('False', 'None', '') else line[6],
                                            'avatar': line[7],
                                            'userId': line[16],
                                            'specialtyCodes': [{
                                                'yearsOfExp': line[23],
                                                'code' : line[15]
                                            }]
                                        },
                                        'id': line[0],
                                        'stateCode': _(status_call),
                                        'stateCodeName': _(status_call_name),
                                        'appointmentState': line[2],
                                        'isAvailableReview': isAvailableToReview,
                                        'typeCode': _(line[24]),
                                        'scheduleTime': int(time.mktime(line[1].timetuple())) or None,
                                        'duration': {
                                            'value': line[9],
                                            'unitCode': line[10]
                                        },
                                        'consultation_fee': line[13],
                                        'consultation_duration': line[14],
                                        'consultation_unit': line[22],
                                        'review': {
                                            'ratingPoint': 0 if line[12] in ('False', 'None', '', None, 'null') else line[12],
                                            'comment': '' if line[21] in ('False', 'None', '', None, 'null') else line[21]
                                        },
                                        'note': line[8],
                                        'genderCode':line[11],
                                        'call_history_state':line[17],
                                        'call_history_time':int(time.mktime(line[18].timetuple())) or None,
                                        'call_start_time': 'null' if line[19] is None else int(time.mktime(line[19].timetuple())),
                                        'call_end_time': 'null' if line[20] is None else int(time.mktime(line[20].timetuple())),
                                        'booking_status':line[25]
                                    
                                    })

                                res['list'] = apms
                        resp.update({'status_code': 200,
                            'resq_body': res
                            })
            else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_call_logs %s' % str(e))
            resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Patient call logs failed"),
                        "msg": _("An error occurred. Please try to get Patient call logs again."),
                        "code": "patient_call_logs_failed",
                        "test" : data,
                        "error" : str(e)
                    }
                }
            #client.captureException()
        return resp


    def get_locale_user_account(self, data):
        
        logger.info('patient locale %s' % data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("locale failed"),
                        "msg": _("An error occurred. Please try to get locale again."),
                        "code": "locale_failed",
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            
            logger.info('patient locale %s' % user_token)
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    logger.info('patient locale res_token %s' % str(res_token))

                    user_id = res_token[1]
                    sql = '''select id,locale from user_account where id  = %s ''' % (user_id)
                    locale_user = gs.sql_execute(pool, sql)
                    logger.info('patient locale user %s' % str(locale_user))

                    resp.update({'status_code': 200,
                        'resq_body': {
                                    "id": locale_user[0][0],
                                    "locale": locale_user[0][1]
                                }
                    })
            else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('locale %s' % str(e))
            
            resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("locale failed"),
                        "msg": _("An error occurred. Please try to get locale again."),
                        "code": "locale_failed",
                        "test" : data,
                        "error" : str(e)
                    }
                }
            #client.captureException()
        return resp

    def update_locale_user_account(self, data):
        
        logger.info('patient locale %s' % data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("locale failed"),
                        "msg": _("An error occurred. Please try to update locale again."),
                        "code": "locale_failed",
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            locale = data.get('locale')
            
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    logger.info('patient locale user %s' % str(res_token))
                    logger.info('patient locale data %s' % str(data))
                    sql = '''update user_account set locale = '%s' where id  = %s ''' % (str(locale),user_id)
                    gs.sql_execute(pool, sql, commit=True)
                    
                    logger.info('patient update sql %s' % str(sql))
                    resp.update({'status_code': 200,
                        'resq_body': {
                                    "code": "success_update_locale",
                                    "title": _("Success Update"),
                                    "msg": _("success_update_locale")
                                }
                    })
            else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('locale %s' % str(e))
            resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("locale failed"),
                        "msg": _("An error occurred. Please try to get locale again."),
                        "code": "locale_failed",
                    }
                }
            #client.captureException()
        return resp