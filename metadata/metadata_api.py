#!/usr/bin/env python
# -*- coding: utf-8 -*-
import falcon
import json
import time
from datetime import datetime
from metadata_business import metadata_business

status_code = {
    200: falcon.HTTP_200,
    401: falcon.HTTP_401,
    502: falcon.HTTP_502,
    203: falcon.HTTP_203
}


class metadata_api(object):
    def __init__(self, signal, erp_connection=False, var_contructor=False):
        self.pool_conn = erp_connection
        self.metadata = metadata_business(signal, erp_connection, var_contructor)

        if var_contructor:
            self.ERP_DB_HOST = var_contructor['ERP_DB_HOST']
            self.ERP_DB_PORT = var_contructor['ERP_DB_PORT']
            self.ERP_DB_USER = var_contructor['ERP_DB_USER']
            self.ERP_DB_PASS = var_contructor['ERP_DB_PASS']
            self.ERP_DB_NAME = var_contructor['ERP_DB_NAME']

        self.signal = signal

    def on_get(self, req, resp, hospital_id = None):
        try:
            if self.signal == 'get_metadata':
                header = req.headers  # key cua header luon viet hoa
                lang = header.get("ACCEPT-LANGUAGE", 'en')
                resp_status = status_code[401]
                resp_body = {'status_code': 401,
                             'resq_body':
                                 {
                                     "title": "Get matadata failed",
                                     "msg": "An error occurred. Please try to request again.",
                                     "code": "get_metadata_failed"
                                 }
                             }
                try:
                    record = self.metadata.get_metadata({'language': lang})
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']
                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'check_metadata':
                resp_status = status_code[401]
                resp_body = {'status_code': 401,
                             'resq_body':
                                 {
                                     "title": "Check matadata failed",
                                     "msg": "An error occurred. Please try to request again.",
                                     "code": "check_metadata_failed"
                                 }
                             }
                try:
                    record = self.metadata.check_metadata()
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']
                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'get_current_time':
                resp_status = status_code[401]
                resp_body = {'status_code': 401,
                             'resq_body':
                                 {
                                     "title": "Getting current system time failed",
                                     "msg": "An error occurred. Please try to get again.",
                                     "code": "get_current_time_failed"
                                 }
                             }
                try:
                    cr_time = int(time.mktime(datetime.now().timetuple()))
                    resp_status = status_code[200]
                    resp_body = {'currentSystemTime': cr_time}

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            elif self.signal == 'check_version_app':
                header = req.headers  # key cua header luon viet hoa
                lang = header.get("ACCEPT-LANGUAGE", 'en')
                resp_status = status_code[401]
                resp_body = {'status_code': 401,
                             'resq_body':
                                 {
                                     "title": "Check version failed",
                                     "msg": "An error occurred. Please try to get again.",
                                     "code": "get_check_version_failed"
                                 }
                             }
                try:
                    record = self.metadata.check_version({'language': lang})
                    resp_status = status_code[200]
                    resp_body = {'data': record}

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

        except Exception as e:
            pass
