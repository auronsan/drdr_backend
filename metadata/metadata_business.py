#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from raven import Client
import ConfigParser
from exceptions import Exception
import logging
import os
from datetime import datetime
import time

project_path =  str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
config = ConfigParser.RawConfigParser(allow_no_value=True)
config.read(project_path + '/' + "environment.conf")
sentry_key = config.get('environment', 'sentry_key')
client = Client(sentry_key)
sys.path.insert(0, "..")
try:
    from general import general_business
except ImportError as e:
    print('No Import ',e)
logger = logging.getLogger('drdr_api')

class metadata_business(object):
    def __init__(self, signal, erp_connection, var_contructor):
        self.pool_conn = erp_connection
        self.general_business = general_business(erp_connection, var_contructor)
        
        self.ERP_DB_HOST = var_contructor['ERP_DB_HOST']
        self.ERP_DB_PORT = var_contructor['ERP_DB_PORT']
        self.ERP_DB_USER = var_contructor['ERP_DB_USER']
        self.ERP_DB_PASS = var_contructor['ERP_DB_PASS']
        self.ERP_DB_NAME = var_contructor['ERP_DB_NAME']

        self.signal = signal
        self.vi_ = self.general_business.vi_
        self.en_ = self.general_business.en_

    def get_metadata_general(self, table_meta, lang='en'):
        if lang == 'vi':
            _ = self.vi_
        else:
            _ = self.en_

        result = []
        try:
            sql_string = 'insert into %s(code,name) values %s'
            gs = self.general_business
            pool = self.pool_conn
            sql = """select code, name
                            from %s
                            """%table_meta
            res = gs.sql_execute(pool, sql)
            record = []
            for r in res:
                result.append({
                    'code': r[0] or '',
                    'name': _(r[1].decode("utf-8")) or '',
                })
                record.append("('%s','%s')"%(r[0],r[1]))
            # print sql_string%(table_meta,', '.join(record))

        except Exception as e:
            logger.error('error_get_metadata_general %s - %s' % (table_meta,str(e)))
            client.captureException()

        return result

    def check_metadata(self, data=None):
        logger.info('check_metadata')
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": "Check matadata failed",
                        "msg": "An error occurred. Please try again.",
                        "code": "metadata_failed"
                    }
                }
        try:
            update_time = self.get_update_time('update_time')
            resp.update({
                        'status_code': 200,
                        'resq_body': {
                            'latestUpdatedTime': update_time
                                      }
                         })
        except Exception as e:
            logger.error('error_check_metadata %s'%str(e))
            client.captureException()
    
        return resp
    
    def get_metadata(self, data=None):
        logger.info('get_metadata %s',data)
        language = data.get('language','en')
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": "Get matadata failed",
                        "msg": "An error occurred. Please try again.",
                        "code": "metadata_failed"
                    }
                }
        try:
            code='''update_time'''
            update_time = self.get_update_time(code)
            user_role = self.get_metadata_general('user_role', language)
            meta_specialities = self.get_metadata_general('meta_dr_specialties', language)
            meta_country_phone = self.get_metadata_general('meta_country_phone_code', language)
            meta_doctor_consultation_types = self.get_metadata_general('meta_dr_consultation_types', language)
            meta_genders = self.get_metadata_general('meta_genders', language)
            meta_education_levels = self.get_metadata_general('meta_education_levels', language)
            meta_languages = self.get_metadata_general('meta_languages', language)
            meta_titles = self.get_metadata_general('meta_titles', language)
            meta_cities = self.get_metadata_general('meta_cities', language)
            meta_countries = self.get_metadata_general('meta_countries', language)
            meta_dr_languages = self.get_metadata_general('meta_dr_languages', language)
            meta_dr_consultation_durations = self.get_meta_dr_consultation_durations(language)
            meta_time_units = self.get_metadata_general('meta_time_units', language)
            meta_currency_units = self.get_meta_currency_units()
            meta_call_types = self.get_metadata_general('meta_call_types', language)
            appointment_types = self.get_metadata_general('meta_apointment_types', language)
            appointment_states = self.get_metadata_general('meta_apointment_states', language)
            payment_options = self.get_metadata_general('meta_payment_options', language)
            medical_record_fields = self.get_metadata_general('meta_medicalrecord_fields', language)
            mobile_operators = self.get_metadata_general('meta_mobile_operators', language)
            appointment_finishing_options = self.get_meta_appointment_finishing_options(language)
            meta_banks = self.get_meta_banks()
            meta_transactions_categories = self.get_metadata_general('meta_transaction_categories', language)
            meta_payout_states = self.get_metadata_general('meta_payout_states', language)
            meta_booking_status = self.get_metadata_general('meta_booking_status', language)
            resp.update({
                'status_code': 200,
                'resq_body': {
                    'updateTime': update_time or 0,
                    'userRoles': user_role,
                    'doctorSpecialties': meta_specialities,
                    'countryPhoneCodeList': meta_country_phone,
                    'doctorConsultationTypes': meta_doctor_consultation_types,
                    'genders': meta_genders,
                    'educationLevels': meta_education_levels,
                    'languages': meta_languages,
                    'titles': meta_titles,
                    'cities': meta_cities,
                    'countries': meta_countries,
                    'doctorSpokenLanguages': meta_dr_languages,
                    'doctorConsultationDurations': meta_dr_consultation_durations,
                    'appointmentStates': appointment_states,
                    'appointmentTypes': appointment_types,
                    'callTypes': meta_call_types,
                    'currencyUnits': meta_currency_units,
                    'timeUnits': meta_time_units,
                    'paymentOptions': payment_options,
                    'medicalRecordFields': medical_record_fields,
                    'mobileOperators': mobile_operators,
                    'appointmentFinishingOptions': appointment_finishing_options,
                    'banks': meta_banks,
                    'transactionCategories': meta_transactions_categories,
                    'payoutStates': meta_payout_states,
                    'bookingStatus': meta_booking_status,
                }
            })
        except Exception as e:
            logger.error('error_get_metadata %s'%str(e))
            client.captureException()

        return resp

    def get_update_time(self,code):
        value = ''
        try:
            gs = self.general_business
            pool = self.pool_conn
            sql = """select value
                    from system_params
                    where code=%s
                    """
            res = gs.sql_execute(pool,sql,para_values = (code,))
            value = res[0][0]
            value = int(time.mktime(datetime.strptime(value, '%Y-%m-%d %H:%M:%S').timetuple()))

        except Exception as e:
            logger.error('error_get_update_time %s'%str(e))
            client.captureException()
        
        return value

    def get_meta_dr_consultation_durations(self, lang):
        if lang == 'vi':
            _ = self.vi_
        else:
            _ = self.en_
        final_res = []
        try:
            sql_string = 'insert into %s(code,value, unit) values %s'
            gs = self.general_business
            pool = self.pool_conn
            sql = """select code, value, unit
                    from meta_dr_consultation_durations
                    """
            res = gs.sql_execute(pool, sql)
            record = []
            for r in res:
                final_res.append({
                    'code': r[0] or '',
                    'value': _(r[1]) or '',
                    'unitCode': r[2] or ''
                })
                record.append("('%s','%s','%s')" % (r[0], r[1], r[2]))
            # print sql_string % ('meta_dr_consultation_durations', ', '.join(record))
        except Exception as e:
            logger.error('error_get_meta_dr_consultation_durations %s' % str(e))
            client.captureException()

        return final_res

    def get_meta_currency_units(self):
        final_res = []
        try:
            sql_string = 'insert into %s(code, name, short_name) values %s'
            gs = self.general_business
            pool = self.pool_conn
            sql = """select code, name, short_name
                    from meta_currency_units
                    """
            res = gs.sql_execute(pool, sql)
            record = []
            for r in res:
                final_res.append({
                    'code': r[0] or '',
                    'name': r[1] or '',
                    'short_name': r[2] or ''
                })
                record.append("('%s','%s','%s')" % (r[0], r[1], r[2]))
            # print sql_string % ('meta_currency_units', ', '.join(record))
        except Exception as e:
            logger.error('error_get_meta_currency_units %s' % str(e))
            client.captureException()

        return final_res

    def get_meta_appointment_finishing_options(self, lang):
        if lang == 'vi':
            _ = self.vi_
        else:
            _ = self.en_
        appointment_finishing_options = []
        try:
            sql_string = 'insert into %s(state, option, explanation, text) values %s'
            gs = self.general_business
            pool = self.pool_conn
            sql = """SELECT coalesce(state,''),
                            coalesce(option,''),
                            coalesce(explanation,''),
                            coalesce(text,'') 
                    FROM meta_appointment_finishing_options order by id;"""
            res = gs.sql_execute(pool, sql)
            record = []
            for r in res:
                appointment_finishing_options.append({
                    'state': None if r[0] == '' else r[0],
                    'option': None if r[1] == '' else r[1],
                    'explanation': None if r[2] == '' else _(r[2]),
                    'text': None if r[3] == '' else _(r[3]),
                })
                record.append("('%s','%s','%s','%s')" % (r[0], r[1], r[2], r[3]))
            # print sql_string % ('meta_appointment_finishing_options', ', '.join(record))
        except Exception as e:
            logger.error('error_get_meta_appointment_finishing_options %s' % str(e))
            client.captureException()
        return appointment_finishing_options

    def get_meta_banks(self):
        meta_banks = []
        try:
            sql_string = 'insert into %s(name, bank_group_code) values %s'
            gs = self.general_business
            pool = self.pool_conn
            sql = '''select mb.id, mb.name, mbg.name, bank_group_code
                    FROM meta_bank mb 
                    inner join meta_bank_group mbg on mb.bank_group_code = mbg.code
                    '''
            res = gs.sql_execute(pool, sql)
            bank_group = 'xxxxxx'
            banks = []
            group = {}
            record = []
            for r in res:
                if bank_group <> r[2]:
                    if len(banks) > 0:
                        meta_banks.append(group)
                    group = {}
                    banks = []
                    banks.append({
                        'id': r[0] or '',
                        'name': r[1] or '',
                    })
                    group.update({
                        "group": r[2],
                        "bankList": banks
                    })
                    bank_group = r[2]

                else:
                    group["bankList"].append({
                        'id': r[0] or '',
                        'name': r[1] or '',
                    })
                    record.append("('%s','%s')" % (r[1], r[3]))
            # print sql_string % ('meta_bank', ', '.join(record))
            meta_banks.append(group)
        except Exception as e:
            logger.error('error_get_meta_banks %s' % str(e))
            client.captureException()
        return meta_banks

    def check_version(self, data = None):
        language = data.get('language','en')
        if language == 'vi':
            _ = self.vi_
        else:
            _ = self.en_
        update_version = {}
        logger.info('check_version')
        try:
            gs = self.general_business
            pool = self.pool_conn
            
            sql = '''select id,code,value from system_params where code = 'ios_version' or code = 'android_version' or code = 'ios_link' or code = 'android_link'
                    '''
            res = gs.sql_execute(pool, sql)
            
            android_version =''
            android_link = ''
            ios_version = ''
            ios_link = ''
            
            if res:
                for item in res :
                    if item[1] == 'ios_version':
                        ios_version = item[2]
                    elif item[1] == 'android_version':
                        android_version = item[2]
                    elif item[1] == 'ios_link':
                        ios_link = item[2]
                    elif item[1] == 'android_link':
                        android_link = item[2]


            update_version = {
                "title" :_("Notice"),
                "message":_("Please update new version for faster connection"),
                "ios_version": ios_version,
                "ios_link_store": ios_link,
                "android_version": android_version,
                "android_link_store": android_link
                }
            
        except Exception as e:
            logger.error('error_check_version %s' % str(e))
            #client.captureException()
        return update_version
