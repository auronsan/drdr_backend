import datetime
import hashlib
import random
import facebook
import psycopg2
import time


# import sys
# from _pytest.assertion.oldinterpret import check


class hash_code_value():
    # hash code for token login
    # input id and return hash code md5 with id and datetime now
    def hash_id(self, id):
        '''
        convert datetime now to strftime('%Y%m%d%H%M%S') syte
        match id and datetime now
        hash this value and return it
        '''
        res = {}
        ids = str(id)
        value_hash = ids + datetime.datetime.now().strftime('%Y%m%d%H%M%S')
        res = hashlib.md5(value_hash).hexdigest()
        return res

    # hash code password
    # input passw and return hash code md5 with id and 4 number random
    def hash_passw(self, passw):
        '''
        Radnom value number 4 rande and hash this number
        endcode base64 password value and hash this value
        match hash(value pass) and string $$SNOW$$ and hash(value number)
        hash value match and return this value
        '''
        res = {}

        num = random.sample(range(1000, 9999), 1)
        pass_value = str(passw).encode('base64')
        pass_endcode = pass_value.encode('base64')

        hash_random = hashlib.md5(str(num[0])).hexdigest()
        hash_pass_value = hashlib.md5(pass_endcode).hexdigest()
        match_hash = hash_pass_value + "$$SNOW$$" + hash_random
        res = {'number_key': num[0],
               'pass_value': hashlib.md5(match_hash).hexdigest(),
               }
        return res

    def get_infomation_facebook(self, token, user_id=None):
        # token="EAADxMfJI2ZCsBAKIObMnmaqpZB4DZAzr9Qi2Ancu9N5DHKyyzgdNjjqaJmswLqlNMEoBabiziLiQX47ohWZAtbaWaJKZAtk05Im0xZAhnjdNvsVSA7H8dZC9hT823G7odWp4lLmZBt8AZAx6sGAZATFWZCbVy9fIDZBhfBgFWbbfdlszAPqRiYxX0bQDK4ZC9BjqpIz40DSrWZCi7xULSm1D4dzSy1"
        facebook_graph = facebook.GraphAPI(token)
        # Try to post something on the wall.
        try:

            profile = facebook_graph.get_object('me')
            print(profile)
            args = {'fields': 'id,name,email,work,birthday,education,location,gender'}
            profile = facebook_graph.get_object('me', **args)
            res = {'all': profile,
                   'facebook_id': profile['id'],
                   'email': profile['email'],
                   'gender': profile['gender'],
                   'birthday': profile['birthday'],
                   'location': profile['location']['name'],
                   }
            return res
        except facebook.GraphAPIError as e:
            print 'Something went wrong:', e.type, e.message
        return facebook_graph

    def check_first_login_fb(self, cursor, token, fb_id):
        check_data = None
        if token:
            check_data = cursor.execute(
                "select access_token from facebook_account where access_token = '" + token + "'")
            if check_data:
                return False
        if fb_id:
            check_data = cursor.execute("select id from facebook_account where facebook_id = '" + fb_id + "'")
            if check_data:
                cursor.execute(
                    "UPDATE facebook_account SET access_token = '" + token + "' WHERE facebook_id = '" + fb_id + "'")
                return False
        return True

    def save_user_profile(self, user_date, token):
        conn_string = "host='doctordoctor.help' dbname='helloDoctor' user='drdrhelp' password='doctorssaymeanth12gs'"

        print (conn_string)

        conn = psycopg2.connect(conn_string)

        cursor = conn.cursor()
        if user_date:
            if self.check_first_login_fb(cursor, token, user_date['facebook_id']) == False:
                return
            else:
                cursor.execute(
                    "INSERT INTO facebook_account (access_token, facebook_id) values ('" + token + "','" + user_date[
                        'facebook_id'] + "')")
                cursor.execute(
                    "INSERT INTO user_account (email, validation_status,email_confirmed) values ('" + user_date[
                        'email'] + "','active', TRUE)")

        # cursor.execute("INSERT INTO user_account (email, validation_status,email_confirmed) values ('"+user_date['email']+"','active', TRUE)")
        return

    def get_profile_account(self, data):
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": "Get profile account failed",
                        "msg": "An error occurred. Please try to get profile account again.",
                        "code": "get_profile_account_failed"
                    }
                }
        try:
            id_user = data['id_user']
            res = {}
            conn_string = "host='doctordoctor.help' dbname='helloDoctor' user='drdrhelp' password='doctorssaymeanth12gs'"

            conn = psycopg2.connect(conn_string)
            cursor = conn.cursor()

            if id_user:
                cursor.execute('''select    title,
                                                    last_name,
                                                    middle_name, 
                                                    first_name,
                                                    complete_name,
                                                    birthday,
                                                    gender,
                                                    ethnicity,
                                                    highest_education_level,
                                                    occupation,
                                                    material_status,
                                                    is_primary,                                        
                                                    number,
                                                    street,
                                                    city,
                                                    state,
                                                    postcode,
                                                    country,

                                                    home_phone,
                                                    work_phone,
                                                    mobile,
                                                    email

                                           from user_mngment um
                                                left join patient_profile pp on  um.id = pp.user_mngment_id 
                                                left join user_contact uc on um.id = uc.user_mngment_id  
                                           where um.id ='%s' ''' % id_user)
                records = cursor.fetchall()

                if records:
                    res.update({
                        'titleCode': records[0][0] or False,
                        'lastName': records[0][1] or False,
                        'middleName': records[0][2] or False,
                        'firstName': records[0][3] or False,
                        'completeName': records[0][4] or False,
                        'dayOfBirth': int(time.mktime(records[0][5].timetuple())) or False,
                        'gender': records[0][6] or False,
                        'ethnicity': records[0][7] or False,
                        'highestEducationLevelCode': records[0][8] or False,
                        'occupation': records[0][9] or False,
                        'maritalStatusCode': records[0][10] or False,
                    })
                    for i in records:
                        if i[11] == True:
                            res.update({
                                'primaryAddress': {
                                    'number': i[12] or False,
                                    'street': i[13] or False,
                                    'cityId': i[14] or False,
                                    'state': i[15] or False,
                                    'postcode': i[16] or False,
                                    'countryId': i[17] or False,
                                },
                                'contact': {
                                    'homephone': i[18] or False,
                                    'workphone': i[19] or False,
                                    'mobile': i[20] or False,
                                    'email': i[21] or False,
                                    # 'emergencyContact': i[22],
                                    # 'name': ,
                                    # 'number': ,
                                    # 'relationship': ,
                                },
                            })
                        else:
                            res.update({
                                'alternateAddress': {
                                    'number': i[12] or False,
                                    'street': i[13] or False,
                                    'cityId': i[14] or False,
                                    'state': i[15] or False,
                                    'postcode': i[16] or False,
                                    'countryId': i[17] or False,
                                },

                            })

                resp.update({'status_code': 200,
                             'resq_body': res
                             })

        except Exception as e:
            print e
            #logger.error('error_tokbox_deregister %s' % str(e))

        try:
            conn.close()
        except Exception as e:
            print e
            #logger.error('error_close_conn_error_tokbox_deregister %s' % str(e))
        return resp


# def login_with_fb(self,token=None):
#         fb_profile = self.get_infomation_facebook(token)
#         if fb_profile['facebook_id']:
#         return

if __name__ == "__main__":
    x = hash_code_value()
    # print(hash_code_value().hash_passw('abc'))
    token_test = 'EAADxMfJI2ZCsBAKIObMnmaqpZB4DZAzr9Qi2Ancu9N5DHKyyzgdNjjqaJmswLqlNMEoBabiziLiQX47ohWZAtbaWaJKZAtk05Im0xZAhnjdNvsVSA7H8dZC9hT823G7odWp4lLmZBt8AZAx6sGAZATFWZCbVy9fIDZBhfBgFWbbfdlszAPqRiYxX0bQDK4ZC9BjqpIz40DSrWZCi7xULSm1D4dzSy1'
    test = {'id_user': 1, }
    user_data = x.get_profile_account(test)
    # x.save_user_profile(user_data,token_test)
#         res = {}
#         passw ='123412345656123456789s'
#         pass_value = str(passw).encode('base64')
#         num = random.sample(range(1000,9999),1)
#         pass_endcode = pass_value.encode('base64')
#         hash_random =hashlib.md5(str(num[0])).hexdigest()
#         hash_pass_value = hashlib.md5(pass_endcode).hexdigest()
#         # hash pass and $$SNOW$$ and hash number
#         metch_hash= hash_pass_value+"$$SNOW$$"+hash_random
#         res = hashlib.md5(metch_hash).hexdigest()
