# -*- encoding: utf-8 -*-
from flask import Flask, render_template, request, redirect, url_for
from opentok import OpenTok, MediaModes, OutputModes
from email.utils import formatdate
import time
app = Flask(__name__)
#1. Secssion được khởi tạo tại đây
opentok = OpenTok(45882532, 'ea48282fe2a0c7580294b8a65f41a15ca38904ac')
session = opentok.create_session(media_mode=MediaModes.routed)

@app.template_filter('datefmt')
def datefmt(dt):
    return formatdate(time.mktime(dt.timetuple()))

@app.route("/")
def index():
    return render_template('index.html')

#2. Host (Người publish một session) sử dụng 2 thành phần : session_id và token(được tạo từ session_id)
@app.route("/host")
def host():
    key = 45882532
    session_id = session.session_id
    token = opentok.generate_token(session_id)
    return render_template('host.html', api_key=key, session_id=session_id, token=token)

#3. Participant(Vai trò là người kết nối vào Host ở #2) yêu cầu dùng chung 1 session_id  và token(được tạo từ session_id)
@app.route("/participant")
def participant():
    key = 45882532
    session_id = session.session_id
    token = opentok.generate_token(session_id)
    return render_template('participant.html', api_key=key, session_id=session_id, token=token)

"""
#4. Bắt đầu một Archiving sau khi đã connect với session thành công
    - session_id: Được tạo ở #1
    - has_audio: bằng true nếu bật âm thanh cuộc gọi, hoặc false nếu tắt tiếng
    - has_video: bằng true bật hình ảnh cuộc gọi, hoặc false nếu tắt hình ảnh
    - output_more: trong tất cả các stream sẽ được lưu trư thành 1 file trung (OutputModes.composed)
                    hoặc thành các file riêng lẻ: (OutputModes.individual)
"""
@app.route("/start", methods=['POST'])
def start():
    has_audio = 'hasAudio' in request.form.keys()
    has_video = 'hasVideo' in request.form.keys()
    output_mode = OutputModes[request.form.get('outputMode')]
    archive = opentok.start_archive(session.session_id, name="Python Archiving Sample App",
                                    has_audio=has_audio, has_video=has_video, output_mode=output_mode)
    return archive.json()

#5. Kết thúc một archive khi đang live
@app.route("/stop/<archive_id>")
def stop(archive_id):
    archive = opentok.stop_archive(archive_id)
    return archive.json()


@app.route("/history")
def history():
    page = int(request.args.get('page', '1'))
    offset = (page - 1) * 5
    archives = opentok.get_archives(offset=offset, count=5)

    show_previous = '/history?page=' + str(page-1) if page > 1 else None
    show_next = '/history?page=' + str(page+1) if archives.count > (offset + 5) else None

    return render_template('history.html', archives=archives, show_previous=show_previous,
                            show_next=show_next)

@app.route("/download/<archive_id>")
def download(archive_id):
    archive = opentok.get_archive(archive_id)
    return redirect(archive.url)

@app.route("/delete/<archive_id>")
def delete(archive_id):
    opentok.delete_archive(archive_id)
    return redirect(url_for('history'))


if __name__ == "__main__":
    app.debug = True
    app.run()
