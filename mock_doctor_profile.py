#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.insert(0, "..")
try:
    from connection_pool import connection_pool
    from user_account.user_account_business import user_account_business
    from tokbox.tokbox_business import tokbox_business
    from metadata.metadata_business import metadata_business
    from general import general_business
    from profile.profile_patient_business import profile_patient_business
    from profile.profile_doctor_business import profile_doctor_business
except ImportError as e:
    print('No Import ',e)


conf = {
            'ERP_DB_HOST' : 'doctordoctor.help',
            'ERP_DB_PORT' : 5432,
            'ERP_DB_USER' : 'drdrhelp',
            'ERP_DB_PASS' : 'doctorssaymeanth12gs',
            'ERP_DB_NAME' : 'helloDoctor',
            'tokbox_conf' : {"api_key": 45882532, "api_secret": "ea48282fe2a0c7580294b8a65f41a15ca38904ac"},
            'mqtt_conf' : {"host": "doctordoctor.help", "port": 1883},
            'numer_of_doctor': 100 #line 107
        }

def prepare_conn():
    try:
        conn_obj = connection_pool(conf['ERP_DB_HOST'],conf['ERP_DB_NAME'],conf['ERP_DB_USER'],conf['ERP_DB_PASS'],conf['ERP_DB_PORT'])

        pool_conn = conn_obj.get_conn()#.connect()
        return pool_conn
    except Exception as e:
        print e
        
pool = prepare_conn()
user_account_business = user_account_business('',pool, conf)
gs = general_business(pool, conf)
profile_patient_business = profile_patient_business('',pool, conf)
profile_doctor_business = profile_doctor_business('',pool, conf)
###############################        USER          ########################################################
data_profile = {
                    'user_token': '',
                    "titleCode": "dr",
                    "firstName": "Minh",
                    "middleName": "Nhat",
                    "lastName": "On",
                    "genderCode": "male",
                    "spokenLanguageCode": ["vi"],
                    "primaryPracticalAddress": {
                                                "number": "152",
                                                "street": "Vo Thi Sau",
                                                "cityCode": "hcm",
                                                "state": "whatever",
                                                "postcode": "whatever",
                                                "countryCode": "vn",
                                                "phone": "+8412345678"
                                                },
                    
                    "consultationTypeCode": "by_appointment",
                    "consultationDurationCode": "10m",
                    "consultationFee": {
                                            "value": 100000,
                                            "unitCode": "vnd"
                                        },
                    "specialtyCodes": [{
                                        "code": "general_practitioner",
                                        "yearsOfExp": 7
                                        }],
                    "yearsOfExperience": 7,
                    "license": {
                                    "number": "whatever",
                                    "licenseStatus": "whatever",
                                    "issueDate": 4123123,
                                    "expiryDate": 4123123,
                                    "issueBy": "whatever"
                                },
                    "dayOfBirth": 4123123,
                    "medicalSchool": "whatever",
                    "graduationDate": 4123123,
                    "postGraduateTraining": "whatever",
                    "industryRelationship": "whatever",
                    "publications": "whatever",
                    "teaching": "whatever",
                    "communityService": "whatever",
                    "professionalAffiliationsAndActivities": "whatever",
                    "criminalConvictions": "whatever",
                    "limitations": "whatever",
                    "hospitalRestrictions": "whatever",
                    "aboutMe": "whatever"
                }




list_account_ids = []
consultationTypeCode = ['by_appointment','live']
titleCode = ['dr','ms','mr']
genderCode = ['male','female','other']
consultationDurationCode = ['10m','15m',"30m",'60m','45m','90m','20m']
specialtyCodes = ['mental_health','general_practitioner','medical_specialist','paediatrics'] 
list_name = ['Jonh','David','Tom','Loki','Thor','Batman','Superman','Spider','Hulk','Toriko','Naruto','Luffy','Conan',
             'Inuyasha','Inoun','Adachi','Toyota','Yamaha','Honda','Job','Bob',
             'Bin','Bao','Minh','Kim','Linh','Nguyen','Tran','Le','Khanh','Trinh']

for index in range(conf['numer_of_doctor']):
    mail = "doctor_%s@gmail.com"%index
    phone = "doctor_%s"%index
    user_data = {
                    "email": mail,
                    "phoneNumber": phone,
                    "password": "123456",
                    "userRoleCode": "doctor"
                }
    res = user_account_business.create_user_account(user_data)
    if res['status_code'] == 200:
        list_account_ids.append(int(res['resq_body']['userId']))
        sql = '''update user_account set validation_status = 'active' where id = %s'''%res['resq_body']['userId']
        gs.sql_execute(pool, sql, commit=True)
        login = user_account_business.authentication({"login":mail,"password": "123456"})
        if login['status_code'] == 200:
            import random
            ddToken = login['resq_body']['ddToken']
            data_profile.update({
                                    'user_token': ddToken,
                                    "titleCode": random.choice(titleCode),
                                    "firstName": random.choice(list_name),
                                    "middleName": random.choice(list_name),
                                    "lastName": random.choice(list_name),
                                    "genderCode": random.choice(genderCode),
                                    "consultationTypeCode": random.choice(consultationTypeCode),
                                    "consultationDurationCode": random.choice(consultationDurationCode),
                                })
            data_profile['specialtyCodes'][0]['code'] = random.choice(specialtyCodes)
            for type in ['basic','consultation','other']:
                data_profile.update({'setting_type': type})
                res = profile_doctor_business.profile_doctor_setup(data_profile)
                print res
    else:
        print 'Existed'