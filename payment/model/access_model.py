#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from datetime import datetime
try:
    from general import general_business
except ImportError as e:
    print('No Import ',e)

logger = logging.getLogger('drdr_api')


class access_model(object):
    def __init__(self, erp_connection, var_contructor):
        self.ERP_DB_HOST = var_contructor['ERP_DB_HOST']
        self.ERP_DB_PORT = var_contructor['ERP_DB_PORT']
        self.ERP_DB_USER = var_contructor['ERP_DB_USER']
        self.ERP_DB_PASS = var_contructor['ERP_DB_PASS']
        self.ERP_DB_NAME = var_contructor['ERP_DB_NAME']

        self.pool_conn = erp_connection
        self.general_business = general_business(erp_connection, var_contructor)


    def search(self,table, data, att=None, limit=False, free_arg=False):
        try:
            gs = self.general_business
            pool= self.pool_conn
            res=''
            if data:
                if free_arg:
                    where_con = "WHERE " + data
                else:
                    if len(data)==1:
                        where_con="where %s %s '%s'"%(data[0][0], data[0][1],data[0][2])
                    if len(data)> 1:
                        where_con="WHERE " + " AND ".join("%s %s '%s'" % (p[0],p[1],p[2]) for p in data)
                if att and isinstance(att, list):
                    attribute=','.join(p for p in att)
                else:
                    attribute='id'
                sql = ("""select %s from %s %s""" % (attribute, table, where_con))
                if limit:
                    sql = sql + ' limit 1'
                res = gs.sql_execute(pool, sql)
                if res:
                   return res
                else:
                    return False
            else:
                return False
        except Exception as e:
            logger.error('Search Error: %s' % str(e))
            client.captureException()

    def update(self,table,data,tran_id):
        try:
            gs = self.general_business
            pool = self.pool_conn
            data.update({
                'write_date': datetime.now()
            })
            if len(data.keys())==1:
                set_value = "%s %s '%s'"%(data.keys()[0], '=', data.values()[0])
            if len(data.keys())>1:
                set_value=",".join("%s %s '%s'" % (p[0],'=',p[1]) for p in data.items())
            sql = """update %s
                               set %s where id in (%s) RETURNING id""" % (table,set_value,tran_id)

            id = gs.sql_execute(pool, sql,para_values=False, commit=True)
            if len(id)>0:
                return id[0]
            else:
                return False
        except Exception as e:
            logger.error('Update Error: %s' % str(e))
            client.captureException()

    def unlink(self, table, tran_id):
        try:
            gs = self.general_business
            pool = self.pool_conn
            sql_delete = '''delete from %s
                                where id in ('%s') ''' % (table,tran_id)
            gs.sql_execute(pool, sql_delete, commit=True)
        except Exception as e:
            logger.error('Delete Error: %s' % str(e))
            client.captureException()

    def create(self, table, data):
        try:
            gs = self.general_business
            pool = self.pool_conn
            #current_date
            data.update({
                'created_date': datetime.now(),
                'write_date': datetime.now()
            })
            query = "insert into %s(%s) values (%s) RETURNING id" % (table,",".join(['%s'
                                                                        % k for k in data]),
                                                              ", ".join(['%s'] * len(data.keys())))
            id = gs.sql_execute(pool, query,para_values=data.values(), commit=True)
            if len(id) > 0:
                return id[0]
            else:
                return False
        except Exception as e:
            logger.error('Insert Error: %s' % str(e))
            client.captureException()
            return False

