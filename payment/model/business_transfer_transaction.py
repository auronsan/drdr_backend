#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import sys

from raven import Client
import ConfigParser
import os
path =  str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
project_path = str(os.path.abspath(os.path.join(path, os.pardir)))
config = ConfigParser.RawConfigParser(allow_no_value=True)
config.read(project_path + '/' + "environment.conf")
sentry_key = config.get('environment', 'sentry_key')
client = Client(sentry_key)

logging.basicConfig()
sys.path.insert(0, "..")
from datetime import datetime
try:
    from access_model import access_model
    from general import general_business
except ImportError as e:
    print('No Import ',e)

logger = logging.getLogger('drdr_api')
# com_ratio=0.4
# max_commission=40000 #VND
# drdr_wallet=1

class business_transfer_transaction(object):
    def __init__(self, erp_connection, var_contructor):
        self.ERP_DB_HOST = var_contructor['ERP_DB_HOST']
        self.ERP_DB_PORT = var_contructor['ERP_DB_PORT']
        self.ERP_DB_USER = var_contructor['ERP_DB_USER']
        self.ERP_DB_PASS = var_contructor['ERP_DB_PASS']
        self.ERP_DB_NAME = var_contructor['ERP_DB_NAME']

        self.pool_conn = erp_connection
        self.general_business = general_business(erp_connection, var_contructor)
        self.access_model = access_model(erp_connection, var_contructor)
        self.drdr_wallet = int(config.get('environment', 'drdr_wallet_id')) or 1
        self.com_ratio = float(config.get('environment', 'com_ratio')) or 40.0
        self.min_commission = float(config.get('environment', 'min_commission')) or 40000.0



    def action_create_in(self, wallet_source_id, wallet_dest_id, amount, payment_trans_id, state=None, currency=None):
        try:
            data = {}
            if wallet_source_id and wallet_source_id:
                access_model = self.access_model
                data.update({
                    'wallet_source_id': wallet_source_id,
                    'wallet_dest_id': wallet_dest_id,
                    'type': 'IN',
                    'state': state or 'paid',
                    'amount': amount,
                    'payment_transaction_id': payment_trans_id
                })
                if currency:
                    data.update({
                        'currency':currency
                    })
                create_trans = access_model.create('business_transfer_transaction', data)
                #TODO: currency
                if create_trans:
                    create_log = self.create_log(create_trans, data.get('state'), data.get('amount'))
                    if not create_log:
                        logger.error('action_create_in error: Cannot create trans')
                        return False
                if create_trans:
                    return create_trans
            else:
                logger.error('action_create_in error: Wallet_ID should not be NULL')
                return False
            return False
        except Exception as e:
            logger.error('action_create_in error: %s' % str(e))
            client.captureException()

    def action_create_internal(self, wallet_source_id, wallet_dest_id, amount, currency=None):
        try:
            data = {}
            if wallet_source_id and wallet_source_id:
                access_model = self.access_model
                data.update({
                    'wallet_source_id': wallet_source_id,
                    'wallet_dest_id': wallet_dest_id,
                    'type': 'INTER',
                    'state': 'confirmed',
                    'amount': amount,
                })
                if currency:
                    data.update({
                        'currency':currency
                    })
                create_trans = access_model.create('business_transfer_transaction', data)
                # TODO: currency
                if create_trans:
                    create_log = self.create_log(create_trans, data.get('state'), data.get('amount'))
                    if not create_log:
                        logger.error('action_create_internal error: Cannot create trans')
                        return False
                if create_trans:
                    return create_trans
            else:
                logger.error('action_create_internal error: Wallet_ID should not be NULL')
                return False
            return False
        except Exception as e:
            logger.error('action_create_internal error: %s' % str(e))
            client.captureException()

    def create_payment_appointment(self, apm_id, amount, wallet_source_id, wallet_dest_id,  note=None, context=None):
        try:
            data_dr = {}
            data_comm={}
            access_model=self.access_model
            if amount > 0 and apm_id:
                amount_commission = max(self.min_commission, amount * (self.com_ratio / 100))
                if self.patient_wallet_validate(wallet_source_id, amount):
                    currency_unit=access_model.search('appointment',
                                                [('id', '=', apm_id)],
                                                ['unit_cost'], limit=True)
                    create_trans=self.action_create_internal(wallet_source_id, wallet_dest_id,amount, currency=currency_unit[0][0])
                    data_dr.update({
                        'business_type':'doctor_apm_fee',
                        'fee_type': 'fee',
                        'note': note or '',
                        'appointment_id': apm_id,
                        'amount_commission': amount_commission,
                    })
                    if create_trans:
                        doctor_tran_id = access_model.update('business_transfer_transaction', data_dr,
                                                             create_trans[0])
                        if doctor_tran_id:
                            data_comm.update({
                                'business_type': 'comm_apm_fee',
                                'fee_type': 'fee',
                                'note': note or '',
                                'appointment_id': apm_id,
                            })
                            create_commission_trans = self.action_create_internal(wallet_source_id,
                                                                                  wallet_dest_id,
                                                                                  amount_commission, currency=currency_unit[0][0])
                            if create_commission_trans:
                                comm_tran_id = access_model.update('business_transfer_transaction', data_comm,
                                                                   create_commission_trans[0])
                                return doctor_tran_id[0]

                else:
                    logger.error('patient_create_payment error: Available balance is lower than 0: %s'%wallet_source_id)
                    return False
            logger.error('patient_create_payment error: Cannot create payment')
            return False
        except Exception as e:
            logger.error('patient_create_payment error: %s' % str(e))
            client.captureException()

    def get_wallet_from_to(self, apm_id):
        try:
            list = []
            access_model = self.access_model
            trans_ids = access_model.search('business_transfer_transaction',
                                            [('appointment_id', '=', apm_id), ('fee_type', '=', 'fee')],
                                            ['wallet_source_id', 'wallet_dest_id'],limit=True)
            if trans_ids and len(trans_ids) > 0:
                list=[int(trans_ids[0][0]), int(trans_ids[0][1])]
                return list
            logger.error(
                'refund_appointment error: Cannot done this transaction: %s' % apm_id)
            return False
        except Exception as e:
            logger.error('get_wallet_from_to error: %s' % str(e))
            client.captureException()

    def create_reschedule_fee(self,apm_id, amount, wallet_source_id=None, wallet_dest_id=None, note=None, context=None):
        try:
            data = {}
            access_model=self.access_model
            if amount > 0 and apm_id:
                if not wallet_source_id or not wallet_dest_id:
                    list_wallet=self.get_wallet_from_to(apm_id)
                    if list_wallet and len(list_wallet)>0:
                        wallet_source_id=list_wallet[0]
                        wallet_dest_id=list_wallet[1]
                    else:
                        logger.error(
                            'create_reschedule_fee error: Cannot get wallet from this appointment: %s' % apm_id)
                        return False
                if self.patient_wallet_validate(wallet_source_id, amount):
                    currency_unit = access_model.search('appointment',
                                                        [('id', '=', apm_id)],
                                                        ['unit_cost'], limit=True)
                    create_trans=self.action_create_internal(wallet_source_id, wallet_dest_id,amount,currency=currency_unit[0][0])
                    data.update({
                        'business_type':'reschedule_apm_fee',
                        'fee_type': 'fee',
                        'note': note or '',
                        'appointment_id': apm_id,
                    })
                    if create_trans:
                        reschedule_tran_ids = access_model.update('business_transfer_transaction', data,
                                                             create_trans[0])
                        if reschedule_tran_ids and len(reschedule_tran_ids)>0:
                            if not self.action_paid(reschedule_tran_ids[0]):
                                logger.error(
                                    'done_appointment error: Cannot done this transaction: %s' % reschedule_tran_ids[0])
                                return False
                        return reschedule_tran_ids[0]
                else:
                    logger.error('create_payment_reschedule error: Available balance is lower than 0')
                    return False
            logger.error('create_payment_reschedule error: Cannot create payment')
            return False
        except Exception as e:
            logger.error('create_payment_reschedule error: %s' % str(e))
            client.captureException()

    def create_appointment_discount(self, apm_id, amount, wallet_source_id=None, wallet_dest_id=None, note=None, context=None):
        try:
            data = {}
            access_model=self.access_model
            if apm_id:
                if not wallet_source_id or not wallet_dest_id:
                    list_wallet=self.get_wallet_from_to(apm_id)
                    if list_wallet and len(list_wallet)>0:
                        wallet_dest_id=list_wallet[0] # revert dest and source due to refund business
                        wallet_source_id=list_wallet[1]
                    else:
                        logger.error(
                            'create_appointment_discount error: Cannot get wallet from this appointment: %s' % apm_id)
                        return False
                if amount >0:
                    currency_unit = access_model.search('appointment',
                                                        [('id', '=', apm_id)],
                                                        ['unit_cost'], limit=True)
                    create_trans=self.action_create_internal(wallet_source_id, wallet_dest_id,amount, currency=currency_unit[0][0])
                    if create_trans:
                        data.update({
                            'business_type': 'apm_discount',
                            'fee_type': 'discount',
                            'note': note or '',
                            'appointment_id': apm_id
                        })
                        discount_tran_id = access_model.update('business_transfer_transaction', data,create_trans[0])
                        if discount_tran_id:
                            if not self.action_paid(discount_tran_id[0]):
                                logger.error(
                                    'create_appointment_discount error: Cannot done this transaction: %s' % discount_tran_id)
                                return False
                            return discount_tran_id[0]
            logger.error('create_appointment_discount error: Cannot create discount')
            return False
        except Exception as e:
            logger.error('create_appointment_discount error: %s' % str(e))
            client.captureException()

    # def refund_appointment_patient_partial(self, apm_id, percent, wallet_source_id=None, wallet_dest_id=None, margin=True,reschedule=True, note=None,context=None):
    #     try:
    #         data = {}
    #         access_model=self.access_model
    #         if apm_id and percent:
    #             if not wallet_source_id or not wallet_dest_id:
    #                 list_wallet=self.get_wallet_from_to(apm_id)
    #                 if list_wallet and len(list_wallet)>0:
    #                     wallet_dest_id=list_wallet[0] # revert dest and source due to refund business
    #                     wallet_source_id=list_wallet[1]
    #                 else:
    #                     logger.error(
    #                         'refund_appointment error: Cannot get wallet from this appointment: %s' % apm_id)
    #                     return False
    #             trans_objs=access_model.search('business_transfer_transaction',
    #                                              [('appointment_id', '=', apm_id), ('fee_type', '=', 'fee'),
    #                                               ('state', '=', 'paid')],
    #                                              ['business_type','amount'])
    #             if trans_objs and len(trans_objs) >0:
    #                 amount_refund=0
    #                 for trans_obj in trans_objs:
    #                     if trans_obj[0] in ('doctor_apm_fee'):
    #                         amount_refund += trans_obj[1] * percent
    #                     if reschedule and trans_obj[0] in ('reschedule_apm_fee'):
    #                         amount_refund += trans_obj[1] * percent
    #                     if margin and trans_obj[0] in ('comm_apm_fee'):
    #                         amount_refund+=trans_obj[1] * percent
    #                 if self.check_refund(apm_id, amount_refund):
    #                     create_trans=self.action_create_internal(wallet_source_id, wallet_dest_id,amount_refund)
    #                     print create_trans
    #                 else:
    #                     return False
    #             if create_trans:
    #                 data.update({
    #                     'business_type': 'apm_partial_refund',
    #                     'fee_type': 'refund',
    #                     'note': note or '',
    #                     'appointment_id': apm_id
    #                 })
    #                 refund_tran_ids = access_model.update('business_transfer_transaction', data,
    #                                                      create_trans[0])
    #                 if refund_tran_ids and len(refund_tran_ids)>0:
    #                     if not self.action_paid(refund_tran_ids[0]):
    #                         logger.error(
    #                             'refund_appointment error: Cannot done this transaction: %s' % refund_tran_ids[0])
    #                         return False
    #                 return refund_tran_ids[0]
    #         logger.error('refund_appointment error: Cannot create payment')
    #         return False
    #     except Exception as e:
    #         logger.error('refund_appointment error: %s' % str(e))

    def refund_appointment_patient(self, apm_id, amount_refund=None, percent=None, margin=True, reschedule=True, note=None,context=None):
        try:
            data = {}
            access_model=self.access_model
            if apm_id:
                list_wallet=self.get_wallet_from_to(apm_id)
                if list_wallet and len(list_wallet)>0:
                    wallet_dest_id=list_wallet[0] # revert dest and source due to refund business
                    wallet_source_id=list_wallet[1]
                else:
                    logger.error(
                        'refund_appointment error: Cannot get wallet from this appointment: %s' % apm_id)
                    return False
                trans_objs = access_model.search('business_transfer_transaction',
                                                 [('appointment_id', '=', apm_id), ('fee_type', '=', 'fee'),
                                                  ('state', '=', 'paid')],
                                                 ['business_type', 'amount'])
                if trans_objs and len(trans_objs) > 0:
                    if not amount_refund and percent:
                        percent = float(percent)
                        percent_ratio = percent / 100
                        amount_refund = 0
                        for trans_obj in trans_objs:
                            if trans_obj[0] in ('doctor_apm_fee'):
                                amount_refund += trans_obj[1] * percent_ratio
                            elif trans_obj[0] in ('reschedule_apm_fee') and reschedule:
                                amount_refund += trans_obj[1] * percent_ratio
                            elif trans_obj[0] in ('comm_apm_fee') and margin:
                                amount_refund += trans_obj[1] * percent_ratio

                        trans_dis_objs = access_model.search('business_transfer_transaction',
                                                             [('appointment_id', '=', apm_id),
                                                              ('fee_type', '=', 'discount'),
                                                              ('state', '=', 'paid')],
                                                             ['business_type', 'amount'])
                        if trans_dis_objs and len(trans_dis_objs) > 0:
                            for trans_dis_obj in trans_dis_objs:
                                if trans_dis_obj[0] in ('apm_discount'):
                                    amount_refund -= trans_dis_obj[1] * percent_ratio

                    if self.check_refund(apm_id, amount_refund):
                        currency_unit = access_model.search('appointment',
                                                            [('id', '=', apm_id)],
                                                            ['unit_cost'], limit=True)
                        create_trans = self.action_create_internal(wallet_source_id, wallet_dest_id,
                                                                   amount_refund, currency=currency_unit[0][0])
                    else:
                        return False
                if create_trans:
                    data.update({
                        'business_type': 'apm_full_refund',
                        'fee_type': 'refund',
                        'note': note or '',
                        'appointment_id': apm_id
                    })
                    refund_tran_ids = access_model.update('business_transfer_transaction', data,
                                                         create_trans[0])
                    if refund_tran_ids and len(refund_tran_ids)>0:
                        if not self.action_paid(refund_tran_ids[0]):
                            logger.error(
                                'refund_appointment error: Cannot done this transaction: %s' % refund_tran_ids[0])
                            return False
                    return refund_tran_ids[0]
            logger.error('refund_appointment error: Cannot create payment')
            return False
        except Exception as e:
            logger.error('refund_appointment error: %s' % str(e))
            client.captureException()

    def cancel_appointment(self, apm_id, note=None):
        try:
            data = {}
            access_model = self.access_model
            cancel_objs = access_model.search('business_transfer_transaction',
                                              [('appointment_id', '=', apm_id),('state', '=', 'confirmed')],
                                              ['id'])
            for cancel_obj in cancel_objs:
                if not self.action_cancel(cancel_obj[0]):
                    logger.error('Cannot cancel this transaction_id: %s' % cancel_obj[0])
                    return False
            return True
        except Exception as e:
            logger.error('cancel_appointment error: %s' % str(e))
            client.captureException()

    def check_refund(self, apm_id, refund_amount):
        try:
            data = {}
            access_model = self.access_model
            refund_objs = access_model.search('business_transfer_transaction',
                                             [('appointment_id', '=', apm_id), ('fee_type', '=', 'refund'),
                                              ('state', '=', 'paid')], ['amount'])
            fee_objs = access_model.search('business_transfer_transaction',
                                          '''
                                          appointment_id = %s and fee_type = '%s' and state='%s' 
                                          and business_type in ('%s', '%s', '%s')
                                          '''% (apm_id, 'fee','paid', 'doctor_apm_fee',
                                                'comm_apm_fee','reschedule_apm_fee'),
                                          ['amount'], free_arg=True)
            if fee_objs and len(fee_objs) > 0:
                total_fee = sum(list(map(lambda x: x[0], fee_objs)))
                total_refund=0
                if refund_objs and len(refund_objs) >0:
                    total_refund = sum(list(map(lambda x: x[0], refund_objs)))
                total_refund=total_refund + refund_amount
                if total_refund > total_fee:
                    logger.error('check_refund error: The refund cannot bigger Total Amount: %s > %s' % (total_refund, total_fee))
                    return False
            return True
        except Exception as e:
            logger.error('check_refund error: %s' % str(e))
            client.captureException()

    def get_doctor(self, apm_id):
        try:
            access_model = self.access_model
            doctor_ids = access_model.search('appointment',
                                             [('id', '=', apm_id)],
                                             ['doctor_id'])
            if doctor_ids and len(doctor_ids) > 0:
                return doctor_ids[0][0]
            logger.error(
                'get_doctor error: Cannot get_doctor in this transaction: %s' % apm_id)
            return False
        except Exception as e:
            logger.error('get_doctor error: %s' % str(e))
            client.captureException()

    def get_wallet(self, user_id):
        try:
            access_model = self.access_model
            wallet_ids = access_model.search('user_wallet',
                                             [('user_account_id', '=', user_id)],
                                             ['id'])
            if wallet_ids and len(wallet_ids) > 0:
                return wallet_ids[0][0]
            logger.error(
                'get_wallet error: Cannot get_wallet in this transaction: %s' % user_id)
            return False
        except Exception as e:
            logger.error('get_wallet error: %s' % str(e))
            client.captureException()

    def done_appointment(self, apm_id, percent=None, amount = None):
        try:
            if apm_id:
                if self.confirm_appointment(apm_id):
                    return self.transfer_apm_fee_doctor(apm_id, percent=percent, total_amount=amount)[0]
            logger.error('done_appointment error: Cannot create done this apointment: %s'%apm_id)
            return False
        except Exception as e:
            logger.error('done_appointment error: %s' % str(e))
            client.captureException()

    def transfer_apm_fee_doctor(self, apm_id, percent=None,total_amount=None,note=None):
        try:
            data_dr={}
            access_model=self.access_model
            if apm_id and (percent or total_amount!=None):
                doctor_id=self.get_doctor(apm_id)
                if doctor_id:
                    doctor_wallet_id=self.get_wallet(doctor_id)
                    if doctor_wallet_id:
                        doctor_fee_amounts = access_model.search('business_transfer_transaction',
                                                                 [('appointment_id', '=', apm_id),
                                                                  ('fee_type', '=', 'fee'),
                                                                  ('business_type', '=', 'doctor_apm_fee')],
                                                                    ['amount'])
                        if doctor_fee_amounts and len(doctor_fee_amounts) >0:
                            if total_amount==None and percent:
                                percent = float(percent)
                                total_amount = sum([c[0] for c in doctor_fee_amounts]) * (percent/100)
                            currency_unit = access_model.search('appointment',
                                                                [('id', '=', apm_id)],
                                                                ['unit_cost'], limit=True)
                            create_trans = self.action_create_internal(self.drdr_wallet, doctor_wallet_id, total_amount, currency=currency_unit[0][0])
                            if create_trans and len(create_trans)>0:
                                data_dr.update({
                                    'note': note or '',
                                    'appointment_id': apm_id,
                                    'fee_type':'pay_dr',
                                })
                                self.action_paid(create_trans[0])
                                access_model.update('business_transfer_transaction', data_dr,create_trans[0])
                                return create_trans
            else:
                logger.error('transfer_apm_fee_doctor error: This appointment does not exist or doesnt have transaction: %s' % apm_id)
                return False
            logger.error('transfer_apm_fee_doctor error: Cannot create payment')
            return False
        except Exception as e:
            logger.error('transfer_apm_fee_doctor error: %s' % str(e))
            client.captureException()

    def confirm_appointment(self, apm_id):
        try:
            data = {}
            access_model=self.access_model
            if apm_id:
                trans_ids = access_model.search('business_transfer_transaction',
                                                 [('appointment_id', '=', apm_id),
                                                  ('state', '=', 'confirmed'),
                                                  ('fee_type', '!=', 'pay_dr')],
                                                 ['id'])
                if trans_ids and len (trans_ids) > 0:
                    for trans_id in trans_ids:
                        if not self.action_paid(trans_id[0]):
                            logger.error(
                                'confrim_appointment error: Cannot done this transaction: %s' % trans_id)
                            return False
                    return True
                else:
                    paid_trans_ids = access_model.search('business_transfer_transaction',
                                                    [('appointment_id', '=', apm_id),
                                                     ('state', '=', 'paid'),
                                                     ('business_type', '=', 'doctor_apm_fee')],
                                                    ['id'], limit=True)
                    if paid_trans_ids and len(paid_trans_ids)>0:
                        return True
                    logger.error('confirm_appointment error: This appointment does not exist or doesnt have transaction: %s' % apm_id)
                    return False
            logger.error('confirm_appointment error: Cannot create payment')
            return False
        except Exception as e:
            logger.error('confirm_appointment error: %s' % str(e))
            client.captureException()

    def patient_wallet_validate(self, wallet_source_id, amount, data=None):
        try:
            access_model=self.access_model
            if wallet_source_id and amount:
                wallet_ids=access_model.search('user_wallet',
                                                      [('id', '=',wallet_source_id)],
                                                      ['id','available_balance'])
                if wallet_ids and len(wallet_ids)>0 and wallet_ids[0][1] - amount >0:
                    return True
            if wallet_ids and len(wallet_ids) ==0:
                logger.error('patient_wallet_validate error: Does not exist this wallet')
                return False
            return False
        except Exception as e:
            logger.error('patient_wallet_validate error: %s' % str(e))
            client.captureException()

    def action_cancel(self, trans_id):
        try:
            access_model = self.access_model
            state = access_model.search('business_transfer_transaction', [('id', '=', trans_id)], ['state'])
            if state and state[0] and state[0] in ('paid', 'cancel'):
                logger.error('patient_create_payment error: Transaction is Paid or Cancelled.')
                return False
            ids = access_model.update('business_transfer_transaction', {'state': 'cancel'}, trans_id)
            if ids:
                amounts = access_model.search('business_transfer_transaction', [('id', '=', trans_id)], ['amount'])
                create_log = self.create_log(trans_id, 'paid', amounts[0] or 0)
                if not create_log:
                    logger.error('patient_create_payment error: Cannot create log')
                    return False
            return True
        except Exception as e:
            logger.error('patient_wallet_validate error: %s' % str(e))
            client.captureException()

    def action_paid(self, trans_id):
        try:
            access_model=self.access_model
            state=access_model.search('business_transfer_transaction', [('id', '=',trans_id)],['state'])
            if state and state[0] and state[0][0] in ('paid', 'cancel'):
                logger.error('patient_create_payment error: Transaction is Paid or Cancelled.')
                return False
            ids=access_model.update('business_transfer_transaction', {'state':'paid', 'trans_date':datetime.now()}, trans_id)
            if ids:
                amounts=access_model.search('business_transfer_transaction', [('id', '=',trans_id)],['amount'])
                create_log = self.create_log(trans_id, 'paid', amounts[0] or 0)
                if not create_log:
                    logger.error('patient_create_payment error: Cannot create log')
                    return False
                return True
        except Exception as e:
            logger.error('patient_wallet_validate error: %s' % str(e))
            client.captureException()

    # def action_cancel(self, trans_id):


    def create_log(self, trans_id, state, amount, data=None):
        try:
            access_model = self.access_model
            if trans_id and state:
                data_log={
                    'business_transfer_transaction_id': trans_id,
                    'state': state,
                    'amount': amount or 0
                }
                create_log=access_model.create('trans_log_state', data_log)
                if create_log:
                    return True
                else:
                    logger.error('create_log error: Cannot create log')
                    return  False
            else:
                logger.error('create_log error: Miss data to create log')
                return False
        except Exception as e:
            logger.error('create_log error: %s' % str(e))
            client.captureException()
            return False

    def action_create_out(self, wallet_source_id, wallet_dest_id, amount, withdraw_trans_id, state=None, currency=None):
        try:
            data = {}
            if wallet_source_id and wallet_source_id:
                access_model = self.access_model
                data.update({
                    'wallet_source_id': wallet_source_id,
                    'wallet_dest_id': wallet_dest_id,
                    'type': 'OUT',
                    'state': state or 'paid',
                    'amount': amount,
                    'withdraw_transaction_id': withdraw_trans_id
                })
                if currency:
                    data.update({
                        'currency':currency
                    })
                create_trans = access_model.create('business_transfer_transaction', data)
                if create_trans:
                    create_log = self.create_log(create_trans, data.get('state'), data.get('amount'))
                    if not create_log:
                        logger.error('action_create_out error: Cannot create trans')
                        return False
                if create_trans:
                    return create_trans
            else:
                logger.error('action_create_out error: Wallet_ID should not be NULL')
                return False
            return False
        except Exception as e:
            logger.error('action_create_in error: %s' % str(e))
            client.captureException()






