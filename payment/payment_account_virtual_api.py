#!/usr/bin/env python
# -*- coding: utf-8 -*-
import falcon
import json
import sys
import logging
import ConfigParser
from datetime import datetime
import random
from dateutil.relativedelta import relativedelta
import time
import os

project_path =  str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
config = ConfigParser.RawConfigParser(allow_no_value=True)
config.read(project_path + '/' + "environment.conf")
from raven import Client
sentry_key = config.get('environment', 'sentry_key')
client = Client(sentry_key)
sys.path.insert(0, "..")
logger = logging.getLogger('drdr_api')
try:
    from general import general_business
except ImportError as e:
    print('No Import ', e)

status_code = {
    200: falcon.HTTP_200,
    401: falcon.HTTP_401,
    502: falcon.HTTP_502,
    203: falcon.HTTP_203
}

categ_trans = {
    'doctor_apm_fee': 'appointment',#-
    'apm_discount': 'appointment',#none
    'reschedule_apm_fee': 'appointment',#-
    'comm_apm_fee': 'appointment',#-
    'apm_partial_refund': 'refund',#+
    'apm_full_refund': 'refund',#+
    'reschedule_apm_refund': 'refund',#+
    'deposit': 'deposit',#+
}
# action_trans = {
#     'fee_by_appointment': 'Book',
#     'fee_live': 'Live Call',
#     'apm_discount': 'Discount',
#     'reschedule_apm_fee': 'Reschedule',
#     'comm_apm_fee': 'Commission',
#     'apm_partial_refund': 'Refund',
#     'apm_full_refund': 'Refund',
#     'reschedule_apm_refund': 'Refund',
#     'deposit': 'Deposit',
# }
action_trans = {
    'fee_by_appointment': 'Appointment',
    'fee_live': 'Live Call',
    'apm_discount': 'Discount',
    'reschedule_apm_fee': 'Reschedule',
    'comm_apm_fee': 'Commission',
    'apm_partial_refund': 'Refund',
    'apm_full_refund': 'Refund',
    'reschedule_apm_refund': 'Refund',
    'deposit': 'Deposit',
}
payment_type = {
    'visa': 'credit_card',
    'mastercard': 'credit_card',
    'scratch_card': 'mobile_card',
    'qr_card': 'dd_card',
}
class account_virtual_api(object):
    def __init__(self, signal, erp_connection, var_contructor):
        self.pool_conn = erp_connection
        self.ERP_DB_HOST = var_contructor['ERP_DB_HOST']
        self.ERP_DB_PORT = var_contructor['ERP_DB_PORT']
        self.ERP_DB_USER = var_contructor['ERP_DB_USER']
        self.ERP_DB_PASS = var_contructor['ERP_DB_PASS']
        self.ERP_DB_NAME = var_contructor['ERP_DB_NAME']
        self.general_business = general_business(erp_connection, var_contructor)
        self.signal = signal

    # MP7.7 API for user to get transaction history
    def transactionhistory_get(self, data):
        logger.info('transactionhistory_get %s' % data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "transactionhistory_get_failed",
                        "title": _("Get transaction history failed"),
                        "msg": _("An error occurred. Please try to request again.")
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            month = data.get('month')
            userId = data.get('user_id')
            sql_wallet = '''select id from user_wallet where user_account_id = %s'''
            wallet_id = gs.sql_execute(pool, sql_wallet, para_values=[userId])[0][0]

            if not month:
                # sql_month = '''select to_char(max(created_date),'MM-YYYY') 
                #                 FROM business_transfer_transaction
                #                 where (wallet_source_id = %s or wallet_dest_id = %s)'''
                # month = gs.sql_execute(pool, sql_month, para_values=[wallet_id,wallet_id])[0][0]

                full_list_ids = self.generate_pagination_list(userId, 'user_account_virtual_payment',
                                                                      context={'api': 'MP7_7','wallet_id':wallet_id})
            else:
                sql = '''select list_id 
                                from user_account_pagination
                                where user_account_id = %s
                                    and table_pagination = '%s'
                                ''' % (userId, 'user_account_virtual_payment')
                full_list_ids = gs.sql_execute(pool, sql)
                if full_list_ids:
                    full_list_ids = eval(full_list_ids[0][0])
                else:
                    full_list_ids = self.generate_pagination_list(userId, 'user_account_virtual_payment',
                                                                      context={'api': 'MP7_7','wallet_id':wallet_id})
            if not full_list_ids:
                resp.update({'status_code': 200,
                                'resq_body': {
                                        "transactionHistoryList": [],
                                        "nextRequestMonth":None,
                                        "currentMonth": None,
                                    }
                                })
                return resp

            if not month:
                month = full_list_ids[0]
                if len(full_list_ids) >= 2:
                    nextRequestMonth = full_list_ids[1]
                else:
                    nextRequestMonth = None
            else:
                index = full_list_ids.index(month)
                if index + 1 >= len(full_list_ids):
                    nextRequestMonth = None
                else:
                    nextRequestMonth = full_list_ids[index + 1]

            # result = {"transactionHistoryList": [],
            #           "nextRequestMonth": month
            #                               and (datetime.strptime(month, "%m-%Y") - relativedelta(months=1)).strftime("%m-%Y")
            #                               or (datetime.now() - relativedelta(months=1)).strftime('%m-%Y')}
            
            result = {"transactionHistoryList": [],
                      "nextRequestMonth": nextRequestMonth,
                      "currentMonth": month,}
            if month:
                sql = '''SELECT bt.id,--0
                                bt.created_date,--1
                                bt.type,--2
                                bt.amount,--3
                                case 
                                    when bt.business_type is null and bt.type = 'IN' then 'deposit'
                                    else bt.business_type
                                end business_type, --4
                                to_char(bt.created_date,'MM-YYYY'),--5
                                apm.type,--6
                                bt.currency, --7
                                COALESCE(bt.amount_commission,0) --8
                        FROM business_transfer_transaction bt
                        left join appointment apm on apm.id = bt.appointment_id
                        where (wallet_source_id = %s or wallet_dest_id = %s)
                        and bt.state = 'paid'
                        and to_char(bt.created_date,'MM-YYYY') = %s
                        and (bt.business_type != 'comm_apm_fee' or bt.business_type is null)
                        order by bt.created_date desc'''
                res = gs.sql_execute(pool, sql, para_values=[wallet_id,wallet_id,month])
                if res:
                    for line in res:
                        amount = line[3]
                        if line[4] == 'doctor_apm_fee':
                            amount = amount + line[8]
                            if line[6] == 'live':
                                actionName = 'fee_live'
                            else:
                                actionName = 'fee_by_appointment'
                        elif not line[4] and line[2] == 'IN':
                            actionName = 'deposit'
                        else:
                            actionName = line[4]

                        if line[4] in ('doctor_apm_fee','reschedule_apm_fee','comm_apm_fee'):
                            sign = 'minus'
                        elif line[4] in ('apm_partial_refund','apm_full_refund','reschedule_apm_refund','deposit'):
                            sign = 'plus'
                        else:
                            sign = 'none'

                        trans = {
                            "id": line[0],
                            "actionTime": line[1] and int(time.mktime(line[1].timetuple())) or None,
                            "categoryCode": categ_trans[line[4]],
                            "actionName": _(action_trans[actionName]),
                            "amount": {
                                "sign": sign,
                                "value": amount,
                                "unitCode": line[7] or None
                            }
                        }
                        result['transactionHistoryList'].append(trans)
            resp.update({'status_code': 200,
                         'resq_body': result})

        except Exception as e:
            logger.error('error_transactionhistory_get %s' % str(e))
            client.captureException()

        return resp

    #MP7.8 API for user to get specific transaction detail
    def specific_transaction_detail(self, data):
        logger.info('specific_transaction_detail %s' % data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "get_specific_transaction_detail_failed",
                        "title": _("Get specific transaction detail failed"),
                        "msg": _("An error occurred. Please try to request again.")
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            id = data.get('id')
            userId = data.get('user_id')
            sql_wallet = '''select id from user_wallet where user_account_id = %s'''
            wallet_id = gs.sql_execute(pool, sql_wallet, para_values=[userId])[0][0]
            sql = '''SELECT bt.id,--0
                            bt.created_date,--1
                            bt.type,--2
                            bt.amount,--3
                            bt.currency, --4
                            case 
                                when bt.business_type is null and bt.type = 'IN' then 'deposit'
                                else bt.business_type
                            end business_type, --5
                            case
                                when wallet_source_id = %s then balance_source_before
                                else balance_dest_before
                            end balance_before, --6
                            case
                                when wallet_source_id = %s then balance_source_after
                                else balance_dest_after
                            end balance_after, --7
                            um.first_name,--8
                            um.middle_name,--9
                            um.last_name,--10
                            apm.type,--11
                            apm.id apm_id,--12
                            pt.id payment_id,--13
                            pt.type,--14
                            COALESCE(bt.amount_commission,0) --15
                    FROM business_transfer_transaction bt
                    left join appointment apm on apm.id = bt.appointment_id
                    left join user_account uc on uc.id = apm.doctor_id
                    left join user_mngment um on um.id = uc.user_mngment_id
                    left join payment_transaction pt on pt.id = bt.payment_transaction_id
                    where bt.id = %s'''
            line = gs.sql_execute(pool, sql, para_values=[wallet_id, wallet_id,id])[0]
            amount = line[3]
            before = line[6]
            after = line[7]
            if line[5] == 'doctor_apm_fee':
                amount = amount + line[15]
                before = before
                after = after - line[15]
                if line[11] == 'live':
                    actionName = 'fee_live'
                else:
                    actionName = 'fee_by_appointment'
            elif not line[5] and line[2] == 'IN':
                actionName = 'deposit'
            else:
                actionName = line[5]

            if line[5] in ('doctor_apm_fee', 'reschedule_apm_fee', 'comm_apm_fee'):
                sign = 'minus'
            elif line[5] in ('apm_partial_refund', 'apm_full_refund', 'reschedule_apm_refund', 'deposit'):
                sign = 'plus'
            else:
                sign = 'none'
                
            result = {
                "categoryCode": categ_trans[line[5]],
                "actionTime": line[1] and int(time.mktime(line[1].timetuple())) or None,
                "actionName": action_trans[actionName],
                "amount": {
                    "sign": sign,
                    "value": amount,
                    "unitCode": line[4] or None
                },
                "beforeTransactionBalance": {
                    "value": before,
                    "unitCode": line[4] or None
                },
                "finalBalance": {
                    "value": after,
                    "unitCode": line[4] or None
                },

            }
            if line[12]:
                result.update({"appointment": {
                    "id": line[12],
                    "doctor": {
                        "firstName": None if line[8] in ('False', 'None', '') else line[8],
                        "middleName": None if line[9] in ('False', 'None', '') else line[9],
                        "lastName": None if line[10] in ('False', 'None', '') else line[10]
                    }
                }})
            if line[2] == 'IN':
                result.update({"deposit": {
                    "paymentOptionCode": payment_type.get(line[14].lower(),'credit_card')
                }})

            resp.update({'status_code': 200,
                         'resq_body': result})

        except Exception as e:
            logger.error('error_specific_transaction_detail %s' % str(e))
            client.captureException()

        return resp
    
    

    def generate_pagination_list(self, user_id, table_pagination, context={}):
        gs = self.general_business
        pool = self.pool_conn
        full_list_ids = []

        def cache_list(full_list_ids, table_pagination, rand=False):
            if rand == True:
                random.shuffle(full_list_ids)
            if full_list_ids:
                sql_search = '''select id 
                                            from user_account_pagination
                                            where user_account_id = %s
                                                and table_pagination = %s '''
                exist = gs.sql_execute(pool, sql_search, para_values=(user_id,
                                                                      table_pagination))
                if exist:
                    sql_update = '''update user_account_pagination set list_id = %s
                                                where user_account_id = %s
                                                    and table_pagination = %s '''
                    gs.sql_execute(pool, sql_update, commit=True, para_values=(str(full_list_ids),
                                                                               user_id,
                                                                               table_pagination))

                else:
                    sql_insert = '''insert into user_account_pagination(user_account_id, list_id, table_pagination)
                                                values (%s,%s,%s)'''
                    gs.sql_execute(pool, sql_insert, commit=True, para_values=(user_id,
                                                                               str(full_list_ids),
                                                                               table_pagination))
            return full_list_ids

        if 'api' in context and context['api'] == 'MP7_7':
            wallet_id = context['wallet_id']

            sql_list = '''select distinct to_char(created_date,'MM-YYYY'),
                                to_char(created_date,'MM')::integer, 
                                to_char(created_date,'YYYY')::integer
                            FROM business_transfer_transaction
                            where (wallet_source_id = %s or wallet_dest_id = %s) 
                                and created_date is not null
                            order by 3 desc, 2 desc'''
            res_list = gs.sql_execute(pool, sql_list, para_values=[wallet_id,wallet_id])

            for rc in res_list:
                full_list_ids.append(rc[0])

            full_list_ids = cache_list(full_list_ids, table_pagination)

        return full_list_ids

    def on_get(self, req, resp, apm_id = None, id = None, hospital_id = None):
        try:
            data = {}
            header = req.headers  # key cua header luon viet hoa
            all_header = self.general_business.get_header_request(header)
            lang = all_header.get('lang')
            _ = self.general_business.get_lang_instance(lang)
            data.update(all_header)

            ## authen token
            user_token = header.get("X-USER-TOKEN", False)
            user_id = False
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                else:
                    resp_status = status_code[203]
                    resp_body = {
                        "code": "expired_token",
                        "title": _("Expired token"),
                        "msg": _("Your login-token is expired, please try to login again")
                    }
                    resp.status = resp_status
                    resp.body = json.dumps(resp_body)
                    return


            if self.signal == 'account_virtual_balance_get' and user_id:
                balance = 0
                unitcode = ''
                sql_select = '''select available_balance, unitcode 
                                  from user_wallet where user_account_id = %s''' % (user_id)
                res = self.general_business.sql_execute(self.pool_conn, sql_select)
                if res:
                    balance = res[0][0]
                    unitcode = res[0][1]
                resp_status = status_code[200]
                resp_body = {"balance": {
                    "value": balance,
                    "unitcode": unitcode
                }}
                resp.status = resp_status
                resp.body = json.dumps(resp_body)
                return
            
            elif self.signal == 'get_dd_pin_role':
                resp_status = status_code[401]
                resp_body = {
                                "code": "get_dd_pin_role",
                                "title": _("Get specific get_dd_pin_role failed"),
                                "msg": _("An error occurred. Please try to request again.")
                         }
                
                dd_pin = data.get('dd_pin')
                sql_select = '''select dd_pin,email,ur.code from dd_card_user_pin ddc INNER JOIN user_account ua ON ddc.user_id = ua.id INNER JOIN user_account_user_role uaur on uaur.user_account_id = ua.id INNER JOIN user_role ur on ur.id = uaur.user_role_id where dd_pin = %s''' % (dd_pin)
                res = self.general_business.sql_execute(self.pool_conn, sql_select)
                if res:
                    dd_pin = res[0][0]
                    email = res[0][1]
                    code = res[0][2]
                    resp_status = status_code[200]
                    resp_body = {
                        "dd_pin" : dd_pin,
                        "email": email,
                        "role": code
                        }
                else:
                    resp_status = status_code[204]
                    resp_body = {
                        "code": "pin_not_exist",
                        "title": _("invalid pin code"),
                        "msg": _("invalid pin code")
                        }
                resp.status = resp_status
                resp.body = json.dumps(resp_body)
                
            elif self.signal == 'check_qr_need_pin':
                
                resp_status = status_code[401]
                resp_body = {
                                "code": "get_dd_pin_role",
                                "title": _("Get specific get_dd_pin_role failed"),
                                "msg": _("An error occurred. Please try to request again.")
                         }
                
                qr_code = data.get('qrCode')
                if qr_code :
                    content = qr_code[32:64]
                    card_code = str(int(content[0:8], 16))
                    card_mid_code = str(int(content[20:32], 16))
                    sql_select = '''select ddc.id,type_card from dd_card ddc INNER JOIN dd_card_user_pin ddup ON ddc.gen_by = ddup.id  where code = %s  and mid_code = %s''' % (card_code,card_mid_code)
                    res = self.general_business.sql_execute(self.pool_conn, sql_select)
                    if res:
                        resp_status = status_code[200]
                        resp_body = {
                            "type_card" : 'post-paid',
                            "message" : 'no need to input pin'
                            }
                        if res[0][1] == 'pre-paid':
                            resp_body = {
                                "type_card" : 'pre-paid',
                                "message" : 'need input the pin for activation'
                                }
                else:
                    resp_status = status_code[204]
                    resp_body = {
                        "code": "pin_not_exist",
                        "title": _("invalid pin code"),
                        "msg": _("invalid pin code")
                        }
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

                        
            
            elif self.signal == 'specific_transaction_detail' and user_id:
                data = {'user_id': user_id,
                        'id': id}
                resp_status = status_code[401]
                resp_body = {
                                "code": "get_specific_transaction_detail_failed",
                                "title": _("Get specific transaction detail failed"),
                                "msg": _("An error occurred. Please try to request again.")
                         }
                record = self.specific_transaction_detail(data)
                if record:
                    resp_status = status_code[record['status_code']]
                    resp_body = record['resq_body']

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
                return

            elif self.signal == 'admin_virtualaccount_get':
                profile_id = req.get_param('profile_id', default=0)
                email = req.get_param('email', default='')
                phone = req.get_param('phone', default='')
                where = ''
                if profile_id == 0 and email == '' and phone == '':
                    admin_id = int(config.get('environment', 'drdr_wallet_id')) or 1
                    where = 'or uw.id = %s' % admin_id
                sql_select = '''select coalesce(ua.id,0) user_account_id,
                                        coalesce(ua.user_mngment_id,0) profile_id,
                                        uw.id user_wallet_id,
                                        uw.total_balance,
                                        uw.available_balance
                                from user_wallet uw
                                left join user_account ua on uw.user_account_id = ua.id
                                where user_mngment_id = %s 
                                    or email = '%s' 
                                    or phone = '%s' 
                                    %s''' % (profile_id,
                                             email,
                                             phone,
                                             where)
                res = self.general_business.sql_execute(self.pool_conn, sql_select)
                if res:
                    res = res[0]
                    resp_status = status_code[200]
                    resp_body = {'user_account_id': res[0],
                                 'profile_id': res[1],
                                 'user_wallet_id': res[2],
                                 'total_balance': res[3],
                                 'available_balance': res[4],
                                 }
                else:
                    resp_status = status_code[401]
                    resp_body = 'Not found'

            elif self.signal == 'payment_appointment_get':
                resp_status = status_code[401]
                resp_body = {
                    "code": "payment_appointment_get_failed",
                    "title": _("Get appointment payment failed"),
                    "msg": _("An error occurred. Please try to request again."),
                }
                if not apm_id:
                    resp.status = resp_status
                    resp.body = json.dumps(resp_body)
                    return
                try:
                    sql = '''select um.id, --0
                                coalesce(profile_image_url,''), --1
                                first_name,--2
                                middle_name,--3
                                last_name,--4
                                number,--5
                                street,--6
                                city,--7
                                uad.state,--8
                                postcode,--9
                                country,--10
                                (select count(*) from appointment where doctor_id = apm.doctor_id and patient_id = apm.patient_id),--11
                                (select max(write_date) from appointment where doctor_id = apm.doctor_id and patient_id = apm.patient_id),--12
                                session_id,--13
                                doctor_call_type,--14
                                patient_call_type,--15
                                apmc.start_time,--16
                                end_time,--17
                                apmc.id, --18
                                apm.state --19
                            from appointment apm
                                left join user_account ua on ua.id = apm.patient_id
                                left join user_mngment um on um.id = ua.user_mngment_id
                                left join user_contact uc on uc.user_mngment_id = um.id
                                left join user_address uad on uad.user_contact_id = uc.id and is_primary = True
                                left join appointment_call_history apmc on apmc.appointment_id = apm.id
                            where apm.id = %s'''
                    res = self.general_business.sql_execute(self.pool_conn, sql, para_values=[apm_id])
                    if res:
                        data = {
                            "patient": {
                                "profileId": res[0][0],
                                "avatarUrl": res[0][1] if res[0][1] else None,
                                "firstName": None if res[0][2] in ('False', 'None', '') else res[0][2],
                                "middleName": None if res[0][3] in ('False', 'None', '') else res[0][3],
                                "lastName": None if res[0][4] in ('False', 'None', '') else res[0][4],
                                "primaryAddress": {
                                    "number": res[0][5] if res[0][5] else None,
                                    "street": res[0][6] if res[0][6] else None,
                                    "cityCode": res[0][7] if res[0][7] else None,
                                    "state": res[0][8] if res[0][8] else None,
                                    "postCode": res[0][9] if res[0][9] else None,
                                    "countryCode": res[0][10] if res[0][10] else None,
                                },
                                "interactedCount": res[0][11],
                                "latestInteractedTime": res[0][12] and int(time.mktime(res[0][12].timetuple())) or None,
                            },
                            "callList": [],
                            "booking": {
                                "value": 0,
                                "unitCode": "vnd"
                            },
                            "finalValue": {
                                "value": 0,
                                "unitCode": "vnd"
                            },
                            "stateAppointment": res[0][19],
                        }
                        callList = []
                        for item in res:
                            if item[18]:
                                if item[16]:
                                    start_time = int(time.mktime(item[16].timetuple()))
                                else:
                                    start_time = 0
                                if item[17] and item[16]:
                                    end_time = int(time.mktime(item[17].timetuple()))
                                    diff_time = end_time - start_time
                                else:
                                    diff_time = 0
                                callList.append({"sessionId": item[13] if item[13] else None,
                                                 "callTypeCode": "incoming",
                                                 "action_time": item[16] and int(time.mktime(item[16].timetuple())) or None,
                                                 "duration": {
                                                     "value": diff_time,
                                                     "unitCode": "second"
                                                 }
                                                 })
                        data.update({'callList': callList})
                        sql = '''SELECT appointment_id,--0
                                        amount,--1
                                        btt.type,--2
                                        business_type,--3
                                        fee_type, --4
                                        currency,--5
                                        apm.state,--6
                                        apm.consultation_fee--7
                                FROM business_transfer_transaction btt
                                  left join appointment apm on apm.id = btt.appointment_id 
                                where appointment_id=%s'''
                        payment_info = self.general_business.sql_execute(self.pool_conn, sql, para_values=[apm_id])
                        if payment_info:
                            reschedule_total = 0
                            refund_total = 0
                            pay_dr = 0
                            apm_state = payment_info[0][6]
                            for pay in payment_info:
                                if pay[3] == 'reschedule_apm_fee':
                                    reschedule_total += pay[1]
                                    if apm_state == 'canceled_by_patient':
                                        refund_total += pay[1]/2
                                if pay[3] == 'doctor_apm_fee':
                                    if apm_state == 'canceled_by_patient':
                                        refund_total += pay[1]/2
                                    data.update({"booking": {
                                        "value": pay[1],
                                        "unitCode": pay[5]
                                    }})
                                if not pay[3] and pay[4] == 'pay_dr':
                                    pay_dr = pay[1]
                                    data.update({
                                        "finalValue": {
                                            "value": pay[1],
                                            "unitCode": pay[5]
                                        }
                                    })
                            if reschedule_total != 0:
                                data.update({
                                    "reschedule": {
                                        "value": reschedule_total,
                                        "unitCode": pay[5]
                                    },
                                })
                            if refund_total != 0:
                                data.update({
                                    "refund": {
                                        "value": (refund_total if pay_dr != 0 else (data['booking']['value'] + reschedule_total)) * -1,
                                        "unitCode": pay[5]
                                    },
                                })
                        resp.status = status_code[200]
                        resp.body = json.dumps(data)
                        return

                    resp.status = resp_status
                    resp.body = json.dumps(resp_body)
                except Exception as e:
                    print e
                    resp.status = resp_status
                    resp.body = json.dumps(resp_body)
        except Exception as e:
            logger.error('account_virtual_api %s' % str(e))

    ## apply for /virtualaccount/paymentmethod/{id}/remove
    def on_post(self, req, resp, id=None, hospital_id=None):

        try:
            data = {}
            header = req.headers  # key cua header luon viet hoa
            all_header = self.general_business.get_header_request(header)
            lang = all_header.get('lang')
            _ = self.general_business.get_lang_instance(lang)
            data.update(all_header)

            resp_status = status_code[401]
            resp_body = {
                "title": _("Post account virtual api failed"),
                "msg": _("An error occurred. Please try to post account virtual again."),
            }

            ## authen token
            user_token = header.get("X-USER-TOKEN", False)
            user_id = False
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                else:
                    resp_status = status_code[203]
                    resp_body = {
                        "code": "expired_token",
                        "title": _("Expired token"),
                        "msg": _("Your login-token is expired, please try to login again")
                    }
                    resp.status = resp_status
                    resp.body = json.dumps(resp_body)
                    return

            # WD8.2 API for doctor to get payment statement summary
            if self.signal == 'payment_summary' and user_id:
                raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                startDate = raw_data.get('startDate') + ' 00:00:00'
                endDate = raw_data.get('endDate') + ' 23:59:59'

                start_date = datetime.strptime(startDate, '%d-%m-%Y %H:%M:%S')
                end_date = datetime.strptime(endDate, '%d-%m-%Y %H:%M:%S')

                if start_date > end_date:
                        resp_status = status_code[401]
                        resp_body = {
                                        "code": "wrong_time",
                                        "title": _("Wrong time"),
                                        "msg": _("The start date is bigger than end date, please try to set again")
                                 }
                        resp.status = resp_status
                        resp.body = json.dumps(resp_body)

                        return

                sql_select = ''' select btt.amount, --0
                                    btt.appointment_id, --1
                                    btt.write_date, --2
                                    apm.state, --3
                                    mdcd.value, --4
                                    mdcd.unit, --5
                                    COALESCE(btt.currency,'vnd') --6
                                from business_transfer_transaction btt
                                inner join user_wallet uw on btt.wallet_dest_id = uw.id and uw.user_account_id = %s
                                inner join appointment apm on apm.id = btt.appointment_id
                                inner join meta_dr_consultation_durations mdcd on mdcd.code = apm.consultation_duration
                                where btt.fee_type = 'pay_dr' and btt.write_date >= '%s'::timestamp and btt.write_date <= '%s'::timestamp
                                order by btt.write_date
                              '''%(user_id,start_date.strftime('%Y-%m-%d %H:%M:%S'),end_date.strftime('%Y-%m-%d %H:%M:%S'))

                # records = self.general_business.sql_execute(self.pool_conn,sql_select,para_values=[user_id,startDate,endDate])
                records = self.general_business.sql_execute(self.pool_conn,sql_select)

                dateFlag = ''
                daySummaryList = []
                appointmentTotal = 0
                canceledAppointmentTotal = 0
                consultationDurationTotal = 0

                resp_status = status_code[200]
                resp_body = {
                                    'appointmentCount': appointmentTotal,
                                    'cancellationCount': canceledAppointmentTotal,
                                    'consultationDuration': {
                                        'value': consultationDurationTotal,
                                        'unitCode': 'vnd'
                                    },
                                    'daySummaryList':daySummaryList
                                 }
                if records:
                    for line in records:
                        date = line[2].strftime('%d-%m-%Y')
                        if date != dateFlag:
                            dateFlag = date
                            daySummaryList.append({
                                'date':date,
                                'totalNetIncome': {
                                    'value':line[0],
                                    'unitCode':line[6]
                                }
                            })
                        elif date == dateFlag:
                            daySummaryList[-1]['totalNetIncome']['value'] = daySummaryList[-1]['totalNetIncome']['value'] + line[0]

                        if line[3] == 'canceled_by_patient' or line[3] == 'canceled_by_doctor':
                            canceledAppointmentTotal += 1

                        appointmentTotal += 1
                        consultationDurationTotal += line[4]

                    resp_status = status_code[200]
                    resp_body = {
                                    'appointmentCount': appointmentTotal,
                                    'cancellationCount': canceledAppointmentTotal,
                                    'consultationDuration': {
                                        'value': consultationDurationTotal,
                                        'unitCode': records[0][5]
                                    },
                                    'daySummaryList':daySummaryList
                                 }

            elif self.signal == 'doctor_hospital_payment_summary' and user_id:
                raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                sql_select = ''' select btt.amount, --0
                                    btt.appointment_id, --1
                                    btt.write_date, --2
                                    apm.state, --3
                                    mdcd.value, --4
                                    mdcd.unit, --5
                                    COALESCE(btt.currency,'vnd') --6
                                from business_transfer_transaction btt
                                inner join user_wallet uw on btt.wallet_dest_id = uw.id and uw.user_account_id = %s
                                inner join appointment apm on apm.id = btt.appointment_id
                                inner join meta_dr_consultation_durations mdcd on mdcd.code = apm.consultation_duration
                                where btt.fee_type = 'pay_dr' 
                                order by btt.write_date
                              '''%(user_id)

                # records = self.general_business.sql_execute(self.pool_conn,sql_select,para_values=[user_id,startDate,endDate])
                records = self.general_business.sql_execute(self.pool_conn,sql_select)

                dateFlag = ''
                daySummaryList = []
                appointmentTotal = 0
                canceledAppointmentTotal = 0
                consultationDurationTotal = 0

                resp_status = status_code[200]
                resp_body = {
                                    'appointmentCount': appointmentTotal,
                                    'cancellationCount': canceledAppointmentTotal,
                                    'consultationDuration': {
                                        'value': consultationDurationTotal,
                                        'unitCode': 'vnd'
                                    },
                                    'daySummaryList':daySummaryList
                                 }
                if records:
                    for line in records:
                        date = line[2].strftime('%d-%m-%Y')
                        if date != dateFlag:
                            dateFlag = date
                            daySummaryList.append({
                                'date':date,
                                'totalNetIncome': {
                                    'value':line[0],
                                    'unitCode':line[6]
                                }
                            })
                        elif date == dateFlag:
                            daySummaryList[-1]['totalNetIncome']['value'] = daySummaryList[-1]['totalNetIncome']['value'] + line[0]

                        if line[3] == 'canceled_by_patient' or line[3] == 'canceled_by_doctor':
                            canceledAppointmentTotal += 1

                        appointmentTotal += 1
                        consultationDurationTotal += line[4]

                    resp_status = status_code[200]
                    resp_body = {
                                    'appointmentCount': appointmentTotal,
                                    'cancellationCount': canceledAppointmentTotal,
                                    'consultationDuration': {
                                        'value': consultationDurationTotal,
                                        'unitCode': records[0][5]
                                    },
                                    'daySummaryList':daySummaryList
                                 }
            
            elif self.signal == 'doctor_hospital_payment_statement_daily_detail' and user_id:
                raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                sql_select = ''' select btt.amount, --0
                                    btt.appointment_id, --1
                                    apm.schedule_date, --2
                                    apm.state, --3
                                    mdcd.value, --4
                                    mdcd.unit, --5
                                    um.first_name,--6
                                    um.middle_name,--7
                                    um.last_name, --8
                                    COALESCE(btt.currency,'vnd') --9
                                from business_transfer_transaction btt
                                inner join user_wallet uw on btt.wallet_dest_id = uw.id and uw.user_account_id = %s
                                inner join appointment apm on apm.id = btt.appointment_id
                                inner join meta_dr_consultation_durations mdcd on mdcd.code = apm.consultation_duration
                                inner join user_account ua on ua.id = apm.patient_id
                                inner join user_mngment um on um.id = ua.user_mngment_id
                                where btt.fee_type = 'pay_dr'
                                order by btt.write_date desc
                              '''%(user_id)

                records = self.general_business.sql_execute(self.pool_conn,sql_select)

                daySummaryList = []
                resp_status = status_code[200]
                resp_body = {
                    'list': daySummaryList
                }
                if records:
                    for line in records:
                        timetmp = int(time.mktime(line[2].timetuple()))
                        sql_history = '''select coalesce(start_time,create_date),--0
                                                end_time,--1
                                                state --2
                                          from appointment_call_history 
                                          where state in ('successful_finished') and appointment_id = %s '''%line[1]
                        list_history = self.general_business.sql_execute(self.pool_conn, sql_history)
                        duration = 0
                        for h in list_history:
                            if h[0]:
                                start_time = int(time.mktime(h[0].timetuple()))
                            else:
                                start_time = 0
                            if h[0] and h[1]:
                                end_time = int(time.mktime(h[1].timetuple()))
                                diff_time = (end_time - start_time)
                            else:
                                diff_time = 0
                            duration += diff_time

                        daySummaryList.append({
                                "appointmentId": line[1],
                                "scheduleTime": timetmp,
	                            "startTime": timetmp,
                                "patient": {
                                    "firstName": None if line[6] in ('False', 'None', '') else line[6],
                                    "middleName": None if line[7] in ('False', 'None', '') else line[7],
                                    "lastName": None if line[8] in ('False', 'None', '') else line[8]
                                },
                                "stateCode": line[3],
                                'duration': {
                                        'value': duration,
                                        'unitCode': 'second'
                                    },
                                'netIncome': {
                                    'value':line[0],
                                    'unitCode':line[9]
                                }
                            })


                    resp_status = status_code[200]
                    resp_body = {
                                    'list':daySummaryList
                                 }
            
            elif self.signal == 'transactionhistory_get' and user_id:
                raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                month = raw_data.get('month',False)
                data.update({'user_id': user_id,
                             'month': month})
                resp_status = status_code[401]
                resp_body = {
                                "code": "transactionhistory_get_failed",
                                "title": _("Get transaction history failed"),
                                "msg": _("An error occurred. Please try to request again.")
                         }
                record = self.transactionhistory_get(data)
                if record:
                    resp_status = status_code[record['status_code']]
                    resp_body = record['resq_body']

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
                return

            # WD8.3 API for doctor to get payment statement daily detail
            elif self.signal == 'payment_statement_daily_detail' and user_id:
                raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                startDate = raw_data.get('date') + ' 00:00:00'
                endDate = raw_data.get('date') + ' 23:59:59'

                start_date = datetime.strptime(startDate, '%d-%m-%Y %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S')
                end_date = datetime.strptime(endDate, '%d-%m-%Y %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S')

                sql_select = ''' select btt.amount, --0
                                    btt.appointment_id, --1
                                    apm.schedule_date, --2
                                    apm.state, --3
                                    mdcd.value, --4
                                    mdcd.unit, --5
                                    um.first_name,--6
                                    um.middle_name,--7
                                    um.last_name, --8
                                    COALESCE(btt.currency,'vnd') --9
                                from business_transfer_transaction btt
                                inner join user_wallet uw on btt.wallet_dest_id = uw.id and uw.user_account_id = %s
                                inner join appointment apm on apm.id = btt.appointment_id
                                inner join meta_dr_consultation_durations mdcd on mdcd.code = apm.consultation_duration
                                inner join user_account ua on ua.id = apm.patient_id
                                inner join user_mngment um on um.id = ua.user_mngment_id
                                where btt.fee_type = 'pay_dr' and btt.write_date >= '%s'::timestamp and btt.write_date <= '%s'::timestamp
                                order by btt.write_date
                              '''%(user_id,start_date,end_date)

                records = self.general_business.sql_execute(self.pool_conn,sql_select)

                daySummaryList = []
                resp_status = status_code[200]
                resp_body = {
                    'list': daySummaryList
                }
                if records:
                    for line in records:
                        timetmp = int(time.mktime(line[2].timetuple()))
                        sql_history = '''select coalesce(start_time,create_date),--0
                                                end_time,--1
                                                state --2
                                          from appointment_call_history 
                                          where state in ('successful_finished') and appointment_id = %s '''%line[1]
                        list_history = self.general_business.sql_execute(self.pool_conn, sql_history)
                        duration = 0
                        for h in list_history:
                            if h[0]:
                                start_time = int(time.mktime(h[0].timetuple()))
                            else:
                                start_time = 0
                            if h[0] and h[1]:
                                end_time = int(time.mktime(h[1].timetuple()))
                                diff_time = (end_time - start_time)
                            else:
                                diff_time = 0
                            duration += diff_time

                        daySummaryList.append({
                                "appointmentId": line[1],
                                "scheduleTime": timetmp,
	                            "startTime": timetmp,
                                "patient": {
                                    "firstName": None if line[6] in ('False', 'None', '') else line[6],
                                    "middleName": None if line[7] in ('False', 'None', '') else line[7],
                                    "lastName": None if line[8] in ('False', 'None', '') else line[8]
                                },
                                "stateCode": line[3],
                                'duration': {
                                        'value': duration,
                                        'unitCode': 'second'
                                    },
                                'netIncome': {
                                    'value':line[0],
                                    'unitCode':line[9]
                                }
                            })


                    resp_status = status_code[200]
                    resp_body = {
                                    'list':daySummaryList
                                 }

        except Exception as e:
            logger.error('account_virtual_api %s' % str(e))
        resp.status = resp_status
        resp.body = json.dumps(resp_body)
