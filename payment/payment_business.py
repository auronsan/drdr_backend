#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime
import hashlib
import logging
from Crypto.Hash import HMAC, SHA256
#from paypalrestsdk import Payment
#import paypalrestsdk
import stripe

logger = logging.getLogger('drdr_api')
import sys
import falcon
import ConfigParser
import os
from raven import Client

project_path =  str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
config = ConfigParser.RawConfigParser(allow_no_value=True)
config.read(project_path + '/' + "environment.conf")
sentry_key = config.get('environment', 'sentry_key')
client = Client(sentry_key)

sys.path.insert(0, "..")
try:
    from general import general_business
    from model.business_transfer_transaction import business_transfer_transaction
    from model.access_model import access_model
except ImportError as e:
    print('No Import ', e)

status_code = {
    200: falcon.HTTP_200,
    401: falcon.HTTP_401,
    502: falcon.HTTP_502,
    203: falcon.HTTP_203
}
drdr_scratch_card_wallet=1
class payment_business(object):
    def __init__(self, erp_connection, var_contructor):
        self.pool_conn = erp_connection

        self.ERP_DB_HOST = var_contructor['ERP_DB_HOST']
        self.ERP_DB_PORT = var_contructor['ERP_DB_PORT']
        self.ERP_DB_USER = var_contructor['ERP_DB_USER']
        self.ERP_DB_PASS = var_contructor['ERP_DB_PASS']
        self.ERP_DB_NAME = var_contructor['ERP_DB_NAME']

        self.general_business = general_business(erp_connection, var_contructor)
        self.business_transfer = business_transfer_transaction(erp_connection, var_contructor)
        self.access_model = access_model(erp_connection, var_contructor)
        self.drdr_wallet = int(config.get('environment', 'drdr_wallet_id')) or 1

    def create_scratch_card_transaction(self, amount, merref, serial, payment_gateway, dest_wallet, gateway_trans_code, note=None, currency=None):
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": "Payment transaction failed",
                        "msg": "An error occurred. Please try to request again.",
                        "code": "payment_card_failed"
                    }
                }
        try:
            business_transfer = self.business_transfer
            access_model = self.access_model
            if amount and merref:
                payment_id=self.create_payment_trans(amount, merref, 'done', payment_gateway, dest_wallet,'scratch_card', gateway_trans_code, serial=serial)
                if payment_id:
                    trans_id=business_transfer.action_create_in(self.drdr_wallet, dest_wallet, amount, payment_id, currency=currency)
                    available_balance = access_model.search('user_wallet', [('id', '=', dest_wallet)],
                                                            ['available_balance'])
                    if trans_id:
                        logger.info('create_payment_trans Successful: %s' % trans_id)
                        resp.update({'status_code': 200,
                                     'resq_body': {
                                         "amount": amount,
                                         "currencyUnitCode": currency,
                                         "balance": {
                                             "value": available_balance[0][0] or False,
                                             "unitCode": currency
                                         }
                                     }})
            else:
                logger.error('create_scratch_card_transaction error: MerchRef or amount should not NULL')
                resp.update({'status_code': 401,
                             'resq_body': {
                                 "title": "Payment transaction failed",
                                 "msg": "An error occurred. Null value. Cannot create trans.",
                                 "code": "payment_failed"
                             }
                             })
        except Exception as e:
            logger.error('create_scratch_card_transaction error: %s' % str(e))
            client.captureException()
        return resp

    def create_drdr_card_transaction(self, data, note=None):
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": "Payment failed",
                        "msg": "An error occurred. Please try to request again.",
                        "code": "payment_card_failed"
                    }
                }
        try:
            access_model = self.access_model
            business_transfer = self.business_transfer
            amount = data.get('amount')
            qr_code = data.get('qr_code')
            serial = data.get('serial')
            currency = data.get('currency')
            dest_wallet = data.get('dest_wallet')
            if data.get('amount', False) and data.get('qr_code', False) and data.get('serial', False):
                payment_id = self.create_payment_trans(amount, '', 'done', 'drdr', dest_wallet,
                                                       'qr_card', serial, serial=qr_code)
                if payment_id:
                    trans_id=business_transfer.action_create_in(self.drdr_wallet, dest_wallet, amount, payment_id, currency=currency)
                    available_balance = access_model.search('user_wallet', [('id', '=', dest_wallet)],
                                                            ['available_balance'])
                    if trans_id:
                        logger.info('create_payment_trans Successful: %s' % trans_id)
                        resp.update({'status_code': 200,
                                     'resq_body': {
                                         "amount": amount,
                                         "currencyUnitCode": currency,
                                         "balance": {
                                             "value": available_balance[0][0] or False,
                                             "unitCode": currency
                                         }
                                     }
                                     })
            else:
                logger.error('create_drdr_card_transaction error: Serial or amount should not NULL')
                resp.update({'status_code': 401,
                             'resq_body': {
                                 "title": "Payment failed",
                                 "msg": "An error occurred. Null value. Cannot create trans.",
                                 "code": "payment_failed"
                             }
                             })
        except Exception as e:
            logger.error('create_drdr_card_transaction error: %s' % str(e))
            client.captureException()
        return resp

    def create_payment_trans(self, amount, merref, state, payment_gateway,user_wallet_id, type, gateway_trans_code,
                             serial=None,user_payment_info=None, note=None):
        try:
            data={}
            access_model=self.access_model
            if payment_gateway and user_wallet_id and state:
                data.update({
                    'merref': merref,
                    'amount': amount,
                    'phone_card_sn': serial or None,
                    'state': state,
                    'note': note or None,
                    'user_payment_info_id':  user_payment_info or None,
                    'type': type or '',
                    'payment_gateway': payment_gateway,
                    'user_wallet_id': user_wallet_id,
                    'gateway_trans_code': gateway_trans_code or ''
                })
                create_tran=access_model.create('payment_transaction', data)
                if len(create_tran)>0:
                    logger.info('create_payment_trans Successful: %s'%create_tran)
                    return create_tran
            logger.error('create_payment_trans error: Cannot create Payment')
            return False
        except Exception as e:
            client.captureException()
            logger.error('create_payment_trans error: %s' % str(e))

    def gen_merref(self, user_id, payment_type, gateway):
        value_hash = str(user_id) + datetime.now().strftime('%Y%m%d%H%M%S')
        token = hashlib.md5(value_hash).hexdigest()
        merref = gateway + '_' + payment_type + '_' +token + '_' + user_id
        return merref

    def gen_signature(self, msg, key):
        hash_obj = HMAC.new(key=key, msg=msg, digestmod=SHA256)
        return hash_obj.hexdigest()

    def gen_url(self, access_key, pin, serial, transRef, type, key, url):
        str = "access_key=%s&pin=%s&serial=%s&transRef=%s&type=%s" % (access_key, pin, serial, transRef, type)
        mac_key = self.gen_signature(str, key)
        gen_url = url + str + '&signature=%s' % mac_key
        return gen_url

    def payment_paypal_proceed(self, data):
        PayerID = data.get('PayerID', False)
        paymentId = data.get('paymentId', False)
        payment = Payment.find(paymentId)
        if payment.execute({"payer_id": PayerID}):
            resp = {'status_code': 200,
                    'resq_body':
                        {
                            "title": 'Successful',
                            "msg": "Sucessful. Please check your wallet.",
                            "code": "successful_payment"
                        }
                    }

            # TODO: update wallet

        else:
            resp = {'status_code': 200,
                    'resq_body':
                        {
                            "title": 'Payment error',
                            "msg": payment.error,
                            "code": "payment_error"
                        }
                    }
        return resp

    def payment_paypal_deposit(self, data):
        logger.info('payment_paypal_deposit %s' % data)
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": "Payment paypal failed",
                        "msg": "An error occurred. Please try to request again.",
                        "code": "payment_paypal_failed"
                    }
                }
        try:
            gs = self.general_business
            user_token = data.get('user_token')
            amount = data.get('amount')
            currencyUnitCode = data.get('currencyUnitCode').upper()
            config = gs.get_system_param('paypal_config')
            domain_api = gs.get_system_param('domain_api')
            paypalrestsdk.configure(config)
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    payment = Payment({
                        "intent": "sale",
                        # Set payment method
                        "payer": {
                            "payment_method": "paypal"
                        },
                        # Set redirect urls
                        "redirect_urls": {
                            "return_url": "%s/mp/paypal/%s/process"%(domain_api,user_id),
                            "cancel_url": "%s/mp/paypal/%s/cancel"%(domain_api,user_id)
                        },
                        # Set transaction object
                        "transactions": [{
                            "item_list": {
                                "items": [{
                                    "name": "Payment deposit to DrDr",
                                    # "sku": "item",
                                    "price": "%s"%amount,
                                    "currency": "%s"%currencyUnitCode,
                                    "quantity": 1}]},
                            "amount": {
                                "total": "%s"%amount,
                                "currency": "%s"%currencyUnitCode
                            },
                            "description": ""
                        }]
                    })
                    # Create payment
                    if payment.create():
                        # Extract redirect url
                        for link in payment.links:
                            if link.method == "REDIRECT":
                                # Capture redirect url
                                redirect_url = str(link.href)
                                resp.update({'status_code': 200,
                                             'resq_body': {
                                                 'redirectUrl': redirect_url
                                             }
                                             })
                    else:
                        resp.update({'status_code': 401,
                                     'resq_body': {
                                         "title": "Payment paypal failed",
                                         "msg": payment.error,
                                         "code": "payment_paypal_failed"
                                     }
                                     })


                else:
                    resp.update({'status_code': 203,
                                 'resq_body': {
                                     'code': 'expired_token',
                                     "title": "Expired token",
                                     "msg": "Your login-token is expired, please try to login again"
                                 }
                                 })
        except Exception as e:
            logger.error('error_user_logout %s' % str(e))
        return resp

    def insert_stripe_customer(self, user_id, card_token):
        try:
            gs = self.general_business
            access_model = self.access_model
            stripe_sk = config.get('environment', 'stripe_sk')
            insert_data = {}
            user_email = access_model.search('user_account', [('id', '=', user_id)], ['email'])
            if user_email and len(user_email) > 0:
                stripe.api_key = stripe_sk
                resp_data = stripe.Customer.create(
                    description=user_email[0][0],
                    source=card_token
                )
                if resp_data and resp_data.get('id'):
                    user_wallet = self.get_wallet(user_id)
                    insert_data.update({
                        'subscription_key': resp_data.get('id'),
                        'user_wallet_id': user_wallet,
                        'payment_gate_code': 'stripe'
                    })
                    id = access_model.create('user_payment_info', insert_data)
                    if id:
                       return id
            return False
        except Exception as e:
            client.captureException()
            return False

    def payment_stripe_create_customer(self, data):
        logger.info('payment_stripe_create_customer %s' % data)
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": "Payment stripe failed",
                        "msg": "An error occurred. Please try to request again.",
                        "code": "payment_stripe_failed"
                    }
                }
        try:
            gs = self.general_business
            user_token = data.get('user_token')
            card_token = data.get('card_token')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    if self.insert_stripe_customer(self, user_id, card_token):
                        resp.update({'status_code': 200,
                                     'resq_body': {
                                         "title": "Payment stripe success",
                                         "msg": 'Payment stripe success',
                                         "code": "payment_stripe_success"
                                     }
                                     })
                    else:
                        resp.update({'status_code': 401,
                                     'resq_body': {
                                         "title": "Payment stripe failed",
                                         "msg": 'User_id is not exist',
                                         "code": "payment_stripe_failed"
                                     }
                                     })
        except Exception as e:
            logger.error('error_user_logout %s' % str(e))
            client.captureException()
        return resp

    def payment_stripe_update_card_customer(self, data):
        logger.info('payment_stripe_update_card_customer %s' % data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": "Payment stripe failed",
                        "msg": "An error occurred. Please try to request again.",
                        "code": "payment_stripe_failed"
                    }
                }
        try:
            gs = self.general_business
            access_model = self.access_model
            user_token = data.get('user_token')
            card_token=data.get('card_token')
            stripe_sk = config.get('environment', 'stripe_sk')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    user_wallet = self.get_wallet(user_id)
                    if user_wallet:
                        stripe.api_key = stripe_sk
                        subs_key = access_model.search('user_payment_info',
                                                [('user_wallet_id', '=', user_wallet)], ['subscription_key'])
                        if subs_key:
                            cus=stripe.Customer.retrieve(subs_key[0][0])
                            cus.source = card_token
                            if cus.save():
                                resp.update({'status_code': 200,
                                             'resq_body': {
                                                 "title": _("Payment update success"),
                                                 "msg": _('Payment update success'),
                                                 "code": "payment_update_success"
                                             }
                                             })
                    else:
                        resp.update({'status_code': 401,
                                     'resq_body': {
                                         "title": "Payment stripe failed",
                                         "msg": 'User_wallet is not exist',
                                         "code": "payment_stripe_failed"
                                     }
                                     })
                else:
                    resp.update({'status_code': 203,
                                 'resq_body': {
                                     'code': 'expired_token',
                                     "title": "Expired token",
                                     "msg": "Your login-token is expired, please try to login again"
                                 }
                                 })
        except Exception as e:
            logger.error('error_stripe_update_card %s' % str(e))
            client.captureException()
        return resp

    def payment_stripe_get_card(self, data):
        logger.info('payment_stripe_get_card %s' % data)
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": "Payment stripe failed",
                        "msg": "An error occurred. Please try to request again.",
                        "code": "payment_stripe_failed"
                    }
                }
        try:
            gs = self.general_business
            access_model = self.access_model
            user_token = data.get('user_token')
            stripe_sk = config.get('environment', 'stripe_sk')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    user_wallet = self.get_wallet(user_id)
                    if user_wallet:
                        stripe.api_key = stripe_sk
                        subs_key = access_model.search('user_payment_info',
                                                       [('user_wallet_id', '=', user_wallet)], ['subscription_key'])
                        logger.info('user_wallet key %s' % user_wallet)
                        logger.info('subs key %s' % subs_key)

                        if subs_key and len (subs_key) > 0:
                            # list_card= (stripe.Customer.retrieve(subs_key[0][0]).sources.all(object='card')).get('data', False)
                            customer=stripe.Customer.retrieve(subs_key[0][0])
                            default_card = customer.default_source
                            if default_card:
                                card_obj = customer.sources.retrieve(default_card)
                                resp.update({'status_code': 200,
                                             'resq_body': {
                                                 "creditCardList": [card_obj]
                                             }
                                             })
                            else:
                                resp.update({'status_code': 200,
                                             'resq_body': {
                                                 "creditCardList": []
                                             }
                                             })
                        else:
                            resp.update({'status_code': 200,
                                         'resq_body': {
                                             "creditCardList": []
                                         }
                                         })
                    else:
                        resp.update({'status_code': 401,
                                     'resq_body': {
                                         "title": "Payment stripe failed",
                                         "msg": 'User_wallet is not exist',
                                         "code": "payment_stripe_failed"
                                     }
                                     })
                else:
                    resp.update({'status_code': 203,
                                 'resq_body': {
                                     'code': 'expired_token',
                                     "title": "Expired token",
                                     "msg": "Your login-token is expired, please try to login again"
                                 }
                                 })
        except Exception as e:
            logger.error('payment_stripe_get_card %s' % str(e))
            client.captureException()
        return resp

    def payment_stripe_add_card(self, data):
        logger.info('payment_stripe_add_card %s' % data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": "Payment stripe failed",
                        "msg": _("Add credit card failed"),
                        "code": "payment_stripe_failed"
                    }
                }
        try:
            gs = self.general_business
            access_model = self.access_model
            user_token = data.get('user_token')
            card_token = data.get('card_token')
            stripe_sk = config.get('environment', 'stripe_sk')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    user_wallet = self.get_wallet(user_id)
                    if user_wallet:
                        stripe.api_key = stripe_sk
                        subs_key = access_model.search('user_payment_info',
                                                       [('user_wallet_id', '=', user_wallet)], ['subscription_key'])
                        if subs_key and len(subs_key) > 0:
                            customer = stripe.Customer.retrieve(subs_key[0][0])
                            resp_stripe = customer.sources.create(source=card_token)
                            if resp_stripe:
                                resp.update({'status_code': 200,
                                             'resq_body': {
                                                 "title": "Payment add card success",
                                                 "msg": _('Add credit card success'),
                                                 "code": "payment_add_success"
                                             }
                                             })
                        else:
                            if self.insert_stripe_customer(user_id, card_token):
                                subs_key = access_model.search('user_payment_info',
                                                       [('user_wallet_id', '=', user_wallet)], ['subscription_key'])
                                if subs_key and len(subs_key) > 0:
                                    customer = stripe.Customer.retrieve(subs_key[0][0])
                                    if customer:
                                        resp.update({'status_code': 200,
                                                     'resq_body': {
                                                         "title": "Payment add card success",
                                                         "msg": _('Add credit card success'),
                                                         "code": "payment_add_success"
                                                     }
                                                     })
                            else:
                                resp.update({'status_code': 401,
                                             'resq_body': {
                                                 "title": "Create customer unsuccessfully",
                                                 "msg": 'Create customer unsuccessfully',
                                                 "code": "create_customer_unsuccess"
                                             }
                                             })
                    else:
                        resp.update({'status_code': 401,
                                     'resq_body': {
                                         "title": "Payment stripe failed",
                                         "msg": 'User_wallet is not exist',
                                         "code": "payment_stripe_failed"
                                     }
                                     })
                else:
                    resp.update({'status_code': 203,
                                 'resq_body': {
                                     'code': 'expired_token',
                                     "title": "Expired token",
                                     "msg": "Your login-token is expired, please try to login again"
                                 }
                                 })
        except Exception as e:
            logger.error('error_payment_stripe_add_card %s' % str(e))
            client.captureException()
        return resp

    def payment_stripe_remove_card(self, data):
        logger.info('payment_stripe_remove_card %s' % data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": "Payment stripe failed",
                        "msg": _("Remove credit card failed"),
                        "code": "payment_stripe_failed"
                    }
                }
        try:
            gs = self.general_business
            access_model = self.access_model
            user_token = data.get('user_token')
            card_id = data.get('card_id')
            stripe_sk = config.get('environment', 'stripe_sk')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    user_wallet = self.get_wallet(user_id)
                    if user_wallet:
                        stripe.api_key = stripe_sk
                        subs_key = access_model.search('user_payment_info',
                                                       [('user_wallet_id', '=', user_wallet)], ['subscription_key'])
                        if subs_key and len(subs_key) >0:
                            customer = stripe.Customer.retrieve(subs_key[0][0])
                            resp_stripe=customer.sources.retrieve(card_id).delete()
                            if resp_stripe:
                                resp.update({'status_code': 200,
                                                 'resq_body': {
                                                     "title": _("Payment remove card success"),
                                                     "msg": _('Remove credit card success'),
                                                     "code": "payment_remove_success"
                                                 }
                                                 })
                    else:
                        resp.update({'status_code': 401,
                                     'resq_body': {
                                         "title": "Payment stripe failed",
                                         "msg": 'User_wallet is not exist',
                                         "code": "payment_stripe_failed"
                                     }
                                     })
                else:
                    resp.update({'status_code': 203,
                                 'resq_body': {
                                     'code': 'expired_token',
                                     "title": "Expired token",
                                     "msg": "Your login-token is expired, please try to login again"
                                 }
                                 })
        except Exception as e:
            logger.error('error_payment_stripe_remove_card %s' % str(e))
            client.captureException()
        return resp


    def payment_stripe_deposit(self, data):
        logger.info('payment_stripe_deposit %s' % data)

        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": "Payment stripe failed",
                        "msg": "An error occurred. Please try to request again.",
                        "code": "payment_stripe_failed"
                    }
                }
        try:
            gs = self.general_business
            access_model = self.access_model
            business_transfer=self.business_transfer
            user_token = data.get('user_token')
            amount=data.get('amount')
            currency = data.get('currency')
            card_id = data.get('card_id')
            resq_user_id = data.get('user_id')
            stripe_sk = config.get('environment', 'stripe_sk')
            client_id = data.get('client_id')

            if currency == 'vnd' and (amount < 20000 or amount > 1000000):
                resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": "Deposit failed",
                        "msg": _("The lowest value is 20.000vnd and maximum is 1.000.000vnd"),
                        "code": "reach_amount_limit"
                    }
                }
                return resp

            data = {}
            resq_user_wallet = False
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,client_id)
                if res_token:
                    user_id = res_token[1]
                    if resq_user_id:
                        resq_user_wallet = self.get_wallet(resq_user_id)
                    user_wallet = self.get_wallet(user_id)
                    if user_wallet:
                        stripe.api_key = stripe_sk
                        subs_key = access_model.search('user_payment_info',
                                                [('user_wallet_id', '=', user_wallet)], ['subscription_key'])
                        user_email = access_model.search('user_account', [('id', '=', user_id)], ['email'])
                        if subs_key:
                            charge_resp=stripe.Charge.create(
                                amount=int(amount),
                                currency=currency,
                                customer=subs_key[0][0],
                                card=card_id,
                                description="Charge for email %s"%user_email[0][0]
                            )
                            if charge_resp:
                                r_amount=charge_resp.get('amount', False)
                                r_currency=charge_resp.get('currency', False)
                                r_customer=charge_resp.get('customer', False)
                                r_charge_id = charge_resp.get('id', False)
                                note=charge_resp.get('outcome', False)
                                source=charge_resp.get('source', False)
                                # if note:
                                #     note=note.get('network_status', False)
                                if r_amount and r_currency and r_customer and r_charge_id:
                                    if r_amount == amount and r_currency == currency and\
                                                    r_customer == subs_key[0][0]:
                                        data.update({
                                            'amount': r_amount,
                                            'state': 'done',
                                            'note': note and note.get('network_status', False) or None,
                                            'customer_token': subs_key or None,
                                            'type': source and source.get('brand', False) or '',
                                            'payment_gateway': 'stripe',
                                            'user_wallet_id': resq_user_wallet or user_wallet,
                                            'gateway_trans_code': r_charge_id or ''
                                        })
                                        create_tran = access_model.create('payment_transaction', data)
                                        if create_tran:
                                            trans_id = business_transfer.action_create_in(self.drdr_wallet,
                                                                                          resq_user_wallet or user_wallet, amount,
                                                                                          create_tran, currency=currency)
                                            available_balance = access_model.search('user_wallet', [('id', '=', resq_user_wallet or user_wallet)],
                                                                             ['available_balance'])
                                            if trans_id:
                                                logger.info('create_payment_trans Successful: %s' % create_tran)
                                                resp.update({'status_code': 200,
                                                             'resq_body': {
                                                                 "balance": {
                                                                            "value": available_balance[0][0] or False,
                                                                            "unitCode": currency
                                                                    }
                                                             }
                                                             })

                    else:
                        resp.update({'status_code': 401,
                                     'resq_body': {
                                         "title": "Payment stripe failed",
                                         "msg": 'User_wallet is not exist',
                                         "code": "payment_stripe_failed"
                                     }
                                     })
                else:
                    resp.update({'status_code': 203,
                                 'resq_body': {
                                     'code': 'expired_token',
                                     "title": "Expired token",
                                     "msg": "Your login-token is expired, please try to login again"
                                 }
                                 })
        except Exception as e:
            logger.error('error_payment_stripe_deposit %s' % str(e))
            client.captureException()
        return resp

    def get_wallet(self, user_email, email=False):
        try:
            access_model = self.access_model
            if email:
                user_id = access_model.search('user_account',
                                                 [('email', '=', user_email)],
                                                 ['id'])
                if not user_id:
                    logger.error(
                        'get_wallet error: This email is not exist: %s' % user_id)
                    return False
                wallet_ids = access_model.search('user_wallet',
                                                 [('user_account_id', '=', user_id[0][0])],
                                                 ['id'])
            else:
                wallet_ids = access_model.search('user_wallet',
                                                 [('user_account_id', '=', user_email)],
                                                 ['id'])
            if wallet_ids and len(wallet_ids) > 0:
                return wallet_ids[0][0]
            logger.error(
                'get_wallet error: Cannot get_wallet for this user: %s' % user_id)
            return False
        except Exception as e:
            client.captureException()
            logger.error('get_wallet error: %s' % str(e))