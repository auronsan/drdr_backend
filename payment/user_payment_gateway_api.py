#!/usr/bin/env python
# -*- coding: utf-8 -*-
import falcon
import json
import sys
import logging
import requests
from raven import Client
import ConfigParser
import os

project_path =  str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
config = ConfigParser.RawConfigParser(allow_no_value=True)
config.read(project_path + '/' + "environment.conf")
sentry_key = config.get('environment', 'sentry_key')
client = Client(sentry_key)
sys.path.insert(0, "..")
logger = logging.getLogger('drdr_api')
try:
    from payment_business import payment_business
    from general import general_business
    from model.access_model import access_model
except ImportError as e:
    print('No Import ', e)
status_code = {
    200: falcon.HTTP_200,
    401: falcon.HTTP_401,
    502: falcon.HTTP_502,
    203: falcon.HTTP_203
}
onepay_config = json.loads(config.get('environment', 'onepay_config'))
ddcard_config = json.loads(config.get('environment', 'ddcard_config'))
scratch_payment_link = onepay_config['scratch_payment_link']
access_key = str(onepay_config['access_key'])
secret_key = str(onepay_config['secret_key'])


class user_payment_gateway_api(object):
    def __init__(self, signal, erp_connection=False, var_contructor=False):
        self.pool_conn = erp_connection

        self.ERP_DB_HOST = var_contructor['ERP_DB_HOST']
        self.ERP_DB_PORT = var_contructor['ERP_DB_PORT']
        self.ERP_DB_USER = var_contructor['ERP_DB_USER']
        self.ERP_DB_PASS = var_contructor['ERP_DB_PASS']
        self.ERP_DB_NAME = var_contructor['ERP_DB_NAME']

        self.signal = signal

        self.general_business = general_business(erp_connection, var_contructor)
        self.payment_business = payment_business(erp_connection, var_contructor)
        self.access_model = access_model(erp_connection, var_contructor)

    def on_get(self, req, resp, user_id = None, hospital_id = None):
        data = {}
        header = req.headers  # key cua header luon viet hoa
        all_header = self.general_business.get_header_request(header)
        lang = all_header.get('lang')
        _ = self.general_business.get_lang_instance(lang)
        data.update(all_header)


        if self.signal == 'payment_paypal_proceed':
            resp_status = status_code[401]
            resp_body = {
                "title": _("Paypal proceed faild"),
                "msg": _("Paypal proceed faild."),
                "code": "paypal_proceed_faild"
            }
            try:
                PayerID = req.get_param('PayerID', default=False)
                paymentId = req.get_param('paymentId', default=False)
                data.update({
                    'PayerID': PayerID,
                    'paymentId': paymentId,
                    'user_id': user_id
                })
                record = self.payment_business.payment_paypal_proceed(data)
                resp_status = status_code[record['status_code']]
                resp_body = record['resq_body']

            except Exception as e:
                print str(e)

            resp.status = resp_status
            resp.body = json.dumps(resp_body)

        elif self.signal == 'payment_paypal_cancel':
            resp_status = status_code[200]
            resp_body = {
                "title": _("Cancel Payment"),
                "msg": _("Cancel payment sucessful."),
                "code": "cancel_payment"
            }
            resp.status = resp_status
            resp.body = json.dumps(resp_body)
            
        elif self.signal == 'payment_stripe_get_list_card':
            resp_status = status_code[401]
            resp_body = {
                "code": "unsuccess_payment",
                "title": _("Unsuccessful retrieve card list"),
                "msg": _("Cannot get card list")
            }
            try:
                header = req.headers  # key cua header luon viet hoa
                data.update({
                    'user_token': header.get("X-USER-TOKEN", False),
                })
                record = self.payment_business.payment_stripe_get_card(data)
                if record:
                    resp_status = status_code[record['status_code']]
                    resp_body = record['resq_body']
            except Exception as e:
                print str(e)
            resp.status = resp_status
            resp.body = json.dumps(resp_body)
    def on_post(self, req, resp, hospital_id = None):
        data = {}
        header = req.headers  # key cua header luon viet hoa
        all_header = self.general_business.get_header_request(header)
        lang = all_header.get('lang')
        _ = self.general_business.get_lang_instance(lang)
        data.update(all_header)


        if self.signal == 'payment_proceed':
            resp_status = status_code[401]
            resp_body = {
                "code": "api_unsuccess_payment",
                "title": _("API Unsuccessful payment"),
                "msg": _("The payment is unsuccessful")
            }
            try:
                raw_data = json.loads(req.stream.read(req.content_length or 0) or '{}')
                logger.info('mobileCard_deposit %s' % raw_data)
                header = req.headers  # key cua header luon viet hoa
                data.update({
                    'user_token': header.get('X-USER-TOKEN', False),
                })
                gs = self.general_business
                pm = self.payment_business
                access_model = self.access_model
                res_user = gs.validate_user_token(data.get('user_token'), data.get('client_id'))
                if raw_data.get('payment_type','scratch_card') == 'scratch_card':
                    paymentOptionCode = raw_data.get('paymentOptionCode')
                    pin = raw_data.get('pin')
                    serial = raw_data.get('serial')
                    type = raw_data.get('operator')
                    gateway = raw_data.get('gateway','onepay')
                    if pin and serial and type and gateway:
                        merref = pm.gen_merref(str(res_user[1]), 'scratch_card', gateway)
                        user_wallet_ids = access_model.search('user_wallet',
                                                              [('user_account_id', '=', res_user[1])],
                                                              ['id'], limit=True)
                        if user_wallet_ids and not len(user_wallet_ids) > 0:
                            resp_body = {
                                "code": "no_wallet",
                                "title": _("Cannot find wallet"),
                                "msg": _("The payment is unsuccessful")
                            }
                            resp.status = resp_status
                            resp.body = json.dumps(resp_body)
                            logger.error('Cannot find wallet of this user: %s' % res_user[1])
                            return False
                        # data_send='access_key=%s&pin=%s&serial=%s&transRef=%s&type=%s'%(access_key,pin,serial,mer_ref,type)
                        # signature = pm.gen_signature(secret_key, data_send)
                        # data_url= data_send + '&signature=%s'%signature
                        # url = scratch_payment_link + data_url
                        url = pm.gen_url(access_key, pin, serial, merref, type, secret_key, scratch_payment_link)
                        response = requests.post(url, json=None)
                        if response.status_code == 200:
                            res = response.json()
                            # res = {u'status': u'00', u'description': u'Giao d\u1ecbch th\xe0nh c\xf4ng.', u'amount': 20000, u'transRef': u'onepay_scratch_card_eecd69e69b2b855f4a8412434a557143_481', u'serial': u'36308200178872', u'transId': u'0020480074'}
                            if res and res.has_key('status'):
                                logger.info('Gateway Scratch-card response: %s' %res)
                                if res.get('status') == '00':
                                    resp_status = status_code[200]
                                    resp_body = {
                                        "title": _("Successful"),
                                        "msg": _("Sucessful. Please check your wallet."),
                                        "code": "successful_payment"
                                    }
                                    record = pm.create_scratch_card_transaction(res.get('amount'), res.get('transRef'),
                                                                       res.get('serial'), 'onepay',
                                                                       user_wallet_ids[0][0],
                                                                       res.get('transId'), currency='vnd')
                                    resp_status = status_code[record['status_code']]
                                    resp_body = record['resq_body']
                                    logger.info('Successful Payment Gateway Scratch-card: %s' %res.get('description'))
                                else:
                                    resp_body = {
                                        "title": _("Unsuccessful payment"),
                                        # "msg": res.get('description'),
                                        "msg": _("The card is not valid"),
                                        "code": "fail_payment"
                                    }
                                    logger.error('Unsuccessful Payment Gateway Scratch-card: %s' %
                                                 res.get('description'))
                                    client.captureException()
                    else:
                        logger.error('Front-end wrong param Scratch-card')
            except Exception as e:
                logger.error('Exception API Scratch-card: %s' %e)
                client.captureException()
            resp.status = resp_status
            resp.body = json.dumps(resp_body)

        elif self.signal == 'dd_card_proceed':
            resp_status = status_code[401]
            resp_body = {
                "code": "unsuccess_payment",
                "title": _("Unsuccessful payment"),
                "msg": _("The payment is unsuccessful")
            }
            try:
                raw_data = json.loads(req.stream.read(req.content_length or 0) or '{}')
                logger.info('dd_card_deposit %s', raw_data)
                header = req.headers  # key cua header luon viet hoa
                data.update({
                    'user_token': header.get('X-USER-TOKEN', False),
                })
                gs = self.general_business
                pm = self.payment_business
                access_model = self.access_model
                res_user = gs.validate_user_token(data.get('user_token'),data.get('client_id'))
                if res_user and len(res_user) > 0:
                    if raw_data.get('payment_type','drdr_card') == 'drdr_card':
                        qr_code = raw_data.get('qrCode', False)
                        user_ip = raw_data.get('user_ip', False)
                        gateway = raw_data.get('gateway','dd_card')
                        if qr_code:
                            user_wallet_ids = access_model.search('user_wallet',
                                                                  [('user_account_id', '=', res_user[1])],
                                                                  ['id'], limit=True)
                            if user_wallet_ids and len(user_wallet_ids) > 0:
                                url = ddcard_config['qr_code_add']
                                drdr_card_api = ddcard_config['drdr_card_api']
                                param={
                                    'qr_code':qr_code,
                                    'user_ip': user_ip,
                                    'api_key': drdr_card_api,
                                    'user_id': res_user[1]
                                }
                                # response = requests.get(url, param=param,json=None)
                                param='?api_key=%s&qr_code=%s&user_id=%s'%(drdr_card_api, qr_code, res_user[1])
                                gen_url= url + param
                                print gen_url
                                logger.info('dd_card_deposit %s', gen_url)
                                response = requests.get(gen_url)
                                if response:
                                    print 'response ',response
                                    logger.info('dd_card_deposit %s', response)
                                    res=response.json()
                                    print 'res ',res
                                    if res.get('code')== 200:
                                        res_data=res.get('data', False)
                                        data={
                                            'amount': res_data.get('amount'),
                                            'serial': res_data.get('serial'),
                                            'currency': res_data.get('currency'),
                                            'qr_code': qr_code,
                                            'dest_wallet':user_wallet_ids[0][0]
                                        }
                                        record = pm.create_drdr_card_transaction(data)
                                        if record:
                                            resp_status = status_code[record['status_code']]
                                            resp_body = record['resq_body']
                                else:
                                    print ('else response %s',response)
                                    res = response
                                    print 'else res ', res
                                    resp_status = status_code[401]
                                    resp_body = {
                                        "code": "payment_fail",
                                        "title": _(res.get('title', False) or 'The payment is unsuccessful'),
                                        "msg": _(res.get('description', False) or 'The payment is unsuccessful')
                                    }
                            else:
                                resp_body = {
                                    "code": "no_wallet",
                                    "title": _("Cannot find wallet"),
                                    "msg": _("The payment is unsuccessful")
                                }
                                resp_status=status_code[401]
                                logger.error('Cannot find wallet of this user: %s' % res_user[1])
                else:
                    resp_body = {
                        "code": "no_user",
                        "title": _("Cannot find this user"),
                        "msg": _("The payment is unsuccessful")
                    }
                    resp_status = status_code[401]
                    logger.error('Cannot find this user')
            except Exception as e:
                logger.error('error_dd_card_deposit %s', e)
                client.captureException()
            resp.status = resp_status
            resp.body = json.dumps(resp_body)

        elif self.signal == 'payment_paypal_deposit':
            resp_status = status_code[401]
            resp_body = {
                "code": "unsuccess_payment",
                "title": _("Unsuccessful payment"),
                "msg": _("The payment is unsuccessful")
            }
            try:
                header = req.headers #key cua header luon viet hoa
                raw_data = json.loads(req.stream.read(req.content_length or 0) or '{}')
                amount = raw_data.get('amount', 0)
                currencyUnitCode = raw_data.get('currencyUnitCode', 'USD')
                data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'amount': amount,
                        'currencyUnitCode': currencyUnitCode
                        })
                record = self.payment_business.payment_paypal_deposit(data)
                if record:
                    resp_status = status_code[record['status_code']]
                    resp_body = record['resq_body']
            except Exception as e:
                client.captureException()

            resp.status = resp_status
            resp.body = json.dumps(resp_body)

        elif self.signal == 'payment_stripe_create_customer':
            resp_status = status_code[401]
            resp_body = {
                "code": "unsuccess_create_customer",
                "title": _("Unsuccessful creating customer"),
                "msg": _("Creating customer is unsuccessful")
            }
            try:
                header = req.headers #key cua header luon viet hoa
                raw_data=eval(req.stream.read(req.content_length or 0) or '{}')
                card_token = raw_data.get('cardToken', False)
                data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'card_token': card_token
                        })
                record = self.payment_business.payment_stripe_create_customer(data)
                if record:
                    resp_status = status_code[record['status_code']]
                    resp_body = record['resq_body']
            except Exception as e:
                print str(e)
            resp.status = resp_status
            resp.body = json.dumps(resp_body)

        elif self.signal == 'payment_stripe_update_card_customer':
            resp_status = status_code[401]
            resp_body = {
                "code": "unsuccess_payment",
                "title": _("Unsuccessful payment"),
                "msg": _("Updating card is unsuccessful")
            }
            try:
                header = req.headers  # key cua header luon viet hoa
                raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                card_token = raw_data.get('cardToken', False)
                data.update({
                    'user_token': header.get("X-USER-TOKEN", False),
                    'card_token': card_token,
                    'lang': lang
                })
                record = self.payment_business.payment_stripe_update_card_customer(data)
                if record:
                    resp_status = status_code[record['status_code']]
                    resp_body = record['resq_body']
            except Exception as e:
                print str(e)
            resp.status = resp_status
            resp.body = json.dumps(resp_body)

        elif self.signal == 'payment_stripe_deposit':
            resp_status = status_code[401]
            resp_body = {
                "code": "unsuccess_payment",
                "title": _("Unsuccessful payment"),
                "msg": _("The payment is unsuccessful")
            }
            try:
                header = req.headers  # key cua header luon viet hoa
                raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                amount = raw_data.get('amount', False)
                currency = raw_data.get('currencyUnitCode', False)
                card_id= raw_data.get('cardId', False)
                userId= raw_data.get('userId', False)
                data.update({
                    'user_token': header.get("X-USER-TOKEN", False),
                    'amount': amount,
                    'currency': currency,
                    'card_id': card_id,
                    'user_id': userId
                })
                record = self.payment_business.payment_stripe_deposit(data)
                if record:
                    resp_status = status_code[record['status_code']]
                    resp_body = record['resq_body']
            except Exception as e:
                print str(e)
            resp.status = resp_status
            resp.body = json.dumps(resp_body)

        elif self.signal == 'payment_stripe_add_card':
            resp_status = status_code[401]
            resp_body = {
                "code": "unsuccess_payment",
                "title": _("Unsuccessful add card"),
                "msg": _("Cannot add card")
            }
            try:
                header = req.headers  # key cua header luon viet hoa
                raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                card_token = raw_data.get('cardToken', False)
                data.update({
                    'user_token': header.get("X-USER-TOKEN", False),
                    'card_token': card_token,
                    'lang': lang
                })
                record = self.payment_business.payment_stripe_add_card(data)
                if record:
                    resp_status = status_code[record['status_code']]
                    resp_body = record['resq_body']
            except Exception as e:
                print str(e)
            resp.status = resp_status
            resp.body = json.dumps(resp_body)

        elif self.signal == 'payment_stripe_remove_card':
            resp_status = status_code[401]
            resp_body = {
                "code": "unsuccess_payment",
                "title": _("Unsuccessful add card"),
                "msg": _("Cannot add card")
            }
            try:
                header = req.headers  # key cua header luon viet hoa
                raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                card_id = raw_data.get('id', False)
                data.update({
                    'user_token': header.get("X-USER-TOKEN", False),
                    'card_id': card_id,
                    'lang': lang
                })
                record = self.payment_business.payment_stripe_remove_card(data)
                if record:
                    resp_status = status_code[record['status_code']]
                    resp_body = record['resq_body']
            except Exception as e:
                print str(e)
            resp.status = resp_status
            resp.body = json.dumps(resp_body)


