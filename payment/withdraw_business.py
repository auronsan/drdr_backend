#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime
import logging

logger = logging.getLogger('drdr_api')
import sys
import falcon
import ConfigParser
import os

project_path =  str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
config = ConfigParser.RawConfigParser(allow_no_value=True)
config.read(project_path + '/' + "environment.conf")

sys.path.insert(0, "..")
try:
    from general import general_business
    from model.business_transfer_transaction import business_transfer_transaction
    from model.access_model import access_model
except ImportError as e:
    print('No Import ', e)

status_code = {
    200: falcon.HTTP_200,
    401: falcon.HTTP_401,
    502: falcon.HTTP_502,
    203: falcon.HTTP_203
}
drdr_scratch_card_wallet=1
class withdraw_business(object):
    def __init__(self, erp_connection, var_contructor):
        self.pool_conn = erp_connection

        self.ERP_DB_HOST = var_contructor['ERP_DB_HOST']
        self.ERP_DB_PORT = var_contructor['ERP_DB_PORT']
        self.ERP_DB_USER = var_contructor['ERP_DB_USER']
        self.ERP_DB_PASS = var_contructor['ERP_DB_PASS']
        self.ERP_DB_NAME = var_contructor['ERP_DB_NAME']

        self.general_business = general_business(erp_connection, var_contructor)
        self.business_transfer = business_transfer_transaction(erp_connection, var_contructor)
        self.access_model = access_model(erp_connection, var_contructor)
        self.drdr_wallet = int(config.get('environment', 'drdr_wallet_id')) or 1

    def create_withdraw_trans(self, data):
        try:
            access_model=self.access_model
            bt = self.business_transfer
            amount = data.get('amount', False)
            currency = data.get('currencyUnitCode', False)
            bank_account = data.get('bankAccountId', False)
            user_id = data.get('user_id', False)
            if user_id and amount and currency and bank_account:
                vals = {}
                vals.update({
                    'user_account_id': user_id,
                    'amount': amount,
                    'currency_unit': currency,
                    'account_bank_id': bank_account
                })
                create_tran=access_model.create('withdraw_transaction', vals)
                wallet_id=bt.get_wallet(data.get('doctorId'))
                wallet_info = access_model.search('user_wallet',
                                                 [('user_account_id', '=', user_id)],
                                                 ['id','available_balance'])
                if wallet_info and len(wallet_info) > 0:
                    wallet_id =  wallet_info[0][0]
                    available_balance = wallet_info[0][1]
                if available_balance < amount:
                    logger.error('not enough available balance')
                    return False
                else:
                    trans_id = bt.action_create_out(wallet_id,self.drdr_wallet, amount,create_tran,\
                                                    state='confirmed', currency=currency)
                    if len(trans_id)>0:
                        logger.info('create_withdraw_trans Successful: %s'%trans_id)
                        return trans_id[0]
            else:
                logger.error('create_payment_trans error: Wrong input data')
            logger.error('create_payment_trans error: Cannot create Payment')
            return False
        except Exception as e:
            logger.error('create_withdraw_trans error: %s' % str(e))
