import falcon
import json
import sys
import os
import math
from datetime import datetime
import ConfigParser
from profile_doctor_business import profile_doctor_business

project_path =  str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
config = ConfigParser.RawConfigParser(allow_no_value=True)
config.read(project_path + '/' + "environment.conf")
sys.path.insert(0, "..")
try:
    from general import general_business
    from aws_management import aws_management
except ImportError as e:
    print('No Import ', e)

AWS_BUCKET = config.get('environment', 's3_bucket')
BUCKET_URL_DEFAULT = AWS_BUCKET + '.' + config.get('environment', 's3_address')

status_code = {
    200: falcon.HTTP_200,
    401: falcon.HTTP_401,
    502: falcon.HTTP_502,
    203: falcon.HTTP_203
}


class profile_doctor_api(object):
    def __init__(self, signal, erp_connection=False, var_contructor=False):
        self.business = profile_doctor_business(signal, erp_connection, var_contructor)
        self.pool_conn = erp_connection
        self.aws = aws_management()

        if var_contructor:
            self.ERP_DB_HOST = var_contructor['ERP_DB_HOST']
            self.ERP_DB_PORT = var_contructor['ERP_DB_PORT']
            self.ERP_DB_USER = var_contructor['ERP_DB_USER']
            self.ERP_DB_PASS = var_contructor['ERP_DB_PASS']
            self.ERP_DB_NAME = var_contructor['ERP_DB_NAME']

        self.signal = signal
        self.general_business = general_business(erp_connection, var_contructor)

    def on_get(self, req, resp, doctor_id=None, patient_id=None, profile_id=None):
        try:
            data = {}
            header = req.headers  # key cua header luon viet hoa
            all_header = self.general_business.get_header_request(header)
            lang = all_header.get('lang')
            _ = self.general_business.get_lang_instance(lang)
            data.update(all_header)

            if self.signal == 'profile_doctor_get':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Get profile doctor failed"),
                    "msg": _("An error occurred. Please try to get profile again."),
                    "code": "get_profile_doctor_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    record = self.business.profile_doctor_get(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'get_list_patient_interacted':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Get list interacted patient"),
                    "msg": _("An error occurred. Please try to get patient list again."),
                    "code": "get_list_patient_interacted"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    searchKeyword = req.get_param('searchKeyword', default=False)
                    sortBy = req.get_param('sortBy', default='newest')
                    requestCount = req.get_param('requestCount', default=10)
                    nextItemId = req.get_param('nextItemId', default=0)

                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        "searchKeyword": searchKeyword,
                        "sortBy": sortBy,
                        "requestCount": requestCount,
                        "nextItemId": nextItemId,
                        'lang': lang,
                    })
                    record = self.business.get_list_patient_interacted(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # WD3.11 API for doctor to listing his documents
            elif self.signal == 'profile_doctor_get_documents':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Get list documents failed"),
                    "msg": _("An error occurred. Please try to request again."),
                    "code": "get_list_documents_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    record = self.business.profile_doctor_get_documents(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'profile_reviews_avgRate_get':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Get average rating point failed"),
                    "msg": _("An error occurred. Please try to request again."),
                    "code": "get_average_rating_point_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa

                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    record = self.business.profile_reviews_avgRate_get(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'admin_get_bank_account':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Doctor get bank account"),
                    "msg": _("An error occurred. Please try to set again."),
                    "code": "doctor_get_bank_account_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa

                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'profile_id': profile_id,
                        'lang': lang,
                    })
                    record = self.business.profile_doctor_get_bank_account(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'profile_reviews_get':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Get profile reviews failed"),
                    "msg": _("An error occurred. Please try to request again."),
                    "code": "get_profile_reviews_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    requestCount = req.get_param('requestCount', default=10)
                    nextItemId = req.get_param('nextItemId', default=0)

                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'requestCount': requestCount,
                        'nextItemId': nextItemId,
                        'lang': lang,
                    })
                    record = self.business.profile_reviews_get(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'get_patient_profile_details_by_id':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Get patient profile details failed"),
                    "msg": _("An error occurred. Please try to get profile account again."),
                    "code": "get_patient_profile_details_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'patient_id': patient_id,
                        'lang': lang,
                    })
                    record = self.business.get_patient_profile_details_by_id(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'get_review_list_pagination':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Get review list failed"),
                    "msg": _("An error occurred. Please try to get review list again."),
                    "code": "get_review_list_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    record = []
                    requestCount = req.get_param('requestCount', default=10)
                    nextItemId = req.get_param('nextItemId', default=0)
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'doctor_id': doctor_id,
                        'requestCount': requestCount,
                        'nextItemId': nextItemId,
                        'lang': lang,
                    })
                    data.update(raw_data)
                    record = self.business.get_review_list_pagination(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # WD6.9 API for doctor to get the upcoming appointment list
            elif self.signal == 'profile_doc_get_appointment_upcominglist_of_patient':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Get appointment upcominglist failed"),
                    "msg": _("An error occurred. Please try to get doctor list again."),
                    "code": "get_appointment_upcominglist_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'patient_id': patient_id,
                        'lang': lang,
                    })
                    record = self.business.profile_doc_get_appointment_upcominglist_of_patient(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # WD6.10 API for doctor to get the previous appointment list
            elif self.signal == 'profile_doc_get_appointment_previouslist_of_patient':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Get appointment previouslist failed"),
                    "msg": _("An error occurred. Please try to get doctor list again."),
                    "code": "get_appointment_previouslist_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'patient_id': patient_id,
                        'lang': lang,
                    })
                    record = self.business.profile_doc_get_appointment_previouslist_of_patient(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # WD2.11 API for doctor to check online status
            elif self.signal == 'doctor_get_online_status':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Get doctor online status"),
                    "msg": _("An error occurred. Please try to set again."),
                    "code": "get_doctor_online_status"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa

                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    record = self.business.doctor_get_online_status(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # WD5.3 API for getting consultation notes in the past with specific patient
            elif self.signal == 'profile_doctor_get_patient_history':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Get patient history"),
                    "msg": _("An error occurred. Please try to set again."),
                    "code": "get_patient_history_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    requestCount = req.get_param('requestCount', default=10)
                    nextItemId = req.get_param('nextItemId', default=0)
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'patient_id': patient_id,
                        'requestCount': requestCount,
                        'nextItemId': nextItemId,
                        'lang': lang,
                    })
                    record = self.business.profile_doctor_get_patient_history(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            
            # WD8.10 API for doctor to get bank account list
            elif self.signal == 'profile_doctor_get_bank_account':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Doctor get bank account"),
                    "msg": _("An error occurred. Please try to set again."),
                    "code": "doctor_get_bank_account_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    record = self.business.profile_doctor_get_bank_account(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            
            # WD8.12 API for doctor to get payout list
            elif self.signal == 'profile_doctor_get_payout_list':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Doctor get payout list"),
                    "msg": _("An error occurred. Please try to set again."),
                    "code": "doctor_get_payout_list"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    requestCount = req.get_param('requestCount', default=10)
                    nextItemId = req.get_param('nextItemId', default=0)
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'patient_id': patient_id,
                        'requestCount': requestCount,
                        'nextItemId': nextItemId,
                        'lang': lang,
                    })

                    record = self.business.profile_doctor_get_payout_list(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

        except Exception as e:
            print str(e)

    def on_post(self, req, resp, setting_type=None,bank_id=None,user_id=None):
        try:
            data = {}
            header = req.headers  # key cua header luon viet hoa
            all_header = self.general_business.get_header_request(header)
            lang = all_header.get('lang')
            _ = self.general_business.get_lang_instance(lang)
            data.update(all_header)

            if self.signal == 'profile_doctor_setup':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Setup profile failed"),
                    "msg": _("An error occurred. Please try to setup profile again."),
                    "code": "setup_profile_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = json.loads(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'setting_type': setting_type,
                        'lang': lang,
                    })
                    data.update(raw_data)
                    record = self.business.profile_doctor_setup(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'profile_doctor_avatar_upload':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Upload avatar failed"),
                    "msg": _("An error occurred. Please try to setup again."),
                    "code": "upload_avatar_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    avatar = req.get_param('avatar', default=False)
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'avatar_url': '',
                        'lang': lang,
                    })
                    record = []
                    if type(avatar).__name__ == 'instance':
                        # Write image to local
                        user_objs = self.general_business.validate_user_token(header.get("X-USER-TOKEN", False),data.get('client_id'))
                        if user_objs and user_objs[7]:
                            aws = aws_management()
                            name, ext = os.path.splitext(avatar.filename)
                            if ext.lower() in ('.jpg', '.png', '.gif', '.jpeg'):
                                create_date = datetime.now().strftime('%Y%m%d%H%M%S')
                                filename = 'avatar_%s_%s_%s%s' % (user_objs[1], user_objs[7], create_date, ext)
                                aws.upload_obj(avatar.file, AWS_BUCKET, filename)
                                data.update({'avatar_url': 'https://' + BUCKET_URL_DEFAULT + '/' + filename})
                            else:
                                data.update({'avatar_url': 'wrong_file_type'})
                        # End Write image to local

                        record = self.business.profile_doctor_avatar_upload(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # WA3.2. API for admin to create payout transaction for specific doctor
            elif self.signal == 'admin_create_doctor_payout':
                gs = self.general_business
                pool = self.pool_conn
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Create doctor payout failed"),
                    "msg": _("An error occurred. Please try to request again."),
                    "code": "create_doctor_payout_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    doctorId = req.get_param('doctorId', default=False)
                    amount = req.get_param('amount', default=False)
                    currencyUnitCode = req.get_param('currencyUnitCode', default=False)
                    bankAccountId = req.get_param('bankAccountId', default=False)
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'bankAccountId': bankAccountId,
                        'amount': amount,
                        'doctorId': doctorId,
                        'currencyUnitCode': currencyUnitCode,
                        'lang': lang,
                    })
                    if header.get("X-USER-TOKEN", False):
                        res_token = gs.validate_user_token(header.get("X-USER-TOKEN", False),data.get('client_id'))
                        if not res_token:
                            resp_status = status_code[203]
                            resp_body = {
                                "code": "expired_token",
                                "title": _("Expired token"),
                                "msg": _("Your login-token is expired, please try to login again")
                            }
                            resp.status = resp_status
                            resp.body = json.dumps(resp_body)
                            return False

                    userId = res_token[1]
                    data.update({'user_id': userId,
                                 'list_file': []})

                    sql = '''select ur.id
                            from user_account ua
                                left join user_account_user_role uaur on uaur.user_account_id = ua.id
                                left join user_role ur on ur.id = uaur.user_role_id 
                            where ua.id = %s and ur.code = 'admin' '''%userId
                    check_role = gs.sql_execute(pool, sql)
                    if not check_role or (check_role and not check_role[0][0]):
                        resp_status = status_code[401]
                        resp_body = {
                            "code": "wrong_role",
                             "title": _("Login failed"),
                             "msg": _("Your account role is not admin")
                        }
                        resp.status = resp_status
                        resp.body = json.dumps(resp_body)
                        return False

                    list_file = []
                    count = 0
                    for i in range(1, 7):
                        document = req.get_param('file%s' % i, default=False)
                        if type(document).__name__ == 'instance':
                            def get_infomation_file(file):
                                f = file.file
                                file_size = os.fstat(f.fileno()).st_size
                                file_name, file_extension = os.path.splitext(file.filename)
                                return {
                                    'file_size': file_size,
                                    'file_name': file_name,
                                    'file_extension': file_extension.replace('.', '').lower()
                                }

                            file_info = get_infomation_file(document)
                            print 'file_info ',file_info
                            if file_info['file_extension'] in ('jpg', 'png', 'gif', 'jpeg', 'pdf'):
                                file_info.update({'binary': document.file})
                                list_file.append(file_info)
                                if long(file_info['file_size']) / math.pow(1024, 2) > 10:
                                    resp_status = status_code[401]
                                    resp_body = {
                                        "code": "invalid_file",
                                        "msg": _("The uploading file must be jpg/jpeg/png/gif/pdf and not larger than 10MB"),
                                        "title": _("Invalid file")
                                    }
                                    resp.status = resp_status
                                    resp.body = json.dumps(resp_body)
                                    return False
                                count += 1
                            else:
                                resp_status = status_code[401]
                                resp_body = {
                                    "code": "invalid_file",
                                    "msg": _("The uploading file must be jpg/jpeg/png/gif/pdf and not larger than 10MB"),
                                    "title": _("Invalid file")
                                }
                                resp.status = resp_status
                                resp.body = json.dumps(resp_body)
                                return False
                    if count > 0:
                        if count > 5:
                            resp_status = status_code[401]
                            resp_body = {
                                "code": "too_much_files",
                                "msg": _("The maximum number of files can be uploaded is 5"),
                                "title": _("To much files")
                            }
                            resp.status = resp_status
                            resp.body = json.dumps(resp_body)
                            return False
                        create_date = datetime.now().strftime('%Y%m%d%H%M%S')
                        i = 0
                        for file in list_file:
                            i += 1
                            filename = 'file_%s_%s_%s.%s' % (userId, i, create_date, file['file_extension'])
                            self.aws.upload_obj(file['binary'], AWS_BUCKET, filename)
                            data['list_file'].append({'url': 'https://' + BUCKET_URL_DEFAULT + '/' + filename,
                                                      'file_name': file['file_name'],
                                                      'file_extension': file['file_extension'],
                                                      'file_size': file['file_size'], })

                    record = self.business.admin_create_doctor_payout(data)
                    if record:
                        resp.status = status_code[record['status_code']]
                        resp.body = json.dumps(record['resq_body'])
                        return

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

                # WD3.9 API for doctor to remove his avatar
            elif self.signal == 'profile_doctor_avatar_remove':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Remove avatar failed"),
                    "msg": _("An error occurred. Please try to setup again."),
                    "code": "remove_avatar_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa

                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    record = self.business.profile_doctor_avatar_remove(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # WA3.3. API for admin to mark specific payout transaction as paid
            elif self.signal == 'admin_update_doctor_payout':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("update doctor payout failed"),
                    "msg": _("An error occurred. Please try to request again."),
                    "code": "update_doctor_payout_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    data.update(raw_data)
                    record = self.business.admin_update_doctor_payout(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # WA4.1. API for admin to activate specific doctor account
            elif self.signal == 'admin_active_user_account':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Active user account failed"),
                    "msg": _("An error occurred. Please try to request again."),
                    "code": "active_user_account_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                        'user_id': user_id
                    })
                    record = self.business.admin_active_user_account(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # WD6.1 API for doctor to set his single availability (no recurring)
            elif self.signal == 'patient_doctor_set_singleday_availability':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Set single day availability failed"),
                    "msg": _("An error occurred. Please try to set again."),
                    "code": "set_single_day_availability_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    data.update(raw_data)
                    record = self.business.patient_doctor_set_singleday_availability(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

                # WD6.2 API for doctor to set his daily-repeat availability
            elif self.signal == 'patient_doctor_repeat_day_availability':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Repeat day availability failed"),
                    "msg": _("An error occurred. Please try to set again."),
                    "code": "repeat_day_availability_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa

                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'startTime': req.get_param('startTime', default=None),
                        'endTime': req.get_param('endTime', default=None),
                        'date': req.get_param('date', default=None),
                        'dayRepeatFrequency': req.get_param('dayRepeatFrequency', default=None),
                        'lang': lang,
                    })
                    record = self.business.patient_doctor_set_singleday_availability(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # WD6.3 API for doctor to set his weekly-repeat availability
            elif self.signal == 'patient_doctor_repeat_week_availability':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Repeat week availability failed"),
                    "msg": _("An error occurred. Please try to set again."),
                    "code": "repeat_week_availability_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa

                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'startTime': req.get_param('startTime', default=None),
                        'endTime': req.get_param('endTime', default=None),
                        'date': req.get_param('date', default=None),
                        'lang': lang,
                    })
                    record = self.business.patient_doctor_set_singleday_availability(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

                # WD2.9 API for doctor to go online
            elif self.signal == 'doctor_go_online':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Set doctor online"),
                    "msg": _("An error occurred. Please try to set again."),
                    "code": "set_doctor_online"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa

                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    record = self.business.doctor_go_online(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # WD2.10 API for doctor to go offline
            elif self.signal == 'doctor_go_offline':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Set doctor offline"),
                    "msg": _("An error occurred. Please try to set again."),
                    "code": "set_doctor_offline"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa

                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    record = self.business.doctor_go_offline(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # WD2.15 API for checking phone existence
            elif self.signal == 'doctor_check_phone_existence':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Check phone existence"),
                    "msg": _("An error occurred. Please try to set again."),
                    "code": "doctor_check_phone_existence"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    data.update(raw_data)
                    record = self.business.doctor_check_phone_existence(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            
            # WD2.14 API for checking email existence
            elif self.signal == 'doctor_check_email_existence':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Check email existence"),
                    "msg": _("An error occurred. Please try to set again."),
                    "code": "doctor_check_email_existence"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    data.update(raw_data)
                    record = self.business.doctor_check_email_existence(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # WD5.4 API for doctor to add note and document of medical record field to specific patient
            elif self.signal == 'profile_doctor_add_note_document':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Doctor add note and document"),
                    "msg": _("An error occurred. Please try to set again."),
                    "code": "doctor_add_note_document"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    medicalRecordFieldCode = req.get_param('medicalRecordFieldCode', default=False)
                    patientId = req.get_param('patientId', default=False)
                    attachment = req.get_param('attachment', default=False)
                    note = req.get_param('note', default='')
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'patientId': patientId,
                        'medicalRecordFieldCode': medicalRecordFieldCode,
                        'note': note,
                        'file_info': {},
                        'lang': lang,
                    })
                    if medicalRecordFieldCode and patientId and type(attachment).__name__ in ('instance', 'bool'):
                        # Write image to local
                        if type(attachment).__name__ == 'instance':
                            def get_infomation_file(file):
                                f = file.file
                                file_size = os.fstat(f.fileno()).st_size
                                file_name, file_extension = os.path.splitext(file.filename)
                                return {
                                    'file_size': file_size,
                                    'file_name': file_name,
                                    'file_extension': file_extension.replace('.', '').lower()
                                }

                            user_objs = self.general_business.validate_user_token(header.get("X-USER-TOKEN", False),data.get('client_id'))
                            file_info = get_infomation_file(attachment)
                            data['file_info'] = file_info
                            if user_objs and user_objs[7]:
                                if file_info['file_extension'] in ('jpg', 'png', 'gif', 'jpeg', 'pdf', 'doc', 'docx'):
                                    filename = '%s_%s.%s' % (
                                    patientId, file_info['file_name'], file_info['file_extension'])
                                    self.aws.upload_obj(attachment.file, AWS_BUCKET, filename)
                                    data['file_info'].update({'url': 'https://' + BUCKET_URL_DEFAULT + '/' + filename,
                                                              'file_name': filename})
                                else:
                                    data['file_info'].update({'url': 'wrong_file_type'})
                                    # End Write image to local
                        record = self.business.doctor_add_note_document(data)
                        if record:
                            resp_status = status_code[record['status_code']]
                            resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # WD8.8 API for doctor to add bank account
            elif self.signal == 'profile_doctor_add_bank_account':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Doctor add bank account"),
                    "msg": _("An error occurred. Please try to set again."),
                    "code": "doctor_add_bank_account"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    data.update(raw_data)
                    record = self.business.profile_doctor_add_bank_account(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            
            # WD8.9 API for doctor to set specific bank account as default
            elif self.signal == 'profile_doctor_set_bank_account_default':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Doctor set bank account as default"),
                    "msg": _("An error occurred. Please try to set again."),
                    "code": "profile_doctor_set_bank_account_default"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'bankId':bank_id,
                        'lang': lang,
                    })
                    data.update(raw_data)
                    record = self.business.profile_doctor_set_bank_account_default(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # WD8.11 API for doctor to remove bank account
            elif self.signal == 'profile_doctor_remove_bank_account':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Doctor remove bank account"),
                    "msg": _("An error occurred. Please try to set again."),
                    "code": "doctor_remove_bank_account_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'bank_id':bank_id,
                        'lang': lang,
                    })
                    data.update(raw_data)
                    record = self.business.profile_doctor_remove_bank_account(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            
        except Exception as e:
            print str(e)
