# -*- coding: utf-8 -*-
from datetime import datetime
import time
import logging
from raven import Client
import ConfigParser
import sys
from profile_patient_business import profile_patient_business
from collections import OrderedDict
import os

project_path =  str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
config = ConfigParser.RawConfigParser(allow_no_value=True)
config.read(project_path + '/' + "environment.conf")
sentry_key = config.get('environment', 'sentry_key')
client = Client(sentry_key)
logger = logging.getLogger('drdr_api')
sys.path.insert(0, "..")
try:
    from general import general_business
    from payment.withdraw_business import withdraw_business
    from payment.model.business_transfer_transaction import business_transfer_transaction
except ImportError as e:
    print('No Import ', e)


class profile_doctor_business(object):
    def __init__(self, signal, erp_connection, var_contructor):
        self.pool_conn = erp_connection
        self.var_contructor = var_contructor
        self.signal = signal
        self.ERP_DB_HOST = var_contructor['ERP_DB_HOST']
        self.ERP_DB_PORT = var_contructor['ERP_DB_PORT']
        self.ERP_DB_USER = var_contructor['ERP_DB_USER']
        self.ERP_DB_PASS = var_contructor['ERP_DB_PASS']
        self.ERP_DB_NAME = var_contructor['ERP_DB_NAME']
        self.general_business = general_business(erp_connection, var_contructor)
        self.withdraw_business = withdraw_business(erp_connection, var_contructor)
        self.business_transfer = business_transfer_transaction(erp_connection, var_contructor)
        self.vi_ = self.general_business.vi_
        self.en_ = self.general_business.en_


    def profile_doctor_get(self, data):
        logger.info('profile_doctor_get %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get profile failed"),
                        "msg": _("An error occurred. Please try to get profile again."),
                        "code": "get_profile_failed"
                    }
                }
        user_token = data.get('user_token')
        try:
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[7]
                    data.update({'doctor_id': user_id, 'from_doctor': True})
                    patient_business = profile_patient_business(self.signal, self.pool_conn, self.var_contructor)
                    return patient_business.profile_patient_get_doctor(data)
                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
                    return resp
        except Exception as e:
            logger.error('error_profile_doctor_get %s' % str(e))
            client.captureException()

    def profile_reviews_avgRate_get(self, data):
        logger.info('profile_reviews_avgRate_get %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get average rating point failed"),
                        "msg": _("An error occurred. Please try to request again."),
                        "code": "get_average_rating_point_failed"
                    }
                }
        user_token = data.get('user_token')
        try:
            if user_token:
                gs = self.general_business
                pool = self.pool_conn
                res_token = gs.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    print user_id
                    sql = '''
                        select coalesce(sum_rating_point,0)/coalesce(count_rating_point,1)
                        from user_account ua
                        left join doctor_profile dp on dp.user_mngment_id = ua.user_mngment_id
                        where ua.id = %s'''
                    value = gs.sql_execute(pool, sql, para_values=[user_id])[0][0]
                    resp.update({'status_code': 200,
                                 'resq_body': {"averageRatingPoint": value}
                                 })
                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
        except Exception as e:
            logger.error('profile_reviews_avgRate_get %s' % str(e))
            client.captureException()
        return resp

    # WD3.7 API for getting review list of a doctor
    def profile_reviews_get(self, data):
        logger.info('profile_reviews_get %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {
            'status_code': 401,
            'resq_body': {
                "title": _("Get profile reviews failed"),
                "msg": _("An error occurred. Please try to request again."),
                "code": "get_profile_reviews_failed"
            }
        }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            requestCount = int(data.get('requestCount'))
            nextItemId = int(data.get('nextItemId'))

            res = {
                'pagination': {
                    "resultCount": 0,
                    "totalCount": 0,
                    "nextItemId": 0
                },
                'list': []
            }
            res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
            start, end = 0, 0
            if res_token:
                patient_business = profile_patient_business(self.signal, self.pool_conn, self.var_contructor)
                user_id = res_token[1]
                data.update({'appointment_review': True,
                             'user_id': user_id})
                if not nextItemId or nextItemId == 0:
                    full_list_ids = patient_business.generate_pagination_list(user_id, 'appointment_review',
                                                                              context=data)
                    start, end = 0, requestCount
                else:
                    sql = '''select list_id 
                            from user_account_pagination
                            where user_account_id = %s
                                and table_pagination = '%s'
                            ''' % (user_id, 'appointment_review')
                    full_list_ids = gs.sql_execute(pool, sql)
                    if full_list_ids:
                        full_list_ids = eval(full_list_ids[0][0])
                        patient_business.update_pagination_list(user_id, 'appointment_review', full_list_ids,
                                                                context=data)
                        start, end = full_list_ids.index(nextItemId), full_list_ids.index(nextItemId) + requestCount
                    else:
                        full_list_ids = patient_business.generate_pagination_list(user_id, 'appointment_review',
                                                                                  context=data)
                        start, end = 0, requestCount
                if not full_list_ids:
                    resp.update({'status_code': 200,
                                 'resq_body': res
                                 })
                    return resp
                end = end if end <= len(full_list_ids) else len(full_list_ids)
                select_list_ids = full_list_ids[start:end]

                res['pagination']['totalCount'] = len(full_list_ids)
                res['pagination']['nextItemId'] = full_list_ids[end] if end < len(full_list_ids) else full_list_ids[
                    end - 1]
                res['pagination']['resultCount'] = len(select_list_ids)

                # get list_specialties
                if len(select_list_ids) > 0:
                    select_list_ids = select_list_ids + [-1, -1]
                    sql = '''select apmr.id, --0
                                    apmr.comment,--1
                                    apmr.rating_point,--2
                                    coalesce(apmr.write_date,apmr.create_date,now()),--3
                                    um.id profile_id,--4
                                    um.first_name,--5
                                    um.middle_name,--6
                                    um.last_name,--7
                                    um.profile_image_url,--8
                                    ua.phone,--9 uc.mobile_phone
                                    ua.phone,--10
                                    ua.id user_account_id--11
                            from appointment_review apmr
                                left join appointment apm on apm.id = apmr.appointment_id
                                left join user_account ua on ua.id = apm.patient_id
                                left join user_mngment um on um.id = ua.user_mngment_id
                                left join user_contact uc on uc.user_mngment_id = um.id
                            where apmr.id in %s
                            order by apmr.write_date desc''' % str(tuple(select_list_ids))
                    list_reviews = gs.sql_execute(pool, sql)
                    if list_reviews:
                        for item in list_reviews:
                            res['list'].append({
                                "id": item[0],
                                "comment": None if item[1] in ('False', 'None', '') else item[1],
                                "ratingPoint": item[2],
                                "latestUpdateTime": item[3] and int(time.mktime(item[3].timetuple())) or None,
                                "user": {
                                    "id": item[4],
                                    "userId": item[11],
                                    "firstName": None if item[5] in ('False', 'None', '') else item[5],
                                    "middleName": None if item[6] in ('False', 'None', '') else item[6],
                                    "lastName": None if item[7] in ('False', 'None', '') else item[7],
                                    "avatarUrl": None if item[8] in ('False', 'None', '') else item[8],
                                    "phoneNumber": None if item[10] in ('False', 'None', '') else item[10]
                                }
                            })
                        resp.update({'status_code': 200,
                                     'resq_body': res
                                     })
            else:
                resp.update({'status_code': 203,
                             'resq_body':
                                 {
                                     "code": "expired_token",
                                     "title": _("Expired token"),
                                     "msg": _("Your login-token is expired, please try to login again")
                                 }
                             })
        except Exception as e:
            logger.error('error_profile_reviews_get %s' % str(e))
            client.captureException()
        return resp

    # WD3.11 API for doctor to listing his documents
    def profile_doctor_get_documents(self, data):
        logger.info('profile_doctor_get_documents %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {
            'status_code': 401,
            'resq_body': {
                "title": _("Get list documents failed"),
                "msg": _("An error occurred. Please try to request again."),
                "code": "get_list_documents_failed"
            }
        }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
            documentList = []
            if res_token:
                user_id = res_token[1]
                sql = '''select id,  --0
                                file_name,--1
                                file_extension,--2
                                file_size,--3
                                url,--4
                                write_date--5 
                        from attachment 
                        where res_model = 'user_account' and res_id = %s'''%user_id
                list_data = gs.sql_execute(pool, sql)
                if list_data:
                    for line in list_data:
                        temp = {
                            "id": line[0],
                            "fileName": line[1],
                            "fileExtension": line[2],
                            "fileSize": gs.bytesToSize(line[3]),
                            "url": line[4],
                            "latestUpdateTime": int(time.mktime(line[5].timetuple()))
                        }
                        documentList.append(temp)
                resp = {
                    'status_code': 200,
                    'resq_body': {
                        "documentList": documentList,
                    }
                }
            else:
                resp.update({'status_code': 203,
                             'resq_body':
                                 {
                                     "code": "expired_token",
                                     "title": _("Expired token"),
                                     "msg": _("Your login-token is expired, please try to login again")
                                 }
                             })
        except Exception as e:
            logger.error('error_profile_doctor_get_documents %s' % str(e))
            client.captureException()
        return resp

    # WD5.1 API for getting the patients who have interacted with current user 
    def get_list_patient_interacted(self, data):
        logger.info('get_list_patient_interacted %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get list interacted patient"),
                        "msg": _("An error occurred. Please try to get patient list again."),
                        "code": "get_list_patient_interacted"
                    }
                }
        try:
            gs = self.general_business
            patient_business = profile_patient_business(self.signal, self.pool_conn, self.var_contructor)
            pool = self.pool_conn
            user_token = data.get('user_token')
            searchKeyword = data.get('searchKeyword')
            sortBy = data.get('sortBy')
            requestCount = int(data.get('requestCount'))
            nextItemId = int(data.get('nextItemId'))

            dicOrderBy = {'newest': '\norder by max_schedule_date asc',
                          'oldest': '\norder by max_schedule_date desc',
                          'lastname_asc': '\norder by last_name asc',
                          'lastname_desc': '\norder by last_name desc',
                          'visitcount_asc': '\norder by interacted_count asc',
                          'visitcount_desc': '\norder by interacted_count desc'}
            sortBy = dicOrderBy[sortBy]

            res = {
                'pagination': {
                    "resultCount": 0,
                    "totalCount": 0,
                    "nextItemId": 0
                },
                'list': [],
            }
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                start = 0
                end = 0
                if res_token:
                    user_id = res_token[1]
                    if not nextItemId or nextItemId == 0:
                        full_list_ids = patient_business.generate_pagination_list(user_id, 'appointment_patient',
                                                                                  context={'patient_interacted': True,
                                                                                           'sortBy': sortBy,
                                                                                           'searchKeyword': searchKeyword})
                        start, end = 0, requestCount
                    else:
                        sql = '''select list_id 
                                from user_account_pagination
                                where user_account_id = %s
                                    and table_pagination = '%s'
                                ''' % (user_id, 'appointment_patient')
                        full_list_ids = gs.sql_execute(pool, sql)
                        if full_list_ids:
                            full_list_ids = eval(full_list_ids[0][0])
                            for index in range(len(full_list_ids)):
                                if int(full_list_ids[index][0]) == nextItemId:
                                    start = index
                                    end = index + requestCount
                                    break
                        else:
                            full_list_ids = patient_business.generate_pagination_list(user_id, 'appointment_patient',
                                                                                      context={
                                                                                          'patient_interacted': True,
                                                                                          'sortBy': sortBy,
                                                                                          'searchKeyword': searchKeyword})
                            start, end = 0, requestCount
                    if not full_list_ids:
                        resp.update({'status_code': 200,
                                     'resq_body': res
                                     })
                        return resp

                    end = end if end <= len(full_list_ids) else len(full_list_ids)
                    select_list_ids = full_list_ids[start:end]

                    res['pagination']['totalCount'] = len(full_list_ids)
                    res['pagination']['nextItemId'] = int(full_list_ids[end][0]) if end < len(full_list_ids) else int(
                        full_list_ids[end - 1][0])
                    res['pagination']['resultCount'] = len(select_list_ids)

                    # get list_specialties
                    if len(select_list_ids) > 0:
                        res_select_list_ids = {}
                        list_ids = [-1, -1]
                        for item in select_list_ids:
                            list_ids.append(int(item[0]))
                            res_select_list_ids.update({int(item[0]): {
                                'schedule_date': item[1],
                                'count': item[2]
                            }})
                        sql = '''select  uac.id, --0
                                        um.id profile_id,--1
                                        COALESCE(um.title,'') title,--2
                                        COALESCE(um.first_name,'') first_name,--3
                                        COALESCE(um.middle_name,'') middle_name,--4
                                        COALESCE(um.last_name,'') last_name,--5
                                        number,--6
                                        street,--7
                                        city,--8
                                        state,--9
                                        postcode,--10
                                        country,--11
                                       COALESCE(um.profile_image_url,'') profile_image_url,--12
                                       uac.phone, -- 13
                                       um.gender, --14
                                       um.birthday --15                                       
                                from user_mngment um
                                left join user_account uac on uac.user_mngment_id = um.id
                                left join user_contact uc on uc.user_mngment_id = um.id
                                left join user_address ua on ua.user_contact_id = uc.id and is_primary = True
                                where um.id in %s
                                ''' % str(tuple(list_ids))

                        list_patient = gs.sql_execute(pool, sql)
                        if list_patient:
                            tmp_res = []
                            for pa_id in list_ids:
                                for pa in list_patient:
                                    if pa_id == int(pa[1]):
                                        tmp_res.append({
                                            "profileId": int(pa[1]),
                                            "avatarUrl": None if pa[12] in ('False', 'None', '') else pa[12],
                                            "firstName": None if pa[3] in ('False', 'None', '') else pa[3],
                                            "middleName": None if pa[4] in ('False', 'None', '') else pa[4],
                                            "lastName": None if pa[5] in ('False', 'None', '') else pa[5],
                                            "primaryAddress": {
                                                "number": None if pa[6] in ('False', 'None', '') else pa[6],
                                                "street": None if pa[7] in ('False', 'None', '') else pa[7],
                                                "cityCode": None if pa[8] in ('False', 'None', '') else pa[8],
                                                "state": None if pa[9] in ('False', 'None', '') else pa[9],
                                                "postCode": None if pa[10] in ('False', 'None', '') else pa[10],
                                                "countryCode": None if pa[11] in ('False', 'None', '') else pa[11],
                                            },
                                            "interactedCount": int(res_select_list_ids[pa[1]]['count']),
                                            "latestInteractedTime": res_select_list_ids[pa[1]]['schedule_date'],
                                            "phoneNumber": None if pa[13] in ('False', 'None', '') else pa[13],
                                            "genderCode": None if pa[14] in ('False', 'None', '') else pa[14],
                                            "dayOfBirth": pa[15] and int(time.mktime(pa[15].timetuple())) or None
                                        })
                                        break
                            res['list'] = tmp_res

                    resp.update({'status_code': 200,
                                 'resq_body': res
                                 })
                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
        except Exception as e:
            logger.error('error_get_list_patient_interacted %s' % str(e))
            client.captureException()

        return resp

    def profile_doctor_setup(self, data,context=None):
        logger.info('profile_doctor_setup %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Setup profile failed"),
                        "msg": _("An error occurred. Please try to setup profile again."),
                        "code": "setup_profile_failed"
                    }
                }
        try:
            if not context:
                context = {}
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')

            if user_token or 'no_token' in context:
                if 'no_token' in context:
                    res_token = [0,context['user_id'],0,0,0,0,0,context['new_profile_id']]
                else:
                    res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    user_mngmentid = res_token[7]
                    # update table user_mngment
                    if data.get('setting_type') == 'basic':
                        dayOfBirth = datetime.fromtimestamp(long(data['dayOfBirth'])).strftime('%Y-%m-%d %H:%M:%S') or None
                        sql_update = '''UPDATE user_mngment
                                        SET title           = %s, 
                                            first_name      = %s, 
                                            middle_name     = %s, 
                                            last_name       = %s, 
                                            gender          = %s,
                                            birthday        = %s,
                                            degree_title    = %s
                                        WHERE id =%s '''
                        gs.sql_execute(pool, sql_update, commit=True, para_values=(data.get('titleCode', ''),
                                                                                   data.get('firstName', ''),
                                                                                   data.get('middleName', ''),
                                                                                   data.get('lastName', ''),
                                                                                   data.get('genderCode', ''),
                                                                                   dayOfBirth,
                                                                                   data.get('degreeTitle', ''),
                                                                                   user_mngmentid))
                        # update table user_contact
                        sql = '''SELECT id FROM user_contact WHERE user_mngment_id ='%s' ''' % (user_mngmentid)
                        records_checkexist = gs.sql_execute(pool, sql)
                        if records_checkexist:
                            sql_update = '''UPDATE user_contact
                                            SET work_phone   = %s                                  
                                            WHERE user_mngment_id = %s
                                            RETURNING id'''
                            user_contactid = gs.sql_execute(pool, sql_update, commit=True, para_values=(
                            data['primaryPracticalAddress'].get('phone', ''), user_mngmentid))[0][0]
                        else:
                            sql_insert = '''INSERT INTO user_contact(work_phone,user_mngment_id) 
                                            VALUES (%s,%s) RETURNING id'''
                            user_contactid = gs.sql_execute(pool, sql_insert, commit=True, para_values=(
                            data['primaryPracticalAddress'].get('phone', ''), user_mngmentid))[0][0]

                        # update spokenLanguageCode
                        sql = '''SELECT dp.id,rel.spoken_language
                                FROM doctor_profile dp
                                  left join dr_doctorprofile_languages rel on rel.doctor_profile_id = dp.id
                                WHERE user_mngment_id ='%s' ''' % (user_mngmentid)
                        spoken_languages = gs.sql_execute(pool, sql)
                        doctor_id = spoken_languages[0][0]

                        cur_langs = []
                        new_langs = data.get('spokenLanguageCode', [])
                        for rc in spoken_languages:
                            if rc[1]:
                                cur_langs.append(rc[1])
                        sql_insert = '''insert into dr_doctorprofile_languages(doctor_profile_id,spoken_language) values %s'''
                        from sets import Set
                        insert_values = list(Set(new_langs).difference(Set(cur_langs)))
                        delete_values = list(Set(cur_langs).difference(Set(new_langs)))
                        values = []
                        for lag in insert_values:
                            values.append("(%s,'%s')" % (doctor_id, lag))
                        if values:
                            gs.sql_execute(pool, sql_insert % ','.join(values), commit=True)

                        sql_delete = '''delete from dr_doctorprofile_languages where doctor_profile_id = %s and spoken_language in %s''' % (
                        doctor_id, str(tuple(delete_values + ['x', 'y'])))
                        gs.sql_execute(pool, sql_delete, commit=True)

                        # update table user_address
                        sql = '''SELECT id FROM user_address WHERE user_contact_id ='%s' ''' % (user_contactid)
                        records_checkexist = gs.sql_execute(pool, sql)
                        if records_checkexist:
                            if 'primaryPracticalAddress' in data:
                                sql_update = '''UPDATE user_address
                                                SET number      = %s,
                                                    street      = %s,
                                                    city        = %s,
                                                    state       = %s,
                                                    postcode    = %s,
                                                    country     = %s,
                                                    is_primary  = 'true' 
                                                WHERE user_contact_id =%s '''
                            gs.sql_execute(pool, sql_update, commit=True,
                                           para_values=(data['primaryPracticalAddress'].get('number', ''),
                                                        data['primaryPracticalAddress'].get('street', ''),
                                                        data['primaryPracticalAddress'].get('cityCode', ''),
                                                        data['primaryPracticalAddress'].get('state', ''),
                                                        data['primaryPracticalAddress'].get('postcode', ''),
                                                        data['primaryPracticalAddress'].get('countryCode', ''),
                                                        user_contactid))
                            resp.update({'status_code': 200,
                                         'resq_body': {
                                             "code": "success",
                                             "title": _("Success"),
                                             "msg": _("Setup profile Successful.")
                                         }
                                         })

                            # insert user_address secondaryPracticalAddress
                            if 'secondaryPracticalAddress' in data:
                                sql = '''SELECT id FROM user_address WHERE user_contact_id ='%s' and is_primary = 'false' ''' % (user_contactid)
                                records_checkexist = gs.sql_execute(pool, sql)
                                if records_checkexist:
                                    sql_update = '''UPDATE user_address
                                                            SET number      = %s,
                                                                street      = %s,
                                                                city        = %s,
                                                                state       = %s,
                                                                postcode    = %s,
                                                                country     = %s,
                                                                is_primary  = 'false' 
                                                            WHERE id =%s '''
                                    gs.sql_execute(pool, sql_update, commit=True,
                                           para_values=(data['secondaryPracticalAddress'].get('number', ''),
                                                        data['secondaryPracticalAddress'].get('street', ''),
                                                        data['secondaryPracticalAddress'].get('cityCode', ''),
                                                        data['secondaryPracticalAddress'].get('state', ''),
                                                        data['secondaryPracticalAddress'].get('postcode', ''),
                                                        data['secondaryPracticalAddress'].get('countryCode', ''),
                                                        records_checkexist[0][0]))
                                else:
                                    sql_insert = '''INSERT INTO user_address(number,street,state,city,country,postcode,user_contact_id,is_primary)
                                                VALUES(%s,%s,%s,%s,%s,%s,%s,%s)'''
                                    records_checkexist = gs.sql_execute(pool, sql_insert, commit=True, para_values=(
                                                                        data['secondaryPracticalAddress'].get('number', ''),
                                                                        data['secondaryPracticalAddress'].get('street', ''),
                                                                        data['secondaryPracticalAddress'].get('cityCode', ''),
                                                                        data['secondaryPracticalAddress'].get('state', ''),
                                                                        data['secondaryPracticalAddress'].get('postcode', ''),
                                                                        data['secondaryPracticalAddress'].get('countryCode', ''),
                                                                        user_contactid,
                                                                        'false'))
                        else:
                            return resp
                    elif data.get('setting_type') == 'consultation':
                        consultationTypeCode = data.get('consultationTypeCode')
                        consultationDurationCode = data.get('consultationDurationCode')
                        fee_value = int(data['consultationFee'].get('value'))
                        fee_unit_code = data['consultationFee'].get('unitCode')
                        if fee_unit_code == 'vnd' and fee_value % 1000 != 0:
                            resp = {'status_code': 401,
                                    'resq_body': {
                                        "title": _("Invalid consultation fee"),
                                        "msg": _("The consulation fee must be greater than 1,000 and be mutiple of 1,000."),
                                        "code": "invalid_consultation_fee_vnd"
                                    }
                                    }
                            return resp
                        sql_update = '''UPDATE doctor_profile
                                    SET consultation_type      = %s,  
                                        consultation_duration  = %s,
                                        consultation_fee       = %s,
                                        consultation_unit      = %s
                                    WHERE user_mngment_id =%s '''
                        gs.sql_execute(pool, sql_update, commit=True, para_values=(
                        consultationTypeCode, consultationDurationCode, fee_value, fee_unit_code, user_mngmentid))
                        resp.update({'status_code': 200,
                                     'resq_body': {
                                         "code": "success",
                                         "title": _("Success"),
                                         "msg": _("Setup consultation successfully")
                                     }
                                     })
                    elif data.get('setting_type') == 'other':
                        graduationDate = data.get('graduationDate', False) and datetime.fromtimestamp(
                            long(data.get('graduationDate'))).strftime('%Y-%m-%d') or None
                        sql_update = '''UPDATE doctor_profile
                                    SET medical_school         = %s,
                                        graduation_date        =  %s,
                                        post_graduate_training = %s,
                                        industry_relationship  = %s,
                                        publications           = %s,
                                        teaching               = %s,
                                        community_service      = %s,
                                        criminal_convictions   = %s,
                                        limitations            = %s,
                                        hospital_restrictions  = %s,
                                        about                  = %s,
                                        professional_affiliations_and_activities = %s
                                    WHERE user_mngment_id =%s 
                                    RETURNING id '''
                        profile_doctorid = \
                        gs.sql_execute(pool, sql_update, commit=True, para_values=(data.get('medicalSchool', ''),
                                                                                   graduationDate,
                                                                                   data.get('postGraduateTraining', ''),
                                                                                   data.get('industryRelationship', ''),
                                                                                   data.get('publications', ''),
                                                                                   data.get('teaching', ''),
                                                                                   data.get('communityService', ''),
                                                                                   data.get('criminalConvictions', ''),
                                                                                   data.get('limitations', ''),
                                                                                   data.get('hospitalRestrictions', ''),
                                                                                   data.get('aboutMe', ''),
                                                                                   data.get(
                                                                                       'professionalAffiliationsAndActivities'),
                                                                                   user_mngmentid))[0][0]
                        for i in data['specialtyCodes']:
                            sql = '''SELECT distinct doctor_profile_id 
                                    FROM dr_doctorprofile_specialties 
                                    WHERE doctor_profile_id =%s 
                                    AND specialties_code = %s '''
                            records_specialties = gs.sql_execute(pool, sql, para_values=(profile_doctorid, i['code']))
                            if not records_specialties:
                                sql_insert = '''INSERT INTO dr_doctorprofile_specialties(doctor_profile_id,specialties_code,years_of_exp) VALUES('%s','%s','%s') 
                                                 ''' % (profile_doctorid, i['code'], i['yearsOfExp'])
                                gs.sql_execute(pool, sql_insert, commit=True)
                            else:
                                sql_update = '''UPDATE dr_doctorprofile_specialties 
                                                    SET years_of_exp = %s
                                                WHERE  doctor_profile_id =%s  AND specialties_code = %s '''
                                gs.sql_execute(pool, sql_update, commit=True,
                                               para_values=(i['yearsOfExp'], profile_doctorid, i['code']))
                        sql = '''SELECT doctor_profile_id FROM dr_license WHERE doctor_profile_id =%s '''
                        records_checkexist = gs.sql_execute(pool, sql, para_values=[user_mngmentid])
                        issueDate = data['license'].get('issueDate', False) and datetime.fromtimestamp(
                            long(data['license'].get('issueDate'))).strftime('%Y-%m-%d') or None
                        expiryDate = data['license'].get('expiryDate', False) and datetime.fromtimestamp(
                            long(data['license'].get('expiryDate'))).strftime('%Y-%m-%d') or None
                        if records_checkexist:
                            sql_update = '''UPDATE dr_license
                                        SET number         = %s,  
                                            license_status = %s,
                                            issue_date     = %s,
                                            expiry_date    = %s,
                                            issue_by       = %s
                                        WHERE doctor_profile_id =%s '''
                            gs.sql_execute(pool, sql_update, commit=True,
                                           para_values=(data['license'].get('number', ''),
                                                        data['license'].get('licenseStatus', ''),
                                                        issueDate,
                                                        expiryDate,
                                                        data['license'].get('issueBy', ''),
                                                        profile_doctorid))
                        else:
                            sql_insert = '''INSERT INTO dr_license(
                                        number,
                                        license_status,
                                        issue_date,
                                        expiry_date,
                                        issue_by,
                                        doctor_profile_id) VALUES(%s,%s,%s,%s,%s,%s)
                                        '''
                            gs.sql_execute(pool, sql_insert, commit=True,
                                           para_values=(data['license'].get('number', ''),
                                                        data['license'].get('licenseStatus', ''),
                                                        issueDate,
                                                        expiryDate,
                                                        data['license'].get('issueBy', ''),
                                                        profile_doctorid))

                        resp.update({'status_code': 200,
                                     'resq_body': {
                                         "code": "success",
                                         "title": _("Success"),
                                         "msg": _("Setup profile Successful.")
                                     }
                                     })
                    else:
                        return resp
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_profile_doctor_setup %s' % str(e))
            client.captureException()
        return resp

    # WA3.2. API for admin to create payout transaction for specific doctor
    def admin_create_doctor_payout(self, data):
        logger.info('admin_create_doctor_payout %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body': {
                    "title": _("Create doctor payout failed"),
                    "msg": _("An error occurred. Please try to request again."),
                    "code": "create_doctor_payout_failed"
                }}
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_id = data['user_id']
            create_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            payment_id = self.withdraw_business.create_withdraw_trans(data)
            if not payment_id:
                return resp
            for file in data['list_file']:
                sql_insert = '''insert into attachment(create_date,
                                                          write_date,
                                                          create_uid,
                                                          write_uid,
                                                          res_model,
                                                          res_id,
                                                          file_name,
                                                          file_extension,
                                                          file_size,
                                                          url)
                                    values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
                                    returning id
                                    '''
                gs.sql_execute(pool, sql_insert, commit=True, para_values=(create_date,
                                                                           create_date,
                                                                           user_id,
                                                                           user_id,
                                                                           'business_transfer_transaction',
                                                                           payment_id,
                                                                           file['file_name'],
                                                                           file['file_extension'],
                                                                           file['file_size'],
                                                                           file['url']))[0][0]
            resp.update({'status_code': 200,
                         'resq_body': {
                             "payoutId": payment_id
                         }
                         })

        except Exception as e:
            logger.error('error_admin_create_doctor_payout %s' % str(e))
            client.captureException()
        return resp


    def profile_doctor_avatar_upload(self, data):
        logger.info('profile_doctor_avatar_upload %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Upload avatar failed"),
                        "msg": _("An error occurred. Please try to setup again."),
                        "code": "upload_avatar_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    avatar_url = data.get('avatar_url')
                    if avatar_url == 'wrong_file_type':
                        return {'status_code': 401,
                                'resq_body':
                                    {
                                        "title": _("Wrong type of file"),
                                        "msg": _("An error occurred. Please try to setup profile again."),
                                        "code": "wrong_file_type"
                                    }
                                }
                    sql_update = '''update user_mngment set profile_image_url = %s where id = %s '''
                    gs.sql_execute(pool, sql_update, commit=True, para_values=(avatar_url, res_token[7]))

                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     'avatarUrl': avatar_url
                                 }
                                 })

                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }

        except Exception as e:
            logger.error('error_profile_doctor_avatar_upload %s' % str(e))
            client.captureException()
        return resp

    # WD3.9 API for doctor to remove his avatar
    def profile_doctor_avatar_remove(self, data):
        logger.info('profile_doctor_avatar_remove %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Remove avatar failed"),
                        "msg": _("An error occurred. Please try to setup again."),
                        "code": "remove_avatar_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]

                    sql_update = '''update user_mngment set profile_image_url = NULL where id = %s ''' % (res_token[7])
                    gs.sql_execute(pool, sql_update, commit=True)

                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     "code": "success",
                                     "title": _("Success"),
                                     "msg": _("Removed doctor avatar successfully.")
                                 }
                                 })

                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }

        except Exception as e:
            logger.error('error_profile_doctor_avatar_upload %s' % str(e))
            client.captureException()

        return resp

    # WA3.3. API for admin to mark specific payout transaction as paid
    def admin_update_doctor_payout(self, data):
        logger.info('admin_update_doctor_payout %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("update doctor payout failed"),
                        "msg": _("An error occurred. Please try to request again."),
                        "code": "update_doctor_payout_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            payoutId = data.get('payoutId')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    sql = '''select ur.id
                            from user_account ua
                                left join user_account_user_role uaur on uaur.user_account_id = ua.id
                                left join user_role ur on ur.id = uaur.user_role_id 
                            where ua.id = %s and ur.code = 'admin' ''' % user_id
                    check_role = gs.sql_execute(pool, sql)
                    if not check_role or (check_role and not check_role[0][0]):
                        resp = {'status_code': 401,
                                'resq_body':
                                    {
                                        "code": "wrong_role",
                                        "title": _("Login failed"),
                                        "msg": _("Your account role is not admin")
                                    }
                                }
                        return resp

                    res = self.business_transfer.action_paid(payoutId)
                    if res:
                        resp.update({'status_code': 200,
                                     'resq_body': {
                                         "code": "success",
                                         "title": _("Success"),
                                         "msg": _("Mark doctor payout transaction as paid successfully.")
                                     }})

                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }

        except Exception as e:
            logger.error('error_admin_update_doctor_payout %s' % str(e))
            client.captureException()

        return resp

    # WA4.1. API for admin to activate specific doctor account
    def admin_active_user_account(self, data):
        logger.info('admin_active_user_account %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Active user account failed"),
                        "msg": _("An error occurred. Please try to request again."),
                        "code": "active_user_account_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            user_id_inactive = data.get('user_id')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    sql = '''select ur.id
                            from user_account ua
                                left join user_account_user_role uaur on uaur.user_account_id = ua.id
                                left join user_role ur on ur.id = uaur.user_role_id 
                            where ua.id = %s and ur.code = 'admin' ''' % user_id
                    check_role = gs.sql_execute(pool, sql)
                    if not check_role or (check_role and not check_role[0][0]):
                        resp = {'status_code': 401,
                                'resq_body':
                                    {
                                        "code": "wrong_role",
                                        "title": _("Login failed"),
                                        "msg": _("Your account role is not admin")
                                    }
                                }
                        return resp

                    update_sql = '''update user_account set validation_status='active' where id = %s returning id'''
                    res = gs.sql_execute(pool, update_sql, commit=True, para_values=[user_id_inactive])
                    if res:
                        resp.update({'status_code': 200,
                                     'resq_body': {
                                         "code": "success",
                                         "title": _("Success"),
                                         "msg": _("Active user account successfully.")
                                     }})

                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }

        except Exception as e:
            logger.error('error_admin_active_user_account %s' % str(e))
            client.captureException()

        return resp

    def get_patient_profile_details_by_id(self, data):
        logger.info('profile_patient_get_by_id %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get patient profile details failed"),
                        "msg": _("An error occurred. Please try to get profile account again."),
                        "code": "get_patient_profile_details_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            patient_id = data.get('patient_id')

            res = {}
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    sql = '''select   title,--0
                                            profile_image_url,--1
                                            first_name,--2
                                            middle_name,--3
                                            last_name,--4
                                            complete_name,--5
                                            birthday,--6
                                            gender,--7
                                            ethnicity,--8
                                            highest_education_level,--9
                                            occupation,--10
                                            material_status,--11
                                            ua.email account_email,--12
                                            ua.phone account_phone,--13
                                            
                                            coalesce(is_primary, False) is_primary,--14
                                            home_phone,--15
                                            work_phone, --16
                                            ua.phone,--17 mobile_phone
                                            uc.email,--18
                                            number,--19
                                            street,--20
                                            city,--21
                                            state,--22
                                            postcode,--23
                                            country,--24
                                            spoken_language --25

                                   from user_mngment um
                                        left join user_account ua on ua.user_mngment_id = um.id
                                        left join patient_profile pp on  um.id = pp.user_mngment_id 
                                        left join user_contact uc on um.id = uc.user_mngment_id  
                                        left join user_address uad on uad.user_contact_id = uc.id  
                                   where um.id =%s ''' % patient_id
                    records = gs.sql_execute(pool, sql)

                    if records:
                        sql_interacted = '''SELECT uac.user_mngment_id patient_id,
                                                                        max(schedule_date) max_schedule_date,
                                                                        count(patient_id)
                                                                from appointment apm
                                                                left join user_account uac on uac.id = apm.patient_id
                                                                where doctor_id = %s and uac.user_mngment_id = %s
                                                              group by 1''' % (user_id, patient_id)
                        interacted = gs.sql_execute(pool, sql_interacted)
                        if interacted:
                            res.update({'interactedCount': int(interacted[0][2]),
                                        'latestInteractedTime': int(time.mktime(interacted[0][1].timetuple()))})
                        else:
                            res.update({'interactedCount': 0,
                                        'latestInteractedTime': None})
                        res.update({
                            'titleCode': None if records[0][0] in ('False', 'None', '') else records[0][0],
                            'avatar': None if records[0][1] in ('False', 'None', '') else records[0][1],
                            'firstName': None if records[0][2] in ('False', 'None', '') else records[0][2],
                            'middleName': None if records[0][3] in ('False', 'None', '') else records[0][3],
                            'lastName': None if records[0][4] in ('False', 'None', '') else records[0][4],
                            'completeName': None if records[0][5] in ('False', 'None', '') else records[0][0],
                            'dayOfBirth': records[0][6] and int(time.mktime(records[0][6].timetuple())) or None,
                            'genderCode': None if records[0][7] in ('False', 'None', '') else records[0][7],
                            'ethnicity': None if records[0][8] in ('False', 'None', '') else records[0][8],
                            'highestEducationLevelCode': None if records[0][9] in ('False', 'None', '') else records[0][
                                9],
                            'occupation': None if records[0][10] in ('False', 'None', '') else records[0][10],
                            #                                     'maritalStatusCode': None if records[0][11] in ('False','None','') else records[0][11],
                            'contact': {
                                'defaultPhone': None if records[0][13] in ('False', 'None', '') else records[0][13],
                                'homephone': None if records[0][15] in ('False', 'None', '') else records[0][15],
                                'workphone': None if records[0][16] in ('False', 'None', '') else records[0][16],
                                # 'mobilePhone': None if records[0][17] in ('False','None','') else records[0][17],
                                'email': None if records[0][18] in ('False', 'None', '') else records[0][18],
                                'primaryAddress': {"number": None,
                                                   "street": None,
                                                   "cityCode": None,
                                                   "state": None,
                                                   "postCode": None,
                                                   "countryCode": None},
                                'alternateAddress': {"number": None,
                                                     "street": None,
                                                     "cityCode": None,
                                                     "state": None,
                                                     "postCode": None,
                                                     "countryCode": None}
                            },
                            'languageCode': None if records[0][25] in ('False', 'None', '') else records[0][25],
                        })

                        for line in records:
                            if line[14] == True:

                                res['contact']['primaryAddress'].update({
                                    "number": None if line[19] in ('False', 'None', '') else line[19],
                                    "street": None if line[20] in ('False', 'None', '') else line[20],
                                    "cityCode": None if line[21] in ('False', 'None', '') else line[21],
                                    "state": None if line[22] in ('False', 'None', '') else line[22],
                                    "postCode": None if line[23] in ('False', 'None', '') else line[23],
                                    "countryCode": None if line[24] in ('False', 'None', '') else line[24],
                                })

                            else:

                                res['contact']['alternateAddress'].update({
                                    "number": None if line[19] in ('False', 'None', '') else line[19],
                                    "street": None if line[20] in ('False', 'None', '') else line[20],
                                    "cityCode": None if line[21] in ('False', 'None', '') else line[21],
                                    "state": None if line[22] in ('False', 'None', '') else line[22],
                                    "postCode": None if line[23] in ('False', 'None', '') else line[23],
                                    "countryCode": None if line[24] in ('False', 'None', '') else line[24],
                                })

                    resp.update({'status_code': 200,
                                 'resq_body': res
                                 })
                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
                return resp
        except Exception as e:
            logger.error('error_get_patient_profile_details_by_id %s' % str(e))
            client.captureException()

    def get_review_list_pagination(self, data):
        logger.info('get_review_list_pagination %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get review list failed"),
                        "msg": _("An error occurred. Please try to get review list again."),
                        "code": "get_review_list_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            patient_business = profile_patient_business(self.signal, self.pool_conn, self.var_contructor)
            requestCount = int(data.get('requestCount'))
            if (data.get('nextItemId')):
                nextItemId = int(data.get('nextItemId'))
            else:
                nextItemId = 0
            doctorId = data.get('doctor_id')
            res = {
                'pagination': {
                    "resultCount": 0,
                    "totalCount": 0,
                    "nextItemId": 0
                },
                'reviewList': [],
            }
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                start = 0
                end = 0
                if res_token:
                    user_id = res_token[1]
                    if not nextItemId or nextItemId == 0:
                        select_list_ids = []
                        full_list_ids = patient_business.generate_pagination_list(user_id, 'appointment_review',
                                                                                  context={'appointment_review': True,
                                                                                           'doctor_id': doctorId})
                        start, end = 0, requestCount
                    else:
                        sql = '''select list_id 
                                from user_account_pagination
                                where user_account_id = %s
                                    and table_pagination = '%s'
                                ''' % (user_id, 'appointment_review')
                        full_list_ids = gs.sql_execute(pool, sql)
                        if full_list_ids:
                            full_list_ids = eval(full_list_ids[0][0])
                            patient_business.update_pagination_list(user_id, 'appointment_review', full_list_ids,
                                                                    {'doctor_id': doctorId})
                            start, end = full_list_ids.index(nextItemId), full_list_ids.index(nextItemId) + requestCount
                        else:
                            full_list_ids = patient_business.generate_pagination_list(user_id, 'appointment_review',
                                                                                      context={'doctor_id': doctorId})
                            start, end = 0, requestCount

                    end = end if end <= len(full_list_ids) else len(full_list_ids)
                    select_list_ids = full_list_ids[start:end]
                    if len(select_list_ids) > 0:
                        res['pagination']['totalCount'] = len(full_list_ids)
                        res['pagination']['nextItemId'] = full_list_ids[end] if end < len(full_list_ids) else \
                        full_list_ids[end - 1]
                        res['pagination']['resultCount'] = len(select_list_ids)
                    else:
                        res['pagination']['totalCount'] = 0
                        res['pagination']['nextItemId'] = 0
                        res['pagination']['resultCount'] = 0
                    # get list_specialties
                    if len(select_list_ids) > 0:
                        select_list_ids = select_list_ids + [-1, -1]
                        sql = '''SELECT ar.id,--0
                                        ar.comment,--1
                                        ar.rating_point,--2
                                        ar.write_date,--3
                                        um.id,--4
                                        um.first_name,--5
                                        um.middle_name,--6
                                        um.last_name,--7
                                        um.profile_image_url--8
                                 FROM appointment ap INNER JOIN appointment_review ar ON ar.appointment_id = ap.id
                                                       INNER JOIN user_account ua ON ua.id = ap.patient_id
                                                       INNER JOIN user_mngment um on um.id = ua.user_mngment_id
                                WHERE ar.id in %s
                                ORDER BY ar.write_date DESC
                            ''' % str(tuple(select_list_ids))

                        list_review = gs.sql_execute(pool, sql)
                        if list_review:
                            tmp_res = []
                            for records in list_review:
                                tmp_res.append({
                                    "id": records[0],
                                    "comment": records[1],
                                    "ratingPoint": records[2],
                                    "latestUpdateTime": int(time.mktime(records[3].timetuple())),
                                    "user": {
                                        "id": records[4],
                                        "firstName": None if records[5] in ('False', 'None', '') else records[5],
                                        "middleName": None if records[6] in ('False', 'None', '') else records[6],
                                        "lastName": None if records[7] in ('False', 'None', '') else records[7],
                                        "avatarUrl": records[8]
                                    }
                                })

                            res['reviewList'] = tmp_res

                    resp.update({'status_code': 200,
                                 'resq_body': res
                                 })
                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
                return resp
        except Exception as e:
            logger.error('profile_patient_dr_list_byspecicalty %s' % str(e))
            client.captureException()
        return resp

    # WD6.9 API for doctor to get the upcoming appointment list
    def profile_doc_get_appointment_upcominglist_of_patient(self, data):
        logger.info('profile_doc_get_appointment_upcominglist_of_patient %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get appointment upcominglist failed"),
                        "msg": _("An error occurred. Please try to get doctor list again."),
                        "code": "get_appointment_upcominglist_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            patient_mngment_id = data.get('patient_id')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:

                    sql = '''select apm.id, --0
                                        apm.schedule_date, --1
                                        apm.state, --2
                                        apm.write_date --3
                                    from appointment apm
                                    left join user_account ua on apm.patient_id = ua.id
                                    left join user_mngment umng on umng.id = ua.user_mngment_id
                                    where apm.doctor_id = %s 
                                      and umng.id = %s 
                                      and (state in ('confirmed_by_patient','suspended')
                                      or (apm.type != 'live' and state='confirmed')
                                      or (apm.type = 'live' and state='confirmed' and COALESCE(is_accepted,False)=True))
                                    order by apm.write_date desc''' % (res_token[1], patient_mngment_id)

                    records = gs.sql_execute(pool, sql)
                    upcominglist = []
                    for line in records:
                        upcoming = {
                            'id': line[0],
                            'scheduleTime': int(time.mktime(line[1].timetuple())),
                            'stateCode': line[2]
                        }
                        if line[3]:
                            upcoming.update({
                                'latestUpdateTime': int(time.mktime(line[3].timetuple()))
                            })
                        else:
                            upcoming.update({
                                'latestUpdateTime': None
                            })
                        upcominglist.append(upcoming)

                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     'list': upcominglist
                                 }
                                 })
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_profile_doc_get_appointment_upcominglist_of_patient %s' % str(e))
            client.captureException()
        return resp

    # WD6.10 API for doctor to get the previous appointment list
    def profile_doc_get_appointment_previouslist_of_patient(self, data):
        logger.info('profile_doc_get_appointment_previouslist_of_patient %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get appointment previouslist failed"),
                        "msg": _("An error occurred. Please try to get doctor list again."),
                        "code": "get_appointment_previouslist_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            patient_mngment_id = data.get('patient_id')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:

                    sql = '''select apm.id, --0
                                        apm.schedule_date, --1
                                        apm.state, --2
                                        apm.write_date --3
                                    from appointment apm
                                    left join user_account ua on apm.patient_id = ua.id
                                    left join user_mngment umng on umng.id = ua.user_mngment_id
                                    where apm.doctor_id = %s and umng.id = %s and state in ('rescheduled_by_patient', 'successful_finished','free_finished','canceled_by_doctor','canceled_by_patient') 
                                    order by apm.write_date desc ''' % (res_token[1], patient_mngment_id)

                    records = gs.sql_execute(pool, sql)
                    previousList = []
                    for line in records:
                        previous = {
                            'id': line[0],
                            'scheduleTime': int(time.mktime(line[1].timetuple())),
                            'stateCode': line[2]
                        }
                        if line[3]:
                            previous.update({
                                'latestUpdateTime': int(time.mktime(line[3].timetuple()))
                            })
                        else:
                            previous.update({
                                'latestUpdateTime': None
                            })
                        previousList.append(previous)

                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     'list': previousList
                                 }
                                 })
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_profile_doc_get_appointment_previouslist_of_patient %s' % str(e))
            client.captureException()

        return resp

        # WD2.9 API for doctor to go online

    def doctor_go_online(self, data):
        logger.info('profile_doctor_go_online %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Set doctor online"),
                        "msg": _("An error occurred. Please try to set again."),
                        "code": "set_doctor_online"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:

                    sql_update = '''update user_account set activity_status = 'online' where id = %s''' % (res_token[1])

                    gs.sql_execute(pool, sql_update, True)

                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     "code": "success",
                                     "title": _("Success"),
                                     "msg": _("Set doctor online successfully.")
                                 }
                                 })
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_doctor_go_online %s' % str(e))
            client.captureException()

        return resp

    # WD2.10 API for doctor to go offline
    def doctor_go_offline(self, data):
        logger.info('profile_doctor_go_offline %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Set doctor offline"),
                        "msg": _("An error occurred. Please try to set again."),
                        "code": "set_doctor_offline"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:

                    sql_update = '''update user_account set activity_status = 'offline' where id = %s''' % (
                    res_token[1])

                    gs.sql_execute(pool, sql_update, True)

                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     "code": "success",
                                     "title": _("Success"),
                                     "msg": _("Set doctor offline successfully.")
                                 }
                                 })
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_doctor_go_offline %s' % str(e))
            client.captureException()

        return resp

    # WD2.11 API for doctor to check online status
    def doctor_get_online_status(self, data):
        logger.info('doctor_get_online_status %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get doctor online status"),
                        "msg": _("An error occurred. Please try to set again."),
                        "code": "get_doctor_online_status"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'),update_status=False)
                if res_token:

                    sql_update = '''select activity_status,dp.consultation_type, case when current_timestamp <= uc.checkout_time and uc.activity_status != 'offline' then True
                                        else False end onlineStatus from user_account uc 
                                      left join doctor_profile dp on dp.user_mngment_id= uc.user_mngment_id
                                    where uc.id = %s''' % (res_token[1])
                    record = gs.sql_execute(pool, sql_update)
                    
                    logger.info('doctor_get_online_status %s', record)
                    if record:
                        if record[0][2] == True:
                            resp.update({'status_code': 200,
                                         'resq_body': {
                                             "isOnline": record[0][2],
                                             "status": 'online',
                                             "consultationType": record[0][1]
                                         }
                                         })
                            return resp

                    resp.update({
                        'status_code': 200,
                        'resq_body': {
                            "isOnline": False,
                            "status": record[0][0],
                            "consultationType": record[0][1]
                        }
                    })
                    return resp
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_doctor_get_online_status %s' % str(e))
            client.captureException()

        return resp

    # WD5.3 API for getting consultation notes in the past with specific patient
    def profile_doctor_get_patient_history(self, data):
        logger.info('profile_doctor_get_patient_history %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get patient history"),
                        "msg": _("An error occurred. Please try to set again."),
                        "code": "get_patient_history_failed"
                    }
                }

        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            patient_business = profile_patient_business(self.signal, self.pool_conn, self.var_contructor)
            requestCount = int(data.get('requestCount'))
            nextItemId = int(data.get('nextItemId'))
            patient_id = int(data.get('patient_id'))
            res = {
                'pagination': {
                    "resultCount": 0,
                    "totalCount": 0,
                    "nextItemId": 0
                },
                'list': [],
            }

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    start, end = 0, 0
                    user_id = res_token[1]
                    if not nextItemId or nextItemId == 0:
                        full_list_ids = patient_business.generate_pagination_list(user_id, 'appointment_patient',
                                                                                  context={'patient_note_history': True,
                                                                                           'patient_id': patient_id})
                        start = 0
                    else:
                        sql = '''select list_id 
                                from user_account_pagination
                                where user_account_id = %s
                                    and table_pagination = '%s'
                                ''' % (user_id, 'appointment_patient')
                        full_list_ids = gs.sql_execute(pool, sql)
                        if full_list_ids:
                            full_list_ids = eval(full_list_ids[0][0])
                            start = full_list_ids.index(nextItemId)
                        else:
                            full_list_ids = patient_business.generate_pagination_list(user_id, 'appointment_patient',
                                                                                      context={
                                                                                          'patient_note_history': True,
                                                                                          'patient_id': patient_id})
                            start = 0

                    if not full_list_ids:
                        resp.update({'status_code': 200,
                                     'resq_body': res
                                     })
                        return resp

                    if (start + requestCount) <= len(full_list_ids):
                        end = start + requestCount
                    else:
                        end = len(full_list_ids)

                    select_list_ids = full_list_ids[start:end]
                    res['pagination']['totalCount'] = len(full_list_ids)
                    res['pagination']['nextItemId'] = full_list_ids[end] if end < len(full_list_ids) else full_list_ids[
                        end - 1]
                    res['pagination']['resultCount'] = len(select_list_ids)

                    # get list_specialties
                    if len(select_list_ids) > 0:
                        select_list_ids = select_list_ids + [-1, -1]
                        sql = '''select consultation_note,--0
                                        write_date --1
                                from appointment
                                where id in %s
                                order by write_date DESC''' % (str(tuple(select_list_ids)))

                        notes = gs.sql_execute(pool, sql)
                        tmp_note = []
                        if notes:
                            for note in notes:
                                tmp_note.append({
                                    "note": note[0],
                                    "latestUpdateTime": int(time.mktime(note[1].timetuple()))
                                })
                            res['list'] = tmp_note

                    resp.update({'status_code': 200,
                                 'resq_body': res
                                 })
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_profile_doctor_get_patient_history %s' % str(e))
            client.captureException()

        return resp

    # WD2.14 API for checking email existence
    def doctor_check_email_existence(self, data):
        logger.info('doctor_check_email_existence %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Check email existence"),
                        "msg": _("An error occurred. Please try to set again."),
                        "code": "doctor_check_email_existence"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            email = data.get('email')
            userId = data.get('userId',0)
            sql = '''select id, validation_status from user_account where email = %s '''
            records = gs.sql_execute(pool,sql,para_values=[email])
            if records:
                if records[0][0] != userId:
                    if records[0][1] == 'active' :
                        resp.update({'status_code': 401,
                                    'resq_body': {
                                        'code': 'exist_email',
                                        'isAccountActivated': True if records[0][1] == 'active' else False,
                                        'title': 'Register failed',
                                        "msg": _("The email already exists in our system, please try to register with another one")
                                     }
                                    })
                    else:
                        resp.update({'status_code': 401,
                                    'resq_body': {
                                        'code': 'exist_email',
                                        'isAccountActivated': True if records[0][1] == 'active' else False,
                                        'userId': records[0][0],
                                        'title': 'Register failed',
                                        "msg": _("The email already exists in our system, please try to register with another one")
                                      }
                                    })
                    return resp
                else:
                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     "code": "success",
                                     "title": _("Success"),
                                     "msg": _("The email exist in our system")
                                 }
                    })
                    return resp        

            resp.update({'status_code': 200,
                                 'resq_body': {
                                     "code": "success",
                                     "title": _("Success"),
                                     "msg": _("The email didn\'t exist in our system")
                                 }
            })

        except Exception as e:
            logger.error('error_doctor_check_email_existence %s' % str(e))
            client.captureException()

        return resp

    # WD2.15 API for checking phone existence
    def doctor_check_phone_existence(self, data):
        logger.info('doctor_check_phone_existence %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Check phone existence"),
                        "msg": _("An error occurred. Please try to set again."),
                        "code": "doctor_check_phone_existence"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            phone = data.get('phone')
            userId = data.get('userId',0)
            sql = '''select id, validation_status from user_account where phone = %s '''
            records = gs.sql_execute(pool,sql,para_values=[phone])
            if records:
                if records[0][0] != userId:
                    resp.update({'status_code': 401,
                                    'resq_body': {
                                        'code': 'exist_phone',
                                        "title": _("Register failed"),
                                        "isAccountActivated": True if records[0][1] == 'active' else False,
                                        "msg": _("The phone already exists in our system, please try to register with another one")
                                    }
                    })
                    return resp
                else:
                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     "code": "success",
                                     "title": _("Success"),
                                     "msg": _("The phone exist in our system")
                                 }
                    })
                    return resp  

            resp.update({'status_code': 200,
                                 'resq_body': {
                                     "code": "success",
                                     "title": _("Success"),
                                     "msg": _("The phone didn\'t exist in our system")
                                 }
            })

        except Exception as e:
            logger.error('error_doctor_check_phone_existence %s' % str(e))
            client.captureException()

        return resp

    # WD5.4 API for doctor to add note and document of medical record field to specific patient
    def doctor_add_note_document(self, data):
        logger.info('doctor_add_note_document %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Doctor add note and document"),
                        "msg": _("An error occurred. Please try to set again."),
                        "code": "doctor_add_note_document"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    if not data['note'] and not data['file_info']:
                        return resp
                    if data['file_info']:
                        if data['file_info']['url'] == 'wrong_file_type':
                            return {'status_code': 401,
                                    'resq_body':
                                        {
                                            "title": _("Wrong type of file"),
                                            "msg": _("An error occurred. Please try to setup profile again."),
                                            "code": "wrong_file_type"
                                        }
                                    }
                    user_id = res_token[1]
                    create_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    sql_insert_mr = '''insert into appointment_medical_record(create_date, write_date, user_mngment_id, code, note, create_uid)
                                       values (%s,%s,%s,%s,%s,%s) 
                                       returning ID'''
                    medical_record_id = gs.sql_execute(pool, sql_insert_mr, commit=True, para_values=(create_date,
                                                                                                      create_date,
                                                                                                      data['patientId'],
                                                                                                      data['medicalRecordFieldCode'],
                                                                                                      data['note'],
                                                                                                      user_id))[0][0]
                    if data['file_info']:
                        if data['file_info']['url'] == 'wrong_file_type':
                            return {'status_code': 401,
                                    'resq_body':
                                        {
                                            "title": _("Wrong type of file"),
                                            "msg": _("An error occurred. Please try to setup profile again."),
                                            "code": "wrong_file_type"
                                        }
                                    }
                        sql_insert = '''insert into attachment(create_date,
                                                              write_date,
                                                              create_uid,
                                                              write_uid,
                                                              res_model,
                                                              res_id,
                                                              description,
                                                              file_name,
                                                              file_extension,
                                                              file_size,
                                                              url)
                                        values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
                                        returning id
                                        '''
                        attachment_id = gs.sql_execute(pool, sql_insert, commit=True, para_values=(create_date,
                                                                                                   create_date,
                                                                                                   user_id,
                                                                                                   user_id,
                                                                                                   'user_mngment',
                                                                                                   medical_record_id,
                                                                                                   data[
                                                                                                       'medicalRecordFieldCode'],
                                                                                                   data['file_info'][
                                                                                                       'file_name'],
                                                                                                   data['file_info'][
                                                                                                       'file_extension'],
                                                                                                   data['file_info'][
                                                                                                       'file_size'],
                                                                                                   data['file_info'][
                                                                                                       'url'],
                                                                                                   ))[0][0]

                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     "code": "success",
                                     "title": _("Success"),
                                     "msg": _("Submit medical record successful.")
                                 }
                                 })

                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }

        except Exception as e:
            logger.error('error_medical_record_submit %s' % str(e))
            client.captureException()
        return resp

    # WD8.8 API for doctor to add bank account
    def profile_doctor_add_bank_account(self, data):
        logger.info('profile_doctor_add_bank_account %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Doctor add bank account"),
                        "msg": _("An error occurred. Please try to set again."),
                        "code": "doctor_add_bank_account"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')

            holderName = data.get('holderName')
            bankId = data.get('bankId')
            bankBranch = data.get('bankBranch')
            accountNumber = data.get('accountNumber')
            user_token = data.get('user_token')
            
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    sql_select = "select id from meta_bank where id = %s"
                    result = gs.sql_execute(pool, sql_select, commit=False, para_values=[bankId])
                    if not result:
                        resp.update({'status_code': 401,
                                    'resq_body': {
                                        'code': 'bank_not_exist',
                                        "title": _("Bank not exist"),
                                        "msg": _("The bank you add does not exist in system!")
                                    }
                                    })
                        return resp

                    sql_select = "select id from user_account_bank where account_number = %s"
                    result = gs.sql_execute(pool, sql_select, commit=False, para_values=[accountNumber])
                    if result:
                        resp.update({'status_code': 401,
                                    'resq_body': {
                                        'code': 'bank_account_exist',
                                        "title": _("Bank account exist"),
                                        "msg": _("The bank acount you add does exist in system!")
                                    }
                                    })
                        return resp

                    sql_insert = '''INSERT INTO user_account_bank(user_account_id, holder_name, account_number, bank_branch, meta_bank_id, is_primary)VALUES (%s, %s, %s, %s, %s, False) RETURNING id'''
                    bankAccountId = gs.sql_execute(pool, sql_insert, commit=True, para_values=[res_token[1],holderName,accountNumber,bankBranch,bankId])

                    if bankAccountId:
                        resp.update({'status_code': 200,
                                        'resq_body': {
                                            'bankAccountId': bankAccountId[0][0]
                                        }
                                        })
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }

        except Exception as e:
            logger.error('error_profile_doctor_add_bank_account %s' % str(e))
            client.captureException()
        return resp

    # WD8.9 API for doctor to set specific bank account as default
    def profile_doctor_set_bank_account_default(self, data):
        logger.info('profile_doctor_set_bank_account_default %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Doctor set bank account as default"),
                        "msg": _("An error occurred. Please try to set again."),
                        "code": "profile_doctor_set_bank_account_default"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            bankId = data.get('bankId')
            
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    
                    sql_update = '''
                        update user_account_bank set is_primary = False where user_account_id = %s;
                        update user_account_bank set is_primary = True where id = %s RETURNING 1
                    '''
                    bankAccountId = gs.sql_execute(pool, sql_update, commit=True, para_values=[res_token[1],bankId])

                    if bankAccountId:
                        resp.update({'status_code': 200,
                                        'resq_body': {
                                            "code": "success",
                                            "title": _("Success"),
                                            "msg": _("Set bank account as default successfully.")
                                        }
                                        })
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }

        except Exception as e:
            logger.error('error_profile_doctor_set_bank_account_default %s' % str(e))
            client.captureException()
        return resp

    def get_bank_account(self, user_id):
        gs = self.general_business
        pool = self.pool_conn
        resp = {}
        sql_select = '''select ucb.id, --0
                                    ucb.holder_name, --1
                                    ucb.account_number, --2
                                    mb.name, --3
                                    ucb.is_primary, --4
                                    ucb.bank_branch --5
                        from user_account_bank ucb
                        inner join meta_bank mb on mb.id = ucb.meta_bank_id
                        where user_account_id = %s '''
        results = gs.sql_execute(pool, sql_select, commit=False, para_values=(user_id,))
        bankList = []
        for line in results:
            bankList.append({
                "id": int(line[0]),
                "holderName": line[1],
                "accountNumber": line[2],
                "bankName": line[3],
                "isDefault": line[4],
                "bankBranch": line[5]
            })

        resp.update({'status_code': 200,
                     'resq_body': {
                         'list': bankList
                     }
                     })
        return resp

    # WD8.10 API for doctor to get bank account list
    def profile_doctor_get_bank_account(self, data):
        logger.info('profile_doctor_set_bank_account_default %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Doctor get bank account"),
                        "msg": _("An error occurred. Please try to set again."),
                        "code": "doctor_get_bank_account_failed"
                    }
                }
        try:
            user_token = data.get('user_token')
            profile_id = data.get('profile_id',False)

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    if profile_id:
                        gs = self.general_business
                        pool = self.pool_conn
                        sql = '''select id from user_account where user_mngment_id = %s'''
                        user_id = gs.sql_execute(pool, sql, commit=False, para_values=[profile_id])[0][0]

                    resp = self.get_bank_account(user_id)
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }

        except Exception as e:
            logger.error('error_profile_doctor_set_bank_account_default %s' % str(e))
            client.captureException()
        return resp

    # WD8.11 API for doctor to remove bank account
    def profile_doctor_remove_bank_account(self, data):
        logger.info('profile_doctor_remove_bank_account %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Doctor remove bank account"),
                        "msg": _("An error occurred. Please try to set again."),
                        "code": "doctor_remove_bank_account_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            bank_id = data.get('bank_id')
            
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    
                    sql_delete = '''delete from user_account_bank
                                    where id = %s returning 1 '''
                    result = gs.sql_execute(pool, sql_delete, commit=True,para_values=(bank_id,))
                    
                    if result:
                        resp.update({'status_code': 200,
                                        'resq_body': {
                                            "code": "success",
                                            "title": _("Success"),
                                            "msg": _("Delete your bank account successfully.")
                                        }
                                        })
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }

        except Exception as e:
            logger.error('error_profile_doctor_remove_bank_account %s' % str(e))
            client.captureException()
        return resp
    
    # WD8.12 API for doctor to get payout list
    def profile_doctor_get_payout_list(self, data):
        logger.info('profile_doctor_get_payout_list %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Doctor get payout list"),
                        "msg": _("An error occurred. Please try to set again."),
                        "code": "doctor_get_payout_list"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            patient_business = profile_patient_business(self.signal, self.pool_conn, self.var_contructor)
            requestCount = int(data.get('requestCount'))
            nextItemId = int(data.get('nextItemId'))
            res = {
                'pagination': {
                    "resultCount": 0,
                    "totalCount": 0,
                    "nextItemId": 0
                },
                'list': [],
            }

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    start, end = 0, 0
                    user_id = res_token[1]
                    if not nextItemId or nextItemId == 0:
                        full_list_ids = patient_business.generate_pagination_list(user_id, 'doctor_payout',
                                                                                  context={'api': 'WD8_12'})
                        start = 0
                    else:
                        sql = '''select list_id 
                                from user_account_pagination
                                where user_account_id = %s
                                    and table_pagination = '%s'
                                ''' % (user_id, 'doctor_payout')
                        full_list_ids = gs.sql_execute(pool, sql)
                        if full_list_ids:
                            full_list_ids = eval(full_list_ids[0][0])
                            start = full_list_ids.index(nextItemId)
                        else:
                            full_list_ids = patient_business.generate_pagination_list(user_id, 'doctor_payout',
                                                                                      context={
                                                                                          'api': 'WD8_12'})
                            start = 0

                    if not full_list_ids:
                        resp.update({'status_code': 200,
                                     'resq_body': res
                                     })
                        return resp

                    if (start + requestCount) <= len(full_list_ids):
                        end = start + requestCount
                    else:
                        end = len(full_list_ids)

                    select_list_ids = full_list_ids[start:end]
                    res['pagination']['totalCount'] = len(full_list_ids)
                    res['pagination']['nextItemId'] = full_list_ids[end] if end < len(full_list_ids) else full_list_ids[
                        end - 1]
                    res['pagination']['resultCount'] = len(select_list_ids)

                    # get list_specialties
                    if len(select_list_ids) > 0:
                        select_list_ids = select_list_ids + [-1, -1]
                        sql = '''select btt.withdraw_transaction_id, --0
                                        btt.created_date, --1
                                        btt.state, --2
                                        btt.amount, --3
                                        btt.currency, --4
                                        att.file_extension, --5
                                        att.file_size, --6
                                        att.write_date, --7
                                        att.url, -- 8
                                        att.file_name, --9
                                        att.create_uid, -- 10
                                        att.id, -- 11
                                        um.id, --12
                                        um.title,--13
                                        um.first_name,--14
                                        um.middle_name,--15
                                        um.last_name --16
                            from business_transfer_transaction btt
                                left join attachment att on att.res_id = btt.withdraw_transaction_id
                                left join user_account ua on ua.id = att.create_uid
                                left join user_mngment um on um.id = ua.user_mngment_id
                                
                            where btt.withdraw_transaction_id in %s
                            order by btt.created_date desc''' % str(tuple(select_list_ids))
                        records = gs.sql_execute(pool, sql)
                    
                        if records:
                            payout_list = OrderedDict()
                            for item in records:
                                if item[0] not in payout_list:
                                    payout_list.update({item[0]: {
                                        "id": item[0],
                                        "createdTime": item[1] and int(time.mktime(item[1].timetuple())) or None,
                                        "statusCode": item[2],
                                        "amount": {
                                            "value": item[3],
                                            "unitCode": item[4]
                                        },
                                        "attachmentList": []
                                    }})
                                payout_list[item[0]]['attachmentList'].append({
                                                                            "id": item[11],
                                                                            "url": item[8],
                                                                            "fileName": item[9],
                                                                            "fileSize": self.general_business.bytesToSize(item[6]),
                                                                            "fileExtension": item[5],
                                                                            "latestUpdateTime": item[7] and int(time.mktime(item[7].timetuple())) or None,
                                                                            "uploadUser": {
                                                                                    "id": item[12],
                                                                                    "titleCode": item[13],
                                                                                    "firstName": None if item[14] in ('False', 'None', '') else item[14],
                                                                                    "middleName": None if item[15] in ('False', 'None', '') else item[15],
                                                                                    "lastName": None if item[16] in ('False', 'None', '') else item[16]
                                                                                }

                                                                        })
                            res['list'] = payout_list.values()
                        

                    resp.update({'status_code': 200,
                                 'resq_body': res
                                 })
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_profile_doctor_get_payout_list %s' % str(e))
            client.captureException()

        return resp