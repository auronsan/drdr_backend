#!/usr/bin/env python
# -*- coding: utf-8 -*-
import falcon
import json
import sys
from datetime import datetime
from profile_patient_business import profile_patient_business
import ConfigParser
import os

project_path =  str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
config = ConfigParser.RawConfigParser(allow_no_value=True)
config.read(project_path + '/' + "environment.conf")
sys.path.insert(0, "..")
try:
    from general import general_business
    from aws_management import aws_management
except ImportError as e:
    print('No Import ', e)

AWS_BUCKET = config.get('environment', 's3_bucket')
BUCKET_URL_DEFAULT = AWS_BUCKET + '.' + config.get('environment', 's3_address')

status_code = {
    200: falcon.HTTP_200,
    401: falcon.HTTP_401,
    502: falcon.HTTP_502,
    203: falcon.HTTP_203
}


class profile_patient_api(object):
    def __init__(self, signal, erp_connection=False, var_contructor=False):
        self.business = profile_patient_business(signal, erp_connection, var_contructor)
        # self.aws = aws_management()
        self.pool_conn = erp_connection

        if var_contructor:
            self.ERP_DB_HOST = var_contructor['ERP_DB_HOST']
            self.ERP_DB_PORT = var_contructor['ERP_DB_PORT']
            self.ERP_DB_USER = var_contructor['ERP_DB_USER']
            self.ERP_DB_PASS = var_contructor['ERP_DB_PASS']
            self.ERP_DB_NAME = var_contructor['ERP_DB_NAME']

        self.signal = signal
        self.general_business = general_business(erp_connection, var_contructor)

    def on_get(self, req, resp, doctor_id=None):
        try:
            data = {}
            header = req.headers  # key cua header luon viet hoa
            all_header = self.general_business.get_header_request(header)
            lang = all_header.get('lang')
            _ = self.general_business.get_lang_instance(lang)
            data.update(all_header)

            if self.signal == 'profile_patient_get':
                resp_status = status_code[401]
                resp_body = {
                    "title": "Get profile patient failed",
                    "msg": "An error occurred. Please try to get profile again.",
                    "code": "get_profile_patient_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                    })
                    record = self.business.profile_patient_get(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'profile_patient_get_doctor':
                resp_status = status_code[401]
                resp_body = {
                    "title": "Get profile doctor failed",
                    "msg": "An error occurred. Please try to get profile again.",
                    "code": "get_profile_doctor_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'doctor_id': doctor_id
                    })
                    record = self.business.profile_patient_get_doctor(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            elif self.signal == 'profile_patient_create_token':
                resp_status = status_code[401]
                resp_body = {
                    "title": "Get profile doctor failed",
                    "msg": "An error occurred. Please try to get profile again.",
                    "code": "get_profile_doctor_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'doctor_id': doctor_id
                    })
                    record = self.business.user_create_token(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'profile_patient_get_dr_shortlist':
                resp_status = status_code[401]
                resp_body = {
                    "title": "Get doctor shortlist failed",
                    "msg": "An error occurred. Please try to get doctor shortlist again.",
                    "code": "get_doctor_shortlist_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                    })
                    record = self.business.profile_patient_get_dr_shortlist(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'profile_patient_dr_list_byspecicalty':
                resp_status = status_code[401]
                resp_body = {
                    "title": "Get doctor list failed",
                    "msg": "An error occurred. Please try to get doctor list again.",
                    "code": "get_doctor_list_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    specialtyCode = req.get_param('specialtyCode', default=False)
                    requestCount = req.get_param('requestCount', default=10)
                    nextItemId = req.get_param('nextItemId', default=0)

                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        "specialtyCode": specialtyCode,
                        "requestCount": requestCount,
                        "nextItemId": nextItemId,
                    })
                    record = self.business.profile_patient_dr_list_byspecicalty(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'profile_patient_get_appointment_upcominglist':
                resp_status = status_code[401]
                resp_body = {
                    "title": "Get appointment upcominglist failed",
                    "msg": "An error occurred. Please try to get doctor list again.",
                    "code": "get_appointment_upcominglist_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa

                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                    })
                    record = self.business.profile_patient_get_appointment_upcominglist(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'profile_patient_get_appointment_previouslist':
                resp_status = status_code[401]
                resp_body = {
                    "title": "Get appointment previouslist failed",
                    "msg": "An error occurred. Please try to get doctor list again.",
                    "code": "get_appointment_previouslist_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    requestCount = req.get_param('requestCount', default=10)
                    nextItemId = req.get_param('nextItemId', default=0)
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        "requestCount": requestCount,
                        "nextItemId": nextItemId,
                    })
                    record = self.business.profile_patient_get_appointment_previouslist(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'profile_patient_get_appointment_sumarylist':
                resp_status = status_code[401]
                resp_body = {
                    "title": "Get appointment sumarylist failed",
                    "msg": "An error occurred. Please try to get sumarylist list again.",
                    "code": "get_appointment_sumarylist_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa

                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                    })
                    record = self.business.profile_patient_get_appointment_sumarylist(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            # MP5.8 API for getting live doctor list (support pagination)
            elif self.signal == 'profile_patient_get_live_dr_list':
                resp_status = status_code[401]
                resp_body = {
                    "title": "Get live doctor list failed",
                    "msg": "An error occurred. Please try to get doctor list again.",
                    "code": "profile_patient_get_live_dr_list_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    requestCount = req.get_param('requestCount', default=10)
                    nextItemId = req.get_param('nextItemId', default=0)

                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        "requestCount": requestCount,
                        "nextItemId": nextItemId,
                    })
                    record = self.business.profile_patient_get_live_dr_list(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

                # MP4.7 API to get patient's note and attachment list of specific medical record field
            elif self.signal == 'profile_patient_get_medicalRecordFieldCode':
                resp_status = status_code[401]
                resp_body = {
                    "title": "Patient get medical record field failed",
                    "msg": "An error occurred. Please try to get doctor list again.",
                    "code": "profile_patient_get_medicalRecordFieldCode_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    requestCount = req.get_param('requestCount', default=10)
                    nextItemId = req.get_param('nextItemId', default=0)
                    medicalRecordFieldCode = req.get_param('medicalRecordFieldCode')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        "requestCount": requestCount,
                        "nextItemId": nextItemId,
                        "medicalRecordFieldCode": medicalRecordFieldCode
                    })
                    record = self.business.profile_patient_get_medicalRecordFieldCode(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            
            # MP4.8 API for patient to get general medical record information
            elif self.signal == 'profile_patient_get_general_medicalRecord':
                resp_status = status_code[401]
                resp_body = {
                    "title": "Patient get general medical record failed",
                    "msg": "An error occurred. Please try to get doctor list again.",
                    "code": "profile_patient_get_general_medicalRecord_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                    })
                    record = self.business.profile_patient_get_general_medicalRecord(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            
            # WD5.5 API for doctor to get patient's note and attachment list of specific medical record field 
            elif self.signal == 'profile_patient_get_medical_record':
                resp_status = status_code[401]
                resp_body = {
                    "title": "Get medical record failed",
                    "msg": "An error occurred. Please try to request again.",
                    "code": "get_medical_record_failed"
                }
                try:

                    header = req.headers  # key cua header luon viet hoa
                    medicalRecordFieldCode = req.get_param('medicalRecordFieldCode', default=False)
                    requestCount = req.get_param('requestCount', default=10)
                    nextItemId = req.get_param('nextItemId', default=0)
                    patientId = req.get_param('patientId', default=False)

                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        "recordField": medicalRecordFieldCode,
                        "requestCount": requestCount,
                        "nextItemId": nextItemId,
                        "patientId": patientId,
                        "requiredApm": False
                    })
                    record = self.business.profile_patient_get_medical_record(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']
                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            
        except Exception as e:
            print str(e)

    def on_post(self, req, resp, doctor_id=None):
        try:
            data = {}
            header = req.headers  # key cua header luon viet hoa
            all_header = self.general_business.get_header_request(header)
            lang = all_header.get('lang')
            _ = self.general_business.get_lang_instance(lang)
            data.update(all_header)

            if self.signal == 'profile_firsttimesetup':
                resp_status = status_code[401]
                resp_body = {
                    "title": "Setup profile failed",
                    "msg": "An error occurred. Please try to setup profile again.",
                    "code": "setup_profile_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    firstName = req.get_param('firstName', default='')
                    lastName = req.get_param('lastName', default='')
                    avatar = req.get_param('avatar', default=False)
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'firstName': firstName,
                        'lastName': lastName,
                        'avatar_url': ''
                    })
                    if type(avatar).__name__ == 'instance':
                        # Write image to local
                        user_objs = self.general_business.validate_user_token(header.get("X-USER-TOKEN", False),data.get('client_id'))
                        if user_objs and user_objs[7]:
                            import os
                            aws = aws_management()
                            name, ext = os.path.splitext(avatar.filename)
                            if ext.lower() in ('.jpg', '.png', '.gif', '.jpeg'):
                                create_date = datetime.now().strftime('%Y%m%d%H%M%S')
                                filename = 'avatar_%s_%s_%s%s' % (user_objs[1], user_objs[7], create_date, ext)
                                aws.upload_obj(avatar.file, AWS_BUCKET, filename)
                                data.update({'avatar_url': 'https://' + BUCKET_URL_DEFAULT + '/' + filename})
                            else:
                                data.update({'avatar_url': 'wrong_file_type'})
                                # End Write image to local

                    record = []
                    record = self.business.profile_firsttimesetup(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)


            elif self.signal == 'profile_patient_setup':
                resp_status = status_code[401]
                resp_body = {
                    "title": "Setup profile failed",
                    "msg": "An error occurred. Please try to setup profile again.",
                    "code": "setup_profile_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = json.loads(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        "titleCode": raw_data.get('titleCode', False),
                        "firstName": raw_data.get('firstName', None),
                        "middleName": raw_data.get('middleName', None),
                        "lastName": raw_data.get('lastName', None),
                        "occupation": raw_data.get('occupation', False),
                        "maritalStatusCode": raw_data.get('maritalStatusCode', False),
                        "dayOfBirth": raw_data.get('dayOfBirth', False),
                        "genderCode": raw_data.get('genderCode', False),
                        "ethnicity": raw_data.get('ethnicity', False),
                        "highestEducationLevelCode": raw_data.get('highestEducationLevelCode', False),
                        "contact": {
                            "homephone": raw_data["contact"].get('homephone', False),
                            "workphone": raw_data["contact"].get('workphone', False),
                            "mobilePhone": raw_data["contact"].get('mobilePhone', False),
                            "email": raw_data["contact"].get('email', False),
                            "primaryAddress": {
                                "number": raw_data["contact"]["primaryAddress"].get('number', False),
                                "street": raw_data["contact"]["primaryAddress"].get('street', False),
                                "cityCode": raw_data["contact"]["primaryAddress"].get('cityCode', False),
                                "state": raw_data["contact"]["primaryAddress"].get('state', False),
                                "postCode": raw_data["contact"]["primaryAddress"].get('postCode', False),
                                "countryCode": raw_data["contact"]["primaryAddress"].get('countryCode', False),
                            },
                            "alternateAddress": {
                                "number": raw_data["contact"]["alternateAddress"].get('number', False),
                                "street": raw_data["contact"]["alternateAddress"].get('street', False),
                                "cityCode": raw_data["contact"]["alternateAddress"].get('cityCode', False),
                                "state": raw_data["contact"]["alternateAddress"].get('state', False),
                                "postCode": raw_data["contact"]["alternateAddress"].get('postCode', False),
                                "countryCode": raw_data["contact"]["alternateAddress"].get('countryCode', False),
                            }
                        },
                        "languageCode": raw_data.get('languageCode', False),
                    })
                    record = self.business.profile_patient_setup(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)


            elif self.signal == 'profile_patient_mark_doctor':
                resp_status = status_code[401]
                resp_body = {
                    "title": "Mark doctor failed",
                    "msg": "An error occurred. Please try to mark doctor again.",
                    "code": "mark_doctor_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'doctor_id': doctor_id
                    })
                    record = self.business.profile_patient_mark_doctor(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'profile_patient_unmark_doctor':
                resp_status = status_code[401]
                resp_body = {
                    "title": "Unmark doctor failed",
                    "msg": "An error occurred. Please try to unmark doctor again.",
                    "code": "unmark_doctor_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'doctor_id': doctor_id
                    })
                    record = self.business.profile_patient_unmark_doctor(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # MP5.7 API for getting doctor list with sort and filter criterias (support pagination)
            elif self.signal == 'profile_patient_get_dr_list_by_sort_filter':
                resp_status = status_code[401]
                resp_body = {
                    "title": "Get doctor list by sort and filter criterias failed",
                    "msg": "An error occurred. Please try to get doctor list again.",
                    "code": "get_dr_list_by_sort_and_filter_criterias"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False)
                    })
                    data.update(raw_data)
                    record = self.business.profile_patient_get_dr_list_by_sort_filter(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            
            # MP5.9 API for getting live doctor list with sort and filter criterias (support pagination)
            elif self.signal == 'profile_patient_get_live_dr_list_by_sort_filter':
                resp_status = status_code[401]
                resp_body = {
                    "title": "Get live doctor list by sort and filter criterias failed",
                    "msg": "An error occurred. Please try to get doctor list again.",
                    "code": "get_live_dr_list_by_sort_and_filter_criterias"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False)
                    })
                    data.update(raw_data)
                    record = self.business.profile_patient_get_live_dr_list_by_sort_filter(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # MP4.9 API for patient to setup general medical record information
            elif self.signal == 'profile_patient_setup_general_medicalRecord':
                resp_status = status_code[401]
                resp_body = {
                    "title": "Patient setup general medical record failed",
                    "msg": "An error occurred. Please try to get doctor list again.",
                    "code": "profile_patient_setup_general_medicalRecord_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                    })
                    data.update(raw_data)
                    record = self.business.profile_patient_setup_general_medicalRecord(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # MP4.10 API for patient to search other patients by email or phone
            elif self.signal == 'profile_patient_search_otherppl':
                resp_status = status_code[401]
                resp_body = {
                    "title": "Patient search other patient failed",
                    "msg": "An error occurred. Please try to get doctor list again.",
                    "code": "profile_patient_search_otherppl_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                    })
                    data.update(raw_data)
                    record = self.business.profile_patient_search_otherppl(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            

        except Exception as e:
            print str(e)
