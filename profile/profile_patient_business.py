# -*- coding: utf-8 -*-
from datetime import datetime
import time
import logging
import random
from raven import Client
import ConfigParser
from collections import OrderedDict
import sys
import os

import json



from opentok import OpenTok, MediaModes

project_path =  str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
config = ConfigParser.RawConfigParser(allow_no_value=True)
config.read(project_path + '/' + "environment.conf")
sentry_key = config.get('environment', 'sentry_key')
client = Client(sentry_key)
logger = logging.getLogger('drdr_api')
sys.path.insert(0, "..")


from opentok import OpenTok, MediaModes

try:
    from general import general_business
except ImportError as e:
    print('No Import ', e)


class profile_patient_business(object):
    def __init__(self, signal, erp_connection, var_contructor):
        self.pool_conn = erp_connection

        self.ERP_DB_HOST = var_contructor['ERP_DB_HOST']
        self.ERP_DB_PORT = var_contructor['ERP_DB_PORT']
        self.ERP_DB_USER = var_contructor['ERP_DB_USER']
        self.ERP_DB_PASS = var_contructor['ERP_DB_PASS']
        self.ERP_DB_NAME = var_contructor['ERP_DB_NAME']
        self.general_business = general_business(erp_connection, var_contructor)
        self.com_ratio = float(config.get('environment', 'com_ratio')) or 40.0
        self.min_commission = float(config.get('environment', 'min_commission')) or 40000.0

        tokbox_config = json.loads(config.get('environment', 'tokbox_config'))

        self.api_key = tokbox_config['api_key']
        self.api_secret = tokbox_config['api_secret']
        self.opentok = OpenTok(self.api_key, self.api_secret)
        self.vi_ = self.general_business.vi_
        self.en_ = self.general_business.en_

    def profile_firsttimesetup(self, data):
        logger.info('profile_firsttimesetup %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Setup profile failed"),
                        "msg": _("An error occurred. Please try to setup profile again."),
                        "code": "setup_profile_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    firstName = data.get('firstName')
                    lastName = data.get('lastName')
                    avatar_url = data.get('avatar_url')
                    if avatar_url == 'wrong_file_type':
                        return {'status_code': 401,
                                'resq_body':
                                    {
                                        "title": _("Wrong type of file"),
                                        "msg": _("An error occurred. Please try to setup profile again."),
                                        "code": "wrong_file_type"
                                    }
                                }
                    sql_update = '''update user_mngment set last_name = %s,
                                                            first_name = %s,
                                                            profile_image_url = %s
                                    where id = (select user_mngment_id from user_account where id = %s)'''
                    gs.sql_execute(pool, sql_update, commit=True, para_values=(lastName,
                                                                               firstName,
                                                                               avatar_url,
                                                                               user_id))

                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     "code": "success",
                                     "title": _("Success"),
                                     "msg": _("Setup profile successful.")
                                 }
                                 })

                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }

        except Exception as e:
            logger.error('error_profile_firsttimesetup %s' % str(e))
            client.captureException()
        return resp

    def user_create_token(self, data):
        logger.info('token_get %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get profile doctor failed"),
                        "msg": _("An error occurred. Please try to get profile again."),
                        "code": "get_profile_doctor_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            doctor_id = data.get('doctor_id')
            res = {}
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    sql_select_doctor = '''select ua.id from user_account as ua where ua.user_mngment_id = %s''' %(doctor_id)
                    res_select_doctor = gs.sql_execute(pool, sql_select_doctor)
                    sql_select_session_2 = '''select ua.id,session_tokbox from user_account_session uas INNER JOIN user_account as ua ON ua.id = uas.user_account_id where ua.user_mngment_id = %s''' %(doctor_id)
                    res_session_2 = gs.sql_execute(pool, sql_select_session_2)
                    doctor_real_id = res_select_doctor[0][0]
                    session_id = ""
                    if res_session_2:
                        session_id = res_session_2[0][1]
                    else :
                        session = self.opentok.create_session( )
                        session_id = session.session_id
                        sql_insert_session = '''insert into user_account_session (session_tokbox,user_account_id) values (%s,%s)'''
                        gs.sql_execute(pool, sql_insert_session, commit=True, para_values=(session_id,doctor_real_id))
                        to_user_token = self.opentok.generate_token(session_id)
                        sql_insert_session = '''insert into user_account_token_tokbox (token_tokbox,doctor_id,user_account_id) values (%s,%s,%s)'''
                        gs.sql_execute(pool, sql_insert_session, commit=True, para_values=(to_user_token,doctor_real_id,doctor_real_id))
                    sql_select_token = '''select * from user_account_token_tokbox where doctor_id = %s and user_account_id = %s''' %(doctor_real_id,res_token[1])
                    res_select_token = gs.sql_execute(pool, sql_select_token)
                    if not res_select_token:
                        from_user_token = self.opentok.generate_token(session_id)
                        sql_insert_session = '''insert into user_account_token_tokbox (token_tokbox,doctor_id,user_account_id) values (%s,%s,%s)'''
                        gs.sql_execute(pool, sql_insert_session, commit=True, para_values=(from_user_token,doctor_real_id,res_token[1]))
                    logger.info('doctor session tokbox %s' % session_id)
                    if session_id:
                        resp.update({'status_code': 200,
                                     'resq_body': session_id
                                     })

                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
        except Exception as e:
            logger.error('error_token_user %s' % str(e))
            client.captureException()

        return resp
    
    # MP5.1 API for getting profile of specific doctor
    def profile_patient_get_doctor(self, data):
        logger.info('profile_patient_get %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get profile doctor failed"),
                        "msg": _("An error occurred. Please try to get profile again."),
                        "code": "get_profile_doctor_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            doctor_id = data.get('doctor_id')
            res = {}
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    sql = '''with doctor_languages as
                            (
                                select dp.id,
                                    array_to_string(array_agg(distinct sla.spoken_language), ', ') as spoken_language
                                from doctor_profile dp
                                left join dr_doctorprofile_languages sla on sla.doctor_profile_id = dp.id
                                where dp.user_mngment_id = %s
                                group by 1
                            )
                            select  distinct title,--0
                                            profile_image_url,--1
                                            first_name,--2
                                            middle_name,--3
                                            last_name,--4
                                            complete_name,--5
                                            type_profile,--6
                                            birthday,--7
                                            gender,--8
                                            sla.spoken_language,--9
                                            
                                            ua.email account_email,--10
                                            ua.phone account_phone,--11
                                            
                                            coalesce(is_primary, False) is_primary,--12
                                            home_phone,--13
                                            work_phone, --14
                                            ua.phone,--15 mobile_phone
                                            uc.email,--16
                                            uad.number,--17
                                            street,--18
                                            city,--19
                                            state,--20
                                            postcode,--21
                                            country,--22
                                            
                                            consultation_type,--23
                                            consultation_duration,--24
                                            coalesce(consultation_fee,0),--25
                                            coalesce(consultation_unit,'vnd'),--26
                                            years_of_experience,--27
                                            
                                            medical_school,--28
                                            graduation_date,--29
                                            post_graduate_training,--30
                                            industry_relationship,--31
                                            publications,--32
                                            teaching,--33
                                            community_service,--34
                                            criminal_convictions,--35
                                            limitations, --36
                                            hospital_restrictions,--37
                                            about,--38
                                            dl.number license_number,--39
                                            license_status,--40
                                            expiry_date,--41
                                            issue_date,--42
                                            issue_by, --43
                                            
                                            spe.specialties_code, --44
                                            spe.years_of_exp, --45 
                                            coalesce(is_primary, False) is_primary,--46
                                            dp.professional_affiliations_and_activities, -- 47
                                            coalesce(round((sum_rating_point/count_rating_point)::numeric,1),0)::float rating_point, --48
                                            case when current_timestamp <= ua.checkout_time and ua.activity_status != 'offline' then True
                                                else False
                                            end onlineStatus, --49
                                            ua.activity_status, -- 50
                                            um.degree_title, --51
                                            ua.id user_account_id--52
                                    from user_mngment um
                                        left join user_account ua on ua.user_mngment_id = um.id
                                        left join doctor_profile dp on  um.id = dp.user_mngment_id 
                                        left join dr_doctorprofile_specialties spe on spe.doctor_profile_id = dp.id
                                        left join doctor_languages sla on sla.id = dp.id
                                        left join dr_license dl on dl.doctor_profile_id = dp.id
                                        left join user_contact uc on um.id = uc.user_mngment_id  
                                        left join user_address uad on uad.user_contact_id = uc.id
                                    where um.id =%s '''
                    records = gs.sql_execute(pool, sql, para_values=(doctor_id, doctor_id))

                    if records:
                        primaryPracticalAddress = {
                            # 'defaultPhone': None,
                            'phone': None,
                            "number": None,
                            "street": None,
                            "cityCode": None,
                            "state": None,
                            "postCode": None,
                            "countryCode": None,
                        }

                        secondaryPracticalAddress = {
                            # 'defaultPhone': None,
                            'phone': None,
                            "number": None,
                            "street": None,
                            "cityCode": None,
                            "state": None,
                            "postCode": None,
                            "countryCode": None,
                        }

                        for line in records:
                            if line[46] == True:
                                primaryPracticalAddress = {
                                    # 'defaultPhone': line[11] or None,
                                    'phone': line[14] or None,
                                    "number": line[17] or None,
                                    "street": line[18] or None,
                                    "cityCode": line[19] or None,
                                    "state": line[20] or None,
                                    "postCode": line[21] or None,
                                    "countryCode": line[22] or None,
                                }
                            elif line[46] == False:
                                secondaryPracticalAddress = {
                                    # 'defaultPhone': line[11] or None,
                                    'phone': line[14] or None,
                                    "number": line[17] or None,
                                    "street": line[18] or None,
                                    "cityCode": line[19] or None,
                                    "state": line[20] or None,
                                    "postCode": line[21] or None,
                                    "countryCode": line[22] or None,
                                }
                        if 'from_doctor' in data:
                            full_fee = int(records[0][25])
                        else:
                            margin = max(self.min_commission, float(records[0][25]) * (self.com_ratio) / 100)
                            full_fee = float(records[0][25]) + margin

                        res.update({
                            'ratingPoint': records[0][48],
                            'titleCode': records[0][0] or None,
                            'avatar': records[0][1] or None,
                            'firstName': None if records[0][2] in ('False', 'None', '') else records[0][2],
                            'middleName': None if records[0][3] in ('False', 'None', '') else records[0][3],
                            'lastName': None if records[0][4] in ('False', 'None', '') else records[0][4],
                            # 'completeName': records[0][5] or None,
                            'dayOfBirth': None if records[0][7] in ('False', 'None', '') or not records[0][7] else int(
                                time.mktime(records[0][7].timetuple())),
                            'genderCode': records[0][8] or None,
                            'spokenLanguageCode': records[0][9] and records[0][9].split(', ') or [],

                            'primaryPracticalAddress': primaryPracticalAddress,
                            'secondaryPracticalAddress':secondaryPracticalAddress,
                            "consultationTypeCode": records[0][23] or None,
                            "consultationDurationCode": records[0][24] or None,
                            # "yearsOfExperience": records[0][27] or None,
                            "medicalSchool": records[0][28] or None,
                            "graduationDate": records[0][29] and int(time.mktime(records[0][29].timetuple())) or None,
                            "postGraduateTraining": records[0][30] or None,
                            "industryRelationship": records[0][31] or None,
                            "publications": records[0][32] or None,
                            "teaching": records[0][33] or None,
                            "communityService": records[0][34] or None,
                            "criminalConvictions": records[0][35] or None,
                            "limitations": records[0][36] or None,
                            "hospitalRestrictions": records[0][37] or None,
                            "aboutMe": records[0][38] or None,

                            "license": {
                                "number": records[0][39] or None,
                                "licenseStatus": records[0][40] or None,
                                "issueDate": records[0][41] and int(time.mktime(records[0][41].timetuple())) or None,
                                "expiryDate": records[0][42] and int(time.mktime(records[0][42].timetuple())) or None,
                                "issueBy": records[0][43] or None,
                            },
                            "specialtyCodes": [],
                            "professionalAffiliationsAndActivities": records[0][47] or None,
                            "isAvailableToCall": True if records[0][23] == 'live' and records[0][49] == True else False,
                            "degreeTitle":records[0][51],
                            "consultationFee": {
	                            "value": full_fee,
                                "unitCode": records[0][26] or None
                            },
                            "statusDoctor": records[0][50]
                        })
                        #update anshor
                        list_specialtyCodes = []
                        for line in records:
                            if line[44] not in list_specialtyCodes:
                                res['specialtyCodes'].append({'code': line[44], 'yearsOfExp': line[45]})
                                list_specialtyCodes.append(line[44])
                        sql_check_favour_doctor = '''SELECT id FROM patient_doctor_favourite where patient_id = %s and doctor_id = %s '''
                        check_exist = gs.sql_execute(pool, sql_check_favour_doctor,
                                                     para_values=(res_token[7], doctor_id))
                        if check_exist:
                            res.update({"isMarkedAsFavourite": True})
                        else:
                            res.update({"isMarkedAsFavourite": False})

                        sql_final_date = '''SELECT Max(final_date) 
                                            FROM calendar_event ce
                                            join user_account ua on ua.user_mngment_id = %s
                                            where ce.user_id = ua.id ''' % (doctor_id)
                        final_date = gs.sql_execute(pool, sql_final_date)
                        if final_date and final_date[0][0]:
                            if final_date[0][0] < datetime.now().date():
                                res.update({"isAvailableToBook": False})
                            else:
                                res.update({"isAvailableToBook": True})
                        else:
                            res.update({"isAvailableToBook": False})

                        # get document list
                        documentList = []
                        sql_select = """select  att.file_extension, --0
                                                att.file_size, --1
                                                att.write_date, --2
                                                att.url, --3
                                                att.file_name, --4
                                                att.create_uid, --5
                                                att.id --6
                                        from attachment att
                                        where att.res_model = 'user_account' and res_id = %s """
                        records = gs.sql_execute(pool, sql_select, para_values=[records[0][52]])
                        documentList = []
                        for line in records:
                            documentList.append({
                                "id": line[6],
                                "url": line[3],
                                "fileName": line[4],
                                "fileExtension": line[0],
                                "fileSize": self.general_business.bytesToSize(line[1]),
                                "latestUpdateTime": int(time.mktime(line[2].timetuple()))
                            }
                            )

                        res.update({'documentList': documentList})
                    if res:
                        resp.update({'status_code': 200,
                                     'resq_body': res
                                     })

                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
        except Exception as e:
            logger.error('error_profile_patient_get_doctor %s' % str(e))
            client.captureException()

        return resp

    def generate_pagination_list(self, user_id, table_pagination, context={}):
        gs = self.general_business
        pool = self.pool_conn
        full_list_ids = []

        def cache_list(full_list_ids, table_pagination, rand=False):
            if rand == True:
                random.shuffle(full_list_ids)
            if full_list_ids:
                sql_search = '''select id 
                                            from user_account_pagination
                                            where user_account_id = %s
                                                and table_pagination = %s '''
                exist = gs.sql_execute(pool, sql_search, para_values=(user_id,
                                                                      table_pagination))
                if exist:
                    sql_update = '''update user_account_pagination set list_id = %s
                                                where user_account_id = %s
                                                    and table_pagination = %s '''
                    gs.sql_execute(pool, sql_update, commit=True, para_values=(str(full_list_ids),
                                                                               user_id,
                                                                               table_pagination))

                else:
                    sql_insert = '''insert into user_account_pagination(user_account_id, list_id, table_pagination)
                                                values (%s,%s,%s)'''
                    gs.sql_execute(pool, sql_insert, commit=True, para_values=(user_id,
                                                                               str(full_list_ids),
                                                                               table_pagination))

            return full_list_ids

        if 'specialtyCode' in context:
            sql_list = '''select distinct dp.user_mngment_id 
                            from dr_doctorprofile_specialties dr
                            inner join doctor_profile dp on dp.id = dr.doctor_profile_id
                            inner join user_account ua on ua.user_mngment_id = dp.user_mngment_id
                            inner join calendar_event ce on ua.id = ce.user_id
                            where dr.specialties_code = %s and final_date > current_timestamp and ua.validation_status = 'active' '''
            res_list = gs.sql_execute(pool, sql_list, para_values=(context['specialtyCode'],))
            for rc in res_list:
                full_list_ids.append(int(rc[0]))

            full_list_ids = cache_list(full_list_ids, table_pagination, rand=True)

        elif 'patient_interacted' in context:
            where = 'and 1=1'
            if 'searchKeyword' in context and context['searchKeyword']:
                where = "and (first_name ilike '%" + context['searchKeyword'] + "%'  or middle_name ilike '%" + context[
                    'searchKeyword'] + "%' or last_name ilike '%" + context['searchKeyword'] + "%')"

            sql_list = '''SELECT uac.user_mngment_id patient_id,last_name,
                                        max(apm.write_date) max_schedule_date,
                                        count(patient_id) interacted_count
                                from appointment apm
                                left join user_account uac on uac.id = apm.patient_id
                                left join user_mngment um on uac.user_mngment_id = um.id
                                where doctor_id = %s and uac.user_mngment_id is not null 
                                %s
                                group by 1,2
                                %s ''' % (user_id, where, context['sortBy'])
            res_list = gs.sql_execute(pool, sql_list)
            for rc in res_list:
                full_list_ids.append([rc[0], int(time.mktime(rc[2].timetuple())), rc[3]])

            full_list_ids = cache_list(full_list_ids, table_pagination)

        # MP5.4 API for getting review list of a doctor (support pagination)
        elif 'appointment_review' in context and 'doctor_id' in context:
            sql_list = '''SELECT ar.id FROM appointment ap 
                        INNER join user_account ua on ua.id = ap.doctor_id
                        INNER JOIN appointment_review ar ON ar.appointment_id = ap.id 
                        WHERE ua.user_mngment_id = %s
                            ORDER BY ar.write_date DESC '''
            res_list = gs.sql_execute(pool, sql_list, para_values=(context['doctor_id'],))
            for rc in res_list:
                full_list_ids.append(int(rc[0]))
            full_list_ids = cache_list(full_list_ids, table_pagination)

        elif 'recordField' in context:
            # TODO: sql injection
            patientId = context.get('patientId')
            recordField = context['recordField']
            requiredApm = context.get('requiredApm',True)
            if recordField:
                where = "and mr.code = '%s'" % recordField
            else:
                where = "and mr.code is not null"

            if patientId:
                if requiredApm:
                    sql_list = '''select mr.id
                                from appointment_medical_record mr 
                                left join appointment apm on apm.id = mr.appointment_id
                                left join user_account ua on ua.id = apm.patient_id or mr.user_mngment_id = ua.user_mngment_id
                                where 1=1
                                %s
                                and ua.user_mngment_id = %s
                                order by mr.write_date desc ''' % (where, patientId)
                else:
                    sql_list = '''select distinct mr.id, mr.write_date
                                from appointment_medical_record mr 
                                left join appointment apm on apm.id = mr.appointment_id or (mr.appointment_id is null and mr.user_mngment_id = %s)
                                left join user_account ua on ua.id = apm.patient_id
                                where 1=1
                                %s
                                and ua.user_mngment_id = %s
                                order by mr.write_date desc ''' % (patientId,where, patientId)
            else:
                sql_list = '''select mr.id
                            from appointment_medical_record mr 
                            left join appointment apm on apm.id = mr.appointment_id
                            left join user_account ua on mr.user_mngment_id = ua.user_mngment_id
                            where 1=1
                            %s
                            and (apm.patient_id = %s or ua.id = %s)
                            order by mr.write_date desc ''' % (where, user_id, user_id)
            res_list = gs.sql_execute(pool, sql_list)
            for rc in res_list:
                full_list_ids.append(int(rc[0]))
            full_list_ids = cache_list(full_list_ids, table_pagination)

        elif 'api' in context and context['api'] == 'MP5_7':
            sql_list = '''select distinct um.id,  (COALESCE(dp.sum_rating_point,0) / COALESCE(dp.count_rating_point,1)) as rating_point, um.first_name, um.last_name,dp.consultation_fee
                            from user_mngment um
                            inner join doctor_profile dp on dp.user_mngment_id = um.id
                            inner join user_account ua on ua.user_mngment_id = um.id
                            inner join calendar_event ce on ce.user_id = ua.id
                            where um.type_profile = 'doctor' and dp.hospital_profile_id IS NULL and ua.validation_status = 'active' and ((dp.consultation_type = 'by_appointment' and ce.end_datetime > current_timestamp) or dp.consultation_type = 'live') '''

            if context['filterBy']:
                if context['filterBy'].get('specialtyCodes') or context['filterBy'].get('experienceYearRange'):
                    sql_list = '''select distinct um.id,  (COALESCE(dp.sum_rating_point,0) / COALESCE(dp.count_rating_point,1)) as rating_point, um.first_name, um.last_name,dp.consultation_fee
                                from user_mngment um
                                inner join doctor_profile dp on dp.user_mngment_id = um.id
                                left join dr_doctorprofile_specialties dr on dr.doctor_profile_id = dp.id
                                inner join user_account ua on ua.user_mngment_id = um.id
                                inner join calendar_event ce on ce.user_id = ua.id
                                where um.type_profile = 'doctor' and dp.hospital_profile_id IS NULL and ua.validation_status = 'active' and ((dp.consultation_type = 'by_appointment' and ce.end_datetime > current_timestamp) or dp.consultation_type = 'live') '''

            if context['keyword']:
                whereLike = '''and (first_name ILIKE '%%%s%%' or last_name ILIKE '%%%s%%' )''' % (
                context['keyword'], context['keyword'])
                sql_list += whereLike

            sql_list += self.filter_doctors_by(context['filterBy'])
            sql_list += context['sortBy']
            res_list = gs.sql_execute(pool, sql_list)
            for rc in res_list:
                full_list_ids.append(int(rc[0]))
            full_list_ids = cache_list(full_list_ids, table_pagination)
        
        elif 'api' in context and context['api'] == 'MP5_9':
            sql_list = '''select distinct um.id,  (COALESCE(dp.sum_rating_point,0) / COALESCE(dp.count_rating_point,1)) as rating_point, um.first_name, um.last_name,dp.consultation_fee
                            from user_mngment um
                            inner join doctor_profile dp on dp.user_mngment_id = um.id
                            inner join user_account ua on ua.user_mngment_id = um.id
                            inner join calendar_event ce on ce.user_id = ua.id
                            where um.type_profile = 'doctor' and dp.hospital_profile_id IS NULL and ua.validation_status = 'active' and dp.consultation_type='live' and ce.final_date > current_timestamp and ua.latest_activity_time > current_timestamp - interval '15 minutes' and ua.activity_status = 'online' '''

            if context['filterBy']:
                if context['filterBy'].get('specialtyCodes') or context['filterBy'].get('experienceYearRange'):
                    sql_list = '''select distinct um.id,  (COALESCE(dp.sum_rating_point,0) / COALESCE(dp.count_rating_point,1)) as rating_point, um.first_name, um.last_name,dp.consultation_fee
                                from user_mngment um
                                inner join doctor_profile dp on dp.user_mngment_id = um.id
                                left join dr_doctorprofile_specialties dr on dr.doctor_profile_id = dp.id
                                inner join user_account ua on ua.user_mngment_id = um.id
                                inner join calendar_event ce on ce.user_id = ua.id
                                where um.type_profile = 'doctor' and dp.hospital_profile_id IS NULL and ua.validation_status = 'active' and dp.consultation_type='live' and ua.latest_activity_time > current_timestamp - interval '15 minutes' and ua.activity_status = 'online' '''

            if context['keyword']:
                whereLike = '''and (first_name ILIKE '%%%s%%' or last_name ILIKE '%%%s%%' )''' % (
                context['keyword'], context['keyword'])
                sql_list += whereLike

            sql_list += self.filter_doctors_by(context['filterBy'])
            sql_list += context['sortBy']

            res_list = gs.sql_execute(pool, sql_list)
            for rc in res_list:
                full_list_ids.append(int(rc[0]))
            full_list_ids = cache_list(full_list_ids, table_pagination)

        elif 'api' in context and context['api'] == 'MP5_8':
            sql_list = '''select distinct um.id, um.first_name
                            from user_mngment um
                            inner join user_account ua on ua.user_mngment_id = um.id
                            inner join doctor_profile dp on dp.user_mngment_id = um.id
                            inner join calendar_event ce on ce.user_id = ua.id
                            where um.type_profile = 'doctor' and dp.hospital_profile_id IS NULL and ua.validation_status = 'active' and dp.consultation_type='live' and ce.final_date > current_timestamp and ua.latest_activity_time > current_timestamp - interval '15 minutes' and ua.activity_status = 'online'
                            order by um.first_name asc'''

            res_list = gs.sql_execute(pool, sql_list)
            for rc in res_list:
                full_list_ids.append(int(rc[0]))
            full_list_ids = cache_list(full_list_ids, table_pagination)

        # WD3.7 API for getting review list of a doctor
        elif 'appointment_review' in context and 'user_id' in context:
            user_id = context['user_id']
            sql_list = '''select apmr.id
                        from appointment_review apmr
                        left join appointment apm on apm.id = apmr.appointment_id
                        where apm.doctor_id = %s
                        order by apmr.write_date desc
                  '''
            res_list = gs.sql_execute(pool, sql_list, para_values=[user_id])
            for rc in res_list:
                full_list_ids.append(int(rc[0]))
            full_list_ids = cache_list(full_list_ids, table_pagination)
        elif 'patient_note_history' in context:
            sql_list = '''SELECT apm.id FROM appointment apm 
                        INNER join user_account ua on ua.id = apm.patient_id
                        WHERE apm.doctor_id = %s and ua.user_mngment_id = %s and COALESCE(apm.consultation_note, '') <> ''
                            ORDER BY apm.write_date DESC '''

            res_list = gs.sql_execute(pool, sql_list, para_values=(user_id, context['patient_id']))
            for rc in res_list:
                full_list_ids.append(int(rc[0]))
            full_list_ids = cache_list(full_list_ids, table_pagination)

        elif 'api' in context and context['api'] == 'MP6_4':
            sql_list = '''select apm.id
                                    from appointment apm
                                    where apm.patient_id = %s and apm.state in ('rescheduled_by_patient', 'successful_finished','free_finished','canceled_by_doctor','canceled_by_patient') 
                                    order by apm.schedule_date desc ''' % (user_id)
            res_list = gs.sql_execute(pool, sql_list, para_values=[user_id])
            for rc in res_list:
                full_list_ids.append(int(rc[0]))
            full_list_ids = cache_list(full_list_ids, table_pagination)
        
        elif 'api' in context and context['api'] == 'WD8_12':
            sql_list = '''select wt.id
                            from withdraw_transaction wt
                            where wt.user_account_id = %s
                            order by wt.created_date desc'''%(user_id)

            res_list = gs.sql_execute(pool, sql_list)
            for rc in res_list:
                full_list_ids.append(int(rc[0]))
            full_list_ids = cache_list(full_list_ids, table_pagination)

        elif 'api' in context and context['api'] == 'MP4_10':
            sql_list = '''select distinct um.id, um.first_name
                            from user_mngment um
                            left join user_account ua on ua.user_mngment_id = um.id
                            where um.type_profile = 'patient' and ( CONCAT(first_name, ' ', middle_name, ' ', last_name) ilike %s%s%s or CONCAT(last_name, ' ', middle_name, ' ', first_name) ilike %s%s%s or ua.phone like %s%s%s or ua.email ilike %s%s%s)
                            order by um.first_name asc'''%("'%",context['searchKeyword'],"%'","'%",context['searchKeyword'],"%'","'%",context['searchKeyword'][1:],"%'","'%",context['searchKeyword'],"%'")
            
            res_list = gs.sql_execute(pool, sql_list)
            for rc in res_list:
                full_list_ids.append(int(rc[0]))
            full_list_ids = cache_list(full_list_ids, table_pagination)

        return full_list_ids

    def filter_doctors_by(self, filterBy):
        sql_where = ''
        if filterBy:
            if filterBy.get('feeRange'):
                sql_where += '''and (consultation_fee >= %s and  consultation_fee <= %s ) ''' % (
                filterBy['feeRange']['min'].get('value'), filterBy['feeRange']['max'].get('value'))

            if filterBy.get('experienceYearRange'):
                sql_where += '''and (years_of_exp >= %s and  years_of_exp <= %s ) ''' % (
                filterBy['experienceYearRange'].get('min'), filterBy['experienceYearRange'].get('max'))

            if filterBy.get('rating'):
                rating = int(filterBy.get('rating'))
                if rating == 1:
                    sql_where += '''and (COALESCE(sum_rating_point,0) / COALESCE(count_rating_point,1)) <= %s ''' % (rating)
                elif rating == 2:
                    sql_where += '''and ( (COALESCE(sum_rating_point,0) / COALESCE(count_rating_point,1)) > 1 and (COALESCE(sum_rating_point,0) / COALESCE(count_rating_point,1)) <= %s )''' % (rating)
                elif rating == 3:
                    sql_where += '''and ( (COALESCE(sum_rating_point,0) / COALESCE(count_rating_point,1)) > 2 and (COALESCE(sum_rating_point,0) / COALESCE(count_rating_point,1)) <= %s )''' % (rating)
                elif rating == 4:
                    sql_where += '''and ( (COALESCE(sum_rating_point,0) / COALESCE(count_rating_point,1)) > 3 and (COALESCE(sum_rating_point,0) / COALESCE(count_rating_point,1)) <= %s )''' % (rating)
                elif rating == 5:
                    sql_where += '''and (COALESCE(sum_rating_point,0) / COALESCE(count_rating_point,1)) > 4 '''

            if filterBy.get('specialtyCodes'):
                sql_where += '''and specialties_code in (%s) ''' % (str(filterBy.get('specialtyCodes'))[1:-1])

            if filterBy.get('doctorSpokenLanguageCodes'):
                sql_where += '''and spoken_language in (%s) ''' % (str(filterBy.get('doctorSpokenLanguageCodes'))[1:-1])

            if filterBy.get('consultationType'):
                sql_where += '''and consultation_type = '%s' ''' % (filterBy.get('consultationType'))

        return sql_where

    def update_pagination_list(self, user_id, table_pagination, current_list, context={}):
        gs = self.general_business
        pool = self.pool_conn
        full_list_ids = current_list
        list_ids = []
        if 'specialtyCode' in context:
            sql_list = '''select distinct user_mngment_id 
                            from dr_doctorprofile_specialties dr
                            inner join doctor_profile dp on dp.id = doctor_profile_id
                            where specialties_code = %s 
                                and user_mngment_id not in %s '''
            res_list = gs.sql_execute(pool, sql_list, para_values=(context['specialtyCode'], current_list))
            for rc in res_list:
                list_ids.append(int(rc[0]))

            random.shuffle(list_ids)
            if list_ids:
                sql_update = '''update user_account_pagination set list_id = %s
                                where user_account_id = %s
                                    and table_pagination = %s '''
                gs.sql_execute(pool, sql_update, commit=True, para_values=(str(current_list + list_ids),
                                                                           user_id,
                                                                           table_pagination))
                full_list_ids.extend(list_ids)

        elif 'recordField' in context:
            patientId = context['patientId']
            recordField = context['recordField']
            if recordField:
                where = "and mr.code = '%s'" % recordField
            else:
                where = "and mr.code is not null"
            sql_list = '''select mr.id
                        from appointment_medical_record mr 
                          left join appointment apm on apm.id = mr.appointment_id
                          left join user_account ua on ua.id = apm.patient_id
                        where 1=1
                        %s
                          and ua.user_mngment_id = %s
                          and rm.id not in %s
                        order by mr.create_date desc ''' % (where,
                                                            patientId,
                                                            str(tuple(current_list)))
            res_list = gs.sql_execute(pool, sql_list)
            for rc in res_list:
                list_ids.append(int(rc[0]))

            if list_ids:
                sql_update = '''update user_account_pagination set list_id = %s
                                where user_account_id = %s
                                    and table_pagination = %s '''
                gs.sql_execute(pool, sql_update, commit=True, para_values=(str(current_list + list_ids),
                                                                           user_id,
                                                                           table_pagination))
                full_list_ids.extend(list_ids)

        elif 'doctor_id' in context:
            sql_list = '''SELECT ar.id FROM appointment ap 
                        INNER join user_account ua on ua.id = ap.doctor_id
                        INNER JOIN appointment_review ar ON ar.appointment_id = ap.id 
                        WHERE ua.user_mngment_id = %s and ua.validation_status = 'active' and  ar.id not in %s
                            ORDER BY ar.write_date DESC '''
            res_list = gs.sql_execute(pool, sql_list, para_values=(context['doctor_id'], tuple(current_list)))
            for rc in res_list:
                list_ids.append(int(rc[0]))

            if list_ids:
                sql_update = '''update user_account_pagination set list_id = %s
                                where user_account_id = %s
                                    and table_pagination = %s '''
                gs.sql_execute(pool, sql_update, commit=True, para_values=(str(current_list + list_ids),
                                                                           user_id,
                                                                           table_pagination))
                full_list_ids.extend(list_ids)
        return full_list_ids

    def profile_patient_dr_list_byspecicalty(self, data):
        logger.info('profile_patient_dr_list_byspecicalty %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get doctor list byspecicalty failed"),
                        "msg": _("An error occurred. Please try to get doctor list again."),
                        "code": "get_doctor_list_byspecicalty_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            specialtyCode = data.get('specialtyCode')
            requestCount = int(data.get('requestCount'))
            nextItemId = int(data.get('nextItemId'))

            res = {
                'pagination': {
                    "resultCount": 0,
                    "totalCount": 0,
                    "nextItemId": 0
                },
                'doctorList': [],
            }
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                start = 0
                end = 0
                if res_token:
                    user_id = res_token[1]
                    if not nextItemId or nextItemId == 0:
                        select_list_ids = []
                        full_list_ids = self.generate_pagination_list(user_id, 'doctor_profile',
                                                                      context={'specialtyCode': specialtyCode})
                        start, end = 0, requestCount
                    else:
                        sql = '''select list_id 
                                from user_account_pagination
                                where user_account_id = %s
                                    and table_pagination = %s
                                '''
                        full_list_ids = gs.sql_execute(pool, sql, para_values=(user_id, 'doctor_profile'))
                        if full_list_ids:
                            full_list_ids = eval(full_list_ids[0][0])
                            start, end = full_list_ids.index(nextItemId), full_list_ids.index(nextItemId) + requestCount
                        else:
                            full_list_ids = self.generate_pagination_list(user_id, 'doctor_profile',
                                                                          context={'specialtyCode': specialtyCode})
                            start, end = 0, requestCount

                    if not full_list_ids:
                        resp.update({'status_code': 200,
                                     'resq_body': res
                                     })
                        return resp

                    end = end if end <= len(full_list_ids) else len(full_list_ids)
                    select_list_ids = full_list_ids[start:end]

                    res['pagination']['totalCount'] = len(full_list_ids)
                    res['pagination']['nextItemId'] = full_list_ids[end] if end < len(full_list_ids) else full_list_ids[
                        end - 1]
                    res['pagination']['resultCount'] = len(select_list_ids)

                    # get list_specialties
                    if len(select_list_ids) > 0:
                        select_list_ids = select_list_ids + [-1, -1]
                        sql = '''with list_doctor as
                                (
                                
                                select um.id profile_id,
                                        dp.id doctor_id, 
                                        COALESCE(um.profile_image_url,'') profile_image_url,
                                        COALESCE(um.title,'') title,
                                        COALESCE(um.first_name,'') first_name,
                                        COALESCE(um.middle_name,'') middle_name,
                                        COALESCE(um.last_name,'') last_name,
                                        consultation_type,
                                        drsp.specialties_code,
                                        mtsp.name specialties_name,
                                        (COALESCE(sum_rating_point,0)/COALESCE(count_rating_point,1))::float rating_point,
                                        case when ua.latest_activity_time > current_timestamp - interval '15 minutes' then True
                                            else False
                                        end onlineStatus,
                                        ua.activity_status
                                from doctor_profile dp
                                left join user_mngment um on um.id = dp.user_mngment_id
                                left join dr_doctorprofile_specialties drsp on drsp.doctor_profile_id = dp.id
                                left join meta_dr_specialties mtsp on code = specialties_code
                                left join user_account ua on ua.user_mngment_id = um.id
                                where um.id in %s and ua.validation_status = 'active' 
                                )
                                select d.profile_id,--0
                                        d.profile_image_url,--1
                                        d.title,--2
                                        d.first_name,--3
                                        d.middle_name,--4
                                        d.last_name,--5
                                        d.consultation_type,--6
                                        d.rating_point, --7
                                        d.onlineStatus, --8
                                        d.activity_status, --9
                                        array_to_string(array_agg(distinct s.specialties_code), ', ') as list_specialties --10
                                from list_doctor d
                                left join dr_doctorprofile_specialties s on s.doctor_profile_id = d.doctor_id
                                group by 1,2,3,4,5,6,7,8,9,10''' % str(tuple(select_list_ids))

                        list_doctor = gs.sql_execute(pool, sql)
                        if list_doctor:
                            tmp_res = []
                            for dr in list_doctor:
                                list_sp = []
                                for sp in dr[10].split(', '):
                                    list_sp.append({'code': sp})
                                tmp_res.append({
                                    "id": dr[0],
                                    "avatar": dr[1],
                                    "titleCode": dr[2],
                                    "firstName": None if dr[3] in ('False', 'None', '') else dr[3],
                                    "middleName": None if dr[4] in ('False', 'None', '') else dr[4],
                                    "lastName": None if dr[5] in ('False', 'None', '') else dr[5],
                                    "specialtyCodes": list_sp,
                                    "isAvailableToCall": True if dr[6] == 'live' and dr[8] == True and dr[9] == 'online' else False,
                                    "ratingPoint": dr[7]
                                })
                            res['doctorList'] = tmp_res

                    resp.update({'status_code': 200,
                                 'resq_body': res
                                 })
                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
        except Exception as e:
            logger.error('profile_patient_dr_list_byspecicalty %s' % str(e))
            client.captureException()

        return resp

    #MP5.2
    def profile_patient_get_dr_shortlist(self, data):
        logger.info('profile_patient_get_dr_shortlist %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get doctor shortlist failed"),
                        "msg": _("An error occurred. Please try to get doctor shortlist again."),
                        "code": "get_doctor_shortlist_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            res = {
                'favouriteList': [],
                'liveList': [],
                'summary': {
                    'favouriteListCount': 0,
                    'liveListCount': 0
                }
            }
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    # get sumary
                    sql = '''select     drsp.specialties_code,
                                        mtsp.name specialties_name,
                                        count(distinct ua.id)
                                from doctor_profile dp
                                left join user_account ua on ua.user_mngment_id = dp.user_mngment_id
                                left join dr_doctorprofile_specialties drsp on drsp.doctor_profile_id = dp.id
                                left join meta_dr_specialties mtsp on code = specialties_code
                                left join calendar_event ce on ce.user_id = ua.id
                                where dp.hospital_profile_id IS NULL and ua.validation_status = 'active' and ((dp.consultation_type = 'by_appointment' and ce.end_datetime > current_timestamp ) or dp.consultation_type = 'live')
                                group by 1,2
                    '''
                    list_sumary = gs.sql_execute(pool, sql)
                    for line in list_sumary:
                        if line[1]:
                            spec_name = line[1].split(' ')
                            spec_name[0] = spec_name[0].lower()
                            key = ''.join(spec_name) + 'ListCount'
                            res['summary'].update({key: line[2]})

                    # get list_specialties
                    sql = '''select code from meta_dr_specialties'''
                    list_specialties = gs.sql_execute(pool, sql)
                    if list_specialties:
                        sql = '''with list_doctor as
                                (
                                
                                select distinct um.id profile_id,
                                        dp.id doctor_id, 
                                        COALESCE(um.profile_image_url,'') profile_image_url,
                                        COALESCE(um.title,'') title,
                                        COALESCE(um.first_name,'') first_name,
                                        COALESCE(um.middle_name,'') middle_name,
                                        COALESCE(um.last_name,'') last_name,
                                        consultation_type,
                                        drsp.specialties_code,
                                        mtsp.name specialties_name,
                                        (COALESCE(sum_rating_point,0)/COALESCE(count_rating_point,1))::float rating_point,
                                        case when latest_activity_time > current_timestamp - interval '15 minutes' then True
                                            else False
                                        end onlineStatus,
                                        ua.activity_status
                                from doctor_profile dp
                                left join user_mngment um on um.id = dp.user_mngment_id
                                left join user_account ua on ua.user_mngment_id = um.id
                                left join dr_doctorprofile_specialties drsp on drsp.doctor_profile_id = dp.id
                                left join meta_dr_specialties mtsp on code = specialties_code
                                left join calendar_event ce on ce.user_id = ua.id
                                %s
                                limit 10
                                )
                                select d.profile_id,--0
                                        d.profile_image_url,--1
                                        d.title,--2
                                        d.first_name,--3
                                        d.middle_name,--4
                                        d.last_name,--5
                                        d.consultation_type,--6
                                        d.specialties_code,--7
                                        d.specialties_name,--8
                                        d.rating_point,--9
                                        d.onlineStatus, --10
                                        d.activity_status, --11
                                        array_to_string(array_agg(distinct s.specialties_code), ', ') as list_specialties --12
                                from list_doctor d
                                left join dr_doctorprofile_specialties s on s.doctor_profile_id = d.doctor_id
                                group by 1,2,3,4,5,6,7,8,9,10,11,12'''
                        for spec in list_specialties:
                            where = '''where drsp.specialties_code = '%s' and dp.hospital_profile_id IS NULL and ua.validation_status = 'active' and ((dp.consultation_type = 'by_appointment' and ce.end_datetime > current_timestamp) or dp.consultation_type = 'live') ''' % spec                     
                            list_doctor = gs.sql_execute(pool, sql % where)
                            if list_doctor:
                                spec_name = list_doctor[0][8].split(' ')
                                spec_name[0] = spec_name[0].lower()
                                key = ''.join(spec_name) + 'List'
                                tmp_res = []
                                for dr in list_doctor:
                                    list_sp = []
                                    for sp in dr[12].split(', '):
                                        list_sp.append({'code': sp})
                                    tmp_res.append({
                                        "id": dr[0],
                                        "avatar": dr[1],
                                        "titleCode": dr[2],
                                        "firstName": None if dr[3] in ('False', 'None', '') else dr[3],
                                        "middleName": None if dr[4] in ('False', 'None', '') else dr[4],
                                        "lastName": None if dr[5] in ('False', 'None', '') else dr[5],
                                        "specialtyCodes": list_sp,
                                        "isAvailableToCall": True if dr[6] == 'live' and dr[10] == True and dr[11] == 'online' else False,
                                        "ratingPoint": dr[9]
                                    })
                                res.update({key: tmp_res})

                    # update live doctor
                    sql = '''select consultation_type,count(distinct ua.id)
                            from doctor_profile dp
                                inner join user_mngment um on um.id = dp.user_mngment_id
                                inner join user_account ua on ua.user_mngment_id = um.id
                                inner join calendar_event ce on ce.user_id = ua.id
                            where um.type_profile = 'doctor' and dp.hospital_profile_id IS NULL and ua.validation_status = 'active' and consultation_type = 'live' and ua.latest_activity_time > current_timestamp - interval '15 minutes' and ua.activity_status = 'online'
                            group by 1'''
                    live_count = gs.sql_execute(pool, sql)
                    if live_count and live_count[0][1]:
                        res['summary'].update({'liveListCount': live_count[0][1]})
                    else:
                        res['summary'].update({'liveListCount': 0})
                    sql_live_dr = '''with list_doctor as
                                    (
                                    
                                    select distinct um.id profile_id,
                                            dp.id doctor_id, 
                                            COALESCE(um.profile_image_url,'') profile_image_url,
                                            COALESCE(um.title,'') title,
                                            COALESCE(um.first_name,'') first_name,
                                            COALESCE(um.middle_name,'') middle_name,
                                            COALESCE(um.last_name,'') last_name,
                                            consultation_type,
                                            (COALESCE(sum_rating_point,0)/COALESCE(count_rating_point,1))::float rating_point
                                    from doctor_profile dp
                                        left join user_mngment um on um.id = dp.user_mngment_id
                                        left join user_account ua on ua.user_mngment_id = um.id
                                        left join calendar_event ce on ce.user_id = ua.id
                                    where um.type_profile = 'doctor' and dp.hospital_profile_id IS NULL and ua.validation_status = 'active' and consultation_type = 'live' and ua.latest_activity_time > current_timestamp - interval '15 minutes' and ua.activity_status = 'online'
                                    order by first_name asc
                                    limit 10
                                    )
                                    select d.profile_id,--0
                                            d.profile_image_url,--1
                                            d.title,--2
                                            d.first_name,--3
                                            d.middle_name,--4
                                            d.last_name,--5
                                            d.consultation_type,--6
                                            d.rating_point,--7
                                            array_to_string(array_agg(distinct s.specialties_code), ', ') as list_specialties --8
                                    from list_doctor d
                                        left join dr_doctorprofile_specialties s on s.doctor_profile_id = d.doctor_id
                                    group by 1,2,3,4,5,6,7,8
                                    order by first_name asc '''
                    list_live_doctor = gs.sql_execute(pool, sql_live_dr)
                    if list_live_doctor:
                        key = 'liveList'
                        tmp_res = []
                        for dr in list_live_doctor:
                            list_sp = []
                            for sp in dr[8].split(', '):
                                list_sp.append({'code': sp})
                            tmp_res.append({
                                "id": dr[0],
                                "avatar": dr[1],
                                "titleCode": dr[2],
                                "firstName": None if dr[3] in ('False', 'None', '') else dr[3],
                                "middleName": None if dr[4] in ('False', 'None', '') else dr[4],
                                "lastName": None if dr[5] in ('False', 'None', '') else dr[5],
                                "specialtyCodes": list_sp,
                                "isAvailableToCall": True,
                                "ratingPoint": dr[7]
                            })
                        res.update({key: tmp_res})
                    # done update live doctor
                    # get favourite list
                    patient_mngment_id = res_token[7]
                    sql = '''SELECT doctor_id as id FROM patient_doctor_favourite WHERE patient_id  = %s '''
                    favourite_count = gs.sql_execute(pool, sql, para_values=[patient_mngment_id])
                    ids = [0]
                    if favourite_count:
                        for line in favourite_count:
                            ids.append(line[0])
                    favourite_count = len(ids)
                    ids = random.sample(ids, 10 if favourite_count >= 10 else favourite_count)
                    idss = ','.join(str(x) for x in ids)

                    sql_list_info_dr = '''with list_doctor as
                                (
                                
                                select distinct um.id profile_id,
                                        dp.id doctor_id, 
                                        COALESCE(um.profile_image_url,'') profile_image_url,
                                        COALESCE(um.title,'') title,
                                        COALESCE(um.first_name,'') first_name,
                                        COALESCE(um.middle_name,'') middle_name,
                                        COALESCE(um.last_name,'') last_name,
                                        consultation_type,
                                        case when latest_activity_time > current_timestamp - interval '15 minutes' then True
                                                else False
                                        end onlineStatus,
                                        ua.activity_status
                                from doctor_profile dp
                                    left join user_mngment um on um.id = dp.user_mngment_id
                                    left join user_account ua on ua.user_mngment_id = um.id
                                where um.id in (%s) and ua.validation_status = 'active' and dp.hospital_profile_id IS NULL 
                                )
                                select d.profile_id,--0
                                        d.profile_image_url,--1
                                        d.title,--2
                                        d.first_name,--3
                                        d.middle_name,--4
                                        d.last_name,--5
                                        d.consultation_type,--6
                                        d.onlineStatus, --7
                                        d.activity_status, --8
                                        array_to_string(array_agg(distinct s.specialties_code), ', ') as list_specialties --9
                                from list_doctor d
                                    left join dr_doctorprofile_specialties s on s.doctor_profile_id = d.doctor_id
                                group by 1,2,3,4,5,6,7,8,9 ''' % (idss)
                    list_info_dr = gs.sql_execute(pool, sql_list_info_dr)
                    if list_info_dr:
                        tmp_res = []
                        i = 0
                        for dr in list_info_dr:
                            list_sp = []
                            i = i + 1
                            for sp in dr[9].split(', '):
                                list_sp.append({'code': sp})
                            tmp_res.append({
                                "id": dr[0],
                                "avatar": dr[1],
                                "titleCode": dr[2],
                                "firstName": None if dr[3] in ('False', 'None', '') else dr[3],
                                "middleName": None if dr[4] in ('False', 'None', '') else dr[4],
                                "lastName": None if dr[5] in ('False', 'None', '') else dr[5],
                                "specialtyCodes": list_sp,
                                "isAvailableToCall": True if dr[6] == 'live' and dr[7] == True and dr[8] == 'online' else False,
                            })
                        key = 'favouriteList'
                        res.update({key: tmp_res})
                        res['summary'].update({'favouriteListCount': favourite_count})

                    resp.update({'status_code': 200,
                                 'resq_body': res
                                 })
                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
        except Exception as e:
            logger.error('error_profile_patient_get_dr_shortlist %s' % str(e))
            client.captureException()

        return resp

    def profile_patient_get(self, data):
        logger.info('profile_patient_get %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get profile account failed"),
                        "msg": _("An error occurred. Please try to get profile account again."),
                        "code": "get_profile_account_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            res = {}
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    sql = '''select   title,--0
                                            profile_image_url,--1
                                            first_name,--2
                                            middle_name,--3
                                            last_name,--4
                                            complete_name,--5
                                            birthday,--6
                                            gender,--7
                                            ethnicity,--8
                                            highest_education_level,--9
                                            occupation,--10
                                            material_status,--11
                                            ua.email account_email,--12
                                            ua.phone account_phone,--13
                                            
                                            coalesce(is_primary, False) is_primary,--14
                                            home_phone,--15
                                            work_phone, --16
                                            work_phone,--17 mobile_phone
                                            uc.email,--18
                                            number,--19
                                            street,--20
                                            city,--21
                                            state,--22
                                            postcode,--23
                                            country,--24
                                            um.spoken_language --25

                                   from user_mngment um
                                        left join user_account ua on ua.user_mngment_id = um.id
                                        left join patient_profile pp on  um.id = pp.user_mngment_id 
                                        left join user_contact uc on um.id = uc.user_mngment_id  
                                        left join user_address uad on uad.user_contact_id = uc.id  
                                   where ua.id =%s '''
                    records = gs.sql_execute(pool, sql, para_values=[user_id])

                    if records:
                        res.update({
                            'titleCode': None if records[0][0] in ('False', 'None', '') else records[0][0],
                            'avatar': None if records[0][1] in ('False', 'None', '') else records[0][1],
                            'firstName': None if records[0][2] in ('False', 'None', '') else records[0][2],
                            'middleName': None if records[0][3] in ('False', 'None', '') else records[0][3],
                            'lastName': None if records[0][4] in ('False', 'None', '') else records[0][4],
                            # 'completeName': None if records[0][5] in ('False','None','') else records[0][0],
                            'dayOfBirth': records[0][6] and int(time.mktime(records[0][6].timetuple())) or None,
                            'genderCode': None if records[0][7] in ('False', 'None', '') else records[0][7],
                            'ethnicity': None if records[0][8] in ('False', 'None', '') else records[0][8],
                            'highestEducationLevelCode': None if records[0][9] in ('False', 'None', '') else records[0][9],
                            'occupation': None if records[0][10] in ('False', 'None', '') else records[0][10],
                            # 'maritalStatusCode': None if records[0][11] in ('False','None','') else records[0][11],
                            'contact': {
                                'defaultPhone': None if records[0][13] in ('False', 'None', '') else records[0][13],
                                'homephone': None if records[0][15] in ('False', 'None', '') else records[0][15],
                                'workphone': None if records[0][16] in ('False', 'None', '') else records[0][16],
                                # 'mobilePhone': None if records[0][17] in ('False','None','') else records[0][17],
                                'email': None if records[0][18] in ('False', 'None', '') else records[0][18],
                                'primaryAddress': {"number": None,
                                                   "street": None,
                                                   "cityCode": None,
                                                   "state": None,
                                                   "postCode": None,
                                                   "countryCode": None},
                                'alternateAddress': {"number": None,
                                                     "street": None,
                                                     "cityCode": None,
                                                     "state": None,
                                                     "postCode": None,
                                                     "countryCode": None}
                            },
                            'languageCode': None if records[0][25] in ('False', 'None', '') else records[0][25],
                        })
                        for line in records:
                            if line[14] == True:
                                res['contact']['primaryAddress'].update({
                                    "number": None if line[19] in ('False', 'None', '') else line[19],
                                    "street": None if line[20] in ('False', 'None', '') else line[20],
                                    "cityCode": None if line[21] in ('False', 'None', '') else line[21],
                                    "state": None if line[22] in ('False', 'None', '') else line[22],
                                    "postCode": None if line[23] in ('False', 'None', '') else line[23],
                                    "countryCode": None if line[24] in ('False', 'None', '') else line[24],
                                })
                            else:
                                res['contact']['alternateAddress'].update({
                                    "number": None if line[19] in ('False', 'None', '') else line[19],
                                    "street": None if line[20] in ('False', 'None', '') else line[20],
                                    "cityCode": None if line[21] in ('False', 'None', '') else line[21],
                                    "state": None if line[22] in ('False', 'None', '') else line[22],
                                    "postCode": None if line[23] in ('False', 'None', '') else line[23],
                                    "countryCode": None if line[24] in ('False', 'None', '') else line[24],
                                })
                    resp.update({'status_code': 200,
                                 'resq_body': res
                                 })
                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
        except Exception as e:
            logger.error('error_profile_patient_get %s' % str(e))
            client.captureException()

        return resp

    def profile_patient_setup(self, data):
        logger.info('profile_patient_setup %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Setup profile patient failed"),
                        "msg": _("An error occurred. Please try to setup profile again."),
                        "code": "setup_profile_patient_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_mngmentid = res_token[7]
                    # update table user_mngment
                    dayOfBirth = data['dayOfBirth'] and datetime.fromtimestamp(long(data['dayOfBirth'])).strftime('%Y-%m-%d') or None
                    sql_update = '''UPDATE user_mngment
                                SET title        = %s, 
                                    first_name   = %s, 
                                    middle_name  = %s, 
                                    last_name    = %s, 
                                    birthday     = %s, 
                                    gender       = %s,
                                    spoken_language = %s
                                WHERE id =%s '''
                    gs.sql_execute(pool, sql_update, commit=True, para_values=(data['titleCode'],
                                                                               data['firstName'],
                                                                               data['middleName'],
                                                                               data['lastName'],
                                                                               dayOfBirth,
                                                                               data['genderCode'],
                                                                               data['languageCode'],
                                                                               user_mngmentid))
                    # update table patient_profile
                    sql = '''SELECT user_mngment_id FROM patient_profile WHERE user_mngment_id =%s '''
                    records_checkexist = gs.sql_execute(pool, sql, para_values=(user_mngmentid,))
                    if records_checkexist:
                        sql_update = '''UPDATE patient_profile
                                    SET occupation                = %s, 
                                        ethnicity                 = %s, 
                                        highest_education_level   = %s
                                    WHERE user_mngment_id =%s '''
                        gs.sql_execute(pool, sql_update, commit=True, para_values=(data['occupation'],
                                                                                   data['ethnicity'],
                                                                                   data['highestEducationLevelCode'],
                                                                                   user_mngmentid))
                    else:
                        sql_insert = '''INSERT INTO patient_profile(occupation,
                                                                    ethnicity,
                                                                    highest_education_level,
                                                                    user_mngment_id) 
                                    VALUES (%s,%s,%s,%s)'''
                        gs.sql_execute(pool, sql_insert, commit=True, para_values=(data['occupation'],
                                                                                   data['ethnicity'],
                                                                                   data['highestEducationLevelCode'],
                                                                                   user_mngmentid))

                    # update table user_contact
                    if data['contact']:
                        sql = '''SELECT user_mngment_id FROM user_contact WHERE user_mngment_id =%s '''
                        records_checkexist = gs.sql_execute(pool, sql, para_values=[user_mngmentid])

                        if records_checkexist:
                            sql_update = '''UPDATE user_contact
                                        SET home_phone   = %s, 
                                            work_phone   = %s, 
                                            email        = %s
                                        WHERE user_mngment_id =%s 
                                        RETURNING id'''
                            user_contactid = \
                            gs.sql_execute(pool, sql_update, commit=True, para_values=(data['contact']['homephone'],
                                                                                       data['contact']['workphone'],
                                                                                       data['contact']['email'],
                                                                                       user_mngmentid))[0][0]
                        else:
                            sql_insert = '''INSERT INTO user_contact(home_phone, work_phone, email, user_mngment_id) 
                                        VALUES(%s,%s,%s,%s) 
                                        RETURNING id '''
                            user_contactid = \
                            gs.sql_execute(pool, sql_insert, commit=True, para_values=(data['contact']['homephone'],
                                                                                       data['contact']['workphone'],
                                                                                       data['contact']['email'],
                                                                                       user_mngmentid))[0][0]

                        # uuuuuu
                        if data['contact']['primaryAddress']:
                            sql = '''SELECT user_contact_id FROM user_address WHERE user_contact_id =%s AND is_primary = true '''
                            records_checkexist = gs.sql_execute(pool, sql, para_values=[user_contactid])
                            if records_checkexist:
                                sql_update = ''' UPDATE user_address SET
                                            number   = %s,
                                            street   = %s,
                                            city     = %s,
                                            state    = %s,
                                            postcode = %s,
                                            country  = %s
                                            WHERE  user_contact_id = %s 
                                            AND is_primary = true'''
                                gs.sql_execute(pool, sql_update, commit=True,
                                               para_values=(data['contact']['primaryAddress']['number'],
                                                            data['contact']['primaryAddress']['street'],
                                                            data['contact']['primaryAddress']['cityCode'],
                                                            data['contact']['primaryAddress']['state'],
                                                            data['contact']['primaryAddress']['postCode'],
                                                            data['contact']['primaryAddress']['countryCode'],
                                                            records_checkexist[0][0]))
                            else:

                                sql_insert = '''INSERT INTO user_address(number,street,state,city,country,postcode,user_contact_id,is_primary)
                                                VALUES(%s,%s,%s,%s,%s,%s,%s,%s)'''

                                gs.sql_execute(pool, sql_insert, commit=True,
                                               para_values=(data['contact']['primaryAddress']['number'],
                                                            data['contact']['primaryAddress']['street'],
                                                            data['contact']['primaryAddress']['state'],
                                                            data['contact']['primaryAddress']['cityCode'],
                                                            data['contact']['primaryAddress']['countryCode'],
                                                            data['contact']['primaryAddress']['postCode'],
                                                            user_contactid,
                                                            'true'))

                        else:
                            resp = {'status_code': 401,
                                    'resq_body':
                                        {
                                            "code": 'missing_primary_contact',
                                            "title": 'Missing Primary Contact',
                                            "msg": 'The primary address should only be one'
                                        }
                                    }
                            return resp

                        if data['contact']['alternateAddress']:
                            sql = '''SELECT user_contact_id FROM user_address WHERE user_contact_id =%s AND is_primary = false '''
                            records_checkexist = gs.sql_execute(pool, sql, para_values=[user_contactid])
                            if records_checkexist:
                                sql_update = ''' UPDATE user_address SET
                                                    number   = %s,
                                                    street   = %s,
                                                    state    = %s,
                                                    city     = %s,
                                                    country  = %s,
                                                    postcode = %s
                                                WHERE  user_contact_id = %s AND is_primary = false'''
                                records_checkexist = gs.sql_execute(pool, sql_update, commit=True, para_values=(
                                data['contact']['alternateAddress']['number'],
                                data['contact']['alternateAddress']['street'],
                                data['contact']['alternateAddress']['state'],
                                data['contact']['alternateAddress']['cityCode'],
                                data['contact']['alternateAddress']['countryCode'],
                                data['contact']['alternateAddress']['postCode'],
                                records_checkexist[0][0]))
                            else:
                                sql_insert = '''INSERT INTO user_address(number,street,state,city,country,postcode,user_contact_id,is_primary)
                                                VALUES(%s,%s,%s,%s,%s,%s,%s,%s)'''
                                records_checkexist = gs.sql_execute(pool, sql_insert, commit=True, para_values=(
                                data['contact']['alternateAddress']['number'],
                                data['contact']['alternateAddress']['street'],
                                data['contact']['alternateAddress']['state'],
                                data['contact']['alternateAddress']['cityCode'],
                                data['contact']['alternateAddress']['countryCode'],
                                data['contact']['alternateAddress']['postCode'],
                                user_contactid,
                                'false'))

                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     "code": "success",
                                     "title": _("Success"),
                                     "msg": _("Setup profile Successful.")
                                 }
                                 })
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_profile_patient_post %s' % str(e))
        return resp

    # MP5.5 API for patient to mark a specific doctor as favourite
    def profile_patient_mark_doctor(self, data):
        logger.info('profile_patient_mark_doctor %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Mark doctor failed"),
                        "msg": _("An error occurred. Please try to mark doctor again."),
                        "code": "mark_doctor_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    patient_mngment_id = res_token[7]
                    doctor_mngment_id = data.get('doctor_id')

                    dateTimeInput = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    sql_insert = '''INSERT INTO patient_doctor_favourite (create_date,patient_id,doctor_id)
                                                VALUES (%s,%s,%s)
                                                            '''
                    gs.sql_execute(pool, sql_insert, commit=True, para_values=(dateTimeInput,
                                                                               patient_mngment_id,
                                                                               doctor_mngment_id
                                                                               ))
                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     "code": "success",
                                     "title": _("Success"),
                                     "msg": _("Mark doctor successful.")
                                 }
                                 })
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_profile_patient_mark_doctor %s' % str(e))

        return resp

    # MP5.6 API for patient to unmark a specific doctor as favourite
    def profile_patient_unmark_doctor(self, data):
        logger.info('profile_patient_unmark_doctor %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Unmark doctor failed"),
                        "msg": _("An error occurred. Please try to unmark doctor again."),
                        "code": "unmark_doctor_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    patient_mngment_id = res_token[7]
                    doctor_mngment_id = data.get('doctor_id')
                    sql_delete = '''delete from patient_doctor_favourite where patient_id = %s and doctor_id = %s ''' % (
                    patient_mngment_id, doctor_mngment_id)

                    gs.sql_execute(pool, sql_delete, commit=True)
                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     "code": "success",
                                     "title": _("Success"),
                                     "msg": _("Unmark doctor successful.")
                                 }
                                 })
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_profile_patient_unmark_doctor %s' % str(e))

        return resp

    # MP5.7 API for getting doctor list by with sort and filter criterias (support pagination)
    def profile_patient_get_dr_list_by_sort_filter(self, data):
        logger.info('profile_patient_get_dr_list_by_sort_filter %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get doctor list by sort and filter criterias failed"),
                        "msg": _("An error occurred. Please try to get doctor list again."),
                        "code": "get_dr_list_by_sort_and_filter_criterias"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            pagination = data.get('pagination')
            sortBy = data.get('sortBy', 'firstname_asc')
            keyword = data.get('keyword')
            filterBy = data.get('filterBy')

            res = {
                'pagination': {
                    "resultCount": 0,
                    "totalCount": 0,
                    "nextItemId": 0
                },
                'doctorList': [],
            }
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                start = 0
                end = 0
                if res_token:
                    user_id = res_token[1]
                    nextItemId = int(pagination.get('nextItemId', 0))
                    requestCount = int(pagination.get('requestCount'))
                    dicOrderBy = {'lastname_asc': '\norder by last_name asc',
                                  'lastname_desc': '\norder by last_name desc',
                                  'price_asc': '\norder by consultation_fee asc',
                                  'price_desc': '\norder by consultation_fee desc',
                                  'rate_asc': '\norder by rating_point asc',
                                  'rate_desc': '\norder by rating_point desc',
                                  'firstname_asc': '\norder by first_name asc'}
                    orderBy = dicOrderBy[sortBy]

                    if not pagination.get('nextItemId') or pagination.get('nextItemId') == 0:
                        full_list_ids = self.generate_pagination_list(user_id, 'doctor_profile',
                                                                      context={'filterBy': filterBy, 'sortBy': orderBy,
                                                                               'keyword': keyword, 'api': 'MP5_7'})
                        start = 0
                    else:
                        sql = '''select list_id 
                                from user_account_pagination
                                where user_account_id = %s
                                    and table_pagination = '%s'
                                ''' % (user_id, 'doctor_profile')
                        full_list_ids = gs.sql_execute(pool, sql)
                        if full_list_ids:
                            full_list_ids = eval(full_list_ids[0][0])
                            start = full_list_ids.index(nextItemId)
                        else:
                            full_list_ids = self.generate_pagination_list(user_id, 'doctor_profile',
                                                                          context={'filterBy': filterBy,
                                                                                   'sortBy': sortBy, 'keyword': keyword,
                                                                                   'api': 'MP5_7'})
                            start = 0

                    if not full_list_ids:
                        resp.update({'status_code': 200,
                                     'resq_body': res
                                     })
                        return resp

                    if (start + requestCount) <= len(full_list_ids):
                        end = start + requestCount
                    else:
                        end = len(full_list_ids)

                    select_list_ids = full_list_ids[start:end]
                    res['pagination']['totalCount'] = len(full_list_ids)
                    res['pagination']['nextItemId'] = full_list_ids[end] if end < len(full_list_ids) else full_list_ids[
                        end - 1]
                    res['pagination']['resultCount'] = len(select_list_ids)

                    # get list_specialties
                    if len(select_list_ids) > 0:
                        select_list_ids = select_list_ids + [-1, -1]
                        sql = '''with list_doctor as
                                (
                                select um.id profile_id,
                                        dp.id doctor_id, 
                                        COALESCE(um.profile_image_url,'') profile_image_url,
                                        COALESCE(um.title,'') title,
                                        COALESCE(um.first_name,'') first_name,
                                        COALESCE(um.middle_name,'') middle_name,
                                        COALESCE(um.last_name,'') last_name,
                                        dp.consultation_type,
                                        drsp.specialties_code,
                                        dp.consultation_fee,
                                        mtsp.name specialties_name,
                                        (COALESCE(dp.sum_rating_point,0) / COALESCE(dp.count_rating_point,1)) as rating_point, --7
                                        case when latest_activity_time > current_timestamp - interval '15 minutes' then True
                                            else False
                                        end onlineStatus, --8
                                        ua.activity_status
                                from doctor_profile dp
                                left join user_mngment um on um.id = dp.user_mngment_id
                                left join dr_doctorprofile_specialties drsp on drsp.doctor_profile_id = dp.id
                                left join meta_dr_specialties mtsp on mtsp.code = drsp.specialties_code
                                left join user_account ua on ua.user_mngment_id = um.id
                                where um.id in %s and ua.validation_status = 'active' 
                                )
                                select d.profile_id,--0
                                        d.profile_image_url,--1
                                        d.title,--2
                                        d.first_name,--3
                                        d.middle_name,--4
                                        d.last_name,--5
                                        d.consultation_type,--6
                                        d.rating_point, --7
                                        d.consultation_fee, -- 8
                                        d.onlineStatus, --9
                                        d.activity_status, --10
                                        array_to_string(array_agg(distinct s.specialties_code), ', ') as list_specialties --11
                                from list_doctor d
                                left join dr_doctorprofile_specialties s on s.doctor_profile_id = d.doctor_id
                                group by 1,2,3,4,5,6,7,8,9,10,11 %s''' % (str(tuple(select_list_ids)), orderBy)

                        list_doctor = gs.sql_execute(pool, sql)
                        if list_doctor:
                            tmp_res = []
                            for dr in list_doctor:
                                list_sp = []
                                for sp in dr[11].split(', '):
                                    list_sp.append({'code': sp})
                                tmp_res.append({
                                    "id": dr[0],
                                    "avatar": dr[1],
                                    "titleCode": dr[2],
                                    "firstName": None if dr[3] in ('False', 'None', '') else dr[3],
                                    "middleName": None if dr[4] in ('False', 'None', '') else dr[4],
                                    "lastName": None if dr[5] in ('False', 'None', '') else dr[5],
                                    "specialtyCodes": list_sp,
                                    "isAvailableToCall": True if dr[6] == 'live' and dr[9] == True and dr[10] == 'online' else False,
                                    "ratingPoint": dr[7]
                                })
                            res['doctorList'] = tmp_res

                    resp.update({'status_code': 200,
                                 'resq_body': res
                                 })
                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
        except Exception as e:
            logger.error('profile_patient_get_dr_list_by_sort_filter %s' % str(e))

        return resp

    # MP5.8 API for getting live doctor list (support pagination)
    def profile_patient_get_live_dr_list(self, data):
        logger.info('profile_patient_get_live_dr_list %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get live doctor list failed"),
                        "msg": _("An error occurred. Please try to get doctor list again."),
                        "code": "profile_patient_get_live_dr_list_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            nextItemId = int(data.get('nextItemId', 0))
            requestCount = int(data.get('requestCount'))

            if not requestCount:
                return resp

            res = {
                'pagination': {
                    "resultCount": 0,
                    "totalCount": 0,
                    "nextItemId": 0
                },
                'doctorList': [],
            }
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                start = 0
                end = 0
                if res_token:
                    user_id = res_token[1]
                    if not nextItemId or nextItemId == 0:
                        full_list_ids = self.generate_pagination_list(user_id, 'doctor_profile_live',
                                                                      context={'api': 'MP5_8'})
                        start = 0
                    else:
                        sql = '''select list_id 
                                from user_account_pagination
                                where user_account_id = %s
                                    and table_pagination = '%s'
                                ''' % (user_id, 'doctor_profile_live')
                        full_list_ids = gs.sql_execute(pool, sql)
                        if full_list_ids:
                            full_list_ids = eval(full_list_ids[0][0])
                            start = full_list_ids.index(nextItemId)
                        else:
                            full_list_ids = self.generate_pagination_list(user_id, 'doctor_profile_live',
                                                                          context={'api': 'MP5_8'})
                            start = 0

                    if not full_list_ids:
                        resp.update({'status_code': 200,
                                     'resq_body': res
                                     })
                        return resp

                    if (start + requestCount) <= len(full_list_ids):
                        end = start + requestCount
                    else:
                        end = len(full_list_ids)

                    select_list_ids = full_list_ids[start:end]
                    res['pagination']['totalCount'] = len(full_list_ids)
                    res['pagination']['nextItemId'] = full_list_ids[end] if end < len(full_list_ids) else full_list_ids[
                        end - 1]
                    res['pagination']['resultCount'] = len(select_list_ids)

                    #print select_list_ids
                    # get list_specialties
                    if len(select_list_ids) > 0:
                        select_list_ids = select_list_ids + [-1, -1]
                        sql = '''with list_doctor as
                                (
                                select um.id profile_id,
                                        dp.id doctor_id, 
                                        COALESCE(um.profile_image_url,'') profile_image_url,
                                        COALESCE(um.title,'') title,
                                        COALESCE(um.first_name,'') first_name,
                                        COALESCE(um.middle_name,'') middle_name,
                                        COALESCE(um.last_name,'') last_name,
                                        dp.consultation_type,
                                        drsp.specialties_code,
                                        dp.consultation_fee,
                                        mtsp.name specialties_name,
                                        (COALESCE(dp.sum_rating_point,0) / COALESCE(dp.count_rating_point,1)) as rating_point --7
                                from doctor_profile dp
                                left join user_mngment um on um.id = dp.user_mngment_id
                                left join dr_doctorprofile_specialties drsp on drsp.doctor_profile_id = dp.id
                                left join meta_dr_specialties mtsp on mtsp.code = drsp.specialties_code
                                left join user_account ua on ua.user_mngment_id = um.id
                                where um.id in %s and ua.validation_status = 'active'  
                                )
                                select d.profile_id,--0
                                        d.profile_image_url,--1
                                        d.title,--2
                                        d.first_name,--3
                                        d.middle_name,--4
                                        d.last_name,--5
                                        d.consultation_type,--6
                                        d.rating_point, --7
                                        d.consultation_fee, -- 8
                                        array_to_string(array_agg(distinct s.specialties_code), ', ') as list_specialties --9
                                from list_doctor d
                                left join dr_doctorprofile_specialties s on s.doctor_profile_id = d.doctor_id
                                group by 1,2,3,4,5,6,7,8,9
                                order by first_name asc''' % (str(tuple(select_list_ids)))

                        list_doctor = gs.sql_execute(pool, sql)
                        if list_doctor:
                            tmp_res = []
                            for dr in list_doctor:
                                list_sp = []
                                for sp in dr[9].split(', '):
                                    list_sp.append({'code': sp})
                                tmp_res.append({
                                    "id": dr[0],
                                    "avatar": dr[1],
                                    "titleCode": dr[2],
                                    "firstName": None if dr[3] in ('False', 'None', '') else dr[3],
                                    "middleName": None if dr[4] in ('False', 'None', '') else dr[4],
                                    "lastName": None if dr[5] in ('False', 'None', '') else dr[5],
                                    "specialtyCodes": list_sp,
                                    "isAvailableToCall": True,
                                    "ratingPoint": dr[7]
                                })
                            res['doctorList'] = tmp_res

                    resp.update({'status_code': 200,
                                 'resq_body': res
                                 })
                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
        except Exception as e:
            logger.error('profile_patient_get_live_dr_list %s' % str(e))

        return resp

    # MP5.9 API for getting live doctor list with sort and filter criterias (support pagination)
    def profile_patient_get_live_dr_list_by_sort_filter(self, data):
        logger.info('profile_patient_get_live_dr_list_by_sort_filter %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get live doctor list by sort and filter criterias failed"),
                        "msg": _("An error occurred. Please try to get doctor list again."),
                        "code": "get_live_dr_list_by_sort_and_filter_criterias"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            pagination = data.get('pagination')
            sortBy = data.get('sortBy', 'firstname_asc')
            keyword = data.get('keyword')
            filterBy = data.get('filterBy')

            res = {
                'pagination': {
                    "resultCount": 0,
                    "totalCount": 0,
                    "nextItemId": 0
                },
                'doctorList': [],
            }
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                start = 0
                end = 0
                if res_token:
                    user_id = res_token[1]
                    nextItemId = int(pagination.get('nextItemId', 0))
                    requestCount = int(pagination.get('requestCount'))
                    dicOrderBy = {'lastname_asc': '\norder by last_name asc',
                                  'lastname_desc': '\norder by last_name desc',
                                  'price_asc': '\norder by consultation_fee asc',
                                  'price_desc': '\norder by consultation_fee desc',
                                  'rate_asc': '\norder by rating_point asc',
                                  'rate_desc': '\norder by rating_point desc',
                                  'firstname_asc': '\norder by first_name asc'}
                    orderBy = dicOrderBy[sortBy]

                    if not pagination.get('nextItemId') or pagination.get('nextItemId') == 0:
                        full_list_ids = self.generate_pagination_list(user_id, 'doctor_profile_live',
                                                                      context={'filterBy': filterBy, 'sortBy': orderBy,
                                                                               'keyword': keyword, 'api': 'MP5_9'})
                        start = 0
                    else:
                        sql = '''select list_id 
                                from user_account_pagination
                                where user_account_id = %s
                                    and table_pagination = '%s'
                                ''' % (user_id, 'doctor_profile_live')
                        full_list_ids = gs.sql_execute(pool, sql)
                        if full_list_ids:
                            full_list_ids = eval(full_list_ids[0][0])
                            start = full_list_ids.index(nextItemId)
                        else:
                            full_list_ids = self.generate_pagination_list(user_id, 'doctor_profile_live',
                                                                          context={'filterBy': filterBy,
                                                                                   'sortBy': sortBy, 'keyword': keyword,
                                                                                   'api': 'MP5_9'})
                            start = 0

                    if not full_list_ids:
                        resp.update({'status_code': 200,
                                     'resq_body': res
                                     })
                        return resp

                    if (start + requestCount) <= len(full_list_ids):
                        end = start + requestCount
                    else:
                        end = len(full_list_ids)

                    select_list_ids = full_list_ids[start:end]
                    res['pagination']['totalCount'] = len(full_list_ids)
                    res['pagination']['nextItemId'] = full_list_ids[end] if end < len(full_list_ids) else full_list_ids[
                        end - 1]
                    res['pagination']['resultCount'] = len(select_list_ids)

                    # get list_specialties
                    if len(select_list_ids) > 0:
                        select_list_ids = select_list_ids + [-1, -1]
                        sql = '''with list_doctor as
                                (
                                select um.id profile_id,
                                        dp.id doctor_id, 
                                        COALESCE(um.profile_image_url,'') profile_image_url,
                                        COALESCE(um.title,'') title,
                                        COALESCE(um.first_name,'') first_name,
                                        COALESCE(um.middle_name,'') middle_name,
                                        COALESCE(um.last_name,'') last_name,
                                        dp.consultation_type,
                                        drsp.specialties_code,
                                        dp.consultation_fee,
                                        mtsp.name specialties_name,
                                        (COALESCE(dp.sum_rating_point,0) / COALESCE(dp.count_rating_point,1)) as rating_point --7
                                from doctor_profile dp
                                left join user_mngment um on um.id = dp.user_mngment_id
                                left join dr_doctorprofile_specialties drsp on drsp.doctor_profile_id = dp.id
                                left join meta_dr_specialties mtsp on mtsp.code = drsp.specialties_code
                                left join user_account ua on ua.user_mngment_id = um.id
                                where um.id in %s and ua.validation_status = 'active' 
                                )
                                select d.profile_id,--0
                                        d.profile_image_url,--1
                                        d.title,--2
                                        d.first_name,--3
                                        d.middle_name,--4
                                        d.last_name,--5
                                        d.consultation_type,--6
                                        d.rating_point, --7
                                        d.consultation_fee, -- 8
                                        array_to_string(array_agg(distinct s.specialties_code), ', ') as list_specialties --9
                                from list_doctor d
                                left join dr_doctorprofile_specialties s on s.doctor_profile_id = d.doctor_id
                                group by 1,2,3,4,5,6,7,8,9 %s''' % (str(tuple(select_list_ids)), orderBy)
                        
                        list_doctor = gs.sql_execute(pool, sql)
                        if list_doctor:
                            tmp_res = []
                            for dr in list_doctor:
                                list_sp = []
                                for sp in dr[9].split(', '):
                                    list_sp.append({'code': sp})
                                tmp_res.append({
                                    "id": dr[0],
                                    "avatar": dr[1],
                                    "titleCode": dr[2],
                                    "firstName": None if dr[3] in ('False', 'None', '') else dr[3],
                                    "middleName": None if dr[4] in ('False', 'None', '') else dr[4],
                                    "lastName": None if dr[5] in ('False', 'None', '') else dr[5],
                                    "specialtyCodes": list_sp,
                                    "isAvailableToCall": True,
                                    "ratingPoint": dr[7]
                                })
                            res['doctorList'] = tmp_res

                    resp.update({'status_code': 200,
                                 'resq_body': res
                                 })
                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
        except Exception as e:
            logger.error('profile_patient_get_live_dr_list_by_sort_filter %s' % str(e))

        return resp

    # MP4.7 API to get patient's note and attachment list of specific medical record field
    def profile_patient_get_medicalRecordFieldCode(self, data):
        logger.info('profile_patient_get_medicalRecordFieldCode %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Patient get medical record field failed"),
                        "msg": _("An error occurred. Please try to get doctor list again."),
                        "code": "profile_patient_get_medicalRecordFieldCode_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            nextItemId = int(data.get('nextItemId', 0))
            requestCount = int(data.get('requestCount'))
            medicalRecordFieldCode = data.get('medicalRecordFieldCode')
            if not requestCount:
                return resp

            res = {
                'pagination': {
                    "resultCount": 0,
                    "totalCount": 0,
                    "nextItemId": 0
                }
            }
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                start = 0
                end = 0
                if res_token:
                    user_id = res_token[1]
                    if not nextItemId or nextItemId == 0:
                        full_list_ids = self.generate_pagination_list(user_id, 'appointment_medical_record',
                                                                      context={'recordField': medicalRecordFieldCode})
                        start = 0
                    else:
                        sql = '''select list_id 
                                from user_account_pagination
                                where user_account_id = %s
                                    and table_pagination = '%s'
                                ''' % (user_id, 'appointment_medical_record')
                        full_list_ids = gs.sql_execute(pool, sql)
                        if full_list_ids:
                            full_list_ids = eval(full_list_ids[0][0])
                            start = full_list_ids.index(nextItemId)
                        else:
                            full_list_ids = self.generate_pagination_list(user_id, 'appointment_medical_record',
                                                                          context={
                                                                              'recordField': medicalRecordFieldCode})
                            start = 0

                    if not full_list_ids:
                        resp.update({'status_code': 200,
                                     'resq_body': res
                                     })
                        return resp

                    if (start + requestCount) <= len(full_list_ids):
                        end = start + requestCount
                    else:
                        end = len(full_list_ids)

                    select_list_ids = full_list_ids[start:end]
                    res['pagination']['totalCount'] = len(full_list_ids)
                    res['pagination']['nextItemId'] = full_list_ids[end] if end < len(full_list_ids) else full_list_ids[
                        end - 1]
                    res['pagination']['resultCount'] = len(select_list_ids)

                    # get list_specialties
                if len(select_list_ids) > 0:
                    select_list_ids = select_list_ids + [-1, -1]
                    sql = '''select mr.id, --0
                                    mr.note, --1
                                    mr.write_date rm_write_date,--2
                                    um.id as doctor_profile_id,--3
                                    mr.create_uid as doctor_account_id,--4
                                    um.first_name doctor_first_name,--5
                                    um.middle_name doctor_middle_name,--6
                                    um.last_name doctor_last_name,--7
                                    um.title doctor_title,--8
                                    att.url,--9
                                    att.file_name,--10
                                    att.file_size,--11
                                    att.file_extension,--12
                                    att.write_date,--13
                                    mr.code--14
                            from appointment_medical_record mr 
                                left join attachment att on att.res_id = mr.id 
                                  and (att.res_model = 'appointment_medical_record' or att.res_model = 'user_mngment')
                                left join user_account ua on ua.id = mr.create_uid
                                left join user_mngment um on um.id = ua.user_mngment_id
                            where mr.id in %s''' % str(tuple(select_list_ids))
                    list_medical_record = gs.sql_execute(pool, sql)
                    if list_medical_record:
                        res_list = {}

                        for item in list_medical_record:
                            if item[0] not in res_list:
                                res_list.update({item[0]: {
                                    "latestUpdateTime": item[2] and int(time.mktime(item[2].timetuple())) or None,
                                    "note": item[1],
                                    "note_id": item[0],
                                    "medicalRecordFieldCode": item[14],
                                    "attachments": [],
                                    "uploader": {
                                        "id": item[3],
                                        "userId": item[4],
                                        "firstName": item[5],
                                        "middleName": item[6],
                                        "lastName": item[7],
                                        "titleCode": item[8],
                                    }
                                }})
                            if item[9]:
                                res_list[item[0]]['attachments'].append({"url": item[9],
                                                                          "fileName": item[10],
                                                                          "fileSize": self.general_business.bytesToSize(
                                                                              item[11]),
                                                                          "fileExtension": item[12],
                                                                          "latestUpdateTime": item[13] and int(
                                                                              time.mktime(
                                                                                  item[13].timetuple())) or None,
                                                                          })
                        res['list'] = res_list.values()

                    resp.update({'status_code': 200,
                                 'resq_body': res
                                 })
                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
        except Exception as e:
            logger.error('profile_patient_get_medicalRecordFieldCode %s' % str(e))

        return resp

    # MP4.8 API for patient to get general medical record information
    def profile_patient_get_general_medicalRecord(self, data):
        logger.info('profile_patient_get_general_medicalRecord %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Patient get general medical record failed"),
                        "msg": _("An error occurred. Please try to get doctor list again."),
                        "code": "profile_patient_get_general_medicalRecord_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    sql = '''select height, --0
                                    weight, --1
                                    blood_type, --2
                                    smoking, --3
                                    alcohol_intake, --4
                                    sleep_rest, --5
                                    exercise, --6
                                    stress_coping, --7
                                    roles_relationship, --8
                                    psychococial, --9
                                    other_substance --10
                            from general_medical_record
                            where user_mngment_id = %s'''
                    records = gs.sql_execute(pool, sql, para_values=[res_token[7]])

                    res = {
                                "height": None,
                                "weight": None,
                                "bloodType": None,
                                "smoking": None,
                                "alcoholIntake": None,
                                "otherSubstances": None,
                                "sleepRest": None,
                                "exercise": None,
                                "stressCoping": None,
                                "rolesRelationships": None,
                                "psychococial": None
                            }

                    if records:
                        res.update({
                                "height": records[0][0],
                                "weight": records[0][1],
                                "bloodType": records[0][2],
                                "smoking": records[0][3],
                                "alcoholIntake": records[0][4],
                                "otherSubstances": records[0][10],
                                "sleepRest": records[0][5],
                                "exercise": records[0][6],
                                "stressCoping": records[0][7],
                                "rolesRelationships": records[0][8],
                                "psychococial": records[0][9]

                        })

                    resp.update({'status_code': 200,
                                 'resq_body':res
                                 })

                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
        except Exception as e:
            logger.error('error_profile_patient_get_general_medicalRecord %s' % str(e))

        return resp

    # MP4.9 API for patient to setup general medical record information
    def profile_patient_setup_general_medicalRecord(self, data):
        logger.info('profile_patient_setup_general_medicalRecord %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Patient setup general medical record failed"),
                        "msg": _("An error occurred. Please try to get doctor list again."),
                        "code": "profile_patient_setup_general_medicalRecord_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')

            height = data.get('height','unknown')
            weight = data.get('weight','unknown')
            blood_type = data.get('bloodType','unknown')
            smoking = data.get('smoking','unknown')
            alcohol_intake = data.get('alcoholIntake','unknown')
            sleep_rest = data.get('sleepRest','unknown')
            exercise = data.get('exercise','unknown')
            stress_coping = data.get('stressCoping','unknown')
            roles_relationship = data.get('rolesRelationships','unknown')
            psychococial = data.get('psychococial','unknown')
            other_substance = data.get('otherSubstances','unknown')

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    sql = '''select id
                            from general_medical_record
                            where user_mngment_id = %s'''
                    
                    records = gs.sql_execute(pool, sql, para_values=[res_token[7]])

                    if records:
                        sql_update = '''update general_medical_record
                                        set write_date = current_timestamp, height=%s, 
                                            weight=%s, blood_type=%s, smoking=%s, alcohol_intake=%s, sleep_rest=%s, 
                                            exercise=%s, stress_coping=%s, roles_relationship=%s, psychococial=%s, 
                                            other_substance=%s
                                        where id = %s '''
                        gs.sql_execute(pool, sql_update,commit=True, para_values=(height,weight,blood_type,smoking,alcohol_intake,sleep_rest,exercise,stress_coping,roles_relationship,psychococial,other_substance ,records[0][0],))
                    else:
                        current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                        sql_insert = ''' insert into general_medical_record(
                                            create_date, write_date, user_mngment_id, height, weight, 
                                            blood_type, smoking, alcohol_intake, sleep_rest, exercise, stress_coping, 
                                            roles_relationship, psychococial, other_substance)
                                        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) '''
                        
                        gs.sql_execute(pool, sql_insert,commit=True, para_values=[current_time,current_time,res_token[7] ,height,weight,blood_type,smoking,alcohol_intake,sleep_rest,exercise,stress_coping,roles_relationship,psychococial,other_substance])    
                    resp.update({'status_code': 200,
                                 'resq_body': {
                                    "code": "success",
                                    "title": _("Sucess"),
                                    "msg": _("You setup general medical record successfully")
                                 }
                                 })

                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
        except Exception as e:
            logger.error('error_profile_patient_setup_general_medicalRecord %s' % str(e))

        return resp

    # MP4.10 API for patient to search other patients by email or phone
    def profile_patient_search_otherppl(self, data):
        logger.info('profile_patient_search_otherppl %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Patient search other patient failed"),
                        "msg": _("An error occurred. Please try to get doctor list again."),
                        "code": "profile_patient_search_otherppl_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')

            keyword = data.get('keyword')
            requestCount = int(data['pagination'].get('requestCount',10))
            nextItemId = int(data['pagination'].get('nextItemId',0))

            res = {
                'pagination': {
                    "resultCount": 0,
                    "totalCount": 0,
                    "nextItemId": 0
                },
                'patientList': [],
            }
            
            if not keyword or len(keyword) == 0:
                resp.update({'status_code': 200,
                                     'resq_body': res
                                     })
                return resp

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    
                    if not nextItemId or nextItemId == 0:
                        select_list_ids = []
                        full_list_ids = self.generate_pagination_list(user_id, 'patient_search_patient',
                                                                      context={'api': 'MP4_10', 'searchKeyword': keyword})
                        start, end = 0, requestCount
                    else:
                        sql = '''select list_id 
                                from user_account_pagination
                                where user_account_id = %s
                                    and table_pagination = %s
                                '''
                        full_list_ids = gs.sql_execute(pool, sql, para_values=(user_id, 'patient_search_patient'))
                        if full_list_ids:
                            full_list_ids = eval(full_list_ids[0][0])
                            start, end = full_list_ids.index(nextItemId), full_list_ids.index(nextItemId) + requestCount
                        else:
                            full_list_ids = self.generate_pagination_list(user_id, 'patient_search_patient',
                                                                          context={'api': 'MP4_10', 'searchKeyword': keyword})
                            start, end = 0, requestCount

                    if not full_list_ids:
                        resp.update({'status_code': 200,
                                     'resq_body': res
                                     })
                        return resp

                    end = end if end <= len(full_list_ids) else len(full_list_ids)
                    select_list_ids = full_list_ids[start:end]

                    res['pagination']['totalCount'] = len(full_list_ids)
                    res['pagination']['nextItemId'] = full_list_ids[end] if end < len(full_list_ids) else full_list_ids[
                        end - 1]
                    res['pagination']['resultCount'] = len(select_list_ids)
                    
                    # get list_specialties
                    if len(select_list_ids) > 0:
                        select_list_ids = select_list_ids + [-1, -1]
                        sql = '''select ua.id, --0
                                        umng.first_name, --1
                                        umng.middle_name, --2
                                        umng.last_name, --3
                                        umng.profile_image_url,--4
                                        ua.email, --5
                                        umng.gender, --6
                                        umng.birthday, --7
                                        uad.number,--8
                                        uad.street,--9
                                        uad.city,--10
                                        uad.state,--11
                                        uad.postcode,--12
                                        uad.country --13
                                    from user_mngment umng
                                    left join user_account ua on ua.user_mngment_id = umng.id
                                    left join user_contact uc on uc.user_mngment_id = umng.id
                                    left join user_address uad on uad.user_contact_id = uc.id and is_primary = True
                                    where umng.id in %s
                                    order by umng.first_name asc ''' % (str(tuple(select_list_ids)))
                        
                        list_patient = gs.sql_execute(pool, sql)
                        if list_patient:
                            patientList = []
                            for line in list_patient:
                                patient = {
                                    'userId': line[0],
                                    "firstName": line[1],
                                    "middleName": line[2],
                                    "lastName": line[3],
                                    "avatarURL": line[4],
                                    "email": line[5],
                                    "primaryAddress": {
                                        "number": line[8] if line[8] else None,
                                        "street": line[9] if line[9] else None,
                                        "cityCode": line[10] if line[10] else None,
                                        "state": line[11] if line[11] else None,
                                        "postCode": line[12] if line[12] else None,
                                        "countryCode": line[13] if line[13] else None
                                    },
                                    "genderCode":line[6]
                                }

                                if line[7]:
                                    patient.update({
                                        'dayOfBirth': int(time.mktime(line[7].timetuple()))
                                    })
                                else:
                                    patient.update({
                                        'dayOfBirth': None
                                    })
                                patientList.append(patient)
                            res['patientList'] = patientList

                            resp.update({'status_code': 200,
                                 'resq_body':res
                                 })
                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
        except Exception as e:
            logger.error('error_profile_patient_search_otherppl_failed %s' % str(e))

        return resp

    # MP6.3 API for patient to get the upcoming appointment list
    def profile_patient_get_appointment_upcominglist(self, data):
        logger.info('profile_patient_get_appointment_upcominglist %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get appointment upcominglist failed"),
                        "msg": _("An error occurred. Please try to get doctor list again."),
                        "code": "get_appointment_upcominglist_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:

                    sql = '''select apm.id, --0
                                        apm.schedule_date, --1
                                        apm.state, --2
                                        umng.id, --3
                                        umng.first_name, --4
                                        umng.middle_name, --5
                                        umng.last_name, --6
                                        umng.profile_image_url,--7
                                        apm.write_date --8
                                    from appointment apm
                                    left join user_account ua on apm.doctor_id = ua.id
                                    left join user_mngment umng on  umng.id = ua.user_mngment_id
                                    where apm.patient_id = %s 
                                        and (state in ('confirmed_by_patient','suspended') 
                                        or (apm.type != 'live' and state='confirmed')
                                        or (apm.type = 'live' and state='confirmed' and COALESCE(is_accepted,False)=True)) order by apm.schedule_date asc ''' % (res_token[1])
                    records = gs.sql_execute(pool, sql)
                    upcominglist = []
                    for line in records:
                        upcoming = {
                            'id': line[0],
                            'scheduleTime': int(time.mktime(line[1].timetuple())),
                            'stateCode': line[2],
                            'doctor': {
                                'id': line[3],
                                'firstName': None if line[4] in ('False', 'None', '') else line[4],
                                'middleName': None if line[5] in ('False', 'None', '') else line[5],
                                'lastName': None if line[6] in ('False', 'None', '') else line[6],
                                'avatarUrl': line[7]
                            }
                        }
                        if line[8]:
                            upcoming.update({
                                'latestUpdateTime': int(time.mktime(line[8].timetuple()))
                            })
                        else:
                            upcoming.update({
                                'latestUpdateTime': None
                            })
                        upcominglist.append(upcoming)

                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     'upcomingList': upcominglist
                                 }
                                 })
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_profile_patient_get_appointment_upcominglist %s' % str(e))

        return resp

        # MP6.4 API for patient to get the previous appointment list

    def profile_patient_get_appointment_previouslist(self, data):
        logger.info('profile_patient_get_appointment_previouslist %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get appointment previouslist failed"),
                        "msg": _("An error occurred. Please try to get doctor list again."),
                        "code": "get_appointment_previouslist_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn

            user_token = data.get('user_token')
            specialtyCode = data.get('specialtyCode')
            requestCount = int(data.get('requestCount'))
            nextItemId = int(data.get('nextItemId'))

            res = {
                'pagination': {
                    "resultCount": 0,
                    "totalCount": 0,
                    "nextItemId": 0
                },
                'previousList': [],
            }

            user_token = data.get('user_token')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    if not nextItemId or nextItemId == 0:
                        select_list_ids = []
                        full_list_ids = self.generate_pagination_list(user_id, 'previous_appointments',
                                                                      context={'api': 'MP6_4'})
                        start, end = 0, requestCount
                    else:
                        sql = '''select list_id 
                                from user_account_pagination
                                where user_account_id = %s
                                    and table_pagination = %s
                                '''
                        full_list_ids = gs.sql_execute(pool, sql, para_values=(user_id, 'previous_appointments'))
                        if full_list_ids:
                            full_list_ids = eval(full_list_ids[0][0])
                            start, end = full_list_ids.index(nextItemId), full_list_ids.index(nextItemId) + requestCount
                        else:
                            full_list_ids = self.generate_pagination_list(user_id, 'previous_appointments',
                                                                          context={'api': 'MP6_4'})
                            start, end = 0, requestCount

                    if not full_list_ids:
                        resp.update({'status_code': 200,
                                     'resq_body': res
                                     })
                        return resp

                    end = end if end <= len(full_list_ids) else len(full_list_ids)
                    select_list_ids = full_list_ids[start:end]

                    res['pagination']['totalCount'] = len(full_list_ids)
                    res['pagination']['nextItemId'] = full_list_ids[end] if end < len(full_list_ids) else full_list_ids[
                        end - 1]
                    res['pagination']['resultCount'] = len(select_list_ids)

                    # get list_specialties
                    if len(select_list_ids) > 0:
                        select_list_ids = select_list_ids + [-1, -1]
                        sql = '''select apm.id, --0
                                        apm.schedule_date, --1
                                        apm.state, --2
                                        umng.id, --3
                                        umng.first_name, --4
                                        umng.middle_name, --5
                                        umng.last_name, --6
                                        umng.profile_image_url,--7
                                        apm.write_date --8
                                    from appointment apm
                                    left join user_account ua on apm.doctor_id = ua.id
                                    left join user_mngment umng on  umng.id = ua.user_mngment_id
                                    where apm.id in %s and state in ('rescheduled_by_patient', 'successful_finished','free_finished','canceled_by_doctor','canceled_by_patient') 
                                    order by apm.schedule_date desc ''' % (str(tuple(select_list_ids)))
                        
                        list_doctor = gs.sql_execute(pool, sql)
                        if list_doctor:
                            previousList = []
                            for line in list_doctor:
                                previous = {
                                    'id': line[0],
                                    'scheduleTime': int(time.mktime(line[1].timetuple())),
                                    'stateCode': line[2],
                                    'doctor': {
                                        'id': line[3],
                                        'firstName': None if line[4] in ('False', 'None', '') else line[4],
                                        'middleName': None if line[5] in ('False', 'None', '') else line[5],
                                        'lastName': None if line[6] in ('False', 'None', '') else line[6],
                                        'avatarUrl': line[7]
                                    }
                                }
                                if line[8]:
                                    previous.update({
                                        'latestUpdateTime': int(time.mktime(line[8].timetuple()))
                                    })
                                else:
                                    previous.update({
                                        'latestUpdateTime': None
                                    })
                                previousList.append(previous)
                            res['previousList'] = previousList

                    resp.update({'status_code': 200,
                                 'resq_body': res
                                 })
                                 
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_profile_patient_get_appointment_previouslist %s' % str(e))

        return resp

    # MP6.5 API for getting appointment list summary (for My Health home)
    def profile_patient_get_appointment_sumarylist(self, data):
        logger.info('profile_patient_get_appointment_sumarylist %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get appointment sumarylist failed"),
                        "msg": _("An error occurred. Please try to get doctor list again."),
                        "code": "get_appointment_sumarylist_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            res = {}
            user_token = data.get('user_token')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    # Count upcoming and previous appointment
                    sql_count_upcoming = '''SELECT count(apm.id) as number_count --0
                                    FROM appointment apm
                                    WHERE apm.patient_id = %s 
                                    and (state in ('confirmed_by_patient','suspended')
                                    or (apm.type != 'live' and state='confirmed')
                                    or (apm.type = 'live' and state='confirmed' and COALESCE(is_accepted,False)=True))
                                    ''' % (res_token[1])
                    count_upcoming = gs.sql_execute(pool, sql_count_upcoming)
                    res.update({"upcomingVisitCount": int(count_upcoming[0][0])})

                    sql_count_previous = '''SELECT count(apm.id) --0
                                    from appointment apm
                                    where apm.patient_id = %s 
                                      and state in ('rescheduled_by_patient', 'successful_finished','free_finished','canceled_by_doctor','canceled_by_patient')
                                   ''' % (res_token[1])
                    count_previous = gs.sql_execute(pool, sql_count_previous)
                    res.update({"previousVisitCount": int(count_previous[0][0])})
                    # TODO: add is_accepted in sql apm.type != 'live' and state='processing' / (state='processing' and COALESCE(is_accepted,False)=True))
                    # Get 2 upcoming apm
                    upcominglist = []
                    sql = '''SELECT apm.id, --0
                                        apm.schedule_date, --1
                                        apm.state, --2
                                        umng.id, --3
                                        umng.first_name, --4
                                        umng.middle_name, --5
                                        umng.last_name, --6
                                        umng.profile_image_url,--7
                                        apm.write_date --8
                                    from appointment apm
                                    left join user_account ua on apm.doctor_id = ua.id
                                    left join user_mngment umng on  umng.id = ua.user_mngment_id
                                    where apm.patient_id = %s 
                                    and (state in ('confirmed_by_patient','suspended')
                                    or (apm.type != 'live' and state='confirmed')
                                    or (apm.type = 'live' and state='confirmed' and COALESCE(is_accepted,False)=True))
                                    order by apm.schedule_date asc
                                    limit 2 ''' % (res_token[1])
                    records = gs.sql_execute(pool, sql)
                    for line in records:
                        sql_get_specialties = ''' SELECT ds.specialties_code, ds.years_of_exp
                                                  from dr_doctorprofile_specialties ds
                                                  inner join doctor_profile dp on ds.doctor_profile_id = dp.id
												  inner join user_mngment um on um.id = dp.user_mngment_id	
                                                  WHERE um.id = '%s' ''' % (line[3])
                        records_specialties = gs.sql_execute(pool, sql_get_specialties)
                        specialtyCodes = []
                        for specialties in records_specialties:
                            specialtyCode = {
                                "code": specialties[0],
                                "yearsOfExp": specialties[1]
                            }
                            specialtyCodes.append(specialtyCode)

                        upcoming = {
                            'id': line[0],
                            'scheduleTime': int(time.mktime(line[1].timetuple())),
                            'stateCode': line[2],
                            'doctor': {
                                'id': line[3],
                                'firstName': None if line[4] in ('False', 'None', '') else line[4],
                                'middleName': None if line[5] in ('False', 'None', '') else line[5],
                                'lastName': None if line[6] in ('False', 'None', '') else line[6],
                                'avatarUrl': line[7],
                                'specialtyCodes': specialtyCodes
                            }
                        }
                        if line[8]:
                            upcoming.update({
                                'latestUpdateTime': int(time.mktime(line[8].timetuple()))
                            })
                        else:
                            upcoming.update({
                                'latestUpdateTime': None
                            })
                        upcominglist.append(upcoming)
                    res.update({'upcomingList': upcominglist})

                    # Get 2 previous apm
                    previousList = []
                    sql = '''SELECT apm.id, --0
                                        apm.schedule_date, --1
                                        apm.state, --2
                                        umng.id, --3
                                        umng.first_name, --4
                                        umng.middle_name, --5
                                        umng.last_name, --6
                                        umng.profile_image_url,--7
                                        apm.write_date --8
                                    from appointment apm
                                    left join user_account ua on apm.doctor_id = ua.id
                                    left join user_mngment umng on  umng.id = ua.user_mngment_id
                                    where apm.patient_id = %s 
                                      and state in ('rescheduled_by_patient', 'successful_finished','free_finished','canceled_by_doctor','canceled_by_patient')
                                    order by apm.schedule_date desc
                                    limit 2 ''' % (res_token[1])

                    records = gs.sql_execute(pool, sql)
                    for line in records:
                        sql_get_specialties = ''' SELECT ds.specialties_code, ds.years_of_exp
                                                  from dr_doctorprofile_specialties ds
                                                  inner join doctor_profile dp on ds.doctor_profile_id = dp.id
												  inner join user_mngment um on um.id = dp.user_mngment_id	
                                                  WHERE um.id = '%s' ''' % (line[3])
                        records_specialties = gs.sql_execute(pool, sql_get_specialties)
                        specialtyCodes = []
                        for specialties in records_specialties:
                            specialtyCode = {
                                "code": specialties[0],
                                "yearsOfExp": specialties[1]
                            }
                            specialtyCodes.append(specialtyCode)
                        previous = {
                            'id': line[0],
                            'scheduleTime': int(time.mktime(line[1].timetuple())),
                            'stateCode': line[2],
                            'doctor': {
                                'id': line[3],
                                'firstName': None if line[4] in ('False', 'None', '') else line[4],
                                'middleName': None if line[5] in ('False', 'None', '') else line[5],
                                'lastName': None if line[6] in ('False', 'None', '') else line[6],
                                'avatarUrl': line[7],
                                'specialtyCodes': specialtyCodes
                            }

                        }
                        if line[8]:
                            previous.update({
                                'latestUpdateTime': int(time.mktime(line[8].timetuple()))
                            })
                        else:
                            previous.update({
                                'latestUpdateTime': None
                            })
                        previousList.append(previous)
                    res.update({'previousList': previousList})

                    resp.update({'status_code': 200,
                                 'resq_body': res
                                 })
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_profile_patient_get_appointment_sumarylist %s' % str(e))

        return resp

    # WD5.5 API for doctor to get patient's note and attachment list of specific medical record field 
    def profile_patient_get_medical_record(self, data):
        logger.info('profile_patient_get_medical_record %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {
            'status_code': 401,
            'resq_body': {
                "title": _("Get medical record failed"),
                "msg": _("An error occurred. Please try to request again."),
                "code": "get_medical_record_failed"
            }
        }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            patientId = data.get('patientId')
            requestCount = int(data.get('requestCount'))
            nextItemId = data.get('nextItemId')
            recordField = data.get('recordField')

            if not patientId:
                return resp
            res = {
                'pagination': {
                    "resultCount": 0,
                    "totalCount": 0,
                    "nextItemId": 0
                },
                'list': []
            }
            res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
            start = 0
            end = 0
            if res_token:
                user_id = res_token[1]
                if not nextItemId or nextItemId == 0:
                    select_list_ids = []
                    full_list_ids = self.generate_pagination_list(user_id, 'appointment_medical_record',
                                                                              context=data)
                    start, end = 0, requestCount
                else:
                    sql = '''select list_id 
                            from user_account_pagination
                            where user_account_id = %s
                                and table_pagination = '%s'
                            ''' % (user_id, 'appointment_medical_record')
                    full_list_ids = gs.sql_execute(pool, sql)
                    if full_list_ids:
                        full_list_ids = eval(full_list_ids[0][0])
                        # patient_business.update_pagination_list(user_id, 'doctor_profile', full_list_ids,{'recordField': recordField})
                        start, end = full_list_ids.index(nextItemId), full_list_ids.index(nextItemId) + requestCount
                    else:
                        full_list_ids = self.generate_pagination_list(user_id, 'appointment_medical_record',
                                                                                  context=data)
                        start, end = 0, requestCount
                if not full_list_ids:
                    resp.update({'status_code': 200,
                                 'resq_body': res
                                 })
                    return resp
                end = end if end <= len(full_list_ids) else len(full_list_ids)
                select_list_ids = full_list_ids[start:end]

                res['pagination']['totalCount'] = len(full_list_ids)
                res['pagination']['nextItemId'] = full_list_ids[end] if end < len(full_list_ids) else full_list_ids[
                    end - 1]
                res['pagination']['resultCount'] = len(select_list_ids)

                # get list_specialties
                if len(select_list_ids) > 0:
                    select_list_ids = select_list_ids + [-1, -1]
                    sql = '''select distinct apm.id amp_id, --0
                                    mr.note, --1
                                    mr.write_date rm_write_date,--2
                                    um.id as doctor_profile_id,--3
                                    ua.id as doctor_account_id,--4
                                    um.first_name doctor_first_name,--5
                                    um.middle_name doctor_middle_name,--6
                                    um.last_name doctor_last_name,--7
                                    um.title doctor_title,--8
                                    att.url,--9
                                    att.file_name,--10
                                    att.file_size,--11
                                    att.file_extension,--12
                                    att.write_date,--13
                                    mr.id,--14
                                    mr.code--15
                            from appointment_medical_record mr
                                left join attachment att on att.res_id = mr.id and (att.res_model = 'appointment_medical_record' or att.res_model = 'user_mngment')
                                left join user_account ua on ua.id = mr.create_uid
                                left join user_mngment um on um.id = ua.user_mngment_id
                                left join appointment apm on apm.id = mr.appointment_id
                            where mr.id in %s
                            group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
                            order by rm_write_date desc''' % str(tuple(select_list_ids))
                    
                    list_medical_record = gs.sql_execute(pool, sql)
                    if list_medical_record:
                        res_list = OrderedDict()
                        for item in list_medical_record:
                            if item[14] not in res_list:
                                res_list.update({item[14]: {
                                    "latestUpdateTime": item[2] and int(time.mktime(item[2].timetuple())) or None,
                                    "appointmentId": item[0],
                                    "note": item[1],
                                    "note_id": item[14],
                                    "medicalRecordFieldCode": item[15],
                                    "attachments": [],
                                    "uploader": {
                                        "id": item[3],
                                        "userId": item[4],
                                        "firstName": None if item[5] in ('False', 'None', '') else item[5],
                                        "middleName": None if item[6] in ('False', 'None', '') else item[6],
                                        "lastName": None if item[7] in ('False', 'None', '') else item[7],
                                        "titleCode": item[8]
                                    }
                                }})
                            if item[9]:
                                res_list[item[14]]['attachments'].append({"url": item[9],
                                                                          "fileName": item[10],
                                                                          "fileSize": self.general_business.bytesToSize(
                                                                              item[11]),
                                                                          "fileExtension": item[12],
                                                                          "latestUpdateTime": item[13] and int(
                                                                              time.mktime(
                                                                                  item[13].timetuple())) or None,
                                                                          })
                        res['list'] = res_list.values()

                resp.update({'status_code': 200,
                             'resq_body': res
                             })
            else:
                resp.update({'status_code': 203,
                             'resq_body':
                                 {
                                     "code": "expired_token",
                                     "title": _("Expired token"),
                                     "msg": _("Your login-token is expired, please try to login again")
                                 }
                             })
        except Exception as e:
            logger.error('error_profile_patient_get_medical_record %s' % str(e))
            client.captureException()
        return resp