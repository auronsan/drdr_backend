#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
- Using command "which gunicorn" to find locate of unicord module
- Change "locate" var
'''
from subprocess import call
locate = "/usr/local/bin/gunicorn"
call([locate, 'drdr_api:load_app("environment.conf")'])
