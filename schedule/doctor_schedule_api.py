import falcon
import json
import sys

sys.path.insert(0, "..")
try:
    from general import general_business
    from aws_management import aws_management
except ImportError as e:
    print('No Import ', e)
from doctor_schedule_business import doctor_schedule_business
import ConfigParser
import os

project_path =  str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
config = ConfigParser.RawConfigParser(allow_no_value=True)
config.read(project_path + '/' + "environment.conf")

AWS_BUCKET = config.get('environment', 's3_bucket')
BUCKET_URL_DEFAULT = AWS_BUCKET + '.' + config.get('environment', 's3_address')

status_code = {
    200: falcon.HTTP_200,
    401: falcon.HTTP_401,
    502: falcon.HTTP_502,
    203: falcon.HTTP_203
}


class doctor_schedule_api(object):
    def __init__(self, signal, erp_connection=False, var_contructor=False):
        self.business = doctor_schedule_business(signal, erp_connection, var_contructor)
        self.pool_conn = erp_connection

        if var_contructor:
            self.ERP_DB_HOST = var_contructor['ERP_DB_HOST']
            self.ERP_DB_PORT = var_contructor['ERP_DB_PORT']
            self.ERP_DB_USER = var_contructor['ERP_DB_USER']
            self.ERP_DB_PASS = var_contructor['ERP_DB_PASS']
            self.ERP_DB_NAME = var_contructor['ERP_DB_NAME']

        self.signal = signal
        self.general_business = general_business(erp_connection, var_contructor)

    def on_post(self, req, resp, setting_type=None):
        try:
            data = {}
            header = req.headers  # key cua header luon viet hoa
            all_header = self.general_business.get_header_request(header)
            lang = all_header.get('lang')
            _ = self.general_business.get_lang_instance(lang)
            data.update(all_header)

            # WD6.1 API for doctor to set his single availability (no recurring)
            if self.signal == 'doctor_schedule_set_singleday_availability':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Set single day availability failed"),
                    "msg": _("An error occurred. Please try to set again."),
                    "code": "set_single_day_availability_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    data.update(raw_data)
                    record = self.business.doctor_schedule_set_singleday_availability(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

                # WD6.2 API for doctor to set his daily-repeat availability
            elif self.signal == 'doctor_schedule_repeat_day_availability':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Repeat day availability failed"),
                    "msg": _("An error occurred. Please try to set again."),
                    "code": "repeat_day_availability_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    data.update(raw_data)
                    record = self.business.doctor_schedule_repeat_day_availability(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

                # WD6.3 API for doctor to set his weekly-repeat availability
            elif self.signal == 'doctor_schedule_repeat_week_availability':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Repeat week availability failed"),
                    "msg": _("An error occurred. Please try to set again."),
                    "code": "repeat_week_availability_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    data.update(raw_data)
                    record = self.business.doctor_schedule_repeat_week_availability(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

                # WD6.4 API for doctor to set his monthly-repeat availability
            elif self.signal == 'doctor_schedule_repeat_monthly_availability':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Repeat monthly availability failed"),
                    "msg": _("An error occurred. Please try to set again."),
                    "code": "repeat_monthly_availability_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    data.update(raw_data)
                    record = self.business.doctor_schedule_repeat_monthly_availability(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            # WD6.5 API for doctor to get all the availability list in a date range
            elif self.signal == 'doctor_schedule_get_availability_daterange':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Get availability list in a date range failed"),
                    "msg": _("An error occurred. Please try to get again."),
                    "code": "get_availability_daterange_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    data.update(raw_data)
                    print data
                    record = self.business.doctor_schedule_get_availability_daterange(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'doctor_schedule_edit_availability':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Edit availability failed"),
                    "msg": _("An error occurred. Please try to edit again."),
                    "code": "edit_availability_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = json.loads(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    data.update(raw_data)
                    record = self.business.doctor_schedule_edit_availability(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'doctor_schedule_remove_availability':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Remove availability failed"),
                    "msg": _("An error occurred. Please try to remove again."),
                    "code": "remove_availability_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = json.loads(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    data.update(raw_data)
                    record = self.business.doctor_schedule_remove_availability(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'doctor_check_appointment_existence':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Check appointment existence"),
                    "msg": _("An error occurred. Please try to remove again."),
                    "code": "check_appointment_existence_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = json.loads(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    data.update(raw_data)
                    record = self.business.doctor_check_appointment_existence(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            
        except Exception as e:
            print str(e)
