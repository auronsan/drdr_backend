# -*- coding: utf-8 -*-
from datetime import datetime
import time
import logging
from dateutil import rrule
from dateutil.relativedelta import relativedelta
from raven import Client
import ConfigParser
import sys
import re
import os

project_path =  str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
config = ConfigParser.RawConfigParser(allow_no_value=True)
config.read(project_path + '/' + "environment.conf")
sentry_key = config.get('environment', 'sentry_key')
client = Client(sentry_key)
logger = logging.getLogger('drdr_api')
sys.path.insert(0, "..")
try:
    from general import general_business
except ImportError as e:
    print('No Import ', e)


class doctor_schedule_business(object):
    def __init__(self, signal, erp_connection, var_contructor):
        self.pool_conn = erp_connection
        self.var_contructor = var_contructor
        self.signal = signal
        self.ERP_DB_HOST = var_contructor['ERP_DB_HOST']
        self.ERP_DB_PORT = var_contructor['ERP_DB_PORT']
        self.ERP_DB_USER = var_contructor['ERP_DB_USER']
        self.ERP_DB_PASS = var_contructor['ERP_DB_PASS']
        self.ERP_DB_NAME = var_contructor['ERP_DB_NAME']
        self.general_business = general_business(erp_connection, var_contructor)
        self.vi_ = self.general_business.vi_
        self.en_ = self.general_business.en_

    # WD6.1 API for doctor to set his single availability (no recurring)
    def doctor_schedule_set_singleday_availability(self, data):
        logger.info('doctor_schedule_set_singleday_availability %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Set single day availability failed"),
                        "msg": _("An error occurred. Please try to set again."),
                        "code": "set_single_day_availability_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            startTime = data.get('startTime', None)
            endTime = data.get('endTime', None)
            date = data.get('date', None)

            if startTime == None or endTime == None or date == None:
                return resp

            start_datetime = datetime.strptime(startTime, '%H:%M').time()
            end_datetime = datetime.strptime(endTime, '%H:%M').time()

            if start_datetime > end_datetime:
                resp.update({'status_code': 401,
                             'resq_body':
                                 {
                                     "code": "wrong_time",
                                     "title": _("Wrong time"),
                                     "msg": _("The start time is bigger than end time, please try to set again")
                                 }
                             })
                return resp

            if time.mktime(datetime.now().date().timetuple()) > long(date):
                resp.update({'status_code': 401,
                             'resq_body':
                                 {
                                     "code": "wrong_date",
                                     "title": _("Wrong date"),
                                     "msg": _("The current system date is bigger than your date, please try to set again")
                                 }
                             })
                return resp

            res = {}
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    duration = (end_datetime.hour * 60 + end_datetime.minute) - (
                    start_datetime.hour * 60 + start_datetime.minute)
                    currentTime = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    dayAvailability = datetime.fromtimestamp(long(date)).strftime('%Y-%m-%d')

                    sql = '''SELECT id 
                            FROM calendar_event 
                            WHERE user_id =%s AND recurrency = False 
                            and start_datetime = %s 
                            and end_datetime = %s 
                            and start_date = %s '''
                    records_checkexist = gs.sql_execute(pool, sql, para_values=(user_id,
                                                                                '%s %s' % (
                                                                                dayAvailability, start_datetime),
                                                                                '%s %s' % (
                                                                                dayAvailability, end_datetime),
                                                                                dayAvailability))

                    if not records_checkexist:
                        sql_insert = '''INSERT INTO calendar_event(active,start_datetime,end_datetime,duration,start_date,create_date,write_date,user_id,recurrency,final_date) 
                                        VALUES ('True',%s,%s,%s,%s,%s,%s,%s,'False',%s) '''
                        gs.sql_execute(pool, sql_insert, commit=True,
                                       para_values=('%s %s' % (dayAvailability, start_datetime),
                                                    '%s %s' % (dayAvailability, end_datetime),
                                                    duration,
                                                    dayAvailability,
                                                    currentTime,
                                                    currentTime,
                                                    user_id,
                                                    dayAvailability))

                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     "code": "success",
                                     "title": _("Success"),
                                     "msg": _("Set singleday availability successfuly")
                                 }
                                 })
                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
                    return resp

        except Exception as e:
            logger.error('error_doctor_schedule_set_singleday_availability %s' % str(e))
            client.captureException()
        return resp

    # WD6.2 API for doctor to set his daily-repeat availability
    def doctor_schedule_repeat_day_availability(self, data):
        logger.info('doctor_schedule_repeat_day_availability %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Repeat day availability failed"),
                        "msg": _("An error occurred. Please try to set again."),
                        "code": "repeat_day_availability_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            startTime = data.get('startTime', None)
            endTime = data.get('endTime', None)
            startDate = data.get('startDate', None)
            dayRepeatFrequency = data.get('dayRepeatFrequency', None)

            if startTime == None or endTime == None or startDate == None or dayRepeatFrequency == None:
                return resp

            start_datetime = datetime.strptime(startTime, '%H:%M').time()
            end_datetime = datetime.strptime(endTime, '%H:%M').time()

            if start_datetime > end_datetime:
                resp.update({'status_code': 401,
                             'resq_body':
                                 {
                                     "code": "wrong_time",
                                     "title": _("Wrong time"),
                                     "msg": _("The start time is bigger than end time, please try to set again")
                                 }
                             })
                return resp

            if time.mktime(datetime.now().date().timetuple()) > long(startDate):
                resp.update({'status_code': 401,
                             'resq_body':
                                 {
                                     "code": "wrong_date",
                                     "title": _("Wrong date"),
                                     "msg": _("The current system date is bigger than your date, please try to set again")
                                 }
                             })
                return resp

            if dayRepeatFrequency < 1 or dayRepeatFrequency > 30:
                resp.update({'status_code': 401,
                             'resq_body':
                                 {
                                     "code": "wrong_day_repeating_frequency",
                                     "title": _("Wrong day repeating frequency"),
                                     "msg": _("The current day repeating frequency is invalid, please try to set again")
                                 }
                             })
                return resp

            res = {}
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    duration = (end_datetime.hour * 60 + end_datetime.minute) - (
                    start_datetime.hour * 60 + start_datetime.minute)
                    currentTime = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    dayAvailability = datetime.fromtimestamp(long(startDate)).strftime('%Y-%m-%d')

                    sql = '''SELECT id 
                                FROM calendar_event 
                                WHERE user_id = %s 
                                  AND recurrency = True 
                                  AND rrule_type = 'daily' 
                                  and start_datetime = %s 
                                  and end_datetime = %s 
                                  and start_date= %s 
                                  and count = %s '''
                    records_checkexist = gs.sql_execute(pool, sql, para_values=(user_id,
                                                                                '%s %s' % (
                                                                                dayAvailability, start_datetime),
                                                                                '%s %s' % (
                                                                                dayAvailability, end_datetime),
                                                                                dayAvailability,
                                                                                dayRepeatFrequency))

                    if not records_checkexist:
                        final_date = self._get_recurrency_end_date(
                            datetime.strptime('''%s %s''' % (dayAvailability, end_datetime), '%Y-%m-%d %H:%M:00'),
                            'daily', dayRepeatFrequency)
                        sql_insert = '''INSERT INTO calendar_event(active,
                                                                  start_datetime,
                                                                  end_datetime,
                                                                  duration,
                                                                  start_date,
                                                                  create_date,
                                                                  write_date,
                                                                  user_id,
                                                                  count,
                                                                  recurrency,
                                                                  interval,
                                                                  rrule_type,
                                                                  allday,
                                                                  end_type,
                                                                  final_date) 
                                        VALUES ('True',%s,%s,%s,%s,%s,%s,%s,%s,'True',1,'daily','False','count',%s) '''
                        gs.sql_execute(pool, sql_insert, commit=True,
                                       para_values=('%s %s' % (dayAvailability, start_datetime),
                                                    '%s %s' % (dayAvailability, end_datetime),
                                                    duration,
                                                    dayAvailability,
                                                    currentTime,
                                                    currentTime,
                                                    user_id,
                                                    dayRepeatFrequency,
                                                    final_date))

                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     "code": "success",
                                     "title": _("Success"),
                                     "msg": _("Set repeat day availability successfuly")
                                 }
                                 })
            else:
                resp.update({'status_code': 203,
                             'resq_body':
                                 {
                                     "code": "expired_token",
                                     "title": _("Expired token"),
                                     "msg": _("Your login-token is expired, please try to login again")
                                 }
                             })
                return resp
        except Exception as e:
            logger.error('error_doctor_schedule_repeat_day_availability %s' % str(e))
            client.captureException()
        return resp

    # WD6.3 API for doctor to set his weekly-repeat availability
    def doctor_schedule_repeat_week_availability(self, data):
        logger.info('doctor_schedule_repeat_week_availability %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Repeat week availability failed"),
                        "msg": _("An error occurred. Please try to set again."),
                        "code": "repeat_week_availability_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            startTime = data.get('startTime', None)
            endTime = data.get('endTime', None)
            startDate = data.get('startDate', None)
            weekRepeatFrequency = data.get('weekRepeatFrequency', None)
            dayOfTheWeekToRepeat = data.get('dayOfTheWeekToRepeat', None)

            if startTime == None or endTime == None or startDate == None or weekRepeatFrequency == None or dayOfTheWeekToRepeat == None:
                return resp

            start_datetime = datetime.strptime(startTime, '%H:%M').time()
            end_datetime = datetime.strptime(endTime, '%H:%M').time()

            if start_datetime > end_datetime:
                resp.update({'status_code': 401,
                             'resq_body':
                                 {
                                     "code": "wrong_time",
                                     "title": _("Wrong time"),
                                     "msg": _("The start time is bigger than end time, please try to set again")
                                 }
                             })
                return resp

            if time.mktime(datetime.now().date().timetuple()) > time.mktime(
                    datetime.strptime(startDate, '%d-%m-%Y').timetuple()):
                resp.update({'status_code': 401,
                             'resq_body':
                                 {
                                     "code": "wrong_date",
                                     "title": _("Wrong date"),
                                     "msg": _("The current system date is bigger than your date, please try to set again")
                                 }
                             })
                return resp

            if weekRepeatFrequency < 1 or weekRepeatFrequency > 30:
                resp.update({'status_code': 401,
                             'resq_body':
                                 {
                                     "code": "wrong_week_repeating_frequency",
                                     "title": _("Wrong week repeating frequency"),
                                     "msg": _("The current day repeating frequency is invalid, please try to set again")
                                 }
                             })
                return resp

            matchObj = re.match(r"^([1-7],){1,7}$", '''%s,''' % dayOfTheWeekToRepeat)
            if not matchObj:
                resp.update({'status_code': 401,
                             'resq_body':
                                 {
                                     "code": "wrong_day_of_week",
                                     "title": _("Wrong day of week"),
                                     "msg": _("The day of week is invalid, please try to set again")
                                 }
                             })
                return resp

            res = {}
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    duration = (end_datetime.hour * 60 + end_datetime.minute) - (
                    start_datetime.hour * 60 + start_datetime.minute)
                    currentTime = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    dayAvailability = datetime.strptime(startDate, '%d-%m-%Y').date()
                    daily = [False, False, False, False, False, False, False]
                    for v in dayOfTheWeekToRepeat:
                        if v is not ",":
                            daily[int(v) - 1] = True

                    sql = '''SELECT id 
                                FROM calendar_event 
                                WHERE user_id =%s 
                                  AND recurrency = True 
                                  AND rrule_type = 'weekly' 
                                  and start_datetime = %s 
                                  and end_datetime = %s 
                                  and start_date = %s 
                                  and count = %s 
                                  and su = %s 
                                  and mo = %s 
                                  and tu = %s 
                                  and we = %s 
                                  and th = %s 
                                  and fr = %s 
                                  and sa = %s 
                                  '''
                    records_checkexist = gs.sql_execute(pool, sql, para_values=(user_id,
                                                                                '%s %s' % (
                                                                                dayAvailability, start_datetime),
                                                                                '%s %s' % (
                                                                                dayAvailability, end_datetime),
                                                                                dayAvailability,
                                                                                weekRepeatFrequency,
                                                                                daily[0],
                                                                                daily[1],
                                                                                daily[2],
                                                                                daily[3],
                                                                                daily[4],
                                                                                daily[5],
                                                                                daily[6]))

                    if not records_checkexist:
                        final_date = self._get_recurrency_end_date(
                            datetime.strptime('''%s %s''' % (dayAvailability, end_datetime), '%Y-%m-%d %H:%M:00'),
                            'weekly', weekRepeatFrequency)
                        sql_insert = '''INSERT INTO calendar_event(active,start_datetime,end_datetime,duration,start_date,create_date,write_date,user_id,count,su,mo,tu,we,th,fr,sa,recurrency,interval,rrule_type,allday,end_type,final_date) 
                                        VALUES ('True',%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,'True',1,'weekly','False','count',%s) '''
                        gs.sql_execute(pool, sql_insert, commit=True,
                                       para_values=('%s %s' % (dayAvailability, start_datetime),
                                                    '%s %s' % (dayAvailability, end_datetime),
                                                    duration,
                                                    dayAvailability,
                                                    currentTime,
                                                    currentTime,
                                                    user_id,
                                                    weekRepeatFrequency,
                                                    daily[0], daily[1], daily[2], daily[3], daily[4], daily[5],
                                                    daily[6],
                                                    final_date))

                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     "code": "success",
                                     "title": _("Success"),
                                     "msg": _("Set singleday availability successfuly")
                                 }
                                 })
                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
                return resp
        except Exception as e:
            logger.error('error_doctor_schedule_repeat_week_availability %s' % str(e))
            client.captureException()
        return resp

        # WD6.4 API for doctor to set his monthly-repeat availability

    def doctor_schedule_repeat_monthly_availability(self, data):
        # TODO: sql injection
        logger.info('doctor_schedule_repeat_monthly_availability %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Repeat monthly availability failed"),
                        "msg": _("An error occurred. Please try to set again."),
                        "code": "repeat_monthly_availability_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            startTime = data.get('startTime', None)
            endTime = data.get('endTime', None)
            startDate = data.get('startDate', None)
            monthRepeatFrequency = data.get('monthRepeatFrequency', None)
            repeatBy = data.get('repeatBy', None)

            if startTime == None or endTime == None or startDate == None or monthRepeatFrequency == None or repeatBy == None:
                return resp

            start_datetime = datetime.strptime(startTime, '%H:%M').time()
            end_datetime = datetime.strptime(endTime, '%H:%M').time()

            if start_datetime > end_datetime:
                resp.update({'status_code': 401,
                             'resq_body':
                                 {
                                     "code": "wrong_time",
                                     "title": _("Wrong time"),
                                     "msg": _("The start time is bigger than end time, please try to set again")
                                 }
                             })
                return resp

            if time.mktime(datetime.now().date().timetuple()) > time.mktime(
                    datetime.strptime(startDate, '%d-%m-%Y').timetuple()):
                resp.update({'status_code': 401,
                             'resq_body':
                                 {
                                     "code": "wrong_date",
                                     "title": _("Wrong date"),
                                     "msg": _("The current system date is bigger than your date, please try to set again")
                                 }
                             })
                return resp

            if monthRepeatFrequency < 1 or monthRepeatFrequency > 30:
                resp.update({'status_code': 401,
                             'resq_body':
                                 {
                                     "code": "wrong_week_repeating_frequency",
                                     "title": _("Wrong week repeating frequency"),
                                     "msg": _("The current day repeating frequency is invalid, please try to set again")
                                 }
                             })
                return resp

            matchObj = re.match(r"(dayOfTheMonth)|(dayOfTheWeek)", '''%s,''' % repeatBy)
            if not matchObj:
                resp.update({'status_code': 401,
                             'resq_body':
                                 {
                                     "code": "wrong_repeatBy_type",
                                     "title": _("Wrong repeatBy type"),
                                     "msg": _("The repeatBy is invalid, please try to set again")
                                 }
                             })
                return resp

            res = {}
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    duration = (end_datetime.hour * 60 + end_datetime.minute) - (
                    start_datetime.hour * 60 + start_datetime.minute)
                    currentTime = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    dayAvailability = datetime.strptime(startDate, '%d-%m-%Y').date()

                    if repeatBy == 'dayOfTheMonth':
                        month_by = 'date'
                        day = dayAvailability.day
                        sql = '''SELECT id 
                                FROM calendar_event 
                                WHERE user_id =%s 
                                AND recurrency = True 
                                AND rrule_type = 'weekly' 
                                and start_datetime = '%s %s' 
                                and end_datetime = '%s %s' 
                                and start_date = '%s' 
                                and count = %s 
                                and month_by = '%s' 
                                and day = %s ''' % (
                        user_id, dayAvailability, start_datetime, dayAvailability, end_datetime, dayAvailability,
                        monthRepeatFrequency, month_by, day)
                    else:
                        month_by = 'day'
                        byday = '-1'
                        sql = '''SELECT id 
                                FROM calendar_event 
                                WHERE user_id =%s 
                                AND recurrency = True 
                                AND rrule_type = 'weekly' 
                                and start_datetime = '%s %s' 
                                and end_datetime = '%s %s' 
                                and start_date = '%s' 
                                and count = %s 
                                and month_by = '%s' 
                                and byday = '%s' ''' % (
                        user_id, dayAvailability, start_datetime, dayAvailability, end_datetime, dayAvailability,
                        monthRepeatFrequency, month_by, byday)

                    records_checkexist = gs.sql_execute(pool, sql)

                    if not records_checkexist:
                        final_date = self._get_recurrency_end_date(
                            datetime.strptime('''%s %s''' % (dayAvailability, end_datetime), '%Y-%m-%d %H:%M:00'),
                            'monthly', monthRepeatFrequency)
                        if repeatBy == 'dayOfTheMonth':
                            month_by = 'date'
                            day = dayAvailability.day
                            sql_insert = '''INSERT INTO calendar_event(active,start_datetime,end_datetime,duration,start_date,create_date,write_date,user_id,count,month_by,day,recurrency,interval,rrule_type,allday,end_type,final_date) 
                                            VALUES ('True','%s %s','%s %s',%s,'%s','%s','%s',%s,%s,'%s',%s,'True',1,'monthly','False','count','%s') ''' % (
                            dayAvailability,
                            start_datetime,
                            dayAvailability,
                            end_datetime,
                            duration,
                            dayAvailability,
                            currentTime,
                            currentTime,
                            user_id,
                            monthRepeatFrequency,
                            month_by,
                            day, final_date)
                            gs.sql_execute(pool, sql_insert, commit=True)
                        else:
                            month_by = 'day'
                            byday = '-1'
                            week_list = dayAvailability.strftime("%A").lower()[:2]
                            sql_insert = '''INSERT INTO calendar_event(active,start_datetime,end_datetime,duration,start_date,create_date,write_date,user_id,count,month_by,byday,week_list,recurrency,interval,rrule_type,allday,end_type,final_date) 
                                            VALUES ('True','%s %s','%s %s',%s,'%s','%s','%s',%s,%s,'%s','%s','%s','True',1,'monthly','False','count','%s') ''' % (
                            dayAvailability,
                            start_datetime,
                            dayAvailability,
                            end_datetime,
                            duration,
                            dayAvailability,
                            currentTime,
                            currentTime,
                            user_id,
                            monthRepeatFrequency,
                            month_by,
                            byday,
                            week_list,
                            final_date)
                            gs.sql_execute(pool, sql_insert, commit=True)

                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     "code": "success",
                                     "title": _("Success"),
                                     "msg": _("Set singleday availability successfuly")
                                 }
                                 })
                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
                return resp
        except Exception as e:
            logger.error('error_doctor_schedule_repeat_monthly_availability %s' % str(e))
            client.captureException()
        return resp

    def _get_recurrency_end_date(self, start_datetime, rrule_type, count):
        delay, mult = {
            'daily': ('days', 1),
            'weekly': ('days', 7),
            'monthly': ('months', 1),
            'yearly': ('years', 1),
        }[rrule_type]
        final_date = start_datetime + relativedelta(**{delay: count * mult})
        return final_date

    def compute_rule_string(self, data):
        """
        Compute rule string according to value type RECUR of iCalendar from the values given.
        @param self: the object pointer
        @param data: dictionary of freq and interval value
        @return: string containing recurring rule (empty if no rule)
        """
        # data = {'fr': False, 'interval': 1, 'month_by': u'date', 'final_date': False, 'byday': False, 'id': 38, 'count': 5,
        #  'end_type': u'count', 'rrule_type': u'weekly', 'we': True, 'mo': False, 'tu': True, 'recurrency': True,
        #  'week_list': False, 'day': 0, 'th': False, 'su': False, 'sa': False}

        if data['interval'] and data['interval'] < 0:
            print -1
            # raise UserError(_('interval cannot be negative.'))
        if data['end_type'] == 'count' and int(data['count']) <= 0:
            # raise UserError(_('Event recurrence interval cannot be negative.'))
            print -2

        def get_week_string(freq, data):
            weekdays = ['mo', 'tu', 'we', 'th', 'fr', 'sa', 'su']
            if freq == 'weekly':
                byday = map(lambda x: x.upper(), filter(lambda x: data.get(x) and x in weekdays, data))
                if byday:
                    return ';BYDAY=' + ','.join(byday)
            return ''

        def get_month_string(freq, data):
            if freq == 'monthly':
                if data.get('month_by') == 'date' and (data.get('day') < 1 or data.get('day') > 31):
                    # raise UserError(_("Please select a proper day of the month."))
                    return -3
                if data.get('month_by') == 'day':  # Eg : Second Monday of the month
                    return ';BYDAY=' + data.get('byday') + data.get('week_list')
                elif data.get('month_by') == 'date':  # Eg : 16th of the month
                    return ';BYMONTHDAY=' + str(data.get('day'))
            return ''

        def get_end_date(data):
            if data.get('final_date'):
                data['end_date_new'] = ''.join((re.compile('\d')).findall(data.get('final_date'))) + 'T235959Z'

            return (data.get('end_type') == 'count' and (';COUNT=' + str(data.get('count'))) or '') + \
                   ((data.get('end_date_new') and data.get('end_type') == 'end_date' and (
                       ';UNTIL=' + data.get('end_date_new'))) or '')

        freq = data.get('rrule_type', False)  # day/week/month/year
        res = ''
        if freq:
            interval_srting = data.get('interval') and (';INTERVAL=' + str(data.get('interval'))) or ''
            res = 'FREQ=' + freq.upper() + get_week_string(freq, data) + interval_srting + get_end_date(
                data) + get_month_string(freq, data)

        return res

    # WD6.5 API for doctor to get all the availability list in a date range
    def doctor_schedule_get_availability_daterange(self, data):
        logger.info('doctor_schedule_get_availability_daterange %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get availability list in a date range failed"),
                        "msg": _("An error occurred. Please try to get again."),
                        "code": "get_availability_daterange_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            startDate = data.get('startDate', None)
            endDate = data.get('endDate', None)

            if not startDate or not endDate:
                return resp

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    start_date = datetime.strptime(startDate, '%d-%m-%Y').date()
                    end_date = datetime.strptime(endDate, '%d-%m-%Y').date()

                    if start_date > end_date:
                        resp.update({'status_code': 401,
                                     'resq_body':
                                         {
                                             "code": "wrong_time",
                                             "title": _("Wrong time"),
                                             "msg": _("The start date is bigger than end date, please try to set again")
                                         }
                                     })
                        return resp

                    allList = []
                    list_date = []

                    sql_single = '''select id,
                                        start_datetime::date start_date,
                                        start_datetime::time,
                                        end_datetime::time
                                    from calendar_event
                                    where recurrency = False
                                    and active = True
                                    and user_id = %s
                                    and start_datetime::date >= %s
                                    and start_datetime::date <= %s '''
                    single_record = gs.sql_execute(pool, sql_single, para_values=(user_id,
                                                                                  start_date.strftime('%Y-%m-%d'),
                                                                                  end_date.strftime('%Y-%m-%d')))
                    for single in single_record:
                        if single[1].strftime('%d-%m-%Y') not in list_date:
                            list_date.append(single[1].strftime('%d-%m-%Y'))
                            allList.append({"date": single[1].strftime('%d-%m-%Y'),
                                            "availability": [{
                                                "id": str(int(single[0])),
                                                "startTime": single[2].strftime('%H:%M:%S'),
                                                "endTime": single[3].strftime('%H:%M:%S')
                                            }]})

                        else:
                            for t in allList:
                                if t['date'] == single[1].strftime('%d-%m-%Y'):
                                    t['availability'].append({
                                        "id": str(int(single[0])),
                                        "startTime": single[2].strftime('%H:%M:%S'),
                                        "endTime": single[3].strftime('%H:%M:%S')
                                    })

                    sql_loop = '''with raw_data as
                                                (
                                                select count, fr, tu, we, mo, interval, recurrency, byday, week_list, rrule_type, month_by, end_type, th, final_date, sa, su, id, day, start_datetime, start_datetime::time,end_datetime::time,
                                                start_datetime::date + case
                                                                            when rrule_type = 'daily' then count
                                                                            when rrule_type = 'weekly' then count*7
                                                                            when rrule_type = 'monthly' then count*30
                                                                        end	check_date
                                                from calendar_event 
                                                where recurrency = True
                                                  and active = True
                                                  and recurrent_id is null
                                                  and user_id = %s
                                                )
                                                select * from raw_data
                                                where check_date::date >= %s '''
                    loop_record = gs.sql_execute(pool, sql_loop, para_values=(user_id,
                                                                              start_date.strftime('%Y-%m-%d')))
                    for loop in loop_record:
                        now = datetime.now()
                        now = datetime(now.year, now.month, now.day, 0, 0, 0)
                        id = int(loop[16])
                        sql = '''select start_datetime::DATE from calendar_event where recurrent_id = %s''' % id
                        exdate = gs.sql_execute(pool, sql)
                        # all_for = loop[18]
                        start = loop[19]
                        stop = loop[20]
                        vals = {'count': loop[0],
                                'fr': loop[1],
                                'tu': loop[2],
                                'we': loop[3],
                                'mo': loop[4],
                                'interval': loop[5],
                                'recurrency': loop[6],
                                'byday': loop[7],
                                'week_list': loop[8],
                                'rrule_type': loop[9],
                                'month_by': loop[10],
                                'end_type': loop[11],
                                'th': loop[12],
                                'final_date': loop[13].strftime('%Y-%m-%d'),
                                'sa': loop[14],
                                'su': loop[15],
                                'id': loop[16],
                                'day': loop[17]}
                        rrule_string = self.compute_rule_string(vals)
                        rset1 = rrule.rrulestr(rrule_string,
                                               dtstart=start_date if start_date > loop[18].date() else loop[18].date(),
                                               forceset=True)
                        if exdate:
                            for ex in exdate:
                                ex = datetime(ex[0].year, ex[0].month, ex[0].day, 0, 0, 0)
                                rset1._exdate.append(ex)
                        for d in rset1:
                            if d.date() > end_date:
                                continue
                            if d.strftime('%d-%m-%Y') not in list_date:
                                list_date.append(d.strftime('%d-%m-%Y'))
                                allList.append({"date": d.strftime('%d-%m-%Y'),
                                                "availability": [{
                                                    "id": str(int(loop[16])) + '_' + d.strftime(
                                                        '%Y%m%d') + start.strftime('%H%M%S'),
                                                    "startTime": start.strftime('%H:%M:%S'),
                                                    "endTime": stop.strftime('%H:%M:%S')
                                                }]})

                            else:
                                for t in allList:
                                    if t['date'] == d.strftime('%d-%m-%Y'):
                                        t['availability'].append({
                                            "id": str(int(loop[16])) + '_' + d.strftime('%Y%m%d') + start.strftime(
                                                '%H%M%S'),
                                            "startTime": start.strftime('%H:%M:%S'),
                                            "endTime": stop.strftime('%H:%M:%S')
                                        })

                    resp.update({'status_code': 200,
                                 'resq_body': {'list': allList}})
                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
                return resp
        except Exception as e:
            logger.error('error_doctor_schedule_get_availability_daterange %s' % str(e))
            client.captureException()
        return resp

    def doctor_schedule_edit_availability(self, data):
        logger.info('doctor_schedule_edit_availability %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Edit availability failed"),
                        "msg": _("An error occurred. Please try to edit again."),
                        "code": "edit_availability_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            newStartTime = data.get('newStartTime', None)
            newEndTime = data.get('newEndTime', None)
            schedule_id = data.get('id', None)
            raw_date = data.get('date', None)
            editEveryRecurring = data.get('editEveryRecurring', None)

            if not newStartTime or not newEndTime or not schedule_id or editEveryRecurring == None or not raw_date:
                return resp

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    is_recurring = False
                    schedule_id = schedule_id.split('_')
                    vitural_id = False
                    if len(schedule_id) > 1:
                        is_recurring = True
                        vitural_id = schedule_id[1]
                    real_id = int(schedule_id[0])
                    date = datetime.strptime(raw_date, '%d-%m-%Y').strftime('%Y-%m-%d')
                    new_start_time = datetime.strptime(newStartTime, '%H:%M').time()  # .strftime('%Y-%m-%d')
                    new_end_time = datetime.strptime(newEndTime, '%H:%M').time()  # .strftime('%Y-%m-%d')

                    if new_start_time > new_end_time:
                        resp.update({'status_code': 401,
                                     'resq_body':
                                         {
                                             "code": "wrong_time",
                                             "title": _("Wrong time"),
                                             "msg": _("The start time is bigger than end time, please try to set again")
                                         }
                                     })
                        return resp

                    currentTime = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    duration = (new_end_time.hour * 60 + new_end_time.minute) - (
                    new_start_time.hour * 60 + new_start_time.minute)
                    if (is_recurring == True and editEveryRecurring == True) or is_recurring == False:
                        if editEveryRecurring == True and vitural_id:
                            sql = '''select start_datetime::date from calendar_event where id = %s'''
                            date = gs.sql_execute(pool, sql, para_values=[real_id])[0][0].strftime('%Y-%m-%d')

                        sql_udpate = '''update calendar_event 
                                          set start_datetime = %s, 
                                              end_datetime = %s, 
                                              duration = %s,
                                              write_date = %s
                                        where id = %s'''
                        gs.sql_execute(pool, sql_udpate, commit=True, para_values=('%s %s' % (date, newStartTime),
                                                                                   '%s %s' % (date, newEndTime),
                                                                                   duration,
                                                                                   currentTime,
                                                                                   real_id))
                        resp.update({'status_code': 200,
                                     'resq_body': {
                                         "code": "success",
                                         "title": _("Success"),
                                         "msg": _("Edit availability successfuly")
                                     }
                                     })
                    else:
                        dayAvailability = datetime.strptime(vitural_id[:8], '%Y%m%d').strftime('%Y-%m-%d')
                        sql_insert = '''INSERT INTO calendar_event (start_datetime,
                                                                    end_datetime,
                                                                    duration,
                                                                    start_date,
                                                                    create_date,
                                                                    write_date,
                                                                    user_id,
                                                                    recurrency,
                                                                    active,
                                                                    recurrent_id) 
                                        VALUES (%s,%s,%s,%s,%s,%s,%s,'False','True',%s) 
                                        RETURNING id'''
                        new_id = gs.sql_execute(pool, sql_insert, commit=True,
                                                para_values=('%s %s' % (dayAvailability, newStartTime),
                                                             '%s %s' % (dayAvailability, newEndTime),
                                                             duration,
                                                             dayAvailability,
                                                             currentTime,
                                                             currentTime,
                                                             user_id,
                                                             real_id))
                        resp.update({'status_code': 200,
                                     'resq_body': {'new_id': int(new_id[0][0])}
                                     })

                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
                return resp
        except Exception as e:
            logger.error('error_doctor_schedule_edit_availability %s' % str(e))
            client.captureException()
        return resp

    def doctor_schedule_remove_availability(self, data):
        logger.info('doctor_schedule_remove_availability %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Remove availability failed"),
                        "msg": _("An error occurred. Please try to remove again."),
                        "code": "remove_availability_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')  # or '47f3790febd55540cbae2257527a2c80'
            schedule_id = data.get('id', None)
            removeEveryRecurring = data.get('removeEveryRecurring', None)

            if not schedule_id or removeEveryRecurring == None:
                return resp

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    is_recurring = False
                    schedule_id = schedule_id.split('_')
                    if len(schedule_id) > 1:
                        is_recurring = True
                        vitural_id = schedule_id[1]
                    real_id = int(schedule_id[0])

                    currentTime = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    if (is_recurring == True and removeEveryRecurring == True) or is_recurring == False:
                        sql_delete = '''delete from calendar_event where id = %s'''
                        gs.sql_execute(pool, sql_delete, commit=True, para_values=[real_id])
                        resp.update({'status_code': 200,
                                     'resq_body': {
                                         "code": "success",
                                         "title": _("Success"),
                                         "msg": _("Remove availability successfuly")
                                     }
                                     })
                    else:
                        sql = '''select start_datetime::time,
                                         end_datetime::time
                                from calendar_event where id = %s'''
                        timeAvailability = gs.sql_execute(pool, sql, para_values=[real_id])
                        StartTime = timeAvailability[0][0].strftime('%H:%M:%S')
                        EndTime = timeAvailability[0][1].strftime('%H:%M:%S')
                        duration = (timeAvailability[0][1].hour * 60 + timeAvailability[0][1].minute) - (
                        timeAvailability[0][0].hour * 60 + timeAvailability[0][0].minute)
                        dayAvailability = datetime.strptime(vitural_id[:8], '%Y%m%d').strftime('%Y-%m-%d')
                        sql_insert = '''INSERT INTO calendar_event (start_datetime,
                                                                    end_datetime,
                                                                    duration,
                                                                    start_date,
                                                                    create_date,
                                                                    write_date,
                                                                    user_id,
                                                                    recurrency,
                                                                    active,
                                                                    recurrent_id) 
                                        VALUES (%s,%s,%s,%s,%s,%s,%s,'False','False',%s) 
                                        RETURNING id'''
                        new_id = gs.sql_execute(pool, sql_insert, commit=True,
                                                para_values=('%s %s' % (dayAvailability, StartTime),
                                                             '%s %s' % (dayAvailability, EndTime),
                                                             duration,
                                                             dayAvailability,
                                                             currentTime,
                                                             currentTime,
                                                             user_id,
                                                             real_id))
                        resp.update({'status_code': 200,
                                     'resq_body': {"code": "success",
                                                   "title": _("Success"),
                                                   "msg": _("Remove availability successfuly")
                                                   }})

                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
                return resp
        except Exception as e:
            logger.error('error_doctor_schedule_remove_availability %s' % str(e))
            client.captureException()
        return resp
    
    def doctor_check_appointment_existence(self, data):
        logger.info('doctor_check_appointment_existence %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Check appointment existence"),
                        "msg": _("An error occurred. Please try to remove again."),
                        "code": "check_appointment_existence_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')  # or '47f3790febd55540cbae2257527a2c80'
            availabilityId = data.get('availabilityId', None)
            

            if not availabilityId:
                return resp

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    
                    sql = '''select start_datetime,
                                         end_datetime
                                from calendar_event where id = %s'''
                    timeAvailability = gs.sql_execute(pool, sql, para_values=[availabilityId])
                    start_datetime = timeAvailability[0][0].strftime('%Y-%m-%d %H:%M:%S')
                    end_datetime = timeAvailability[0][1].strftime('%Y-%m-%d %H:%M:%S')

                    sql = '''SELECT apm.schedule_date
                            FROM appointment apm
                            INNER JOIN user_account ua on ua.id = apm.doctor_id
                            WHERE ua.user_mngment_id = %s AND
                            apm.schedule_date >= %s::timestamp AND apm.schedule_date <= %s::timestamp  
                            and apm.state in ('confirmed_by_patient','confirmed','processing','suspended') '''

                    apm_records = gs.sql_execute(pool, sql, para_values=(res_token[7], start_datetime, end_datetime))

                    if apm_records:
                        resp.update({'status_code': 200,
                                     'resq_body': {
                                                   "result": False
                                                   }})
                    else:
                        resp.update({'status_code': 200,
                                     'resq_body': {
                                                   "result": True
                                                   }})
                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
        except Exception as e:
            logger.error('error_doctor_check_appointment_existence %s' % str(e))
            client.captureException()
        return resp
    
