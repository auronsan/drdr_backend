import falcon
import json
import sys

sys.path.insert(0, "..")
try:
    from general import general_business
    from aws_management import aws_management
except ImportError as e:
    print('No Import ', e)

from patient_schedule_business import patient_schedule_business

import ConfigParser
import os

project_path =  str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
config = ConfigParser.RawConfigParser(allow_no_value=True)
config.read(project_path + '/' + "environment.conf")

AWS_BUCKET = config.get('environment', 's3_bucket')
BUCKET_URL_DEFAULT = AWS_BUCKET + '.' + config.get('environment', 's3_address')

status_code = {
    200: falcon.HTTP_200,
    401: falcon.HTTP_401,
    502: falcon.HTTP_502,
    203: falcon.HTTP_203
}


class patient_schedule_api(object):
    def __init__(self, signal, erp_connection=False, var_contructor=False):
        self.business = patient_schedule_business(signal, erp_connection, var_contructor)
        self.pool_conn = erp_connection

        if var_contructor:
            self.ERP_DB_HOST = var_contructor['ERP_DB_HOST']
            self.ERP_DB_PORT = var_contructor['ERP_DB_PORT']
            self.ERP_DB_USER = var_contructor['ERP_DB_USER']
            self.ERP_DB_PASS = var_contructor['ERP_DB_PASS']
            self.ERP_DB_NAME = var_contructor['ERP_DB_NAME']

        self.signal = signal
        self.general_business = general_business(erp_connection, var_contructor)

    def on_post(self, req, resp, doctor_id=None):
        try:
            data = {}
            header = req.headers  # key cua header luon viet hoa
            all_header = self.general_business.get_header_request(header)
            lang = all_header.get('lang')
            _ = self.general_business.get_lang_instance(lang)
            data.update(all_header)

            # MP8.1 API for patient to get schedule timeslot list of a doctor for specific date
            if self.signal == 'get_schedule_timeslot_list_of_a_doctor_for_specific_date':
                resp_status = status_code[401]
                resp_body = {
                    "code": "no_avalability",
                    "title": _("No availability"),
                    "msg": _("The doctor didn't define his availability time for the date")
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'doctor_id': doctor_id,
                        'lang': lang,
                    })
                    data.update(raw_data)
                    record = self.business.get_schedule_timeslot_list_of_a_doctor_for_specific_date(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # MP8.2 API for getting schedule status of a specific month
            # MP8.6 API for getting schedule status of a date range
            elif self.signal == 'get_schedule_status':
                resp_status = status_code[401]
                resp_body = {
                    "code": "get_schedule_status_failed",
                    "title": _("Get schedule status failed"),
                    "msg": _("An error occurred. Please try to set again.")
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'doctor_id': doctor_id,
                        'lang': lang,
                    })
                    data.update(raw_data)
                    month = data.get('month', None)
                    year = data.get('year', None)

                    startDate = data.get('startDate', None)
                    endDate = data.get('endDate', None)

                    if month and year:
                        record = self.business.get_schedule_status_of_a_specific_month(data)
                        
                    elif startDate and endDate:
                        record = self.business.get_schedule_status_of_a_date_range(data)

                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # MP8.3 API for patient to get schedule information to book the appointment
            elif self.signal == 'get_schedule_information_to_book_the_appointment':
                resp_status = status_code[401]
                resp_body = {
                    "code": "get_schedule_information_to_book_the_appointment",
                    "title": _("Get schedule information to book the appointment"),
                    "msg": _("An error occurred. Please try to set again.")
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    data.update(raw_data)
                    record = self.business.get_schedule_information_to_book_the_appointment(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # MP8.4 API for get token of promotion code
            elif self.signal == 'patient_get_promotion_code':
                resp_status = status_code[401]
                resp_body = {
                    "code": "patient_get_promotion_code_failed",
                    "title": _("Patient get promotion code failed"),
                    "msg": _("An error occurred. Please try to set again.")
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    data.update(raw_data)
                    record = self.business.patient_get_promotion_code(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # MP8.5 API for patient to book an appointment
            elif self.signal == 'patient_book_the_appointment':
                resp_status = status_code[401]
                resp_body = {
                    "code": "patient_book_the_appointment_failed",
                    "title": _("Patient book the appointment failed"),
                    "msg": _("An error occurred. Please try to set again.")
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = json.loads(req.stream.read(req.content_length or 0) or '{}')
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    data.update(raw_data)
                    record = self.business.patient_book_the_appointment(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

        except Exception as e:
            print str(e)
