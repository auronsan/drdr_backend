# -*- coding: utf-8 -*-
from datetime import datetime
import hashlib
import time
import logging
from datetime import timedelta
import numpy as np
import calendar
import dateutil.relativedelta as relativedelta
import dateutil.rrule as rrule
from raven import Client
import ConfigParser
import sys
import os

project_path =  str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
config = ConfigParser.RawConfigParser(allow_no_value=True)
config.read(project_path + '/' + "environment.conf")
sentry_key = config.get('environment', 'sentry_key')
client = Client(sentry_key)
logger = logging.getLogger('drdr_api')
sys.path.insert(0, "..")
try:
    from general import general_business
    from payment.model.business_transfer_transaction import business_transfer_transaction
except ImportError as e:
    print('No Import ', e)



class patient_schedule_business(object):
    def __init__(self, signal, erp_connection, var_contructor):
        self.pool_conn = erp_connection
        self.var_contructor = var_contructor
        self.signal = signal
        self.ERP_DB_HOST = var_contructor['ERP_DB_HOST']
        self.ERP_DB_PORT = var_contructor['ERP_DB_PORT']
        self.ERP_DB_USER = var_contructor['ERP_DB_USER']
        self.ERP_DB_PASS = var_contructor['ERP_DB_PASS']
        self.ERP_DB_NAME = var_contructor['ERP_DB_NAME']
        self.general_business = general_business(erp_connection, var_contructor)
        self.business_transfer = business_transfer_transaction(erp_connection, var_contructor)
        self.drdr_wallet_id = int(config.get('environment', 'drdr_wallet_id')) or 1
        self.com_ratio = float(config.get('environment', 'com_ratio')) or 40.0
        self.min_commission = float(config.get('environment', 'min_commission')) or 40000
        self.vi_ = self.general_business.vi_
        self.en_ = self.general_business.en_

    # MP8.1 API for patient to get schedule timeslot list of a doctor for specific date
    def get_schedule_timeslot_list_of_a_doctor_for_specific_date(self, data):
        logger.info('get_schedule_timeslot_list_of_a_doctor_for_specific_date %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "no_avalability",
                        "title": _("No availability"),
                        "msg": _("The doctor didn't define his availability time for the date")
                    }
                }
        try:
            date = data.get('date', None)

            if date == None:
                return resp

            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            doctor_id = data.get('doctor_id')
            start_datetime = datetime.strptime(date + ' 00:00:00', '%d-%m-%Y %H:%M:%S')
            end_datetime = datetime.strptime(date + ' 23:59:59', '%d-%m-%Y %H:%M:%S')

            if end_datetime < datetime.now():
                resp.update({'status_code': 401,
                             'resq_body':
                                 {
                                     "code": "wrong_time",
                                     "title": _("Wrong time"),
                                     "msg": _("The time is smaller than current time, please try to set again")
                                 }
                             })
                return resp

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:

                    sql = '''SELECT apm.schedule_date
                            FROM appointment apm
                            INNER JOIN user_account ua on ua.id = apm.doctor_id
                            WHERE ua.user_mngment_id = %s AND
                            apm.schedule_date >= %s::timestamp AND apm.schedule_date <= %s::timestamp  
                            and apm.state in ('confirmed_by_patient','confirmed','processing','suspended') '''

                    apm_records = gs.sql_execute(pool, sql, para_values=(doctor_id, start_datetime, end_datetime))

                    whitelist = []
                    blacklist = []
                    dayStatusList = []
                    sql = '''SELECT cv.id, --0
                                    cv.start_datetime, --1
                                    cv.end_datetime, --2
                                    cv.duration --3
                            FROM calendar_event cv
                            INNER JOIN user_account ua on ua.id = cv.user_id 
                            WHERE cv.recurrency = False 
                            and cv.active = True 
                            AND ua.user_mngment_id = %s 
                            AND cv.start_datetime >= %s::timestamp 
                            AND cv.start_datetime <= %s::timestamp'''

                    records_checkexist = gs.sql_execute(pool, sql,
                                                        para_values=(doctor_id, start_datetime, end_datetime))

                    for line in records_checkexist:
                        whitelist.append({
                            'id': line[0],
                            'startTime': line[1].replace(day=start_datetime.date().day,
                                                         month=start_datetime.date().month,
                                                         year=start_datetime.date().year),
                            'endTime': line[2].replace(day=start_datetime.date().day, month=start_datetime.date().month,
                                                       year=start_datetime.date().year),
                            'duration': line[3]
                        })
                    sql_loop = '''select cv.id, cv.start_datetime, cv.end_datetime, cv.count, cv.interval, cv.recurrency, cv.byday, cv.week_list, cv.rrule_type, cv.month_by, cv.end_type,cv.su,cv.mo,cv.tu,cv.we,cv.th,cv.fr,cv.sa,cv.day,cv.duration
                                                from calendar_event cv
                                                   INNER JOIN user_account ua on ua.id = cv.user_id
                                                where recurrency = True
                                                  and cv.active = True
                                                  and cv.recurrent_id is Null
                                                  and ua.user_mngment_id = %s
                                                  and cv.final_date >= %s'''

                    loop_checkexist = gs.sql_execute(pool, sql_loop, para_values=(doctor_id, start_datetime))

                    for line in loop_checkexist:
                        count = int(line[3])
                        interval = int(line[4])
                        if line[8] == 'daily':
                            for i in range(0, count):
                                sd = line[1] + timedelta(days=interval * i)
                                if sd >= start_datetime and sd <= end_datetime:
                                    whitelist.append({
                                        'date': sd.strftime('%d'),
                                        'startTime': line[1],
                                        'endTime': line[2],
                                        'duration': line[19]
                                    })
                                elif sd > end_datetime:
                                    break

                        elif line[8] == 'weekly':
                            days = []
                            if line[11] == True:  # su
                                days.append('su')
                            if line[12] == True:  # su
                                days.append('mo')
                            if line[13] == True:  # su
                                days.append('tu')
                            if line[14] == True:  # su
                                days.append('we')
                            if line[15] == True:  # su
                                days.append('th')
                            if line[16] == True:  # su
                                days.append('fr')
                            if line[17] == True:  # su
                                days.append('sa')
                            dayOfweek = start_datetime.date().strftime("%A").lower()[:2]

                            for i in range(0, count * 7 * interval):
                                sd = line[1] + timedelta(days=i)
                                if sd >= start_datetime and sd <= end_datetime:
                                    dayOfweek = sd.date().strftime("%A").lower()[:2]
                                    if dayOfweek in days:
                                        whitelist.append({
                                            'date': sd.strftime('%d'),
                                            'startTime': line[1],
                                            'endTime': line[2],
                                            'duration': line[19]
                                        })
                                elif sd > end_datetime:
                                    break

                        elif line[8] == 'monthly':
                            if line[1] <= end_datetime:
                                if line[9] == 'date':
                                    if line[18] == start_datetime.date().day:
                                        whitelist.append({
                                            'id': line[0],
                                            'startTime': line[1].replace(day=start_datetime.date().day,
                                                                         month=start_datetime.date().month,
                                                                         year=start_datetime.date().year),
                                            'endTime': line[2].replace(day=start_datetime.date().day,
                                                                       month=start_datetime.date().month,
                                                                       year=start_datetime.date().year),
                                            'duration': line[19]
                                        })
                                elif line[9] == 'day':
                                    if line[7] == start_datetime.date().strftime("%A").lower()[:2]:
                                        whitelist.append({
                                            'id': line[0],
                                            'startTime': line[1].replace(day=start_datetime.date().day,
                                                                         month=start_datetime.date().month,
                                                                         year=start_datetime.date().year),
                                            'endTime': line[2].replace(day=start_datetime.date().day,
                                                                       month=start_datetime.date().month,
                                                                       year=start_datetime.date().year),
                                            'duration': line[19]
                                        })

                        sql = ''' SELECT cv.start_datetime, --0
                                        cv.end_datetime, --1
                                        cv.active,  --2
                                        cv.duration, -- 3
                                        cv.recurrent_id
                                        FROM calendar_event cv
                                        WHERE cv.recurrent_id = %s 
                                        AND cv.start_datetime >= %s::timestamp 
                                        AND cv.start_datetime <= %s::timestamp '''
                        checkexist = gs.sql_execute(pool, sql, para_values=(line[0], start_datetime, end_datetime))

                        for ll in checkexist:
                            if ll[2] == True:
                                whitelist.append({
                                    'id': line[0],
                                    'startTime': ll[0].replace(day=start_datetime.date().day,
                                                               month=start_datetime.date().month,
                                                               year=start_datetime.date().year),
                                    'endTime': ll[1].replace(day=start_datetime.date().day,
                                                             month=start_datetime.date().month,
                                                             year=start_datetime.date().year),
                                    'duration': ll[3]
                                })
                            else:
                                blacklist.append({
                                    'id': line[0],
                                    'startTime': ll[0].replace(day=start_datetime.date().day,
                                                               month=start_datetime.date().month,
                                                               year=start_datetime.date().year),
                                    'endTime': ll[1].replace(day=start_datetime.date().day,
                                                             month=start_datetime.date().month,
                                                             year=start_datetime.date().year),
                                    'duration': ll[3]
                                })

                    sql = ''' SELECT consultation_duration
                                        FROM doctor_profile
                                        WHERE user_mngment_id = %s '''
                    duration = gs.sql_execute(pool, sql, para_values=(doctor_id,))[0][0][:-1]

                    whitetimeslot = []
                    for x in whitelist:
                        mul = int(x['duration']) / int(duration)
                        for t in range(1, mul + 1):
                            if t == 1:
                                whitetimeslot.append({
                                    'startTime': x['startTime'].strftime('%H:%M'),
                                    'endTime': (x['startTime'] + timedelta(minutes=int(duration))).strftime('%H:%M')
                                })
                            elif t > 1 and t < mul:
                                whitetimeslot.append({
                                    'startTime': (x['startTime'] + timedelta(minutes=int(duration) * (t - 1))).strftime(
                                        '%H:%M'),
                                    'endTime': (x['startTime'] + timedelta(minutes=int(duration) * t)).strftime('%H:%M')
                                })
                            else:
                                whitetimeslot.append({
                                    'startTime': (x['startTime'] + timedelta(minutes=int(duration) * (t - 1))).strftime(
                                        '%H:%M'),
                                    'endTime': (x['startTime'] + timedelta(minutes=int(duration) * (t))).strftime(
                                        '%H:%M')
                                })
                    white_unique_dicts = np.unique(np.array(whitetimeslot))

                    blacktimeslot = []
                    for x in blacklist:
                        mul = int(x['duration']) / int(duration)
                        if mul == 0:
                            blacktimeslot.append({
                                'startTime': x['startTime'].strftime('%H:%M'),
                                'endTime': x['endTime'].strftime('%H:%M')
                            })
                        else:
                            for t in range(1, mul + 1):
                                if t == 1:
                                    blacktimeslot.append({
                                        'startTime': x['startTime'].strftime('%H:%M'),
                                        'endTime': (x['startTime'] + timedelta(minutes=int(duration))).strftime('%H:%M')
                                    })
                                elif t > 1 and t < mul:
                                    blacktimeslot.append({
                                        'startTime': (
                                        x['startTime'] + timedelta(minutes=int(duration) * (t - 1))).strftime('%H:%M'),
                                        'endTime': (x['startTime'] + timedelta(minutes=int(duration) * t)).strftime(
                                            '%H:%M')
                                    })
                                else:
                                    blacktimeslot.append({
                                        'startTime': (
                                        x['startTime'] + timedelta(minutes=int(duration) * (t - 1))).strftime('%H:%M'),
                                        'endTime': (x['startTime'] + timedelta(minutes=int(duration) * (t))).strftime(
                                            '%H:%M')
                                    })

                    black_of_unique_dicts = np.unique(np.array(blacktimeslot))

                    def checkContains(x, items):
                        for item in items:
                            if (x['startTime'] >= item['startTime'] and x['startTime'] <= item['endTime']) or (
                                    x['endTime'] >= item['startTime'] and x['endTime'] <= item['endTime']):
                                return True
                        return False

                    for w in white_unique_dicts:
                        if not checkContains(w, black_of_unique_dicts):
                            timeSlot = w
                            timeSlot.update({
                                'isAvailable': True
                            })
                            if datetime.strptime(date, '%d-%m-%Y').date() == datetime.now().date() and w[
                                'startTime'] < datetime.now().strftime('%H:%M'):
                                timeSlot.update({
                                    'isAvailable': False
                                })
                            else:
                                for apm in apm_records:
                                    # if (w['startTime'] >= apm[0].strftime('%H:%M') and w['endTime'] > apm[0].strftime('%H:%M')) \
                                    #         or (w['startTime'] < apm[0].strftime('%H:%M') and w['endTime'] <= apm[0].strftime('%H:%M')):
                                    #     # timeSlot.update({
                                    #     #     'isAvailable': True
                                    #     # })
                                    #     pass
                                    # else:
                                    #     timeSlot.update({
                                    #         'isAvailable': False
                                    #     })
                                    #     break
                                    time_check = apm[0].strftime('%H:%M')
                                    if (w['startTime'] <= time_check and time_check < w['endTime']):
                                        timeSlot.update({
                                            'isAvailable': False
                                        })
                                        break
                            dayStatusList.append(timeSlot)

                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     'timeSlotList': dayStatusList
                                 }
                                 })

                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
                    return resp

        except Exception as e:
            logger.error('error_get_schedule_timeslot_list_of_a_doctor_for_specific_date %s' % str(e))
            client.captureException()
        return resp

    # MP8.2 API for getting schedule status of a specific month
    def get_schedule_status_of_a_specific_month(self, data):
        logger.info('get_schedule_status_of_a_specific_month %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "get_schedule_status_failed",
                        "title": _("Get schedule status failed"),
                        "msg": _("An error occurred. Please try to set again.")
                    }
                }
        try:
            month = data.get('month', None)
            year = data.get('year', None)

            if (year == datetime.now().year and month < datetime.now().month) or (year < datetime.now().year):
                dayStatusList = []
                for i in range(1, calendar.monthrange(year, month)[1] + 1):
                    dayStatusList.append({
                        "dayOfTheMonth": i,
                        "status": "not_working"
                    })
                resp.update({'status_code': 200,
                             'resq_body': {
                                 'dayStatusList': dayStatusList
                             }
                             })
                return resp

            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            doctor_id = data.get('doctor_id')
            endayOfMonth = calendar.monthrange(year, month)[1]
            start_datetime = datetime.strptime('''%s-%s-1 00:00:00''' % (year, month), '%Y-%m-%d %H:%M:%S')
            end_datetime = datetime.strptime('''%s-%s-%s 23:59:59''' % (year, month, endayOfMonth), '%Y-%m-%d %H:%M:%S')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))

                if res_token:
                    sql = '''SELECT apm.schedule_date
                            FROM appointment apm
                            INNER JOIN user_account ua on ua.id = apm.doctor_id
                            WHERE ua.user_mngment_id = %s AND
                            apm.schedule_date >= %s::timestamp AND apm.schedule_date <= %s::timestamp  
                            and apm.state in ('confirmed_by_patient','confirmed','processing','suspended') '''

                    apm_records = gs.sql_execute(pool, sql, para_values=(doctor_id, start_datetime, end_datetime))

                    whitelist = []
                    blacklist = []
                    dayStatusList = []
                    sql = '''SELECT cv.id, --0
                                    cv.start_datetime, --1
                                    cv.end_datetime, --2
                                    cv.duration --3
                            FROM calendar_event cv
                            INNER JOIN user_account ua on ua.id = cv.user_id 
                            WHERE cv.recurrency = False 
                            and cv.active = True 
                            AND ua.user_mngment_id = %s 
                            AND cv.start_datetime >= %s::timestamp 
                            AND cv.start_datetime <= %s::timestamp'''

                    records_checkexist = gs.sql_execute(pool, sql,
                                                        para_values=(doctor_id, start_datetime, end_datetime))

                    for line in records_checkexist:
                        whitelist.append({
                            'date': line[1].strftime('%d'),
                            'startTime': line[1],
                            'endTime': line[2],
                            'duration': line[3]
                        })

                    sql_loop = '''select cv.id, cv.start_datetime, cv.end_datetime, cv.count, cv.interval, cv.recurrency, cv.byday, cv.week_list, cv.rrule_type, cv.month_by, cv.end_type,cv.su,cv.mo,cv.tu,cv.we,cv.th,cv.fr,cv.sa,cv.day,cv.duration,cv.final_date
                                                from calendar_event cv
                                                   INNER JOIN user_account ua on ua.id = cv.user_id
                                                where recurrency = True
                                                  and cv.active = True
                                                  and cv.recurrent_id is Null
                                                  and ua.user_mngment_id = %s
                                                  and cv.final_date >= %s
                                                  and cv.start_datetime <= %s
                                                                        '''
                    loop_checkexist = gs.sql_execute(pool, sql_loop,
                                                     para_values=(doctor_id, start_datetime, end_datetime))

                    for line in loop_checkexist:
                        count = int(line[3])
                        interval = int(line[4])
                        if line[8] == 'daily':
                            for i in range(0, count):
                                sd = line[1] + timedelta(days=interval * i)
                                if sd >= start_datetime and sd <= end_datetime:
                                    whitelist.append({
                                        'date': sd.strftime('%d'),
                                        'startTime': line[1],
                                        'endTime': line[2],
                                        'duration': line[19]
                                    })
                                elif sd > end_datetime:
                                    break

                        elif line[8] == 'weekly':

                            days = []
                            if line[11] == True:  # su
                                days.append('su')
                            if line[12] == True:  # su
                                days.append('mo')
                            if line[13] == True:  # su
                                days.append('tu')
                            if line[14] == True:  # su
                                days.append('we')
                            if line[15] == True:  # su
                                days.append('th')
                            if line[16] == True:  # su
                                days.append('fr')
                            if line[17] == True:  # su
                                days.append('sa')

                            for i in range(0, int(endayOfMonth)):
                                sd = start_datetime + timedelta(days=i)
                                if sd.date() >= line[1].date() and sd < datetime.strptime(
                                                '''%s 00:00:00''' % (line[20]), '%Y-%m-%d %H:%M:%S'):
                                    dayOfweek = sd.date().strftime("%A").lower()[:2]
                                    if dayOfweek in days:
                                        whitelist.append({
                                            'date': sd.strftime('%d'),
                                            'startTime': line[1],
                                            'endTime': line[2],
                                            'duration': line[19]
                                        })
                                elif sd > end_datetime:
                                    break

                        elif line[8] == 'monthly':
                            if line[1] <= end_datetime:
                                if line[9] == 'date':
                                    sd = start_datetime.replace(day=int(line[18]))
                                    whitelist.append({
                                        'date': sd.strftime('%d'),
                                        'startTime': line[1],
                                        'endTime': line[2],
                                        'duration': line[19]
                                    })
                                elif line[9] == 'day':
                                    if line[7] == 'su':
                                        rr = rrule.rrule(rrule.WEEKLY, byweekday=relativedelta.SU,
                                                         dtstart=start_datetime)
                                    elif line[7] == 'mo':
                                        rr = rrule.rrule(rrule.WEEKLY, byweekday=relativedelta.MO,
                                                         dtstart=start_datetime)
                                    elif line[7] == 'tu':
                                        rr = rrule.rrule(rrule.WEEKLY, byweekday=relativedelta.TU,
                                                         dtstart=start_datetime)
                                    elif line[7] == 'we':
                                        rr = rrule.rrule(rrule.WEEKLY, byweekday=relativedelta.WE,
                                                         dtstart=start_datetime)
                                    elif line[7] == 'th':
                                        rr = rrule.rrule(rrule.WEEKLY, byweekday=relativedelta.TH,
                                                         dtstart=start_datetime)
                                    elif line[7] == 'fr':
                                        rr = rrule.rrule(rrule.WEEKLY, byweekday=relativedelta.FR,
                                                         dtstart=start_datetime)
                                    else:
                                        rr = rrule.rrule(rrule.WEEKLY, byweekday=relativedelta.SA,
                                                         dtstart=start_datetime)

                                    result = rr.between(start_datetime, end_datetime, inc=True)[-1]
                                    whitelist.append({
                                        'date': result.strftime('%d'),
                                        'startTime': line[1],
                                        'endTime': line[2],
                                        'duration': line[19]
                                    })

                        sql = ''' SELECT cv.start_datetime, --0
                                        cv.end_datetime, --1
                                        cv.active,  --2
                                        cv.duration, -- 3
                                        cv.recurrent_id
                                        FROM calendar_event cv
                                        WHERE cv.recurrent_id = %s 
                                        AND cv.start_datetime >= %s::timestamp 
                                        AND cv.start_datetime <= %s::timestamp '''

                        checkexist = gs.sql_execute(pool, sql, para_values=(line[0], start_datetime, end_datetime))

                        if checkexist:
                            for ll in checkexist:
                                if ll[2] == True:
                                    whitelist.append({
                                        'date': ll[0].strftime('%d'),
                                        'startTime': ll[0],
                                        'endTime': ll[1],
                                        'duration': ll[3]
                                    })
                                else:
                                    blacklist.append({
                                        'date': ll[0].strftime('%d'),
                                        'startTime': ll[0],
                                        'endTime': ll[1],
                                        'duration': ll[3]
                                    })

                    sql = ''' SELECT consultation_duration
                                        FROM doctor_profile
                                        WHERE user_mngment_id = %s '''
                    duration = int(gs.sql_execute(pool, sql, para_values=(doctor_id,))[0][0][:-1])

                    whitetimeslot = []
                    for x in whitelist:
                        mul = int(x['duration']) / duration
                        for t in range(1, mul + 1):
                            if t == 1:
                                whitetimeslot.append({
                                    'date': x['date'],
                                    'startTime': x['startTime'].strftime('%H:%M'),
                                    'endTime': (x['startTime'] + timedelta(minutes=duration)).strftime('%H:%M')
                                })
                            elif t > 1 and t < mul:
                                whitetimeslot.append({
                                    'date': x['date'],
                                    'startTime': (x['startTime'] + timedelta(minutes=duration * (t - 1))).strftime(
                                        '%H:%M'),
                                    'endTime': (x['startTime'] + timedelta(minutes=duration * t)).strftime('%H:%M')
                                })
                            else:
                                whitetimeslot.append({
                                    'date': x['date'],
                                    'startTime': (x['startTime'] + timedelta(minutes=duration * (t - 1))).strftime(
                                        '%H:%M'),
                                    'endTime': x['endTime'].strftime('%H:%M')
                                })

                    white_unique_dicts = np.unique(np.array(whitetimeslot))

                    blacktimeslot = []
                    for x in blacklist:
                        mul = int(x['duration']) / duration
                        for t in range(1, mul + 1):
                            if t == 1:
                                blacktimeslot.append({
                                    'date': x['date'],
                                    'startTime': x['startTime'].strftime('%H:%M'),
                                    'endTime': (x['startTime'] + timedelta(minutes=duration)).strftime('%H:%M')
                                })
                            elif t > 1 and t < mul:
                                blacktimeslot.append({
                                    'date': x['date'],
                                    'startTime': (x['startTime'] + timedelta(minutes=duration * (t - 1))).strftime(
                                        '%H:%M'),
                                    'endTime': (x['startTime'] + timedelta(minutes=duration * t)).strftime('%H:%M')
                                })
                            else:
                                blacktimeslot.append({
                                    'date': x['date'],
                                    'startTime': (x['startTime'] + timedelta(minutes=duration * (t - 1))).strftime(
                                        '%H:%M'),
                                    'endTime': x['endTime'].strftime('%H:%M')
                                })
                    black_of_unique_dicts = np.unique(np.array(blacktimeslot))

                    def checkContains(x, items):
                        for item in items:
                            if item['date'] == x['date']:
                                if (x['startTime'] >= item['startTime'] and x['startTime'] <= item['endTime']) or (
                                        x['endTime'] >= item['startTime'] and x['endTime'] <= item['endTime']):
                                    return True
                        return False

                    flag = -1
                    available = 0
                    notAvailable = 0
                    total = 0
                    for w in white_unique_dicts:
                        if flag != w['date']:
                            if (flag != -1):
                                timeSlot = {
                                    "dayOfTheMonth": int(flag),
                                }
                                if available == 0 and notAvailable == 0:
                                    timeSlot.update({
                                        "status": "not_working"
                                    })
                                else:
                                    if available / ((available + notAvailable) * 1.0) == 0:
                                        timeSlot.update({
                                            "status": "none"
                                        })
                                    elif available / ((available + notAvailable) * 1.0) < 0.5:
                                        timeSlot.update({
                                            "status": "few"
                                        })
                                    else:
                                        timeSlot.update({
                                            "status": "many"
                                        })

                                dayStatusList.append(timeSlot)
                            total = 1
                            flag = w['date']
                            available = 0
                            notAvailable = 0
                        else:
                            total = total + 1
                        
                        if not checkContains(w, black_of_unique_dicts):
                            if datetime.now().date().month == month and datetime.now().date().year == year:
                                if datetime.now().date().day > int(flag):
                                    notAvailable = 0
                                    available = 0
                                elif datetime.now().date().day == int(flag) and (datetime.now() + timedelta(minutes=duration)).strftime('%H:%M') >= w[
                                    'endTime']:
                                    notAvailable = notAvailable + 1
                                else:
                                    available = available + 1
                                    for apm in apm_records:
                                        # if not ((w['startTime'] > apm[0].strftime('%H:%M') and w['endTime'] > apm[
                                        #     0].strftime('%H:%M')) or
                                        #             (w['startTime'] < apm[0].strftime('%H:%M') and w['endTime'] < apm[
                                        #                 0].strftime('%H:%M'))) and apm[0].strftime('%d') == w['date']:
                                        #     available = available - 1
                                        #     notAvailable = notAvailable + 1
                                        #     break

                                        time_check = apm[0].strftime('%H:%M')
                                        if (w['startTime'] <= time_check and time_check < w['endTime']) and apm[0].strftime('%d') == w['date']:
                                            available = available - 1
                                            notAvailable = notAvailable + 1
                                            break
                            else:
                                available = available + 1
                                for apm in apm_records:
                                    # if not ((w['startTime'] > apm[0].strftime('%H:%M') and w['endTime'] > apm[
                                    #     0].strftime('%H:%M')) or
                                    #             (w['startTime'] < apm[0].strftime('%H:%M') and w['endTime'] < apm[
                                    #                 0].strftime('%H:%M'))) and apm[0].strftime('%d') == w['date']:
                                    #     available = available - 1
                                    #     notAvailable = notAvailable + 1
                                    #     break
                                    time_check = apm[0].strftime('%H:%M')
                                    if (w['startTime'] <= time_check and time_check < w['endTime']) and apm[0].strftime('%d') == w['date']:
                                        available = available - 1
                                        notAvailable = notAvailable + 1
                                        break

                    if (flag != -1):
                        timeSlot = {
                            "dayOfTheMonth": int(flag),
                        }
                        if available == 0 and notAvailable == 0:
                            timeSlot.update({
                                "status": "not_working"
                            })
                        else:
                            if available / ((available + notAvailable) * 1.0) == 0:
                                timeSlot.update({
                                    "status": "none"
                                })
                            elif available / ((available + notAvailable) * 1.0) < 0.5:
                                timeSlot.update({
                                    "status": "few"
                                })
                            else:
                                timeSlot.update({
                                    "status": "many"
                                })

                        dayStatusList.append(timeSlot)

                    def checkWorkingDay(day, dayStatusList):
                        for dayStatus in dayStatusList:
                            if int(dayStatus['dayOfTheMonth']) == day:
                                return dayStatus
                        return {
                            "dayOfTheMonth": day,
                            "status": "not_working"
                        }

                    resultList = []
                    for i in range(1, calendar.monthrange(year, month)[1] + 1):
                        resultList.append(checkWorkingDay(i, dayStatusList))

                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     'dayStatusList': resultList
                                 }
                                 })


                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
                    return resp

        except Exception as e:
            logger.error('error_get_schedule_status_of_a_specific_month %s' % str(e))
            client.captureException()
        return resp

    # MP8.3 API for patient to get schedule information to book the appointment
    def get_schedule_information_to_book_the_appointment(self, data):
        logger.info('get_schedule_information_to_book_the_appointment %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "get_schedule_information_to_book_the_appointment_failed",
                        "title": _("Get schedule information to book the appointment failed"),
                        "msg": _("An error occurred. Please try to set again.")
                    }
                }
        try:

            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            doctor_id = data.get('doctorId')
            timeSlotStartTime = data.get('timeSlotStartTime')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))

                if res_token:
                    sql = '''select um.title,--0
                                            um.profile_image_url,--1
                                            um.first_name,--2
                                            um.middle_name,--3
                                            um.last_name,--4
                                            spe.specialties_code, --5
                                            spe.years_of_exp, --6 

                                            dp.consultation_duration,--7
                                            dp.consultation_fee,--8
                                            dp.consultation_unit --9

                                    from user_mngment um
                                        left join doctor_profile dp on  um.id = dp.user_mngment_id 
                                        left join dr_doctorprofile_specialties spe on spe.doctor_profile_id = dp.id
                                    where um.id =%s '''

                    records = gs.sql_execute(pool, sql, para_values=[doctor_id])
                    res = {}
                    if records:
                        doctor = {
                            'titleCode': records[0][0] or None,
                            'avatarUrl': records[0][1] or None,
                            'firstName': None if records[0][2] in ('False', 'None', '') else records[0][2],
                            'middleName': None if records[0][3] in ('False', 'None', '') else records[0][3],
                            'lastName': None if records[0][4] in ('False', 'None', '') else records[0][4]
                        }

                        list_specialtyCodes = []
                        for line in records:
                            list_specialtyCodes.append({'code': line[5], 'yearsOfExp': line[6]})

                        doctor.update({
                            "specialtyCodes": list_specialtyCodes
                        })
                        timeSlotEndTime = (
                        datetime.strptime('''2017-01-01 %s''' % (timeSlotStartTime), '%Y-%m-%d %H:%M') + timedelta(
                            minutes=int(records[0][7][:-1]))).strftime('%H:%M')
                        res.update({
                            "doctor": doctor,
                            "consultationDurationCode": records[0][7],
                            "consultationFee": {
                                "value": int(records[0][8]) + max(self.min_commission, int(records[0][8]) * (self.com_ratio/100)),
                                "unitCode": records[0][9]
                            },

                            "timeSlotEndTime": timeSlotEndTime
                        })
                        sql = '''select available_balance, unitcode from user_wallet where user_account_id = %s '''
                        wallet = gs.sql_execute(pool, sql, para_values=(res_token[1],))
                        if wallet:
                            res.update({
                                "patientAvailableBalance": {
                                    "value": wallet[0][0],
                                    "unitCode": wallet[0][1],
                                }
                            })
                        else:
                            res.update({
                                "patientAvailableBalance": {
                                    "value": 0,
                                    "unitCode": "vnd",
                                }
                            })

                    resp.update({'status_code': 200,
                                 'resq_body': res
                                 })


                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
                    return resp

        except Exception as e:
            logger.error('error_get_schedule_status_of_a_specific_month %s' % str(e))
            client.captureException()
        return resp

    # MP8.4 API for get token of promotion code
    def patient_get_promotion_code(self, data):
        logger.info('patient_get_promotion_code %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "patient_get_promotion_code_failed",
                        "title": _("Patient get promotion code failed"),
                        "msg": _("An error occurred. Please try to set again.")
                    }
                }

        invalid = {'status_code': 401,
                   'resq_body': {
                       "code": "expired_promotion_code",
                       "title": _("Expired promotion code"),
                       "msg": _("The promotion code is invalid or expired, please try another one.")
                   }
                   }

        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            doctor_id = data.get('doctorId')
            promotionCode = data.get('promotionCode')

            if not promotionCode:
                return invalid

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    sql = '''select id, type, discount_type, value, unit_code, qty_available,expiry_time
                             from promotion_code 
                             where code = '%s' and active = True and qty_available > 0''' % (promotionCode)
                    promotion = gs.sql_execute(pool, sql)
                    if not promotion:
                        return invalid
                    elif promotion[0][1] == 'date' and promotion[0][6].date() < datetime.now().date():
                        return invalid

                    sql = '''select id, state, expiry_time,token
                                from promotion_transaction pt
                                where pt.promotion_id = %s and pt.user_account_id = %s '''
                    transaction = gs.sql_execute(pool, sql, para_values=(promotion[0][0], res_token[1]))
                    expiry_time = datetime.now() + timedelta(minutes=60)
                    value_hash = promotionCode + str(res_token[1]) + datetime.now().strftime('%Y%m%d%H%M%S')
                    token = hashlib.md5(value_hash).hexdigest()
                    # TODO: change 1!=1 to transaction (allow user using code only 1 time)
                    if 1!=1:
                        if transaction[0][1] == 'done':
                            return invalid
                        elif transaction[0][1] == 'confirmed' and transaction[0][2] < datetime.now():
                            sql = '''update promotion_transaction set token = %s,expiry_time = %s
                                    where id = %s '''
                            gs.sql_execute(pool, sql, commit=True, para_values=(token,
                                                                                expiry_time.strftime('%Y-%m-%d %H:%M:%S'),
                                                                                transaction[0][0]))
                        else:
                            expiry_time = transaction[0][2]
                            token = transaction[0][3]
                    else:
                        sql = '''insert into promotion_transaction(user_account_id, promotion_id, state, token, expiry_time)
                                    VALUES (%s, %s, 'confirmed', %s,%s)'''
                        gs.sql_execute(pool, sql, commit=True, para_values=(res_token[1], promotion[0][0],
                                                                            token,
                                                                            expiry_time.strftime('%Y-%m-%d %H:%M:%S')))
                        sql = '''update promotion_code set qty_available = qty_available - 1 
                                    where id = %s ''' % (promotion[0][0])
                        gs.sql_execute(pool, sql, commit=True)

                    sql = '''select consultation_fee, consultation_unit
                                from doctor_profile dp
                                where dp.user_mngment_id = %s ''' % (doctor_id)
                    doctorfee = gs.sql_execute(pool, sql)
                    if doctorfee:
                        margin = max(self.min_commission, float(doctorfee[0][0]) * (self.com_ratio/100))
                        if promotion[0][2] == 'fixed_value':
                            final_value = float(doctorfee[0][0]) + margin - float(promotion[0][3])
                        else:
                            final_value = (float(doctorfee[0][0]) + margin) * (100 - float(promotion[0][3])) / 100
                        resp.update({'status_code': 200,
                                     'resq_body': {
                                         "promotionToken": {
                                             "token": token,
                                             "type": promotion[0][2],
                                             "expiryDate": long(
                                                 time.mktime(expiry_time.replace(microsecond=0).timetuple())),
                                             "remainCountForUser": 0,
                                             "discountValue": {
                                                 "value": promotion[0][3],
                                                 "unitCode": promotion[0][4]
                                             }
                                         },
                                         "doctorConsultationFee": {
                                             "value": doctorfee[0][0] + margin,
                                             "unitCode": doctorfee[0][1]
                                         },
                                         "finalDoctorConsultationFee": {
                                             "value": final_value,
                                             "unitCode": promotion[0][4]
                                         },
                                     }
                                     })
                    else:
                        return resp

                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
                    return resp

        except Exception as e:
            logger.error('error_patient_get_promotion_code %s' % str(e))
            client.captureException()
        return resp

    # MP8.5 API for patient to book an appointment
    def patient_book_the_appointment(self, data):
        logger.info('patient_book_the_appointment %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "patient_book_the_appointment_failed",
                        "title": _("Patient book the appointment failed"),
                        "msg": _("An error occurred. Please try to set again.")
                    }
                }
        try:

            gs = self.general_business
            bt = self.business_transfer
            pool = self.pool_conn
            user_token = data.get('user_token')
            doctor_id = data.get('doctor_Id')
            scheduleDate = data.get('scheduleDate')
            timeSlotStartTime = data.get('startTime')
            promotionToken = data.get('promotionToken', False)
            if doctor_id == None or scheduleDate == None or timeSlotStartTime == None:
                return resp
            scheduleDate = datetime.strptime(scheduleDate, '%d-%m-%Y').strftime('%Y-%m-%d')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    timesavail_sql = '''SELECT cv.id, --0
                                            cv.start_datetime, --1
                                            cv.end_datetime, --2
                                            cv.duration --3
                                    FROM calendar_event cv
                                      INNER JOIN user_account ua on ua.id = cv.user_id 
                                    WHERE cv.active = True 
                                        AND ua.user_mngment_id = %s
                                        AND cv.start_datetime <= '%s %s'::timestamp 
                                        AND cv.end_datetime >= '%s %s'::timestamp''' % (doctor_id,
                                                                                       scheduleDate, timeSlotStartTime,
                                                                                       scheduleDate, timeSlotStartTime)
                    timesavail_result = gs.sql_execute(pool, timesavail_sql)
                    if not timesavail_result:
                        resp.update({'status_code': 401,
                                     'resq_body': {
                                         "code": "doctor_unavailable",
                                         "title": _("Doctor unavailable"),
                                         "msg": _("The doctor is not available for the time you choose")
                                     }
                                     })
                        return resp
                    
                    sql = '''select apm.id,apm.state, umng.first_name
                                from appointment apm 
                                inner join user_account ua on apm.doctor_id = ua.id
                                inner join user_mngment umng on  umng.id = ua.user_mngment_id 
                                where apm.schedule_date = '%s %s' and apm.patient_id = %s''' % (scheduleDate, timeSlotStartTime, res_token[1])
                    records = gs.sql_execute(pool, sql)

                    if records and records[0][1] not in ('canceled_by_doctor', 'canceled_by_patient','successful_finished','rescheduled_by_patient','free_finished'):
                        tmp = datetime.strptime(scheduleDate, '%Y-%m-%d')
                        tmpLang = data.get('lang', 'en')
                        if tmpLang == 'en':
                            scheduleDate = tmp.strftime("%d/%m/%Y")
                        else:
                            scheduleDate = tmp.strftime("%d/%m/%Y")
                        resp.update({'status_code': 401,
                                     'resq_body':
                                         {
                                             "code": "overlap_booking",
                                             "title": _("Overlap booking"),
                                             "msg": _("You have an appointment with doctor ") + records[0][2] + _(" during this time at ") + timeSlotStartTime + ", " + scheduleDate + _(". Please pick another date for this appointment.")
                                         }
                                     })
                        return resp

                    sql = '''select apm.id,apm.state
                                from appointment apm 
                                inner join user_account ua on apm.doctor_id = ua.id                               
                                where ua.user_mngment_id = %s and apm.schedule_date = '%s %s' ''' % (doctor_id, scheduleDate, timeSlotStartTime)
                    records = gs.sql_execute(pool, sql)
                    if records and records[0][1] not in ('canceled_by_doctor', 'canceled_by_patient','successful_finished','rescheduled_by_patient','free_finished'):
                        resp.update({'status_code': 401,
                                     'resq_body':
                                         {
                                             "code": "doctor_unavailable",
                                             "title": _("Doctor unavailable"),
                                             "msg": _("The doctor is not available for the time you choose")
                                         }
                                     })
                        return resp
                    else:
                        sql = '''select consultation_fee,
                                  consultation_duration,
                                  consultation_unit 
                                from doctor_profile where user_mngment_id = %s ''' % (doctor_id)
                        doctor_info = gs.sql_execute(pool, sql)
                        doctor_fee = doctor_info[0][0]
                        consultation_duration = doctor_info[0][1]
                        consultation_unit = doctor_info[0][2]
                        promotion_cost = 0
                        full_fee = doctor_fee + max(self.min_commission, doctor_fee * (self.com_ratio/100))
                        if promotionToken:
                            sql_prom = '''select discount_type, value, pt.id
                                        from promotion_transaction pt
                                          left join promotion_code pc on pc.id = promotion_id
                                        where token = %s
                                            and state = 'confirmed'
                                            and pt.expiry_time >= current_timestamp
                                            and active = True 
                                            and coalesce(pc.expiry_time,'9999-12-31'::timestamp) > current_timestamp'''
                            promotion = gs.sql_execute(pool, sql_prom, para_values=[promotionToken])
                            if not promotion:
                                resp.update({'status_code': 401,
                                             'resq_body': {
                                                 "code": "expired_promotion_code",
                                                 "title": _("Expired promotion code"),
                                                 "msg": _("The promotion code is invalid or expired, please try another one.")
                                             }
                                             })
                                return resp
                            else:
                                if promotion[0][0] == 'fixed_value':
                                    promotion_cost = promotion[0][1]
                                else:
                                    promotion_cost = full_fee * (float(promotion[0][1]) / 100)

                        sql = '''SELECT id FROM user_account WHERE user_mngment_id = %s''' % (doctor_id)
                        doctor_ua_id = gs.sql_execute(pool, sql)[0][0]
                        sql = '''select id, 
                                        user_account_id, 
                                        available_balance 
                                from user_wallet 
                                where user_account_id in (%s,%s) ''' % (res_token[1], doctor_ua_id)
                        wallets = gs.sql_execute(pool, sql)
                        info_wallets = {}
                        for wallet in wallets:
                            if int(wallet[1]) == int(doctor_ua_id):
                                info_wallets.update({'doctor': {'wallet_id': wallet[0],
                                                                'available_balance': wallet[2]}})
                            else:
                                info_wallets.update({'patient': {'wallet_id': wallet[0],
                                                                 'available_balance': wallet[2]}})
                        if 'patient' in info_wallets:
                            patient_balance = info_wallets['patient']['available_balance']
                            if patient_balance < doctor_fee + max(self.min_commission, doctor_fee * (self.com_ratio/100)) - promotion_cost:
                                resp.update({'status_code': 401,
                                             'resq_body':
                                                 {
                                                     "code": "not_enough_balance",
                                                     "title": _("Not enough money"),
                                                     "msg": _("Your account balance is not enough for booking the appointment")
                                                 }
                                             })
                                return resp
                            else:
                                currentTime = datetime.now().strftime('%Y-%m-%d %H:%M')
                                apm_cost = full_fee - promotion_cost
                                sql_insert = '''INSERT INTO appointment (created_date, 
                                                                         schedule_date,
                                                                         state,
                                                                         doctor_id,
                                                                         patient_id,
                                                                         write_date,
                                                                         consultation_duration,
                                                                         consultation_fee,
                                                                         cost,
                                                                         unit_cost,
                                                                         type) 
                                                VALUES (%s,%s,'confirmed_by_patient',%s,%s,%s,%s,%s,%s,%s,%s)
                                                RETURNING ID'''

                                apm_id = gs.sql_execute(pool, sql_insert, commit=True, para_values=(currentTime,
                                                                                                    '%s %s' % (
                                                                                                    scheduleDate,
                                                                                                    timeSlotStartTime),
                                                                                                    doctor_ua_id,
                                                                                                    res_token[1],
                                                                                                    currentTime,
                                                                                                    consultation_duration,
                                                                                                    doctor_fee,
                                                                                                    apm_cost,
                                                                                                    consultation_unit,
                                                                                                    'by_appointment'))
                                apm_id = apm_id[0][0]
                                # Save trans apm
                                sql_insert_trans = '''INSERT INTO appointment_transaction(create_date, 
                                                                                          write_date, 
                                                                                          appointment_id, 
                                                                                          payment_trans_id, 
                                                                                          from_state, 
                                                                                          to_state, 
                                                                                          user_account_id,
                                                                                          note)
                                                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s)'''
                                gs.sql_execute(pool, sql_insert_trans, commit=True, para_values=(currentTime,
                                                                                                 currentTime,
                                                                                                 apm_id,
                                                                                                 0,
                                                                                                 'new',
                                                                                                 'confirmed_by_patient',
                                                                                                 res_token[1],
                                                                                                 'Create APM'))
                                if promotion_cost > 0:
                                    discount_tran_id = bt.create_appointment_discount(apm_id,
                                                                                      promotion_cost,
                                                                                      wallet_source_id=self.drdr_wallet_id,
                                                                                      wallet_dest_id=
                                                                                      info_wallets['patient']['wallet_id'])
                                    sql = '''update promotion_transaction set state = 'done' where id = %s'''
                                    gs.sql_execute(pool, sql, commit=True, para_values=[promotion[0][2]])
                                bt.create_payment_appointment(apm_id, doctor_fee,
                                                              wallet_source_id=info_wallets['patient']['wallet_id'],
                                                              wallet_dest_id=self.drdr_wallet_id, note=None,
                                                              context=None)
                                bt.confirm_appointment(apm_id)

                                # Push notify to webapp
                                gs.publish_message_mqtt("%s" % doctor_ua_id, 'patient',
                                                        context={'action': 'patient_create_appointment',
                                                                 'appointmentId': int(apm_id),
                                                             'lang': data.get('lang', 'en')})
                                resp.update({'status_code': 200,
                                             'resq_body':
                                                 {
                                                     'appointmentId': apm_id
                                                 }
                                             })
                        else:
                            resp.update({'status_code': 401,
                                         'resq_body':
                                             {
                                                 "code": "not_enough_balance",
                                                 "title": _("Not enough money"),
                                                 "msg": _("Your account balance is not enough for booking the appointment")
                                             }
                                         })
                            return resp
                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
                    return resp

        except Exception as e:
            logger.error('error_patient_book_the_appointment %s' % str(e))
            client.captureException()
        return resp

    # MP8.6 API for getting schedule status of a date range
    def get_schedule_status_of_a_date_range(self, data):
        logger.info('get_schedule_status_of_a_date_range %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "get_schedule_status_failed",
                        "title": _("Get schedule status failed"),
                        "msg": _("An error occurred. Please try to set again.")
                    }
                }
        try:
            startDate = data.get('startDate', None)
            endDate = data.get('endDate', None)
            
            start_datetime = datetime.strptime('''%s 00:00:00''' % (startDate), '%d-%m-%Y %H:%M:%S')
            end_datetime = datetime.strptime('''%s 23:59:59''' % (endDate), '%d-%m-%Y %H:%M:%S')

            if end_datetime < start_datetime:
                resp.update({'status_code': 401,
                             'resq_body':
                                 {
                                     "code": "wrong_time",
                                     "title": _("Wrong time"),
                                     "msg": _("The end time is smaller than the start time, please try to set again")
                                 }
                             })
                return resp
            betweenTwoDates = abs((end_datetime - start_datetime).days)+1
            if (end_datetime < datetime.now()):
                dayStatusList = []
                for i in range(0, betweenTwoDates):
                    dayStatusList.append({
                        "date": (start_datetime + timedelta(days=i)).strftime('%d-%m-%Y'),
                        "status": "not_working"
                    })
                resp.update({'status_code': 200,
                             'resq_body': {
                                 'dayStatusList': dayStatusList
                             }
                             })
                return resp

            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            doctor_id = data.get('doctor_id')
            # endayOfMonth = calendar.monthrange(year, month)[1]
           
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))

                if res_token:
                    sql = '''SELECT apm.schedule_date
                            FROM appointment apm
                            INNER JOIN user_account ua on ua.id = apm.doctor_id
                            WHERE ua.user_mngment_id = %s AND
                            apm.schedule_date >= %s::timestamp AND apm.schedule_date <= %s::timestamp  
                            and apm.state in ('confirmed_by_patient','confirmed','processing','suspended') '''

                    apm_records = gs.sql_execute(pool, sql, para_values=(doctor_id, start_datetime, end_datetime))

                    whitelist = []
                    blacklist = []
                    dayStatusList = []
                    sql = '''SELECT cv.id, --0
                                    cv.start_datetime, --1
                                    cv.end_datetime, --2
                                    cv.duration --3
                            FROM calendar_event cv
                            INNER JOIN user_account ua on ua.id = cv.user_id 
                            WHERE cv.recurrency = False 
                            and cv.active = True 
                            AND ua.user_mngment_id = %s 
                            AND cv.start_datetime >= %s::timestamp 
                            AND cv.start_datetime <= %s::timestamp'''

                    records_checkexist = gs.sql_execute(pool, sql,
                                                        para_values=(doctor_id, start_datetime, end_datetime))

                    for line in records_checkexist:
                        whitelist.append({
                            'date': line[1].strftime('%d-%m-%Y'),
                            'startTime': line[1],
                            'endTime': line[2],
                            'duration': line[3]
                        })

                    sql_loop = '''select cv.id, cv.start_datetime, cv.end_datetime, cv.count, cv.interval, cv.recurrency, cv.byday, cv.week_list, cv.rrule_type, cv.month_by, cv.end_type,cv.su,cv.mo,cv.tu,cv.we,cv.th,cv.fr,cv.sa,cv.day,cv.duration,cv.final_date
                                                from calendar_event cv
                                                   INNER JOIN user_account ua on ua.id = cv.user_id
                                                where recurrency = True
                                                  and cv.active = True
                                                  and cv.recurrent_id is Null
                                                  and ua.user_mngment_id = %s
                                                  and cv.final_date >= %s
                                                  and cv.start_datetime <= %s
                                                                        '''
                    loop_checkexist = gs.sql_execute(pool, sql_loop,
                                                     para_values=(doctor_id, start_datetime, end_datetime))

                    for line in loop_checkexist:
                        count = int(line[3])
                        interval = int(line[4])
                        if line[8] == 'daily':
                            for i in range(0, count):
                                sd = line[1] + timedelta(days=interval * i)
                                if sd >= start_datetime and sd <= end_datetime:
                                    whitelist.append({
                                        'date': sd.strftime('%d-%m-%Y'),
                                        'startTime': line[1],
                                        'endTime': line[2],
                                        'duration': line[19]
                                    })
                                elif sd > end_datetime:
                                    break

                        elif line[8] == 'weekly':

                            days = []
                            if line[11] == True:  # su
                                days.append('su')
                            if line[12] == True:  # su
                                days.append('mo')
                            if line[13] == True:  # su
                                days.append('tu')
                            if line[14] == True:  # su
                                days.append('we')
                            if line[15] == True:  # su
                                days.append('th')
                            if line[16] == True:  # su
                                days.append('fr')
                            if line[17] == True:  # su
                                days.append('sa')

                            for i in range(0, betweenTwoDates):
                                sd = start_datetime + timedelta(days=i)
                                if sd.date() >= line[1].date() and sd < datetime.strptime(
                                                '''%s 00:00:00''' % (line[20]), '%Y-%m-%d %H:%M:%S'):
                                    dayOfweek = sd.date().strftime("%A").lower()[:2]
                                    if dayOfweek in days:
                                        whitelist.append({
                                            'date': sd.strftime('%d-%m-%Y'),
                                            'startTime': line[1],
                                            'endTime': line[2],
                                            'duration': line[19]
                                        })
                                elif sd > end_datetime:
                                    break

                        elif line[8] == 'monthly':
                            if line[1] <= end_datetime:
                                if line[9] == 'date':
                                    sd = start_datetime.replace(day=int(line[18]))
                                    whitelist.append({
                                        'date': sd.strftime('%d-%m-%Y'),
                                        'startTime': line[1],
                                        'endTime': line[2],
                                        'duration': line[19]
                                    })
                                elif line[9] == 'day':
                                    if line[7] == 'su':
                                        rr = rrule.rrule(rrule.WEEKLY, byweekday=relativedelta.SU,
                                                         dtstart=start_datetime)
                                    elif line[7] == 'mo':
                                        rr = rrule.rrule(rrule.WEEKLY, byweekday=relativedelta.MO,
                                                         dtstart=start_datetime)
                                    elif line[7] == 'tu':
                                        rr = rrule.rrule(rrule.WEEKLY, byweekday=relativedelta.TU,
                                                         dtstart=start_datetime)
                                    elif line[7] == 'we':
                                        rr = rrule.rrule(rrule.WEEKLY, byweekday=relativedelta.WE,
                                                         dtstart=start_datetime)
                                    elif line[7] == 'th':
                                        rr = rrule.rrule(rrule.WEEKLY, byweekday=relativedelta.TH,
                                                         dtstart=start_datetime)
                                    elif line[7] == 'fr':
                                        rr = rrule.rrule(rrule.WEEKLY, byweekday=relativedelta.FR,
                                                         dtstart=start_datetime)
                                    else:
                                        rr = rrule.rrule(rrule.WEEKLY, byweekday=relativedelta.SA,
                                                         dtstart=start_datetime)

                                    result = rr.between(start_datetime, end_datetime, inc=True)[-1]
                                    whitelist.append({
                                        'date': result.strftime('%d-%m-%Y'),
                                        'startTime': line[1],
                                        'endTime': line[2],
                                        'duration': line[19]
                                    })

                        sql = ''' SELECT cv.start_datetime, --0
                                        cv.end_datetime, --1
                                        cv.active,  --2
                                        cv.duration, -- 3
                                        cv.recurrent_id
                                        FROM calendar_event cv
                                        WHERE cv.recurrent_id = %s 
                                        AND cv.start_datetime >= %s::timestamp 
                                        AND cv.start_datetime <= %s::timestamp '''

                        checkexist = gs.sql_execute(pool, sql, para_values=(line[0], start_datetime, end_datetime))

                        if checkexist:
                            for ll in checkexist:
                                if ll[2] == True:
                                    whitelist.append({
                                        'date': ll[0].strftime('%d-%m-%Y'),
                                        'startTime': ll[0],
                                        'endTime': ll[1],
                                        'duration': ll[3]
                                    })
                                else:
                                    blacklist.append({
                                        'date': ll[0].strftime('%d-%m-%Y'),
                                        'startTime': ll[0],
                                        'endTime': ll[1],
                                        'duration': ll[3]
                                    })

                    sql = ''' SELECT consultation_duration
                                        FROM doctor_profile
                                        WHERE user_mngment_id = %s '''
                    duration = int(gs.sql_execute(pool, sql, para_values=(doctor_id,))[0][0][:-1])

                    whitetimeslot = []
                    for x in whitelist:
                        mul = int(x['duration']) / duration
                        for t in range(1, mul + 1):
                            if t == 1:
                                whitetimeslot.append({
                                    'date': x['date'],
                                    'startTime': x['startTime'].strftime('%H:%M'),
                                    'endTime': (x['startTime'] + timedelta(minutes=duration)).strftime('%H:%M')
                                })
                            elif t > 1 and t < mul:
                                whitetimeslot.append({
                                    'date': x['date'],
                                    'startTime': (x['startTime'] + timedelta(minutes=duration * (t - 1))).strftime(
                                        '%H:%M'),
                                    'endTime': (x['startTime'] + timedelta(minutes=duration * t)).strftime('%H:%M')
                                })
                            else:
                                whitetimeslot.append({
                                    'date': x['date'],
                                    'startTime': (x['startTime'] + timedelta(minutes=duration * (t - 1))).strftime(
                                        '%H:%M'),
                                    'endTime': x['endTime'].strftime('%H:%M')
                                })

                    white_unique_dicts = np.unique(np.array(whitetimeslot))

                    blacktimeslot = []
                    for x in blacklist:
                        mul = int(x['duration']) / int(duration)
                        for t in range(1, mul + 1):
                            if t == 1:
                                blacktimeslot.append({
                                    'date': x['date'],
                                    'startTime': x['startTime'].strftime('%H:%M'),
                                    'endTime': (x['startTime'] + timedelta(minutes=duration)).strftime('%H:%M')
                                })
                            elif t > 1 and t < mul:
                                blacktimeslot.append({
                                    'date': x['date'],
                                    'startTime': (x['startTime'] + timedelta(minutes=duration * (t - 1))).strftime(
                                        '%H:%M'),
                                    'endTime': (x['startTime'] + timedelta(minutes=duration * t)).strftime('%H:%M')
                                })
                            else:
                                blacktimeslot.append({
                                    'date': x['date'],
                                    'startTime': (x['startTime'] + timedelta(minutes=duration * (t - 1))).strftime(
                                        '%H:%M'),
                                    'endTime': x['endTime'].strftime('%H:%M')
                                })
                    black_of_unique_dicts = np.unique(np.array(blacktimeslot))

                    def checkContains(x, items):
                        for item in items:
                            if item['date'] == x['date']:
                                if (x['startTime'] >= item['startTime'] and x['startTime'] <= item['endTime']) or (
                                        x['endTime'] >= item['startTime'] and x['endTime'] <= item['endTime']):
                                    return True
                        return False

                    flag = "1-1-1990"
                    available = 0
                    notAvailable = 0
                    total = 0
                    for w in white_unique_dicts:
                        if flag != w['date']:
                            if (flag != "1-1-1990"):
                                timeSlot = {
                                    "date": flag,
                                }
                                if available == 0 and notAvailable == 0:
                                    timeSlot.update({
                                        "status": "not_working"
                                    })
                                else:
                                    if available / ((available + notAvailable) * 1.0) == 0:
                                        timeSlot.update({
                                            "status": "none"
                                        })
                                    elif available / ((available + notAvailable) * 1.0) < 0.5:
                                        timeSlot.update({
                                            "status": "few"
                                        })
                                    else:
                                        timeSlot.update({
                                            "status": "many"
                                        })

                                dayStatusList.append(timeSlot)
                            total = 1
                            flag = w['date']
                            available = 0
                            notAvailable = 0
                        else:
                            total = total + 1

                        if not checkContains(w, black_of_unique_dicts):
                            if datetime.now() >= start_datetime and datetime.now() <= end_datetime:
                                if datetime.now().date() > datetime.strptime(flag,'%d-%m-%Y').date():
                                    notAvailable = 0
                                    available = 0
                                elif datetime.now().strftime("%d-%m-%Y") == flag and (datetime.now() + timedelta(minutes=duration)) >= datetime.strptime('''%s %s'''%(flag,w[
                                    'endTime']),'%d-%m-%Y %H:%M'):
                                    notAvailable = notAvailable + 1
                                else:
                                    available = available + 1
                                    for apm in apm_records:
                                        # if not ((w['startTime'] > apm[0].strftime('%H:%M') and w['endTime'] > apm[
                                        #     0].strftime('%H:%M')) or
                                        #             (w['startTime'] < apm[0].strftime('%H:%M') and w['endTime'] < apm[
                                        #                 0].strftime('%H:%M'))) and apm[0].strftime('%d-%m-%Y') == w['date']:
                                        #     available = available - 1
                                        #     notAvailable = notAvailable + 1
                                        #     break
                                        time_check = apm[0].strftime('%H:%M')
                                        if (w['startTime'] <= time_check and time_check < w['endTime']) and apm[0].strftime('%d') == w['date']:
                                            available = available - 1
                                            notAvailable = notAvailable + 1
                                            break
                            else:
                                available = available + 1
                                for apm in apm_records:
                                    # if not ((w['startTime'] > apm[0].strftime('%H:%M') and w['endTime'] > apm[
                                    #     0].strftime('%H:%M')) or
                                    #             (w['startTime'] < apm[0].strftime('%H:%M') and w['endTime'] < apm[
                                    #                 0].strftime('%H:%M'))) and apm[0].strftime('%d-%m-%Y') == w['date']:
                                    #     available = available - 1
                                    #     notAvailable = notAvailable + 1
                                    #     break
                                    time_check = apm[0].strftime('%H:%M')
                                    if (w['startTime'] <= time_check and time_check < w['endTime']) and apm[0].strftime('%d') == w['date']:
                                        available = available - 1
                                        notAvailable = notAvailable + 1
                                        break

                    if (flag != "1-1-1990"):
                        timeSlot = {
                            "date": flag,
                        }
                        if available == 0 and notAvailable == 0:
                            timeSlot.update({
                                "status": "not_working"
                            })
                        else:
                            if available / ((available + notAvailable) * 1.0) == 0:
                                timeSlot.update({
                                    "status": "none"
                                })
                            elif available / ((available + notAvailable) * 1.0) < 0.5:
                                timeSlot.update({
                                    "status": "few"
                                })
                            else:
                                timeSlot.update({
                                    "status": "many"
                                })

                        dayStatusList.append(timeSlot)

                    def checkWorkingDay(day, dayStatusList):
                        for dayStatus in dayStatusList:
                            if dayStatus['date'] == day:
                                return dayStatus
                        return {
                            "date": day,
                            "status": "not_working"
                        }

                    resultList = []
                    for i in range(0, betweenTwoDates):
                        sd = (start_datetime + timedelta(days=i)).strftime('%d-%m-%Y')
                        resultList.append(checkWorkingDay(sd, dayStatusList))

                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     'dayStatusList': resultList
                                 }
                                 })


                else:
                    resp.update({'status_code': 203,
                                 'resq_body':
                                     {
                                         "code": "expired_token",
                                         "title": _("Expired token"),
                                         "msg": _("Your login-token is expired, please try to login again")
                                     }
                                 })
                    return resp

        except Exception as e:
            logger.error('error_get_schedule_status_of_a_date_range %s' % str(e))
            client.captureException()
        return resp
