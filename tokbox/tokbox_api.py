#!/usr/bin/env python
# -*- coding: utf-8 -*-
import falcon
import json
import logging

logger = logging.getLogger('drdr_api')
from tokbox_business import tokbox_business

import sys

sys.path.insert(0, "..")
try:
    from general import general_business
except ImportError as e:
    print('No Import ', e)

status_code = {
    200: falcon.HTTP_200,
    401: falcon.HTTP_401,
    502: falcon.HTTP_502,
    203: falcon.HTTP_203
}


class tokbox_api(object):
    def __init__(self, signal, erp_connection=False, var_contructor=False):
        self.business = tokbox_business(signal, erp_connection, var_contructor)
        self.pool_conn = erp_connection

        if var_contructor:
            self.ERP_DB_HOST = var_contructor['ERP_DB_HOST']
            self.ERP_DB_PORT = var_contructor['ERP_DB_PORT']
            self.ERP_DB_USER = var_contructor['ERP_DB_USER']
            self.ERP_DB_PASS = var_contructor['ERP_DB_PASS']
            self.ERP_DB_NAME = var_contructor['ERP_DB_NAME']

        self.signal = signal
        self.general_business = general_business(erp_connection, var_contructor)

    def on_post(self, req, resp,hospital_id=None):
        try:
            data = {}
            header = req.headers  # key cua header luon viet hoa
            all_header = self.general_business.get_header_request(header)
            lang = all_header.get('lang')
            _ = self.general_business.get_lang_instance(lang)
            data.update(all_header)

            if self.signal == 'tokbox_init':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Call failed"),
                    "msg": _("An error occurred. Please try to call again."),
                    "code": "call_failed"
                }
                try:
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    header = req.headers  # key cua header luon viet hoa
                    to_user_id = raw_data['to_id']
                    appointment_id = raw_data.get('appointment_id', False)
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'environment': header.get("X-APP-ENVIRONMENT", 'production'),
                        'to_user_id': to_user_id,
                        'appointment_id': appointment_id,
                        'lang': lang,
                    })
                    # old
                    record = self.business.tokbox_init(data)
                   #record = self.business.tokbox_init_hospital(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            elif self.signal == 'tokbox_init_hospital':
                resp_status = status_code[502]
                resp_body =  {
                            "title": _("Notice"),
                            "msg": _("Please remove and install new version app"),
                            "code": "update_version"
                    }
                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            elif self.signal == 'tokbox_init_hospital_v3':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Call failed"),
                    "msg": _("An error occurred. Please try to call again.2"),
                    "code": "call_failed"
                }
                try:
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    header = req.headers  # key cua header luon viet hoa
                    to_user_id = raw_data['to_id']
                    appointment_id = raw_data.get('appointment_id', False)
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'environment': header.get("X-APP-ENVIRONMENT", 'production'),
                        'to_user_id': to_user_id,
                        'appointment_id': appointment_id,
                        'lang': lang,
                        "hospital_id": hospital_id
                    })
                    print str(data)
                    # old
                    record = self.business.tokbox_init_hospital_v3(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            elif self.signal == 'tokbox_status_hospital_v3':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Status failed"),
                    "msg": _("An error occurred"),
                    "code": "status_failed"
                }
                try:
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    header = req.headers  # key cua header luon viet hoa
                    appointment_id = raw_data.get('appointment_id', False)
                    msg_error = raw_data.get('error_message', False)
                    msg = raw_data.get('message', False)
                    record = []
                    data.update({
                        'appointment_id': appointment_id,
                        "hospital_id": hospital_id,
                        'error_msg': msg_error
                    })
                    # old
                    print str(data)
                    if not appointment_id == False :
                        record = self.business.tokbox_status_hospital_v3(data)
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)


            elif self.signal == 'tokbox_call':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Call failed"),
                    "msg": _("An error occurred. Please try to call again."),
                    "code": "call_failed"
                }
                try:
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    header = req.headers  # key cua header luon viet hoa
                    #                     from_user_id = raw_data['from_id']
                    to_user_id = raw_data['to_id']
                    appointment_id = raw_data.get('appointment_id', False)
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'environment': header.get("X-APP-ENVIRONMENT", 'production'),
                        'to_user_id': to_user_id,
                        'appointment_id': appointment_id,
                        'lang': lang,
                    })
                    record = self.business.tokbox_call(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'tokbox_call_v2':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Call failed"),
                    "msg": _("An error occurred. Please try to call again."),
                    "code": "call_failed"
                }
                try:
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    header = req.headers  # key cua header luon viet hoa
                    call_history_id = raw_data['call_history_id']
                    if call_history_id:
                        data.update({
                            'user_token': header.get("X-USER-TOKEN", False),
                            'environment': header.get("X-APP-ENVIRONMENT", 'development'),
                            'call_history_id': call_history_id,
                            'lang': lang,
                        })
                        record = self.business.tokbox_call_v2(data)
                        if record:
                            resp_status = status_code[record['status_code']]
                            resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'tokbox_register':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Videocall register failed"),
                    "msg": _("An error occurred. Please try to register videocall again."),
                    "code": "videocall_register_failed"
                }
                try:
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    header = req.headers  # key cua header luon viet hoa
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'user_language': header.get("ACCEPT-LANGUAGE", False),
                        'lang': lang,
                    })
                    data.update(raw_data)
                    record = self.business.tokbox_register(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'tokbox_deregister':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Videocall deregister failed"),
                    "msg": _("An error occurred. Please try to deregister videocall again."),
                    "code": "videocall_deregister_failed"
                }
                try:
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    header = req.headers  # key cua header luon viet hoa
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'user_language': header.get("ACCEPT-LANGUAGE", False),
                        'lang': lang,
                    })
                    record = self.business.tokbox_deregister(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'tokbox_end':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Videocall end failed"),
                    "msg": _("An error occurred. Please try to end videocall again."),
                    "code": "videocall_end_failed"
                }
                try:
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    tokbox_token = raw_data['tokbox_token']
                    tokbox_session_id = raw_data['tokbox_session_id']
                    appointment_id = raw_data['appointment_id']
                    call_history_id = raw_data['call_history_id']
                    header = req.headers  # key cua header luon viet hoa
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'user_language': header.get("ACCEPT-LANGUAGE", False),
                        'tokbox_token': tokbox_token,
                        'tokbox_session_id': tokbox_session_id,
                        'appointment_id': appointment_id,
                        'call_history_id': call_history_id,
                        'lang': lang,
                    })
                    record = self.business.tokbox_end(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            
            elif self.signal == 'tokbox_end_v3':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Videocall end failed"),
                    "msg": _("An error occurred. Please try to end videocall again."),
                    "code": "videocall_end_failed"
                }
                try:
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    tokbox_token = raw_data['tokbox_token']
                    tokbox_session_id = raw_data['tokbox_session_id']
                    appointment_id = raw_data['appointment_id']
                    call_history_id = raw_data['call_history_id']
                    header = req.headers  # key cua header luon viet hoa
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'user_language': header.get("ACCEPT-LANGUAGE", False),
                        'tokbox_token': tokbox_token,
                        'tokbox_session_id': tokbox_session_id,
                        'appointment_id': appointment_id,
                        'call_history_id': call_history_id,
                        'lang': lang,
                    })
                    record = self.business.tokbox_end_v3(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)


            elif self.signal == 'tokbox_deny':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Videocall deny failed"),
                    "msg": _("An error occurred. Please try to deny videocall again."),
                    "code": "videocall_deny_failed"
                }
                try:
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    tokbox_token = raw_data['tokbox_token']
                    tokbox_session_id = raw_data['tokbox_session_id']
                    appointment_id = raw_data['appointment_id']
                    call_history_id = raw_data['call_history_id']
                    header = req.headers  # key cua header luon viet hoa
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'user_language': header.get("ACCEPT-LANGUAGE", False),
                        'tokbox_token': tokbox_token,
                        'tokbox_session_id': tokbox_session_id,
                        'appointment_id': appointment_id,
                        'call_history_id': call_history_id,
                        'lang': lang,
                    })
                    record = self.business.tokbox_deny(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'tokbox_deny_v3':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Videocall deny failed"),
                    "msg": _("An error occurred. Please try to deny videocall again."),
                    "code": "videocall_deny_failed"
                }
                try:
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    tokbox_token = raw_data['tokbox_token']
                    tokbox_session_id = raw_data['tokbox_session_id']
                    appointment_id = raw_data['appointment_id']
                    call_history_id = raw_data['call_history_id']
                    header = req.headers  # key cua header luon viet hoa
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'user_language': header.get("ACCEPT-LANGUAGE", False),
                        'tokbox_token': tokbox_token,
                        'tokbox_session_id': tokbox_session_id,
                        'appointment_id': appointment_id,
                        'call_history_id': call_history_id,
                        'lang': lang,
                    })
                    record = self.business.tokbox_deny_v3(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'tokbox_accept':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Videocall accept failed"),
                    "msg": _("An error occurred. Please try to accept videocall again."),
                    "code": "videocall_accept_failed"
                }
                try:
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    tokbox_token = raw_data['tokbox_token']
                    tokbox_session_id = raw_data['tokbox_session_id']
                    appointment_id = raw_data['appointment_id']
                    call_history_id = raw_data['call_history_id']
                    header = req.headers  # key cua header luon viet hoa
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'user_language': header.get("ACCEPT-LANGUAGE", False),
                        'tokbox_token': tokbox_token,
                        'tokbox_session_id': tokbox_session_id,
                        'appointment_id': appointment_id,
                        'call_history_id': call_history_id,
                        'lang': lang,
                    })
                    record = self.business.tokbox_accept(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'tokbox_accept_hospital':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Videocall accept failed"),
                    "msg": _("An error occurred. Please try to accept videocall again."),
                    "code": "videocall_accept_failed"
                }
                try:
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    tokbox_token = raw_data['tokbox_token']
                    tokbox_session_id = raw_data['tokbox_session_id']
                    appointment_id = raw_data['appointment_id']
                    call_history_id = raw_data['call_history_id']
                    header = req.headers  # key cua header luon viet hoa
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'user_language': header.get("ACCEPT-LANGUAGE", False),
                        'tokbox_token': tokbox_token,
                        'tokbox_session_id': tokbox_session_id,
                        'appointment_id': appointment_id,
                        'call_history_id': call_history_id,
                        'lang': lang,
                    })
                    record = self.business.tokbox_accept(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'tokbox_missed_v3':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Videocall missed failed"),
                    "msg": _("An error occurred. Please try to missed videocall again."),
                    "code": "videocall_missed_failed"
                }
                try:
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    call_history_id = raw_data['call_history_id']
                    header = req.headers  # key cua header luon viet hoa
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'user_language': header.get("ACCEPT-LANGUAGE", False),
                        'call_history_id': call_history_id,
                        'lang': lang
                    })
                    record = self.business.tokbox_missed_v3(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                    resp_body = str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

        except Exception as e:
            pass
