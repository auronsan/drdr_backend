#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from datetime import datetime
import time
from threading import Thread
from raven import Client
import ConfigParser
from opentok import OpenTok, MediaModes
import paho.mqtt.client as paho
import stripe
import sys
import json
import os
import jwt
import requests
import json
import numpy as nump
import zarr
project_path =  str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
config = ConfigParser.RawConfigParser(allow_no_value=True)
config.read(project_path + '/' + "environment.conf")
sentry_key = config.get('environment', 'sentry_key')
client_sentry = Client(sentry_key)
loggerStatus = logging.getLogger('drdr_api_status')
logger = logging.getLogger('drdr_api')

        
        
sys.path.insert(0, "..")
try:
    from general import general_business
    from payment.model.business_transfer_transaction import business_transfer_transaction
    from payment.model.access_model import access_model
    
except ImportError as e:
    print('No Import ', e)

com_ratio = float(config.get('environment', 'com_ratio')) or 40.0
min_commission = float(config.get('environment', 'min_commission')) or 40000.0

class tokbox_business(object):
    def __init__(self, signal, erp_connection, var_contructor):
        self.pool_conn = erp_connection

        self.ERP_DB_HOST = var_contructor['ERP_DB_HOST']
        self.ERP_DB_PORT = var_contructor['ERP_DB_PORT']
        self.ERP_DB_USER = var_contructor['ERP_DB_USER']
        self.ERP_DB_PASS = var_contructor['ERP_DB_PASS']
        self.ERP_DB_NAME = var_contructor['ERP_DB_NAME']
        self.general_business = general_business(erp_connection, var_contructor)
        tokbox_config = json.loads(config.get('environment', 'tokbox_config'))
        mqtt_config = json.loads(config.get('environment', 'mqtt_config'))

        self.api_key = tokbox_config['api_key']
        self.api_secret = tokbox_config['api_secret']
        self.mqtt_host = mqtt_config['host']
        self.mqtt_port = mqtt_config['port']
        self.authen_ssl = mqtt_config.get('authen_ssl',False)

        self.signal = signal
        self.opentok = OpenTok(self.api_key, self.api_secret)
        self.business_transfer = business_transfer_transaction(erp_connection, var_contructor)
        self.drdr_wallet_id = int(config.get('environment', 'drdr_wallet_id')) or 1
        self.vi_ = self.general_business.vi_
        self.en_ = self.general_business.en_
        self.drdr_wallet = int(config.get('environment', 'drdr_wallet_id')) or 1
        self.access_model = access_model(erp_connection, var_contructor)


    # MP3.7 API for patient to initiate the video call
    def send_push_notification_delay(self ,ios_device_token,droid_device_token,mqtt_body,_):
        time.sleep(5)
        if ios_device_token and ios_device_token not in ('','False','NULL'):
            print 'ios_device_token ', ios_device_token 
            ios_apns = json.loads(config.get('environment', 'ios_apns'))
            is_sandbox = ios_apns["sandbox"]

            # if ios_apns == "development":
            #     is_sandbox = True
            from apns import APNs, Payload
            apns = APNs(use_sandbox=is_sandbox, cert_file="tokbox/VOIP.pem", enhanced=True)
            mqtt_body["to"].update({ 
                "alert": _("You have a call from Doctor Doctor")
            })
            payload = Payload(alert=json.dumps(mqtt_body['to']))
            apns.gateway_server.send_notification(ios_device_token, payload)
        print 'droid_device_token ' ,droid_device_token
        if droid_device_token and droid_device_token not in ('','False','NULL'):
            fcm_key = config.get('environment', 'fcm_key')
            from pyfcm import FCMNotification
            push_service = FCMNotification(api_key=fcm_key)
            result = push_service.single_device_data_message(registration_id=droid_device_token, data_message=mqtt_body['to'])
            print 'result ',result

    def tokbox_init(self, data):
        logger.info('tokbox_init %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Call failed"),
                        "msg": _("An error occurred. Please try to call again."),
                        "code": "call_failed"
                    }
                }
        try:
            gs = self.general_business
            bt = self.business_transfer
            pool = self.pool_conn
            user_token = data.get('user_token')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    from_user_id = res_token[1]
                    to_profile_id = data.get('to_user_id')
                    sql_user = 'select id, ios_device_token from user_account where user_mngment_id = %s'
                    to_user = gs.sql_execute(pool, sql_user, para_values=[to_profile_id])
                    to_user_id = to_user[0][0]
                    appointment_id = data.get('appointment_id')
                    now = datetime.now()
                    create_date = now.strftime('%Y-%m-%d %H:%M:%S')
                    select_doc = ''' select ua.id,ua.activity_status,um.type_profile from user_account ua INNER JOIN user_mngment um ON ua.user_mngment_id = um.id where ua.id = %s ''' % (to_user_id)
                    select_result = gs.sql_execute(pool, select_doc)
                    if select_result[0][1] == 'busy' and select_result[0][2] == 'doctor' and res_token[6] == "patient":
                        resp = {'status_code': 401,
                        'resq_body':
                            {
                                "title": _("Doctor busy"),
                                "msg": _("Please call back later! Thank you"),
                                "code": "doctor_busy"
                            }
                        }
                        return resp
                    else :
                        if select_result[0][2] == 'doctor' :
                            update_doc = '''update user_account set activity_status = 'busy' where id = %s '''% (to_user_id)
                            gs.sql_execute(pool, update_doc, commit=True)
                        elif select_result[0][2] == 'patient' :
                            update_doc = '''update user_account set activity_status = 'busy' where id = %s '''% (from_user_id)
                            gs.sql_execute(pool, update_doc, commit=True)
                        if not appointment_id:
                            if res_token[6] == "patient":
                                sql = '''select consultation_fee,
                                            consultation_duration,
                                            consultation_unit 
                                        from doctor_profile where user_mngment_id = %s '''
                                doctor_info = gs.sql_execute(pool, sql, para_values=(to_profile_id,))
                                doctor_fee = doctor_info[0][0]
                                consultation_duration = doctor_info[0][1]
                                consultation_unit = doctor_info[0][2]

                                sql = '''SELECT id FROM user_account WHERE user_mngment_id = %s'''
                                doctor_ua_id = gs.sql_execute(pool, sql, para_values=(to_profile_id,))[0][0]
                                sql = '''select id, 
                                                user_account_id, 
                                                available_balance 
                                        from user_wallet 
                                        where user_account_id in (%s,%s) '''
                                wallets = gs.sql_execute(pool, sql, para_values=(res_token[1], doctor_ua_id))
                                info_wallets = {}
                                for wallet in wallets:
                                    if int(wallet[1]) == int(doctor_ua_id):
                                        info_wallets.update({'doctor': {'wallet_id': wallet[0],
                                                                        'available_balance': wallet[2]}})
                                    else:
                                        info_wallets.update({'patient': {'wallet_id': wallet[0],
                                                                        'available_balance': wallet[2]}})
                                if 'patient' in info_wallets:
                                    patient_balance = info_wallets['patient']['available_balance']
                                    full_fee = doctor_fee + max(min_commission, doctor_fee * (com_ratio/100))
                                    if patient_balance < full_fee:
                                        resp.update({'status_code': 401,
                                                    'resq_body':
                                                        {
                                                            "code": "not_enough_balance",
                                                            "title": _("Not enough balance"),
                                                            "msg": _("Your account balance is not enough for making the call")
                                                        }
                                                    })
                                        return resp
                                    else:
                                        insert_apm = '''insert into appointment (created_date,
                                                                                write_date,
                                                                                schedule_date,
                                                                                state,
                                                                                patient_id,
                                                                                doctor_id,
                                                                                consultation_duration,
                                                                                consultation_fee,
                                                                                cost,
                                                                                unit_cost,
                                                                                type)
                                                        values (%s, %s, %s, 'confirmed', %s, %s,%s,%s,%s,%s,%s)
                                                        RETURNING id'''
                                        appointment_id = \
                                        gs.sql_execute(pool, insert_apm, commit=True, para_values=(create_date,
                                                                                                create_date,
                                                                                                create_date,
                                                                                                from_user_id,
                                                                                                to_user_id,
                                                                                                consultation_duration,
                                                                                                doctor_fee,
                                                                                                full_fee,
                                                                                                consultation_unit,
                                                                                                'live',))[0][0]
                                        # Save trans apm
                                        sql_insert_trans = '''INSERT INTO appointment_transaction(create_date, 
                                                                                                write_date, 
                                                                                                appointment_id, 
                                                                                                payment_trans_id, 
                                                                                                from_state, 
                                                                                                to_state, 
                                                                                                user_account_id,
                                                                                                note)
                                                            VALUES (%s, %s, %s, %s, %s, %s, %s, %s)
                                                            '''
                                        gs.sql_execute(pool, sql_insert_trans, commit=True, para_values=(create_date,
                                                                                                        create_date,
                                                                                                        appointment_id,
                                                                                                        0,
                                                                                                        'new',
                                                                                                        'confirmed',
                                                                                                        res_token[1],
                                                                                                        'Create APM - Tokbox'))

                                        # TODO discount when video call???
                                        # bt.create_payment_appointment(appointment_id, doctor_fee,
                                        #                               wallet_source_id=info_wallets['patient']['wallet_id'],
                                        #                               wallet_dest_id=self.drdr_wallet_id, note=None,
                                        #                               context=None)
                                        # bt.confirm_appointment(appointment_id)
                                # TODO disable in production - don't allow doctor call direct
                                else:
                                    sql = '''select consultation_fee,consultation_duration,consultation_unit
                                            from doctor_profile where user_mngment_id = %s ''' % (res_token[7])
                                    doctor_info = gs.sql_execute(pool, sql)
                                    consultation_duration = doctor_info[0][1]
                                    consultation_unit = doctor_info[0][2]
                                    insert_apm = '''insert into appointment (created_date,
                                                                            write_date,
                                                                            schedule_date,
                                                                            state,
                                                                            doctor_id,
                                                                            patient_id,
                                                                            consultation_duration,
                                                                            consultation_fee,
                                                                            cost,
                                                                            unit_cost,
                                                                            type)
                                                    values (%s, %s, %s, 'confirmed', %s, %s,%s,%s,%s,%s,%s)
                                                    RETURNING id'''
                                    appointment_id = gs.sql_execute(pool, insert_apm, commit=True, para_values=(create_date,
                                                                                                                create_date,
                                                                                                                create_date,
                                                                                                                from_user_id,
                                                                                                                to_user_id,
                                                                                                                consultation_duration,
                                                                                                                doctor_fee,
                                                                                                                0,
                                                                                                                consultation_unit,
                                                                                                                'live'))[0][0]
                        else:
                            sql = '''SELECT state FROM appointment WHERE id = %s'''
                            appointment_state = gs.sql_execute(pool, sql, para_values=(appointment_id,))[0][0]
                            if appointment_state != 'suspended':
                                currentTime = datetime.now().strftime('%Y-%m-%d %H:%M')
                                sql_update = '''UPDATE appointment SET state= 'confirmed',write_date=%s WHERE id = %s '''
                                gs.sql_execute(pool, sql_update, commit=True, para_values=(currentTime,appointment_id,))
                        sql_select_session = '''select id,session_tokbox from user_account_session where user_account_id = %s''' %(to_user_id)
                        res_session = gs.sql_execute(pool, sql_select_session)
                        session_id = ""
                        if res_session:
                            session_id = res_session[0][1]
                        else :
                            session = self.opentok.create_session()
                            session_id = session.session_id
                            sql_insert_session = '''insert into user_account_session (session_tokbox,user_account_id) values (%s,%s)'''
                            gs.sql_execute(pool, sql_insert_session, commit=True, para_values=(session_id,to_user_id))
                        
                        from_user_token = self.opentok.generate_token(session_id)
                        to_user_token = self.opentok.generate_token(session_id)
                        insert_call_his = '''insert into appointment_call_history (appointment_id,
                                                                                create_date,
                                                                                write_date,
                                                                                session_id,
                                                                                token_to,
                                                                                token_from,
                                                                                state)
                                                    values (%s,%s,%s,%s,%s,%s, 'processing')
                                                    RETURNING id'''
                        call_history_id = gs.sql_execute(pool,
                                                        insert_call_his,
                                                        commit=True,
                                                        para_values=(appointment_id,
                                                                    create_date,
                                                                    create_date,
                                                                    session_id,
                                                                    to_user_token,
                                                                    from_user_token))[0][0]
                        resp.update({
                            'status_code': 200,
                            'resq_body': {
                                "tokbox_api_key": str(self.api_key),
                                "tokbox_token": str(from_user_token),
                                "tokbox_session_id": str(session_id),
                                "appointment_id": int(appointment_id),
                                "call_history_id": int(call_history_id)

                            }
                        })
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }

        except Exception as e:
            logger.error('error_tokbox_init %s' % str(e))
            client_sentry.captureException()

        return resp
    def tokbox_init_hospital(self, data):
        logger.info('tokbox_init hospital %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Call failed"),
                        "msg": _("An error occurred. Please try to call again."),
                        "code": "call_failed"
                    }
                }
        try:
            gs = self.general_business
            bt = self.business_transfer
            pool = self.pool_conn
            user_token = data.get('user_token')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    from_user_id = res_token[1]
                    to_profile_id = data.get('to_user_id')
                    sql_user = 'select id, ios_device_token from user_account where user_mngment_id = %s'
                    to_user = gs.sql_execute(pool, sql_user, para_values=[to_profile_id])
                    to_user_id = to_user[0][0]
                    appointment_id = data.get('appointment_id')
                    now = datetime.now()
                    create_date = now.strftime('%Y-%m-%d %H:%M:%S')
                    
                    select_doc = ''' select ua.id,ua.activity_status,um.type_profile from user_account ua INNER JOIN user_mngment um ON ua.user_mngment_id = um.id where ua.id = %s ''' % (to_user_id)
                    select_result = gs.sql_execute(pool, select_doc)
                    if select_result[0][1] == 'busy' and select_result[0][2] == 'doctor' and res_token[6] == 'patient':
                        resp = {'status_code': 401,
                        'resq_body':
                            {
                                "title": _("Doctor busy"),
                                "msg": _("Please call back later! Thank you"),
                                "code": "doctor_busy"
                            }
                        }
                        return resp
                    else :
                        #if select_result[0][2] == 'doctor' :
                        #    update_doc = '''update user_account set activity_status = 'busy' where id = %s '''% (to_user_id)
                        #    gs.sql_execute(pool, update_doc, commit=True)
                        #elif select_result[0][2] == 'patient' :
                        #    update_doc = '''update user_account set activity_status = 'busy' where id = %s '''% (from_user_id)
                        #    gs.sql_execute(pool, update_doc, commit=True)
                        #update_doc = '''update user_account set activity_status = 'busy' where id = %s '''% (to_user_id)
                        #gs.sql_execute(pool, update_doc, commit=True)
                        if not appointment_id:
                            if res_token[6] == "patient":
                                insert_new = ''' insert into call_record_logs (patient_id,doctor_id,status_call,last_update) values (%s,%s,'%s',current_timestamp) ''' % (from_user_id,to_user_id,'init')
                                insert_new_result = gs.sql_execute(pool, insert_new, commit=True)

                                sql = '''select consultation_fee,
                                            consultation_duration,
                                            consultation_unit 
                                        from doctor_profile where user_mngment_id = %s '''
                                doctor_info = gs.sql_execute(pool, sql, para_values=(to_profile_id,))
                                doctor_fee = doctor_info[0][0]
                                consultation_duration = doctor_info[0][1]
                                consultation_unit = doctor_info[0][2]

                                sql = '''SELECT id FROM user_account WHERE user_mngment_id = %s'''
                                doctor_ua_id = gs.sql_execute(pool, sql, para_values=(to_profile_id,))[0][0]
                                sql = '''select id, 
                                                user_account_id, 
                                                available_balance 
                                        from user_wallet 
                                        where user_account_id in (%s,%s) '''
                                wallets = gs.sql_execute(pool, sql, para_values=(res_token[1], doctor_ua_id))
                                info_wallets = {}
                                for wallet in wallets:
                                    if int(wallet[1]) == int(doctor_ua_id):
                                        info_wallets.update({'doctor': {'wallet_id': wallet[0],
                                                                        'available_balance': wallet[2]}})
                                    else:
                                        info_wallets.update({'patient': {'wallet_id': wallet[0],
                                                                        'available_balance': wallet[2]}})
                                if 'patient' in info_wallets:
                                    patient_balance = info_wallets['patient']['available_balance']
                                    full_fee = 0 #free
                                    if patient_balance < full_fee:
                                        resp.update({'status_code': 401,
                                                    'resq_body':
                                                        {
                                                            "code": "not_enough_balance",
                                                            "title": _("Not enough balance"),
                                                            "msg": _("Your account balance is not enough for making the call")
                                                        }
                                                    })
                                        return resp
                                    else:
                                        insert_apm = '''insert into appointment (created_date,
                                                                                write_date,
                                                                                schedule_date,
                                                                                state,
                                                                                patient_id,
                                                                                doctor_id,
                                                                                consultation_duration,
                                                                                consultation_fee,
                                                                                cost,
                                                                                unit_cost,
                                                                                type)
                                                        values (%s, %s, %s, 'confirmed', %s, %s,%s,%s,%s,%s,%s)
                                                        RETURNING id'''
                                        appointment_id = \
                                        gs.sql_execute(pool, insert_apm, commit=True, para_values=(create_date,
                                                                                                create_date,
                                                                                                create_date,
                                                                                                from_user_id,
                                                                                                to_user_id,
                                                                                                consultation_duration,
                                                                                                doctor_fee,
                                                                                                full_fee,
                                                                                                consultation_unit,
                                                                                                'live',))[0][0]
                                        # Save trans apm
                                        sql_insert_trans = '''INSERT INTO appointment_transaction(create_date, 
                                                                                                write_date, 
                                                                                                appointment_id, 
                                                                                                payment_trans_id, 
                                                                                                from_state, 
                                                                                                to_state, 
                                                                                                user_account_id,
                                                                                                note)
                                                            VALUES (%s, %s, %s, %s, %s, %s, %s, %s)
                                                            '''
                                        gs.sql_execute(pool, sql_insert_trans, commit=True, para_values=(create_date,
                                                                                                        create_date,
                                                                                                        appointment_id,
                                                                                                        0,
                                                                                                        'new',
                                                                                                        'confirmed',
                                                                                                        res_token[1],
                                                                                                        'Create APM - Tokbox'))

                                        # TODO discount when video call???
                                        # bt.create_payment_appointment(appointment_id, doctor_fee,
                                        #                               wallet_source_id=info_wallets['patient']['wallet_id'],
                                        #                               wallet_dest_id=self.drdr_wallet_id, note=None,
                                        #                               context=None)
                                        # bt.confirm_appointment(appointment_id)
                                # TODO disable in production - don't allow doctor call direct
                                else:
                                    sql = '''select consultation_fee,consultation_duration,consultation_unit
                                            from doctor_profile where user_mngment_id = %s ''' % (res_token[7])
                                    doctor_info = gs.sql_execute(pool, sql)
                                    consultation_duration = doctor_info[0][1]
                                    consultation_unit = doctor_info[0][2]
                                    insert_apm = '''insert into appointment (created_date,
                                                                            write_date,
                                                                            schedule_date,
                                                                            state,
                                                                            doctor_id,
                                                                            patient_id,
                                                                            consultation_duration,
                                                                            consultation_fee,
                                                                            cost,
                                                                            unit_cost,
                                                                            type)
                                                    values (%s, %s, %s, 'confirmed', %s, %s,%s,%s,%s,%s,%s)
                                                    RETURNING id'''
                                    appointment_id = gs.sql_execute(pool, insert_apm, commit=True, para_values=(create_date,
                                                                                                                create_date,
                                                                                                                create_date,
                                                                                                                from_user_id,
                                                                                                                to_user_id,
                                                                                                                consultation_duration,
                                                                                                                doctor_fee,
                                                                                                                0,
                                                                                                                consultation_unit,
                                                                                                                'live'))[0][0]

                        else:
                            sql = '''SELECT state FROM appointment WHERE id = %s'''
                            appointment_state = gs.sql_execute(pool, sql, para_values=(appointment_id,))[0][0]
                            if appointment_state != 'suspended':
                                currentTime = datetime.now().strftime('%Y-%m-%d %H:%M')
                                sql_update = '''UPDATE appointment SET state= 'confirmed',write_date=%s WHERE id = %s '''
                                gs.sql_execute(pool, sql_update, commit=True, para_values=(currentTime,appointment_id,))
                        sql_select_session = '''select id,session_tokbox from user_account_session where user_account_id = %s''' %(to_user_id)
                        res_session = gs.sql_execute(pool, sql_select_session)
                        session_id = ""
                        if res_session:
                            session_id = res_session[0][1]
                        else :
                            session = self.opentok.create_session()
                            session_id = session.session_id
                            sql_insert_session = '''insert into user_account_session (session_tokbox,user_account_id) values (%s,%s)'''
                            gs.sql_execute(pool, sql_insert_session, commit=True, para_values=(session_id,to_user_id))
                        
                        encoded = jwt.encode({
                            "iss": "45882532",
                            "ist": "project",
                            "iat": int(time.time()),
                            "exp": int(time.time())+300
                            }, 'ea48282fe2a0c7580294b8a65f41a15ca38904ac', algorithm='HS256')
                        logger.info(session_id)
                        url = 'https://api.opentok.com/v2/project/45882532/session/'+ session_id +'/stream/'
                        headers = {'X-OPENTOK-AUTH': encoded, 'Accept': 'application/json'}
                        r = requests.get(url,headers=headers)
                        responseTokbox = r.json()
                        if(responseTokbox.get('count')) :
                            if(responseTokbox.get('count') >= 2) :
                                logger.info('full session hospital init')
                                resp = {
                                    'status_code': 401,
                                    'resq_body':
                                    {
                                        "title": ("User busy"),
                                        "msg": _("Please call back later! Thank you"),
                                        "code": "user_busy"
                                    }
                                }
                                return resp
                            else :
                                logger.info('open 1 session')
                        else : 
                            logger.info('free')
                        from_user_token = self.opentok.generate_token(session_id)
                        to_user_token = self.opentok.generate_token(session_id)
                        #sql_select_session = '''select id,session_tokbox from user_account_session where user_account_id = %s''' %(to_user_id)
                        #res_session = gs.sql_execute(pool, sql_select_session)
                        #session_id = ""
                        #if res_session:
                        #    session_id = res_session[0][1]
                        #else :
                        #    session = self.opentok.create_session(media_mode=MediaModes.routed)
                        #    session_id = session.session_id
                        #    sql_insert_session = '''insert into user_account_session (session_tokbox,user_account_id) values (%s,%s)'''
                        #    gs.sql_execute(pool, sql_insert_session, commit=True, para_values=(session_id,to_user_id))
                        #sql_select_token = '''select id,token_tokbox from user_account_token_tokbox where doctor_id = %s ''' %(to_user_id)
                        #res_select_token = gs.sql_execute(pool, sql_select_token)
                        #if not res_select_token:
                        #    from_user_token = self.opentok.generate_token(session_id)
                        #    to_user_token = self.opentok.generate_token(session_id)
                        #    sql_insert_session = '''insert into user_account_token_tokbox (token_tokbox,doctor_id,user_account_id) values (%s,%s,%s)'''
                        #    gs.sql_execute(pool, sql_insert_session, commit=True, para_values=(to_user_token,to_user_id,to_user_id))
                        #    sql_insert_session = '''insert into user_account_token_tokbox (token_tokbox,doctor_id,user_account_id) values (%s,%s,%s)'''
                        #    gs.sql_execute(pool, sql_insert_session, commit=True, para_values=(from_user_token,to_user_id,from_user_id))
                        #else :
                        #    from_user_token = res_select_token[0][1]
                        #    to_user_token = res_select_token[1][1]
                        insert_call_his = '''insert into appointment_call_history (appointment_id,
                                                                                create_date,
                                                                                write_date,
                                                                                session_id,
                                                                                token_to,
                                                                                token_from,
                                                                                state)
                                                    values (%s,%s,%s,%s,%s,%s, 'processing')
                                                    RETURNING id'''
                        call_history_id = gs.sql_execute(pool,
                                                        insert_call_his,
                                                        commit=True,
                                                        para_values=(appointment_id,
                                                                    create_date,
                                                                    create_date,
                                                                    session_id,
                                                                    to_user_token,
                                                                    from_user_token))[0][0]
                        resp.update({
                            'status_code': 200,
                            'resq_body': {
                                "tokbox_api_key": str(self.api_key),
                                "tokbox_token": str(from_user_token),
                                "tokbox_session_id": str(session_id),
                                "appointment_id": int(appointment_id),
                                "call_history_id": int(call_history_id)

                            }
                        })
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }

        except Exception as e:
            logger.error('error_tokbox_init hospital %s' % str(e))
            client_sentry.captureException()

        return resp
    
    def tokbox_init_hospital_v3(self, data):
        logger.info('tokbox_init hospital v3 %s', data)
        
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Call failed"),
                        "msg": _("An error occurred. Please try to call again."),
                        "code": "call_failed"
                    }
                }
        try:
            gs = self.general_business
            bt = self.business_transfer
            pool = self.pool_conn
            stripe_sk = config.get('environment', 'stripe_sk')

            user_token = data.get('user_token')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    from_user_id = res_token[1]
                    to_profile_id = data.get('to_user_id')
                    sql_user = '''select id, ios_device_token, droid_device_token, case when current_timestamp <= checkout_time and activity_status != 'offline' then True
                                                else False
                                            end onlineStatus from user_account where user_mngment_id = %s '''
                    to_user = gs.sql_execute(pool, sql_user, para_values=[to_profile_id])
                    to_user_id = to_user[0][0]
                    appointment_id = data.get('appointment_id')
                    #to_user[0][3] onlinestatus
                    if not to_user[0][3]: 
                        if res_token[6] == 'patient' :
                            resp = {'status_code': 401,
                            'resq_body':
                                {
                                    "title": _("Doctor offline"),
                                    "msg": _("Please call back later! Thank you"),
                                    "code": "doctor_offline"
                                }
                            }
                            return resp
                    ios_device_token = to_user[0][1]
                    droid_device_token = to_user[0][2]
                    doctor_ios_device_token  = to_user[0][1]
                    doctor_droid_device_token  = to_user[0][2]
                    now = datetime.now()
                    create_date = now.strftime('%Y-%m-%d %H:%M:%S')
                    
                    select_doc = ''' select ua.id,ua.activity_status,um.type_profile from user_account ua INNER JOIN user_mngment um ON ua.user_mngment_id = um.id where ua.id = %s ''' % (to_user_id)
                    select_result = gs.sql_execute(pool, select_doc)
                    isOnCall = False
                    z1 = zarr.load('/home/data/call.zarr')

                    patient_id_zarr = 0
                    doctor_id_zarr = 0
                    
                    if res_token[6] == 'patient' :
                        doctor_id_zarr  = to_user_id
                        patient_id_zarr = from_user_id
                    else :
                        doctor_id_zarr = from_user_id
                        patient_id_zarr = to_user_id
                    
                    #logger.info('tokbox_init hospital v3 doctor_id_zarr %s', doctor_id_zarr)
                    #logger.info('tokbox_init hospital v3 patient_id_zarr %s', patient_id_zarr)                                
                    try:
                        boolZarrArray = hasattr(z1, '__len__')
                        if boolZarrArray:
                            #if prod not use (z1==None) dev use (z1 == None)[0]
                            if (z1 == None)[0]:
                                logger.info('tokbox_init hospital v3 timestampLast z1 none ', z1)
                                z1 = zarr.zeros(1000,chunks = 1000, dtype='S40')
                                zarr.save('/home/data/call.zarr',z1)
                            else :
                                for item in z1 :
                                    if item: 
                                        itemarray = item.split(',')
                                        doctorLastID = int(itemarray[0])
                                        patientLastID = int(itemarray[1])
                                        timestampLast = int(itemarray[2])
                                        if timestampLast + 15 >= int(time.time()):
                                            if (doctorLastID == doctor_id_zarr or patientLastID == patient_id_zarr) and not (doctorLastID == doctor_id_zarr and patientLastID == patient_id_zarr):
                                                isOnCall = True
                                                break
                        else:
                            if (z1 == None) :
                                logger.info('tokbox_init hospital v3 timestampLast z1 none ', z1)
                                z1 = zarr.zeros(1000,chunks = 1000, dtype='S40')
                                zarr.save('/home/data/call.zarr',z1)
                            else :
                                for item in z1 :
                                    if item: 
                                        itemarray = item.split(',')
                                        doctorLastID = int(itemarray[0])
                                        patientLastID = int(itemarray[1])
                                        timestampLast = int(itemarray[2])
                                        if timestampLast + 15 >= int(time.time()):
                                            if (doctorLastID == doctor_id_zarr or patientLastID == patient_id_zarr) and not (doctorLastID == doctor_id_zarr and patientLastID == patient_id_zarr):
                                                isOnCall = True
                                                break
                            
                                

                    except Exception as e :
                        logger.info('tokbox_init hospital v3 error %s', e)
                                

                    
                    logger.info('tokbox_init  zarr data %s', isOnCall)
                    if isOnCall:
                        if res_token[6] == 'patient' :
                            resp = {'status_code': 401,
                            'resq_body':
                                {
                                    "title": _("Doctor busy"),
                                    "msg": _("Please call back later! Thank you"),
                                    "code": "doctor_busy"
                                }
                            }
                        else : 
                            resp = {'status_code': 401,
                            'resq_body':
                                {
                                    "title": _("Patient busy"),
                                    "msg": _("Please call back later! Thank you"),
                                    "code": "patient_busy"
                                }
                            }
                        return resp
                    else :
                        #update_doc = '''update user_account set activity_status = 'busy' where id = %s '''% (to_user_id)
                        #gs.sql_execute(pool, update_doc, commit=True)
                        #update_pat = '''update user_account set activity_status = 'busy' where id = %s '''% (from_user_id)
                        #gs.sql_execute(pool, update_pat, commit=True)
                        indexZarr = 0 
                        
                        if not appointment_id:
                            if res_token[6] == "patient":
                               

                                sql = '''select consultation_fee,
                                            consultation_duration,
                                            consultation_unit 
                                        from doctor_profile where user_mngment_id = %s '''
                                doctor_info = gs.sql_execute(pool, sql, para_values=(to_profile_id,))
                                doctor_fee = doctor_info[0][0]
                                consultation_duration = doctor_info[0][1]
                                consultation_unit = doctor_info[0][2]

                                sql = '''SELECT id FROM user_account WHERE user_mngment_id = %s'''
                                doctor_ua_id = gs.sql_execute(pool, sql, para_values=(to_profile_id,))[0][0]
                                sql = '''select id, 
                                                user_account_id, 
                                                available_balance 
                                        from user_wallet 
                                        where user_account_id in (%s,%s) '''
                                wallets = gs.sql_execute(pool, sql, para_values=(res_token[1], doctor_ua_id))
                                info_wallets = {}
                                for wallet in wallets:
                                    if int(wallet[1]) == int(doctor_ua_id):
                                        info_wallets.update({'doctor': {'wallet_id': wallet[0],
                                                                        'available_balance': wallet[2]}})
                                    else:
                                        info_wallets.update({'patient': {'wallet_id': wallet[0],
                                                                        'available_balance': wallet[2]}})
                                if 'patient' in info_wallets:
                                    patient_balance = info_wallets['patient']['available_balance']
                                    
                                    credit_card_payment = False

                                    full_fee = doctor_fee + max(min_commission, doctor_fee * (com_ratio/100)) #free

                                    sql_role_hotline = ''' select uaur.id,ur.code from user_account_user_role uaur INNER JOIN user_role ur ON ur.id = uaur.user_role_id where uaur.user_account_id =  %s ''' % (to_user_id)
                                    
                                    res_role_hotline = gs.sql_execute(pool, sql_role_hotline)
                                    
                                    default_appointment_status = 'confirmed'

                                    if doctor_fee * com_ratio / 100 <= min_commission:
                                        full_fee = doctor_fee + min_commission
                                    else:
                                        full_fee = doctor_fee + (doctor_fee * com_ratio/100)

                                    #full_fee = 0 #free bypass ios
                                    if res_role_hotline and res_role_hotline[0][1] == 'hotline' :
                                        full_fee = 0
                                        #default_appointment_status = 'free_finished'
                                    #check credit_cardx
                                    if patient_balance < full_fee :
                                        resq_user_wallet =  info_wallets['patient']['wallet_id']
                                        user_wallet =  info_wallets['patient']['wallet_id']
                                        access_model = self.access_model
                                        subs_key = access_model.search('user_payment_info',
                                                       [('user_wallet_id', '=', resq_user_wallet)], ['subscription_key'])
                                        user_email = access_model.search('user_account', [('id', '=', from_user_id)], ['email'])
                                        amount = full_fee - patient_balance
                                        currency ='vnd'
                                        if subs_key:
                                            stripe.api_key = stripe_sk
                                            customer = stripe.Customer.retrieve(subs_key[0][0])
                                            default_card = customer.default_source
                                            logger.info('defaul card %s' % default_card)
                                            card_id = default_card
                                            charge_resp=stripe.Charge.create(
                                                amount=int(amount),
                                                currency=currency,
                                                customer=subs_key[0][0],
                                                card=card_id,
                                                description="Charge for email %s"%user_email[0][0]
                                            )
                                            logger.info('charge_resp Successful: %s' % charge_resp)

                                            if charge_resp:
                                                r_amount=charge_resp.get('amount', False)
                                                r_currency=charge_resp.get('currency', False)
                                                r_customer=charge_resp.get('customer', False)
                                                r_charge_id = charge_resp.get('id', False)
                                                note=charge_resp.get('outcome', False)
                                                source=charge_resp.get('source', False)
                                                # if note:
                                                #     note=note.get('network_status', False)
                                                if r_amount and r_currency and r_customer and r_charge_id:
                                                    if r_amount == amount and r_currency == currency and\
                                                                    r_customer == subs_key[0][0]:
                                                        data = {}
                                                        
                                                        data.update({
                                                            'amount': r_amount,
                                                            'state': 'done',
                                                            'note': note and note.get('network_status', False) or None,
                                                            'customer_token': subs_key or None,
                                                            'type': source and source.get('brand', False) or '',
                                                            'payment_gateway': 'stripe',
                                                            'user_wallet_id': resq_user_wallet or user_wallet,
                                                            'gateway_trans_code': r_charge_id or ''
                                                        })
                                                        create_tran = access_model.create('payment_transaction', data)
                                                        if create_tran:
                                                            trans_id = bt.action_create_in(self.drdr_wallet,
                                                                                                        resq_user_wallet or user_wallet, amount,
                                                                                                        create_tran, currency=currency)
                                                            available_balance = access_model.search('user_wallet', [('id', '=', resq_user_wallet or user_wallet)],
                                                                                            ['available_balance'])
                                                            if trans_id:
                                                                logger.info('create_payment_trans Successful: %s' % create_tran)
                                                                credit_card_payment = True
                                        
                                    if patient_balance < full_fee and not credit_card_payment:
                                        resp.update({'status_code': 401,
                                                    'resq_body':
                                                        {
                                                            "code": "not_enough_balance",
                                                            "title": _("Not enough balance"),
                                                            "msg": _("Your account balance is not enough for making the call")
                                                        }
                                                    })
                                        return resp
                                    else:
                                        insert_apm = '''insert into appointment (created_date,
                                                                                write_date,
                                                                                schedule_date,
                                                                                state,
                                                                                patient_id,
                                                                                doctor_id,
                                                                                consultation_duration,
                                                                                consultation_fee,
                                                                                cost,
                                                                                unit_cost,
                                                                                type)
                                                        values (%s, %s, %s, 'confirmed', %s, %s,%s,%s,%s,%s,%s)
                                                        RETURNING id'''
                                        appointment_id = \
                                        gs.sql_execute(pool, insert_apm, commit=True, para_values=(create_date,
                                                                                                create_date,
                                                                                                create_date,
                                                                                                from_user_id,
                                                                                                to_user_id,
                                                                                                consultation_duration,
                                                                                                doctor_fee,
                                                                                                full_fee,
                                                                                                consultation_unit,
                                                                                                'live',))[0][0]
                                        # Save trans apm
                                        sql_insert_trans = '''INSERT INTO appointment_transaction(create_date, 
                                                                                                write_date, 
                                                                                                appointment_id, 
                                                                                                payment_trans_id, 
                                                                                                from_state, 
                                                                                                to_state, 
                                                                                                user_account_id,
                                                                                                note)
                                                            VALUES (%s, %s, %s, %s, %s, %s, %s, %s)
                                                            '''
                                        gs.sql_execute(pool, sql_insert_trans, commit=True, para_values=(create_date,
                                                                                                        create_date,
                                                                                                        appointment_id,
                                                                                                        0,
                                                                                                        'new',
                                                                                                        'confirmed',
                                                                                                        res_token[1],
                                                                                                        'Create APM - Tokbox'))

                                        insert_new = ''' insert into call_record_logs (patient_id,doctor_id,status_call,last_update,apm_id) values (%s,%s,'%s',current_timestamp,%s) ''' % (from_user_id,to_user_id,'init',appointment_id)
                                        insert_new_result = gs.sql_execute(pool, insert_new, commit=True)
                                        # TODO discount when video call???
                                        # bt.create_payment_appointment(appointment_id, doctor_fee,
                                        #                               wallet_source_id=info_wallets['patient']['wallet_id'],
                                        #                               wallet_dest_id=self.drdr_wallet_id, note=None,
                                        #                               context=None)
                                        # bt.confirm_appointment(appointment_id)
                                # TODO disable in production - don't allow doctor call direct
                                else:
                                    sql = '''select consultation_fee,consultation_duration,consultation_unit
                                            from doctor_profile where user_mngment_id = %s ''' % (res_token[7])
                                    doctor_info = gs.sql_execute(pool, sql)
                                    consultation_duration = doctor_info[0][1]
                                    consultation_unit = doctor_info[0][2]
                                    insert_apm = '''insert into appointment (created_date,
                                                                            write_date,
                                                                            schedule_date,
                                                                            state,
                                                                            doctor_id,
                                                                            patient_id,
                                                                            consultation_duration,
                                                                            consultation_fee,
                                                                            cost,
                                                                            unit_cost,
                                                                            type)
                                                    values (%s, %s, %s, 'confirmed', %s, %s,%s,%s,%s,%s,%s)
                                                    RETURNING id'''
                                    appointment_id = gs.sql_execute(pool, insert_apm, commit=True, para_values=(create_date,
                                                                                                                create_date,
                                                                                                                create_date,
                                                                                                                from_user_id,
                                                                                                                to_user_id,
                                                                                                                consultation_duration,
                                                                                                                doctor_fee,
                                                                                                                0,
                                                                                                                consultation_unit,
                                                                                                                'live'))[0][0]

                        else:
                            sql = '''SELECT state,booking_status FROM appointment WHERE id = %s'''
                            appointment_sql = gs.sql_execute(pool, sql, para_values=(appointment_id,))
                            appointment_state = appointment_sql[0][0]
                            appointment_booking = appointment_sql[0][1]
                            if appointment_state != 'suspended' and appointment_booking == None:
                                currentTime = datetime.now().strftime('%Y-%m-%d %H:%M')
                                sql_update = '''UPDATE appointment SET state= 'confirmed',write_date=%s WHERE id = %s '''
                                gs.sql_execute(pool, sql_update, commit=True, para_values=(currentTime,appointment_id,))
                        sql_select_session = '''select id,session_tokbox from user_account_session where user_account_id = %s''' %(to_user_id)
                        res_session = gs.sql_execute(pool, sql_select_session)
                        session_id = ""
                        if res_session:
                            session_id = res_session[0][1]
                        else :
                            session = self.opentok.create_session(location=u'219.75.27.16')
                            session_id = session.session_id
                            sql_insert_session = '''insert into user_account_session (session_tokbox,user_account_id) values (%s,%s)'''
                            gs.sql_execute(pool, sql_insert_session, commit=True, para_values=(session_id,to_user_id))
                        try:
                            for index,item in enumerate(z1):
                                #logger.info('tokbox_init hospital v3 item %s', item)
                                if item == None or item == '':
                                    indexZarr = index
                                    break
                            if indexZarr == 1000 :
                                indexZarr = 0
                                z1 = zarr.zeros(1000,chunks = 1000, dtype='S40')
                                zarr.save('/home/data/call.zarr',z1)
                            z1[indexZarr] = str(doctor_id_zarr) + ',' + str(patient_id_zarr) +  ',' + str(int(time.time()))
                            zarr.save('/home/data/call.zarr',z1)
                        except Exception as e:
                            print e
                        encoded = jwt.encode({
                            "iss": "45882532",
                            "ist": "project",
                            "iat": int(time.time()),
                            "exp": int(time.time())+300
                            }, 'ea48282fe2a0c7580294b8a65f41a15ca38904ac', algorithm='HS256')
                        logger.info(session_id)
                        url = 'https://api.opentok.com/v2/project/45882532/session/'+ session_id +'/stream/'
                        headers = {'X-OPENTOK-AUTH': encoded, 'Accept': 'application/json'}
                        r = requests.get(url,headers=headers)
                        responseTokbox = r.json()
                        #dataTokbox = {'type' : 'init_session' , 'data' : 'from rest api server'}
                        #url = 'https://api.opentok.com/v2/project/45882532/session/'+ session_id +'/signal/'
                        #r2 = requests.post(url,data=json.dumps(dataTokbox),headers=headers)
                        #initTokbox = r2
                        #logger.info('tokbox_signal_v3  %s' , initTokbox)
                        if(responseTokbox.get('count')) :
                            if(responseTokbox.get('count') >= 1) :
                                logger.info('full session hospital init')
                                if res_token[6] == 'patient' :
                                    resp = {
                                        'status_code': 401,
                                        'resq_body':
                                        {
                                            "title": _("Doctor busy"),
                                            "msg": _("Please call back later! Thank you"),
                                            "code": "doctor_busy"
                                            }
                                        }
                                else :
                                    resp = {
                                        'status_code': 401,
                                        'resq_body':
                                        {
                                            "title": _("Patient busy"),
                                            "msg": _("Please call back later! Thank you"),
                                            "code": "Patient_busy"
                                            }
                                        }
                                return resp
                            else :
                                logger.info('open 1 session')
                        else : 
                            logger.info('free')
                        from_user_token = self.opentok.generate_token(session_id)
                        to_user_token = self.opentok.generate_token(session_id)
                        #sql_select_session = '''select id,session_tokbox from user_account_session where user_account_id = %s''' %(to_user_id)
                        #res_session = gs.sql_execute(pool, sql_select_session)
                        #session_id = ""
                        #if res_session:
                        #    session_id = res_session[0][1]
                        #else :
                        #    session = self.opentok.create_session(media_mode=MediaModes.routed)
                        #    session_id = session.session_id
                        #    sql_insert_session = '''insert into user_account_session (session_tokbox,user_account_id) values (%s,%s)'''
                        #    gs.sql_execute(pool, sql_insert_session, commit=True, para_values=(session_id,to_user_id))
                        #sql_select_token = '''select id,token_tokbox from user_account_token_tokbox where doctor_id = %s ''' %(to_user_id)
                        #res_select_token = gs.sql_execute(pool, sql_select_token)
                        #if not res_select_token:
                        #    from_user_token = self.opentok.generate_token(session_id)
                        #    to_user_token = self.opentok.generate_token(session_id)
                        #    sql_insert_session = '''insert into user_account_token_tokbox (token_tokbox,doctor_id,user_account_id) values (%s,%s,%s)'''
                        #    gs.sql_execute(pool, sql_insert_session, commit=True, para_values=(to_user_token,to_user_id,to_user_id))
                        #    sql_insert_session = '''insert into user_account_token_tokbox (token_tokbox,doctor_id,user_account_id) values (%s,%s,%s)'''
                        #    gs.sql_execute(pool, sql_insert_session, commit=True, para_values=(from_user_token,to_user_id,from_user_id))
                        #else :
                        #    from_user_token = res_select_token[0][1]
                        #    to_user_token = res_select_token[1][1]
                        insert_call_his = '''insert into appointment_call_history (appointment_id,
                                                                                create_date,
                                                                                write_date,
                                                                                session_id,
                                                                                token_to,
                                                                                token_from,
                                                                                state)
                                                    values (%s,%s,%s,%s,%s,%s, 'processing')
                                                    RETURNING id'''
                        call_history_id = gs.sql_execute(pool,
                                                        insert_call_his,
                                                        commit=True,
                                                        para_values=(appointment_id,
                                                                    create_date,
                                                                    create_date,
                                                                    session_id,
                                                                    to_user_token,
                                                                    from_user_token))[0][0]

                        sql = '''SELECT state,booking_status FROM appointment WHERE id = %s'''
                        appointment_sql = gs.sql_execute(pool, sql, para_values=(appointment_id,))
                        appointment_state = appointment_sql[0][0]
                        appointment_booking = appointment_sql[0][1]
                        status_call = 'incomplete'
                        status_call_name = 'Incomplete'
                        if appointment_state in ('successful_finished','free_finished','canceled_by_doctor'):
                            status_call = 'complete'
                            status_call_name  = 'Complete'
                        resp.update({
                            'status_code': 200,
                            'resq_body': {
                                "tokbox_api_key": str(self.api_key),
                                "tokbox_token": str(from_user_token),
                                "tokbox_session_id": str(session_id),
                                "appointment_id": int(appointment_id),
                                "call_history_id": int(call_history_id),
                                "appointment_state": str(appointment_state),
                                "booking_status" : str(appointment_booking),
                                "stateCode" : str(status_call)

                            }
                        })
                        now = datetime.now()
                        create_date = now.strftime('%Y-%m-%d %H:%M:%S')
                        action_time = long(time.time())
                        scheduleTime = action_time
                        mqtt_body = {
                            "to": {
                            "action": "session_init",
                            "action_time": action_time,
                            "tokbox_api_key": str(self.api_key),
                            "tokbox_session_id": str(session_id),
                            "tokbox_token": str(to_user_token),
                            "appointment_id": int(appointment_id),
                            "call_history_id": int(call_history_id),
                            "scheduleTime": scheduleTime,
                            "booking_status" : str(appointment_booking),
                            "appointment_state": str(appointment_state),
                            "stateCode" : str(status_call)
                            }
                            }
                        import ssl
                        client = paho.Client()
                        if self.authen_ssl == True:
                            client = paho.Client(transport='websockets')
                            client.tls_set('tokbox/ca_file.crt', cert_reqs=ssl.CERT_NONE)
                            client.tls_insecure_set(True)
                        client.connect(self.mqtt_host, self.mqtt_port, 60)
                        client.loop_start()
                        if res_token[6] == "patient":
                            if appointment_id and appointment_state != None:
                                sql_update = '''update appointment set booking_status = 'close' where id = %s'''
                                gs.sql_execute(pool, sql_update, commit=True, para_values=[appointment_id])

                            to_user_id = to_user_id
                            sql = """select ua.city
                                        from user_contact uc
                                            left join user_address ua on ua.user_contact_id = uc.id
                                        where uc.user_mngment_id = %s AND ua.is_primary = True """
                            records = gs.sql_execute(pool, sql, para_values=(res_token[7],))

                            mqtt_body["to"].update({
                                "user_id": int(to_user_id),
                                "patient_firstname": res_token[8],
                                "patient_middlename": res_token[9],
                                "patient_lastname": res_token[10],
                                "patient_avatar_url": res_token[11],
                                "patient_genderCode": res_token[12],
                                "patient_id": res_token[7]
                            })

                            if res_token[13]:
                                age = datetime.now().date().year - res_token[13].timetuple().tm_year
                                mqtt_body["to"].update({
                                    "patient_age": age
                                })
                            else:
                                mqtt_body["to"].update({"patient_age": None})

                            if records:
                                mqtt_body["to"].update({"patient_primaryAddress_cityCode": records[0][0]})
                            else:
                                mqtt_body["to"].update({"patient_primaryAddress_cityCode": None})

                        elif res_token[6] == "doctor" or  res_token[6] == "hotline" :
                            to_user_id = to_user_id
                            specialty_codes = []
                            sql = """select drspc.specialties_code
                                        from doctor_profile dp
                                            left join dr_doctorprofile_specialties drspc on dp.id = drspc.doctor_profile_id
                                        where dp.user_mngment_id = %s """

                            records = gs.sql_execute(pool, sql, para_values=(res_token[7],))

                            for line in records:
                                specialty_codes.append({"code": line[0]})

                            mqtt_body["to"].update({
                                "user_id": int(to_user_id),
                                "doctor_firstname": res_token[8],
                                "doctor_middlename": res_token[9],
                                "doctor_lastname": res_token[10],
                                "doctor_avatar_url": res_token[11],
                                "doctor_specialty_code": specialty_codes,
                                "doctor_title": res_token[14],
                                "doctor_id": res_token[7]
                            })

                        #client.publish("%s"%from_user_id, str(mqtt_body["from"]), qos=1)
                        
                        if res_token[6] == 'doctor' or  res_token[6] == "hotline":
                            curr_thread = Thread(target=self.send_push_notification_delay, args=(ios_device_token,droid_device_token,mqtt_body,_))
                            curr_thread.daemon = False
                            curr_thread.start()
                            

                        if res_token[6] == 'patient':
                            if doctor_ios_device_token and doctor_ios_device_token not in ('','False','NULL'):
                                ios_apns = json.loads(config.get('environment', 'ios_apns'))
                                is_sandbox = ios_apns["sandbox"]
                                
                                from apns import APNs, Payload
                                apns = APNs(use_sandbox=is_sandbox, cert_file="DOCTORAPP.pem", enhanced=True)
                                mqtt_body["to"].update({ 
                                    "alert": _("You have a call from Hello Doctor")
                                })
                                payload = Payload(alert=json.dumps(mqtt_body['to']))
                                apns.gateway_server.send_notification(doctor_ios_device_token, payload)
                            
                            if doctor_droid_device_token and doctor_droid_device_token not in ('','False','NULL'):
                                fcm_key = config.get('environment', 'fcm_key')
                                from pyfcm import FCMNotification
                                push_service = FCMNotification(api_key=fcm_key)
                                result = push_service.single_device_data_message(registration_id=doctor_droid_device_token, data_message=mqtt_body['to'])
                                

                        client.publish("%s" % to_user_id, json.dumps(mqtt_body['to']), qos=2)
                        client.loop_stop()
                        client.disconnect()
                        
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }

        except Exception as e:
            logger.error('error_tokbox_init hospital %s' % str(e))
            client_sentry.captureException()

        return resp
    
    def tokbox_status_hospital_v3(self,data):
        logger.info('tokbox_status %s', data)
        loggerStatus.info('tokbox_status %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("error send notification"),
                        "msg": _("error_notif"),
                        "code": "error_notif"
                    }
                }
        try :
            gs = self.general_business
            #bt = self.business_transfer
            pool = self.pool_conn
            user_token = data.get('user_token')
            appointment_id = data.get('appointment_id')
            error_msg = data.get('error_msg')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    from_user_id = res_token[1]
                    sql = '''select doctor_id, 
                                        patient_id,
                                        schedule_date,
                                        state
                                 from appointment
                                 where id = %s '''
                    apm_info = gs.sql_execute(pool, sql, para_values=[appointment_id])[0]
                    sql_user = '''select id, ios_device_token, droid_device_token from user_account where id = %s '''
                    to_user = gs.sql_execute(pool, sql_user, para_values=[apm_info[0]])
                    to_user_id = to_user[0][0]
                    ios_device_token = to_user[0][1]
                    droid_device_token = to_user[0][2]
                    message_status = 'You received call but your device is off or no internet connected. Please check it! '
                    action_status = 'notification_doctor_timeout'
                    mqtt_body = {
                            "to": {
                            "action": action_status,
                            "action_time": long(time.time()),
                            "message" : _(message_status)
                            }
                        }
                    if res_token[6] == 'patient' and error_msg == 'doctor_timeout' :
                        if ios_device_token and ios_device_token not in ('','False','NULL'):
                            ios_apns = json.loads(config.get('environment', 'ios_apns'))
                            is_sandbox = ios_apns["sandbox"]
                            
                            from apns import APNs, Payload
                            apns = APNs(use_sandbox=is_sandbox, cert_file="DOCTORAPP.pem", enhanced=True)
                            mqtt_body["to"].update({ 
                                "alert": _(message_status)
                            })
                            payload = Payload(alert=json.dumps(mqtt_body['to']))
                            apns.gateway_server.send_notification(ios_device_token, payload)
                        
                        if droid_device_token and droid_device_token not in ('','False','NULL'):
                            fcm_key = config.get('environment', 'fcm_key')
                            from pyfcm import FCMNotification
                            push_service = FCMNotification(api_key=fcm_key)
                            mqtt_body["to"].update({ 
                                "alert": _(message_status)
                            })
                            result = push_service.single_device_data_message(registration_id=droid_device_token, data_message=mqtt_body['to'])
                    
                    resp = {'status_code': 200,
                            'resq_body':
                                {
                                    "title": _("notification success"),
                                    "msg": _("notification_success"),
                                    "code": "sucess"
                                }
                            }    
        except Exception as e:
            logger.error('error_send_notif %s' % str(e))
            resp = {'status_code': 200,
                            'resq_body':
                                {
                                    "title": _("error notif"),
                                    "msg": _("error"),
                                    "code": "error",
                                    "error" : str(e)
                                }
                            }
        return resp

    def tokbox_call(self, data):
        logger.info('tokbox_call %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Call failed"),
                        "msg": _("An error occurred. Please try to call again."),
                        "code": "call_failed"
                    }
                }
        try:
            gs = self.general_business
            bt = self.business_transfer
            pool = self.pool_conn
            user_token = data.get('user_token')
            environment = data.get('environment')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    from_user_id = res_token[1]
                    to_profile_id = data.get('to_user_id')
                    sql_user = 'select id, ios_device_token, droid_device_token from user_account where user_mngment_id = %s'
                    to_user = gs.sql_execute(pool, sql_user, para_values=[to_profile_id])
                    to_user_id = to_user[0][0]
                    ios_device_token = to_user[0][1]
                    droid_device_token = to_user[0][2]
                    appointment_id = data.get('appointment_id')
                    now = datetime.now()
                    create_date = now.strftime('%Y-%m-%d %H:%M:%S')

                    if not appointment_id:
                        scheduleTime = int(time.mktime(now.timetuple()))
                        if res_token[6] == "patient":
                            sql = '''select consultation_fee,
                                            consultation_duration,
                                            consultation_unit 
                                    from doctor_profile where user_mngment_id = %s '''
                            doctor_info = gs.sql_execute(pool, sql, para_values=(to_profile_id,))
                            doctor_fee = doctor_info[0][0]
                            consultation_duration = doctor_info[0][1]
                            consultation_unit = doctor_info[0][2]
                            sql = '''SELECT id FROM user_account WHERE user_mngment_id = %s'''
                            doctor_ua_id = gs.sql_execute(pool, sql, para_values=(to_profile_id,))[0][0]
                            sql = '''select id, 
                                            user_account_id, 
                                            available_balance 
                                    from user_wallet 
                                    where user_account_id in (%s,%s) '''
                            wallets = gs.sql_execute(pool, sql, para_values=(res_token[1], doctor_ua_id))
                            info_wallets = {}
                            for wallet in wallets:
                                if int(wallet[1]) == int(doctor_ua_id):
                                    info_wallets.update({'doctor': {'wallet_id': wallet[0],
                                                                    'available_balance': wallet[2]}})
                                else:
                                    info_wallets.update({'patient': {'wallet_id': wallet[0],
                                                                     'available_balance': wallet[2]}})
                            if 'patient' in info_wallets:
                                patient_balance = info_wallets['patient']['available_balance']
                                full_fee = doctor_fee + max(min_commission, doctor_fee * (com_ratio/100))
                                if patient_balance < full_fee:
                                    resp.update({'status_code': 401,
                                                 'resq_body':
                                                     {
                                                         "code": "not_enough_balance",
                                                         "title": _("Not enough balance"),
                                                         "msg": _("Your account balance is not enough for making the call")
                                                     }
                                                 })
                                    return resp
                                else:
                                    insert_apm = '''insert into appointment (created_date,
                                                                            write_date,
                                                                            schedule_date,
                                                                            state,
                                                                            patient_id,
                                                                            doctor_id,
                                                                            consultation_duration,
                                                                            consultation_fee,
                                                                            cost,
                                                                            unit_cost,
                                                                            type)
                                                    values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
                                                    RETURNING id'''
                                    appointment_id = \
                                    gs.sql_execute(pool, insert_apm, commit=True, para_values=(create_date,
                                                                                               create_date,
                                                                                               create_date,
                                                                                               'confirmed',
                                                                                               from_user_id,
                                                                                               to_user_id,
                                                                                               consultation_duration,
                                                                                               doctor_fee,
                                                                                               full_fee,
                                                                                               consultation_unit,
                                                                                               'live'))[0][0]
                                    # Save trans apm
                                    sql_insert_trans = '''INSERT INTO appointment_transaction(create_date, 
                                                                                              write_date, 
                                                                                              appointment_id, 
                                                                                              payment_trans_id, 
                                                                                              from_state, 
                                                                                              to_state, 
                                                                                              user_account_id,
                                                                                              note)
                                                        VALUES (%s, %s, %s, %s, %s, %s, %s, %s)
                                                        '''
                                    gs.sql_execute(pool, sql_insert_trans, commit=True, para_values=(create_date,
                                                                                                     create_date,
                                                                                                     appointment_id,
                                                                                                     0,
                                                                                                     'new',
                                                                                                     'confirmed',
                                                                                                     res_token[1],
                                                                                                     'Create APM - Tokbox'))

                                    # TODO discount when video call???
                                    bt.create_payment_appointment(appointment_id, doctor_fee,
                                                                  wallet_source_id=info_wallets['patient']['wallet_id'],
                                                                  wallet_dest_id=self.drdr_wallet_id, note=None,
                                                                  context=None)
                                    bt.confirm_appointment(appointment_id)
                            # TODO disable in production - don't allow doctor call direct
                            else:
                                sql = '''select consultation_fee,consultation_duration,consultation_unit 
                                        from doctor_profile where user_mngment_id = %s ''' % (res_token[7])
                                doctor_info = gs.sql_execute(pool, sql)
                                consultation_duration = doctor_info[0][1]
                                consultation_unit = doctor_info[0][2]
                                insert_apm = '''insert into appointment (created_date,
                                                                        write_date,
                                                                        schedule_date,
                                                                        state,
                                                                        doctor_id,
                                                                        patient_id,
                                                                        consultation_duration,
                                                                        consultation_fee,
                                                                        cost,
                                                                        unit_cost,
                                                                        type)
                                                values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
                                                RETURNING id'''
                                appointment_id = gs.sql_execute(pool, insert_apm, commit=True, para_values=(create_date,
                                                                                                            create_date,
                                                                                                            create_date,
                                                                                                            'confirmed',
                                                                                                            from_user_id,
                                                                                                            to_user_id,
                                                                                                            consultation_duration,
                                                                                                            doctor_fee,
                                                                                                            0,
                                                                                                            consultation_unit,
                                                                                                            'live'))[0][0]
                    else:
                        sql = '''select doctor_id, 
                                        patient_id,
                                        schedule_date,
                                        state
                                 from appointment
                                 where id = %s '''
                        apm_info = gs.sql_execute(pool, sql, para_values=[appointment_id])[0]
                        scheduleTime = int(time.mktime(apm_info[2].timetuple()))
                        currentTime = datetime.now().strftime('%Y-%m-%d %H:%M')
                        sql_update = '''UPDATE appointment SET state= 'confirmed',write_date=%s WHERE id = %s '''
                        gs.sql_execute(pool, sql_update, commit=True, para_values=(currentTime,appointment_id,))
                    # Get tokbox info
                    import ssl
                    client = paho.Client()
                    if self.authen_ssl == True:
                        client = paho.Client(transport='websockets')
                        client.tls_set('tokbox/ca_file.crt', cert_reqs=ssl.CERT_NONE)
                        client.tls_insecure_set(True)
                    client.connect(self.mqtt_host, self.mqtt_port, 60)
                    client.loop_start()
                    action_time = int(time.mktime(now.timetuple()))
                    session = self.opentok.create_session()
                    session_id = session.session_id
                    from_user_token = self.opentok.generate_token(session_id)
                    to_user_token = self.opentok.generate_token(session_id)
                    insert_call_his = '''insert into appointment_call_history (appointment_id,
                                                                            create_date,
                                                                            write_date,
                                                                            session_id,
                                                                            token_to,
                                                                            token_from,
                                                                            state)
                                                values (%s,%s,%s,%s,%s,%s, 'processing')
                                                RETURNING id'''
                    call_history_id = gs.sql_execute(pool, insert_call_his, commit=True, para_values=(appointment_id,
                                                                                                      create_date,
                                                                                                      create_date,
                                                                                                      session_id,
                                                                                                      to_user_token,
                                                                                                      from_user_token))[0][0]
                    mqtt_body = {
                        "from": {
                            "action": "incoming_call",
                            "action_time": action_time,
                            "tokbox_api_key": str(self.api_key),
                            "tokbox_session_id": str(session_id),
                            "tokbox_token": str(from_user_token),
                            "user_id": int(from_user_id),
                            "appointment_id": int(appointment_id),
                            "call_history_id": int(call_history_id),
                            "scheduleTime": scheduleTime,
                        },
                        "to": {
                            "action": "incoming_call",
                            "action_time": action_time,
                            "tokbox_api_key": str(self.api_key),
                            "tokbox_session_id": str(session_id),
                            "tokbox_token": str(to_user_token),
                            "user_id": int(to_user_id),
                            "appointment_id": int(appointment_id),
                            "call_history_id": int(call_history_id),
                            "scheduleTime": scheduleTime,
                        }
                    }
                    if res_token[6] == "patient":

                        sql = """select ua.city
                                    from user_contact uc
                                        left join user_address ua on ua.user_contact_id = uc.id
                                    where uc.user_mngment_id = %s AND ua.is_primary = True """
                        records = gs.sql_execute(pool, sql, para_values=(res_token[7],))

                        mqtt_body["to"].update({
                            "patient_firstname": res_token[8],
                            "patient_middlename": res_token[9],
                            "patient_lastname": res_token[10],
                            "patient_avatar_url": res_token[11],
                            "patient_genderCode": res_token[12],
                            "patient_id": res_token[7]
                        })

                        if res_token[13]:
                            age = datetime.now().date().year - res_token[13].timetuple().tm_year
                            mqtt_body["to"].update({
                                "patient_age": age
                            })
                        else:
                            mqtt_body["to"].update({"patient_age": None})

                        if records:
                            mqtt_body["to"].update({"patient_primaryAddress_cityCode": records[0][0]})
                        else:
                            mqtt_body["to"].update({"patient_primaryAddress_cityCode": None})

                    elif res_token[6] == "doctor":
                        specialty_codes = []
                        sql = """select drspc.specialties_code
                                    from doctor_profile dp
                                        left join dr_doctorprofile_specialties drspc on dp.id = drspc.doctor_profile_id
                                    where dp.user_mngment_id = %s """

                        records = gs.sql_execute(pool, sql, para_values=(res_token[7],))

                        for line in records:
                            specialty_codes.append({"code": line[0]})

                        mqtt_body["to"].update({
                            "doctor_firstname": res_token[8],
                            "doctor_middlename": res_token[9],
                            "doctor_lastname": res_token[10],
                            "doctor_avatar_url": res_token[11],
                            "doctor_specialty_code": specialty_codes,
                            "doctor_title": res_token[14],
                            "doctor_id": res_token[7]
                        })

                    #client.publish("%s"%from_user_id, str(mqtt_body["from"]), qos=1)
                    if ios_device_token and ios_device_token not in ('','False','NULL'):
                        ios_apns = json.loads(config.get('environment', 'ios_apns'))
                        is_sandbox = ios_apns["sandbox"]
                        # if ios_apns == "development":
                        #     is_sandbox = True
                        from apns import APNs, Payload
                        apns = APNs(use_sandbox=is_sandbox, cert_file="tokbox/VOIP.pem", enhanced=True)
                        mqtt_body["to"].update({ 
                            "alert": _("You have a video call from Doctor Doctor")
                        })
                        payload = Payload(alert=json.dumps(mqtt_body['to']))
                        apns.gateway_server.send_notification(ios_device_token, payload)
                    
                    if droid_device_token and droid_device_token not in ('','False','NULL'):
                        fcm_key = config.get('environment', 'fcm_key')
                        from pyfcm import FCMNotification
                        push_service = FCMNotification(api_key=fcm_key)
                        result = push_service.single_device_data_message(registration_id=droid_device_token, data_message=mqtt_body['to'])

                    client.publish("%s" % to_user_id, json.dumps(mqtt_body['to']), qos=2)
                    client.loop_stop()
                    client.disconnect()
                    resp.update({
                        'status_code': 200,
                        'resq_body': {
                            "tokbox_api_key": str(self.api_key),
                            "tokbox_token": str(from_user_token),
                            "tokbox_session_id": str(session_id),
                            "appointment_id": int(appointment_id),
                            "call_history_id": int(call_history_id)
                        }
                    })
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }

        except Exception as e:
            logger.error('error_tokbox_call %s' % str(e))
            client_sentry.captureException()

        return resp

    def tokbox_call_v2(self, data):
        logger.info('tokbox_call_v2 %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Call failed"),
                        "msg": _("An error occurred. Please try to call again."),
                        "code": "call_failed"
                    }
                }
        try:
            gs = self.general_business
            # bt = self.business_transfer
            pool = self.pool_conn
            user_token = data.get('user_token')
            environment = data.get('environment')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    from_user_id = res_token[1]
                    call_history_id = data.get('call_history_id')
                    sql = '''select session_id, --0
                                    token_to, --1
                                    appointment_id,--2
                                    schedule_date,--3
                                    p.id patient_user_id,--4
                                    p.user_mngment_id,--5
                                    coalesce(p.ios_device_token,''),--6
                                    d.id doctor_user_id,--7
                                    d.user_mngment_id,--8
                                    coalesce(p.droid_device_token,''), --9
                                    coalesce(d.ios_device_token,''),-- 10
                                    coalesce(d.droid_device_token,'') -- 11
                            from appointment_call_history h
                            left join appointment a on h.appointment_id = a.id
                            left join user_account p on p.id = a.patient_id
                            left join user_account d on d.id = a.doctor_id
                            where h.id = %s'''
                    payload_info = gs.sql_execute(pool, sql, para_values=[call_history_id])[0]
                    session_id = payload_info[0]
                    to_user_token = payload_info[1]
                    appointment_id = payload_info[2]
                    scheduleTime = int(time.mktime(payload_info[3].timetuple()))
                    ios_device_token = payload_info[6]
                    droid_device_token = payload_info[9]
                    doctor_ios_device_token = payload_info[10]
                    doctor_droid_device_token = payload_info[11]
                    now = datetime.now()
                    action_time = int(time.mktime(now.timetuple()))
                    # create_date = now.strftime('%Y-%m-%d %H:%M:%S')
                    
                    select_call_logs = '''select id,status_call from call_record_logs where patient_id = %s and doctor_id = %s order by id desc ''' % (from_user_id,payload_info[7])
                    select_call_logs_result = gs.sql_execute(pool, select_call_logs)
                    if  select_call_logs_result:
                        update_call_logs = ''' update call_record_logs set status_call = 'calling', last_update = current_timestamp where id = %s ''' % (select_call_logs_result[0][0])
                        gs.sql_execute(pool, update_call_logs, commit=True)
                    mqtt_body = {
                        "to": {
                            "action": "incoming_call",
                            "action_time": action_time,
                            "tokbox_api_key": str(self.api_key),
                            "tokbox_session_id": str(session_id),
                            "tokbox_token": str(to_user_token),
                            "appointment_id": int(appointment_id),
                            "call_history_id": int(call_history_id),
                            "scheduleTime": scheduleTime,
                        }
                    }

                    import ssl
                    client = paho.Client()
                    if self.authen_ssl == True:
                        client = paho.Client(transport='websockets')
                        client.tls_set('tokbox/ca_file.crt', cert_reqs=ssl.CERT_NONE)
                        client.tls_insecure_set(True)
                    client.connect(self.mqtt_host, self.mqtt_port, 60)
                    client.loop_start()
                    if res_token[6] == "patient":
                        to_user_id = payload_info[7]
                        sql = """select ua.city
                                    from user_contact uc
                                        left join user_address ua on ua.user_contact_id = uc.id
                                    where uc.user_mngment_id = %s AND ua.is_primary = True """
                        records = gs.sql_execute(pool, sql, para_values=(res_token[7],))

                        mqtt_body["to"].update({
                            "user_id": int(to_user_id),
                            "patient_firstname": res_token[8],
                            "patient_middlename": res_token[9],
                            "patient_lastname": res_token[10],
                            "patient_avatar_url": res_token[11],
                            "patient_genderCode": res_token[12],
                            "patient_id": res_token[7]
                        })

                        if res_token[13]:
                            age = datetime.now().date().year - res_token[13].timetuple().tm_year
                            mqtt_body["to"].update({
                                "patient_age": age
                            })
                        else:
                            mqtt_body["to"].update({"patient_age": None})

                        if records:
                            mqtt_body["to"].update({"patient_primaryAddress_cityCode": records[0][0]})
                        else:
                            mqtt_body["to"].update({"patient_primaryAddress_cityCode": None})

                    elif res_token[6] == "doctor":
                        to_user_id = payload_info[4]
                        specialty_codes = []
                        sql = """select drspc.specialties_code
                                    from doctor_profile dp
                                        left join dr_doctorprofile_specialties drspc on dp.id = drspc.doctor_profile_id
                                    where dp.user_mngment_id = %s """

                        records = gs.sql_execute(pool, sql, para_values=(res_token[7],))

                        for line in records:
                            specialty_codes.append({"code": line[0]})

                        mqtt_body["to"].update({
                            "user_id": int(to_user_id),
                            "doctor_firstname": res_token[8],
                            "doctor_middlename": res_token[9],
                            "doctor_lastname": res_token[10],
                            "doctor_avatar_url": res_token[11],
                            "doctor_specialty_code": specialty_codes,
                            "doctor_title": res_token[14],
                            "doctor_id": res_token[7]
                        })

                    #client.publish("%s"%from_user_id, str(mqtt_body["from"]), qos=1)
                    
                    if res_token[6] == 'doctor':
                        if ios_device_token and ios_device_token not in ('','False','NULL'):
                            ios_apns = json.loads(config.get('environment', 'ios_apns'))
                            is_sandbox = ios_apns["sandbox"]
                            # if ios_apns == "development":
                            #     is_sandbox = True
                            from apns import APNs, Payload
                            apns = APNs(use_sandbox=is_sandbox, cert_file="tokbox/VOIP.pem", enhanced=True)
                            mqtt_body["to"].update({ 
                                "alert": _("You have a call from Doctor Doctor")
                            })
                            payload = Payload(alert=json.dumps(mqtt_body['to']))
                            apns.gateway_server.send_notification(ios_device_token, payload)
                        print 'droid_device_token ' ,droid_device_token
                        if droid_device_token and droid_device_token not in ('','False','NULL'):
                            fcm_key = config.get('environment', 'fcm_key')
                            from pyfcm import FCMNotification
                            push_service = FCMNotification(api_key=fcm_key)
                            result = push_service.single_device_data_message(registration_id=droid_device_token, data_message=mqtt_body['to'])
                            print 'result ',result

                    if res_token[6] == 'patient':
                        if doctor_ios_device_token and doctor_ios_device_token not in ('','False','NULL'):
                            ios_apns = json.loads(config.get('environment', 'ios_apns'))
                            is_sandbox = ios_apns["sandbox"]
                            
                            from apns import APNs, Payload
                            apns = APNs(use_sandbox=is_sandbox, cert_file="DOCTORAPP.pem", enhanced=True)
                            mqtt_body["to"].update({ 
                                "alert": _("You have a call from Hello Doctor")
                            })
                            payload = Payload(alert=json.dumps(mqtt_body['to']))
                            apns.gateway_server.send_notification(doctor_ios_device_token, payload)
                        
                        if doctor_droid_device_token and doctor_droid_device_token not in ('','False','NULL'):
                            fcm_key = config.get('environment', 'fcm_key')
                            from pyfcm import FCMNotification
                            push_service = FCMNotification(api_key=fcm_key)
                            result = push_service.single_device_data_message(registration_id=doctor_droid_device_token, data_message=mqtt_body['to'])
                            

                    client.publish("%s" % to_user_id, json.dumps(mqtt_body['to']), qos=2)
                    client.loop_stop()
                    client.disconnect()
                    resp.update({
                        'status_code': 200,
                        'resq_body': {
                            "code": "succeed",
                            "title": _("Videocall succeed"),
                            "msg": _("Videocall  succeed")

                        }
                    })
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }

        except Exception as e:
            logger.error('error_tokbox_call_v2 %s' % str(e))
            client_sentry.captureException()

        return resp

    def tokbox_deregister(self, data):
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Videocall deregister failed"),
                        "msg": _("An error occurred. Please try to deregister videocall again."),
                        "code": "videocall_deregister_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    if res_token[5] != True:
                        resp.update({'status_code': 401,
                                     'resq_body': {
                                         "code": "not_register",
                                         "title": _("Not Register"),
                                         "msg": _("You have not registered yet!")
                                     }
                                     })
                    else:
                        user_id = res_token[1]
                        sql_update = '''update user_account set call_register = False where id = %s'''
                        gs.sql_execute(pool, sql_update, commit=True, para_values=[user_id])
                        resp.update({'status_code': 200,
                                     'resq_body': {
                                         "code": "succeed",
                                         "title": _("Videocall deregister succeed"),
                                         "msg": _("Videocall deregister succeed")
                                     }
                                     })
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_tokbox_deregister %s' % str(e))
            client_sentry.captureException()
        return resp

    def tokbox_register(self, data):
        print data
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Videocall register failed"),
                        "msg": _("An error occurred. Please try to register videocall again."),
                        "code": "videocall_register_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            iOSDeviceToken = data.get('iOSDeviceToken','')
            androidDeviceToken = data.get('AndroidDeviceToken','')
            user_device = data.get('client_type')
            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    if user_device == "Android":
                        sql_update = '''update user_account set call_register = True,
                                                droid_device_token = %s where id = %s'''
                        gs.sql_execute(pool, sql_update, commit=True, para_values=(androidDeviceToken, user_id))
                        resp.update({'status_code': 200,
                                    'resq_body': {
                                        "code": "succeed",
                                        "title": _("Videocall register succeed"),
                                        "msg": _("Videocall register succeed")
                                    }
                                    })
                    elif user_device == "iOS":
                        sql_update = '''update user_account set call_register = True,
                                                ios_device_token = %s where id = %s'''
                        gs.sql_execute(pool, sql_update, commit=True, para_values=(iOSDeviceToken, user_id))
                        resp.update({'status_code': 200,
                                    'resq_body': {
                                        "code": "succeed",
                                        "title": _("Videocall register succeed"),
                                        "msg": _("Videocall register succeed")
                                    }
                                    })
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_tokbox_register %s' % str(e))
            client_sentry.captureException()

        return resp

    def tokbox_end(self, data):
        logger.info('tokbox_end %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Videocall end failed"),
                        "msg": _("An error occurred. Please try to end videocall again."),
                        "code": "videocall_end_failed"
                    }
                }
        gs = self.general_business
        pool = self.pool_conn
        try:
            user_token = data.get('user_token')
            tokbox_token = data.get('tokbox_token')
            tokbox_session_id = data.get('tokbox_session_id')
            appointment_id = data.get('appointment_id')
            call_history_id = data.get('call_history_id')
            now = datetime.now()
            create_date = now.strftime('%Y-%m-%d %H:%M:%S')
            action_time = int(time.mktime(now.timetuple()))

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    end_user_id = res_token[1]
                    sql_apm = '''select doctor_id, patient_id, ua.user_mngment_id,schedule_date,COALESCE(ios_device_token,''),COALESCE(droid_device_token,'')
                                from appointment apm
                                left join user_account ua on ua.id = apm.patient_id
                                where apm.id = %s'''
                    appointment = gs.sql_execute(pool, sql_apm, para_values=[appointment_id])

                    if appointment:
                        doctor_id = appointment[0][0]
                        #sql = '''update user_account set activity_status = 'online' where id = %s'''%doctor_id
                        #record = gs.sql_execute(pool, sql,commit=True)
                        ios_device_token = appointment[0][4]
                        droid_device_token = appointment[0][5]
                        be_end_user_id = appointment[0][0] if appointment[0][1] == end_user_id else appointment[0][1]
                        scheduleTime = int(time.mktime(appointment[0][3].timetuple()))
                        sql_call_his = 'select token_from, token_to, session_id from appointment_call_history where id = %s '
                        call_his = gs.sql_execute(pool, sql_call_his, para_values=[call_history_id])
                        if call_his:
                            if tokbox_token in (call_his[0][0], call_his[0][1]):
                                if tokbox_session_id == call_his[0][2]:
                                    import ssl
                                    client = paho.Client()
                                    if self.authen_ssl == True:
                                        client = paho.Client(transport='websockets')
                                        client.tls_set('tokbox/ca_file.crt', cert_reqs=ssl.CERT_NONE)
                                        client.tls_insecure_set(True)
                                    client.connect(self.mqtt_host, self.mqtt_port, 60)
                                    client.loop_start()
                                    be_end_token = call_his[0][0] if call_his[0][1] == tokbox_token else call_his[0][1]

                                    mqtt_body = {
                                        "end_user": {
                                            "action": "end_call",
                                            "action_time": action_time,
                                            "tokbox_api_key": str(self.api_key),
                                            "tokbox_session_id": str(tokbox_session_id),
                                            "tokbox_token": str(tokbox_token),
                                            "user_id": int(end_user_id),
                                            "appointment_id": int(appointment_id),
                                            "call_history_id": int(call_history_id),
                                            "scheduleTime": scheduleTime,
                                        },
                                        "be_end_user": {
                                            "action": "end_call",
                                            "action_time": action_time,
                                            "tokbox_api_key": str(self.api_key),
                                            "tokbox_session_id": str(tokbox_session_id),
                                            "tokbox_token": str(be_end_token),
                                            "user_id": int(be_end_user_id),
                                            "appointment_id": int(appointment_id),
                                            "call_history_id": int(call_history_id),
                                            "scheduleTime": scheduleTime,
                                        }
                                    }
                                    if res_token[6] != "patient":
                                        mqtt_body["be_end_user"].update({'patient_id': appointment[0][2]})
                                    
                                        if ios_device_token not in ('','False','NULL'):
                                            ios_apns = json.loads(config.get('environment', 'ios_apns'))
                                            is_sandbox = ios_apns["sandbox"]
                                            from apns import APNs, Payload
                                            apns = APNs(use_sandbox=is_sandbox, cert_file="tokbox/VOIP.pem", enhanced=True)
                                            payload = Payload(alert=json.dumps(mqtt_body["be_end_user"]))
                                            apns.gateway_server.send_notification(appointment[0][4], payload)

                                        if droid_device_token and droid_device_token not in ('','False','NULL'):
                                            fcm_key = config.get('environment', 'fcm_key')
                                            from pyfcm import FCMNotification
                                            push_service = FCMNotification(api_key=fcm_key)
                                            result = push_service.single_device_data_message(registration_id=droid_device_token, data_message=mqtt_body['be_end_user'])
                                    logger.info('tokbox_end %s', be_end_user_id)
                                    logger.info('tokbox_end %s', json.dumps(mqtt_body["be_end_user"]))
        
                                    client.publish("%s" % be_end_user_id, json.dumps(mqtt_body["be_end_user"]), qos=2)
                                    client.loop_stop()
                                    client.disconnect()
                                    sql_update_history = '''update appointment_call_history 
                                                            set end_time = now(), 
                                                                write_date = now(),
                                                                state = 'successful_finished'
                                                            where id = %s'''
                                    gs.sql_execute(pool, sql_update_history, commit=True,
                                                   para_values=[call_history_id])
                                    
                                    resp.update({'status_code': 200,
                                                 'resq_body': {
                                                     "code": "succeed",
                                                     "title": _("Videocall end succeed"),
                                                     "msg": _("Videocall end succeed")
                                                 }
                                                 })
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_tokbox_end %s' % str(e))
            client_sentry.captureException()
        return resp

    
    def tokbox_end_v3(self, data):
        logger.info('tokbox_end_v3 %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Videocall end failed"),
                        "msg": _("An error occurred. Please try to end videocall again."),
                        "code": "videocall_end_failed"
                    }
                }
        gs = self.general_business
        pool = self.pool_conn
        try:
            user_token = data.get('user_token')
            tokbox_token = data.get('tokbox_token')
            tokbox_session_id = data.get('tokbox_session_id')
            session_id = data.get('tokbox_session_id')
            appointment_id = data.get('appointment_id')
            call_history_id = data.get('call_history_id')
            now = datetime.now()
            create_date = now.strftime('%Y-%m-%d %H:%M:%S')
            action_time = int(time.mktime(now.timetuple()))

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    end_user_id = res_token[1]
                    if res_token[6] == 'patient':
                        sql_apm = '''select doctor_id, patient_id, ua.user_mngment_id,schedule_date,COALESCE(ios_device_token,''),COALESCE(droid_device_token,'')
                                from appointment apm
                                inner join user_account ua on ua.id = apm.doctor_id
                                where apm.id = %s'''
                        appointment = gs.sql_execute(pool, sql_apm, para_values=[appointment_id])
                    if res_token[6] == 'doctor':
                        sql_apm = '''select doctor_id, patient_id, ua.user_mngment_id,schedule_date,COALESCE(ios_device_token,''),COALESCE(droid_device_token,'')
                                from appointment apm
                                inner join user_account ua on ua.id = apm.patient_id
                                where apm.id = %s'''
                        appointment = gs.sql_execute(pool, sql_apm, para_values=[appointment_id])

                    if appointment:
                        doctor_id = appointment[0][0]
                        patient_id = appointment[0][1]
                        #sql = '''update user_account set activity_status = 'online' where id = %s'''%doctor_id
                        #record = gs.sql_execute(pool, sql,commit=True)
                        
                        #sql = '''update user_account set activity_status = 'online' where id = %s'''%patient_id
                        #record = gs.sql_execute(pool, sql,commit=True)

                        
                        logger.info('tokbox_end appointment %s' , appointment)
                        logger.info('tokbox_end doctor id %s' , doctor_id)
                        logger.info('tokbox_end patient id %s' , patient_id)
                        logger.info('tokbox_end user %s' , res_token[6])
                        ios_device_token = appointment[0][4]
                        droid_device_token = appointment[0][5]
                        be_end_user_id = appointment[0][0] if appointment[0][1] == end_user_id else appointment[0][1]
                        scheduleTime = int(time.mktime(appointment[0][3].timetuple()))
                        #scheduleTime = long(time.time())
                        #action_time = long(time.time())
                        sql_call_his = 'select token_from, token_to, session_id from appointment_call_history where id = %s '
                        call_his = gs.sql_execute(pool, sql_call_his, para_values=[call_history_id])
                        if call_his:
                            if tokbox_token:
                                if tokbox_session_id:
                                    be_end_token = call_his[0][0] if call_his[0][1] == tokbox_token else call_his[0][1]
                                    mqtt_body = {
                                        "end_user": {
                                            "action": "end_call",
                                            "action_time": action_time,
                                            "tokbox_api_key": str(self.api_key),
                                            "tokbox_session_id": str(tokbox_session_id),
                                            "tokbox_token": str(tokbox_token),
                                            "user_id": int(end_user_id),
                                            "appointment_id": int(appointment_id),
                                            "call_history_id": int(call_history_id),
                                            "scheduleTime": scheduleTime
                                        },
                                        "be_end_user": {
                                            "action": "end_call",
                                            "action_time": action_time,
                                            "tokbox_api_key": str(self.api_key),
                                            "tokbox_session_id": str(tokbox_session_id),
                                            "tokbox_token": str(be_end_token),
                                            "user_id": int(be_end_user_id),
                                            "appointment_id": int(appointment_id),
                                            "call_history_id": int(call_history_id),
                                            "scheduleTime": scheduleTime
                                        }
                                    }
                                    #encoded = jwt.encode({
                                    #    "iss": "45882532",
                                    #    "ist": "project",
                                    #    "iat": int(time.time()),
                                    #    "exp": int(time.time())+300
                                    #    }, 'ea48282fe2a0c7580294b8a65f41a15ca38904ac', algorithm='HS256')
                                    #url = 'https://api.opentok.com/v2/project/45882532/session/'+ session_id +'/signal/'
                                    #headers = {'X-OPENTOK-AUTH': encoded, 'Accept': 'application/json'}
                                    #tokbox_data = {
                                    #    "type" : "end_call",
                                    #    "data": "end_call_data"
                                    #    }
                                    #r = requests.post(url,data=json.dumps(tokbox_data),headers=headers)


                                    logger.info('tokbox_end ios_device %s' , ios_device_token)

                                    logger.info('tokbox_end droid_device %s' , droid_device_token)
                                    sql_user = '''select user_mngment_id from user_account where id = %s'''
                                    res_user_sql = gs.sql_execute(pool, sql_user, para_values=[appointment[0][1]])

                                    if res_user_sql:
                                        mqtt_body["be_end_user"].update({'patient_id': res_user_sql[0][0]})
                                    if res_token[6] == "doctor" or  res_token[6] == "hotline":
                                        if ios_device_token not in ('','False','NULL', None):
                                            ios_apns = json.loads(config.get('environment', 'ios_apns'))
                                            is_sandbox = ios_apns["sandbox"]
                                            from apns import APNs, Payload
                                            apns = APNs(use_sandbox=is_sandbox, cert_file="tokbox/VOIP.pem", enhanced=True)
                                            payload = Payload(alert=json.dumps(mqtt_body["be_end_user"]))
                                            apns.gateway_server.send_notification(ios_device_token, payload)

                                        if droid_device_token and droid_device_token not in ('','False','NULL', None):
                                            fcm_key = config.get('environment', 'fcm_key')
                                            from pyfcm import FCMNotification
                                            push_service = FCMNotification(api_key=fcm_key)
                                            result = push_service.single_device_data_message(registration_id=droid_device_token, data_message=mqtt_body['be_end_user'])
                                    
                                    if res_token[6] == "patient":
                                        
                                        if ios_device_token not in ('','False','NULL', None):
                                            ios_apns = json.loads(config.get('environment', 'ios_apns'))
                                            is_sandbox = ios_apns["sandbox"]
                                            from apns import APNs, Payload
                                            apns = APNs(use_sandbox=is_sandbox, cert_file="DOCTORAPP.pem", enhanced=True)
                                            payload = Payload(alert=json.dumps(mqtt_body["be_end_user"]))
                                            apns.gateway_server.send_notification(ios_device_token, payload)

                                        if droid_device_token and droid_device_token not in ('','False','NULL', None):
                                            fcm_key = config.get('environment', 'fcm_key')
                                            from pyfcm import FCMNotification
                                            push_service = FCMNotification(api_key=fcm_key)
                                            result = push_service.single_device_data_message(registration_id=droid_device_token, data_message=mqtt_body['be_end_user'])
                                    #responseTokbox = r.json()
                                    #logger.info('tokbox_end_v3 response tokbox %s', responseTokbox)
                                    logger.info('tokbox_end_v3 %s', be_end_user_id)
                                    logger.info('tokbox_end_v3 %s', json.dumps(mqtt_body["be_end_user"]))
                                    import ssl
                                    client = paho.Client()
                                    if self.authen_ssl == True:
                                        client = paho.Client(transport='websockets')
                                        client.tls_set('tokbox/ca_file.crt', cert_reqs=ssl.CERT_NONE)
                                        client.tls_insecure_set(True)
                                    client.connect(self.mqtt_host, self.mqtt_port, 60)
                                    client.loop_start()
                                    client.publish("%s" % be_end_user_id, json.dumps(mqtt_body['be_end_user']), qos=2)
                                    client.loop_stop()
                                    client.disconnect()
                                    sql_update_history = '''update appointment_call_history 
                                                            set end_time = now(), 
                                                                write_date = now(),
                                                                state = 'successful_finished'
                                                            where id = %s'''
                                    gs.sql_execute(pool, sql_update_history, commit=True,
                                                   para_values=[call_history_id])
                                    
                                    resp.update({'status_code': 200,
                                                 'resq_body': {
                                                     "code": "succeed",
                                                     "title": _("Videocall end v3 succeed"),
                                                     "msg": _("Videocall end v3 succeed")
                                                 }
                                                 })
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_tokbox_end_v3 %s' % str(e))
            client_sentry.captureException()
        return resp


    def tokbox_deny(self, data):
        logger.info('tokbox_deny %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Videocall deny failed"),
                        "msg": _("An error occurred. Please try to deny videocall again."),
                        "code": "videocall_deny_failed"
                    }
                }
        gs = self.general_business
        pool = self.pool_conn
        try:
            user_token = data.get('user_token')
            tokbox_token = data.get('tokbox_token')
            tokbox_session_id = data.get('tokbox_session_id')
            appointment_id = data.get('appointment_id')
            call_history_id = data.get('call_history_id')
            now = datetime.now()
            action_time = int(time.mktime(now.timetuple()))

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    deny_user_id = res_token[1]
                    sql_call_his = '''select token_from, token_to, session_id, state  
                                    from appointment_call_history where id = %s '''
                    call_his = gs.sql_execute(pool, sql_call_his, commit=False, para_values=[call_history_id])
                    if call_his and call_his[0][3] in ['denied','accepted']:
                        resp = {'status_code': 401,
                                'resq_body':
                                    {
                                        "title": _("Videocall deny failed"),
                                        "msg": _("Duplicate deny/accept call"),
                                        "code": "videocall_deny_accept_failed"
                                    }
                                }
                        return resp

                    sql_update_history = '''update appointment_call_history set write_date = now(), state = 'denied'
                                            where id = %s'''
                    gs.sql_execute(pool, sql_update_history, commit=True,para_values=[call_history_id])

                    sql_apm = 'select doctor_id, patient_id, schedule_date from appointment where id = %s'
                    appointment = gs.sql_execute(pool, sql_apm, commit=False, para_values=[appointment_id])
                    #update_doc = '''update user_account set activity_status = 'online' where id = %s '''% (appointment[0][0])
                    #gs.sql_execute(pool, update_doc, commit=True)
                    if appointment:
                        select_call_logs = '''select id,status_call from call_record_logs where patient_id = %s and doctor_id = %s order by id desc ''' % (appointment[0][1],appointment[0][0])
                        select_call_logs_result = gs.sql_execute(pool, select_call_logs)
                        if  select_call_logs_result:
                            update_call_logs = ''' update call_record_logs set status_call = 'denied', last_update = current_timestamp where id = %s ''' % (select_call_logs_result[0][0])
                            gs.sql_execute(pool, update_call_logs, commit=True)
                    
                        #sql_update_doctor = '''update user_account set activity_status = 'online'
                        #                    where id = %s''' % (appointment[0][0])
                        #gs.sql_execute(pool,sql_update_doctor, commit=True)

                        be_deny_user_id = appointment[0][0] if appointment[0][1] == deny_user_id else appointment[0][1]
                        scheduleTime = int(time.mktime(appointment[0][2].timetuple()))
                        
                        if call_his:
                            logger.info('tokbox_deny_no_call_his %s', data)
                            if tokbox_token in (call_his[0][0], call_his[0][1]):
                                logger.info('tokbox_deny_wrong_tokbox_token %s', data)
                                if tokbox_session_id == call_his[0][2]:
                                    logger.info('tokbox_deny_wrong_tokbox_session_id %s', data)
                                    import ssl
                                    client = paho.Client()
                                    if self.authen_ssl == True:
                                        client = paho.Client(transport='websockets')
                                        client.tls_set('tokbox/ca_file.crt', cert_reqs=ssl.CERT_NONE)
                                        client.tls_insecure_set(True)
                                    client.connect(self.mqtt_host, self.mqtt_port, 60)
                                    client.loop_start()
                                    be_deny_token = call_his[0][0] if call_his[0][1] == tokbox_token else call_his[0][1]

                                    mqtt_body = {
                                        "deny_user": {
                                            "action": "deny_call",
                                            "action_time": action_time,
                                            "tokbox_api_key": str(self.api_key),
                                            "tokbox_session_id": str(tokbox_session_id),
                                            "tokbox_token": str(tokbox_token),
                                            "user_id": int(deny_user_id),
                                            "appointment_id": int(appointment_id),
                                            "call_history_id": int(call_history_id),
                                            "scheduleTime": scheduleTime,
                                        },
                                        "be_deny_user": {
                                            "action": "deny_call",
                                            "action_time": action_time,
                                            "tokbox_api_key": str(self.api_key),
                                            "tokbox_session_id": str(tokbox_session_id),
                                            "tokbox_token": str(be_deny_token),
                                            "user_id": int(be_deny_user_id),
                                            "appointment_id": int(appointment_id),
                                            "call_history_id": int(call_history_id),
                                            "scheduleTime": scheduleTime,
                                        }
                                    }

                                    client.publish("%s" % be_deny_user_id, json.dumps(mqtt_body["be_deny_user"]), qos=2)
                                    client.loop_stop()
                                    client.disconnect()

                                    resp.update({'status_code': 200,
                                                 'resq_body': {
                                                     "code": "succeed",
                                                     "title": _("Videocall deny succeed"),
                                                     "msg": _("Videocall deny succeed")
                                                 }
                                                 })
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_tokbox_deny %s' % str(e))
            client_sentry.captureException()
        return resp

    def tokbox_deny_v3(self, data):
        logger.info('tokbox_deny_v3 %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Videocall deny failed"),
                        "msg": _("An error occurred. Please try to deny videocall again."),
                        "code": "videocall_deny_failed"
                    }
                }
        gs = self.general_business
        pool = self.pool_conn
        try:
            user_token = data.get('user_token')
            tokbox_token = data.get('tokbox_token')
            tokbox_session_id = data.get('tokbox_session_id')
            appointment_id = data.get('appointment_id',None)
            call_history_id = data.get('call_history_id')
            now = datetime.now()
            action_time = long(time.time())

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    deny_user_id = res_token[1]
                    sql_call_his = '''select token_from, token_to, session_id, state  
                                    from appointment_call_history where id = %s '''
                    call_his = gs.sql_execute(pool, sql_call_his, commit=False, para_values=[call_history_id])
                    if call_his and call_his[0][3] in ['denied','accepted']:
                        resp = {'status_code': 401,
                                'resq_body':
                                    {
                                        "title": _("Videocall deny failed"),
                                        "msg": _("Duplicate deny/accept call"),
                                        "code": "videocall_deny_accept_failed"
                                    }
                                }
                        return resp

                    sql_update_history = '''update appointment_call_history set write_date = now(), state = 'denied'
                                            where id = %s'''
                    gs.sql_execute(pool, sql_update_history, commit=True,para_values=[call_history_id])

                    sql_apm = 'select doctor_id, patient_id, schedule_date, id from appointment where id = %s'
                    appointment = gs.sql_execute(pool, sql_apm, commit=False, para_values=[appointment_id])
                    ios_device_token = ''
                    droid_device_token = ''
                    if appointment:
                        
                        logger.info('tokbox_deny_v3 %s', appointment)
                        logger.info('tokbox_deny_v3 %s', appointment[0][0])
                        logger.info('tokbox_deny_v3 %s', res_token[6])
                        if res_token[6] == 'patient' :
                            sql_token = 'select droid_device_token, ios_device_token from user_account where id = %s'
                            res_sql_token = gs.sql_execute(pool, sql_token, commit=False, para_values=[appointment[0][0]])
                        if res_token[6] == 'doctor' :
                            sql_token = 'select droid_device_token, ios_device_token from user_account where id = %s'
                            res_sql_token = gs.sql_execute(pool, sql_token, commit=False, para_values=[appointment[0][1]])
                        if res_sql_token :
                            droid_device_token = res_sql_token[0][0]
                            ios_device_token = res_sql_token[0][1]
                        logger.info('tokbox_deny_v3 %s', sql_token)
                        logger.info('tokbox_deny_v3 %s', res_sql_token)
                        if res_sql_token :
                            droid_device_token = res_sql_token[0][0]
                            ios_device_token = res_sql_token[0][1]
                        logger.info('tokbox_deny_v3 %s', res_sql_token)
                        logger.info('tokbox_deny_v3 %s', ios_device_token)
                        logger.info('tokbox_deny_v3 %s', droid_device_token)
                        select_call_logs = '''select id,status_call from call_record_logs where patient_id = %s and doctor_id = %s order by id desc ''' % (appointment[0][1],appointment[0][0])
                        select_call_logs_result = gs.sql_execute(pool, select_call_logs)
                        if  select_call_logs_result:
                            update_call_logs = ''' update call_record_logs set status_call = 'denied', last_update = current_timestamp where id = %s ''' % (select_call_logs_result[0][0])
                            gs.sql_execute(pool, update_call_logs, commit=True)
                    
                        #sql_update_doctor = '''update user_account set activity_status = 'online'
                        #                    where id = %s''' % (appointment[0][0])
                        #gs.sql_execute(pool,sql_update_doctor, commit=True)

                        be_deny_user_id = appointment[0][0] if appointment[0][1] == deny_user_id else appointment[0][1]
                        test = appointment[0][2]
                        action_time = long(time.time())
                        scheduleTime = long(time.time())
                        
                        logger.info('tokbox_deny_schedule %s', action_time)
                        logger.info('tokbox_deny_schedule %s', scheduleTime)
                        if call_his:
                            logger.info('tokbox_deny_no_call_his %s', data)
                            if tokbox_token in (call_his[0][0], call_his[0][1]):
                                logger.info('tokbox_deny_wrong_tokbox_token %s', data)
                                if tokbox_session_id == call_his[0][2]:
                                    logger.info('tokbox_deny_wrong_tokbox_session_id_v3 %s', data)
                                    be_deny_token = call_his[0][0] if call_his[0][1] == tokbox_token else call_his[0][1]
                                    logger.info('tokbox_deny_id_v3 %s', be_deny_token)
                                    mqtt_body = {
                                        "deny_user": {
                                            "action": "deny_call",
                                            "action_time": action_time,
                                            "tokbox_api_key": str(self.api_key),
                                            "tokbox_session_id": str(tokbox_session_id),
                                            "tokbox_token": str(tokbox_token),
                                            "user_id": int(deny_user_id),
                                            "appointment_id": int(appointment_id),
                                            "call_history_id": int(call_history_id),
                                            "scheduleTime": scheduleTime
                                        },
                                        "be_deny_user": {
                                            "action": "deny_call",
                                            "action_time": action_time,
                                            "tokbox_api_key": str(self.api_key),
                                            "tokbox_session_id": str(tokbox_session_id),
                                            "tokbox_token": str(be_deny_token),
                                            "user_id": int(be_deny_user_id),
                                            "appointment_id": int(appointment_id),
                                            "call_history_id": int(call_history_id),
                                            "scheduleTime": scheduleTime
                                        }
                                    }
                                    logger.info('tokbox_deny_id_v3 %s', mqtt_body)
                                    if res_token[6] == "doctor" or  res_token[6] == "hotline":
                                        mqtt_body["be_deny_user"].update({'patient_id': appointment[0][1]})
                                    
                                        if ios_device_token not in ('','False','NULL', None):
                                            ios_apns = json.loads(config.get('environment', 'ios_apns'))
                                            is_sandbox = ios_apns["sandbox"]
                                            from apns import APNs, Payload
                                            apns = APNs(use_sandbox=is_sandbox, cert_file="tokbox/VOIP.pem", enhanced=True)
                                            payload = Payload(alert=json.dumps(mqtt_body["be_deny_user"]))
                                            apns.gateway_server.send_notification(ios_device_token, payload)

                                        if droid_device_token and droid_device_token not in ('','False','NULL'):
                                            fcm_key = config.get('environment', 'fcm_key')
                                            from pyfcm import FCMNotification
                                            push_service = FCMNotification(api_key=fcm_key)
                                            result = push_service.single_device_data_message(registration_id=droid_device_token, data_message=mqtt_body['be_deny_user'])
                                    
                                    if res_token[6] == "patient":
                                        mqtt_body["be_deny_user"].update({'patient_id': appointment[0][1]})
                                        if ios_device_token not in ('','False','NULL', None):
                                            ios_apns = json.loads(config.get('environment', 'ios_apns'))
                                            is_sandbox = ios_apns["sandbox"]
                                            from apns import APNs, Payload
                                            apns = APNs(use_sandbox=is_sandbox, cert_file="DOCTORAPP.pem", enhanced=True)
                                            payload = Payload(alert=json.dumps(mqtt_body["be_deny_user"]))
                                            apns.gateway_server.send_notification(ios_device_token, payload)

                                        if droid_device_token and droid_device_token not in ('','False','NULL'):
                                            fcm_key = config.get('environment', 'fcm_key')
                                            from pyfcm import FCMNotification
                                            push_service = FCMNotification(api_key=fcm_key)
                                            result = push_service.single_device_data_message(registration_id=droid_device_token, data_message=mqtt_body['be_deny_user'])
                                    import ssl
                                    client = paho.Client()
                                    if self.authen_ssl == True:
                                        client = paho.Client(transport='websockets')
                                        client.tls_set('tokbox/ca_file.crt', cert_reqs=ssl.CERT_NONE)
                                        client.tls_insecure_set(True)
                                    client.connect(self.mqtt_host, self.mqtt_port, 60)
                                    client.loop_start()
                                    
                                    client.publish("%s" % be_deny_user_id, json.dumps(mqtt_body["be_deny_user"]), qos=2)
                                    client.loop_stop()
                                    client.disconnect()

                                    resp.update({'status_code': 200,
                                                 'resq_body': {
                                                     "code": "succeed",
                                                     "title": _("Videocall deny succeed"),
                                                     "msg": _("Videocall deny succeed")
                                                 }
                                                 })
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_tokbox_deny %s' % str(e))
            client_sentry.captureException()
        return resp

    def tokbox_accept(self, data):
        logger.info('tokbox_accept %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Videocall accept failed"),
                        "msg": _("An error occurred. Please try to accept videocall again."),
                        "code": "videocall_accept_failed"
                    }
                }
        gs = self.general_business
        pool = self.pool_conn
        try:
            user_token = data.get('user_token')
            tokbox_token = data.get('tokbox_token')
            tokbox_session_id = data.get('tokbox_session_id')
            appointment_id = data.get('appointment_id')
            call_history_id = data.get('call_history_id')
            now = datetime.now()
            action_time = int(time.mktime(now.timetuple()))

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    accept_user_id = res_token[1]
                    sql_call_his = '''select token_from, 
                                            token_to, 
                                            session_id,
                                            state, appointment_id  
                                      from appointment_call_history where id = %s '''
                    call_his = gs.sql_execute(pool, sql_call_his, commit=False, para_values=[call_history_id])
                    if call_his and call_his[0][3] in ['denied','accepted']:
                        resp = {'status_code': 401,
                                'resq_body':
                                    {
                                        "title": _("Videocall accept failed"),
                                        "msg": _("Duplicate deny/accept call"),
                                        "code": "videocall_deny_accept_failed"
                                    }
                                }
                        return resp
                    sql_update_history = '''update appointment_call_history 
                                                set write_date = now(), start_time = now(), state = 'accepted'
                                            where id = %s'''
                    gs.sql_execute(pool, sql_update_history, commit=True, para_values=[call_history_id])
                    appointment_id = call_his[0][4]
                    if appointment_id:
                        sql_update = '''update appointment set booking_status = 'open' where id = %s'''
                        gs.sql_execute(pool, sql_update, commit=True, para_values=[appointment_id])


                    sql_apm = 'select doctor_id, patient_id, schedule_date from appointment where id = %s'
                    appointment = gs.sql_execute(pool, sql_apm, commit=False, para_values=[appointment_id])

                     
                    if appointment:
                        select_call_logs = '''select id from call_record_logs where patient_id = %s and doctor_id = %s order by id desc ''' % (appointment[0][1],appointment[0][0])
                        select_call_logs_result = gs.sql_execute(pool, select_call_logs)
                        if  select_call_logs_result:
                            update_call_logs = ''' update call_record_logs set status_call = 'accepted', last_update = current_timestamp where id = %s ''' % (select_call_logs_result[0][0])
                            gs.sql_execute(pool, update_call_logs, commit=True)
                    
                        scheduleTime = int(time.mktime(appointment[0][2].timetuple()))
                        be_accept_user_id = appointment[0][0] if appointment[0][1] == accept_user_id else appointment[0][1]
                        if call_his:
                            if tokbox_token in (call_his[0][0], call_his[0][1]):
                                if tokbox_session_id == call_his[0][2]:
                                    import ssl
                                    client = paho.Client()
                                    if self.authen_ssl == True:
                                        client = paho.Client(transport='websockets')
                                        client.tls_set('tokbox/ca_file.crt', cert_reqs=ssl.CERT_NONE)
                                        client.tls_insecure_set(True)
                                    client.connect(self.mqtt_host, self.mqtt_port, 60)
                                    client.loop_start()
                                    be_deny_token = call_his[0][0] if call_his[0][1] == tokbox_token else call_his[0][1]

                                    mqtt_body = {
                                        "accept_user": {
                                            "action": "accept_call",
                                            "action_time": action_time,
                                            "tokbox_api_key": str(self.api_key),
                                            "tokbox_session_id": str(tokbox_session_id),
                                            "tokbox_token": str(tokbox_token),
                                            "user_id": int(accept_user_id),
                                            "appointment_id": int(appointment_id),
                                            "call_history_id": int(call_history_id),
                                            "scheduleTime": scheduleTime,
                                        },
                                        "be_accept_user": {
                                            "action": "accept_call",
                                            "action_time": action_time,
                                            "tokbox_api_key": str(self.api_key),
                                            "tokbox_session_id": str(tokbox_session_id),
                                            "tokbox_token": str(be_deny_token),
                                            "user_id": int(be_accept_user_id),
                                            "appointment_id": int(appointment_id),
                                            "call_history_id": int(call_history_id),
                                            "scheduleTime": scheduleTime,
                                        }
                                    }

                                    client.publish("%s" % be_accept_user_id, json.dumps(mqtt_body["be_accept_user"]),
                                                   qos=2)
                                    client.loop_stop()
                                    client.disconnect()

                                    sql_update_apm = '''update appointment 
                                                            set is_accepted = True, write_date = now()
                                                            where id = %s'''
                                    gs.sql_execute(pool, sql_update_apm, commit=True, para_values=[appointment_id])

                                    if res_token[6] == "doctor":
                                        sql_payment_trans = '''select id
                                                              from business_transfer_transaction 
                                                              where business_type = 'doctor_apm_fee' 
                                                                  and state = 'paid' 
                                                                  and appointment_id = %s'''%appointment_id
                                        payment_trans = gs.sql_execute(pool, sql_payment_trans)
                                        if not payment_trans:
                                            sql_doctor = '''select consultation_type,--0
                                                                    consultation_duration,--1
                                                                    consultation_fee,--2
                                                                    consultation_unit--3
                                                            from doctor_profile dp
                                                            left join user_account uc on uc.user_mngment_id = dp.user_mngment_id                                                              
                                                            WHERE uc.id = %s'''%accept_user_id
                                            doctor_info = gs.sql_execute(pool, sql_doctor)[0]
                                            sql_wallet = '''select id from user_wallet where user_account_id = %s'''%be_accept_user_id
                                            patient_wallet_id = gs.sql_execute(pool, sql_wallet)[0][0]
                                            if doctor_info[0] == 'live':
                                                bt = self.business_transfer
                                                bt.create_payment_appointment(appointment_id, doctor_info[2],
                                                                              wallet_source_id=patient_wallet_id,
                                                                              wallet_dest_id=self.drdr_wallet_id, note=None,
                                                                              context=None)
                                                bt.confirm_appointment(appointment_id)
                                                # update status busy by anshor
                                                #sql = '''update user_account set activity_status = 'busy' where id = %s'''%accept_user_id
                                                #record = gs.sql_execute(pool, sql,commit=True)
                   
                                    

                                    resp.update({'status_code': 200,
                                                 'resq_body': {
                                                     "code": "succeed",
                                                     "title": _("Videocall accept succeed"),
                                                     "msg": _("Videocall accept succeed")
                                                 }
                                                 })
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_tokbox_accept %s' % str(e))
            client_sentry.captureException()
        return resp



    def tokbox_missed_v3(self, data):
        logger.info('tokbox_missed_v3 %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Videocall missed failed"),
                        "msg": _("An error occurred. Please try to missed videocall again.2"),
                        "code": "videocall_missed_failed"
                    }
                }
        gs = self.general_business
        pool = self.pool_conn
        try:
            user_token = data.get('user_token')
            call_history_id = data.get('call_history_id')

            if user_token:
                res_token = self.general_business.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    deny_user_id = res_token[1]
                    sql_call_his = '''select token_from, token_to, session_id, state,id, appointment_id  
                                    from appointment_call_history where id = %s '''
                    call_his = gs.sql_execute(pool, sql_call_his, commit=False, para_values=[call_history_id])
                    if call_his and call_his[0][3] in ['denied','accepted']:
                        resp = {'status_code': 401,
                                'resq_body':
                                    {
                                        "title": _("Videocall missed failed"),
                                        "msg": _("Duplicate missed call"),
                                        "code": "videocall_missed_failed"
                                    }
                                }
                        return resp
                    appointment_id = call_his[0][5]
                    sql_update_history = '''update appointment_call_history set write_date = now(), state = 'missed'
                                            where id = %s'''
                    gs.sql_execute(pool, sql_update_history, commit=True,para_values=[call_history_id])

                    sql_apm = 'select doctor_id, patient_id, schedule_date, id from appointment where id = %s'
                    appointment = gs.sql_execute(pool, sql_apm, commit=False, para_values=[appointment_id])
                    ios_device_token = ''
                    droid_device_token = ''
                    if appointment:
                        select_call_logs = '''select id,status_call from call_record_logs where apm_id = %s order by id desc ''' % (appointment_id)
                        select_call_logs_result = gs.sql_execute(pool, select_call_logs)
                        if  select_call_logs_result:
                            update_call_logs = ''' update call_record_logs set status_call = 'missed', last_update = current_timestamp where id = %s ''' % (select_call_logs_result[0][0])
                            gs.sql_execute(pool, update_call_logs, commit=True)
                    

                    resp.update({'status_code': 200,
                                    'resq_body': {
                                        "code": "succeed",
                                        "title": _("Videocall missed succeed"),
                                        "msg": _("Videocall missed succeed")
                                    }
                                })
                        #sql_update_doctor = '''update user_account set activity_status = 'online'
                        #                    where id = %s''' % (appointment[0][0])
                        #gs.sql_execute(pool,sql_update_doctor, commit=True)
                            
                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }
        except Exception as e:
            logger.error('error_tokbox_missed %s' % str(e))
            resp = {'status_code': 401,
                    'resq_body':
                        {
                        "title": _("Videocall missed failed"),
                        "msg": _("Duplicate missed call"),
                        "code": "videocall_missed_failed",
                        "error" : str(e)
                        }
                    }
            #client_sentry.captureException()
        return resp

    def get_wallet(self, user_email, email=False):
        try:
            access_model = self.access_model
            if email:
                user_id = access_model.search('user_account',
                                                 [('email', '=', user_email)],
                                                 ['id'])
                if not user_id:
                    logger.error(
                        'get_wallet error: This email is not exist: %s' % user_id)
                    return False
                wallet_ids = access_model.search('user_wallet',
                                                 [('user_account_id', '=', user_id[0][0])],
                                                 ['id'])
            else:
                wallet_ids = access_model.search('user_wallet',
                                                 [('user_account_id', '=', user_email)],
                                                 ['id'])
            if wallet_ids and len(wallet_ids) > 0:
                return wallet_ids[0][0]
            logger.error(
                'get_wallet error: Cannot get_wallet for this user: %s' % user_id)
            return False
        except Exception as e:
            client.captureException()
            logger.error('get_wallet error: %s' % str(e))

    