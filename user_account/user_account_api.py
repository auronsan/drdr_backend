#!/usr/bin/env python
# -*- coding: utf-8 -*-
import falcon
import json
import  os
from datetime import datetime
from user_account_business import user_account_business
try:
    from general import general_business
    from aws_management import aws_management
except ImportError as e:
    print('No Import ', e)
import ConfigParser
import os

project_path =  str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
config = ConfigParser.RawConfigParser(allow_no_value=True)
config.read(project_path + '/' + "environment.conf")

AWS_BUCKET = config.get('environment', 's3_bucket')
BUCKET_URL_DEFAULT = AWS_BUCKET + '.' + config.get('environment', 's3_address')

status_code = {
    200: falcon.HTTP_200,
    401: falcon.HTTP_401,
    502: falcon.HTTP_502,
    203: falcon.HTTP_203
}


class user_account(object):
    def __init__(self, signal, erp_connection=False, var_contructor=False):
        self.business = user_account_business(signal, erp_connection, var_contructor)
        self.pool_conn = erp_connection
        self.general_business = general_business(erp_connection, var_contructor)
        self.aws = aws_management()

        if var_contructor:
            self.ERP_DB_HOST = var_contructor['ERP_DB_HOST']
            self.ERP_DB_PORT = var_contructor['ERP_DB_PORT']
            self.ERP_DB_USER = var_contructor['ERP_DB_USER']
            self.ERP_DB_PASS = var_contructor['ERP_DB_PASS']
            self.ERP_DB_NAME = var_contructor['ERP_DB_NAME']

        self.signal = signal

    def on_get(self, req, resp,hospital_id=None):
        try:
            data = {}
            header = req.headers  # key cua header luon viet hoa
            all_header = self.general_business.get_header_request(header)
            lang = all_header.get('lang')
            _ = self.general_business.get_lang_instance(lang)
            data.update(all_header)

            if self.signal == 'user_settings_get':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Get setting failed"),
                    "msg": _("An error occurred. Please try to get setting again."),
                    "code": "get_setting_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'lang': lang,
                    })
                    record = self.business.user_settings_get(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

        except Exception as e:
            print str(e)

    def on_post(self, req, resp,hospital_id=None):
        try:
            data = {}
            header = req.headers  # key cua header luon viet hoa
            all_header = self.general_business.get_header_request(header)
            lang = all_header.get('lang')
            _ = self.general_business.get_lang_instance(lang)
            data.update(all_header)

            if self.signal == 'user_login':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Login failed"),
                    "msg": _("An error occurred. Please try to login again."),
                    "code": "login_failed"
                }
                try:
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    login = raw_data['email']
                    password = raw_data['password']
                    record = []
                    data.update({
                        'login': login,
                        'password': password,
                        'version': 1,
                        'lang': lang,
                        'client_side': 'website'
                    })
                    record = self.business.authentication(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            
            # MP2.10 API for patient to login by email or phone number
            elif self.signal == 'user_login_v2':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Login failed"),
                    "msg": _("An error occurred. Please try to login again."),
                    "code": "login_failed"
                }
                try:
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    login = raw_data['emailOrPhone']
                    password = raw_data['password']
                    data.update({
                        'login': login,
                        'password': password,
                        'version': 1,
                        'client_side': 'mobile',
                        'lang': lang,
                    })
                    record = self.business.authentication(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            elif self.signal == 'user_login_v2_hospital':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Login failed"),
                    "msg": _("An error occurred. Please try to login again."),
                    "code": "login_failed"
                }
                try:
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    login = raw_data['emailOrPhone']
                    password = raw_data['password']
                    data.update({
                        'login': login,
                        'password': password,
                        'version': 1,
                        'client_side': 'mobile',
                        'lang': lang,
                        "hospital_id": hospital_id
                    })
                    record = self.business.authentication_hospital(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            elif self.signal == 'user_login_v3_hospital':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Login failed"),
                    "msg": _("An error occurred. Please try to login again."),
                    "code": "login_failed"
                }
                try:
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    login = raw_data['emailOrPhone']
                    password = raw_data['password']
                    data.update({
                        'login': login,
                        'password': password,
                        'version': 1,
                        'client_side': 'mobile',
                        'lang': lang,
                        "hospital_id": hospital_id
                    })
                    record = self.business.authentication_hospital_v3(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'admin_user_login_v2':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Login failed"),
                    "msg": _("An error occurred. Please try to login again."),
                    "code": "login_failed"
                }
                try:
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    login = raw_data['emailOrPhone']
                    password = raw_data['password']
                    data.update({
                        'login': login,
                        'password': password,
                        'version': 1,
                        'admin_role': True,
                        'lang': lang,
                    })
                    record = self.business.authentication(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'user_facebook_login':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Login failed"),
                    "msg": _("An error occurred. Please try to login again."),
                    "code": "login_failed"
                }
                try:
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    header = req.headers
                    record = []
                    data.update({
                        'facebook_token': header.get("X-USERFACEBOOK-TOKEN", False),
                        'lang': lang,
                        'hospital_id': hospital_id
                    })
                    record = self.business.user_facebook_login(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            elif self.signal == 'user_facebook_login_hospital':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Login failed"),
                    "msg": _("An error occurred. Please try to login again."),
                    "code": "login_failed"
                }
                try:
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    header = req.headers
                    record = []
                    data.update({
                        'facebook_token': header.get("X-USERFACEBOOK-TOKEN", False),
                        'lang': lang,
                        "hospital_id": hospital_id
                    })
                    record = self.business.user_facebook_login_hospital(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            
            elif self.signal == 'user_facebook_login_hospital_v3':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Login failed"),
                    "msg": _("An error occurred. Please try to login again."),
                    "code": "login_failed"
                }
                try:
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    header = req.headers
                    record = []
                    data.update({
                        'facebook_token': header.get("X-USERFACEBOOK-TOKEN", False),
                        'lang': lang,
                        "hospital_id": hospital_id
                    })
                    record = self.business.user_facebook_login_hospital_v3(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'user_logout':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Logout failed"),
                    "msg": _("An error occurred. Please try to logout again."),
                    "code": "logout_failed"
                }
                try:
                    #                     raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    header = req.headers  # key cua header luon viet hoa
                    #                     user_id = raw_data['userId']
                    record = []
                    data.update({
                        #                             'user_id': user_id,
                        'user_token': header.get("X-USER-TOKEN", False),
                        'user_language': header.get("ACCEPT-LANGUAGE", False),
                        'lang': lang,

                    })
                    record = self.business.user_logout(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'user_register':
                resp_status = status_code[401]
                resp_body = {
                    "code": "register_failed",
                    "title": _("Register failed"),
                    "msg": _("An error occurred. Please try to register again.")
                }
                try:
                    raw_data = json.loads(req.stream.read(req.content_length or 0) or '{}')
                    email = raw_data.get('email', False)
                    password = raw_data.get('password', False)
                    phone = raw_data.get('phoneNumber', False)
                    user_role = raw_data.get('userRoleCode', False)
                    if password and phone and user_role:
                        data.update({
                            'userRoleCode': user_role,
                            'email': email,
                            'phoneNumber': phone,
                            'password': password,
                            'lang': lang,
                        })
                        record = self.business.create_user_account(data)
                        if record:
                            resp_status = status_code[record['status_code']]
                            resp_body = record['resq_body']
                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'user_register_v2':
                resp_status = status_code[401]
                resp_body = {
                    "code": "register_failed",
                    "title": _("Register failed"),
                    "msg": _("An error occurred. Please try to register again.")
                }
                try:
                    data = json.loads(req.stream.read(req.content_length or 0) or '{}')
                    data.update({'lang': lang})
                    record = self.business.create_user_account_v2(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']
                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            elif self.signal == 'user_register_v2_hospital':
                resp_status = status_code[401]
                resp_body = {
                    "code": "register_failed",
                    "title": _("Register failed"),
                    "msg": _("An error occurred. Please try to register again.")
                }
                try:
                    data = json.loads(req.stream.read(req.content_length or 0) or '{}')
                    data.update({
                        'lang': lang,
                        "hospital_id": hospital_id})
                    record = self.business.create_user_account_v2_hospital(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']
                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'user_register_v2_editprofile':
                resp_status = status_code[401]
                resp_body = {
                    "code": "edit_profile_failed",
                    "title": _("Edit profile failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }
                try:
                    data = json.loads(req.stream.read(req.content_length or 0) or '{}')
                    data.update({'lang': lang})
                    record = self.business.user_register_v2_editprofile(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']
                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'user_register_v2_uploaddocs':
                import math
                gs = self.general_business
                pool = self.pool_conn
                resp_status = status_code[401]
                resp_body = {
                    "code": "uploaddocs_failed",
                    "title": _("Upload documents failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }
                try:
                    # header = req.headers  # key cua header luon viet hoa
                    userId = req.get_param('userId', default=False)
                    sql = """SELECT validation_status, user_mngment_id FROM user_account where id = %s """
                    user_info = gs.sql_execute(pool, sql, para_values=[userId])[0]
                    if user_info:
                        if user_info[0] == 'active':
                            resp_status = status_code[401]
                            resp_body = {
                                            "code": "activated_account",
                                            "msg": _("The account is already activated"),
                                            "title": _("Account is activated")
                                        }
                            resp.status = resp_status
                            resp.body = json.dumps(resp_body)
                            return False
                        data.update({'user_id': userId,
                                'list_file': [],
                                'lang': lang,})
                        list_file = []
                        count = 0
                        for i in range(1,12):
                            document = req.get_param('file%s'%i, default=False)
                            if type(document).__name__ == 'instance':
                                def get_infomation_file(file):
                                    f = file.file
                                    file_size = os.fstat(f.fileno()).st_size
                                    file_name, file_extension = os.path.splitext(file.filename)
                                    return {
                                        'file_size': file_size,
                                        'file_name': file_name,
                                        'file_extension': file_extension.replace('.', '').lower()
                                    }

                                file_info = get_infomation_file(document)
                                if file_info['file_extension'] in ('jpg', 'png', 'gif', 'jpeg', 'pdf'):
                                    file_info.update({'binary': document.file})
                                    list_file.append(file_info)
                                    if long(file_info['file_size']) / math.pow(1024, 2) > 10:
                                        resp_status = status_code[401]
                                        resp_body = {
                                            "code": "invalid_file",
                                            "msg": _("The uploading file must be jpg/jpeg/png/gif/pdf and not larger than 10MB"),
                                            "title": _("Invalid file")
                                        }
                                        resp.status = resp_status
                                        resp.body = json.dumps(resp_body)
                                        return False
                                    count += 1
                                else:
                                    resp_status = status_code[401]
                                    resp_body = {
                                        "code": "invalid_file",
                                        "msg": _("The uploading file must be jpg/jpeg/png/gif/pdf and not larger than 10MB"),
                                        "title": _("Invalid file")
                                    }
                                    resp.status = resp_status
                                    resp.body = json.dumps(resp_body)
                                    return False
                        if count > 0:
                            if count > 10:
                                resp_status = status_code[401]
                                resp_body = {
                                    "code": "too_much_files",
                                    "msg": _("The maximum number of files can be uploaded is 10"),
                                    "title": _("To much files")
                                }
                                resp.status = resp_status
                                resp.body = json.dumps(resp_body)
                                return False
                            create_date = datetime.now().strftime('%Y%m%d%H%M%S')
                            i = 0
                            for file in list_file:
                                i+=1
                                # print file['binary']
                                filename = 'file_%s_%s_%s.%s' % (userId, i, create_date, file['file_extension'])
                                self.aws.upload_obj(file['binary'], AWS_BUCKET, filename)
                                data['list_file'].append({'url': 'https://' + BUCKET_URL_DEFAULT + '/' + filename,
                                                         'file_name': file['file_name'],
                                                         'file_extension': file['file_extension'],
                                                         'file_size': file['file_size'],})

                            record = self.business.user_register_v2_uploaddocs(data)
                            if record:
                                resp.status = status_code[record['status_code']]
                                resp.body = json.dumps(record['resq_body'])
                                return False
                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'user_documents_upload':
                import math
                gs = self.general_business
                pool = self.pool_conn
                resp_status = status_code[401]
                resp_body = {
                    "code": "uploaddocs_failed",
                    "title": _("Upload documents failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    res_token = gs.validate_user_token(header.get("X-USER-TOKEN", False),data.get('client_id'))
                    if not res_token:
                        resp_status = status_code[203]
                        resp_body = {
                            "code": "expired_token",
                            "title": _("Expired token"),
                            "msg": _("Your login-token is expired, please try to login again")
                        }
                        resp.status = resp_status
                        resp.body = json.dumps(resp_body)
                        return False

                    else:
                        userId = res_token[1]
                        data.update({'user_id': userId,
                                'list_file': [],
                                'lang': lang,})
                        list_file = []
                        count = 0
                        for i in range(1,12):
                            document = req.get_param('file%s'%i, default=False)
                            if type(document).__name__ == 'instance':
                                def get_infomation_file(file):
                                    f = file.file
                                    file_size = os.fstat(f.fileno()).st_size
                                    file_name, file_extension = os.path.splitext(file.filename)
                                    return {
                                        'file_size': file_size,
                                        'file_name': file_name,
                                        'file_extension': file_extension.replace('.', '').lower()
                                    }

                                file_info = get_infomation_file(document)
                                if file_info['file_extension'] in ('jpg', 'png', 'gif', 'jpeg', 'pdf'):
                                    file_info.update({'binary': document.file})
                                    list_file.append(file_info)
                                    if long(file_info['file_size']) / math.pow(1024, 2) > 10:
                                        resp_status = status_code[401]
                                        resp_body = {
                                            "code": "invalid_file",
                                            "msg": _("The uploading file must be jpg/jpeg/png/gif/pdf and not larger than 10MB"),
                                            "title": _("Invalid file")
                                        }
                                        resp.status = resp_status
                                        resp.body = json.dumps(resp_body)
                                        return False
                                    count += 1
                                else:
                                    resp_status = status_code[401]
                                    resp_body = {
                                        "code": "invalid_file",
                                        "msg": _("The uploading file must be jpg/jpeg/png/gif/pdf and not larger than 10MB"),
                                        "title": _("Invalid file")
                                    }
                                    resp.status = resp_status
                                    resp.body = json.dumps(resp_body)
                                    return False
                        if count > 0:
                            if count > 10:
                                resp_status = status_code[401]
                                resp_body = {
                                    "code": "too_much_files",
                                    "msg": _("The maximum number of files can be uploaded is 10"),
                                    "title": _("To much files")
                                }
                                resp.status = resp_status
                                resp.body = json.dumps(resp_body)
                                return False
                            create_date = datetime.now().strftime('%Y%m%d%H%M%S')
                            i = 0
                            for file in list_file:
                                i+=1
                                # print file['binary']
                                filename = 'file_%s_%s_%s.%s' % (userId, i, create_date, file['file_extension'])
                                self.aws.upload_obj(file['binary'], AWS_BUCKET, filename)
                                data['list_file'].append({'url': 'https://' + BUCKET_URL_DEFAULT + '/' + filename,
                                                         'file_name': file['file_name'],
                                                         'file_extension': file['file_extension'],
                                                         'file_size': file['file_size'],})

                            record = self.business.user_register_v2_uploaddocs(data)
                            if record:
                                resp.status = status_code[record['status_code']]
                                resp.body = json.dumps(record['resq_body'])
                                return False
                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)



            elif self.signal == 'user_active_sms':
                print 'user_active_sms'
                resp_status = status_code[401]
                resp_body = {
                    "code": "activate_failed",
                    "title": _("Activate failed"),
                    "msg": _("An error occurred. Please try to activate your account again.")
                }
                try:
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    header = req.headers  # key cua header luon viet hoa
                    user_id = raw_data.get('userId', False)
                    sms_opt = raw_data.get('smsOTP', False)
                    if user_id and sms_opt:
                        data.update({
                            'sms_opt': sms_opt,
                            'user_id': user_id,
                            'lang': lang,
                        })
                        record = self.business.active_user_account_sms(data)
                        if record:
                            resp_status = status_code[record['status_code']]
                            resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'user_request_sms':
                print 'user_request_sms'
                resp_status = status_code[401]
                resp_body = {
                    "code": "request_sms_failed",
                    "title": _("Request SMS failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }
                try:
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    header = req.headers  # key cua header luon viet hoa
                    email = raw_data.get('email', False)
                    if email:
                        data.update({
                            'email': email,
                            'lang': lang,
                        })
                        record = self.business.user_request_sms(data)
                        if record:
                            resp_status = status_code[record['status_code']]
                            resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'user_request_sms_v2':
                print 'user_request_sms_v2'
                resp_status = status_code[401]
                resp_body = {
                    "code": "request_sms_failed",
                    "title": _("Request SMS failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }
                try:
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    header = req.headers  # key cua header luon viet hoa
                    userId = raw_data.get('userId', False)
                    if userId:
                        data.update({
                            'userId': userId,
                            'lang': lang,
                        })
                        record = self.business.user_request_sms(data)
                        if record:
                            resp_status = status_code[record['status_code']]
                            resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'user_request_new_pasword':
                print 'user_request_new_pasword'
                resp_status = status_code[401]
                resp_body = {
                    "code": "request_failed",
                    "title": _("Request failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }
                try:
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    email = raw_data.get('email', False)
                    if email:
                        data.update({
                            'email': email,
                            'lang': lang,
                        })
                        record = self.business.request_new_password(data)
                        if record:
                            resp_status = status_code[record['status_code']]
                            resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            
            elif self.signal == 'user_request_new_pasword_hospital':
                resp_status = status_code[401]
                resp_body = {
                    "code": "request_failed",
                    "title": _("Request failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }
                try:
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    email = raw_data.get('email', False)
                    if email:
                        data.update({
                            'email': email,
                            'lang': lang,
                            'hospital_id': hospital_id
                        })
                        record = self.business.request_new_password_hospital(data)
                        if record:
                            resp_status = status_code[record['status_code']]
                            resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            
            elif self.signal == 'user_change_pasword':
                print 'user_change_pasword'
                resp_status = status_code[401]
                resp_body = {
                    "code": "request_failed",
                    "title": _("Request failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }
                try:
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    header = req.headers
                    cur_pass = raw_data.get('currentPassword', False)
                    new_pass = raw_data.get('newPassword', False)
                    if cur_pass and new_pass:
                        data.update({
                            'user_token': header.get("X-USER-TOKEN", False),
                            'cur_pass': cur_pass,
                            'new_pass': new_pass,
                            'lang': lang,
                        })
                        record = self.business.change_password(data)
                        if record:
                            resp_status = status_code[record['status_code']]
                            resp_body = record['resq_body']
                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'user_update_phone':
                print 'user_update_phone'
                resp_status = status_code[401]
                resp_body = {
                    "code": "update_phone_failed",
                    "title": _("Update Phone failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }
                try:
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    header = req.headers
                    user_id = raw_data.get('userId', False)
                    new_phone = raw_data.get('newPhoneNumber', False)
                    if user_id and new_phone:
                        data.update({
                            'user_id': user_id,
                            'new_phone': new_phone,
                            'lang': lang,
                        })
                        record = self.business.user_update_phone(data)
                        if record:
                            resp_status = status_code[record['status_code']]
                            resp_body = record['resq_body']
                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'user_settings_set':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Set setting failed"),
                    "msg": _("An error occurred. Please try to set setting again."),
                    "code": "set_setting_failed"
                }
                try:
                    header = req.headers  # key cua header luon viet hoa
                    raw_data = json.loads(req.stream.read(req.content_length or 0) or '{}')
                    language_code = raw_data.get('languageCode', 'en')
                    automatic_video = raw_data.get('automaticVideo', False)
                    record = []
                    data.update({
                        'user_token': header.get("X-USER-TOKEN", False),
                        'language_code': language_code,
                        'automatic_video': automatic_video,
                        'lang': lang,
                    })
                    record = self.business.user_settings_set(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)

                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # WD2.17 API for listing the documents of an inactivated doctor account
            elif self.signal == 'user_register_v2_getdocs':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Get document list"),
                    "msg": _("An error occurred. Please try to set again."),
                    "code": "doctor_get_document_list"
                }
                try:
                    
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    raw_data.update({'lang': lang})
                    record = []
                    record = self.business.user_register_v2_getdocs(raw_data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body) 

            # WD2.18 API for deleting a specific document of an inactivated doctor account
            elif self.signal == 'user_register_v2_deletedoc':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Delete document"),
                    "msg": _("An error occurred. Please try to set again."),
                    "code": "doctor_delete_document"
                }
                try:
                    
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    raw_data.update({'lang': lang})
                    record = []
                    record = self.business.user_register_v2_deletedoc(raw_data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            elif self.signal == 'user_documents_delete':
                header = req.headers  # key cua header luon viet hoa
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Delete document"),
                    "msg": _("An error occurred. Please try to request again."),
                    "code": "doctor_delete_document"
                }
                try:

                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    raw_data.update({'lang': lang,
                                     'user_token': header.get("X-USER-TOKEN", False),
                                     })
                    record = []
                    record = self.business.user_documents_delete(raw_data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)
            
            # WD2.19 API for sending application form
            elif self.signal == 'user_register_v2_sendtheform':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Send application form"),
                    "msg": _("An error occurred. Please try to set again."),
                    "code": "send_application_form"
                }
                try:
                    
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    raw_data.update({'lang': lang})
                    record = []
                    record = self.business.user_register_v2_sendtheform(raw_data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)

            # WD2.20 API for doctor to go online with a specific duration
            elif self.signal == 'doctor_goOnline_v2':
                resp_status = status_code[401]
                resp_body = {
                    "title": _("Set doctor online"),
                    "msg": _("An error occurred. Please try to set again."),
                    "code": "set_doctor_online"
                }
                try:
                    
                    raw_data = eval(req.stream.read(req.content_length or 0) or '{}')
                    raw_data.update({'lang': lang,
                                     'user_token': header.get("X-USER-TOKEN", False),
                                     })
                    record = []
                    data.update(raw_data)
                    record = self.business.doctor_goOnline_v2(data)
                    if record:
                        resp_status = status_code[record['status_code']]
                        resp_body = record['resq_body']

                except Exception as e:
                    print str(e)
                resp.status = resp_status
                resp.body = json.dumps(resp_body)
             

        except Exception as e:
            pass
