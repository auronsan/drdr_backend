#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime
import time
import hashlib
import random
import facebook
import logging
from raven import Client
import ConfigParser
import json
from dateutil.relativedelta import relativedelta
from twilio.rest import Client as twilio_Client
import sys
import smtplib
import threading
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.utils import formataddr
from email.header import Header
import os

from opentok import OpenTok, MediaModes

from io import BytesIO
import requests
import ConfigParser

project_path =  str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
config = ConfigParser.RawConfigParser(allow_no_value=True)
config.read(project_path + '/' + "environment.conf")
sentry_key = config.get('environment', 'sentry_key')
client = Client(sentry_key)
logger = logging.getLogger('drdr_api')
sys.path.insert(0, "..")

try:
    from general import general_business
    from profile.profile_doctor_business import profile_doctor_business
    
    from aws_management import aws_management
except ImportError as e:
    print('No Import ', e)

AWS_BUCKET = config.get('environment', 's3_bucket')
BUCKET_URL_DEFAULT = AWS_BUCKET + '.' + config.get('environment', 's3_address')

class send_email_thread(threading.Thread):
   def __init__(self, information, userId):
      threading.Thread.__init__(self)
      self.information = information
      self.userId = userId
   def run(self):
       print "Starting send_email_thread"
       self.send_email_application_form()
       print "Exiting send_email_thread"

   def send_email_application_form(self):
       information = self.information
       userId = self.userId
       email_config = json.loads(config.get('environment', 'email_config'))
       fromaddr = email_config['sender']
       # toaddr = 'yenbao1340@gmail.com'
       toaddr = 'doctor.reg@doctor.help'
       username = email_config['username']
       password = email_config['password']
       msg = MIMEMultipart()

       msg['From'] = formataddr((str(Header('DoctorDoctor', 'utf-8')), fromaddr))
       msg['To'] = toaddr  # doctor.reg@doctor.help
       msg['Subject'] = "[Application form] - userId: %s" % (userId)
       body = '''
        Hi,

        Doctor information:
            %s

        Thanks,
        The DoctorDoctor Team
        ''' % (information)
       msg.attach(MIMEText(body, 'plain'))
       s = smtplib.SMTP(email_config['host'], email_config['port'])
       s.ehlo()
       s.starttls()
       s.ehlo
       s.login(username, password)
       text = msg.as_string()
       s.sendmail(fromaddr, toaddr, text)
       s.close()
       return True

class user_account_business(object):
    def __init__(self, signal, erp_connection, var_contructor):
        self.pool_conn = erp_connection

        self.ERP_DB_HOST = var_contructor['ERP_DB_HOST']
        self.ERP_DB_PORT = var_contructor['ERP_DB_PORT']
        self.ERP_DB_USER = var_contructor['ERP_DB_USER']
        self.ERP_DB_PASS = var_contructor['ERP_DB_PASS']
        self.ERP_DB_NAME = var_contructor['ERP_DB_NAME']

        self.signal = signal

        self.general_business = general_business(erp_connection, var_contructor)
        self.doctor_profile = profile_doctor_business('',erp_connection, var_contructor)
        self.vi_ = self.general_business.vi_
        self.en_ = self.general_business.en_
        tokbox_config = json.loads(config.get('environment', 'tokbox_config'))
        self.api_key = tokbox_config['api_key']
        self.api_secret = tokbox_config['api_secret']
        self.opentok = OpenTok(self.api_key, self.api_secret)

    def send_email_reset_password(self, mail_to, new_pass):
        email_config = json.loads(config.get('environment', 'email_config'))
        fromaddr = email_config['sender']
        toaddr = mail_to
        username = email_config['username']
        password = email_config['password']
        msg = MIMEMultipart()

        msg['From'] = formataddr((str(Header('DoctorDoctor', 'utf-8')), fromaddr))
        msg['To'] = mail_to
        msg['Subject'] = "DoctorDoctor password reset"
        body = '''
        Hi,
        
        Your DoctorDoctor password was reset using the email address %s.
        New password is: %s
        
        Thanks,
        The DoctorDoctor Team
        ''' % (toaddr, new_pass)
        msg.attach(MIMEText(body, 'plain'))
        s = smtplib.SMTP(email_config['host'], email_config['port'])
        s.ehlo()
        s.starttls()
        s.ehlo
        s.login(username, password)
        text = msg.as_string()
        s.sendmail(fromaddr, toaddr, text)
        s.close()

    def get_infomation_facebook(self, token, user_id=None):
        facebook_graph = facebook.GraphAPI(token)
        res = {}
        try:
            args = {'fields': 'id, name, email, work, birthday, education, location, gender, first_name,last_name,middle_name'}
            profile = facebook_graph.get_object('me', **args)
            photo = facebook_graph.get_object('me/picture?width=720', **{'fields': 'url'})
            birthday = None
            if profile.get('birthday', False):
                birthday = datetime.strptime(profile.get('birthday', False), '%m/%d/%Y').strftime('%Y-%m-%d')
            res = {
                'facebook_id': profile.get('id', None),
                'name': profile.get('name', None),
                'work': profile.get('work', None),
                'email': profile.get('email', None),
                'gender': profile.get('gender', None),
                'birthday': birthday,
                'location': profile.get('location', None),
                'education': profile.get('education', None),
                'first_name': profile.get('first_name', None),
                'last_name': profile.get('last_name', None),
                'middle_name': profile.get('middle_name', None),
                'profile_image_url': photo.get('url',None),
            }
            return res
        except Exception as e:
            logger.error('error_get_infomation_facebook %s' % str(e))
            client_sentry.captureException()

        return res

    def user_facebook_login(self, data):
        logger.info('start func user_facebook_login %s' % data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Login failed"),
                        "msg": _("An error occurred. Please try to login again."),
                        "code": "login_failed"
                    }
                }

          
              
        try:
            gs = self.general_business
            pool = self.pool_conn
            if data and data.get('facebook_token', False):
                fb_token = data.get('facebook_token')
                user_token = False
                user_id = False
                user_role = 'patient'

                # Get fb infomation from token
                fb_info = self.get_infomation_facebook(fb_token)
                if fb_info:
                    sql = "select user_account_id from facebook_account where facebook_id = %s "
                    fb_acc_exist = gs.sql_execute(pool, sql, para_values=(fb_info['facebook_id'],)) 
                    if fb_acc_exist:
                        user_sql = """select ua.id,
                                        code,
                                        token,
                                        email,
                                        user_mngment_id,
                                        client_id
                                from user_account ua
                                    left join user_account_user_role uaur on uaur.user_account_id = ua.id
                                    left join user_role ur on ur.id = uaur.user_role_id 
                                    left join user_account_token uat on uat.user_account_id = ua.id
                                where ua.id = %s"""

                        user_res = gs.sql_execute(pool, user_sql, para_values=(fb_acc_exist[0][0],))
                        user_token = False
                        user_role = user_res[0][1]
                        user_id = fb_acc_exist[0][0]
                        new_profile_id = user_res[0][4]
                        for line in user_res:
                            if line[5] == data.get('client_id'):
                                user_token = line[2]
                                break
                        if not user_token:
                            value_hash = str(user_id) + datetime.now().strftime('%Y%m%d%H%M%S')
                            user_token = hashlib.md5(value_hash).hexdigest()
                            sql = """insert into user_account_token (user_account_id,token,client_id) 
                                                                                      VALUES (%s, %s, %s) RETURNING id"""
                            gs.sql_execute(pool, sql, commit=True,
                                           para_values=(user_id, user_token, data.get('client_id')))
                        
                             
                        sql_update = '''update user_account 
                                                set validation_status = 'active',
                                                    activity_status = 'online',
                                                    email = %s
                                            where id = %s'''
                        gs.sql_execute(pool, sql_update, commit=True,
                                       para_values=(fb_info['email'] or user_res[0][3], user_id))
                        sql_update2 = "UPDATE facebook_account SET access_token = %s WHERE facebook_id = %s"
                        gs.sql_execute(pool, sql_update2, commit=True, para_values=(fb_token, fb_info['facebook_id']))
                    else:
                        if fb_info['email']:
                            user_sql = """select ua.id,
                                            code,
                                            validation_status,
                                            phone_confirmed,
                                            token,
                                            facebook_id,
                                            user_mngment_id
                                    from user_account ua
                                        left join user_account_user_role uaur on uaur.user_account_id = ua.id
                                        left join user_role ur on ur.id = uaur.user_role_id 
                                        left join user_account_token uat on uat.user_account_id = ua.id
                                        left join facebook_account fa on fa.user_account_id = ua.id
                                    where email=%s """
                            user_res = gs.sql_execute(pool, user_sql, para_values=(fb_info['email'],))
                        else:
                            user_res = []

                        if user_res:
                            user_token = user_res[0][4]
                            user_role = user_res[0][1]
                            user_id = user_res[0][0]
                            new_profile_id = user_res[0][6]

                        else:
                            sql = '''select id from user_role where code = %s '''
                            role_id = gs.sql_execute(pool, sql, para_values=(user_role,))
                            if role_id:
                                # insert profile to db
                                insert_profile = '''insert into user_mngment (type_profile, 
                                                                              relationship,
                                                                              first_name,
                                                                              last_name,
                                                                              middle_name,
                                                                              birthday,
                                                                              gender,
                                                                              profile_image_url)
                                                    values (%s,'me',%s,%s,%s,%s,%s,%s)
                                                    RETURNING id'''
                                new_profile_id = gs.sql_execute(pool, insert_profile, commit=True,
                                                                para_values=(user_role,
                                                                             fb_info['first_name'],
                                                                             fb_info['last_name'],
                                                                             fb_info['middle_name'],
                                                                             fb_info['birthday'],
                                                                             fb_info['gender'],
                                                                             fb_info['profile_image_url']))[0][0]

                                url = fb_info['profile_image_url']         
                                r = requests.get(url,stream=True)
                                if r.status_code == 200:
                                    aws = aws_management()
                                    f = BytesIO(r.content)
                                    f.seek(0)
                                    filename = 'avatar_fb_ddvnavatar_%s' % new_profile_id
                                    aws.upload_obj(f, AWS_BUCKET, filename)
                                    avatar_url = 'https://' + BUCKET_URL_DEFAULT + '/' + filename
                                    sql_update3 = "UPDATE user_mngment set profile_image_url = %s where id= %s"
                                    gs.sql_execute(pool, sql_update3, commit=True, para_values=(avatar_url,new_profile_id))
                                # insert useraccount to db
                                create_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                                create_user_sql = ("""
                                  insert into user_account (email,
                                                            validation_status,
                                                            activity_status,
                                                            created_date,
                                                            phone_confirmed,
                                                            email_confirmed,
                                                            user_mngment_id
                                                            )
                                  VALUES (%s, 'active','online',%s, 'False', 'False',%s)
                                  RETURNING id
                                """)
                                user_id = gs.sql_execute(pool, create_user_sql, commit=True,
                                                         para_values=(fb_info['email'], create_date, new_profile_id))[
                                    0][0]

                                # insert role-user to db
                                sql_insert_role = ''' insert into user_account_user_role(user_role_id,user_account_id) 
                                                    Values  (%s,%s)
                                                    returning id'''
                                gs.sql_execute(pool, sql_insert_role, commit=True, para_values=(role_id[0][0], user_id))

                                # insert wallet to db
                                sql_insert_wallet = ''' insert into user_wallet(total_balance,available_balance,state,created_date,write_date,user_account_id,unitcode) 
                                                                            Values  (0,0,'active',%s,%s,%s,'vnd')
                                                                            returning id'''
                                gs.sql_execute(pool, sql_insert_wallet, commit=True,
                                               para_values=(create_date, create_date, user_id))

                                # insert profile
                                if user_role == "patient":
                                    sql_insert_profile = '''INSERT INTO patient_profile (user_mngment_id) VALUES (%s)'''
                                elif user_role == "doctor":
                                    sql_insert_profile = '''INSERT INTO doctor_profile (user_mngment_id) VALUES (%s)'''
                                gs.sql_execute(pool, sql_insert_profile, commit=True, para_values=[new_profile_id])
                                # insert user_contact
                                sql_insert_contact = ''' INSERT INTO user_contact(user_mngment_id) VALUES (%s) returning id'''
                                new_id_contact = gs.sql_execute(pool, sql_insert_contact, commit=True,
                                                                para_values=(new_profile_id,))
                                # insert user_address with primary = true
                                sql_insert_address = ''' INSERT INTO user_address(user_contact_id,is_primary) VALUES (%s,'true')
                                                                    returning id'''
                                gs.sql_execute(pool, sql_insert_address, commit=True,
                                               para_values=(new_id_contact[0][0],))
                                # insert user_address with primary = fals
                                if user_role == "patient":
                                    sql_insert_address = ''' INSERT INTO user_address(user_contact_id,is_primary) VALUES  (%s,'false')
                                                                        returning id'''
                                    gs.sql_execute(pool, sql_insert_address, commit=True,
                                                   para_values=(new_id_contact[0][0],))

                            else:
                                resp.update({
                                    'status_code': 401,
                                    'resq_body': {
                                        "code": "no_exist_role",
                                        "title": _("No exist role"),
                                        "msg": _("The role does not exists in our system, please try to register with another one")
                                    }
                                })

                        # insert fb account to db
                        sql_insert_fb = '''insert into facebook_account (access_token,facebook_id,user_account_id)
                                        Values  (%s,%s,%s)'''
                        gs.sql_execute(pool, sql_insert_fb, commit=True,
                                       para_values=(fb_token, fb_info['facebook_id'], user_id))

                if not user_token and user_id:
                    value_hash = str(user_id) + datetime.now().strftime('%Y%m%d%H%M%S')
                    user_token = hashlib.md5(value_hash).hexdigest()
                    # lưu token lại trong db
                    sql = """insert into user_account_token (user_account_id,token,client_id) 
                              VALUES (%s, %s, %s) RETURNING id"""
                    gs.sql_execute(pool, sql, commit=True, para_values=(user_id, user_token, data.get('client_id')))

                if user_token and user_id and user_role:
                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     "ddToken": user_token,
                                     "userId": user_id,
                                     "userProfileId": new_profile_id,
                                     "userRoleCode": user_role
                                 }
                                 })
        except Exception as e:
            logger.error('error_user_facebook_login %s' % str(e))
            client_sentry.captureException()

        return resp
    
    def user_facebook_login_hospital(self, data):
        logger.info('start func user_facebook_login_hospital %s' % data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Login failed"),
                        "msg": _("An error occurred. Please try to login again."),
                        "code": "login_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            if data and data.get('facebook_token', False):
                fb_token = data.get('facebook_token')
                user_token = False
                user_id = False
                user_role = 'patient'
                hospital_id = data.get('hospital_id')
                # Get fb infomation from token
                fb_info = self.get_infomation_facebook(fb_token)
                if fb_info:
                    
                    sql = "select user_account_id from facebook_account fa inner join user_account ua on ua.id = fa.user_account_id inner join patient_profile pp on pp.user_mngment_id = ua.user_mngment_id where fa.facebook_id = %s and pp.hospital_profile_id = %s"
                    fb_acc_exist = gs.sql_execute(pool, sql, para_values=(fb_info['facebook_id'],hospital_id))
                    if fb_acc_exist:
                        user_sql = """select ua.id,
                                        code,
                                        token,
                                        email,
                                        ua.user_mngment_id,
                                        client_id
                                from user_account ua
                                    left join user_account_user_role uaur on uaur.user_account_id = ua.id
                                    left join user_role ur on ur.id = uaur.user_role_id 
                                    left join user_account_token uat on uat.user_account_id = ua.id
                                where ua.id = %s"""

                        user_res = gs.sql_execute(pool, user_sql, para_values=(fb_acc_exist[0][0],))
                        user_token = False
                        user_role = user_res[0][1]
                        user_id = fb_acc_exist[0][0]
                        new_profile_id = user_res[0][4]
                           
                        for line in user_res:
                            if line[5] == data.get('client_id'):
                                user_token = line[2]
                                break
                        if not user_token:
                            value_hash = str(user_id) + datetime.now().strftime('%Y%m%d%H%M%S')
                            user_token = hashlib.md5(value_hash).hexdigest()
                            sql = """insert into user_account_token (user_account_id,token,client_id) 
                                                                                      VALUES (%s, %s, %s) RETURNING id"""
                            gs.sql_execute(pool, sql, commit=True,
                                           para_values=(user_id, user_token, data.get('client_id')))
                        sql_update = '''update user_account 
                                                set validation_status = 'active',
                                                    activity_status = 'online',
                                                    email = %s
                                            where id = %s'''
                        gs.sql_execute(pool, sql_update, commit=True,
                                       para_values=(fb_info['email'] or user_res[0][3], user_id))
                        sql_update2 = "UPDATE facebook_account SET access_token = %s WHERE facebook_id = %s"
                        gs.sql_execute(pool, sql_update2, commit=True, para_values=(fb_token, fb_info['facebook_id']))
                        

                    else:
                        if fb_info['email']:
                            user_sql = """select ua.id,
                                            code,
                                            validation_status,
                                            phone_confirmed,
                                            token,
                                            facebook_id,
                                            ua.user_mngment_id
                                    from user_account ua
                                        left join user_account_user_role uaur on uaur.user_account_id = ua.id
                                        left join user_role ur on ur.id = uaur.user_role_id 
                                        left join user_account_token uat on uat.user_account_id = ua.id
                                        left join facebook_account fa on fa.user_account_id = ua.id
                                        left join patient_profile pp on pp.user_mngment_id = ua.user_mngment_id
                                    where email=%s and pp.hospital_profile_id = %s """
                            user_res = gs.sql_execute(pool, user_sql, para_values=(fb_info['email'],hospital_id))
                        else:
                            user_res = []

                        if user_res:
                            user_token = user_res[0][4]
                            user_role = user_res[0][1]
                            user_id = user_res[0][0]
                            new_profile_id = user_res[0][6]

                        else:
                            sql = '''select id from user_role where code = %s '''
                            role_id = gs.sql_execute(pool, sql, para_values=(user_role,))
                            if role_id:
                                # insert profile to db
                                
                                insert_profile = '''insert into user_mngment (type_profile, 
                                                                              relationship,
                                                                              first_name,
                                                                              last_name,
                                                                              middle_name,
                                                                              birthday,
                                                                              gender,
                                                                              profile_image_url)
                                                    values (%s,'me',%s,%s,%s,%s,%s,%s)
                                                    RETURNING id'''
                                new_profile_id = gs.sql_execute(pool, insert_profile, commit=True,
                                                                para_values=(user_role,
                                                                             fb_info['first_name'],
                                                                             fb_info['last_name'],
                                                                             fb_info['middle_name'],
                                                                             fb_info['birthday'],
                                                                             fb_info['gender'],
                                                                             fb_info['profile_image_url']))[0][0]

                                url = fb_info['profile_image_url']         
                                r = requests.get(url,stream=True)
                                if r.status_code == 200:
                                    aws = aws_management()
                                    f = BytesIO(r.content)
                                    f.seek(0)
                                    filename = 'avatar_fb_ddvnavatar_%s' % new_profile_id
                                    aws.upload_obj(f, AWS_BUCKET, filename)
                                    avatar_url = 'https://' + BUCKET_URL_DEFAULT + '/' + filename
                                    sql_update3 = "UPDATE user_mngment set profile_image_url = %s where id= %s"
                                    gs.sql_execute(pool, sql_update3, commit=True, para_values=(avatar_url,new_profile_id))
                                # insert useraccount to db
                                create_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                                create_user_sql = ("""
                                  insert into user_account (email,
                                                            validation_status,
                                                            activity_status,
                                                            created_date,
                                                            phone_confirmed,
                                                            email_confirmed,
                                                            user_mngment_id
                                                            )
                                  VALUES (%s, 'active','online',%s, 'False', 'False',%s)
                                  RETURNING id
                                """)
                                user_id = gs.sql_execute(pool, create_user_sql, commit=True,
                                                         para_values=(fb_info['email'], create_date, new_profile_id))[
                                    0][0]

                                # insert role-user to db
                                sql_insert_role = ''' insert into user_account_user_role(user_role_id,user_account_id) 
                                                    Values  (%s,%s)
                                                    returning id'''
                                gs.sql_execute(pool, sql_insert_role, commit=True, para_values=(role_id[0][0], user_id))

                                # insert wallet to db
                                sql_insert_wallet = ''' insert into user_wallet(total_balance,available_balance,state,created_date,write_date,user_account_id,unitcode) 
                                                                            Values  (0,0,'active',%s,%s,%s,'vnd')
                                                                            returning id'''
                                gs.sql_execute(pool, sql_insert_wallet, commit=True,
                                               para_values=(create_date, create_date, user_id))

                                # insert profile
                                if user_role == "patient":
                                    sql_insert_profile = '''INSERT INTO patient_profile (user_mngment_id,hospital_profile_id) VALUES (%s,%s)'''
                                elif user_role == "doctor":
                                    sql_insert_profile = '''INSERT INTO doctor_profile (user_mngment_id,hospital_profile_id) VALUES (%s,%s)'''
                                gs.sql_execute(pool, sql_insert_profile, commit=True, para_values=(new_profile_id,hospital_id))
                                # insert user_contact
                                sql_insert_contact = ''' INSERT INTO user_contact(user_mngment_id) VALUES (%s) returning id'''
                                new_id_contact = gs.sql_execute(pool, sql_insert_contact, commit=True,
                                                                para_values=(new_profile_id,))
                                # insert user_address with primary = true
                                sql_insert_address = ''' INSERT INTO user_address(user_contact_id,is_primary) VALUES (%s,'true')
                                                                    returning id'''
                                gs.sql_execute(pool, sql_insert_address, commit=True,
                                               para_values=(new_id_contact[0][0],))
                                # insert user_address with primary = fals
                                if user_role == "patient":
                                    sql_insert_address = ''' INSERT INTO user_address(user_contact_id,is_primary) VALUES  (%s,'false')
                                                                        returning id'''
                                    gs.sql_execute(pool, sql_insert_address, commit=True,
                                                   para_values=(new_id_contact[0][0],))

                            else:
                                resp.update({
                                    'status_code': 401,
                                    'resq_body': {
                                        "code": "no_exist_role",
                                        "title": _("No exist role"),
                                        "msg": _("The role does not exists in our system, please try to register with another one")
                                    }
                                })

                        # insert fb account to db
                        sql_insert_fb = '''insert into facebook_account (access_token,facebook_id,user_account_id)
                                        Values  (%s,%s,%s)'''
                        gs.sql_execute(pool, sql_insert_fb, commit=True,
                                       para_values=(fb_token, fb_info['facebook_id'], user_id))

                if not user_token and user_id:
                    value_hash = str(user_id) + datetime.now().strftime('%Y%m%d%H%M%S')
                    user_token = hashlib.md5(value_hash).hexdigest()
                    # lưu token lại trong db
                    sql = """insert into user_account_token (user_account_id,token,client_id) 
                              VALUES (%s, %s, %s) RETURNING id"""
                    gs.sql_execute(pool, sql, commit=True, para_values=(user_id, user_token, data.get('client_id')))

                if user_token and user_id and user_role:
                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     "ddToken": user_token,
                                     "userId": user_id,
                                     "userProfileId": new_profile_id,
                                     "userRoleCode": user_role
                                 }
                                 })
                session = self.opentok.create_session(location=u'219.75.27.16')
                session_id = session.session_id
                logger.info('session v3 facebook login tokbox %s' % session_id)
                sql_select_session = '''select * from user_account_session where user_account_id = %s''' %(user_id)
                res_session = gs.sql_execute(pool, sql_select_session)
                if res_session:
                    sql_update_session = '''update user_account_session set session_tokbox = %s where user_account_id = %s'''
                    gs.sql_execute(pool, sql_update_session, commit=True, para_values=(session_id,user_id))
                else :
                    sql_insert_session = '''insert into user_account_session (session_tokbox,user_account_id) values (%s,%s)'''
                    gs.sql_execute(pool, sql_insert_session, commit=True, para_values=(session_id,user_id))
                
        except Exception as e:
            logger.error('error_user_facebook_login %s' % str(e))
            client_sentry.captureException()

        return resp

    def user_facebook_login_hospital_v3(self, data):
        logger.info('start func user_facebook_login_hospital_v3 %s' % data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Login failed"),
                        "msg": _("An error occurred. Please try to login again."),
                        "code": "login_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            if data and data.get('facebook_token', False):
                fb_token = data.get('facebook_token')
                user_token = False
                user_id = False
                user_role = 'patient'
                hospital_id = data.get('hospital_id')
                # Get fb infomation from token
                fb_info = self.get_infomation_facebook(fb_token)
                if fb_info:
                    
                    sql = "select user_account_id from facebook_account fa inner join user_account ua on ua.id = fa.user_account_id inner join patient_profile pp on pp.user_mngment_id = ua.user_mngment_id where fa.facebook_id = %s and pp.hospital_profile_id = %s"
                    fb_acc_exist = gs.sql_execute(pool, sql, para_values=(fb_info['facebook_id'],hospital_id))
                    if fb_acc_exist:
                        user_sql = """select ua.id,
                                        code,
                                        token,
                                        email,
                                        ua.user_mngment_id,
                                        client_id
                                from user_account ua
                                    left join user_account_user_role uaur on uaur.user_account_id = ua.id
                                    left join user_role ur on ur.id = uaur.user_role_id 
                                    left join user_account_token uat on uat.user_account_id = ua.id
                                where ua.id = %s"""

                        user_res = gs.sql_execute(pool, user_sql, para_values=(fb_acc_exist[0][0],))
                        user_token = False
                        user_role = user_res[0][1]
                        user_id = fb_acc_exist[0][0]
                        new_profile_id = user_res[0][4]
                           
                        for line in user_res:
                            if line[5] == data.get('client_id'):
                                user_token = line[2]
                                break
                        if not user_token:
                            value_hash = str(user_id) + datetime.now().strftime('%Y%m%d%H%M%S')
                            user_token = hashlib.md5(value_hash).hexdigest()
                            sql = """insert into user_account_token (user_account_id,token,client_id) 
                                                                                      VALUES (%s, %s, %s) RETURNING id"""
                            gs.sql_execute(pool, sql, commit=True,
                                           para_values=(user_id, user_token, data.get('client_id')))
                        sql_update = '''update user_account 
                                                set validation_status = 'active',
                                                    activity_status = 'online',
                                                    email = %s
                                            where id = %s'''
                        gs.sql_execute(pool, sql_update, commit=True,
                                       para_values=(fb_info['email'] or user_res[0][3], user_id))
                        sql_update2 = "UPDATE facebook_account SET access_token = %s WHERE facebook_id = %s"
                        gs.sql_execute(pool, sql_update2, commit=True, para_values=(fb_token, fb_info['facebook_id']))
                        

                    else:
                        if fb_info['email']:
                            user_sql = """select ua.id,
                                            code,
                                            validation_status,
                                            phone_confirmed,
                                            token,
                                            facebook_id,
                                            ua.user_mngment_id
                                    from user_account ua
                                        left join user_account_user_role uaur on uaur.user_account_id = ua.id
                                        left join user_role ur on ur.id = uaur.user_role_id 
                                        left join user_account_token uat on uat.user_account_id = ua.id
                                        left join facebook_account fa on fa.user_account_id = ua.id
                                        left join patient_profile pp on pp.user_mngment_id = ua.user_mngment_id
                                    where email=%s and pp.hospital_profile_id = %s """
                            user_res = gs.sql_execute(pool, user_sql, para_values=(fb_info['email'],hospital_id))
                        else:
                            user_res = []

                        if user_res:
                            user_token = user_res[0][4]
                            user_role = user_res[0][1]
                            user_id = user_res[0][0]
                            new_profile_id = user_res[0][6]

                        else:
                            sql = '''select id from user_role where code = %s '''
                            role_id = gs.sql_execute(pool, sql, para_values=(user_role,))
                            if role_id:
                                # insert profile to db
                                
                                insert_profile = '''insert into user_mngment (type_profile, 
                                                                              relationship,
                                                                              first_name,
                                                                              last_name,
                                                                              middle_name,
                                                                              birthday,
                                                                              gender,
                                                                              profile_image_url)
                                                    values (%s,'me',%s,%s,%s,%s,%s,%s)
                                                    RETURNING id'''
                                new_profile_id = gs.sql_execute(pool, insert_profile, commit=True,
                                                                para_values=(user_role,
                                                                             fb_info['first_name'],
                                                                             fb_info['last_name'],
                                                                             fb_info['middle_name'],
                                                                             fb_info['birthday'],
                                                                             fb_info['gender'],
                                                                             fb_info['profile_image_url']))[0][0]

                                url = fb_info['profile_image_url']         
                                r = requests.get(url,stream=True)
                                if r.status_code == 200:
                                    aws = aws_management()
                                    f = BytesIO(r.content)
                                    f.seek(0)
                                    filename = 'avatar_fb_ddvnavatar_%s' % new_profile_id
                                    aws.upload_obj(f, AWS_BUCKET, filename)
                                    avatar_url = 'https://' + BUCKET_URL_DEFAULT + '/' + filename
                                    sql_update3 = "UPDATE user_mngment set profile_image_url = %s where id= %s"
                                    gs.sql_execute(pool, sql_update3, commit=True, para_values=(avatar_url,new_profile_id))
                                # insert useraccount to db
                                create_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                                create_user_sql = ("""
                                  insert into user_account (email,
                                                            validation_status,
                                                            activity_status,
                                                            created_date,
                                                            phone_confirmed,
                                                            email_confirmed,
                                                            user_mngment_id
                                                            )
                                  VALUES (%s, 'active','online',%s, 'False', 'False',%s)
                                  RETURNING id
                                """)
                                user_id = gs.sql_execute(pool, create_user_sql, commit=True,
                                                         para_values=(fb_info['email'], create_date, new_profile_id))[
                                    0][0]

                                # insert role-user to db
                                sql_insert_role = ''' insert into user_account_user_role(user_role_id,user_account_id) 
                                                    Values  (%s,%s)
                                                    returning id'''
                                gs.sql_execute(pool, sql_insert_role, commit=True, para_values=(role_id[0][0], user_id))

                                # insert wallet to db
                                sql_insert_wallet = ''' insert into user_wallet(total_balance,available_balance,state,created_date,write_date,user_account_id,unitcode) 
                                                                            Values  (0,0,'active',%s,%s,%s,'vnd')
                                                                            returning id'''
                                gs.sql_execute(pool, sql_insert_wallet, commit=True,
                                               para_values=(create_date, create_date, user_id))

                                # insert profile
                                if user_role == "patient":
                                    sql_insert_profile = '''INSERT INTO patient_profile (user_mngment_id,hospital_profile_id) VALUES (%s,%s)'''
                                elif user_role == "doctor":
                                    sql_insert_profile = '''INSERT INTO doctor_profile (user_mngment_id,hospital_profile_id) VALUES (%s,%s)'''
                                gs.sql_execute(pool, sql_insert_profile, commit=True, para_values=(new_profile_id,hospital_id))
                                # insert user_contact
                                sql_insert_contact = ''' INSERT INTO user_contact(user_mngment_id) VALUES (%s) returning id'''
                                new_id_contact = gs.sql_execute(pool, sql_insert_contact, commit=True,
                                                                para_values=(new_profile_id,))
                                # insert user_address with primary = true
                                sql_insert_address = ''' INSERT INTO user_address(user_contact_id,is_primary) VALUES (%s,'true')
                                                                    returning id'''
                                gs.sql_execute(pool, sql_insert_address, commit=True,
                                               para_values=(new_id_contact[0][0],))
                                # insert user_address with primary = fals
                                if user_role == "patient":
                                    sql_insert_address = ''' INSERT INTO user_address(user_contact_id,is_primary) VALUES  (%s,'false')
                                                                        returning id'''
                                    gs.sql_execute(pool, sql_insert_address, commit=True,
                                                   para_values=(new_id_contact[0][0],))

                            else:
                                resp.update({
                                    'status_code': 401,
                                    'resq_body': {
                                        "code": "no_exist_role",
                                        "title": _("No exist role"),
                                        "msg": _("The role does not exists in our system, please try to register with another one")
                                    }
                                })

                        # insert fb account to db
                        sql_insert_fb = '''insert into facebook_account (access_token,facebook_id,user_account_id)
                                        Values  (%s,%s,%s)'''
                        gs.sql_execute(pool, sql_insert_fb, commit=True,
                                       para_values=(fb_token, fb_info['facebook_id'], user_id))

                if not user_token and user_id:
                    value_hash = str(user_id) + datetime.now().strftime('%Y%m%d%H%M%S')
                    user_token = hashlib.md5(value_hash).hexdigest()
                    # lưu token lại trong db
                    sql = """insert into user_account_token (user_account_id,token,client_id) 
                              VALUES (%s, %s, %s) RETURNING id"""
                    gs.sql_execute(pool, sql, commit=True, para_values=(user_id, user_token, data.get('client_id')))

                if user_token and user_id and user_role:
                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     "ddToken": user_token,
                                     "userId": user_id,
                                     "userProfileId": new_profile_id,
                                     "userRoleCode": user_role
                                 }
                                 })
                session = self.opentok.create_session(location=u'219.75.27.16')
                session_id = session.session_id
                logger.info('session v3 facebook login tokbox %s' % session_id)
                sql_select_session = '''select * from user_account_session where user_account_id = %s''' %(user_id)
                res_session = gs.sql_execute(pool, sql_select_session)
                if res_session:
                    sql_update_session = '''update user_account_session set session_tokbox = %s where user_account_id = %s'''
                    gs.sql_execute(pool, sql_update_session, commit=True, para_values=(session_id,user_id))
                else :
                    sql_insert_session = '''insert into user_account_session (session_tokbox,user_account_id) values (%s,%s)'''
                    gs.sql_execute(pool, sql_insert_session, commit=True, para_values=(session_id,user_id))
                
        except Exception as e:
            logger.error('error_user_facebook_login %s' % str(e))
            client_sentry.captureException()

        return resp


    def authentication(self, data):
        logger.info('start func authentication %s' % data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Login failed"),
                        "msg": _("An error occurred. Please try to login again."),
                        "code": "login_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            if data and data.get('login', False) and data.get('password', False):
                password = data.get('password')
                login = data.get('login')
                version = data.get('version', 1)
                client_side = data.get('client_side', False)
                admin_role = data.get('admin_role', False)
                if version == 2:
                    sql = """select ua.id, --0
                                    code, --1
                                    validation_status, --2
                                    phone_confirmed, --3
                                    token, --4
                                    user_mngment_id, --5
                                    phone --6
                            from user_account ua
                                left join user_account_user_role uaur on uaur.user_account_id = ua.id
                                left join user_role ur on ur.id = uaur.user_role_id 
                                left join user_account_token uat on uat.user_account_id = ua.id
                            where password=%s 
                                and ( lower(email) = %s or case
                                                                when position('+' in %s) > 0 then phone=%s
                                                                else right(phone,6) = right(%s,6) 
                                                            end )"""(login, login, login)
                    res = gs.sql_execute(pool, sql, commit=False, para_values=(password,
                                                                               login.lower(),
                                                                               login.lower(),
                                                                               login.lower(),
                                                                               login.lower()))
                else:
                    sql = """select ua.id, --0
                                    code, --1
                                    validation_status, --2
                                    phone_confirmed, --3
                                    token, --4
                                    user_mngment_id, --5
                                    phone, --6
                                    COALESCE(client_id,'')--7
                            from user_account ua
                                left join user_account_user_role uaur on uaur.user_account_id = ua.id
                                left join user_role ur on ur.id = uaur.user_role_id 
                                left join user_account_token uat on uat.user_account_id = ua.id
                            where password=%s and ( lower(email) = %s or phone=%s )"""
                    res = gs.sql_execute(pool, sql, commit=False, para_values=(password,
                                                                               login.lower(),
                                                                               login.lower()))
                if res:
                    result = res[0]
                    logger.info('userid  %s' % result[0])
                    if client_side:
                        if result[1] == 'doctor' or result[1] == 'hotline':
                            session = self.opentok.create_session(location=u'219.75.27.16')
                            session_id = session.session_id
                            logger.info('session tokbox %s' % session_id)
                            sql_select_session = '''select * from user_account_session where user_account_id = %s''' %(result[0])
                            res_session = gs.sql_execute(pool, sql_select_session)
                            if res_session:
                                sql_update_session = '''update user_account_session set session_tokbox = %s where user_account_id = %s'''
                                gs.sql_execute(pool, sql_update_session, commit=True, para_values=(session_id,result[0]))
                            else :
                                sql_insert_session = '''insert into user_account_session (session_tokbox,user_account_id) values (%s,%s)'''
                                gs.sql_execute(pool, sql_insert_session, commit=True, para_values=(session_id,result[0]))
                            from_user_token = self.opentok.generate_token(session_id)
                            sql_clean_token = '''delete from user_account_token_tokbox where doctor_id = %s''' %(result[0])
                            res_token = gs.sql_execute(pool, sql_clean_token)
                            sql_insert_session = '''insert into user_account_token_tokbox (token_tokbox,doctor_id,user_account_id) values (%s,%s,%s)'''
                            gs.sql_execute(pool, sql_insert_session, commit=True, para_values=(from_user_token,result[0],result[0]))
                        if client_side == 'mobile' and result[1] != 'patient':
                            resp.update({'status_code': 401,
                                         'resq_body': {
                                             "code": "wrong_role",
                                             "title": _("Login failed"),
                                             "msg": _("Your account role is not patient")
                                         }})
                            return resp
                        if client_side == 'website' and (result[1] != 'doctor' and result[1] != 'hotline'):
                            resp.update({'status_code': 401,
                                         'resq_body': {
                                             "code": "wrong_role",
                                             "title": _("Login failed"),
                                             "msg": _("Your account role is not doctor")
                                         }})
                            return resp

                    if admin_role == True and result[1] != 'admin':
                        resp.update({'status_code': 401,
                                     'resq_body': {
                                         "code": "wrong_role",
                                         "title": _("Login failed"),
                                         "msg": _("Your account role is not admin")
                                     }})
                        return resp
                    if '+' not in login and version == 2:
                        sql_meta = '''SELECT code FROM meta_country_phone_code;'''
                        phone_codes = gs.sql_execute(pool, sql_meta)
                        for i in range(len(res)):
                            record = res[i]
                            is_ok = False
                            for code in phone_codes:
                                check_login = '%s%s' % (code[0], login[1:])
                                if check_login == record[6]:
                                    is_ok = True
                                    break
                            if is_ok == True:
                                result = record
                                break

                    if result[2] == 'inactive' and result[3] in (False, None):
                        resp.update({'status_code': 401,
                                     'resq_body': {
                                         "code": "not_activated",
                                         "title": _("Login failed"),
                                         "userId": result[0],
                                         "userProfileId": result[5],
                                         "msg": _("Your account is not activated by SMS")
                                     }
                                     })
                    else:
                        token = False
                        for l in res:
                            if l[4] and l[7] == data.get('client_id'):
                                token = l[4]
                                break

                        if not token:
                            value_hash = str(result[0]) + datetime.now().strftime('%Y%m%d%H%M%S')
                            token = hashlib.md5(value_hash).hexdigest()
                            # lưu token lại trong db
                            sql = """
                                      insert into user_account_token (user_account_id,token,client_id)
                                      VALUES (%s, %s, %s)
                                      RETURNING id
                                    """
                            gs.sql_execute(pool, sql, commit=True, para_values=(result[0], token, data.get('client_id')))
                            sql_update = '''update user_account set activity_status = 'offline' where id = %s'''
                            gs.sql_execute(pool, sql_update, commit=True, para_values=(result[0],))
                        if result[1] == 'doctor' or result[1] == 'hotline' :
                            
                            resp.update({'status_code': 200,
                                        'resq_body': {
                                            "ddToken": token,
                                            "userId": result[0],
                                            "userProfileId": result[5],
                                            "userRoleCode": result[1],
                                            "sessionId": session_id,
                                            "tokenTokbox": from_user_token
                                        }
                                        })
                        else :
                            resp.update({'status_code': 200,
                                     'resq_body': {
                                         "ddToken": token,
                                         "userId": result[0],
                                         "userProfileId": result[5],
                                         "userRoleCode": result[1]
                                     }
                                     })
                else:
                    resp.update({'status_code': 401,
                                 'resq_body':
                                     {
                                         "code": "wrong_info",
                                         "title": _("Login failed"),
                                         "msg": _("Wrong email or password, please try to login again")
                                     }
                                 })
        except Exception as e:
            logger.error('error_authentication %s' % str(e))
            client_sentry.captureException()
        return resp

    def authentication_hospital(self, data):
        logger.info('start func authentication hospital %s' % data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Login failed"),
                        "msg": _("An error occurred. Please try to login again."),
                        "code": "login_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            hospital_id = data.get('hospital_id')
            if data and data.get('login', False) and data.get('password', False):
                password = data.get('password')
                login = data.get('login')
                version = data.get('version', 1)
                client_side = data.get('client_side', False)
                admin_role = data.get('admin_role', False)
                sql = """select ua.id, --0
                                code, --1
                                validation_status, --2
                                phone_confirmed, --3
                                token, --4
                                ua.user_mngment_id, --5
                                phone, --6
                                COALESCE(client_id,''),--7
								dp.hospital_profile_id as dp_hospital_id,
								pp.hospital_profile_id as pp_hospital_id
                        from user_account ua
                            left join user_account_user_role uaur on uaur.user_account_id = ua.id
                            left join user_role ur on ur.id = uaur.user_role_id 
                            left join user_account_token uat on uat.user_account_id = ua.id
                            left join doctor_profile dp on dp.user_mngment_id = ua.user_mngment_id
                            left join patient_profile pp on pp.user_mngment_id = ua.user_mngment_id
                        where password=%s and ( lower(email) = %s or phone=%s ) and (dp.hospital_profile_id = %s or pp.hospital_profile_id = %s) """
                res = gs.sql_execute(pool, sql, commit=False, para_values=(password,
                                                                            login.lower(),
                                                                            login.lower(), hospital_id, hospital_id))
                if res:
                    result = res[0]
                    if client_side:
                        if client_side == 'mobile' and result[1] != 'patient':
                            resp.update({'status_code': 401,
                                         'resq_body': {
                                             "code": "wrong_role",
                                             "title": _("Login failed"),
                                             "msg": _("Your account role is not patient")
                                         }})
                            return resp
                        if client_side == 'website' and (result[1] != 'doctor' and  result[1] != 'hotline'):
                            resp.update({'status_code': 401,
                                         'resq_body': {
                                             "code": "wrong_role",
                                             "title": _("Login failed"),
                                             "msg": _("Your account role is not doctor")
                                         }})
                            return resp

                    if admin_role == True and result[1] != 'admin':
                        resp.update({'status_code': 401,
                                     'resq_body': {
                                         "code": "wrong_role",
                                         "title": _("Login failed"),
                                         "msg": _("Your account role is not admin")
                                     }})
                        return resp
                    if result[2] == 'inactive' and result[3] in (False, None):
                        resp.update({'status_code': 401,
                                     'resq_body': {
                                         "code": "not_activated",
                                         "title": _("Login failed"),
                                         "userId": result[0],
                                         "userProfileId": result[5],
                                         "msg": _("Your account is not activated by SMS")
                                     }
                                     })
                    else:
                        token = False
                        for l in res:
                            if l[4] and l[7] == data.get('client_id'):
                                token = l[4]
                                break

                        if not token:
                            value_hash = str(result[0]) + datetime.now().strftime('%Y%m%d%H%M%S')
                            token = hashlib.md5(value_hash).hexdigest()
                            # lưu token lại trong db
                            sql = """
                                      insert into user_account_token (user_account_id,token,client_id)
                                      VALUES (%s, %s, %s)
                                      RETURNING id
                                    """
                            gs.sql_execute(pool, sql, commit=True, para_values=(result[0], token, data.get('client_id')))
                            sql_update = '''update user_account set activity_status = 'offline' where id = %s'''
                            gs.sql_execute(pool, sql_update, commit=True, para_values=(result[0],))

                        resp.update({'status_code': 200,
                                     'resq_body': {
                                         "ddToken": token,
                                         "userId": result[0],
                                         "userProfileId": result[5],
                                         "userRoleCode": result[1]
                                     }
                                     })
                else:
                    resp.update({'status_code': 401,
                                 'resq_body':
                                     {
                                         "code": "wrong_info",
                                         "title": _("Login failed"),
                                         "msg": _("Wrong email or password, please try to login again")
                                     }
                                 })
        except Exception as e:
            logger.error('error_authentication %s' % str(e))
            client_sentry.captureException()
        return resp
    
    
    def authentication_hospital_v3(self, data):
        logger.info('start func v3 authentication hospital %s' % data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Login failed"),
                        "msg": _("An error occurred. Please try to login again."),
                        "code": "login_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            hospital_id = data.get('hospital_id')
            if data and data.get('login', False) and data.get('password', False):
                password = data.get('password')
                login = data.get('login')
                version = data.get('version', 1)
                client_side = data.get('client_side', False)
                admin_role = data.get('admin_role', False)
                sql = """select ua.id, --0
                                code, --1
                                validation_status, --2
                                phone_confirmed, --3
                                token, --4
                                ua.user_mngment_id, --5
                                phone, --6
                                COALESCE(client_id,''),--7
								dp.hospital_profile_id as dp_hospital_id,
								pp.hospital_profile_id as pp_hospital_id
                        from user_account ua
                            left join user_account_user_role uaur on uaur.user_account_id = ua.id
                            left join user_role ur on ur.id = uaur.user_role_id 
                            left join user_account_token uat on uat.user_account_id = ua.id
                            left join doctor_profile dp on dp.user_mngment_id = ua.user_mngment_id
                            left join patient_profile pp on pp.user_mngment_id = ua.user_mngment_id
                        where password=%s and ( lower(email) = %s or phone=%s ) and (dp.hospital_profile_id = %s or pp.hospital_profile_id = %s) """
                res = gs.sql_execute(pool, sql, commit=False, para_values=(password,
                                                                            login.lower(),
                                                                            login.lower(), hospital_id, hospital_id))
                if res:
                    result = res[0]
                    logger.info('userid  %s' % result[0])
                    if client_side:
                        session = self.opentok.create_session(location=u'219.75.27.16')
                        session_id = session.session_id
                        logger.info('session v3 tokbox %s' % session_id)
                        sql_select_session = '''select * from user_account_session where user_account_id = %s''' %(result[0])
                        res_session = gs.sql_execute(pool, sql_select_session)
                        if res_session:
                            sql_update_session = '''update user_account_session set session_tokbox = %s where user_account_id = %s'''
                            gs.sql_execute(pool, sql_update_session, commit=True, para_values=(session_id,result[0]))
                        else :
                            sql_insert_session = '''insert into user_account_session (session_tokbox,user_account_id) values (%s,%s)'''
                            gs.sql_execute(pool, sql_insert_session, commit=True, para_values=(session_id,result[0]))
                        #from_user_token = self.opentok.generate_token(session_id)
                        #sql_clean_token = '''delete from user_account_token_tokbox where doctor_id = %s''' %(result[0])
                        #res_token = gs.sql_execute(pool, sql_clean_token)
                        #sql_insert_session = '''insert into user_account_token_tokbox (token_tokbox,doctor_id,user_account_id) values (%s,%s,%s)'''
                        #gs.sql_execute(pool, sql_insert_session, commit=True, para_values=(from_user_token,result[0],result[0]))
                    
                        if client_side == 'mobile' and result[1] != 'patient':
                            resp.update({'status_code': 401,
                                         'resq_body': {
                                             "code": "wrong_role",
                                             "title": _("Login failed"),
                                             "msg": _("Your account role is not patient")
                                         }})
                            return resp
                        if client_side == 'website' and (result[1] != 'doctor' and  result[1] != 'hotline'):
                            resp.update({'status_code': 401,
                                         'resq_body': {
                                             "code": "wrong_role",
                                             "title": _("Login failed"),
                                             "msg": _("Your account role is not doctor")
                                         }})
                            return resp

                    if admin_role == True and result[1] != 'admin':
                        resp.update({'status_code': 401,
                                     'resq_body': {
                                         "code": "wrong_role",
                                         "title": _("Login failed"),
                                         "msg": _("Your account role is not admin")
                                     }})
                        return resp
                    if result[2] == 'inactive' and result[3] in (False, None):
                        resp.update({'status_code': 401,
                                     'resq_body': {
                                         "code": "not_activated",
                                         "title": _("Login failed"),
                                         "userId": result[0],
                                         "userProfileId": result[5],
                                         "msg": _("Your account is not activated by SMS")
                                     }
                                     })
                    else:
                        token = False
                        for l in res:
                            if l[4] and l[7] == data.get('client_id'):
                                token = l[4]
                                break

                        if not token:
                            value_hash = str(result[0]) + datetime.now().strftime('%Y%m%d%H%M%S')
                            token = hashlib.md5(value_hash).hexdigest()
                            # lưu token lại trong db
                            sql = """
                                      insert into user_account_token (user_account_id,token,client_id)
                                      VALUES (%s, %s, %s)
                                      RETURNING id
                                    """
                            gs.sql_execute(pool, sql, commit=True, para_values=(result[0], token, data.get('client_id')))
                            sql_update = '''update user_account set activity_status = 'offline' where id = %s'''
                            gs.sql_execute(pool, sql_update, commit=True, para_values=(result[0],))

                        resp.update({'status_code': 200,
                                     'resq_body': {
                                         "ddToken": token,
                                         "userId": result[0],
                                         "userProfileId": result[5],
                                         "userRoleCode": result[1]
                                     }
                                     })
                else:
                    resp.update({'status_code': 401,
                                 'resq_body':
                                     {
                                         "code": "wrong_info",
                                         "title": _("Login failed"),
                                         "msg": _("Wrong email or password, please try to login again")
                                     }
                                 })
                return resp
        except Exception as e:
            logger.error('error_authentication %s' % str(e))
            client_sentry.captureException()
        return resp

    def user_logout(self, data):
        logger.info('user_logout %s' % data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Logout failed"),
                        "msg": _("An error occurred. Please try to logout again."),
                        "code": "logout_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            if data:
                user_token = data.get('user_token')
                sql = """select uat.id token_id,
                                ua.id user_id,
                                token,
                                validation_status
                        from user_account ua
                            left join user_account_token uat on uat.user_account_id = ua.id
                        where token = %s """
                res = gs.sql_execute(pool, sql, para_values=[user_token])
                if res:
                    user_id = res[0][1]
                    sql_delete = '''delete from user_account_token where user_account_id = %s'''
                    gs.sql_execute(pool, sql_delete, commit=True, para_values=[user_id])
                    sql_update = '''update user_account set activity_status = 'offline' where id = %s'''
                    gs.sql_execute(pool, sql_update, commit=True, para_values=[user_id])
                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     "code": "success",
                                     "title": _("Success"),
                                     "msg": _("Logout Successful.")
                                 }
                                 })
                else:
                    resp.update({'status_code': 203,
                                 'resq_body': {
                                     "code": "expired_token",
                                     "title": _("Expired token"),
                                     "msg": _("Your login-token is expired, please try to login again")
                                 }
                                 })
        except Exception as e:
            logger.error('error_user_logout %s' % str(e))
            client_sentry.captureException()

        return resp

    def create_user_account(self, data):
        logger.info('create_user_account %s' % data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "register_failed",
                        "title": _("Register failed"),
                        "msg": _("An error occurred. Please try to register again.")
                    }
                }
        pwd = data.get('password','')
        if self.general_business.isPwdValid(pwd) == False:
            resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "bad_password",
	                    "title": _("Register failed"),
	                    "msg": _("The password requires 6 charaters with at least 1 capital and 1 number")
                    }
                }
            return resp
        try:
            gs = self.general_business
            pool = self.pool_conn

            # Check trung email/phone
            sql = """select id,email,phone,validation_status from user_account where email=%s or phone =%s """
            check_exist = gs.sql_execute(pool, sql, para_values=(data.get('email',''), data.get('phoneNumber','')))
            if check_exist:
                if check_exist[0][3] == "active":
                    resp.update({
                            'status_code': 401,
                            'resq_body': {
                                "code": "exist_email_and_phone",
                                "title": _("Register failed"),
                                "msg": _("The email and phone number already exists in our system, please try to register with another one")
                            }
                        })
                    return resp
                else:
                    sql_update = "update user_account set password = %s where id = %s"
                    gs.sql_execute(pool, sql_update, commit=True, para_values=(pwd, check_exist[0][0]))
                    resp.update({
                            'status_code': 401,
                            'resq_body': {
                                "code": "not_activated",
                                "title": _("Register failed"),
	                            "msg": _("Your account is not activated"),
                                "userId": check_exist[0][0]
                            }
                        })
                    return resp
            else:
                # check role
                sql = '''select id from user_role where code = %s '''
                role_id = gs.sql_execute(pool, sql, para_values=[data['userRoleCode'].strip()])
                if role_id:
                    sms_opt = random.sample(range(1000, 9999), 1)

                    create_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    sms_expiry_time = (datetime.now() + relativedelta(minutes=10)).strftime('%Y-%m-%d %H:%M:%S')

                    # insert profile to db
                    insert_profile = '''insert into user_mngment (type_profile, relationship)
                                        values (%s,'me')
                                        RETURNING id'''
                    new_profile_id = \
                        gs.sql_execute(pool, insert_profile, commit=True, para_values=[data['userRoleCode']])[0][0]

                    # insert user account to db
                    sql = """
                            insert into user_account (email,
                                                        phone,
                                                        password,
                                                        validation_status,
                                                        activity_status,
                                                        created_date,
                                                        phone_confirmed,
                                                        email_confirmed,
                                                        phone_confirm_token,
                                                        phone_confirm_token_expiry_time,
                                                        user_mngment_id
                                                        )
                            VALUES (%s, %s, %s, 'inactive','offline',%s, 'False', 'False',%s, %s, %s)
                            RETURNING id
                            """
                    new_id = gs.sql_execute(pool, sql, commit=True, para_values=(data['email'],
                                                                                data['phoneNumber'],
                                                                                data['password'],
                                                                                create_date,
                                                                                sms_opt[0],
                                                                                sms_expiry_time,
                                                                                new_profile_id))

                    # insert role-user to db
                    sql_insert_role = ''' insert into user_account_user_role(user_role_id,user_account_id) 
                                    Values  (%s,%s)
                                    returning id'''
                    gs.sql_execute(pool, sql_insert_role, commit=True, para_values=(role_id[0][0], new_id[0][0]))

                    # insert wallet to db
                    sql_insert_wallet = ''' insert into user_wallet(total_balance,available_balance,state,created_date,write_date,user_account_id,unitcode) 
                                            Values  (0,0,'active',%s,%s,%s,'vnd')
                                            returning id'''
                    gs.sql_execute(pool, sql_insert_wallet, commit=True,
                                para_values=(create_date, create_date, new_id[0][0]))

                    # insert profile
                    if data['userRoleCode'] == "patient":
                        sql_insert_profile = '''INSERT INTO patient_profile (user_mngment_id) VALUES (%s)'''
                    elif data['userRoleCode'] == "doctor":
                        sql_insert_profile = '''INSERT INTO doctor_profile (user_mngment_id) VALUES (%s)'''
                    gs.sql_execute(pool, sql_insert_profile, commit=True, para_values=(new_profile_id,))
                    # insert user_contact
                    sql_insert_contact = ''' INSERT INTO user_contact(user_mngment_id) VALUES (%s) returning id'''
                    new_id_contact = gs.sql_execute(pool, sql_insert_contact, commit=True,
                                                    para_values=(new_profile_id,))
                    # insert user_address with primary = true
                    sql_insert_address = ''' INSERT INTO user_address(user_contact_id,is_primary) 
                                    VALUES  (%s,'true')
                                    returning id'''
                    gs.sql_execute(pool, sql_insert_address, commit=True, para_values=(new_id_contact[0][0],))
                    # insert user_address with primary = fals
                    if data['userRoleCode'] == "patient":
                        sql_insert_address = ''' INSERT INTO user_address(user_contact_id,is_primary) VALUES  (%s,'false')
                                        returning id'''
                        gs.sql_execute(pool, sql_insert_address, commit=True, para_values=(new_id_contact[0][0],))
                    # Sent OPT SMS
                    # twilio_config = json.loads(config.get('environment', 'twilio_config'))
                    # account_sid = twilio_config['account_sid']
                    # auth_token = twilio_config['auth_token']
                    # phone_to = twilio_config['phone_to']
                    # messaging_service_sid = twilio_config['messaging_service_sid']
                    # client = twilio_Client(account_sid, auth_token)
                    # try:
                    #     client.messages.create(
                    #         # from_ = phone_to,
                    #         messaging_service_sid=messaging_service_sid,
                    #         to=data['phoneNumber'],
                    #         body="Your DoctorDoctor activate code is: %s" % sms_opt[0]
                    #     )
                    # except Exception as e:
                    #     logger.error('error_send_sms %s - %s - %s' % (data['phoneNumber'], sms_opt[0],e))
                    #     client_sentry.captureException()

                    resp.update({'status_code': 200,
                                'resq_body': {
                                    "userId": new_id[0][0],
                                }
                                })
                else:
                    resp.update({
                        'status_code': 401,
                        'resq_body': {
                            "code": "no_exist_role",
                            "title": _("No exist role"),
                            "msg": _("The role does not exists in our system, please try to register with another one")
                        }
                    })

        except Exception as e:
            logger.error('error_create_user_account %s' % str(e))
            client_sentry.captureException()

        return resp

    # WD2.12 API for doctor to register new account including his profile
    def create_user_account_v2(self, data):
        logger.info('create_user_account_v2 %s' % data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "register_failed",
                        "title": _("Register failed"),
                        "msg": _("An error occurred. Please try to register again.")
                    }
                }
        pwd = data.get('password','')
        if self.general_business.isPwdValid(pwd) == False:
            resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "bad_password",
	                    "title": _("Register failed"),
	                    "msg": _("The password requires 6 charaters with at least 1 capital and 1 number")
                    }
                }
            return resp
        try:
            gs = self.general_business
            pool = self.pool_conn
            doctor_profile = self.doctor_profile

            # Check trung email/phone
            sql = """select id,email,phone from user_account where email=%s or phone =%s """
            check_exist = gs.sql_execute(pool, sql, para_values=(data['email'], data['phoneNumber']))
            if check_exist:
                if check_exist[0][1] == data['email'].strip():
                    resp.update({
                        'status_code': 401,
                        'resq_body': {
                            "code": "exist_email",
                            "title": _("Register failed"),
                            "msg": _("The email already exists in our system, please try to register with another one")
                        }
                    })
                elif check_exist[0][2] == data['phoneNumber'].strip():
                    resp.update({
                        'status_code': 401,
                        'resq_body': {
                            "code": "exist_phone",
                            "title": _("Register failed"),
                            "msg": _("The phone already exists in our system, please try to register with another one")
                        }
                    })
            else:
                # check role
                sql = '''select id from user_role where code = %s '''
                role_id = gs.sql_execute(pool, sql, para_values=[data['userRoleCode'].strip()])
                if role_id:
                    create_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    # insert profile to db
                    insert_profile = '''insert into user_mngment (type_profile, relationship)
                                        values (%s,'me')
                                        RETURNING id'''
                    new_profile_id = \
                        gs.sql_execute(pool, insert_profile, commit=True, para_values=[data['userRoleCode']])[0][0]

                    # insert user account to db
                    sql = """
                              insert into user_account (email,
                                                        phone,
                                                        password,
                                                        validation_status,
                                                        activity_status,
                                                        created_date,
                                                        phone_confirmed,
                                                        email_confirmed,
                                                        user_mngment_id
                                                        )
                              VALUES (%s, %s, %s, 'inactive','offline',%s, 'False', 'False', %s)
                              RETURNING id
                            """
                    new_id = gs.sql_execute(pool, sql, commit=True, para_values=(data['email'],
                                                                                 data['phoneNumber'],
                                                                                 data['password'],
                                                                                 create_date,
                                                                                 new_profile_id))

                    # insert role-user to db
                    sql_insert_role = ''' insert into user_account_user_role(user_role_id,user_account_id) 
                                    Values  (%s,%s)
                                    returning id'''
                    gs.sql_execute(pool, sql_insert_role, commit=True, para_values=(role_id[0][0], new_id[0][0]))

                    # insert wallet to db
                    sql_insert_wallet = ''' insert into user_wallet(total_balance,available_balance,state,created_date,write_date,user_account_id,unitcode) 
                                            Values  (0,0,'active',%s,%s,%s,'vnd')
                                            returning id'''
                    gs.sql_execute(pool, sql_insert_wallet, commit=True,
                                   para_values=(create_date, create_date, new_id[0][0]))

                    # insert profile
                    if data['userRoleCode'] == "patient":
                        sql_insert_profile = '''INSERT INTO patient_profile (user_mngment_id) VALUES (%s)'''
                    elif data['userRoleCode'] == "doctor":
                        sql_insert_profile = '''INSERT INTO doctor_profile (user_mngment_id) VALUES (%s)'''
                    gs.sql_execute(pool, sql_insert_profile, commit=True, para_values=(new_profile_id,))
                    # insert user_contact
                    sql_insert_contact = ''' INSERT INTO user_contact(user_mngment_id) VALUES (%s) returning id'''
                    new_id_contact = gs.sql_execute(pool, sql_insert_contact, commit=True,
                                                    para_values=(new_profile_id,))
                    # insert user_address with primary = true
                    sql_insert_address = ''' INSERT INTO user_address(user_contact_id,is_primary) 
                                    VALUES  (%s,'true')
                                    returning id'''
                    gs.sql_execute(pool, sql_insert_address, commit=True, para_values=(new_id_contact[0][0],))
                    # insert user_address with primary = fals
                    if data['userRoleCode'] == "patient":
                        sql_insert_address = ''' INSERT INTO user_address(user_contact_id,is_primary) VALUES  (%s,'false')
                                        returning id'''
                        gs.sql_execute(pool, sql_insert_address, commit=True, para_values=(new_id_contact[0][0],))

                    all_ok = True
                    for type in ['basic', 'other','consultation']:
                        data.update({'setting_type': type})
                        res = doctor_profile.profile_doctor_setup(data,{'user_id':new_id[0][0],
                                                                        'new_profile_id': new_profile_id,
                                                                        'no_token': True})
                        if 'status_code' in res and res['status_code'] != 200:
                            all_ok = False
                            break
                    if all_ok == True:
                        resp.update({'status_code': 200,
                                     'resq_body': {
                                         "userId": new_id[0][0],
                                     }
                                     })
                else:
                    resp.update({
                        'status_code': 401,
                        'resq_body': {
                            "code": "no_exist_role",
                            "title": _("No exist role"),
                            "msg": _("The role does not exists in our system, please try to register with another one")
                        }
                    })

        except Exception as e:
            logger.error('error_create_user_account_v2 %s' % str(e))
            client_sentry.captureException()

        return resp

    
    def create_user_account_v2_hospital(self, data):
        logger.info('create_user_account_v2 hospital %s' % data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "register_failed",
                        "title": _("Register Failed"),
                        "msg": _("An error occurred. Please try to register again.")
                    }
                }
        pwd = data.get('password','')
        if self.general_business.isPwdValid(pwd) == False:
            resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "bad_password",
	                    "title": _("Register Failed"),
	                    "msg": _("Bad Password")
                    }
                }
            return resp
        try:
            gs = self.general_business
            pool = self.pool_conn
            doctor_profile = self.doctor_profile
            hospital_id = data.get('hospital_id')
            # Check trung email/phone
            sql = ''' select ua.id,ua.email,ua.phone,ua.validation_status, CASE WHEN ua.validation_status like %s THEN True ELSE False END as valid_item from user_account ua left join doctor_profile dp on dp.user_mngment_id = ua.user_mngment_id left join patient_profile pp on pp.user_mngment_id = ua.user_mngment_id where (ua.email like %s or ua.phone like %s) and (dp.hospital_profile_id = %s or pp.hospital_profile_id = %s)'''
            check_exist = gs.sql_execute(pool, sql, para_values=["active%","%" + data['email'].strip() + "%", "%" + data['phoneNumber'].strip() + "%", hospital_id, hospital_id])
            logger.info('create_user_account_v2 hospital exist data %s' % check_exist)
            if check_exist:
                exist_email = False
                exist_phone = False
                exist_id = 0
                
                exist_status_email = False
                exist_status_phone = False
                for checkItem in check_exist:
                    if str(checkItem[1]) == str(data['email'].strip()):
                        exist_email = True
                        exist_id = checkItem[0]
                        if checkItem[4]:
                            exist_status_email = checkItem[4]
                        exist_status_email = checkItem[4]
                    if str(checkItem[2]) == str(data['phoneNumber'].strip()):
                        exist_phone = True
                        exist_id = checkItem[0]
                        if checkItem[4]:
                            exist_status_phone = checkItem[4]
                
                if (exist_email or exist_phone) and  (exist_status_email or exist_status_phone):
                    logger.info('create_user_account_v2 hospital exist status active email %s' % exist_status_email)
                
                    logger.info('create_user_account_v2 hospital exist status active phone %s' % exist_status_phone)
                    if exist_email and exist_status_email:     
                        resp.update({
                            'status_code': 401,
                            'resq_body': {
                                "code": "exist_email",
                                "title": _("Register Failed"),
                                "msg": _("Email Exist Register Hospital")
                            }
                        })
                    elif exist_phone and exist_status_phone:
                        resp.update({
                            'status_code': 401,
                            'resq_body': {
                                "code": "exist_phone",
                                "title": _("Register Failed"),
                                "msg": _("Phone Exist Register Hospital")
                            }
                        })
                elif not  (exist_status_email or exist_status_phone):
                    
                    logger.info('create_user_account_v2 hospital else inactive')
                    sql = """ update user_account set email = %s, phone = %s, password = %s where id = %s
                            """
                    gs.sql_execute(pool, sql, commit=True, para_values=(data['email'],data['phoneNumber'],
                                                                                 data['password'],
                                                                                 exist_id))
                    resp.update({'status_code': 200,
                                     'resq_body': {
                                         "userId": exist_id
                                     }
                                })
                else :
                    
                    logger.info('create_user_account_v2 hospital exist status email %s' % exist_status_email)
                    
                    logger.info('create_user_account_v2 hospital exist status phone %s' % exist_status_phone)
                    logger.info('create_user_account_v2 hospital exist email %s' % exist_email)
                    logger.info('create_user_account_v2 hospital exist phone %s' % exist_phone)
                    logger.info('create_user_account_v2 hospital exist id %s ' % exist_id)
                    logger.info('create_user_account_v2 hospital exist status %s' % exist_status)
                
            else:
                # check role
                sql = '''select id from user_role where code = %s '''
                role_id = gs.sql_execute(pool, sql, para_values=[data['userRoleCode'].strip()])
                if role_id:
                    create_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    # insert profile to db
                    insert_profile = '''insert into user_mngment (type_profile, relationship)
                                        values (%s,'me')
                                        RETURNING id'''
                    new_profile_id = \
                        gs.sql_execute(pool, insert_profile, commit=True, para_values=[data['userRoleCode']])[0][0]

                    # insert user account to db
                    sql = """
                              insert into user_account (email,
                                                        phone,
                                                        password,
                                                        validation_status,
                                                        activity_status,
                                                        created_date,
                                                        phone_confirmed,
                                                        email_confirmed,
                                                        user_mngment_id
                                                        )
                              VALUES (%s, %s, %s, 'inactive','offline',%s, 'False', 'False', %s)
                              RETURNING id
                            """
                    new_id = gs.sql_execute(pool, sql, commit=True, para_values=(data['email'],
                                                                                 data['phoneNumber'],
                                                                                 data['password'],
                                                                                 create_date,
                                                                                 new_profile_id))

                    # insert role-user to db
                    sql_insert_role = ''' insert into user_account_user_role(user_role_id,user_account_id) 
                                    Values  (%s,%s)
                                    returning id'''
                    gs.sql_execute(pool, sql_insert_role, commit=True, para_values=(role_id[0][0], new_id[0][0]))

                    # insert wallet to db
                    sql_insert_wallet = ''' insert into user_wallet(total_balance,available_balance,state,created_date,write_date,user_account_id,unitcode) 
                                            Values  (0,0,'active',%s,%s,%s,'vnd')
                                            returning id'''
                    gs.sql_execute(pool, sql_insert_wallet, commit=True,
                                   para_values=(create_date, create_date, new_id[0][0]))

                    # insert profile
                    if data['userRoleCode'] == "patient":
                        sql_insert_profile = '''INSERT INTO patient_profile (user_mngment_id,hospital_profile_id) VALUES (%s,%s)'''
                    elif data['userRoleCode'] == "doctor":
                        sql_insert_profile = '''INSERT INTO doctor_profile (user_mngment_id,hospital_profile_id) VALUES (%s,%s)'''
                    gs.sql_execute(pool, sql_insert_profile, commit=True, para_values=(new_profile_id,hospital_id))
                    # insert user_contact
                    sql_insert_contact = ''' INSERT INTO user_contact(user_mngment_id) VALUES (%s) returning id'''
                    new_id_contact = gs.sql_execute(pool, sql_insert_contact, commit=True,
                                                    para_values=(new_profile_id,))
                    # insert user_address with primary = true
                    sql_insert_address = ''' INSERT INTO user_address(user_contact_id,is_primary) 
                                    VALUES  (%s,'true')
                                    returning id'''
                    gs.sql_execute(pool, sql_insert_address, commit=True, para_values=(new_id_contact[0][0],))
                    # insert user_address with primary = fals
                    if data['userRoleCode'] == "patient":
                        sql_insert_address = ''' INSERT INTO user_address(user_contact_id,is_primary) VALUES  (%s,'false')
                                        returning id'''
                        gs.sql_execute(pool, sql_insert_address, commit=True, para_values=(new_id_contact[0][0],))

                    all_ok = True
                    #for type in ['basic', 'other','consultation']:
                    #    data.update({'setting_type': type})
                    #    res = doctor_profile.profile_doctor_setup(data,{'user_id':new_id[0][0],
                    #                                                    'new_profile_id': new_profile_id,
                    #                                                    'no_token': True})
                    #    if 'status_code' in res and res['status_code'] != 200:
                    #        all_ok = False
                    #        break
                    if all_ok == True:
                        resp.update({'status_code': 200,
                                     'resq_body': {
                                         "userId": new_id[0][0],
                                     }
                                     })
                else:
                    resp.update({
                        'status_code': 401,
                        'resq_body': {
                            "code": "register_failed",
                            "title": _("Register Failed"),
                            "msg": _("An error occurred. Please try to register again.")
                        }
                    })

        except Exception as e:
            logger.error('error_create_user_account_v2 hospital %s' % str(e))
            client_sentry.captureException()

        return resp

    def user_register_v2_editprofile(self, data):
        logger.info('user_register_v2_editprofile %s' % data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "edit_profile_failed",
                        "title": _("Edit profile failed"),
                        "msg": _("An error occurred. Please try to request again.")
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            doctor_profile = self.doctor_profile

            sql = """SELECT validation_status, user_mngment_id FROM user_account where id = %s """
            user_mngment_id = gs.sql_execute(pool, sql, para_values=[data['userId']])[0]
            if user_mngment_id:
                if user_mngment_id[0] == 'active':
                    resp = {'status_code': 401,
                            'resq_body':
                                {
                                    "code": "activated_account",
                                	"msg": _("The account is already activated"),
                                    "title": _("Account is activated")
                                }
                            }
                    return resp
                all_ok = True
                for type in ['basic', 'other']:
                    data.update({'setting_type': type})
                    res = doctor_profile.profile_doctor_setup(data,{'user_id':data['userId'],
                                                                    'new_profile_id': user_mngment_id[1],
                                                                    'no_token': True})
                    sql = """update user_account set phone = %s where id = %s"""
                    res_updatephone = gs.sql_execute(pool, sql, para_values=(data['phoneNumber'],data['userId']))                                                    
                    
                    if 'status_code' in res and res['status_code'] != 200:
                        all_ok = False
                        break
                if all_ok == True:
                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     "code": "success",
                                     "title": _("Success"),
                                     "msg": _("Success")
                                 }
                                 })
        except Exception as e:
            logger.error('user_register_v2_editprofile %s' % str(e))
            client_sentry.captureException()

        return resp

    def user_register_v2_uploaddocs(self, data):
        logger.info('user_register_v2_uploaddocs %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body': {
                    "code": "uploaddocs_failed",
                    "title": _("Upload documents failed"),
                    "msg": _("An error occurred. Please try to request again.")
                }}
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_id = data['user_id']
            create_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            files = []
            for file in data['list_file']:
                sql_insert = '''insert into attachment(create_date,
                                                          write_date,
                                                          create_uid,
                                                          write_uid,
                                                          res_model,
                                                          res_id,
                                                          file_name,
                                                          file_extension,
                                                          file_size,
                                                          url)
                                    values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
                                    returning id
                                    '''
                id = gs.sql_execute(pool, sql_insert, commit=True, para_values=(create_date,
                                                                           create_date,
                                                                           user_id,
                                                                           user_id,
                                                                           'user_account',
                                                                           user_id,
                                                                           file['file_name'],
                                                                           file['file_extension'],
                                                                           file['file_size'],
                                                                           file['url'],
                                                                           ))[0][0]
                files.append({'id': id,
                              'url': file['url']})

            resp.update({'status_code': 200,
                         'resq_body': {
                             "files": files
                         }
                         })

        except Exception as e:
            logger.error('error_user_register_v2_uploaddocs %s' % str(e))
            client_sentry.captureException()
        return resp

    def active_user_account_sms(self, data):
        logger.info('active_user_account_sms %s' % data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "activate_failed",
                        "title": _("Activate failed"),
                        "msg": _("An error occurred. Please try to activate your account again.")
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            sms_opt = data.get('sms_opt')
            user_id = data.get('user_id')
            sql = """select id,
                        phone_confirm_token,
                        phone_confirm_token_expiry_time 
                    from user_account where id = %s """
            res = gs.sql_execute(pool, sql, para_values=[user_id])
            if res:
                if res[0][1] == str(sms_opt):
                    if res[0][2] >= datetime.now():
                        sql_update = ''' update user_account set validation_status = 'active',
                                            phone_confirmed = True where id = %s'''
                        gs.sql_execute(pool, sql_update, commit=True, para_values=[user_id])
                        resp.update({
                            'status_code': 200,
                            'resq_body': {
                                "code": "success",
                                "title": _("Success"),
                                "msg": _("Success")
                            }
                        })
                    else:
                        resp.update({
                            'status_code': 401,
                            'resq_body': {
                                "code": "otp_code_expired",
                                "title": _("OTP code is expired"),
                                "msg": _("OTP code is expired. Please try to request new OTP code!")
                            }
                        })
                else:
                    resp.update({
                        'status_code': 401,
                        'resq_body': {
                            "code": "wrong_info",
                            "title": _("Login failed"),
                            "msg": _("Incorrect code")
                        }
                    })

            else:
                resp.update({
                    'status_code': 401,
                    'resq_body': {
                        "code": "no_exist_user",
                        "title": _("No exist user"),
                        "msg": _("The user does not exists in our system, please try to register with another one")
                    }
                })
        except Exception as e:
            logger.error('error_active_user_account_sms %s' % str(e))
            client_sentry.captureException()

        return resp

    def user_request_sms(self, data):
        logger.info('user_request_sms %s' % data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "request_sms_failed",
                        "title": _("Request SMS failed"),
                        "msg": _("An error occurred. Please try to request again.")
                    }
                }

        try:
            gs = self.general_business
            pool = self.pool_conn
            email = data.get('email')
            userId = data.get('userId')
            if email:
                value = email
                sql = """select id,
                            phone,
                            validation_status
                        from user_account where email = %s """
            if userId:
                value = userId
                sql = """select id,
                            phone,
                            validation_status
                        from user_account where id = %s """
            res = gs.sql_execute(pool, sql, para_values=[value])
            if res:
                if res[0][2] == 'inactive':
                    sms_opt = random.sample(range(1000, 9999), 1)
                    twilio_config = json.loads(config.get('environment', 'twilio_config'))
                    account_sid = twilio_config['account_sid']
                    auth_token = twilio_config['auth_token']
                    phone_to = twilio_config['phone_to']
                    messaging_service_sid = twilio_config['messaging_service_sid']
                    client = twilio_Client(account_sid, auth_token)
                    client.messages.create(
                        # from_ = phone_to,
                        messaging_service_sid=messaging_service_sid,
                        to=res[0][1],
                        body="Your DoctorDoctor activate code is: %s" % sms_opt[0]
                    )
                    sms_expiry_time = (datetime.now() + relativedelta(minutes=10)).strftime('%Y-%m-%d %H:%M:%S')
                    sql_update = '''update user_account 
                                        set phone_confirm_token = %s,
                                            phone_confirm_token_expiry_time = %s 
                                    where id = %s'''
                    gs.sql_execute(pool, sql_update, commit=True, para_values=(sms_opt[0], sms_expiry_time, res[0][0]))
                    resp.update({
                        'status_code': 200,
                        'resq_body': {
                            'userId': res[0][0],
                            'phoneNumber': res[0][1]
                        }
                    })
                else:
                    resp.update({
                        'status_code': 401,
                        'resq_body': {
                            "code": "activated_account",
                            "title": _("Activated Account"),
                            'msg': _("The account is already activated")

                        }
                    })

            else:
                resp.update({
                    'status_code': 401,
                    'resq_body': {
                        "code": "no_exist_user",
                        "title": _("No exist user"),
                        "msg": _("The user does not exists in our system, please try to register with one")
                    }
                })
        except Exception as e:
            logger.error('error_user_request_sms %s' % str(e))
            client_sentry.captureException()

        return resp
    
    # MP2.7 API for patient to request new password (forgot password)
    def request_new_password(self, data):
        logger.info('request_new_password %s' % data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "request_new_password_failed",
                        "title": _("Request New Password failed"),
                        "msg": _("An error occurred. Please try to request again.")
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            if data and data.get('email', False):
                new_pass = gs.randompassword()
                email = data.get('email')
                sql = """select id from user_account ua where email = %s """
                res = gs.sql_execute(pool, sql, para_values=[email])
                if res:
                    sql_update = '''update user_account set password = %s where email = %s '''
                    gs.sql_execute(pool, sql_update, commit=True, para_values=(new_pass, email))
                    self.send_email_reset_password(email, new_pass)
                    resp.update({
                        'status_code': 200,
                        'resq_body': {
                            "code": "success",
                            "title": _("Success"),
                            "msg": _("Your new password has been sent to you via email. Please check your email inbox!")
                        }
                    })
                else:
                    resp.update({
                        'status_code': 401,
                        'resq_body': {
                            "code": "not_exist_email",
                            "title": _("Not Exist Email"),
                            "msg": _("The email does not exist in our system. Please check and try again")
                        }
                    })
        except Exception as e:
            logger.error('error_request_new_password %s' % str(e))
            client_sentry.captureException()
        return resp
    def request_new_password_hospital(self, data):
        logger.info('request_new_password_hospital %s' % data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "request_new_password_failed",
                        "title": _("Request New Password failed"),
                        "msg": _("An error occurred. Please try to request again.")
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            hospital_id = data.get('hospital_id')
            if data and data.get('email', False):
                new_pass = gs.randompassword()
                email = data.get('email')
                sql = """select ua.id from user_account ua left join doctor_profile dp on dp.user_mngment_id = ua.user_mngment_id left join patient_profile pp on pp.user_mngment_id = ua.user_mngment_id where ua.email = %s and (dp.hospital_profile_id = %s or pp.hospital_profile_id = %s) """
                res = gs.sql_execute(pool, sql, para_values=(email,hospital_id,hospital_id))
                
                logger.info('res info %s' % res)
                if res:
                    sql_update = '''update user_account set password = %s where id = %s '''
                    gs.sql_execute(pool, sql_update, commit=True, para_values=(new_pass, res[0][0]))
                    self.send_email_reset_password(email, new_pass)
                    resp.update({
                        'status_code': 200,
                        'resq_body': {
                            "code": "success",
                            "title": _("Success"),
                            "msg": _("Your new password has been sent to you via email. Please check your email inbox!")
                        }
                    })
                else:
                    resp.update({
                        'status_code': 401,
                        'resq_body': {
                            "code": "not_exist_email",
                            "title": _("Not Exist Email"),
                            "msg": _("The email does not exist in our system. Please check and try again")
                        }
                    })
        except Exception as e:
            logger.error('error_request_new_password %s' % str(e))
        return resp
    def change_password(self, data):
        logger.info('change_password %s' % data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "change_password_failed",
                        "title": _("Change Password failed"),
                        "msg": _("An error occurred. Please try to request again.")
                    }
                }
        
        try:
            gs = self.general_business
            pool = self.pool_conn
            if data and data.get('cur_pass', False) and data.get('new_pass', False) \
                    and data.get('user_token', False):
                cur_password = data.get('cur_pass')
                user_token = data.get('user_token')
                new_password = data.get('new_pass')
                
                if gs.isPwdValid(new_password) == False:
                    resp = {'status_code': 401,
                        'resq_body':
                            {
                                "code": "bad_password",
                                "title": _("Register failed"),
                                "msg": _("The password requires 6 charaters with at least 1 capital and 1 number")
                            }
                        }
                    return resp

                sql = """select user_account_id from user_account_token where token=%s"""
                res = gs.sql_execute(pool, sql, para_values=(user_token,))
                if res:
                    user_id = res[0][0]
                    if user_id:
                        sql_check_user = '''
                            select id from user_account 
                             where id=%s and password=%s'''
                        res_check_user = gs.sql_execute(pool, sql_check_user, para_values=(user_id, cur_password))
                        if res_check_user:
                            sql_pass_update = '''update user_account set 
                                                        password = %s where id = %s'''
                            gs.sql_execute(pool, sql_pass_update, commit=True, para_values=(new_password, user_id))
                            resp.update({'status_code': 200,
                                         'resq_body': {
                                             "code": "success",
                                             "title": _("Success"),
                                             "msg": _("Change password successful.")
                                         }
                                         })
                        else:
                            resp.update({'status_code': 401,
                                         'resq_body': {
                                             "code": "wrong_current_password",
                                             "title": _("Change password failed"),
                                             "msg": _("The current password you entered is incorrect, please try again")
                                         }
                                         })
                else:
                    resp.update({'status_code': 203,
                                 'resq_body': {
                                     "code": "expired_token",
                                     "title": _("Expired token"),
                                     "msg": _("Your login-token is expired, please try to login again")
                                 }
                                 })
        except Exception as e:
            logger.error('error_change_password %s' % str(e))
            client_sentry.captureException()
        return resp

    def user_update_phone(self, data):
        logger.info('user_update_phone %s' % data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "code": "update_phone_failed",
                        "title": _("Update Phone failed"),
                        "msg": _("An error occurred. Please try to request again.")
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            if data.get('user_id', False) and data.get('new_phone', False):
                user_id = data.get('user_id')
                new_phone = data.get('new_phone')
                sql_check = """select id, phone_confirmed, phone, validation_status from user_account where phone = %s """
                check_exist = gs.sql_execute(pool, sql_check, para_values=[new_phone])
                if not check_exist:
                    sql = """select id, phone_confirmed, phone, validation_status from user_account where id = %s """
                    res = gs.sql_execute(pool, sql, para_values=[user_id])
                    if res:
                        if res[0][1] == True:
                            resp.update({'status_code': 401,
                                         'resq_body': {
                                             "code": "activated_by_SMS",
                                             "title": _("Update the phone failed"),
                                             "msg": _("Your account was activated by SMS already")
                                         }
                                         })
                        else:
                            sql_update = '''update user_account set phone = %s where id = %s '''
                            gs.sql_execute(pool, sql_update, commit=True, para_values=(new_phone, user_id))
                            resp.update({'status_code': 200,
                                         'resq_body': {
                                             "code": "success",
                                             "title": _("Success"),
                                             "msg": _("Update phone successful.")
                                         }
                                         })
                else:
                    resp.update({'status_code': 401,
                                 'resq_body': {
                                     "code": "exist_phone",
                                     "title": _("Update the phone failed"),
                                     "msg": _("The phone number already exists in our system, please try to update with another one")
                                 }
                                 })
        except Exception as e:
            logger.error('error_user_update_phone %s - %s' % (data, e))
            client_sentry.captureException()
        return resp

    def user_settings_set(self, data):
        logger.info('user_settings_set %s' % data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Set setting failed"),
                        "msg": _("An error occurred. Please try to set setting again."),
                        "code": "set_setting_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            language_code = data.get('language_code')
            automatic_video = data.get('automatic_video')
            if user_token:
                res_token = gs.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    sql_select = '''select 1 from user_setting where user_account_id = %s'''
                    setting = gs.sql_execute(pool, sql_select, commit=False, para_values=(user_id,))
                    if setting:
                        sql_update = '''update user_setting
                                        set language_code = %s,
                                            automatic_video = %s
                                        where user_account_id = %s'''
                        gs.sql_execute(pool, sql_update, commit=True,
                                       para_values=(language_code, automatic_video, user_id))
                    else:
                        sql_insert = '''insert into user_setting (language_code,
                                                                  automatic_video,
                                                                  user_account_id
                                                                  )
                                        values (%s, %s, %s)'''
                        gs.sql_execute(pool, sql_insert, commit=True, para_values=(language_code,
                                                                                   automatic_video,
                                                                                   user_id))

                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     "code": "success",
                                     "title": _("Success"),
                                     "msg": _("Update setting successful.")
                                 }
                                 })

                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }

        except Exception as e:
            logger.error('user_settings_set %s' % str(e))
            client_sentry.captureException()
        return resp

    def user_settings_get(self, data):
        logger.info('user_settings_get %s' % data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get setting failed"),
                        "msg": _("An error occurred. Please try to get setting again."),
                        "code": "get_setting_failed"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            user_token = data.get('user_token')
            if user_token:
                res_token = gs.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    sql_update = '''select language_code,
                                            automatic_video
                                    from user_setting
                                    where user_account_id = %s'''
                    res = gs.sql_execute(pool, sql_update, para_values=(user_id,))
                    if res:
                        resp.update({
                            'status_code': 200,
                            'resq_body': {
                                'languageCode': res[0][0],
                                'automaticVideo': res[0][1]
                            }
                        })
                    else:
                        resp.update({
                            'status_code': 200,
                            'resq_body': {
                                'languageCode': 'en',
                                'automaticVideo': True
                            }
                        })

                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }

        except Exception as e:
            logger.error('user_settings_get %s' % str(e))
            client_sentry.captureException()
        return resp

    # WD2.17 API for listing the documents of an inactivated doctor account
    def user_register_v2_getdocs(self, data):
        logger.info('user_register_v2_getdocs %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Get document list"),
                        "msg": _("An error occurred. Please try to set again."),
                        "code": "doctor_get_document_list"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            
            userId = data['userId']

            sql = """SELECT validation_status, user_mngment_id FROM user_account where id = %s """
            user_mngment_id = gs.sql_execute(pool, sql, para_values=[userId])[0]
            if user_mngment_id:
                if user_mngment_id[0] == 'active':
                    resp = {'status_code': 401,
                            'resq_body':
                                {
                                    "code": "activated_account",
                                	"msg": _("The account is already activated"),
                                    "title": _("Account is activated")
                                }
                            }
                    return resp
                
                sql_select = """select  att.file_extension, --0
                                        att.file_size, --1
                                        att.write_date, --2
                                        att.url, --3
                                        att.file_name, --4
                                        att.create_uid, --5
                                        att.id --6
                                from attachment att
                                where att.res_model = 'user_account' and create_uid = %s """
                records = gs.sql_execute(pool, sql_select, para_values=[userId])
                documentList = []
                for line in records:
                    documentList.append({
                            "id": line[6],
                            "url": line[3],
                            "fileName": line[4],
                            "fileExtension": line[0],
                            "fileSize": self.general_business.bytesToSize(line[1]),
                            "latestUpdateTime": int(time.mktime(line[2].timetuple()))
                        }
                    )
                            

                resp.update({'status_code': 200,
                                 'resq_body': {
                                     'documentList':documentList
                                 }
                                 })
        except Exception as e:
            logger.error('error_user_register_v2_getdocs %s' % str(e))
            client_sentry.captureException()

        return resp

    # WD3.12 API for doctor to delete specific profile document
    def user_documents_delete(self, data):
        logger.info('user_documents_delete %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Delete document"),
                        "msg": _("An error occurred. Please try to set again."),
                        "code": "doctor_delete_document"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn

            documentId = data['documentId']
            user_token = data.get('user_token')
            if user_token:
                res_token = gs.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    sql_select = """select  att.create_uid
                                    from attachment att
                                    where att.res_model = 'user_account' 
                                        and att.id = %s 
                                        and att.res_id = %s"""
                    record = gs.sql_execute(pool, sql_select, para_values=(documentId,user_id))

                    if record:
                        sql_delete = """delete from attachment where id = %s """
                        gs.sql_execute(pool, sql_delete, para_values=[documentId], commit=True)
                    else:
                        resp = {'status_code': 401,
                                'resq_body':
                                    {
                                        "title": _("Invalid file owner"),
                                        "code": "invalid_file_owner",
                                        "msg": _("The file is not belong to you")

                                    }
                                }
                        return resp

                    resp.update({'status_code': 200,
                                 'resq_body': {
                                     "title": _("Success"),
                                     "msg": _("Delete document successfully"),
                                     "code": "success"
                                 }
                                 })

                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }

        except Exception as e:
            logger.error('error_user_documents_delete %s' % str(e))
            client_sentry.captureException()

        return resp

    # WD2.18 API for deleting a specific document of an inactivated doctor account
    def user_register_v2_deletedoc(self, data):
        logger.info('user_register_v2_deletedoc %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Delete document"),
                        "msg": _("An error occurred. Please try to set again."),
                        "code": "doctor_delete_document"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn

            userId = data['userId']
            fileId = data['fileId']

            sql = """SELECT validation_status, user_mngment_id FROM user_account where id = %s """
            user_mngment_id = gs.sql_execute(pool, sql, para_values=[userId])[0]
            if user_mngment_id:
                if user_mngment_id[0] == 'active':
                    resp = {'status_code': 401,
                            'resq_body':
                                {
                                    "code": "activated_account",
                                	"msg": _("The account is already activated"),
                                    "title": _("Account is activated")
                                }
                            }
                    return resp

                sql_select = """select  att.create_uid
                                from attachment att
                                where att.res_model = 'user_account' and att.id = %s """
                record = gs.sql_execute(pool, sql_select, para_values=[fileId])

                if record:
                    sql_delete = """delete from attachment where id = %s """
                    gs.sql_execute(pool, sql_delete, para_values=[fileId],commit=True)
                else:
                    resp = {'status_code': 401,
                            'resq_body':
                                {
                                    "title": _("Invalid file owner"),
                                    "code": "invalid_file_owner",
                                	"msg": _("The file is not belong to you")

                                }
                            }
                    return resp

                resp.update({'status_code': 200,
                                 'resq_body': {
                                    "title": _("Success"),
                                    "msg": _("Delete document successfully"),
                                    "code": "success"
                                 }
                                 })
        except Exception as e:
            logger.error('error_user_register_v2_deletedoc %s' % str(e))
            client_sentry.captureException()

        return resp

    # WD2.19 API for sending application form
    def user_register_v2_sendtheform(self, data):
        logger.info('user_register_v2_sendtheform %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Send application form"),
                        "msg": _("An error occurred. Please try to set again."),
                        "code": "send_application_form"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn
            
            userId = data['userId']

            sql = """SELECT validation_status, user_mngment_id FROM user_account where id = %s """
            user_mngment_id = gs.sql_execute(pool, sql, para_values=[userId])[0]
            if user_mngment_id:
                if user_mngment_id[0] == 'active':
                    resp = {'status_code': 401,
                            'resq_body':
                                {
                                    "code": "activated_account",
                                	"msg": _("The account is already activated"),
                                    "title": _("Account is activated")
                                }
                            }
                    return resp
            
            sql = '''with doctor_languages as
                            (
                                select dp.id,
                                    array_to_string(array_agg(distinct mdl.name), ', ') as spoken_language
                                from doctor_profile dp
                                left join dr_doctorprofile_languages sla on sla.doctor_profile_id = dp.id
                                left join meta_dr_languages mdl on mdl.code = sla.spoken_language
                                left join user_account ua on ua.user_mngment_id = dp.user_mngment_id
                                where ua.id = %s
                                group by 1
                            )
                            select  distinct title,--0
                                            profile_image_url,--1
                                            first_name,--2
                                            middle_name,--3
                                            last_name,--4
                                            complete_name,--5
                                            type_profile,--6
                                            birthday,--7
                                            gender,--8
                                            sla.spoken_language,--9
                                            
                                            ua.email account_email,--10
                                            ua.phone account_phone,--11
                                            
                                            coalesce(is_primary, False) is_primary,--12
                                            home_phone,--13
                                            work_phone, --14
                                            ua.phone,--15 mobile_phone
                                            uc.email,--16
                                            uad.number,--17
                                            street,--18
                                            city,--19
                                            state,--20
                                            postcode,--21
                                            country,--22
                                            
                                            consultation_type,--23
                                            consultation_duration,--24
                                            consultation_fee,--25
                                            consultation_unit,--26
                                            years_of_experience,--27
                                            
                                            medical_school,--28
                                            graduation_date,--29
                                            post_graduate_training,--30
                                            industry_relationship,--31
                                            publications,--32
                                            teaching,--33
                                            community_service,--34
                                            criminal_convictions,--35
                                            limitations, --36
                                            hospital_restrictions,--37
                                            about,--38
                                            dl.number license_number,--39
                                            license_status,--40
                                            expiry_date,--41
                                            issue_date,--42
                                            issue_by, --43
                                            
                                            mds.name, --44
                                            spe.years_of_exp, --45 
                                            coalesce(is_primary, False) is_primary,--46
                                            dp.professional_affiliations_and_activities, -- 47
                                            coalesce(round((sum_rating_point/count_rating_point)::numeric,1),0)::float rating_point, --48
                                            case when ua.latest_activity_time > current_timestamp - interval '15 minutes' then True
                                                else False
                                            end onlineStatus, --49
                                            ua.activity_status, -- 50
                                            um.degree_title --51
                                    from user_mngment um
                                        left join user_account ua on ua.user_mngment_id = um.id
                                        left join doctor_profile dp on  um.id = dp.user_mngment_id 
                                        left join dr_doctorprofile_specialties spe on spe.doctor_profile_id = dp.id
                                        left join doctor_languages sla on sla.id = dp.id
                                        left join dr_license dl on dl.doctor_profile_id = dp.id
                                        left join user_contact uc on um.id = uc.user_mngment_id  
                                        left join user_address uad on uad.user_contact_id = uc.id
                                        left join meta_dr_specialties mds on mds.code = spe.specialties_code
                                    where ua.id =%s '''
            records = gs.sql_execute(pool, sql, para_values=(userId, userId))

            if records:
                primaryPracticalAddress = {
                    #                                                 'defaultPhone': None,
                    'phone': None,
                    "number": None,
                    "street": None,
                    "cityCode": None,
                    "state": None,
                    "postCode": None,
                    "countryCode": None,
                }

                secondaryPracticalAddress = {
                    #                                                 'defaultPhone': None,
                    'phone': '',
                    "number": '',
                    "street": '',
                    "cityCode": '',
                    "state": '',
                    "postCode": '',
                    "countryCode": '',
                }
                list_specialtyCodes = []
                list_specialty_txt = ''
                for line in records:
                    if line[46] == True:
                        primaryPracticalAddress = {
                            # 'defaultPhone': line[11] or None,
                            'phone': line[14] or '',
                            "number": line[17] or '',
                            "street": line[18] or '',
                            "cityCode": line[19] or '',
                            "state": line[20] or '',
                            "postCode": line[21] or '',
                            "countryCode": line[22] or ''
                        }
                    elif line[46] == False:
                        secondaryPracticalAddress = {
                            # 'defaultPhone': line[11] or None,
                            'phone': ' ' if line[14] in ('False', 'None', '') or not line[14] else line[14],
                            "number": ' ' if line[17] in ('False', 'None', '') or not line[17] else line[17],
                            "street": ' ' if line[18] in ('False', 'None', '') or not line[18] else line[18],
                            "cityCode": ' ' if line[19] in ('False', 'None', '') or not line[19] else line[19],
                            "state": ' ' if line[20] in ('False', 'None', '') or not line[20] else line[20],
                            "postCode": ' ' if line[21] in ('False', 'None', '') or not line[21] else line[21],
                            "countryCode": ' ' if line[22] in ('False', 'None', '') or not line[22] else line[22]
                        }
                    if line[44] not in list_specialtyCodes:
                        list_specialtyCodes.append(line[44])
                        list_specialty_txt = list_specialty_txt + '''\n\t\t\t    + %s - %s years'''%(line[44],line[45])
                
                sql_select = """select att.url
                        from attachment att
                        where att.res_model = 'user_account' and create_uid = %s """
                records2 = gs.sql_execute(pool, sql_select, para_values=[userId])
                documentLinks = ''
                for line in records2:
                    documentLinks = documentLinks + '''\n\t\t\t    + %s'''%(line[0])

                application_form = '''
                    - First name: %s
                    - Middle name: %s
                    - Last name: %s
                    - Birthday: %s
                    - Email: %s
                    - Phone: %s
                    - Degree title: %s
                    - Primary Practical Address:
                            + Phone: %s
                            + Number: %s
                            + Street: %s
                            + City: %s
                            + State: %s
                            + Code: %s
                    - Secondary Practical Address:
                            + Phone: %s
                            + Number: %s
                            + Street: %s
                            + City: %s
                            + State: %s
                            + Code: %s
                    - Language: %s
                    - Specialty: %s
                    - License:
                            + Number: %s
                            + LlicenseStatus: %s
                            + IssueDate: %s
                            + ExpiryDate: %s
                            + IssueBy: %s
                    - Medical school: %s
                    - Graduation date": %s
                    - Post graduate training: %s
                    - Industry relationship: %s
                    - Publications: %s
                    - Teaching: %s
                    - Community service: %s
                    - Criminal convictions: %s
                    - Limitations: %s
                    - Hospital restrictions: %s
                    - Professional affiliations and activities": %s
                    - About me: %s
                    - Attachments: %s
                ''' % (records[0][2] or None, 
                        records[0][3] or None, 
                        records[0][4] or None, 
                None if records[0][7] in ('False', 'None', '') or not records[0][7] else records[0][7].strftime('%d-%m-%Y'),
                records[0][10] or None, 
                records[0][11] or None,
                records[0][51], 
                primaryPracticalAddress['phone'],
                primaryPracticalAddress['number'],
                primaryPracticalAddress['street'],
                primaryPracticalAddress['cityCode'],
                primaryPracticalAddress['postCode'],
                primaryPracticalAddress['countryCode'],
                secondaryPracticalAddress['phone'],
                secondaryPracticalAddress['number'],
                secondaryPracticalAddress['street'],
                secondaryPracticalAddress['cityCode'],
                secondaryPracticalAddress['postCode'],
                secondaryPracticalAddress['countryCode'],
                records[0][9] and records[0][9].split(', ') or [],
                list_specialty_txt,
                records[0][39] or None,records[0][40] or None,
                None if records[0][41] in ('False', 'None', '') or not records[0][41] else records[0][41].strftime('%d-%m-%Y'),
                None if records[0][42] in ('False', 'None', '') or not records[0][42] else records[0][42].strftime('%d-%m-%Y'),
                records[0][43] or None, 
                records[0][28] or None, 
                None if records[0][29] in ('False', 'None', '') or not records[0][29] else records[0][29].strftime('%d-%m-%Y'),
                records[0][30] or None,
                records[0][31] or None,
                records[0][32] or None,
                records[0][33] or None,
                records[0][34] or None,
                records[0][35] or None,
                records[0][36] or None, 
                records[0][37] or None,
                records[0][47] or None, 
                records[0][38] or None,
                documentLinks)

            # self.send_email_application_form(application_form,userId)
            mail_thread = send_email_thread(application_form,userId)
            mail_thread.start()
            resp.update({'status_code': 200,
                                 'resq_body': {
                                    "title": _("Success"),
                                    "msg": _("Send application form successfully"),
                                    "code": "success"
                                }
                        })
        except Exception as e:
            logger.error('error_user_register_v2_sendtheform %s' % str(e))
            client_sentry.captureException()

        return resp

    # WD2.20 API for doctor to go online with a specific duration
    def doctor_goOnline_v2(self, data):
        logger.info('doctor_goOnline_v2 %s', data)
        _ = self.general_business.get_lang_instance(data.get('lang', 'en'))
        resp = {'status_code': 401,
                'resq_body':
                    {
                        "title": _("Set doctor online"),
                        "msg": _("An error occurred. Please try to set again."),
                        "code": "set_doctor_online"
                    }
                }
        try:
            gs = self.general_business
            pool = self.pool_conn

            duration = data.get('duration', None)
            if not duration:
                return resp

            value = duration.get('value')
            unit = duration.get('unit')

            if unit != 'minute':
                return resp

            if value < 30 or value > 240:
                return resp

            user_token = data.get('user_token')
            if user_token:
                res_token = gs.validate_user_token(user_token,data.get('client_id'))
                if res_token:
                    user_id = res_token[1]
                    sql = '''update user_account set activity_status = 'online', checkout_time = CURRENT_TIMESTAMP + (%s * interval '1 minute') where id = %s'''
                    record = gs.sql_execute(pool, sql, para_values=(value,user_id),commit=True)
                    resp.update({'status_code': 200,
                                'resq_body': {
                                    "code": "success",
                                    "title": _("Success"),
                                    "msg": _("Set doctor online successfully.")
                                }
                                })

                else:
                    resp = {'status_code': 203,
                            'resq_body':
                                {
                                    "code": "expired_token",
                                    "title": _("Expired token"),
                                    "msg": _("Your login-token is expired, please try to login again")
                                }
                            }

        except Exception as e:
            logger.error('error_doctor_goOnline_v2 %s' % str(e))
            client_sentry.captureException()

        return resp